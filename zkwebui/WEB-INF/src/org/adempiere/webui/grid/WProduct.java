package org.adempiere.webui.grid;

import java.util.logging.Level;

import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.VerticalBox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.window.FDialog;
import org.compiere.model.MPriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MRole;
import org.compiere.model.MUOM;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Separator;

/**
 * Product Quick Entry
 *
 * @author jtrinidad
 *
 */

public class WProduct extends Window implements EventListener, ValueChangeListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5842369060073088746L;

	private static CLogger log = CLogger.getCLogger(WProduct.class);
	
	private int m_WindowNo;
	
	/** The product				*/
	private MProduct m_product = null;
	
	
	/** Read Only				*/
	private boolean m_readOnly = false;

	private Object[] m_uom;
	
	private Textbox fValue = new Textbox();
	private Textbox fName = new Textbox();
	private Textbox fDescription = new Textbox();
	private Listbox fUOM = new Listbox();
	
	
	private VerticalBox centerPanel = new VerticalBox();
	
	private ConfirmPanel confirmPanel = new ConfirmPanel(true, false, false, false, false, false);
	
	/**
	 *	Constructor.
	 *	Requires call loadBPartner
	 * 	@param frame	parent
	 * 	@param WindowNo	Window No
	 */
	
	public WProduct(int WindowNo)
	{
		super();
		
		m_WindowNo = WindowNo;
		m_readOnly = !MRole.getDefault().canUpdate(
			Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()), 
			MProduct.Table_ID, 0, false);
		log.info("R/O=" + m_readOnly);
		
		try
		{
			jbInit();
		}
		catch(Exception ex)
		{
			log.log(Level.SEVERE, ex.getMessage());
		}
		
		initProduct();
		
	}	//	WProduct
	
	/**
	 *	Static Init
	 * 	@throws Exception
	 */
	
	void jbInit() throws Exception
	{
		this.setWidth("350px");
		this.setBorder("normal");
		this.setClosable(true);
		this.setTitle("Product");
		this.setAttribute("mode", "modal");
		this.appendChild(centerPanel);
		this.appendChild(confirmPanel);
		
		confirmPanel.addActionListener(Events.ON_CLICK, this);
	}
	
	/**
	 *	Dynamic Init
	 */
	private void initProduct()
	{
		//	Get Data
		m_uom = fillUOM();

		//	Value
		fValue.addEventListener(Events.ON_CHANGE , this);
		createLine (fValue, "Value", true);
		
		
		//	Name
		fName.addEventListener(Events.ON_CLICK, this);
		createLine (fName, "Name", false)/*.setFontBold(true)*/;

		//	Name2
		createLine (fDescription, "Description", false);
		
		//	UOM
		fUOM.setMold("select");
		fUOM.setRows(0);
		
		for (int i = 0; i < m_uom.length; i++)
			fUOM.appendItem(m_uom[i].toString(), m_uom[i]);
		createLine (fUOM, "UOM", false);
		
		
	}	//	initProduct

	/**
	 * 	Create Line
	 * 	@param field 	field
	 * 	@param title	label value
	 * 	@param addSpace	add more space
	 * 	@return label
	 */
	
	private Label createLine (Component field, String title, boolean addSpace)
	{
		Hbox hbox = new Hbox(); 
		
		hbox.setWidth("100%");
		hbox.setWidths("30%, 70%");
		
		Label label = new Label(Msg.translate(Env.getCtx(), title));
		hbox.appendChild(label);

		hbox.appendChild(field);
		
		centerPanel.appendChild(hbox);
		centerPanel.appendChild(new Separator());
		
		return label;
	}	//	createLine

	/**
	 *	Fill Greeting
	 * 	@return KeyNamePair Array of Greetings
	 */
	
	private Object[] fillUOM()
	{
		String sql = "SELECT C_UOM_ID, Name FROM C_UOM WHERE IsActive='Y' ORDER BY 2";
		sql = MRole.getDefault().addAccessSQL(sql, "C_UOM", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO);
		
		return DB.getKeyNamePairs(sql, true);
	}	//	fillGreeting

	/**
	 *	Search m_greeting for key
	 * 	@param key	C_Greeting_ID
	 * 	@return	Greeting
	 */
	
	private KeyNamePair getUOM(int key)
	{
		for (int i = 0; i < m_uom.length; i++)
		{
			KeyNamePair p = (KeyNamePair)m_uom[i];
			if (p.getKey() == key)
				return p;
		}
		
		return new KeyNamePair(-1, " ");
	}	//	getGreeting

	/**
	 *	Load BPartner
	 *  @param C_BPartner_ID - existing BPartner or 0 for new
	 * 	@return true if loaded
	 */
	
	public boolean loadProduct (int M_Product_ID)
	{
		log.config("M_Product_ID=" + M_Product_ID);
		
		//  New Product
		if (M_Product_ID == 0)
		{
			m_product = null;
			return true;
		}

		m_product = new MProduct(Env.getCtx(), M_Product_ID, null);
		
		if (m_product.get_ID() == 0)
		{
			FDialog.error(m_WindowNo, this, "MProductNotFound");
			return false;
		}

		//	Product - Load values
		fValue.setText(m_product.getValue());
		
		KeyNamePair keynamepair = getUOM(m_product.getC_UOM_ID());
		
		for (int i = 0; i < fUOM.getItemCount(); i++)
		{
			ListItem listitem = fUOM.getItemAtIndex(i);
			KeyNamePair compare = (KeyNamePair)listitem.getValue();
			
			if (compare == keynamepair)
			{
				fUOM.setSelectedIndex(i);
				break;
			}
		}
		
		fName.setText(m_product.getName());
		fDescription.setText(m_product.getDescription());

		return true;
	}	//	loadBPartner

	/**
	 *	Save.
	 *	Checks mandatory fields and saves Partner, Contact and Location
	 * 	@return true if saved
	 */
	
	private boolean actionSave()
	{
		log.config("");

		//	Check Mandatory fields
		if (fName.getText().equals(""))
		{
			throw new WrongValueException(fName, Msg.translate(Env.getCtx(), "FillMandatory"));
		}
			

		//	***** Product *****
		
		if (m_product == null)
		{
			int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
			m_product = MProduct.getTemplate(Env.getCtx(), AD_Client_ID);
			m_product.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
		}
		
		//	Check Value
		
		String value = fValue.getText();
		
		if (value == null || value.length() == 0)
		{
			//	get Table Document No
			value = DB.getDocumentNo (Env.getAD_Client_ID(Env.getCtx()), "M_Product", null);
			fValue.setText(value);
		}
		
		m_product.setValue(fValue.getText());
		
		m_product.setName(fName.getText());
		m_product.setDescription(fDescription.getText());
		
		ListItem listitem = fUOM.getSelectedItem();
		KeyNamePair p = listitem != null ? (KeyNamePair)listitem.getValue() : null;
		
		if (p != null && p.getKey() > 0)
			m_product.setC_UOM_ID(p.getKey());
		else
			m_product.setC_UOM_ID(MUOM.getDefault_UOM_ID(Env.getCtx()));;
		
		if (m_product.save())
			log.fine("M_Product_ID=" + m_product.getM_Product_ID());
		else
			FDialog.error(m_WindowNo, this, "MProductNotSaved");
		
		int priceList_ID = Env.getContextAsInt(Env.getCtx(), m_WindowNo, "M_PriceList_ID");
		if (priceList_ID != 0) {
			MPriceList pl = new MPriceList(Env.getCtx(), priceList_ID, null);
			MPriceListVersion plv = pl.getPriceListVersion(null);
			MProductPrice price = MProductPrice.get(Env.getCtx(),plv.getM_PriceList_Version_ID(), m_product.getM_Product_ID(), null); 
			if (price == null) {
				price = new MProductPrice(Env.getCtx(), plv.getM_PriceList_Version_ID() 
						, m_product.getM_Product_ID(), Env.ONE, Env.ONE, Env.ONE, null);
			} else {
				price.setPrices(Env.ONE, Env.ONE,Env.ONE);
			}
			price.save();
		}
		
		return true;
	}	//	actionSave

	/**
	 *	Returns Product ID
	 *	@return M_Product_ID (0 = not saved)
	 */
	
	public int getM_Product_ID()
	{
		if (m_product == null)
			return 0;
		
		return m_product.getM_Product_ID();
	}	//	getM_Product_ID

	public void onEvent(Event e) throws Exception 
	{
		if (m_readOnly)
			this.detach();
		
		//	copy value
		
		else if (e.getTarget() == fValue)
		{
			if (fName.getText() == null || fName.getText().length() == 0)
				fName.setText(fValue.getText());
		}
		
		//	OK pressed
		else if ((e.getTarget() == confirmPanel.getButton("Ok")) && actionSave())
			this.detach();
		
		//	Cancel pressed
		else if (e.getTarget() == confirmPanel.getButton("Cancel"))
			this.detach();
		
	}

	public void valueChange(ValueChangeEvent evt)
	{
		
	}
}