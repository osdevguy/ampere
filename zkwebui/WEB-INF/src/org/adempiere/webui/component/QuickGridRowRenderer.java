/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin                                            *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.component;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.editor.WButtonEditor;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WNumberEditor;
import org.adempiere.webui.editor.WPAttributeEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WebEditorFactory;
import org.adempiere.webui.panel.AbstractADWindowPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.GridTabDataBinder;
import org.adempiere.webui.window.ADWindow;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.GridWindow;
import org.compiere.model.MLookupInfo;
import org.compiere.util.CLogger;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.NamePair;
import org.zkoss.xml.XMLs;
import org.zkoss.zhtml.Input;
import org.zkoss.zhtml.Label;
import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Text;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Paging;
import org.zkoss.zul.RendererCtrl;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;

/**
 * Row renderer for GridTab grid.
 * @author hengsin
 * 
 * @author Teo Sarca, teo.sarca@gmail.com
 * 		<li>BF [ 2996608 ] GridPanel is not displaying time
 * 			https://sourceforge.net/tracker/?func=detail&aid=2996608&group_id=176962&atid=955896
 */
public class QuickGridRowRenderer implements RowRenderer, RowRendererExt, RendererCtrl, EventListener {

	private static final String CURRENT_ROW_STYLE = "border-top: 2px solid #6f97d2; border-bottom: 2px solid #6f97d2";
	public static final String GRID_ROW_INDEX_ATTR = "grid.row.index";
	public static final String GRID_ROW_COLUMNNAME_ATTR = "quickentry.columnname";
	private static final int MAX_TEXT_LENGTH = 60;
	private GridTab gridTab;
	private int windowNo;
	private GridTabDataBinder dataBinder;
	private Map<GridField, WEditor> editors = new LinkedHashMap<GridField, WEditor>();
	private Paging paging;

	private Map<String, Map<Object, String>> lookupCache = null;
	private RowListener rowListener;

	private Grid grid = null;
	private QuickGridPanel gridPanel = null;
	private Row currentRow;
	private Object[] currentValues;
	private boolean editing = false;
	private int currentRowIndex = -1;
	private AbstractADWindowPanel m_windowPanel;
	GridWindow gridWindow = null;
	MLookupInfo info = null;
	
	private static CLogger log = CLogger.getCLogger(QuickGridRowRenderer.class);
	
	/**
	 *
	 * @param gridTab
	 * @param windowNo
	 */
	public QuickGridRowRenderer(GridTab gridTab, int windowNo) {
		this.gridTab = gridTab;
		this.windowNo = windowNo;
		this.dataBinder = new GridTabDataBinder(gridTab);
	}

	private WEditor getEditorCell(GridField gridField, Object object) {
		WEditor editor = WebEditorFactory.getEditor(gridField, true);
		if (editor != null)  {
			if (editor instanceof WButtonEditor)
            {
				if (m_windowPanel != null)
				{
					((WButtonEditor)editor).addActionListener(m_windowPanel);	
				}
				else
				{
					Object window = SessionManager.getAppDesktop().findWindow(windowNo);
	            	if (window != null && window instanceof ADWindow)
	            	{
	            		AbstractADWindowPanel windowPanel = ((ADWindow)window).getADWindowPanel();
	            		((WButtonEditor)editor).addActionListener(windowPanel);
	            	}
				}
            }
			else
			{
				editor.addValueChangeListener(dataBinder);
			}
			gridField.removePropertyChangeListener(editor);
			gridField.addPropertyChangeListener(editor);
			editor.setValue(object);

            if(editor instanceof WNumberEditor)
            {
            	Component tr= (Component)((Component)editor.getComponent().getChildren().get(0)).getChildren().get(0);
            	if(tr.getChildren().size() == 2) {
            		HtmlBasedComponent db = (HtmlBasedComponent) ((Component)tr.getChildren().get(0)).getChildren().get(0);
            		db.setWidth("100%");
            		tr.getChildren().remove(1);
            		
            	}
            	((HtmlBasedComponent)editor.getComponent()).setWidth("85%");
            } else {
            	editor.fillHorizontal();	
            }
		}
		return editor;
	}

	private int getColumnIndex(GridField field) {
		GridField[] fields = gridTab.getFields();
		for(int i = 0; i < fields.length; i++) {
			if (fields[i] == field)
				return i;
		}
		return 0;
	}

	private Component createReadonlyCheckbox(Object value) {
		Checkbox checkBox = new Checkbox();
		if (value != null && "true".equalsIgnoreCase(value.toString()))
			checkBox.setChecked(true);
		else
			checkBox.setChecked(false);
		checkBox.setDisabled(true);
		return checkBox;
	}

	private String getDisplayText(Object value, GridField gridField)
	{
		if (value == null)
			return "";

		if (gridField.isEncryptedField())
		{
			return "********";
		}
		else if (gridField.isLookup())
    	{
			if (lookupCache != null)
			{
				Map<Object, String> cache = lookupCache.get(gridField.getColumnName());
				if (cache != null && cache.size() >0)
				{
					String text = cache.get(value);
					if (text != null)
					{
						return text;
					}
				}
			}
			NamePair namepair = gridField.getLookup().get(value);
			if (namepair != null)
			{
				String text = namepair.getName();
				if (lookupCache != null)
				{
					Map<Object, String> cache = lookupCache.get(gridField.getColumnName());
					if (cache == null)
					{
						cache = new HashMap<Object, String>();
						lookupCache.put(gridField.getColumnName(), cache);
					}
					cache.put(value, text);
				}
				return text;
			}
			else
				return "";
    	}
    	else if (gridTab.getTableModel().getColumnClass(getColumnIndex(gridField)).equals(Timestamp.class))
    	{
    		SimpleDateFormat dateFormat = DisplayType.getDateFormat(gridField.getDisplayType(), AEnv.getLanguage(Env.getCtx()));
    		return dateFormat.format((Timestamp)value);
    	}
    	else if (DisplayType.isNumeric(gridField.getDisplayType()))
    	{
    		return DisplayType.getNumberFormat(gridField.getDisplayType(), AEnv.getLanguage(Env.getCtx())).format(value);
    	}
    	else if (DisplayType.Button == gridField.getDisplayType())
    	{
    		return "";
    	}
    	else if (DisplayType.Image == gridField.getDisplayType())
    	{
    		if (value == null || (Integer)value <= 0)
    			return "";
    		else
    			return "...";
    	}
    	else
    		return value.toString();
	}

	@SuppressWarnings("unused")
	private Component getDisplayComponent(Object value, GridField gridField) {
		Component component;
		if (gridField.getDisplayType() == DisplayType.YesNo) {
			component = createReadonlyCheckbox(value);
		} else {
			String text = getDisplayText(value, gridField);

			Label label = new Label();
			setLabelText(text, label);

			component = label;
		}
		return component;
	}

	/**
	 * @param text
	 * @param label
	 */
	private void setLabelText(String text, Label label) {
		String display = text;
		if (text != null && text.length() > MAX_TEXT_LENGTH)
			display = text.substring(0, MAX_TEXT_LENGTH - 3) + "...";
		if (display != null)
			display = XMLs.encodeText(display);
		label.appendChild(new Text(display));
		if (text != null && text.length() > MAX_TEXT_LENGTH)
			label.setDynamicProperty("title", text);
		else
			label.setDynamicProperty("title", "");
	}

	/**
	 *
	 * @return active editor list
	 */
	public List<WEditor> getEditors() {
		List<WEditor> editorList = new ArrayList<WEditor>();
		if (!editors.isEmpty())
			editorList.addAll(editors.values());

		return editorList;
	}

	/**
	 * @param paging
	 */
	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	/**
	 * Detach all editor and optionally set the current value of the editor as cell label.
	 * @param updateCellLabel
	 */
	public void stopEditing(boolean updateCellLabel) {
		if (!editing) {
			return;
		} else {
			editing = false;
		}
		Row row = null;
		for (Entry<GridField, WEditor> entry : editors.entrySet()) {
			if (entry.getValue().getComponent().getParent() != null) {
				Component child = entry.getValue().getComponent();
				Div div = null;
				while (div == null && child != null) {
					Component parent = child.getParent();
					if (parent instanceof Div && parent.getParent() instanceof Row)
						div = (Div)parent;
					else
						child = parent;
				}
				Component component = div.getFirstChild();
				if (updateCellLabel) {
					if (component instanceof Label) {
						Label label = (Label)component;
						label.getChildren().clear();
						String text = getDisplayText(entry.getValue().getValue(), entry.getValue().getGridField());
						setLabelText(text, label);
					} else if (component instanceof Checkbox) {
						Checkbox checkBox = (Checkbox)component;
						Object value = entry.getValue().getValue();
						if (value != null && "true".equalsIgnoreCase(value.toString()))
							checkBox.setChecked(true);
						else
							checkBox.setChecked(false);
					}
				}
				component.setVisible(true);
				if (row == null)
					row = ((Row)div.getParent());

				entry.getValue().getComponent().detach();
				entry.getKey().removePropertyChangeListener(entry.getValue());
				entry.getValue().removeValuechangeListener(dataBinder);
			}
		}

		GridTableListModel model = (GridTableListModel) grid.getModel();
		model.setEditing(false);
	}

	/**
	 * @param row
	 * @param data
	 * @see RowRenderer#render(Row, Object)
	 */
	public void render(Row row, Object data) throws Exception
	{

		Timestamp r1 = new Timestamp(new Date().getTime());
		log.finest("Render = Row START " + row + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS Z").format(r1));

		// don't render if not visible
		if (gridPanel != null && !gridPanel.isVisible())
		{
			return;
		}

		if (grid == null)
			grid = (Grid) row.getParent().getParent();

		if (rowListener == null)
			rowListener = new RowListener((Grid) row.getParent().getParent());

		Object[] dataObj = (Object[]) data;
		GridField[] gridTabFields = gridTab.getFields();
		GridField[] gridField = gridPanel.getGridField(); // gridTab.getFields();
		List<Object> dataList = new ArrayList<Object>();

		for (GridField gField : gridField)
		{
			for (int i = 0; i < gridTabFields.length; i++)
			{
				if (gField.getAD_Field_ID() == gridTabFields[i].getAD_Field_ID())
				{
					dataList.add(dataObj[i]);
					break;
				}
			}
		}
		currentValues = dataList.toArray(new Object[0]);

		Grid grid = (Grid) row.getParent().getParent();
		org.zkoss.zul.Columns columns = grid.getColumns();

		int rowIndex = row.getParent().getChildren().indexOf(row);
		if (paging != null && paging.getPageSize() > 0)
		{
			rowIndex = (paging.getActivePage() * paging.getPageSize()) + rowIndex;
		}

		Div div = new Div();
		div.setAttribute("columnName", Msg.getMsg(Env.getCtx(), "Select"));
		div.setTooltiptext(Msg.getMsg(Env.getCtx(), "Select"));
		div.setStyle("border: none; width: 100%; height: 100%; text-align:center;");
		div.setWidth("40px;");

		Checkbox selection = new Checkbox();
		selection.setAttribute(GRID_ROW_INDEX_ATTR, rowIndex);
		selection.setChecked(gridTab.isSelected(rowIndex));
		selection.addEventListener(Events.ON_CHECK, this);

		if (!selection.isChecked())
		{
			if (gridPanel.selectAll.isChecked())
			{
				gridPanel.selectAll.setChecked(false);
			}
		}

		div.appendChild(selection);
		row.appendChild(div);

		int colIndex = 0;
		for (int i = 0; i < gridField.length; i++)
		{
			if (!gridField[i].isQuickEntry())
			{
				continue;
			}
			colIndex++;

			div = new Div();
			String divStyle = "border: none; width: 100%; height: 100%; ";
			org.zkoss.zul.Column column = (org.zkoss.zul.Column) columns.getChildren().get(colIndex);
			if (column.isVisible())
			{
				if (DisplayType.YesNo == gridField[i].getDisplayType() || DisplayType.Image == gridField[i].getDisplayType())
				{
					divStyle += "text-align:center; ";
				}

				WEditor editor = getEditorCell(gridField[i], currentValues[i]);
				div.appendChild(editor.getComponent());
				div.setAttribute(GRID_ROW_COLUMNNAME_ATTR, editor.getColumnName());
				editor.getComponent().addEventListener(Events.ON_FOCUS, gridPanel);

				if (editor instanceof WNumberEditor)
				{
					((Table) ((WNumberEditor) editor).getComponent().getFirstChild()).setStyle("float:right;");
				}
				else if (editor instanceof WPAttributeEditor)
				{
					((WPAttributeEditor) editor).getComponent().getButton().addEventListener(Events.ON_FOCUS, gridPanel);
				}
				else if (editor instanceof WSearchEditor)
				{
					((WSearchEditor) editor).getComponent().getButton().addEventListener(Events.ON_FOCUS, gridPanel);
				}
				
				if (!gridField[i].isDisplayed(true))
				{
					editor.setVisible(false);
				}
				editor.setReadWrite(gridField[i].isEditable(true));
			}
			div.setStyle(divStyle);
			div.setAttribute("columnName", gridField[i].getColumnName());
			div.addEventListener(Events.ON_CLICK, rowListener);

			editing = true;
			GridTableListModel model = (GridTableListModel) grid.getModel();
			model.setEditing(true);

			row.appendChild(div);
		}

		if (rowIndex == gridTab.getCurrentRow())
		{
			setCurrentRow(row);
		}
		row.addEventListener(Events.ON_OK, rowListener);

		if (currentDiv == null)
		{
			setCurrentCell(gridTab.getCurrentRow(), 1, KeyEvent.RIGHT);
		}

		log.finest("Render: Ended RowIndex = " + rowIndex);
		Timestamp r2 = new Timestamp(new Date().getTime());
		log.finest("Render: END " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS Z").format(r2) + "\n Time Diff Millis: "
				+ new DecimalFormat("###,###").format(r2.getTime() - r1.getTime()));
	} // render

	private Div currentDiv = null;
	
	public Div getCurrentDiv()
	{
		return currentDiv;
	}

	public void setCurrentCell(int row, int col, int code)
	{
		if (col < 0)
			return;
		while (!isEditable(row,col))
		{
			if (!(code == KeyEvent.RIGHT || code == KeyEvent.LEFT))
				break;
			else if (code == KeyEvent.RIGHT)
				++col;
			else if (code == KeyEvent.LEFT)
				--col;

			if (col < 0)
			{
				setFocusCell();
				return;
			}
		}
		
		int pgIndex = row >= 0 ? row % paging.getPageSize() : 0;
		if (row != currentRowIndex || pgIndex != currentRowIndex)
		{
			if (currentRow != null)
				currentRow.setStyle(null);
			if (grid.getRows().getChildren().size() <= 0)
			{
				currentDiv = null;
				return;
			}
			gridTab.setCurrentRow(row);
			currentRow = ((Row) grid.getRows().getChildren().get(pgIndex));
			currentRowIndex = gridTab.getCurrentRow();
			currentRow.setStyle(CURRENT_ROW_STYLE);
		}
		setFocusToEditor();
		
		if (grid.getCell(pgIndex, col) instanceof Div)
			currentDiv = (Div) grid.getCell(pgIndex, col);
		
		if (currentDiv != null && code != 0)
		{
			setFocusCell();
		}
		else if (currentDiv == null)
		{
			setCurrentCell(row, 0, code);

		}
	}
	
	private boolean isEditable(int row,int col)
	{
		Div div = null;
		
		if (col > getCurrentRow().getChildren().size())
			return true;
		
		if (grid.getCell(row, col) instanceof Div)
			div = (Div) grid.getCell(row, col);
		else
			return true;
		
		if(div == null)
			return true;
		if (div.getChildren().size() <= 0)
			return false;
		
		if (div.getChildren().get(0) instanceof NumberBox && ( !((NumberBox) div.getChildren().get(0)).getDecimalbox().isDisabled() && !((NumberBox) div.getChildren().get(0)).getDecimalbox().isReadonly()))
			return true;
		else if (div.getChildren().get(0) instanceof Checkbox && ((Checkbox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Combobox && ((Combobox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Textbox && (!((Textbox) div.getChildren().get(0)).isDisabled() && !((Textbox) div.getChildren().get(0)).isReadonly()))
			return true;
		else if (div.getChildren().get(0) instanceof Datebox && ((Datebox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof DatetimeBox && ((DatetimeBox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Locationbox && ((Locationbox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Searchbox && (!((Searchbox) div.getChildren().get(0)).getTextbox().isDisabled() && !((Searchbox) div.getChildren().get(0)).getTextbox().isReadonly()))
			return true;
		else if (div.getChildren().get(0) instanceof Button && ((Button) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Combinationbox && ((Combinationbox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof EditorBox && ((EditorBox) div.getChildren().get(0)).isEnabled())
			return true;
		else
			return false;
	}
	
	public void setFocusCell()
	{
		if (currentDiv == null || currentDiv.getChildren().size() <= 0) {
			return;
		}
		
		if (currentDiv.getChildren().get(0) instanceof NumberBox)
		{
			((NumberBox) currentDiv.getChildren().get(0)).focus();
			((NumberBox) currentDiv.getChildren().get(0)).getDecimalbox().select();
		}
		else if (currentDiv.getChildren().get(0) instanceof Checkbox)
			((Checkbox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Combobox)
			((Combobox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Textbox)
		{
			((Textbox) currentDiv.getChildren().get(0)).focus();
			((Textbox) currentDiv.getChildren().get(0)).select();
		}
		else if (currentDiv.getChildren().get(0) instanceof Datebox)
			((Datebox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof DatetimeBox)
			((DatetimeBox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Locationbox)
			((Locationbox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Combinationbox)
		{
			((Combinationbox) currentDiv.getChildren().get(0)).getTextbox().focus();
			((Combinationbox) currentDiv.getChildren().get(0)).getTextbox().select();
		}
		else if (currentDiv.getChildren().get(0) instanceof Searchbox)
		{
			((Searchbox) currentDiv.getChildren().get(0)).getTextbox().focus();
			((Searchbox) currentDiv.getChildren().get(0)).getTextbox().select();
		}
		else if (currentDiv.getChildren().get(0) instanceof Button)
			((Button) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof EditorBox)
			((EditorBox) currentDiv.getChildren().get(0)).focus();
		
	}

	/**
	 * @param component
	 * @return
	 */
	@SuppressWarnings("unused")
	private Input createAnchorInput() {
		Input input = new Input();
		input.setDynamicProperty("type", "text");
		input.setValue("");
		input.setDynamicProperty("readonly", "readonly");
		input.setStyle("border: none; display: none; width: 3px;");
		return input;
	}

	/**
	 * @param row
	 */
	public void setCurrentRow(Row row) {
		if (currentRow != null && currentRow.getParent() != null && currentRow != row) {
			currentRow.setStyle(null);
		}
		currentRow = row;
		currentRow.setStyle(CURRENT_ROW_STYLE);
//		setFocusToEditor();
//
//		if (currentRowIndex == gridTab.getCurrentRow()) {
			if (editing) {
				stopEditing(false);
				editCurrentRow();
			}
//		} else {
//			currentRowIndex = gridTab.getCurrentRow();
//			if (editing) {
//				stopEditing(false);
//			}
//		}
	}

	/**
	 * @return Row
	 */
	public Row getCurrentRow() {
		return currentRow;
	}

	/**
	 * @return current row index ( absolute )
	 */
	public int getCurrentRowIndex() {
		return currentRowIndex;
	}

	/**
	 * Enter edit mode
	 */
	public void editCurrentRow()
	{
		if (currentRow != null && currentRow.getParent() != null && currentRow.isVisible() && grid != null
				&& grid.isVisible() && grid.getParent() != null && grid.getParent().isVisible())
		{
//			int columnCount = gridTab.getTableModel().getColumnCount();
//			GridField[] gridField = gridTab.getFields();
//			org.zkoss.zul.Columns columns = grid.getColumns();
//			int colIndex = -1;
//
//			for (int i = 0; i < columnCount; i++)
//			{
//				if (!gridField[i].isQuickEntry())
//				{
//					continue;
//				}
//				colIndex++;
//
//				if (editors.get(gridField[i]) == null)
//					editors.put(gridField[i], WebEditorFactory.getEditor(gridField[i], true));
//				org.zkoss.zul.Column column = (org.zkoss.zul.Column) columns.getChildren().get(colIndex);
//				if (column.isVisible())
//				{
//					Div div = (Div) currentRow.getChildren().get(colIndex);
//
//					WEditor editor = getEditorCell(gridField[i], currentValues[i], i);
//					div.appendChild(editor.getComponent());
//					div.getFirstChild().setVisible(false);
//					// check context
//					if (!gridField[i].isDisplayed(true))
//					{
//						editor.setVisible(false);
//					}
//					editor.setReadWrite(gridField[i].isEditable(true));
//				}
//			}

			editing = true;

			GridTableListModel model = (GridTableListModel) grid.getModel();
			model.setEditing(true);

		}
	}


	/**
	 * @see RowRendererExt#getControls()
	 */
	public int getControls() {
		return DETACH_ON_RENDER;
	}

	/**
	 * @see RowRendererExt#newCell(Row)
	 */
	public Component newCell(Row row) {
		return null;
	}

	/**
	 * @see RowRendererExt#newRow(Grid)
	 */
	public Row newRow(Grid grid) {
		return null;
	}

	/**
	 * @see RendererCtrl#doCatch(Throwable)
	 */
	public void doCatch(Throwable ex) throws Throwable {
		lookupCache = null;
	}

	/**
	 * @see RendererCtrl#doFinally()
	 */
	public void doFinally() {
		lookupCache = null;
	}

	/**
	 * @see RendererCtrl#doTry()
	 */
	public void doTry() {
		lookupCache = new HashMap<String, Map<Object,String>>();
	}

	/**
	 * set focus to first active editor
	 */
	public void setFocusToEditor() {
		if (currentRow != null && currentRow.getParent() != null) {
			WEditor toFocus = null;
			WEditor firstEditor = null;
			for (WEditor editor : getEditors()) {
				if (editor.isHasFocus() && editor.isVisible() && editor.getComponent().getParent() != null) {
					toFocus = editor;
					break;
				}

				if (editor.isVisible() && editor.getComponent().getParent() != null) {
					if (toFocus == null && editor.isReadWrite()) {
						toFocus = editor;
					}
					if (firstEditor == null)
						firstEditor = editor;
				}
			}
			if (toFocus != null) {
				Component c = toFocus.getComponent();
				if (c instanceof EditorBox) {
					c = ((EditorBox)c).getTextbox();
				}
				Clients.response(new AuFocus(c));
			} else if (firstEditor != null) {
				Component c = firstEditor.getComponent();
				if (c instanceof EditorBox) {
					c = ((EditorBox)c).getTextbox();
				}
				Clients.response(new AuFocus(c));
			}
		}
	}

	/**
	 *
	 * @param gridPanel
	 */
	public void setGridPanel(QuickGridPanel gridPanel) {
		this.gridPanel = gridPanel;
	}

	class RowListener implements EventListener {

		private Grid _grid;

		public RowListener(Grid grid) {
			_grid = grid;
		}

		@Override
		public void onEvent(Event event) throws Exception {
			if (Events.ON_CLICK.equals(event.getName())) {
				Event evt = new Event(Events.ON_CLICK, _grid, event.getTarget());
				Events.sendEvent(_grid, evt);
			}
			else if (Events.ON_DOUBLE_CLICK.equals(event.getName())) {
				Event evt = new Event(Events.ON_DOUBLE_CLICK, _grid, _grid);
				Events.sendEvent(_grid, evt);
			}
			else if (Events.ON_OK.equals(event.getName())) {
				Event evt = new Event(Events.ON_OK, _grid, _grid);
				Events.sendEvent(_grid, evt);
			}
		}
	}

	/**
	 * @return boolean
	 */
	public boolean isEditing() {
		return editing;
	}

	/**
	 * @param windowPanel
	 */
	public void setADWindowPanel(AbstractADWindowPanel windowPanel) {
		this.m_windowPanel = windowPanel;
	}

	public Object[] getCurrentValues() {
		return currentValues;
	}

	public void setCurrentValues(Object[] currentValues) {
		this.currentValues = currentValues;
	}

	@Override
	public void onEvent(Event event) throws Exception
	{
		if (event.getTarget() instanceof Checkbox)
		{
			Executions.getCurrent().setAttribute("quickGridPanel.onSelectRow", Boolean.TRUE);
			Checkbox checkBox = (Checkbox) event.getTarget();
			Events.sendEvent(gridPanel, new Event("onSelectRow", gridPanel, checkBox));
		}
	}
}
