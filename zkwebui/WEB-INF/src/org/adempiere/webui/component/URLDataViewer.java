/**
 * 
 */
package org.adempiere.webui.component;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.compiere.util.DB;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Util;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Iframe;

/**
 * Display data from URL on floating window mode.
 * 
 * @author <a href="mailto:sachin.bhimani89@gmail.com">Sachin Bhimani</a>
 * @since 2016-Aug-05
 * @see WTreeBOM#showDocumentWindow(String)
 */
public class URLDataViewer extends Window implements EventListener
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 5059872219862739061L;

	private Borderlayout		borderLayout		= new Borderlayout();
	private Panel				parameterPanel		= new Panel();
	private Grid				parameterLayout		= GridFactory.newGridLayout();

	private Label				lblDropDown			= new Label();
	private Listbox				fieldSelectionList	= ListboxFactory.newDropdownListbox();
	private Iframe				iFrame				= new Iframe();
	private Vector<Pair>		listData			= new Vector<Pair>();
	private Hbox				hbox;

	public URLDataViewer()
	{
		super();
	}

	private void init()
	{
		North north = new North();
		north.setStyle("border-bottom: 2px solid #000;");
		north.appendChild(parameterPanel);
		parameterPanel.appendChild(parameterLayout);

		parameterLayout.setWidth("100%");
		Rows rows = parameterLayout.newRows();
		Row row = rows.newRow();
		row.appendChild(lblDropDown);
		row.appendChild(fieldSelectionList);
		fieldSelectionList.setWidth("50%");

		hbox = new Hbox();
		hbox.setWidth("100%");
		hbox.setHeight("100%");

		// Content Display Panel
		Center center = new Center();
		center.appendChild(hbox);

		borderLayout.appendChild(north);
		borderLayout.appendChild(center);
		this.appendChild(borderLayout);
	}

	public void show(String LabelTitle, String WindowTitle, Vector<Pair> data)
	{
		init();

		lblDropDown.setText(LabelTitle + " :");
		this.setWidth("650px");
		this.setHeight("550px");
		this.setTitle(WindowTitle);
		this.setTooltip(WindowTitle);
		this.setBorder("normal");
		this.setPosition("left,top");
		this.setClosable(true);
		this.setSizable(true);
		this.setVisible(true);
		this.setShadow(false);
		this.setAttribute(Window.MODE_KEY, Window.MODE_OVERLAPPED);

		listData.clear();
		listData = data;
		for (Pair pair : listData)
		{
			fieldSelectionList.addItem((KeyNamePair) pair.getX());
		}
		fieldSelectionList.setMold("select");
		fieldSelectionList.addEventListener(Events.ON_SELECT, this);
		fieldSelectionList.setSelectedIndex(0);
		setURLDataOnIFrame();

		AEnv.showCenterScreen(this);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.zkoss.zk.ui.event.EventListener#onEvent(org.zkoss.zk.ui.event.Event)
	 */
	@Override
	public void onEvent(Event event) throws Exception
	{
		if (event.getTarget().equals(fieldSelectionList))
		{
			setURLDataOnIFrame();
		}

	}

	/**
	 * @author Sachin
	 */
	public void setURLDataOnIFrame()
	{
		int selectedID = Integer.parseInt(fieldSelectionList.getSelectedItem().getValue().toString());
		for (Pair pair : listData)
		{
			KeyNamePair knp = (KeyNamePair) pair.getX();
			if (knp.getKey() == selectedID)
			{
				if (hbox != null && iFrame != null)
				{
					hbox.removeChild(iFrame);
					iFrame.detach();
				}
				iFrame = new Iframe(pair.getY().toString()); // Get URL
				iFrame.setWidth("100%");
				iFrame.setHeight("100%");
				hbox.appendChild(iFrame);
				break;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.adempiere.webui.component.Window#dispose()
	 */
	@Override
	public void dispose()
	{
		if (iFrame != null)
			iFrame.detach();

		super.dispose();
	}

	/**
	 * Show window dialog
	 * 
	 * @author Sachin
	 * @param LabelTitle - Set title of the drop down label.
	 * @param WindowTitle - Title of the Window
	 * @param data - data of the drop down list
	 */
	public static void showDialog(String LabelTitle, String WindowTitle, Vector<Pair> data)
	{
		URLDataViewer viewer = new URLDataViewer();
		viewer.show(LabelTitle, WindowTitle, data);
	}

	/**
	 * Retrieve value of list items for Drop Down
	 * 
	 * @author Sachin
	 * @param KeyColumnID - Primary column ID
	 * @param NameColumn - Name or value display on the drop down items
	 * @param URLColumn - URL Column Name
	 * @param TableName - Table Name
	 * @param WhereClause - Where condition
	 * @param OrderByClause - Order by clause
	 * @return Data as Vector of Pair
	 * @throws Exception
	 */
	public static Vector<Pair> getSelectedValue(String KeyColumnID, String NameColumn, String URLColumn, String TableName,
			String WhereClause, String OrderByClause)
	{
		StringBuilder error = new StringBuilder();
		if (Util.isEmpty(KeyColumnID, true))
			error.append("Primary column ID is mandatory. \n");
		if (Util.isEmpty(NameColumn, true))
			error.append("Name/Value column is mandatory. \n");
		if (Util.isEmpty(URLColumn, true))
			error.append("URL Column is mandatory. \n");
		if (Util.isEmpty(TableName, true))
			error.append("Table Name is mandatory. \n");
		if (error.length() > 0)
			throw new AdempiereException(error.toString());

		StringBuilder sql = new StringBuilder("SELECT ");
		sql.append(KeyColumnID).append(",");
		sql.append(NameColumn).append(",");
		sql.append(URLColumn);
		sql.append(" FROM ").append(TableName);

		if (!Util.isEmpty(WhereClause, true))
			sql.append(" WHERE ").append(WhereClause);

		if (!Util.isEmpty(OrderByClause, true))
			sql.append(" ORDER BY ").append(OrderByClause);

		Vector<Pair> vector = new Vector<Pair>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				KeyNamePair knPair = new KeyNamePair(rs.getInt(1), rs.getString(2));
				Pair pair = new Pair(knPair, rs.getString(3));
				vector.add(pair);
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException(e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return vector;
	}
}
