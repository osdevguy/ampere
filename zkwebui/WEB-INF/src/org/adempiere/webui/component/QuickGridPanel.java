/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin * This program is free software; you can
 * redistribute it and/or modify it * under the terms version 2 of the GNU
 * General Public License as published * by the Free Software Foundation. This
 * program is distributed in the hope * that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied * warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. * See the GNU General Public License for more
 * details. * You should have received a copy of the GNU General Public License
 * along * with this program; if not, write to the Free Software Foundation,
 * Inc., * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA. *
 *****************************************************************************/
package org.adempiere.webui.component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.table.AbstractTableModel;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.panel.AbstractADWindowPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.SortComparator;
import org.adempiere.webui.window.CustomizeGridPanelDialog;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.GridTable;
import org.compiere.model.MOrderLine;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTabCustomization;
import org.compiere.util.CLogger;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.zkforge.keylistener.Keylistener;
import org.zkoss.zk.au.out.AuEcho;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.au.out.AuScript;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.South;
import org.zkoss.zul.Column;
import org.zkoss.zul.Div;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.event.ZulEvents;

/**
 * Grid view implemented using the Grid component.
 * 
 * @author Low Heng Sin
 */
public class QuickGridPanel extends Borderlayout implements EventListener
{
	private static final int KEYBOARD_KEY_S = 83;
	private static final int KEYBOARD_KEY_C = 67;
	private static final int KEYBOARD_KEY_K = 75;
	private static final int KEYBOARD_KEY_RETURN = 13;

	/**
	 * generated serial version ID
	 */
	private static final long serialVersionUID = -6223449250193363944L;

	private static CLogger			log						= CLogger.getCLogger(QuickGridPanel.class);

	private static final int		MIN_COLUMN_WIDTH		= 100;

	private static final int		MAX_COLUMN_WIDTH		= 300;

	private static final int		MIN_COMBOBOX_WIDTH		= 160;

	private static final int		MIN_NUMERIC_COL_WIDTH	= 130;

	public static final String		CNTRL_KEYS				= "#left#right#up#down#home^s^c#enter^k";
	
	public static final String		GRID_COLUMN_FIELD_ID_ATTR	= "QuickEntry_AD_Field_ID";

	private Grid					listbox					= null;

	private int						pageSize				= 100;

	private GridField[]				gridField;
	
	private AbstractTableModel		tableModel;

	private int						numColumns				= 5;

	private int						windowNo;

	private GridTab					gridTab;

	private boolean					init;

	private GridTableListModel		listModel;

	private Paging					paging;

	private QuickGridRowRenderer	renderer;

	private South					south;
	
	private Center					center;

	private boolean					modeless;

	private String					columnOnClick;

	private AbstractADWindowPanel	windowPanel;

	public static final String		PAGE_SIZE_KEY			= "ZK_PAGING_SIZE";

	public static final String		MODE_LESS_KEY			= "ZK_GRID_EDIT_MODELESS";

	public static final int			SALES_ORDER_LINE_TAB_ID	= 187;

	private Keylistener				keyListener;

	protected Checkbox				selectAll;

	public boolean					isNewLineSaved				= true;

	private boolean					isHasCustomizeData			= false;
	
	private Timer					refreshTimer				= new Timer();
	private boolean					isTimerScheduled			= false;

	private String					formWidth					= "800px";
	private String					formHeight					= "600px";
	
	private Map<Integer, String>	columnWidthMap;
	
	public Button					bOK;

	public QuickGridPanel()
	{
		this(0);
	}

	/**
	 * @param windowNo
	 */
	public QuickGridPanel(int windowNo)
	{
		this.windowNo = windowNo;
		createListbox();
		
		south = new South();
		south.setStyle("border: none; margin:0; padding: 0;");
		this.appendChild(south);
		
		center = new Center();
		center.appendChild(listbox);
		this.appendChild(center);
		
		setId("quickGridPanel");

		// default paging size
		pageSize = MSysConfig.getIntValue(PAGE_SIZE_KEY, 100);

		// default false for better performance
		modeless = MSysConfig.getBooleanValue(MODE_LESS_KEY, false);
		
		addEventListener(CustomizeGridPanelDialog.CUSTOMIZE_GRID, this);
	}

	/**
	 * @param gridTab
	 */
	public void init(GridTab gridTab)
	{
		if (init)
			return;

		this.gridTab = gridTab;
		tableModel = gridTab.getTableModel();

		numColumns = tableModel.getColumnCount();

		setupFields(gridTab);
		setupColumns();
		render();

		updateListIndex();

		this.init = true;
	}

	/**
	 * @return boolean
	 */
	public boolean isInit()
	{
		return init;
	}

	/**
	 * call when tab is activated
	 * 
	 * @param gridTab
	 */
	public void activate(GridTab gridTab)
	{
		if (!isInit())
		{
			init(gridTab);
		}
	}

	/**
	 * refresh after switching from form view
	 * 
	 * @param gridTab
	 */
	public void refresh(GridTab gridTab)
	{
		if (!isInit())
		{
			init = false;
			init(gridTab);
		}
		else
		{
			listbox.setModel(listModel);
			updateListIndex();
		}
	}

	/**
	 * Update current row from model
	 */
	public void updateListIndex()
	{
		if (gridTab == null || !gridTab.isOpen())
			return;

		int rowIndex = gridTab.getCurrentRow();
		if (pageSize > 0)
		{
			if (paging.getTotalSize() != gridTab.getRowCount())
				paging.setTotalSize(gridTab.getRowCount());
			int pgIndex = rowIndex >= 0 ? rowIndex % pageSize : 0;
			int pgNo = rowIndex >= 0 ? (rowIndex - pgIndex) / pageSize : 0;
			if (listModel.getPage() != pgNo)
			{
				listModel.setPage(pgNo);
				if (renderer.isEditing())
				{
					renderer.stopEditing(false);
				}
			}
			else if (rowIndex == renderer.getCurrentRowIndex())
			{
				if (modeless && !renderer.isEditing())
					Events.echoEvent("onPostSelectedRowChanged", this, null);
				return;
			}
			else
			{
				if (renderer.isEditing())
				{
					renderer.stopEditing(false);
					if (((renderer.getCurrentRowIndex() - pgIndex) / pageSize) == pgNo)
					{
						listModel.updateComponent(renderer.getCurrentRowIndex() % pageSize);
					}
				}
			}
			if (paging.getActivePage() != pgNo)
			{
				paging.setActivePage(pgNo);
			}
			if (rowIndex >= 0 && pgIndex >= 0)
			{
				Events.echoEvent("onPostSelectedRowChanged", this, null);
			}
		}
		else
		{
			if (rowIndex >= 0)
			{
				Events.echoEvent("onPostSelectedRowChanged", this, null);
			}
		}
	}

	public void clear()
	{
		this.getChildren().clear();
	}

	/**
	 * toggle visibility
	 * 
	 * @param bool
	 */
	public void showGrid(boolean bool)
	{
		if (bool)
			this.setVisible(true);
		else
			this.setVisible(false);
	}

	private void setupFields(GridTab gridTab)
	{
		this.gridTab = gridTab;

		tableModel = gridTab.getTableModel();
		columnWidthMap = new HashMap<Integer, String>();
		GridField[] tmpFields = ((GridTable) tableModel).getFields();

		// Retrieve custom column width as per tab and user wise having
		// QuickEntry.
		MTabCustomization tabCustomization = MTabCustomization.get(Env.getCtx(), Env.getAD_User_ID(Env.getCtx()),
				gridTab.getAD_Tab_ID(), true, null);
		if (tabCustomization == null || Util.isEmpty(tabCustomization.getcustom(), true))
		{
			tabCustomization = MTabCustomization.get(Env.getCtx(), MTabCustomization.SUPERUSER, gridTab.getAD_Tab_ID(), true,
					null);
		}

		isHasCustomizeData = tabCustomization != null && !Util.isEmpty(tabCustomization.getcustom(), true);

		if (isHasCustomizeData)
		{
			String custom = tabCustomization.getcustom().trim();
			String[] customComponent = custom.split(";");
			String[] fieldIds = customComponent[0].split("[,]");
			List<GridField> fieldList = new ArrayList<GridField>();
			if (fieldIds[0].contains("px")) {
				ArrayList<GridField> gridFieldList = new ArrayList<GridField>();

				for (GridField field : tmpFields)
				{
					if (field.isQuickEntry())
					{
						gridFieldList.add(field);
					}
				}

				Collections.sort(gridFieldList, new Comparator<GridField>() {
					@Override
					public int compare(GridField o1, GridField o2)
					{
						return o1.getSeqNoGrid() - o2.getSeqNoGrid();
					}
				});

			} else {
				for (String fieldIdStr : fieldIds)
				{
					fieldIdStr = fieldIdStr.trim();
					if (fieldIdStr.length() == 0)
						continue;
					int AD_Field_ID = Integer.parseInt(fieldIdStr);
					for (GridField gridField : tmpFields)
					{
						if (gridField.getAD_Field_ID() == AD_Field_ID)
						{
							// add field in tabCustomization list to display list
							// event this field have showInGrid = false
							if (gridField.isQuickEntry())
								fieldList.add(gridField);
							break;
						}
					}
				}
				
			}
			

			gridField = fieldList.toArray(new GridField[0]);

			if (customComponent.length >= 2)
			{
				String[] widths = customComponent[1].split("[,]");
				for (int i = 0; i < gridField.length && i < widths.length; i++)
				{
					columnWidthMap.put(gridField[i].getAD_Field_ID(), widths[i]);
				}
			}
			
			// if any new column added for quick form.
			for (GridField field : tmpFields)
			{
				if (field.isQuickEntry())
				{
					boolean isFieldAvailable = false;
					for (GridField gField : gridField)
					{
						if (gField.getAD_Field_ID() == field.getAD_Field_ID())
						{
							isFieldAvailable = true;
							break;
						}
					}
					if (!isFieldAvailable)
						fieldList.add(field);
				}
			}
			
			gridField = fieldList.toArray(new GridField[0]);
			
			if (customComponent.length >= 3) {
				String[] dimension = customComponent[2].split("[,]");
				formWidth = dimension[0];
				formHeight = dimension[1];
				
			}
			
			if (isTimerScheduled)
			{
				refreshTimer.cancel();
				isTimerScheduled = false;
			}
			if (tabCustomization.isRefreshGridTimer())
			{
				int period = tabCustomization.getRefreshInterval() * 1000;
				if (period < 5000)
					period = 5000;
				
				refreshTimer = new Timer();
				refreshTimer.schedule(getRefreshOnIntervalTask(), period, period);
				isTimerScheduled = true;
			}
		}
		else
		{
			ArrayList<GridField> gridFieldList = new ArrayList<GridField>();

			for (GridField field : tmpFields)
			{
				if (field.isQuickEntry())
				{
					gridFieldList.add(field);
				}
			}

			Collections.sort(gridFieldList, new Comparator<GridField>() {
				@Override
				public int compare(GridField o1, GridField o2)
				{
					return o1.getSeqNoGrid() - o2.getSeqNoGrid();
				}
			});

			gridField = new GridField[gridFieldList.size()];
			gridFieldList.toArray(gridField);
		}

		numColumns = gridField.length;
	}
	
	private void setupColumns()
	{
		if (init)
			return;

		Columns columns = new Columns();
		listbox.appendChild(columns);
		columns.setSizable(true);
		columns.setMenupopup("auto");
		columns.setColumnsgroup(false);

		org.zkoss.zul.Column selection = new Column();
		selection.setSort("none");
		selection.setStyle("border: none; height:100%; text-align:center;");
		selection.setWidth("40px");

		selectAll = new Checkbox();
		selectAll.setId("selectAll");
		selectAll.addEventListener(Events.ON_CHECK, this);
		selection.appendChild(selectAll);
		columns.appendChild(selection);

		for (int i = 0; i < numColumns; i++)
		{
			// customize + QuickEntry check for some of field might be
			// remove for quick form.
			if ((isHasCustomizeData && gridField[i].isQuickEntry()) || gridField[i].isQuickEntry())
			{
				org.zkoss.zul.Column column = new Column();
				column.setSortAscending(new SortComparator(i, true, Env.getLanguage(Env.getCtx())));
				column.setSortDescending(new SortComparator(i, false, Env.getLanguage(Env.getCtx())));
				column.setLabel(gridField[i].getHeader());
				column.setAttribute(GRID_COLUMN_FIELD_ID_ATTR, gridField[i].getAD_Field_ID());

				if (columnWidthMap != null && columnWidthMap.get(gridField[i].getAD_Field_ID()) != null
						&& !columnWidthMap.get(gridField[i].getAD_Field_ID()).equals(""))
				{
					column.setWidth(columnWidthMap.get(gridField[i].getAD_Field_ID()));
				}
				else
				{
					int l = DisplayType.isNumeric(gridField[i].getDisplayType()) ? 120 : gridField[i].getDisplayLength() * 9;
					if (gridField[i].getHeader().length() * 9 > l)
						l = gridField[i].getHeader().length() * 9;
					if (l > MAX_COLUMN_WIDTH)
						l = MAX_COLUMN_WIDTH;
					else if (l < MIN_COLUMN_WIDTH)
						l = MIN_COLUMN_WIDTH;
					if (gridField[i].getDisplayType() == DisplayType.Table
							|| gridField[i].getDisplayType() == DisplayType.TableDir)
					{
						if (l < MIN_COMBOBOX_WIDTH)
							l = MIN_COMBOBOX_WIDTH;
					}
					else if (DisplayType.isNumeric(gridField[i].getDisplayType()))
					{
						if (l < MIN_NUMERIC_COL_WIDTH)
							l = MIN_NUMERIC_COL_WIDTH;
					}
					column.setWidth(Integer.toString(l) + "px");
				}
				
				if (DisplayType.isNumeric(gridField[i].getDisplayType()))
				{
					column.setStyle("text-align: right");
				}
				else if (DisplayType.YesNo == gridField[i].getDisplayType())
				{
					column.setStyle("text-align: center");
				}
				columns.appendChild(column);
			}
		}
	}

	private void render()
	{
		LayoutUtils.addSclass("adtab-grid-panel", this);

		listbox.setVflex(true);
		listbox.setFixedLayout(true);

		if (keyListener != null)
			keyListener.detach();
		keyListener = new Keylistener();
		if (windowPanel != null)
			windowPanel.getStatusBar().appendChild(keyListener);
		keyListener.setCtrlKeys(CNTRL_KEYS);
		keyListener.addEventListener(Events.ON_CTRL_KEY, this);

		updateModel();

		if (pageSize > 0)
		{
			paging = new Paging();
			paging.setPageSize(pageSize);
			paging.setTotalSize(tableModel.getRowCount());
			paging.setDetailed(true);
			south.appendChild(paging);
			paging.addEventListener(ZulEvents.ON_PAGING, this);
			renderer.setPaging(paging);
		}
		else
		{
			south.setVisible(false);
		}

	}

	private void updateModel()
	{
		if (listModel != null)
			((GridTable)tableModel).removeTableModelListener(listModel);
		
		listModel = new GridTableListModel((GridTable) tableModel, windowNo);
		listModel.setPageSize(pageSize);
		if (renderer != null && renderer.isEditing())
			renderer.stopEditing(false);
		renderer = new QuickGridRowRenderer(gridTab, windowNo);
		renderer.setGridPanel(this);
		renderer.setADWindowPanel(windowPanel);

		addEventListener("onSelectRow", this);

		listbox.setModel(listModel);
		if (listbox.getRows() == null) 
			listbox.appendChild(new Rows());
		listbox.setRowRenderer(renderer);
	}

	/**
	 * deactivate panel
	 */
	public void deactivate()
	{
		if (renderer != null && renderer.isEditing())
			renderer.stopEditing(true);
	}

	public void onEvent(Event event) throws Exception
	{
		if (event.getTarget() == listbox && Events.ON_CLICK.equals(event.getName()))
		{
			Object data = event.getData();
			org.zkoss.zul.Row row = null;
			String columnName = null;
			if (data != null && data instanceof Component)
			{
				if (data instanceof org.zkoss.zul.Row)
					row = (org.zkoss.zul.Row) data;
				else
				{
					AbstractComponent cmp = (AbstractComponent) data;
					if (cmp.getParent() instanceof org.zkoss.zul.Row)
					{
						row = (Row) cmp.getParent();
						columnName = (String) cmp.getAttribute("columnName");
					}
				}
			}
			if (row != null)
			{
				// click on selected row to enter edit mode
				if (row == renderer.getCurrentRow())
				{
					if (!renderer.isEditing())
					{
						renderer.editCurrentRow();
						if (columnName != null && columnName.trim().length() > 0)
							setFocusToField(columnName);
						else
							renderer.setFocusToEditor();
					}
				}
				else
				{
					int index = listbox.getRows().getChildren().indexOf(row);
					if (index >= 0)
					{
						columnOnClick = columnName;
						onSelectedRowChange(index);
					}
				}
			}
		}
		else if (event.getTarget() == paging)
		{
			int pgNo = paging.getActivePage();
			if (pgNo != listModel.getPage())
			{
				listModel.setPage(pgNo);
				onSelectedRowChange(0);
			}
		}
		else if (event.getName().equals(Events.ON_CTRL_KEY) && event.getTarget() == keyListener
				&& this == windowPanel.getCurrQuickGridPanel())
		{
			Timestamp t1 = new Timestamp(new Date().getTime()); 
			log.finest("OnEvent = ON_CTRL_KEY START " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS Z").format(t1));
			
			KeyEvent keyEvent = (KeyEvent) event;
			int code = keyEvent.getKeyCode();
			boolean isAlt = keyEvent.isAltKey();
			boolean isCtrl = keyEvent.isCtrlKey();
			boolean isShift = keyEvent.isShiftKey();

			int row = renderer.getCurrentRowIndex();
			int col = renderer.getCurrentRow().getChildren().indexOf(renderer.getCurrentDiv());
			int totalRow = gridTab.getRowCount();
			
			log.finest("PanelEvent Initial Row=" + row + " Col=" + col);

			if (code == KEYBOARD_KEY_RETURN)
			{
				if (gridTab.getAD_Tab_ID() == SALES_ORDER_LINE_TAB_ID && renderer.getCurrentDiv() != null
						&& renderer.getCurrentDiv().getAttribute(QuickGridRowRenderer.GRID_ROW_COLUMNNAME_ATTR)
								.equals(MOrderLine.COLUMNNAME_QtyEntered))
				{
					code = KeyEvent.DOWN;
				}
			}
			if (code == KEYBOARD_KEY_C && isCtrl && !isAlt && !isShift)
			{
				Div div = renderer.getCurrentDiv();
				if (div != null && div.getChildren().get(0) instanceof NumberBox
						&& (!((NumberBox) div.getChildren().get(0)).getDecimalbox().isDisabled()
								&& !((NumberBox) div.getChildren().get(0)).getDecimalbox().isReadonly()))
				{
					NumberBox nbox = (NumberBox) div.getChildren().get(0);
					nbox.setEnabled(true);
					nbox.getPopupMenu().open(nbox, "after_end");
					for (Object vBoxObj : nbox.getPopupMenu().getChildren())
					{
						if (vBoxObj instanceof Vbox)
							for (Object obj : ((Vbox) vBoxObj).getChildren())
								if (obj instanceof Textbox)
									((Textbox) obj).focus();
					}
				}
			}
			else if (code == KEYBOARD_KEY_S && isCtrl && !isAlt && !isShift)
			{
				if (!save(code, row, col))
					return;
			}
			else if (code == KEYBOARD_KEY_K && isCtrl && !isAlt && !isShift)
			{
				Events.echoEvent(Events.ON_CLICK, bOK, null);
			}
			else
			{
				// save data if row changes is made.
				if (code == KeyEvent.DOWN || code == KeyEvent.UP || code == KeyEvent.HOME || code == KeyEvent.END)
				{
					ArrayList<Integer> i = gridTab.getMTable().getRowChanged();
					if (i.contains(row))
					{
						if (!save(code, row, col))
							return;
					}
				}

				// if fire event on last row then it will create new record
				// line.
				if (code == KeyEvent.DOWN && !isCtrl && !isAlt && !isShift)
				{
					row += 1;
					if (row % paging.getPageSize() == 0)
					{
						listModel.setPage(paging.getActivePage() + 1);
						paging.setActivePage(paging.getActivePage() + 1);
						updateModelIndex(0);
						row = 0;
					}

					int currentRow = (paging.getActivePage() * paging.getPageSize()) + row % paging.getPageSize();
					if (currentRow == totalRow && isNewLineSaved)
					{
						createNewLine();
//						updateListIndex();
						refresh(gridTab);
						return;
					}
				}
				else if (code == KeyEvent.LEFT && !isCtrl && !isAlt && !isShift)
				{
					col -= 1;
				}
				else if (code == KeyEvent.RIGHT && !isCtrl && !isAlt && !isShift)
				{
					col += 1;
				}
				else if (code == KeyEvent.UP && !isCtrl && !isAlt && !isShift)
				{
					row -= 1;
					if (paging.getActivePage() > 0 && (row + 1) % paging.getPageSize() == 0)
					{
						listModel.setPage(paging.getActivePage() - 1);
						paging.setActivePage(paging.getActivePage() - 1);
						updateModelIndex(paging.getPageSize() - 1);
						row = paging.getPageSize() - 1;
					}

					if (row < 0)
					{
						row = 0;
					}
				}
				else if (code == KeyEvent.HOME)
				{
					row = 0;
				}
				else
				{
					renderer.setCurrentCell(row, col, code);
					return;
				}

				if (row < 0 || row >= gridTab.getTableModel().getRowCount() || col < 0
						|| col >= gridTab.getTableModel().getColumnCount())
				{
					renderer.setFocusCell();
					return;
				}
				
				Component source = listbox.getCell(row, col);
				if (source == null)
				{
					listbox.renderAll();
					source = listbox.getCell(row, col);
				}
				
				while (source != null && !(source.getClass() == Div.class))
				{
					source = source.getParent();
				}
				
				renderer.setCurrentCell(row, col, code);
			}
			
			log.finest("PanelEvent New row/col Row=" + row + " Col=" + col);
			Timestamp t2 = new Timestamp(new Date().getTime());
			log.finest("OnEvent = ON_CTRL_KEY END " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS Z").format(t2)
					+ "\n Time Diff Millis: " + new DecimalFormat("###,###").format(t2.getTime() - t1.getTime()));
		}
		else if (event.getName().equals(Events.ON_FOCUS))
		{
			Component source = event.getTarget();
			while (source != null && !(source.getClass() == Div.class))
			{
				source = source.getParent();
			}
			if (renderer.getCurrentDiv() != null && !renderer.getCurrentDiv().equals(source))
			{
				int row = renderer.getCurrentRowIndex();
				int col = renderer.getCurrentRow().getChildren().indexOf(renderer.getCurrentDiv());

				setFocusOnDiv(source);

				int rowChange = renderer.getCurrentRowIndex();

				if (row != rowChange)
				{
					ArrayList<Integer> rows = gridTab.getMTable().getRowChanged();
					if (rows.contains(row))
					{
						if (!save(KeyEvent.RIGHT, row, col))
							return;
					}
				}
			}
		}
		else if (event.getTarget() == selectAll)
		{
			allRowsSelection(selectAll.isChecked());
		}
		else if (event.getName().equals("onSelectRow"))
		{
			Checkbox checkbox = (Checkbox) event.getData();
			int rowIndex = (Integer) checkbox.getAttribute(QuickGridRowRenderer.GRID_ROW_INDEX_ATTR);
			if (checkbox.isChecked())
			{
				gridTab.addToSelection(rowIndex);
				if (!selectAll.isChecked() && isAllSelected())
				{
					selectAll.setChecked(true);
				}
			}
			else
			{
				gridTab.removeFromSelection(rowIndex);
				if (selectAll.isChecked())
					selectAll.setChecked(false);
			}
		}
		else if (event.getName().equals(CustomizeGridPanelDialog.CUSTOMIZE_GRID))
		{
			reInit();
		}
	}

	/**
	 * @param code
	 * @param row
	 * @param col
	 */
	public boolean save(int code, int row, int col)
	{
		boolean isSave = isNecessaryDataFill(row, true);

		if (isSave)
		{
			isSave = dataSave(code);
		}

		if (!isSave)
		{
			renderer.setCurrentCell(row, col, code);
		}
		return isSave;
	}

	/**
	 * Fill with mandatory data
	 * 
	 * @param row
	 * @param isShowError
	 * @return boolean
	 */
	public boolean isNecessaryDataFill(int row, boolean isShowError)
	{
		if (gridTab.getAD_Table_ID() == MOrderLine.Table_ID && gridTab.getAD_Tab_ID() == SALES_ORDER_LINE_TAB_ID)
		{
			Object object = gridTab.getValue(row, MOrderLine.COLUMNNAME_M_Product_ID);
			if (object == null)
			{
				if (isShowError)
				{
					setStatusLine("Error - Fill mandatory value of Product", true, true);
				}
				return false;
			}

			object = gridTab.getValue(row, MOrderLine.COLUMNNAME_QtyEntered);
			if (object == null || ((BigDecimal) object).compareTo(Env.ZERO) == 0)
			{
				if (isShowError)
				{
					setStatusLine("Error - Quantity should not empty/zero", true, true);
				}
				return false;
			}
		}

		return true;
	} // isNecessaryDataFill

	public void setStatusLine(String text, boolean error, boolean showPopup)
	{
		log.finest(text);
		windowPanel.getStatusBar().setStatusLineFromQuickEntry(text, error, showPopup);
	}

	@SuppressWarnings("unchecked")
	protected boolean isAllSelected()
	{
		org.zkoss.zul.Rows rows = listbox.getRows();
		List<Component> childs = rows.getChildren();
		boolean all = false;
		for (Component comp : childs)
		{
			org.zkoss.zul.Row row = (org.zkoss.zul.Row) comp;
			Component firstChild = row.getFirstChild();
			if (firstChild instanceof Div)
			{
				firstChild = firstChild.getFirstChild();
			}
			if (firstChild instanceof Checkbox)
			{
				Checkbox checkbox = (Checkbox) firstChild;
				if (!checkbox.isChecked())
					return false;
				else
					all = true;
			}
		}
		return all;
	}

	@SuppressWarnings("unchecked")
	protected void allRowsSelection(boolean flag)
	{
		org.zkoss.zul.Rows rows = listbox.getRows();
		List<Component> childs = rows.getChildren();
		for (Component comp : childs)
		{
			org.zkoss.zul.Row row = (org.zkoss.zul.Row) comp;
			Component firstChild = row.getFirstChild();
			if (firstChild instanceof Div)
			{
				firstChild = firstChild.getFirstChild();
			}
			if (firstChild instanceof Checkbox)
			{
				Checkbox checkbox = (Checkbox) firstChild;
				checkbox.setChecked(flag);
				int rowIndex = (Integer) checkbox.getAttribute(QuickGridRowRenderer.GRID_ROW_INDEX_ATTR);
				if (flag)
					gridTab.addToSelection(rowIndex);
				else
					gridTab.removeFromSelection(rowIndex);
			}
		}
	}

	/**
	 * @param code
	 */
	public boolean dataSave(int code)
	{
		boolean isSave = false;
		isSave = gridTab.dataSave(true);

		if (isSave)
		{
			isNewLineSaved = true;
		}

		int row = renderer.getCurrentRowIndex();
		int col = renderer.getCurrentRow().getChildren().indexOf(renderer.getCurrentDiv());
		if (code != KeyEvent.DOWN && code != KeyEvent.UP)
			renderer.setCurrentCell(row, col, code);

		return isSave;
	}

	public void createNewLine()
	{
		isNewLineSaved = false;
		gridTab.dataNew(false);
	}

	private void setFocusOnDiv(Component source)
	{
		int rowCount = gridTab.getTableModel().getRowCount();
		int colCount = renderer.getCurrentRow().getChildren().size();
		for (int i = 0; i < rowCount; i++)
		{
			for (int j = 0; j < colCount; j++)
			{
				if (listbox.getCell(i, j) != null && listbox.getCell(i, j).equals(source))
				{
					renderer.setCurrentCell(i, j, 0);
					return;
				}
			}
		}
	}

	private void onSelectedRowChange(int index)
	{
		if (updateModelIndex(index))
		{
			updateListIndex();
		}
	}

	/**
	 * Event after the current selected row change
	 */
	public void onPostSelectedRowChanged()
	{
		if (listbox.getRows().getChildren().isEmpty())
			return;

		int rowIndex = gridTab.isOpen() ? gridTab.getCurrentRow() : -1;
		if (rowIndex >= 0 && pageSize > 0)
		{
			int pgIndex = rowIndex >= 0 ? rowIndex % pageSize : 0;
			org.zkoss.zul.Row row = (org.zkoss.zul.Row) listbox.getRows().getChildren().get(pgIndex);
			if (!isRowRendered(row, pgIndex))
			{
				listbox.renderRow(row);
			}
			else
			{
				Row old = renderer.getCurrentRow();
				int oldIndex = renderer.getCurrentRowIndex();
				renderer.setCurrentRow(row);
				if (old != null && old != row && oldIndex >= 0 && oldIndex != gridTab.getCurrentRow())
				{
					listModel.updateComponent(oldIndex % pageSize);
				}
			}
			if (modeless && !renderer.isEditing())
			{
				renderer.editCurrentRow();
				if (columnOnClick != null && columnOnClick.trim().length() > 0)
				{
					setFocusToField(columnOnClick);
					columnOnClick = null;
				}
				else
				{
					renderer.setFocusToEditor();
				}
			}
			else
			{
				focusToRow(row);
				if (gridTab.getRecord_ID() <= 0)
				{
					renderer.setCurrentCell(rowIndex, 1, KeyEvent.RIGHT);
				}
			}
		}
		else if (rowIndex >= 0)
		{
			org.zkoss.zul.Row row = (org.zkoss.zul.Row) listbox.getRows().getChildren().get(rowIndex);
			if (!isRowRendered(row, rowIndex))
			{
				listbox.renderRow(row);
			}
			else
			{
				Row old = renderer.getCurrentRow();
				int oldIndex = renderer.getCurrentRowIndex();
				renderer.setCurrentRow(row);
				if (old != null && old != row && oldIndex >= 0 && oldIndex != gridTab.getCurrentRow())
				{
					listModel.updateComponent(oldIndex);
				}
			}
			if (modeless && !renderer.isEditing())
			{
				renderer.editCurrentRow();
				if (columnOnClick != null && columnOnClick.trim().length() > 0)
				{
					setFocusToField(columnOnClick);
					columnOnClick = null;
				}
				else
				{
					renderer.setFocusToEditor();
				}
			}
			else
			{
				focusToRow(row);
			}
		}
	}

	/**
	 * scroll grid to the current focus row
	 */
	public void scrollToCurrentRow()
	{
		onPostSelectedRowChanged();
	}

	private void focusToRow(org.zkoss.zul.Row row)
	{
		if (renderer.isEditing())
		{
			if (columnOnClick != null && columnOnClick.trim().length() > 0)
			{
				setFocusToField(columnOnClick);
				columnOnClick = null;
			}
			else
			{
				renderer.setFocusToEditor();
			}
		}
		else
		{
			Component cmp = null;
			List<?> childs = row.getChildren();
			for (Object o : childs)
			{
				Component c = (Component) o;
				if (!c.isVisible())
					continue;
				c = c.getFirstChild();
				if (c == null)
					continue;
				if (c.getNextSibling() != null)
				{
					cmp = c.getNextSibling();
					break;
				}
			}
			if (cmp != null)
				Clients.response(new AuScript(null, "scrollToRow('" + cmp.getUuid() + "');"));

			if (columnOnClick != null && columnOnClick.trim().length() > 0)
			{
				List<?> list = row.getChildren();
				for (Object element : list)
				{
					if (element instanceof Div)
					{
						Div div = (Div) element;
						if (columnOnClick.equals(div.getAttribute("columnName")))
						{
							cmp = div.getFirstChild().getNextSibling();
							if (cmp != null)
								Clients.response(new AuScript(null, "scrollToRow('" + cmp.getUuid() + "');"));
							break;
						}
					}
				}
				columnOnClick = null;
			}
		}
	}

	private boolean isRowRendered(org.zkoss.zul.Row row, int index)
	{
		if (row.getChildren().size() == 0)
		{
			return false;
		}
		else if (row.getChildren().size() == 1)
		{
			if (!(row.getChildren().get(0) instanceof Div))
			{
				return false;
			}
		}
		return true;
	}

	private boolean updateModelIndex(int rowIndex)
	{
		if (pageSize > 0)
		{
			int start = listModel.getPage() * listModel.getPageSize();
			rowIndex = start + rowIndex;
		}

		if (gridTab.getCurrentRow() != rowIndex)
		{
			gridTab.navigate(rowIndex);
			return true;
		}
		return false;
	}

	/**
	 * @return Grid
	 */
	public Grid getListbox()
	{
		return listbox;
	}

	/**
	 * Validate display properties of fields of current row
	 * 
	 * @param col
	 */
	public void dynamicDisplay(int col)
	{
		if (gridTab == null || !gridTab.isOpen())
		{
			return;
		}

		// Selective
		if (col > 0)
		{
			GridField changedField = gridTab.getField(col);
			String columnName = changedField.getColumnName();
			ArrayList<?> dependants = gridTab.getDependantFields(columnName);
			if (dependants.size() == 0 && changedField.getCallout().length() > 0)
			{
				return;
			}
		}

		boolean noData = gridTab.getRowCount() == 0;
		List<WEditor> list = renderer.getEditors();
		for (WEditor comp : list)
		{
			GridField mField = comp.getGridField();
			if (mField != null)
			{
				if (noData)
				{
					comp.setReadWrite(false);
				}
				else
				{
					comp.dynamicDisplay();
					boolean rw = mField.isEditable(true); // r/w - check Context
					comp.setReadWrite(rw);
				}

				comp.setVisible(mField.isDisplayed(true));
			}
		} // all components
	}

	/**
	 * @param windowNo
	 */
	public void setWindowNo(int windowNo)
	{
		this.windowNo = windowNo;
	}

	@Override
	public void focus()
	{
		if (renderer != null && renderer.isEditing())
		{
			renderer.setFocusToEditor();
		}
	}

	/**
	 * Handle enter key event
	 */
	public boolean onEnterKey()
	{
		if (!modeless && renderer != null && !renderer.isEditing())
		{
			renderer.editCurrentRow();
			renderer.setFocusToEditor();
			return true;
		}
		return false;
	}

	/**
	 * @param columnName
	 */
	public void setFocusToField(String columnName)
	{
		boolean found = false;
		for (WEditor editor : renderer.getEditors())
		{
			if (found)
				editor.setHasFocus(false);
			else if (columnName.equals(editor.getColumnName()))
			{
				editor.setHasFocus(true);
				Clients.response(new AuFocus(editor.getComponent()));
				found = true;
			}
		}
	}

	/**
	 * @param winPanel
	 */
	public void setADWindowPanel(AbstractADWindowPanel winPanel)
	{
		windowPanel = winPanel;
		if (renderer != null)
			renderer.setADWindowPanel(windowPanel);
	}

	public QuickGridRowRenderer getRenderer()
	{
		return renderer;
	}

	public void dispose()
	{
		if (isTimerScheduled)
		{
			refreshTimer.cancel();
			isTimerScheduled = false;
		}
		refreshTimer = null;

		if (keyListener != null)
		{
			keyListener.detach();
			keyListener = null;
		}

		removeEventListener(CustomizeGridPanelDialog.CUSTOMIZE_GRID, this);
	}

	public boolean isNewLineSaved()
	{
		return isNewLineSaved;
	}

	public void setNewLineSaved(boolean isNewLineSaved)
	{
		this.isNewLineSaved = isNewLineSaved;
	}
	
	public GridField[] getGridField()
	{
		return gridField;
	}
	
	protected void createListbox()
	{
		listbox = new Grid();
		listbox.setOddRowSclass(null);
		listbox.setVflex(true);
	} // createListbox

	/**
	 * Re-initialize after completing customization of grid.
	 * 
	 * @author Sachin
	 */
	public void reInit()
	{
		listbox.getChildren().clear();
		listbox.detach();

		if (paging != null)
		{
			south.removeChild(paging);
			paging.detach();
			paging = null;
		}

		// default paging size
		pageSize = MSysConfig.getIntValue(PAGE_SIZE_KEY, 100);

		renderer = null;
		init = false;

		Grid tmp = listbox;
		createListbox();
		tmp.copyEventListeners(listbox);
		center.appendChild(listbox);

		refresh(gridTab);
		scrollToCurrentRow();
	} // reInit
	
	public String getFormWidth() {
		return formWidth;
	}

	public String getFormHeight() {
		return formHeight;
	}

	public TimerTask getRefreshOnIntervalTask()
	{
		return new TimerTask() {
			public void run()
			{
				if (isNewLineSaved)
				{
					Desktop m_desktop = QuickGridPanel.this.getDesktop();

					if (!m_desktop.isServerPushEnabled())
						m_desktop.enableServerPush(true);

					SessionManager.activateDesktop(m_desktop);

					int rowIdx = getRenderer().getCurrentRowIndex();
					int colIdx = getRenderer().getCurrentRow().getChildren().indexOf(getRenderer().getCurrentDiv());

					gridTab.dataRefreshAll(false, false);

					// echoing response event after current thread execution
					// completion to set focus on the cell.
					Clients.response(new AuEcho(QuickGridPanel.this, "setFocusOnCell", new String(rowIdx + ";" + colIdx)));

					SessionManager.releaseDesktop(m_desktop);
				}
			}
		};
	}

	/**
	 * @author Sachin
	 * @param event
	 */
	public void setFocusOnCell(Event event)
	{
		String data = (String) event.getData();
		if (data != null)
		{
			String[] index = data.split(";");

			int rowIndex = Integer.parseInt(index[0]);
			int colIndex = Integer.parseInt(index[1]);

			int pgIndex = rowIndex >= 0 ? rowIndex % pageSize : 0;
			Row row = (Row) listbox.getRows().getChildren().get(pgIndex);
			renderer.setCurrentRow(row);
			scrollToCurrentRow();
			renderer.setCurrentCell(rowIndex, colIndex, KeyEvent.RIGHT);
		}
	}
}
