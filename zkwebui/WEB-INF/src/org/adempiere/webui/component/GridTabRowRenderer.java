/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin                                            *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.component;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.editor.WButtonEditor;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WEditorPopupMenu;
import org.adempiere.webui.editor.WNumberEditor;
import org.adempiere.webui.editor.WebEditorFactory;
import org.adempiere.webui.event.ContextMenuListener;
import org.adempiere.webui.panel.AbstractADWindowPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.GridTabDataBinder;
import org.adempiere.webui.window.ADWindow;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.GridWindow;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.NamePair;
import org.zkoss.xml.XMLs;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Paging;
import org.zkoss.zul.RendererCtrl;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;
import org.zkoss.zhtml.Input;
import org.zkoss.zhtml.Label;
import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Text;

/**
 * Row renderer for GridTab grid.
 * @author hengsin
 * 
 * @author Teo Sarca, teo.sarca@gmail.com
 * 		<li>BF [ 2996608 ] GridPanel is not displaying time
 * 			https://sourceforge.net/tracker/?func=detail&aid=2996608&group_id=176962&atid=955896
 */
public class GridTabRowRenderer implements RowRenderer, RowRendererExt, RendererCtrl {

	private static final String CURRENT_ROW_STYLE = "border-top: 2px solid #6f97d2; border-bottom: 2px solid #6f97d2";
	private static final int MAX_TEXT_LENGTH = 60;
	private GridTab gridTab;
	private int windowNo;
	private GridTabDataBinder dataBinder;
	private Map<GridField, WEditor> editors = new LinkedHashMap<GridField, WEditor>();
	private Paging paging;

	private Map<String, Map<Object, String>> lookupCache = null;
	private RowListener rowListener;

	private Grid grid = null;
	private GridPanel gridPanel = null;
	private Row currentRow;
	private Object[] currentValues;
	private boolean editing = false;
	private int currentRowIndex = -1;
	private AbstractADWindowPanel m_windowPanel;
	GridWindow gridWindow = null;
	
	/**
	 * Flag detect this view has customized column or not value is set at
	 * {@link #render(Row, Object[], int)}
	 */
	private boolean isGridViewCustomized = false;

	/**
	 *
	 * @param gridTab
	 * @param windowNo
	 */
	public GridTabRowRenderer(GridTab gridTab, int windowNo) {
		this.gridTab = gridTab;
		this.windowNo = windowNo;
		this.dataBinder = new GridTabDataBinder(gridTab);
	}

	private WEditor getEditorCell(GridField gridField, Object object, int i) {
		// Remark: following Included tab is create new editor from factory with
		// GridField, otherwise line cause the NPE on each row.
		WEditor editor;
		if (gridTab.isIncluded())
			editor = WebEditorFactory.getEditor(gridField, true);
		else
			editor = editors.get(gridField);
		
		if (editor != null)  {
			if (editor instanceof WButtonEditor)
            {
				if (m_windowPanel != null)
				{
					((WButtonEditor)editor).addActionListener(m_windowPanel);	
				}
				else
				{
					Object window = SessionManager.getAppDesktop().findWindow(windowNo);
	            	if (window != null && window instanceof ADWindow)
	            	{
	            		AbstractADWindowPanel windowPanel = ((ADWindow)window).getADWindowPanel();
	            		((WButtonEditor)editor).addActionListener(windowPanel);
	            	}
				}
            }
			else
			{
				editor.addValueChangeListener(dataBinder);
			}
			gridField.removePropertyChangeListener(editor);
			gridField.addPropertyChangeListener(editor);
			editor.setValue(object);

			// Remark: following Included tab is set value from object,
			// otherwise all line rendered with duplicate values.
			if (gridTab.isIncluded())
				editor.setValue(object);
			else
				editor.setValue(gridField.getValue());
			
            //streach component to fill grid cell
            if (editor.getComponent() instanceof Textbox)
            	((HtmlBasedComponent)editor.getComponent()).setWidth("94%");
            else
            	editor.fillHorizontal();

            if(editor instanceof WNumberEditor)
            {
            	Component tr= (Component)((Component)editor.getComponent().getChildren().get(0)).getChildren().get(0);
            	if(tr.getChildren().size() == 2)
            		tr.getChildren().remove(1);
            }
		}
		return editor;
	}

	private int getColumnIndex(GridField field) {
		GridField[] fields = gridTab.getFields();
		for(int i = 0; i < fields.length; i++) {
			if (fields[i] == field)
				return i;
		}
		return 0;
	}

	private Component createReadonlyCheckbox(Object value) {
		Checkbox checkBox = new Checkbox();
		if (value != null && "true".equalsIgnoreCase(value.toString()))
			checkBox.setChecked(true);
		else
			checkBox.setChecked(false);
		checkBox.setDisabled(true);
		return checkBox;
	}

	private String getDisplayText(Object value, GridField gridField)
	{
		if (value == null)
			return "";

		if (gridField.isEncryptedField())
		{
			return "********";
		}
		else if (gridField.isLookup())
    	{
			if (lookupCache != null)
			{
				Map<Object, String> cache = lookupCache.get(gridField.getColumnName());
				if (cache != null && cache.size() >0)
				{
					String text = cache.get(value);
					if (text != null)
					{
						return text;
					}
				}
			}
			NamePair namepair = gridField.getLookup().get(value);
			if (namepair != null)
			{
				String text = namepair.getName();
				if (lookupCache != null)
				{
					Map<Object, String> cache = lookupCache.get(gridField.getColumnName());
					if (cache == null)
					{
						cache = new HashMap<Object, String>();
						lookupCache.put(gridField.getColumnName(), cache);
					}
					cache.put(value, text);
				}
				return text;
			}
			else
				return "";
    	}
    	else if (gridTab.getTableModel().getColumnClass(getColumnIndex(gridField)).equals(Timestamp.class))
    	{
    		SimpleDateFormat dateFormat = DisplayType.getDateFormat(gridField.getDisplayType(), AEnv.getLanguage(Env.getCtx()));
    		return dateFormat.format((Timestamp)value);
    	}
    	else if (DisplayType.isNumeric(gridField.getDisplayType()))
    	{
    		return DisplayType.getNumberFormat(gridField.getDisplayType(), AEnv.getLanguage(Env.getCtx())).format(value);
    	}
    	else if (DisplayType.Button == gridField.getDisplayType())
    	{
    		return "";
    	}
    	else if (DisplayType.Image == gridField.getDisplayType())
    	{
    		if (value == null || (Integer)value <= 0)
    			return "";
    		else
    			return "...";
    	}
    	else
    		return value.toString();
	}

	private Component getDisplayComponent(Object value, GridField gridField) {
		Component component;
		if (gridField.getDisplayType() == DisplayType.YesNo) {
			component = createReadonlyCheckbox(value);
		} else {
			String text = getDisplayText(value, gridField);

			Label label = new Label();
			setLabelText(text, label);

			component = label;
		}
		return component;
	}

	/**
	 * @param text
	 * @param label
	 */
	private void setLabelText(String text, Label label) {
		String display = text;
		if (text != null && text.length() > MAX_TEXT_LENGTH)
			display = text.substring(0, MAX_TEXT_LENGTH - 3) + "...";
		if (display != null)
			display = XMLs.encodeText(display);
		label.appendChild(new Text(display));
		if (text != null && text.length() > MAX_TEXT_LENGTH)
			label.setDynamicProperty("title", text);
		else
			label.setDynamicProperty("title", "");
	}

	/**
	 *
	 * @return active editor list
	 */
	public List<WEditor> getEditors() {
		List<WEditor> editorList = new ArrayList<WEditor>();
		if (!editors.isEmpty())
			editorList.addAll(editors.values());

		return editorList;
	}

	/**
	 * @param paging
	 */
	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	/**
	 * Detach all editor and optionally set the current value of the editor as cell label.
	 * @param updateCellLabel
	 */
	public void stopEditing(boolean updateCellLabel) {
		if (!editing) {
			return;
		} else {
			editing = false;
		}
		Row row = null;
		for (Entry<GridField, WEditor> entry : editors.entrySet()) {
			if (entry.getValue().getComponent().getParent() != null) {
				Component child = entry.getValue().getComponent();
				Div div = null;
				while (div == null && child != null) {
					Component parent = child.getParent();
					if (parent instanceof Div && parent.getParent() instanceof Row)
						div = (Div)parent;
					else
						child = parent;
				}
				Component component = div.getFirstChild();
				if (updateCellLabel) {
					if (component instanceof Label) {
						Label label = (Label)component;
						label.getChildren().clear();
						String text = getDisplayText(entry.getValue().getValue(), entry.getValue().getGridField());
						setLabelText(text, label);
					} else if (component instanceof Checkbox) {
						Checkbox checkBox = (Checkbox)component;
						Object value = entry.getValue().getValue();
						if (value != null && "true".equalsIgnoreCase(value.toString()))
							checkBox.setChecked(true);
						else
							checkBox.setChecked(false);
					}
				}
				component.setVisible(true);
				if (row == null)
					row = ((Row)div.getParent());

				entry.getValue().getComponent().detach();
				entry.getKey().removePropertyChangeListener(entry.getValue());
				entry.getValue().removeValuechangeListener(dataBinder);
			}
		}

		GridTableListModel model = (GridTableListModel) grid.getModel();
		model.setEditing(false);
	}

	/**
	 * @param row
	 * @param data
	 * @see RowRenderer#render(Row, Object)
	 */
	public void render(Row row, Object data) throws Exception {
		//don't render if not visible
		int columnCount = 0;
		Object [] dataObj = (Object[]) data;
		GridField[] gridPanelFields = null;
		GridField[] gridTabFields = null;
		
		if (gridPanel != null)
		{
			if (!gridPanel.isVisible())
			{
				return;
			}
			else
			{
				gridPanelFields = gridPanel.getFields();
				columnCount = gridPanelFields.length;
				gridTabFields = gridTab.getFields();
				isGridViewCustomized = gridTabFields.length != gridPanelFields.length;
			}
		}

		if (grid == null)
			grid = (Grid) row.getParent().getParent();

		if (rowListener == null)
			rowListener = new RowListener((Grid)row.getParent().getParent());

		if (!isGridViewCustomized)
		{
			for (int i = 0; i < gridTabFields.length; i++)
			{
				if (gridPanelFields[i].getAD_Field_ID() != gridTabFields[i].getAD_Field_ID())
				{
					isGridViewCustomized = true;
					break;
				}
			}
		}
		
		if (!isGridViewCustomized)
		{
			currentValues = dataObj;
		}
		else
		{
			List<Object> dataList = new ArrayList<Object>();
			for (GridField gridField : gridPanelFields)
			{
				for (int i = 0; i < gridTabFields.length; i++)
				{
					if (gridField.getAD_Field_ID() == gridTabFields[i].getAD_Field_ID())
					{
						dataList.add(dataObj[i]);
						break;
					}
				}
			}
			currentValues = dataList.toArray(new Object[0]);
		}
		
		if (!isGridViewCustomized)
			columnCount = gridTab.getTableModel().getColumnCount();
		
		Grid grid = (Grid) row.getParent().getParent();
		org.zkoss.zul.Columns columns = grid.getColumns();

		int rowIndex = row.getParent().getChildren().indexOf(row);
		if (paging != null && paging.getPageSize() > 0) {
			rowIndex = (paging.getActivePage() * paging.getPageSize()) + rowIndex;
		}

		int colIndex = -1;
		for (int i = 0; i < columnCount; i++)
		{
			if (!(isGridViewCustomized && gridPanelFields[i].isDisplayedGrid()))
			{
				continue;
			}
			colIndex++;

			Div div = new Div();
			String divStyle = "border: none; width: 100%; height: 100%;";
			org.zkoss.zul.Column column = (org.zkoss.zul.Column) columns.getChildren().get(colIndex);
			if (column.isVisible())
			{
				if (gridTab.isIncluded())
				{
					if (DisplayType.YesNo == gridPanelFields[i].getDisplayType()
							|| DisplayType.Image == gridPanelFields[i].getDisplayType())
					{
						divStyle += "text-align:center; ";
					}
					WEditor editor = getEditorCell(gridPanelFields[i], currentValues[i], i);
					div.appendChild(editor.getComponent());
					editor.getComponent().addEventListener(Events.ON_FOCUS, gridPanel);
					WEditorPopupMenu popupMenu = editor.getPopupMenu();

					if (editor instanceof WNumberEditor)
					{
						((Table)((WNumberEditor) editor).getComponent().getFirstChild()).setStyle("float:right;");
					}
					
					if (popupMenu != null)
					{
						popupMenu.addMenuListener((ContextMenuListener) editor);
						div.appendChild(popupMenu);
					}
					if (!gridPanelFields[i].isDisplayed(true))
					{
						editor.setVisible(false);
					}
					editor.setReadWrite(gridPanelFields[i].isEditable(true));
				}
				else
				{
					Component component = getDisplayComponent(currentValues[i], gridPanelFields[i]);
					div.appendChild(component);
					// add hidden input component to help focusing to row
					div.appendChild(createAnchorInput());

					if (DisplayType.YesNo == gridPanelFields[i].getDisplayType()
							|| DisplayType.Image == gridPanelFields[i].getDisplayType())
					{
						divStyle += "text-align:center; ";
					}
					else if (DisplayType.isNumeric(gridPanelFields[i].getDisplayType()))
					{
						divStyle += "text-align:right; ";
					}
				}
			}
			div.setStyle(divStyle);
			div.setAttribute("columnName", gridPanelFields[i].getColumnName());
			div.addEventListener(Events.ON_CLICK, rowListener);
			if(!gridTab.isIncluded())
				div.addEventListener(Events.ON_DOUBLE_CLICK, rowListener);
			else
			{
				editing = true;
				GridTableListModel model = (GridTableListModel) grid.getModel();
				model.setEditing(true);
			}

			row.appendChild(div);
		}

		if (colIndex == -1)
		{
			org.zkoss.zul.Label label = new org.zkoss.zul.Label("No fields are marked as 'Displayed' or 'Displayed in Grid'.");
			row.appendChild(label);
			return;
		}
		
		if (rowIndex == gridTab.getCurrentRow() && !gridTab.isIncluded())
		{
			setCurrentRow(row);
		}
		row.addEventListener(Events.ON_OK, rowListener);
		if(gridTab.isIncluded() && currentDiv == null)
		{
			setCurrentCell(0, 0, KeyEvent.RIGHT);
			updateNavigationButton(0, gridTab.getRowCount());
		}
	}
	
	public void updateNavigationButton(int row, int totalRow)
	{
		if (totalRow == 1 || totalRow == 0 )
		{
			m_windowPanel.getToolbar().enableNavigation(false);
		}
		else if (row == 0)
		{
			m_windowPanel.getToolbar().enableFirstNavigation(false);
			m_windowPanel.getToolbar().enableLastNavigation(true);
		}
		else if (row == totalRow - 1)
		{
			m_windowPanel.getToolbar().enableFirstNavigation(true);
			m_windowPanel.getToolbar().enableLastNavigation(false);
		}
		else
		{
			m_windowPanel.getToolbar().enableNavigation(true);
		}
	}

	private Div currentDiv=null;
	
	public Div getCurrentDiv()
	{
		return currentDiv;
	}

	public void setCurrentCell(int row, int col, int code)
	{
		if (col < 0 || row < 0)
			return;
		while (!isEditable(row, col))
		{
			if (!(code == KeyEvent.RIGHT || code == KeyEvent.LEFT))
				break;
			else if (code == KeyEvent.RIGHT)
				++col;
			else if (code == KeyEvent.LEFT)
				--col;

			if (col < 0)
			{
				setFocusCell();
				return;
			}
		}

		if (row != currentRowIndex)
		{
			if (currentRow != null)
				currentRow.setStyle(null);
			if (grid.getRows().getChildren().size() <= 0)
			{
				currentDiv = null;
				return;
			}
			currentRow = ((Row) grid.getRows().getChildren().get(row));
			currentRowIndex = row;
			gridTab.setCurrentRow(row);
			currentRow.setStyle(CURRENT_ROW_STYLE);
		}

		setFocusToEditor();

		if (grid.getCell(row, col) instanceof Div)
			currentDiv = (Div) grid.getCell(row, col);

		if (currentDiv != null && code != 0)
		{
			setFocusCell();
		}
		else if (currentDiv == null)
		{
			setCurrentCell(row, 0, code);
		}
	}
	
	private boolean isEditable(int row,int col)
	{
		Div div = null;
		if (grid.getCell(row, col) instanceof Div)
			div = (Div) grid.getCell(row, col);
		else
			return true;
		
		if(div == null)
			return true;
		if (div.getChildren().size() <= 0)
			return false;
		
		if (div.getChildren().get(0) instanceof NumberBox && ( !((NumberBox) div.getChildren().get(0)).getDecimalbox().isDisabled() && !((NumberBox) div.getChildren().get(0)).getDecimalbox().isReadonly()))
			return true;
		else if (div.getChildren().get(0) instanceof Checkbox && ((Checkbox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Combobox && ((Combobox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Textbox && (!((Textbox) div.getChildren().get(0)).isDisabled() && !((Textbox) div.getChildren().get(0)).isReadonly()))
			return true;
		else if (div.getChildren().get(0) instanceof Datebox && ((Datebox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof DatetimeBox && ((DatetimeBox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Locationbox && ((Locationbox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Searchbox && (!((Searchbox) div.getChildren().get(0)).getTextbox().isDisabled() && !((Searchbox) div.getChildren().get(0)).getTextbox().isReadonly()))
			return true;
		else if (div.getChildren().get(0) instanceof Button && ((Button) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof Combinationbox && ((Combinationbox) div.getChildren().get(0)).isEnabled())
			return true;
		else if (div.getChildren().get(0) instanceof EditorBox && ((EditorBox) div.getChildren().get(0)).isEnabled())
			return true;
		else
			return false;
	}
	
	public void setFocusCell()
	{
		if (currentDiv == null || currentDiv.getChildren().size() <= 0)
			return;
		
		if (currentDiv.getChildren().get(0) instanceof NumberBox)
			((NumberBox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Checkbox)
			((Checkbox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Combobox)
			((Combobox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Textbox)
		{
			((Textbox) currentDiv.getChildren().get(0)).focus();
			((Textbox) currentDiv.getChildren().get(0)).select();
		}
		else if (currentDiv.getChildren().get(0) instanceof Datebox)
			((Datebox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof DatetimeBox)
			((DatetimeBox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Locationbox)
			((Locationbox) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof Combinationbox)
		{
			((Combinationbox) currentDiv.getChildren().get(0)).getTextbox().focus();
			((Combinationbox) currentDiv.getChildren().get(0)).getTextbox().select();
		}
		else if (currentDiv.getChildren().get(0) instanceof Searchbox)
		{
			((Searchbox) currentDiv.getChildren().get(0)).getTextbox().focus();
			((Searchbox) currentDiv.getChildren().get(0)).getTextbox().select();
		}
		else if (currentDiv.getChildren().get(0) instanceof Button)
			((Button) currentDiv.getChildren().get(0)).focus();
		else if (currentDiv.getChildren().get(0) instanceof EditorBox)
			((EditorBox) currentDiv.getChildren().get(0)).focus();
	}

	/**
	 * @param component
	 * @return
	 */
	private Input createAnchorInput() {
		Input input = new Input();
		input.setDynamicProperty("type", "text");
		input.setValue("");
		input.setDynamicProperty("readonly", "readonly");
		input.setStyle("border: none; display: none; width: 3px;");
		return input;
	}

	/**
	 * @param row
	 */
	public void setCurrentRow(Row row) {
		if (currentRow != null && currentRow.getParent() != null && currentRow != row) {
			currentRow.setStyle(null);
		}
		currentRow = row;
		currentRow.setStyle(CURRENT_ROW_STYLE);
//		setFocusToEditor();

//		currentRowIndex = gridTab.getCurrentRow();
//		if (currentRowIndex == gridTab.getCurrentRow()) {
			if (editing) {
				stopEditing(false);
				editCurrentRow();
			}
//		} else {
//			currentRowIndex = gridTab.getCurrentRow();
//			if (editing) {
//				stopEditing(false);
//			}
//		}
			}

	/**
	 * @return Row
	 */
	public Row getCurrentRow() {
		return currentRow;
	}

	/**
	 * @return current row index ( absolute )
	 */
	public int getCurrentRowIndex() {
		return currentRowIndex;
	}

	/**
	 * Enter edit mode
	 */
	public void editCurrentRow() {
		if (currentRow != null && currentRow.getParent() != null && currentRow.isVisible()
			&& grid != null && grid.isVisible() && grid.getParent() != null && grid.getParent().isVisible()) {
			GridField[] gridPanelFields = gridPanel.getFields();
			int columnCount = gridPanelFields.length;
			org.zkoss.zul.Columns columns = grid.getColumns();
			int colIndex = -1;
			if(!gridTab.isIncluded()) {
				for (int i = 0; i < columnCount; i++) {
					if (!isGridViewCustomized && !gridPanelFields[i].isDisplayedGrid()) {
						continue;
					}
					colIndex ++;
					
					if (editors.get(gridPanelFields[i]) == null)
						editors.put(gridPanelFields[i], WebEditorFactory.getEditor(gridPanelFields[i], true));
					org.zkoss.zul.Column column = (org.zkoss.zul.Column) columns.getChildren().get(colIndex);
					if (column.isVisible()) {
						Div div = (Div) currentRow.getChildren().get(colIndex);
						WEditor editor = getEditorCell(gridPanelFields[i], currentValues[i], i);
						div.appendChild(editor.getComponent());
						WEditorPopupMenu popupMenu = editor.getPopupMenu();
	
			            if (popupMenu != null)
			            {
			            	popupMenu.addMenuListener((ContextMenuListener)editor);
			            	div.appendChild(popupMenu);
			            }
			            div.getFirstChild().setVisible(false);
			            //check context
						if (!gridPanelFields[i].isDisplayed(true)) 
						{
							editor.setVisible(false);
						}
						editor.setReadWrite(gridPanelFields[i].isEditable(true));
					}
				}
			}
			editing = true;

			GridTableListModel model = (GridTableListModel) grid.getModel();
			model.setEditing(true);

		}
	}

	/**
	 * @see RowRendererExt#getControls()
	 */
	public int getControls() {
		return DETACH_ON_RENDER;
	}

	/**
	 * @see RowRendererExt#newCell(Row)
	 */
	public Component newCell(Row row) {
		return null;
	}

	/**
	 * @see RowRendererExt#newRow(Grid)
	 */
	public Row newRow(Grid grid) {
		return null;
	}

	/**
	 * @see RendererCtrl#doCatch(Throwable)
	 */
	public void doCatch(Throwable ex) throws Throwable {
		lookupCache = null;
	}

	/**
	 * @see RendererCtrl#doFinally()
	 */
	public void doFinally() {
		lookupCache = null;
	}

	/**
	 * @see RendererCtrl#doTry()
	 */
	public void doTry() {
		lookupCache = new HashMap<String, Map<Object,String>>();
	}

	/**
	 * set focus to first active editor
	 */
	public void setFocusToEditor() {
		if (currentRow != null && currentRow.getParent() != null) {
			WEditor toFocus = null;
			WEditor firstEditor = null;
			for (WEditor editor : getEditors()) {
				if (editor.isHasFocus() && editor.isVisible() && editor.getComponent().getParent() != null) {
					toFocus = editor;
					break;
				}

				if (editor.isVisible() && editor.getComponent().getParent() != null) {
					if (toFocus == null && editor.isReadWrite()) {
						toFocus = editor;
					}
					if (firstEditor == null)
						firstEditor = editor;
				}
			}
			if (toFocus != null) {
				Component c = toFocus.getComponent();
				if (c instanceof EditorBox) {
					c = ((EditorBox)c).getTextbox();
				}
				Clients.response(new AuFocus(c));
			} else if (firstEditor != null) {
				Component c = firstEditor.getComponent();
				if (c instanceof EditorBox) {
					c = ((EditorBox)c).getTextbox();
				}
				Clients.response(new AuFocus(c));
			}
		}
	}

	/**
	 *
	 * @param gridPanel
	 */
	public void setGridPanel(GridPanel gridPanel) {
		this.gridPanel = gridPanel;
	}

	class RowListener implements EventListener {

		private Grid _grid;

		public RowListener(Grid grid) {
			_grid = grid;
		}

		public void onEvent(Event event) throws Exception {
			if (Events.ON_CLICK.equals(event.getName())) {
				Event evt = new Event(Events.ON_CLICK, _grid, event.getTarget());
				Events.sendEvent(_grid, evt);
				evt.stopPropagation();
			}
			else if (Events.ON_DOUBLE_CLICK.equals(event.getName())) {
				Event evt = new Event(Events.ON_DOUBLE_CLICK, _grid, _grid);
				Events.sendEvent(_grid, evt);
			}
			else if (Events.ON_OK.equals(event.getName())) {
				Event evt = new Event(Events.ON_OK, _grid, _grid);
				Events.sendEvent(_grid, evt);
			}
		}
	}

	/**
	 * @return boolean
	 */
	public boolean isEditing() {
		return editing;
	}

	/**
	 * @param windowPanel
	 */
	public void setADWindowPanel(AbstractADWindowPanel windowPanel) {
		this.m_windowPanel = windowPanel;
	}
}
