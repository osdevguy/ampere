package org.adempiere.webui.process;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.window.WDistributeCost;
import org.zkoss.zk.ui.DesktopUnavailableException;

/**
 * Execute from org.compiere.process.DistributeCost process
 * 
 * @author Sachin Bhimani
 */
public class DistributeCost
{
	public DistributeCost(int Record_ID)
	{
		try
		{
			if (AEnv.getDesktop() != null)
			{
				SessionManager.activateDesktop(AEnv.getDesktop());
				new WDistributeCost(Record_ID);
				SessionManager.releaseDesktop(AEnv.getDesktop());
			}
		}
		catch (DesktopUnavailableException e)
		{
			throw new AdempiereException("Timeout activate desktop." + e);
		}

	}
}
