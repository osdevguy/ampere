package org.adempiere.webui.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.sql.RowSet;

import org.adempiere.exceptions.AdempiereException;
import org.apache.commons.lang.StringUtils;
import org.compiere.model.MProduct;
import org.compiere.model.MQuery;
import org.compiere.model.MTable;
import org.compiere.model.MWarehouse;
import org.compiere.model.PrintInfo;
import org.compiere.model.X_T_Product_BOMLine;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Language;
import org.compiere.util.ValueNamePair;

/**
 * Exploding BOM product with required qty calculation and shown report
 * 
 * @author Sachin Bhimani
 */
public class ProductionTestKitPull extends SvrProcess
{

	private int					p_M_Product_ID				= 0;
	private int					p_M_Warehouse_ID			= 0;
	private BigDecimal			p_QtyRequiered				= Env.ONE;
	private int					p_LevelNo					= 1;

	public static final String	SQL_BOM_PRODUCT_EXPLODING	= " SELECT t.M_Product_ID, t.M_ProductBOM_ID, t.LevelNo, t.Levels, l.M_Locator_ID, l.X, l.Y, l.Z, "
			+ " 	t.BOMQty, ROUND(t.QtyRequired, 4) AS QtyRequired, p.C_UOM_ID, "
			+ " 	SUM(s.qtyonhand) AS QtyOnHand, SUM(s.QtyReserved) AS QtyReserved, SUM(s.QtyOrdered) AS QtyOrdered "
			+ " FROM( WITH RECURSIVE supplytree(M_Product_ID) AS ( "
			+ " 		SELECT M_Product_ID, M_ProductBOM_ID, BomQty, BomQty * ?::Numeric AS QtyRequired, 1 AS LevelNo, M_Product_ID || '-' || M_ProductBOM_ID AS Levels "
			+ " 		FROM M_Product_BOM WHERE M_Product_ID = ? "
			+ " 	UNION ALL  																			"
			+ " 		SELECT b.M_Product_ID, b.M_ProductBOM_ID, b.BOMQty, b.BOMQty * sp.QtyRequired AS QtyRequired, LevelNo + 1 , Levels || '-' || b.M_ProductBOM_ID "
			+ " 		FROM M_Product_BOM AS b  "
			+ " 		INNER JOIN supplytree AS sp ON (sp.M_ProductBOM_ID = b.M_Product_ID) "
			+ " 		WHERE b.IsActive='Y' "
			+ "   ) SELECT sp.M_Product_ID, sp.M_ProductBOM_ID, sp.BOMQty, sp.QtyRequired, sp.LevelNo, sp.Levels "
			+ " 	FROM supplytree sp WHERE LevelNo <= ? "
			+ " UNION ALL																				"
			+ " 	SELECT M_Product_ID, M_Product_ID, 1, ? AS QtyRequired, 0 AS LevelNo, '' || M_Product_ID AS Levels "
			+ " 	FROM M_Product 	WHERE M_Product_ID = ? ) AS t "
			+ " INNER JOIN M_Product p ON (p.M_Product_ID = t.M_Product_ID) "
			+ " LEFT JOIN M_Storage s ON s.m_product_id = t.M_ProductBOM_ID AND s.QtyOnHand <> 0 AND (EXISTS (SELECT 1 FROM M_Locator ld WHERE s.M_Locator_ID = ld.M_Locator_ID AND ld.M_Warehouse_ID = ?)) "
			+ " LEFT JOIN M_Locator l ON l.M_Locator_ID = s.M_Locator_ID "
			+ " GROUP BY t.M_Product_ID, p.M_Product_ID, t.M_ProductBOM_ID, t.BOMQty, t.QtyRequired, t.LevelNo, t.Levels, l.M_Locator_ID "
			+ " ORDER BY t.Levels ";

	@Override
	protected void prepare()
	{
		for (ProcessInfoParameter para : getParameter())
		{
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals(MWarehouse.COLUMNNAME_M_Warehouse_ID))
				p_M_Warehouse_ID = para.getParameterAsInt();
			else if (name.equals(X_T_Product_BOMLine.COLUMNNAME_M_Product_ID))
				p_M_Product_ID = para.getParameterAsInt();
			else if (name.equals(X_T_Product_BOMLine.COLUMNNAME_QtyRequired))
				p_QtyRequiered = para.getParameterAsBigDecimal();
			else if (name.equals(X_T_Product_BOMLine.COLUMNNAME_LevelNo))
				p_LevelNo = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception
	{
		final MProduct product = MProduct.get(getCtx(), p_M_Product_ID);
		if (!product.isVerified())
			throw new AdempiereException(
					"Product BOM Configuration not verified. Please verify the product first - " + product.getValue());
		try
		{
			loadBOM();
			print();
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "PrintBOM", e.toString());
			throw new Exception(e.getLocalizedMessage());
		}
		finally
		{
			String sql = "DELETE FROM T_BomLine WHERE AD_PInstance_ID = " + getAD_PInstance_ID();
			DB.executeUpdateEx(sql, get_TrxName());
		}

		return "@OK@";
	}

	private void loadBOM() throws Exception
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int seqNo = 10;
		int i = 1;
		try
		{
			pstmt = DB.prepareStatement(SQL_BOM_PRODUCT_EXPLODING, get_TrxName());
			pstmt.setBigDecimal(i++, p_QtyRequiered);
			pstmt.setInt(i++, p_M_Product_ID);
			pstmt.setInt(i++, p_LevelNo);
			pstmt.setBigDecimal(i++, p_QtyRequiered);
			pstmt.setInt(i++, p_M_Product_ID);
			pstmt.setInt(i++, p_M_Warehouse_ID);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				X_T_Product_BOMLine bLine = new X_T_Product_BOMLine(getCtx(), 0, get_TrxName());
				bLine.setM_Product_ID(p_M_Product_ID);
				bLine.setAD_PInstance_ID(getAD_PInstance_ID());
				bLine.setM_ProductBOM_ID(rs.getInt("M_ProductBOM_ID"));
				bLine.setLevelNo(p_LevelNo);
				bLine.setLevels(StringUtils.repeat(".", rs.getInt("LevelNo")) + rs.getString("LevelNo"));
				bLine.setM_Locator_ID(rs.getInt("M_Locator_ID"));
				bLine.setX(rs.getString("X"));
				bLine.setY(rs.getString("Y"));
				bLine.setZ(rs.getString("Z"));
				bLine.setC_UOM_ID(rs.getInt("C_UOM_ID"));
				bLine.setBOMQty(rs.getBigDecimal("BOMQty"));
				bLine.setQtyRequired(rs.getBigDecimal("QtyRequired"));
				bLine.setQtyOnHand(rs.getBigDecimal("QtyOnHand"));
				bLine.setQtyReserved(rs.getBigDecimal("QtyReserved"));
				bLine.setQtyOrdered(rs.getBigDecimal("QtyOrdered"));
				bLine.setSeqNo(seqNo);
				bLine.saveEx();

				seqNo += 10;
			}
		}
		catch (SQLException e)
		{
			throw new Exception(e.getLocalizedMessage());
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}

	/**
	 * Print result generate for this report
	 */
	void print() throws Exception
	{
		int pfid = 0;
		MPrintFormat pf = null;
		Language language = Language.getLoginLanguage(); // Base Language

		// get print format for client, else copy system to client
		ResultSet pfrs = MPrintFormat.getAccessiblePrintFormats(MTable.getTable_ID(X_T_Product_BOMLine.Table_Name), -1,
				null);
		pfrs.next();
		pfid = pfrs.getInt("AD_PrintFormat_ID");

		if (pfrs.getInt("AD_Client_ID") != 0)
			pf = MPrintFormat.get(getCtx(), pfid, false);
		else
			pf = MPrintFormat.copyToClient(getCtx(), pfid, getAD_Client_ID());
		pfrs.close();

		if (pf == null)
			raiseError("Error: ", "No Print Format");

		pf.setLanguage(language);
		pf.setTranslationLanguage(language);

		// query
		MQuery query = new MQuery(X_T_Product_BOMLine.Table_Name);
		query.addRestriction(X_T_Product_BOMLine.COLUMNNAME_AD_PInstance_ID, MQuery.EQUAL, getAD_PInstance_ID(),
				getParamenterName(X_T_Product_BOMLine.COLUMNNAME_AD_PInstance_ID),
				getParamenterInfo(X_T_Product_BOMLine.COLUMNNAME_AD_PInstance_ID));

		PrintInfo info = new PrintInfo(X_T_Product_BOMLine.Table_Name,
				MTable.getTable_ID(X_T_Product_BOMLine.Table_Name), getRecord_ID());
		ReportEngine re = new ReportEngine(getCtx(), pf, query, info, get_TrxName());

		ReportCtl.preview(re);
	}

	public String getParamenterInfo(String name)
	{
		final String sql = "SELECT ip.Info FROM AD_PInstance_Para ip"
				+ " WHERE ip.AD_PInstance_ID=? AND ip.ParameterName=?";
		return DB.getSQLValueString(get_TrxName(), sql, getProcessInfo().getAD_PInstance_ID(), name);
	}

	public String getParamenterName(String columnName)
	{
		boolean trl = !Env.isBaseLanguage(getCtx(), "AD_Process_Para");
		String sql = null;
		if (trl)
		{
			sql = "SELECT pp.Name FROM AD_Process_Para pp " + "WHERE pp.IsActive='Y' "
					+ " AND pp.AD_Process_ID=? AND pp.ColumnName=?";
		}
		else
		{
			sql = "SELECT ppt.Name " + "FROM AD_Process_Para pp , AD_Process_Para_Trl ppt " + "WHERE pp.IsActive='Y'"
					+ " AND pp.AD_Process_Para_ID=ppt.AD_Process_Para_ID"
					+ " AND pp.AD_Process_ID=? AND pp.ColumnName=?";
		}
		return DB.getSQLValueString(get_TrxName(), sql, getProcessInfo().getAD_Process_ID(), columnName);
	}

	private void raiseError(String string, String hint) throws Exception
	{
		String msg = string;
		ValueNamePair pp = CLogger.retrieveError();
		if (pp != null)
			msg = pp.getName() + " - ";
		msg += hint;
		throw new Exception(msg);
	}
}
