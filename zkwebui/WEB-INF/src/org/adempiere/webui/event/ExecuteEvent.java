package org.adempiere.webui.event;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

/**
 * 
 * @author jobriant
 *
 */
public class ExecuteEvent extends Event {

	public static final String EVENT_NAME = "onExecute";

	public ExecuteEvent(Component target, Object data) {
		super(EVENT_NAME, target, data);
	}

}
