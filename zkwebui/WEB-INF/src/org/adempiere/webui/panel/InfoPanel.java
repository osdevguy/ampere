/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.panel;

import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.DBException;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.BusyDialog;
import org.adempiere.webui.apps.ProcessModalDialog;
import org.adempiere.webui.apps.WProcessCtl;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.info.InfoWindow;
import org.adempiere.webui.part.ITabOnSelectHandler;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.window.FDialog;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MInfoColumn;
import org.compiere.model.MInfoProcess;
import org.compiere.model.MInfoWindow;
import org.compiere.model.MPInstance;
import org.compiere.model.MProcess;
import org.compiere.model.MRole;
import org.compiere.model.MTable;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.au.out.AuEcho;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ListModelExt;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.event.ZulEvents;

/**
 *	Search Information and return selection - Base Class.
 *  Based on Info written by Jorg Janke
 *  
 *  @author Sendy Yagambrum
 *  
 * Zk Port
 * @author Elaine
 * @version	Info.java Adempiere Swing UI 3.4.1
 */
public abstract class InfoPanel extends Window implements EventListener, WTableModelListener, ListModelExt
{

	/**
	 * generated serial version ID
	 */
	private static final long										serialVersionUID		= 325050327514511004L;
	protected CLogger												log						= CLogger
			.getCLogger(getClass());

	private final static int										PAGE_SIZE				= 100;
	private static final String[]									lISTENER_EVENTS			= {};
	protected final static String									PROCESS_ID_KEY			= "processId";
	protected final static String									ON_RUN_PROCESS			= "onRunProcess";
	protected final static String									ATT_INFO_PROCESS_KEY	= "INFO_PROCESS";
	protected static final int										INFO_WIDTH				= 800;

	protected List<Button>											btProcessList			= new ArrayList<Button>();
	protected Map<String, WEditor>									editorMap				= new HashMap<String, WEditor>();
	public LinkedHashMap<Integer, LinkedHashMap<String, Object>>	m_values				= null;
	/** Initial ID value from context */
	protected int													initialId				= 0;
	protected ConfirmPanel											confirmPanel;
	/** Master (owning) Window */
	protected int													p_WindowNo;
	/** Table Name */
	protected String												p_tableName;
	/** Key Column Name */
	protected String												p_keyColumn;
	/** Enable more than one selection */
	protected boolean												p_multipleSelection;
	/** Initial WHERE Clause */
	protected String												p_whereClause			= "";

	protected StatusBarPanel										statusBar				= new StatusBarPanel();
	private List<Object>											line;
	protected boolean												m_lookup;
	private boolean													m_ok					= false;
	/** Cancel pressed - need to differentiate between OK - Cancel - Exit */
	private boolean													m_cancel				= false;
	/** Result IDs */
	private ArrayList<Integer>										m_results				= new ArrayList<Integer>(3);
	private ListModelTable<Object>									model;
	/** Layout of Grid */
	protected ColumnInfo[]											p_layout;
	/** Main SQL Statement */
	protected String												m_sqlMain;
	/** Count SQL Statement */
	private String													m_sqlCount;
	/** Order By Clause */
	protected String												m_sqlOrder;

	protected String												m_sqlUserOrder;
	/** ValueChange listeners */
	private ArrayList<ValueChangeListener>							listeners				= new ArrayList<ValueChangeListener>();
	/** Loading success indicator */
	protected boolean												p_loadedOK				= false;
	/** SO Zoom Window */
	private int														m_SO_Window_ID			= -1;
	/** PO Zoom Window */
	private int														m_PO_Window_ID			= -1;

	@SuppressWarnings("unused")
	private MInfoWindow												infoWindow;

	protected WListbox												contentPanel			= new WListbox();
	protected Paging												paging;
	protected int													pageNo;
	protected int													m_count;
	private int														cacheStart;
	private int														cacheEnd;
	private boolean													m_useDatabasePaging		= false;
	private BusyDialog												progressWindow;
	private int														m_lastSelectedIndex		= -1;
	// false, use saved where clause
	protected boolean												isQueryByUser			= false;
	// save where clause of prev requery
	protected String												prevWhereClause			= null;
	// save value of parameter to set info query paramenter
	protected List<Object>											prevParameterValues		= null;
	protected List<String>											prevQueryOperators		= null;
	protected List<WEditor>											prevRefParmeterEditor	= null;

	// All info process of this infoWindow
	protected MInfoProcess[]										infoProcessList;
	// Info process have style is button
	protected List<MInfoProcess>									infoProcessBtList;
	// Info process have style is drop down list
	protected List<MInfoProcess>									infoProcessDropList;
	// Info process have style is menu
	protected List<MInfoProcess>									infoProcessMenuList;
	// save selected id and viewID
	protected Map<Integer, List<String>>							m_viewIDMap				= new HashMap<Integer, List<String>>();
	// flag indicate have infoOProcess define ViewID
	protected boolean												isHasViewID				= false;
	// number of infoProcess contain ViewID
	protected int													numOfViewID				= 0;
	protected Button												btCbbProcess;
	protected Combobox												cbbProcess;
	protected Button												btMenuProcess;

	public static InfoPanel create(int WindowNo, String tableName, String keyColumn, String value,
			boolean multiSelection, String whereClause, int AD_InfoWindow_ID, boolean lookup)
	{
		InfoPanel info = null;
		info = new InfoWindow(WindowNo, tableName, keyColumn, value, multiSelection, whereClause, AD_InfoWindow_ID,
				lookup);
		if (!info.loadedOK())
		{
			info = create(WindowNo, tableName, keyColumn, value, multiSelection, whereClause, lookup);
			if (!info.loadedOK())
			{
				info.dispose(false);
				info = null;
			}
		}

		return info;
	}
	
    public static InfoPanel create (int WindowNo,
            String tableName, String keyColumn, String value,
            boolean multiSelection, String whereClause, boolean lookup)
        {
            InfoPanel info = null;

            if (tableName.equals("C_BPartner"))
                info = new InfoBPartnerPanel (value,WindowNo, !Env.getContext(Env.getCtx(),"IsSOTrx").equals("N"),
                        multiSelection, whereClause);
            else if (tableName.equals("M_Product"))
                info = new InfoProductPanel ( WindowNo,  0,0, 
                        multiSelection, value,whereClause);
            else if (tableName.equals("C_Invoice"))
                info = new InfoInvoicePanel ( WindowNo, value,
                        multiSelection, whereClause);
            else if (tableName.equals("A_Asset"))
                info = new InfoAssetPanel (WindowNo, 0, value,
                        multiSelection, whereClause);
            else if (tableName.equals("C_Order"))
                info = new InfoOrderPanel ( WindowNo, value,
                        multiSelection, whereClause);
            else if (tableName.equals("M_InOut"))
                info = new InfoInOutPanel (WindowNo, value,
                        multiSelection, whereClause);
            else if (tableName.equals("C_Payment"))
                info = new InfoPaymentPanel (WindowNo, value, multiSelection, whereClause);
            else if (tableName.equals("C_CashLine"))
               info = new InfoCashLinePanel (WindowNo, value,
                        multiSelection, whereClause);
            else if (tableName.equals("S_ResourceAssigment"))
                info = new InfoAssignmentPanel (WindowNo, value,
                        multiSelection, whereClause);
            else
			{
				info = new InfoWindow(WindowNo, tableName, keyColumn, value, multiSelection, whereClause, 0, lookup);
				if (!info.loadedOK())
				{
					info = new InfoGeneralPanel(value, WindowNo, tableName, keyColumn, multiSelection, whereClause);
					if (!info.loadedOK())
					{
						info.dispose(false);
						info = null;
					}
				}
			}
            
            return info;
        }
    
	/**
	 * Show BPartner Info (non modal)
	 * @param WindowNo window no
	 */
	public static void showBPartner (int WindowNo)
	{
		InfoBPartnerPanel info = new InfoBPartnerPanel ( "", WindowNo,
			!Env.getContext(Env.getCtx(),"IsSOTrx").equals("N"),false, "", false);
		AEnv.showWindow(info);
	}   //  showBPartner

	/**
	 * Show Asset Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 */
	public static void showAsset (int WindowNo)
	{
		InfoPanel info = new InfoAssetPanel (WindowNo, 0, "", false, "", false);
		AEnv.showWindow(info);
	}   //  showBPartner

	/**
	 * Show Product Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 */
	public static void showProduct (int WindowNo)
	{
		InfoPanel info = new InfoProductPanel(WindowNo, 
				Env.getContextAsInt(Env.getCtx(), WindowNo, "M_Warehouse_ID"),
				Env.getContextAsInt(Env.getCtx(), WindowNo, "M_PriceList_ID"),
				false, "", "", false);
		AEnv.showWindow(info);
	}   //  showProduct
	
	/**
	 * Show Order Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showOrder (int WindowNo, String value)
	{
		InfoPanel info = new InfoOrderPanel(WindowNo, "", false, "", false);
		AEnv.showWindow(info);
	}   //  showOrder

	/**
	 * Show Order Info but Only on Credit (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showOrderOnCredit (int WindowNo, String value)
	{
		InfoOrderPanel info = new InfoOrderPanel(WindowNo, "", false, "", false, true);
		AEnv.showWindow(info);
		info.onUserQuery();
	}   //  showOrder

	/**
	 * Show Invoice Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showInvoice (int WindowNo, String value)
	{
		InfoPanel info = new InfoInvoicePanel(WindowNo, "", false, "", false);
		AEnv.showWindow(info);
	}   //  showInvoice

	/**
	 * Show Shipment Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showInOut (int WindowNo, String value)
	{
		InfoPanel info = new InfoInOutPanel (WindowNo, value,
			false, "", false);
		AEnv.showWindow(info);
	}   //  showInOut

	/**
	 * Show Payment Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showPayment (int WindowNo, String value)
	{
		InfoPanel info = new InfoPaymentPanel (WindowNo, value,
			false, "", false);
		AEnv.showWindow(info);
	}   //  showPayment

	/**
	 * Show Cash Line Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showCashLine (int WindowNo, String value)
	{
		InfoPanel info = new InfoCashLinePanel (WindowNo, value,
			false, "", false);
		AEnv.showWindow(info);
	}   //  showCashLine

	/**
	 * Show Assignment Info (non modal)
	 * @param frame Parent Frame
	 * @param WindowNo window no
	 * @param value query value
	 */
	public static void showAssignment (int WindowNo, String value)
	{
		InfoPanel info = new InfoAssignmentPanel (WindowNo, value,
			false, "", false);
		AEnv.showWindow(info);
	}   //  showAssignment

	/**************************************************
     *  Detail Constructor 
     * @param WindowNo  WindowNo
     * @param tableName tableName
     * @param keyColumn keyColumn   
     * @param whereClause   whereClause
	 */
	protected InfoPanel (int WindowNo,
		String tableName, String keyColumn,boolean multipleSelection,
		 String whereClause)
	{
		this(WindowNo, tableName, keyColumn, multipleSelection, whereClause, true);
	}
		
	/**************************************************
	 * Detail Constructor
	 * 
	 * @param WindowNo WindowNo
	 * @param tableName tableName
	 * @param keyColumn keyColumn
	 * @param whereClause whereClause
	 */
	protected InfoPanel(int WindowNo, String tableName, String keyColumn, boolean multipleSelection, String whereClause,
			boolean lookup)
	{
		log.info("WinNo=" + p_WindowNo + " " + whereClause);

		if (WindowNo <= 0)
		{
			p_WindowNo = SessionManager.getAppDesktop().registerWindow(this);
		}
		else
		{
			p_WindowNo = WindowNo;
		}

		p_tableName = tableName;
		p_keyColumn = keyColumn;
		p_multipleSelection = multipleSelection;
		m_lookup = lookup;

		if (whereClause == null || whereClause.indexOf('@') == -1)
		{
			p_whereClause = whereClause == null ? "" : whereClause;
		}
		else
		{
			p_whereClause = Env.parseContext(Env.getCtx(), p_WindowNo, whereClause, false, false);
			if (p_whereClause.length() == 0)
				log.log(Level.SEVERE, "Cannot parse context= " + whereClause);
		}

		init();

		this.setAttribute(ITabOnSelectHandler.ATTRIBUTE_KEY, new ITabOnSelectHandler() {
			public void onSelect()
			{
				scrollToSelectedRow();
			}
		});

		// infoWindow = MInfoWindow.get(p_keyColumn.replace("_ID", ""), null);
		// addEventListener(WindowContainer.ON_WINDOW_CONTAINER_SELECTION_CHANGED_EVENT,
		// this);
		addEventListener(ON_RUN_PROCESS, this);
		addEventListener(Events.ON_CLOSE, this);

	} // InfoPanel
	
	private void init()
	{
		if (isLookup())
		{
			setAttribute(Window.MODE_KEY, Window.MODE_MODAL);
			setBorder("normal");
			setClosable(true);
			int height = SessionManager.getAppDesktop().getClientInfo().desktopHeight * 85 / 100;
    		int width = SessionManager.getAppDesktop().getClientInfo().desktopWidth * 80 / 100;
    		setWidth(width + "px");
    		setHeight(height + "px");
    		this.setContentStyle("overflow: auto");
		}
		else
		{
			setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
			setBorder("none");
			setWidth("100%");
			setHeight("100%");
			setStyle("position: absolute;");
		}
		
        confirmPanel = new ConfirmPanel(true, true, false, true, true, true);  // Elaine 2008/12/16
        confirmPanel.addActionListener(Events.ON_CLICK, this);
        
        // Elaine 2008/12/16
		confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE).setVisible(hasCustomize());
		confirmPanel.getButton(ConfirmPanel.A_HISTORY).setVisible(hasHistory());
		confirmPanel.getButton(ConfirmPanel.A_ZOOM).setVisible(hasZoom());		
		//
		if (!isLookup()) 
		{
			confirmPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
		}
		
        this.setSizable(true);      
        this.setMaximizable(true);
        
        this.addEventListener(Events.ON_OK, this);
        if (isLookup())
        	addEventListener(Events.ON_CANCEL, this);
        contentPanel.setOddRowSclass(null);
        contentPanel.addEventListener("onAfterRender", this);
        this.setSclass("info-panel");
		contentPanel.setStyle("overflow: hidden"); // not working - force in CSS - jobrian

	}  //  init

	/**
	 *  Loaded correctly
	 *  @return true if loaded OK
	 */
	public boolean loadedOK()
	{
		return p_loadedOK;
	}   //  loadedOK

	/**
	 *	Set Status Line
	 *  @param text text
	 *  @param error error
	 */
	public void setStatusLine (String text, boolean error)
	{
		statusBar.setStatusLine(text, error);
	}	//	setStatusLine

	/**
	 *	Set Status DB
	 *  @param text text
	 */
	public void setStatusDB (String text)
	{
		statusBar.setStatusDB(text);
	}	//	setStatusDB

	protected void prepareTable(ColumnInfo[] layout, String from, String where, String orderBy)
	{
		String sql = contentPanel.prepareTable(layout, from, where, p_multipleSelection, getTableName(), false);
		p_layout = contentPanel.getLayout();
		m_sqlMain = sql;
		m_sqlCount = "SELECT COUNT(*) FROM " + from + " WHERE " + where;
		m_sqlOrder = "";
		// m_sqlUserOrder = "";
		if (orderBy != null && orderBy.length() > 0)
			m_sqlOrder = " ORDER BY " + orderBy;
	} // prepareTable

	
	/**************************************************************************
	 *  Execute Query
	 */
	protected void executeQuery()
	{
		line = new ArrayList<Object>();
		cacheStart = -1;
		cacheEnd = -1;

		testCount();
		m_useDatabasePaging = (m_count > 1000);
		if (m_useDatabasePaging)
		{			
			return ;
		}
		else
		{
			readLine(0, -1);
		}
	}
            
	private void readData(ResultSet rs) throws SQLException
	{
		int colOffset = 1; // columns start with 1
		List<Object> data = new ArrayList<Object>();
		for (int col = 0; col < p_layout.length; col++)
		{
			Object value = null;
			Class<?> c = p_layout[col].getColClass();
			int colIndex = col + colOffset;
			if (c == IDColumn.class)
			{
				value = new IDColumn(rs.getInt(colIndex));

			}
			else if (c == Boolean.class)
				value = new Boolean("Y".equals(rs.getString(colIndex)));
			else if (c == Timestamp.class)
				value = rs.getTimestamp(colIndex);
			else if (c == BigDecimal.class)
				value = rs.getBigDecimal(colIndex);
			else if (c == Double.class)
				value = new Double(rs.getDouble(colIndex));
			else if (c == Integer.class)
				value = new Integer(rs.getInt(colIndex));
			else if (c == KeyNamePair.class)
			{
				if (p_layout[col].isKeyPairCol())
				{
					String display = rs.getString(colIndex);
					int key = rs.getInt(colIndex + 1);
					if (!rs.wasNull())
					{
						value = new KeyNamePair(key, display);
					}

					colOffset++;
				}
				else
				{
					int key = rs.getInt(colIndex);
					if (!rs.wasNull())
					{
						WEditor editor = editorMap.get(p_layout[col].getColSQL());
						if (editor != null)
						{
							editor.setValue(key);
							value = new KeyNamePair(key, editor.getDisplayTextForGridView(key));
						}
						else
						{
							value = new KeyNamePair(key, Integer.toString(key));
						}
					}
				}
			}
			else
			{
				value = rs.getString(colIndex);
			}
			data.add(value);
		}
		line.add(data);

		readViewID(rs, data);
	}
    
	/**
	 * save all viewID to end of data line when override readData(ResultSet rs),
	 * consider call this method
	 */
	protected void readViewID(ResultSet rs, List<Object> data) throws SQLException
	{
		if (infoProcessList == null || infoProcessList.length == 0)
		{
			return;
		}

		// with each process have viewID, read it form resultSet by name
		for (MInfoProcess infoProcess : infoProcessList)
		{
			if (infoProcess.getAD_InfoColumn_ID() <= 0)
				continue;

			MInfoColumn infocol = (MInfoColumn) infoProcess.getAD_InfoColumn();
			String viewIDValue = rs.getString(infocol.getColumnName());
			if (rs.wasNull())
			{
				data.add(null);
			}
			else
			{
				data.add(viewIDValue);
			}
		}
	}
	
    protected void renderItems()
    {
        if (m_count > 0)
        {
        	if (m_count > PAGE_SIZE)
        	{
        		if (paging == null) 
        		{
	        		paging = new Paging();
	    			paging.setPageSize(PAGE_SIZE);
	    			paging.setTotalSize(m_count);
	    			paging.setDetailed(true);
	    			paging.addEventListener(ZulEvents.ON_PAGING, this);
	    			insertPagingComponent();
        		}
        		else
        		{
        			paging.setTotalSize(m_count);
        			paging.setActivePage(0);
        		}
    			List<Object> subList = readLine(0, PAGE_SIZE);
    			model = new ListModelTable<Object>(subList);
    			model.setSorter(this);
	            model.addTableModelListener(this);
	            contentPanel.setData(model, null);
	            
	            pageNo = 0;
        	}
        	else
        	{
        		if (paging != null) 
        		{
        			paging.setTotalSize(m_count);
        			paging.setActivePage(0);
        			pageNo = 0;
        		}
	            model = new ListModelTable<Object>(readLine(0, -1));
	            model.setSorter(this);
	            model.addTableModelListener(this);
	            contentPanel.setData(model, null);
        	}
        }
        else
		{
			if (paging != null)
			{
				paging.setTotalSize(m_count);
				paging.setActivePage(0);
				pageNo = 0;
			}
			model = new ListModelTable<Object>(new ArrayList<Object>());
			model.setSorter(this);
			model.addTableModelListener(this);
			contentPanel.setData(model, null);
		}
        
        //jobriant
        if  (m_count == 1) {
        	contentPanel.setSelectedIndex(0);
        	SelectEvent se = new SelectEvent("onSelect", contentPanel, null, contentPanel.getSelectedItem());
        	Events.sendEvent(contentPanel, se);
        	//refresh(contentPanel.getValueAt(row,2), M_Warehouse_ID, M_PriceList_Version_ID);
        }
        // metas c.ghita@metas.ro : start
        int no = m_count;
        setStatusLine(Integer.toString(no) + " " + Msg.getMsg(Env.getCtx(), "SearchRows_EnterQuery"), false);
        setStatusDB(Integer.toString(no));
        addDoubleClickListener();
    }
    
	private List<Object> readLine(int start, int end)
	{
		// cacheStart & cacheEnd - 1 based index, start & end - 0 based index
		if (cacheStart >= 1 && cacheEnd > cacheStart)
		{
			if (start + 1 >= cacheStart && end + 1 <= cacheEnd)
			{
				return end == -1 ? line : line.subList(start - cacheStart + 1, end - cacheStart + 2);
			}
		}

		cacheStart = start + 1 - (PAGE_SIZE * 4);
		if (cacheStart <= 0)
			cacheStart = 1;

		if (end == -1)
		{
			cacheEnd = m_count;
		}
		else
		{
			cacheEnd = end + 1 + (PAGE_SIZE * 4);
			if (cacheEnd > m_count)
				cacheEnd = m_count;
		}

		line = new ArrayList<Object>();

		PreparedStatement m_pstmt = null;
		ResultSet m_rs = null;

		long startTime = System.currentTimeMillis();
		//
		String dataSql = buildDataSQL(start, end);
		if (log.isLoggable(Level.FINER))
			log.finer(dataSql);
		try
		{
			m_pstmt = DB.prepareStatement(dataSql, null);
			setParameters(m_pstmt, false); // no count
			if (log.isLoggable(Level.FINE))
				log.fine("Start query - " + (System.currentTimeMillis() - startTime) + "ms");
			m_pstmt.setFetchSize(100);
			m_rs = m_pstmt.executeQuery();
			if (log.isLoggable(Level.FINE))
				log.fine("End query - " + (System.currentTimeMillis() - startTime) + "ms");
			// skips the row that we dont need if we can't use native db paging
			if (end > start && m_useDatabasePaging && !DB.getDatabase().isPagingSupported())
			{
				for (int i = 0; i < cacheStart - 1; i++)
				{
					if (!m_rs.next())
						break;
				}
			}

			int rowPointer = cacheStart - 1;
			while (m_rs.next())
			{
				rowPointer++;
				readData(m_rs);
				// check now of rows loaded, break if we hit the suppose end
				if (m_useDatabasePaging && rowPointer >= cacheEnd)
				{
					break;
				}
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, dataSql, e);
		}
		finally
		{
			DB.close(m_rs, m_pstmt);
			m_rs = null;
			m_pstmt = null;
		}

		if (end >= cacheEnd || end <= 0)
		{
			end = cacheEnd - 1;
		}

		if (end == -1)
		{
			return line;
		}
		else
		{
			int fromIndex = start - getCacheStart() + 1;
			int toIndex = end - getCacheStart() + 2;
			if (toIndex > line.size())
				toIndex = line.size();
			return line.subList(fromIndex, toIndex);
		}
	}

	protected String buildDataSQL(int start, int end)
	{
		String dataSql;
		String dynWhere = getSQLWhere();
		StringBuilder sql = new StringBuilder(m_sqlMain);
		if (dynWhere.length() > 0)
			sql.append(dynWhere); // includes first AND

		if (sql.toString().trim().endsWith("WHERE"))
		{
			int index = sql.lastIndexOf(" WHERE");
			sql.delete(index, sql.length());
		}
		if (m_sqlUserOrder != null && m_sqlUserOrder.trim().length() > 0)
			sql.append(m_sqlUserOrder);
		else
			sql.append(m_sqlOrder);
		dataSql = Msg.parseTranslation(Env.getCtx(), sql.toString()); // Variables
		dataSql = MRole.getDefault().addAccessSQL(dataSql, getTableName(), MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);
		if (end > start && m_useDatabasePaging && DB.getDatabase().isPagingSupported())
		{
			dataSql = DB.getDatabase().addPagingSQL(dataSql, getCacheStart(), cacheEnd);
		}
		return dataSql;
	}

    private void addDoubleClickListener() {
		Iterator<?> i = contentPanel.getListenerIterator(Events.ON_DOUBLE_CLICK);
		while (i.hasNext()) {
			if (i.next() == this)
				return;
		}
		contentPanel.addEventListener(Events.ON_DOUBLE_CLICK, this);
		contentPanel.addEventListener(Events.ON_SELECT, this);
	}
    
    protected void insertPagingComponent() {
		contentPanel.getParent().insertBefore(paging, contentPanel.getNextSibling());
	}
    
    public Vector<String> getColumnHeader(ColumnInfo[] p_layout)
    {
        Vector<String> columnHeader = new Vector<String>();
        
        for (ColumnInfo info: p_layout)
        {
             columnHeader.add(info.getColHeader());
        }
        return columnHeader;
    }
	/**
	 * 	Test Row Count
	 *	@return true if display
	 */
	private boolean testCount()
	{
		long start = System.currentTimeMillis();
		String dynWhere = getSQLWhere();
		StringBuffer sql = new StringBuffer (m_sqlCount);
		
		if (dynWhere.length() > 0)
			sql.append(dynWhere);   //  includes first AND
		
		String countSql = Msg.parseTranslation(Env.getCtx(), sql.toString());	//	Variables
		if (countSql.trim().endsWith("WHERE"))
		{
			countSql = countSql.trim();
			countSql = countSql.substring(0, countSql.length() - 5);
		}
		countSql = MRole.getDefault().addAccessSQL(countSql, getTableName(), MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);
		if (log.isLoggable(Level.FINER))
			log.finer(countSql);
		m_count = -1;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(countSql, null);
			setParameters (pstmt, true);
			//initialId = 0; // clear initial query;
			rs = pstmt.executeQuery();
		
			if (rs.next())
				m_count = rs.getInt(1);
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, countSql, e);
			m_count = -2;
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		if (log.isLoggable(Level.FINE))
			log.fine("#" + m_count + " - " + (System.currentTimeMillis() - start) + "ms");

		return true;
	}	//	testCount
			

	/**
	 *	Save Selection	- Called by dispose
	 */
	protected void saveSelection ()
	{
		// Already disposed
		if (contentPanel == null)
			return;

		if (log.isLoggable(Level.CONFIG))
			log.config("OK=" + m_ok);
		// clear prev selected result
		m_results.clear();

		if (!m_ok) // did not press OK
		{
			contentPanel = null;
			this.detach();
			return;
		}

		// Multi Selection
		if (p_multipleSelection)
		{
			m_results.addAll(getSelectedRowKeys());
		}
		else // singleSelection
		{
			Integer data = getSelectedRowKey();
			if (data != null)
				m_results.add(data);
		}

		log.config(getSelectedSQL());

		// Save Settings of detail info screens
		saveSelectionDetail();

	}	//	saveSelection

	/**
	 *  Get the key of currently selected row
	 *  @return selected key
	 */
	protected Integer getSelectedRowKey()
	{
		Integer key = contentPanel.getSelectedRowKey();
		
		return key;        
	}   //  getSelectedRowKey
	
	/**
     *  Get the keys of selected row/s based on layout defined in prepareTable
     *  @return IDs if selection present
     *  @author ashley
     */
    protected ArrayList<Integer> getSelectedRowKeys()
    {
        ArrayList<Integer> selectedDataList = new ArrayList<Integer>();
        
        if (contentPanel.getKeyColumnIndex() == -1)
        {
            return selectedDataList;
        }
        
        if (p_multipleSelection)
        {
        	int[] rows = contentPanel.getSelectedIndices();
            for (int row = 0; row < rows.length; row++)
            {
                Object data = contentPanel.getModel().getValueAt(rows[row], contentPanel.getKeyColumnIndex());
                if (data instanceof IDColumn)
                {
                    IDColumn dataColumn = (IDColumn)data;
                    selectedDataList.add(dataColumn.getRecord_ID());
                }
                else
                {
                    log.severe("For multiple selection, IDColumn should be key column for selection");
                }
            }
        }
        
        if (selectedDataList.size() == 0)
        {
        	int row = contentPanel.getSelectedRow();
    		if (row != -1 && contentPanel.getKeyColumnIndex() != -1)
    		{
    			Object data = contentPanel.getModel().getValueAt(row, contentPanel.getKeyColumnIndex());
    			if (data instanceof IDColumn)
    				selectedDataList.add(((IDColumn)data).getRecord_ID());
    			if (data instanceof Integer)
    				selectedDataList.add((Integer)data);
    		}
        }
      
        return selectedDataList;
    }   //  getSelectedRowKeys

	/**
	 * Get selected Keys as Collection
	 * 
	 * @deprecated use getSaveKeys
	 * @return selected keys (Integers)
	 */
	public Collection<Integer> getSelectedKeysCollection()
	{
		m_ok = true;
		saveSelection();
		if (!m_ok || m_results.size() == 0)
			return null;
		return m_results;
	}
    
	/**
	 * Save selected id, viewID of all process to map viewIDMap to save into
	 * T_Selection
	 */
	public Map<Integer, List<String>> getSaveKeys()
	{
		// clear result from prev time
		m_viewIDMap.clear();

		int[] rows = contentPanel.getSelectedIndices();

		// this flag to just check key column in first record
		boolean isCheckedKeyType = false;

		for (int row = 0; row < rows.length; row++)
		{
			// get key data column
			Object keyData = contentPanel.getModel().getValueAt(rows[row], contentPanel.getKeyColumnIndex());

			// check key data must is IDColumn
			if (!isCheckedKeyType)
			{
				if (keyData instanceof IDColumn)
				{
					isCheckedKeyType = true;
				}
				else
				{
					log.severe("For multiple selection, IDColumn should be key column for selection");
					break;
				}
			}

			IDColumn dataColumn = (IDColumn) keyData;

			if (isHasViewID)
			{
				// have viewID, get it
				List<String> viewIDValueList = new ArrayList<String>();
				String viewIDValue = null;
				for (int viewIDIndex = 0; viewIDIndex < numOfViewID; viewIDIndex++)
				{
					// get row data from model
					@SuppressWarnings("unchecked")
					List<Object> selectedRowData = (List<Object>) contentPanel.getModel().get(rows[row]);
					// view data store at end of data line
					viewIDValue = (String) selectedRowData.get(contentPanel.getLayout().length + viewIDIndex);
					viewIDValueList.add(viewIDValue);
				}

				m_viewIDMap.put(dataColumn.getRecord_ID(), viewIDValueList);
			}
			else
			{
				// hasn't viewID, set viewID value collection is null
				m_viewIDMap.put(dataColumn.getRecord_ID(), null);
			}

		}

		return m_viewIDMap;
	}

	/**
	 * Get selected Keys
	 * 
	 * @return selected keys (Integers)
	 */
	public Object[] getSelectedKeys()
	{
		if (!m_ok || m_results.size() == 0)
			return null;
		return m_results.toArray(new Integer[0]);
	} // getSelectedKeys;

	/**
	 * Get (first) selected Key
	 * 
	 * @return selected key
	 */
	public Object getSelectedKey()
	{
		if (!m_ok || m_results.size() == 0)
			return null;
		return m_results.get(0);
	} // getSelectedKey

	/**
	 *	Is cancelled?
	 *	- if pressed Cancel = true
	 *	- if pressed OK or window closed = false
	 *  @return true if cancelled
	 */
	public boolean isCancelled()
	{
		return m_cancel;
	}	//	isCancelled

	/**
	 *	Get where clause for (first) selected key
	 *  @return WHERE Clause
	 */
	public String getSelectedSQL()
	{
		// No results
		Object[] keys = getSelectedKeys();
		if (keys == null || keys.length == 0)
		{
			log.config("No Results - OK=" + m_ok + ", Cancel=" + m_cancel);
			return "";
		}
		//
		StringBuffer sb = new StringBuffer(getKeyColumn());
		if (keys.length > 1)
			sb.append(" IN (");
		else
			sb.append("=");

		// Add elements
		for (int i = 0; i < keys.length; i++)
		{
			if (getKeyColumn().endsWith("_ID"))
				sb.append(keys[i].toString()).append(",");
			else
				sb.append("'").append(keys[i].toString()).append("',");
		}

		sb.replace(sb.length() - 1, sb.length(), "");
		if (keys.length > 1)
			sb.append(")");
		return sb.toString();
	}	//	getSelectedSQL;

		
		
	/**
	 *  Get Table name Synonym
	 *  @return table name
	 */
	protected String getTableName()
	{
		return p_tableName;
	}   //  getTableName

	/**
	 *  Get Key Column Name
	 *  @return column name
	 */
	protected String getKeyColumn()
	{
		return p_keyColumn;
	}   //  getKeyColumn

	
	public String[] getEvents()
    {
        return InfoPanel.lISTENER_EVENTS;
    }
	/**
	 * enable all control button or disable all rely to selected record 
	 */
	protected void enableButtons (){
		boolean enable = (contentPanel.getSelectedCount() > 0);
		enableButtons(enable);
	}
	
	// Elaine 2008/11/28
	/**
	 * enable or disable all control button
	 *  Enable OK, History, Zoom if row/s selected
     *  ---
     *  Changes: Changed the logic for accommodating multiple selection
     *  @author ashley
	 */
	protected void enableButtons (boolean enable)
	{
		confirmPanel.getOKButton().setEnabled(enable); // red1 allow Process for
														// 1 or more records

		if (hasHistory())
			confirmPanel.getButton(ConfirmPanel.A_HISTORY).setEnabled(enable);
		if (hasZoom())
			confirmPanel.getButton(ConfirmPanel.A_ZOOM)
					.setEnabled(!enable ? enable : (contentPanel.getSelectedCount() == 1)); 
		if (hasProcess())
			confirmPanel.getButton(ConfirmPanel.A_PROCESS).setEnabled(enable);

		for (Button btProcess : btProcessList)
		{
			btProcess.setEnabled(enable);
		}
		if (btCbbProcess != null)
		{
			btCbbProcess.setEnabled(enable);
		}

		if (btMenuProcess != null)
		{
			btMenuProcess.setEnabled(enable);
		}

		if (cbbProcess != null)
		{
			cbbProcess.setEnabled(enable);
		}
	}   //  enableButtons
	//
		
	/**************************************************************************
	 *  Get dynamic WHERE part of SQL
	 *	To be overwritten by concrete classes
	 *  @return WHERE clause
	 */
	protected abstract String getSQLWhere();
      	
	/**
	 *  Set Parameters for Query
	 *	To be overwritten by concrete classes
	 *  @param pstmt statement
	 *  @param forCount for counting records
	 *  @throws SQLException
	 */
	protected abstract void setParameters (PreparedStatement pstmt, boolean forCount) 
		throws SQLException;
    /**
     * notify to search editor of a value change in the selection info
     * @param event event
    *
     */

	protected void showHistory()					{}
	/**
	 *  Has History (false)
	 *	To be overwritten by concrete classes
	 *  @return true if it has history (default false)
	 */
	protected boolean hasHistory()				{return false;}
	/**
	 *  Customize dialog
	 *	To be overwritten by concrete classes
	 */
	protected boolean hasProcess()				{return false;}
	/**
	 *  Customize dialog
	 *	To be overwritten by concrete classes
	 */
	protected void customize()					{}
	/**
	 *  Has Customize (false)
	 *	To be overwritten by concrete classes
	 *  @return true if it has customize (default false)
	 */
	protected boolean hasCustomize()				{return false;}
	/**
	 *  Has Zoom (false)
	 *	To be overwritten by concrete classes
	 *  @return true if it has zoom (default false)
	 */
	protected boolean hasZoom()					{return false;}
	/**
	 *  Save Selection Details
	 *	To be overwritten by concrete classes
	 */
	protected void saveSelectionDetail()          {}

	/**
	 * 	Get Zoom Window
	 *	@param tableName table name
	 *	@param isSOTrx sales trx
	 *	@return AD_Window_ID
	 */
	protected int getAD_Window_ID (String tableName, boolean isSOTrx)
	{
		if (!isSOTrx && m_PO_Window_ID > 0)
			return m_PO_Window_ID;
		if (m_SO_Window_ID > 0)
			return m_SO_Window_ID;
		//
		String sql = "SELECT AD_Window_ID, PO_Window_ID FROM AD_Table WHERE TableName=?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setString(1, tableName);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				m_SO_Window_ID = rs.getInt(1);
				m_PO_Window_ID = rs.getInt(2);
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		if (!isSOTrx && m_PO_Window_ID > 0)
			return m_PO_Window_ID;
		
		return m_SO_Window_ID;
	}	//	getAD_Window_ID
    
	public void onEvent(Event event)
	{
		if (event == null)
			return;

		if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK)))
		{
			onOk();
		}
		else if (event.getTarget() == contentPanel && event.getName().equals(Events.ON_SELECT))
		{
			SelectEvent selectEvent = (SelectEvent) event;

			if (selectEvent.getReference() != null && selectEvent.getReference() instanceof Listitem)
			{
				Listitem m_lastOnSelectItem = (Listitem) selectEvent.getReference();
				m_lastSelectedIndex = m_lastOnSelectItem.getIndex();
			}

		}
		else if (event.getTarget() == contentPanel && event.getName().equals("onAfterRender"))
		{
			// this event selected item from listBox and model is sync.
			enableButtons();
		}
		else if (event.getTarget() == contentPanel && event.getName().equals(Events.ON_DOUBLE_CLICK))
		{

			if (event.getClass().equals(MouseEvent.class))
			{
				return;
			}
			if (!confirmPanel.getButton(ConfirmPanel.A_OK).isEnabled()) 
			{
				return;
			}
			if (contentPanel.isMultiple() && m_lastSelectedIndex >= 0)
			{

				contentPanel.setSelectedIndex(m_lastSelectedIndex);

				// model.clearSelection(); // TODO: Need to implement Clearing
				// Selection with firing event.
//				List<Object> lsSelectedItem = new ArrayList<Object>();
//				lsSelectedItem.add(model.getElementAt(m_lastSelectedIndex));
//				model.setSelection(lsSelectedItem);

				int m_keyColumnIndex = contentPanel.getKeyColumnIndex();
				for (int i = 0; i < contentPanel.getRowCount(); i++)
				{
					// Find the IDColumn Key
					Object data = contentPanel.getModel().getValueAt(i, m_keyColumnIndex);
					if (data instanceof IDColumn)
					{
						IDColumn dataColumn = (IDColumn) data;

						if (i == m_lastSelectedIndex)
						{
							dataColumn.setSelected(true);
						}
						else
						{
							dataColumn.setSelected(false);
						}
					}
				}
			}
			onDoubleClick();
			contentPanel.repaint();
			m_lastSelectedIndex = -1;
		}
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_REFRESH)))
		{
			onUserQuery();
		}
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
		{
			m_cancel = true;
			dispose(false);
		}
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_RESET)))
		{
			resetParameters();
		}
		// Elaine 2008/12/16
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_HISTORY)))
		{
			if (!contentPanel.getChildren().isEmpty() && contentPanel.getSelectedRowKey() != null)
			{
				showHistory();
			}
		}
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CUSTOMIZE)))
		{
			if (!contentPanel.getChildren().isEmpty() && contentPanel.getSelectedRowKey() != null)
			{
				customize();
			}
		}
		//
		else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_ZOOM)))
		{
			if (!contentPanel.getChildren().isEmpty() && contentPanel.getSelectedRowKey() != null)
			{
				zoom();
				if (isLookup())
					this.detach();
			}
		}

		// handle event click into process button start
		else if (ON_RUN_PROCESS.equals(event.getName()))
		{
			// hand echo event after click button process
			runProcess(event.getData());
		}
		else if (Events.ON_CLICK.equals(event.getName()) && event.getTarget() != null
				&& event.getTarget() instanceof Menuitem && event.getTarget().getAttribute(PROCESS_ID_KEY) != null)
		{
			// handle event when click to menu item of info process
			preRunProcess((Integer) event.getTarget().getAttribute(PROCESS_ID_KEY));
		}
		else if (event.getTarget().equals(btCbbProcess))
		{
			// click bt process in case display drop down list
			Comboitem cbbSelectedItem = cbbProcess.getSelectedItem();

			if (cbbSelectedItem == null || cbbSelectedItem.getValue() == null)
			{
				// do nothing when no process is selected
				return;
			}

			MProcess selectedProcess = (MProcess) cbbSelectedItem.getValue();
			preRunProcess(selectedProcess.getAD_Process_ID());
		}
		else if (btProcessList.contains(event.getTarget()))
		{
			// click bt process in case display button list
			Button btProcess = (Button) event.getTarget();
			Integer processId = (Integer) btProcess.getAttribute(PROCESS_ID_KEY);

			preRunProcess(processId);
		}
		else if (event.getTarget() == paging)
		{
			int pgNo = paging.getActivePage();
			if (pageNo != pgNo)
			{
				contentPanel.clearSelection();

				pageNo = pgNo;
				int start = pageNo * PAGE_SIZE;
				int end = start + PAGE_SIZE;
				if (end >= m_count)
					end = m_count - 1;
				
				List<Object> subList = readLine(start, end);
				
				model = new ListModelTable<Object>(subList);
				model.setSorter(this);
				model.addTableModelListener(this);
				contentPanel.setData(model, null);
			}
		}
		else if (event.getName().equals(Events.ON_CHANGE))
		{
		}
		// else if
		// (event.getName().equals(WindowContainer.ON_WINDOW_CONTAINER_SELECTION_CHANGED_EVENT))
		// {
		// if (infoWindow != null)
		// SessionManager.getAppDesktop().updateHelpContext(X_AD_CtxHelp.CTXTYPE_Info,
		// infoWindow.getAD_InfoWindow_ID());
		// else
		// SessionManager.getAppDesktop().updateHelpContext(X_AD_CtxHelp.CTXTYPE_Home,
		// 0);
		// }
		else if (event.getName().equals(Events.ON_CTRL_KEY))
		{
			KeyEvent keyEvent = (KeyEvent) event;
			if (LayoutUtils.isReallyVisible(this))
			{
				this.onCtrlKeyEvent(keyEvent);
			}
		}
		else if (event.getName().equals(Events.ON_OK))
		{// on ok when focus at non parameter component. example grid result
			if (m_lookup && contentPanel.getSelectedIndex() >= 0)
			{
				// do nothing when parameter not change and at window mode, or
				// at dialog mode but select non record
				onOk();
			}
		}
		else if (event.getName().equals(Events.ON_CANCEL)
				|| (event.getTarget().equals(this) && event.getName().equals(Events.ON_CLOSE)))
		{
			m_cancel = true;
			dispose(false);
		}
		// when user push enter keyboard at input parameter field
		else
		{
			// onUserQuery(); // captured now on control key
		}
	} // onEvent

	public static final int	VK_ENTER	= '\r';
	public static final int	VK_ESCAPE	= 0x1B;

	public void onCtrlKeyEvent(KeyEvent keyEvent)
	{
		if (keyEvent.isAltKey() && !keyEvent.isCtrlKey() && keyEvent.isShiftKey())
		{ // Shift+Alt
			if (keyEvent.getKeyCode() == KeyEvent.DOWN)
			{ // Shift+Alt+Down
				// navigate to results
				if (contentPanel.getRowCount() > 0)
				{
					contentPanel.setFocus(true);
					contentPanel.setSelectedIndex(0);
				}
			}
		}
		else if (keyEvent.getKeyCode() == VK_ENTER)
		{ // Enter
			// do nothing, let on_ok at infoWindo do, at this is too soon to get
			// value from control, it's not bind
		}
	}

	/**
	 * Call query when user click to query button enter in parameter field
	 */
	public void onUserQuery()
	{
		if (validateParameters())
		{
			showBusyDialog();
			isQueryByUser = true;
			Clients.response(new AuEcho(this, "onQueryCallback", null));
		}
	}

	/**
	 * validate parameter before run query
	 * 
	 * @return
	 */
	public boolean validateParameters()
	{
		return true;
	}

	/**
	 * Call after load parameter panel to set init value can call when reset
	 * parameter implement this method at inheritance class with each parameter,
	 * remember call Env.setContext to set new value to env
	 */
	protected void initParameters()
	{

	}

	/**
	 * Reset parameter to default value or to empty value? implement at
	 * inheritance class when reset parameter maybe need init again parameter,
	 * reset again default value
	 */
	protected void resetParameters()
	{
	}

	void preRunProcess(Integer processId)
	{
		// disable all control button when run process
		enableButtons(false);
		// call run process in next request to disable all button control
		Events.echoEvent(ON_RUN_PROCESS, this, processId.toString());
	}

	/**
	 * Run a process. show process dialog, before start process, save id of
	 * record selected after run process, show message report result
	 * 
	 * @param processIdObj
	 */
	protected void runProcess(Object processIdObj)
	{
		final Integer processId = Integer.parseInt(processIdObj.toString());
		final MProcess m_process = MProcess.get(Env.getCtx(), processId);
		final ProcessInfo m_pi = new ProcessInfo(m_process.getName(), processId);
		m_pi.setAD_User_ID(Env.getAD_User_ID(Env.getCtx()));
		m_pi.setAD_Client_ID(Env.getAD_Client_ID(Env.getCtx()));

		MPInstance instance = new MPInstance(Env.getCtx(), processId, 0);
		instance.saveEx();
		final int pInstanceID = instance.getAD_PInstance_ID();
		// Execute Process
		m_pi.setAD_PInstance_ID(pInstanceID);

		// to let process end with message and requery
		WProcessCtl.process(p_WindowNo, m_pi, (Trx) null, new EventListener() {

			@Override
			public void onEvent(Event event) throws Exception
			{
				ProcessModalDialog processModalDialog = (ProcessModalDialog) event.getTarget();
				if (ProcessModalDialog.ON_BEFORE_RUN_PROCESS.equals(event.getName()))
				{
					// store in T_Selection table selected rows for Execute
					// Process that retrieves from T_Selection in code.
					DB.createT_Selection(pInstanceID, getSaveKeys(),
							getProcessIndex(processModalDialog.getAD_Process_ID()), null);
					saveResultSelection();
					 createT_Selection_InfoWindow(pInstanceID);
				}
				else if (ProcessModalDialog.ON_WINDOW_CLOSE.equals(event.getName()))
				{
					if (processModalDialog.isCancel())
					{
						m_results.clear();
						// enable/disable control button rely selected record
						// status
						enableButtons();
					}
					else if (m_pi.isError())
					{
						FDialog.error(p_WindowNo, m_pi.getSummary());
						// enable/disable control button rely selected record
						// status
						enableButtons();
					}
					else if (!m_pi.isError())
					{
						// when success, show process info and it's summary
						FDialog.info(p_WindowNo, null, m_pi.getTitle() + ":" + Msg.getMsg(getContext(), "Success")
								+ "</br>" + m_pi.getSummary());
						Clients.response(new AuEcho(InfoPanel.this, "onQueryCallback", null));
					}
				}
			}
		});
	}

	/**
	 * save result values
	 */
	protected void saveResultSelection()
	{
		int m_keyColumnIndex = contentPanel.getKeyColumnIndex();
		if (m_keyColumnIndex == -1)
		{
			return;
		}

		int rows = contentPanel.getRowCount();
		m_values = new LinkedHashMap<Integer, LinkedHashMap<String, Object>>();
		for (int row = 0; row < rows; row++)
		{
			// Find the IDColumn Key
			Object data = contentPanel.getModel().getValueAt(row, m_keyColumnIndex);
			if (data instanceof IDColumn)
			{
				IDColumn dataColumn = (IDColumn) data;
				if (dataColumn.isSelected())
				{
					LinkedHashMap<String, Object> values = new LinkedHashMap<String, Object>();
					for (int col = 0; col < p_layout.length; col++)
					{
						if (!p_layout[col].isReadOnly())
						{
							values.put(p_layout[col].getColHeader(), contentPanel.getModel().getValueAt(row, col));
						}
					}
					if (values.size() > 0)
						m_values.put(dataColumn.getRecord_ID(), values);
				}
			}
		}
	} // saveResultSelection

	/**
	 * Insert result values
	 * 
	 * @param AD_PInstance_ID
	 */
	public void createT_Selection_InfoWindow(int AD_PInstance_ID)
	{
		String insert = "INSERT INTO T_Selection_InfoWindow (AD_PINSTANCE_ID, T_SELECTION_ID, COLUMNNAME, VALUE_STRING, VALUE_NUMBER, VALUE_DATE)	VALUES(?,?,?,?,?,?)";

		for (Entry<Integer, LinkedHashMap<String, Object>> records : m_values.entrySet())
		{
			// set Record ID
			LinkedHashMap<String, Object> fields = records.getValue();
			for (Entry<String, Object> field : fields.entrySet())
			{
				List<Object> parameters = new ArrayList<Object>();

				parameters.add(AD_PInstance_ID);
				parameters.add(records.getKey());
				parameters.add(field.getKey());

				Object data = field.getValue();
				// set Values
				if (data instanceof IDColumn)
				{
					IDColumn id = (IDColumn) data;
					parameters.add(null);
					parameters.add(id.getRecord_ID());
					parameters.add(null);
				}
				else if (data instanceof String)
				{
					parameters.add(data);
					parameters.add(null);
					parameters.add(null);
				}
				else if (data instanceof BigDecimal || data instanceof Double)
				{
					parameters.add(null);
					if (data instanceof Double)
					{
						BigDecimal value = BigDecimal.valueOf((Double) data);
						parameters.add(value);
					}
					else
						parameters.add(data);
					parameters.add(null);
				}
				else if (data instanceof Integer)
				{
					parameters.add(null);
					parameters.add((Integer) data);
					parameters.add(null);
				}
				else if (data instanceof Timestamp || data instanceof Date)
				{
					parameters.add(null);
					parameters.add(null);
					if (data instanceof Date)
					{
						Timestamp value = new Timestamp(((Date) data).getTime());
						parameters.add(value);
					}
					else
						parameters.add(data);
				}
				else
				{
					parameters.add(data);
					parameters.add(null);
					parameters.add(null);
				}

				DB.executeUpdateEx(insert, parameters.toArray(), null);
			}
		}
	} // createT_Selection_InfoWindow

	/**
	 * @param AD_PInstance_ID
	 * @param alias
	 * @param recordId
	 * @param trxName
	 * @return
	 */
	static public LinkedHashMap<String, Object> getInfoWindowValues(int AD_PInstance_ID, String alias, int recordId,
			String trxName)
	{
		LinkedHashMap<String, Object> values = new LinkedHashMap<String, Object>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Object> parameters = new ArrayList<Object>();
		try
		{
			StringBuilder sql = new StringBuilder(
					"SELECT ColumnName, Value_String, Value_Date, Value_Number FROM T_Selection_InfoWindow "
							+ "WHERE  AD_PInstance_ID = ? AND T_Selection_ID = ? ");
			parameters.add(AD_PInstance_ID);
			parameters.add(recordId);

			if (alias != null)
			{
				sql.append("AND ColumnName LIKE ?");
				parameters.add(alias.toUpperCase() + "_%");
			}
			
			pstmt = DB.prepareStatement(sql.toString(), trxName);
			DB.setParameters(pstmt, parameters);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				String columnName = rs.getString(1);
				String valueString = rs.getString(2);
				Timestamp valueDate = rs.getTimestamp(3);
				BigDecimal valueBigDecimal = rs.getBigDecimal(4);
				if (valueString != null)
				{
					values.put(columnName, valueString);
					continue;
				}
				else if (valueDate != null)
				{
					values.put(columnName, valueDate);
					continue;
				}
				else if (valueBigDecimal != null)
				{
					values.put(columnName, valueBigDecimal);
					continue;
				}
			}
		}
		catch (SQLException ex)
		{
			throw new DBException(ex);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return values;
	} // getInfoWindowValues
	
	/**
	 * Get index of infoProcess have processId
	 * 
	 * @param processId
	 * @return
	 */
	protected int getProcessIndex(int processId)
	{
		int index = 0;
		for (int i = 0; i < infoProcessList.length; i++)
		{
			if (infoProcessList[i].getAD_Process_ID() == processId)
			{
				return index;
			}
			// just increase index when process is have ViewID column
			if (infoProcessList[i].getAD_InfoColumn_ID() > 0)
				index++;
		}
		return -1;
	}
    
	private void showBusyDialog() {
		if(progressWindow == null)
		{
			progressWindow = new BusyDialog();
			progressWindow.setPage(this.getPage());
			progressWindow.doHighlighted();
		}
	}

	private void hideBusyDialog()
	{
		if (progressWindow != null)
		{
			progressWindow.dispose();
			progressWindow = null;
		}
	}

	public void onQueryCallback(Event event)
	{
		try
		{
			Listhead listHead = contentPanel.getListHead();
			if (listHead != null)
			{
				List<?> headers = listHead.getChildren();
				for (Object obj : headers)
				{
					Listheader header = (Listheader) obj;
					header.setSortDirection("natural");
				}
			}
			
			m_sqlUserOrder = "";
			executeQuery();
			renderItems();

			if (event != null && event.getData() != null)
			{
				m_results.clear();
			}
			// just evaluate display logic of process button when requery by use
			// click requery button
			if (isQueryByUser)
			{
				bindInfoProcess();
			}
		}
		finally
		{
			isQueryByUser = false;
			hideBusyDialog();
		}
	}

	/**
	 * evaluate display logic of button process empty method. implement at child
	 * class extend
	 */
	protected void bindInfoProcess()
	{}

	protected void onOk()
	{
		if (!contentPanel.getChildren().isEmpty() && contentPanel.getSelectedRowKey() != null)
		{
			dispose(true);
		}
	}
    
	private void onDoubleClick()
	{
		if (isLookup())
		{
			dispose(true);
		}
		else
		{
			zoom();
		}

	}

    public void tableChanged(WTableModelEvent event)
    {
    	enableButtons();
    }
    
    public void zoom()
    {
    	Integer recordId = contentPanel.getSelectedRowKey();
    	// prevent NPE when double click is raise but no record is selected
    	if (recordId == null)
    		return;
    	
    	if (listeners != null && listeners.size() > 0)
    	{
	        ValueChangeEvent event = new ValueChangeEvent(this,"zoom",
	                   contentPanel.getSelectedRowKey(),contentPanel.getSelectedRowKey());
	        fireValueChange(event);
    	}
    	else
    	{
    		int AD_Table_ID = MTable.getTable_ID(p_tableName);
    		if (AD_Table_ID <= 0)
    		{
    			if (p_keyColumn.endsWith("_ID")) 
    			{
    				AD_Table_ID = MTable.getTable_ID(p_keyColumn.substring(0, p_keyColumn.length() - 3));
    			}
    		}
    		if (AD_Table_ID > 0)
    			AEnv.zoom(AD_Table_ID, recordId);
    	}
    }
    
    public void addValueChangeListener(ValueChangeListener listener)
    {
        if (listener == null)
        {
            return;
        }
        
        listeners.add(listener);
    }
        
    public void fireValueChange(ValueChangeEvent event)
    {
        for (ValueChangeListener listener : listeners)
        {
           listener.valueChange(event);
        }
    }
    /**
     *  Dispose and save Selection
     *  @param ok OK pressed
     */
    public void dispose(boolean ok)
    {
        log.config("OK=" + ok);
        m_ok = ok;

        //  End Worker
        if (isLookup())
        {
        	saveSelection();
        }
        if (Window.MODE_EMBEDDED.equals(getAttribute(Window.MODE_KEY)))
        	SessionManager.getAppDesktop().closeActiveWindow();
        else
	        this.detach();
    }   //  dispose
        
	@SuppressWarnings("rawtypes")
	public void sort(Comparator cmpr, boolean ascending) {
		WListItemRenderer.ColumnComparator lsc = (WListItemRenderer.ColumnComparator) cmpr;
		if (m_useDatabasePaging)
		{
			int col = lsc.getColumnIndex();
			String colsql = p_layout[col].getColSQL().trim();
			int lastSpaceIdx = colsql.lastIndexOf(" ");
			if (lastSpaceIdx > 0)
			{
				String tmp = colsql.substring(0, lastSpaceIdx).trim();
				char last = tmp.charAt(tmp.length() - 1);
				if (tmp.toLowerCase().endsWith("as"))
				{
					colsql = colsql.substring(lastSpaceIdx).trim();
				}
				else if (!(last == '*' || last == '-' || last == '+' || last == '/' || last == '>' || last == '<' || last == '='))
				{
					tmp = colsql.substring(lastSpaceIdx).trim();
					if (tmp.startsWith("\"") && tmp.endsWith("\""))
					{
						colsql = colsql.substring(lastSpaceIdx).trim();
					}
					else
					{
						boolean hasAlias = true;
						for(int i = 0; i < tmp.length(); i++)
						{
							char c = tmp.charAt(i);
							if (Character.isLetterOrDigit(c))
							{
								continue;
							}
							else
							{
								hasAlias = false;
								break;
							}
						}
						if (hasAlias)
						{
							colsql = colsql.substring(lastSpaceIdx).trim();
						}
					}
				}
			}
			m_sqlUserOrder = " ORDER BY " + colsql;
			if (!ascending)
				m_sqlUserOrder += " DESC ";
			executeQuery();
			renderItems();
		}
		else
		{
			Collections.sort(line, lsc);
			renderItems();
		}
	}

    public boolean isLookup()
    {
    	return m_lookup;
    }

    public void scrollToSelectedRow()
    {
    	if (contentPanel != null && contentPanel.getSelectedIndex() >= 0) {
    		Listitem selected = contentPanel.getItemAtIndex(contentPanel.getSelectedIndex());
    		if (selected != null) {
    			selected.focus();
    		}
    	}
    }
    
	public void setLine(List<Object> line) {
		this.line = line;
	}
	
	public int getWindowNo() {
		return p_WindowNo;
	}
	
	public int getRowCount() {
		return contentPanel.getRowCount();
	}
	
	/**
	 * @return the cacheStart
	 */
	protected int getCacheStart() {
		return cacheStart;
	}

 	/**
	 * @return the cacheEnd
	 */
	protected int getCacheEnd() {
		return cacheEnd;
	}
	
	protected boolean isUseDatabasePaging() {
		return m_useDatabasePaging;
	}
	
	@Override
	public void onPageAttached(Page newpage, Page oldpage)
	{
		super.onPageAttached(newpage, oldpage);
		SessionManager.getSessionApplication().getKeylistener().addEventListener(Events.ON_CTRL_KEY, this);
	}

	@Override
	public void onPageDetached(Page page)
	{
		super.onPageDetached(page);
		SessionManager.getSessionApplication().getKeylistener().removeEventListener(Events.ON_CTRL_KEY, this);
	}

}	//	Info
