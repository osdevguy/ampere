/******************************************************************************
 * Product: Posterita Ajax UI 												  *
 * Copyright (C) 2007 Posterita Ltd.  All Rights Reserved.                    *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Posterita Ltd., 3, Draper Avenue, Quatre Bornes, Mauritius                 *
 * or via info@posterita.org or http://www.posterita.org/                     *
 *****************************************************************************/

package org.adempiere.webui.panel;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.theme.ThemeManager;
import org.adempiere.webui.window.AboutWindow;
import org.compiere.util.Ini;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.East;
import org.zkoss.zkex.zul.West;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vbox;

/**
 *
 * @author  <a href="mailto:agramdass@gmail.com">Ashley G Ramdass</a>
 * @author  <a href="mailto:hengsin@gmail.com">Low Heng Sin</a>
 * @date    Mar 2, 2007
 * @date    July 7, 2007
 * @version $Revision: 0.20 $
 */

public class HeaderPanel extends Panel implements EventListener
{
	private static final long serialVersionUID = -2351317624519209484L;

	private Image image = new Image();

	private boolean hasHeadline = false;
	
    public HeaderPanel()
    {
        super();
        init();
    }

    private void init()
    {
    	LayoutUtils.addSclass("desktop-header", this);

    	UserPanel userPanel = new UserPanel();

    	image.setSrc(ThemeManager.getSmallLogo());
    	image.addEventListener(Events.ON_CLICK, this);
    	image.setStyle("cursor: pointer;max-height: 32px; min-width: 100px;");

    	String bg = "background-color: transparent; border: none;";
    	Borderlayout layout = new Borderlayout();
    	LayoutUtils.addSclass("desktop-header", layout);
    	layout.setParent(this);
    	West west = new West();
    	west.setFlex(true);
    	west.setParent(layout);
        
    	Vbox vb = new Vbox();
        vb.setParent(west);
        vb.setHeight("100%");
        vb.setPack("center");
        vb.setAlign("left");

        image.setParent(vb);
    	

        
        hasHeadline = Ini.isHeadlineSet();
    	if (hasHeadline) {
    		Center center = new Center();
    		center.setParent(layout);
    		center.setFlex(true);
    		Vbox vb2 = new Vbox();
    		vb2.setParent(center);
    		vb2.setHeight("100%");
    		vb2.setPack("center");
    		vb2.setAlign("center");
    		bg = "background-color: " + Ini.getBGColor() + "; border:none;";
    		Label text = null;
    		text = new Label();
    		text.setValue(Ini.getHeadLineText());
    		text.setWidth("100%");
    		text.setHeight("100%");
    		String style = "font-size: larger; font-weight: bolder; color:" + Ini.getFGColor() + ";";
    		text.setStyle(style);
    		text.setParent(vb2);
    		center.setStyle(bg);
    		LayoutUtils.addSclass("desktop-header-left", center);
    	}
    	
    	west.setStyle(bg);

    	LayoutUtils.addSclass("desktop-header-left", west);
    	//the following doesn't work when declare as part of the header-left style
    	west.setStyle(bg);

    	// Elaine 2009/03/02
    	East east = new East();
    	east.setParent(layout);
    	userPanel.setParent(east);
    	userPanel.setWidth("100%");
    	userPanel.setHeight("100%");
    	userPanel.setStyle("position: absolute");
    	east.setWidth("400px");
    	LayoutUtils.addSclass("desktop-header-right", east);
    	//the following doesn't work when declare as part of the header-right style
    	east.setStyle(bg);
    }

	public void onEvent(Event event) throws Exception {
		if (Events.ON_CLICK.equals(event.getName())) {
			if(event.getTarget() == image)
			{
				AboutWindow w = new AboutWindow();
				w.setPage(this.getPage());
				w.doModal();
			}
		}

	}
}
