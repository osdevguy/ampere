/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2007 Adempiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *
 * Copyright (C) 2007 Low Heng Sin hengsin@avantz.com
 * _____________________________________________
 *****************************************************************************/
package org.adempiere.webui.window;

import java.io.FileInputStream;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.session.SessionManager;
import org.compiere.print.ReportEngine;
import org.compiere.print.ReportViewerProvider;
import org.compiere.util.CLogger;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;

/**
 * 
 * @author Low Heng Sin
 *
 */
public class ZkReportViewerProvider implements ReportViewerProvider {

	protected static CLogger	log = CLogger.getCLogger (ZkReportViewerProvider.class);
	private Desktop m_desktop = null;
	
	public void openViewer(ReportEngine report) {

		m_desktop = AEnv.getDesktop();

		if (!SessionManager.grantAccess(m_desktop)) {
			return;
		}
			
		
		Window viewer = new ZkReportViewer(report, report.getPrintInfo().getName(), m_desktop);
		
		viewer.setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
		viewer.setAttribute(Window.INSERT_POSITION_KEY, Window.INSERT_NEXT);

		if (!SessionManager.grantAccess(m_desktop)) {
			return;
		}
		SessionManager.getAppDesktop(m_desktop).showWindow(viewer);
		((ZkReportViewer)viewer).updateTitle();
		SessionManager.releaseDesktop(m_desktop);
		
		
	}
	

	@Override
	public void openViewer(String title, FileInputStream input) {
		boolean inUIThread = Executions.getCurrent() != null;
		boolean desktopActivated = false;

		try {
			if (!inUIThread) {
				m_desktop = AEnv.getDesktop();

				if (m_desktop == null) {
					log.warning("desktop is null");
					return;
				}
				if (!SessionManager.grantAccess(m_desktop)) {
					return;
				}
				desktopActivated = true;
			}
			Window win = new SimplePDFViewer(title, input);
			SessionManager.getAppDesktop().showWindow(win, "center");
		} catch (Exception e) {
			log.severe(e.getMessage());
		} finally {
			if (!inUIThread && desktopActivated) {
				SessionManager.releaseDesktop(m_desktop);
			}
		}
	}
	
	
}