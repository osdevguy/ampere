package org.adempiere.webui.window;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.StatusBarPanel;
import org.compiere.model.MBPartner;
import org.compiere.model.MCurrency;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;
import org.zkoss.zul.Div;
import org.zkoss.zul.Space;

/**
 * @author Nikunj Panelia
 */
public class WDistributeCost extends Window implements EventListener, WTableModelListener
{

	private static final long	serialVersionUID	= -7297481864984135973L;
	private static final String	msg					= "Applied Amt/Total Amt";
	private static final String	msgPer				= "Applied Per/Total Per";
	private static final String	AMOUNT				= "A";
	private static final String	PERCENTAGE			= "P";

	private static CLogger		log					= CLogger.getCLogger(WDistributeCost.class);
	private MInvoiceLine		invoiceLine			= null;
	private MInvoice			invoice				= null;
	private Label				lamtPer				= new Label("Amount / Percentage(%) :");
	private Listbox				amtPer				= new Listbox();
	private Label				ldocType			= new Label("Document Type :");
	private Listbox				docType				= new Listbox();
	private WListbox			miniTable			= new WListbox();
	private Borderlayout		mainPanel			= new Borderlayout();
	private Grid				northPanel			= GridFactory.newGridLayout();
	private ConfirmPanel		confPanel			= new ConfirmPanel(true);
	private StatusBarPanel		statusBar			= new StatusBarPanel();

	public WDistributeCost(int record_ID)
	{
		invoiceLine = new MInvoiceLine(Env.getCtx(), record_ID, null);
		invoice = invoiceLine.getParent();
		init();
		dyInit();
		AEnv.showWindow(this);

	}

	private void init()
	{
		this.setWidth("700px");
		this.setBorder("normal");
		this.setHeight("500px");
		this.setClosable(true);
		this.setTitle("Distribute Cost");
		this.setAttribute("mode", "modal");
		this.setSizable(true);
		this.appendChild(mainPanel);

		North north = new North();
		north.appendChild(northPanel);

		Rows rows = northPanel.newRows();

		Row row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(lamtPer.rightAlign());
		row.appendChild(new Space());
		row.appendChild(amtPer);
		amtPer.setMold("select");
		amtPer.addEventListener(Events.ON_SELECT, this);

		row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(ldocType.rightAlign());
		row.appendChild(new Space());
		row.appendChild(docType);

		mainPanel.appendChild(north);

		Center center = new Center();
		center.appendChild(miniTable);
		mainPanel.appendChild(center);

		South south = new South();

		Div vbox = new Div();
		vbox.setStyle("width: 100%; text-align: right;");
		south.appendChild(vbox);

		vbox.appendChild(statusBar);
		vbox.appendChild(confPanel);

		mainPanel.appendChild(south);
		confPanel.addActionListener(this);
		statusBar.setStatusLine(msg + " : 0/" + invoiceLine.getLineNetAmt());
	}

	private void dyInit()
	{
		// init amtPer
		amtPer.removeAllItems();
		amtPer.appendItem("Amount", AMOUNT);
		amtPer.appendItem("Percentage(%)", PERCENTAGE);

		// init docType
		docType.removeAllItems();
		StringBuffer sql = new StringBuffer("Select C_DocType_Id,name from C_DocType "
				+ "where DocBaseType IN('ARI', 'API','ARC','APC') and IsSOTrx='Y' and AD_Client_ID="
				+ invoiceLine.getAD_Client_ID());
		for (KeyNamePair kn : DB.getKeyNamePairs(sql.toString(), false))
		{
			docType.addItem(kn);
		}
		docType.setMold("select");
		docType.setSelectedIndex(0);

		loadBPartner();
	}

	ArrayList<Integer>	bPartnerList	= new ArrayList<Integer>();

	private void loadBPartner()
	{
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuffer sql = new StringBuffer();
		sql.append("(SELECT c_bpartner_ID,name,AD_OrgBP_ID from C_BPartner WHERE AD_Client_ID=? and (AD_OrgBP_ID > 0  and  AD_OrgBP_ID <> ? ))");

		PreparedStatement pstm = DB.prepareStatement(sql.toString(), null);
		ResultSet rs = null;

		try
		{
			pstm.setInt(1, invoiceLine.getAD_Client_ID());
			pstm.setInt(2, invoice.getAD_Org_ID());

			rs = pstm.executeQuery();
			while (rs.next())
			{
				bPartnerList.add(rs.getInt(1));
				Vector<Object> line = new Vector<Object>();
				line.add(rs.getString(2));
				line.add(BigDecimal.ZERO);
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, e.getLocalizedMessage());
		}
		finally
		{
			DB.close(rs, pstm);
		}

		loadTable(data);
	}

	private void loadTable(Vector<Vector<Object>> data)
	{
		Vector<String> columnNames = null;
		miniTable.clear();
		columnNames = new Vector<String>();
		columnNames.add("Business Partner");
		columnNames.add("Amount");

		ListModelTable model = new ListModelTable(data);

		miniTable.setData(model, columnNames);
		model.addTableModelListener(this);
		miniTable.setColumnClass(0, String.class, true);
		miniTable.setColumnClass(1, BigDecimal.class, false);

		miniTable.autoSize();
		miniTable.addActionListener(this);
		miniTable.repaint();
	}

	@Override
	public void onEvent(Event e) throws Exception
	{

		if (e.getTarget() == confPanel.getButton(ConfirmPanel.A_OK))
		{
			createSO();
			onClose();
		}
		else if (e.getTarget() == confPanel.getButton(ConfirmPanel.A_CANCEL))
		{
			onClose();
		}
		else if (e.getTarget() == amtPer)
		{
			updateStatus();
		}
	}

	private void createSO()
	{

		for (int i = 0; i < bPartnerList.size(); i++)
		{
			BigDecimal lineAmt = (BigDecimal) miniTable.getValueAt(i, 1);
			if (lineAmt == null || (lineAmt).compareTo(BigDecimal.ZERO) == 0)
				continue;

			MCurrency cur = MCurrency.get(Env.getCtx(), invoice.getC_Currency_ID());
			if (amtPer.getSelectedItem().getValue().equals(PERCENTAGE))
				lineAmt = ((lineAmt).multiply(invoiceLine.getLineNetAmt())).divide(Env.ONEHUNDRED,
						cur.getStdPrecision());
			MInvoice salesInvoice = new MInvoice(Env.getCtx(), 0, null);

			salesInvoice.setAD_Client_ID(invoice.getAD_Client_ID());
			salesInvoice.setAD_Org_ID(invoice.getAD_Org_ID());

			salesInvoice.setDateAcct(invoice.getDateAcct());
			salesInvoice.setDateInvoiced(invoice.getDateAcct());
			salesInvoice.setM_PriceList_ID(invoice.getM_PriceList_ID());
			salesInvoice.setSalesRep_ID(invoice.getSalesRep_ID());
			salesInvoice.setC_ConversionType_ID(invoice.getC_ConversionType_ID());

			MBPartner bp = new MBPartner(Env.getCtx(), invoice.getC_BPartner_ID(), null);
			salesInvoice.setDescription((invoice.getDescription() == null ? "" : invoice.getDescription() + "-")
					+ bp.getName() + "-" + invoice.getDocumentNo());

			salesInvoice.setC_BPartner_ID(bPartnerList.get(i));

			salesInvoice.setC_DocTypeTarget_ID((Integer) docType.getSelectedItem().getValue());
			salesInvoice.setC_DocType_ID((Integer) docType.getSelectedItem().getValue());
			salesInvoice.save();

			MInvoiceLine line = new MInvoiceLine(salesInvoice);
			line.setM_Product_ID(invoiceLine.getM_Product_ID());
			line.setDescription(invoiceLine.getDescription());
			line.setC_Charge_ID(invoiceLine.getC_Charge_ID());
			line.setQtyInvoiced(BigDecimal.ONE);

			line.setPriceActual(lineAmt);
			line.setLineNetAmt();
			line.setPrice(lineAmt);

			line.save();
			salesInvoice.setM_PriceList_ID(invoice.getM_PriceList_ID());
			salesInvoice.save();

			AEnv.zoom(318, salesInvoice.get_ID());

		}

	}

	@Override
	public void tableChanged(WTableModelEvent event)
	{
		if ((event.getType() == WTableModelEvent.CONTENTS_CHANGED))
		{
			updateStatus();
		}
	}

	private void updateStatus()
	{

		BigDecimal statusAmt = BigDecimal.ZERO;
		for (int i = 0; i < bPartnerList.size(); i++)
		{
			statusAmt = statusAmt.add((BigDecimal) miniTable.getValueAt(i, 1));
		}

		if (amtPer.getSelectedItem().getValue().equals(AMOUNT))
		{
			statusBar.setStatusLine(msg + " : " + statusAmt + "/" + invoiceLine.getLineNetAmt());
		}
		else
		{
			statusBar.setStatusLine(msgPer + " : " + statusAmt + "/" + 100);
		}
	}
}