package org.adempiere.webui.window;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Window;
import org.compiere.model.MRole;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;
import org.zkoss.zul.Space;

public class CopyTimeSheetFrom extends Window  implements EventListener
{	

	private static final long serialVersionUID = 1L;
	private int author_Id = 0;
	private int project_Id = 0;
	private Label  labelAuthor=new  Label("Author");
	private Listbox listBoxAuthor = ListboxFactory.newDropdownListbox();
	private Label  labelProject=new  Label("project");
	private Listbox listBoxProject = ListboxFactory.newDropdownListbox();
	private Grid selNorthPanel = GridFactory.newGridLayout();
	private ConfirmPanel confirmPanel = new ConfirmPanel(true);
	
	
	public CopyTimeSheetFrom()
	{
		
		zkInit();
		Borderlayout contentPane = new Borderlayout();
		this.appendChild(contentPane);
		contentPane.setWidth("99%");
		contentPane.setHeight("100%");

		North north = new North();
		contentPane.appendChild(north);
		north.appendChild(selNorthPanel);


		South south = new South();
		contentPane.appendChild(south);		
		south.appendChild(confirmPanel);
		confirmPanel.addActionListener(this);
		this.setAttribute("mode", "modal");
		this.setTitle("Copy From");
		this.setBorder("normal");
		this.setWidth("700px");
		this.setHeight("150px");
		this.setSizable(true);
		
		AEnv.showWindow(this);
	}
	private void zkInit()
	{
		String SQL = MRole.getDefault().addAccessSQL("SELECT s_resource_id, Name FROM s_resource WHERE IsActive='Y' and AD_Client_ID="+Env.getAD_Client_ID(Env.getCtx()),
						"s_resource", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)+ " ORDER BY Name";
		for (KeyNamePair kn : DB.getKeyNamePairs(SQL, true)) 
		{
			listBoxAuthor.addItem(kn);
			if (kn.getKey() == Env.getAD_User_ID(Env.getCtx()))
				listBoxAuthor.setSelectedKeyNamePair(kn);
		}
		listBoxAuthor.setMold("select");
		listBoxAuthor.addEventListener(Events.ON_SELECT, this);
		
		Row row = selNorthPanel.newRows().newRow();

		row.appendChild(labelAuthor.rightAlign());
		row.appendChild(listBoxAuthor);
		row.appendChild(new Space());
		row.appendChild(labelProject.rightAlign());
		row.appendChild(listBoxProject);
		listBoxProject.addEventListener(Events.ON_SELECT, this);
	
	}
	

	@Override
	public void onEvent(Event event) throws Exception
	{
		if(event.getTarget() == listBoxAuthor ) 
		{
			author_Id=(Integer)listBoxAuthor.getSelectedItem().getValue();
			renderProjects();
		}
		else if(event.getTarget() == listBoxProject ) 
		{
			project_Id=(Integer)listBoxProject.getSelectedItem().getValue();
		}
		if(event.getTarget() == confirmPanel.getButton(ConfirmPanel.A_OK))
		{			
			
			dispose();
		}
		else if(event.getTarget() == confirmPanel.getButton(ConfirmPanel.A_CANCEL))
		{
			author_Id  = 0;
			project_Id = 0;
			dispose();
		}
		
	}
	
	private void renderProjects()
	{
		listBoxProject.removeAllItems();
		String SQL = "SELECT  DISTINCT p.C_Project_ID, p.Name FROM C_Project p " +
				" join c_projectphase ph on p.c_project_id=ph.c_project_id " +
				" join c_projecttask t on ph.c_projectphase_id=t.c_projectphase_id " +
				" WHERE p.projectlinelevel = 'T' and p.IsActive='Y' " 
				+ getSQLWhere() +" and p.AD_Client_ID="+Env.getAD_Client_ID(Env.getCtx())+ " ORDER BY Name";
		for (KeyNamePair kn : DB.getKeyNamePairs(SQL, true)) 
			{
				listBoxProject.addItem(kn);
				if (kn.getKey() == Env.getAD_User_ID(Env.getCtx()))
					listBoxProject.setSelectedKeyNamePair(kn);
			}
		listBoxProject.setMold("select");
	
		
	}
	protected String getSQLWhere()
	{
		StringBuffer where = new StringBuffer();
		if (listBoxAuthor.getSelectedItem().toString() != null)
			where.append(" AND p.C_Project_id in (Select C_project_id from c_projectresource where s_resource_id="+listBoxAuthor.getSelectedItem().getValue()+")");

		return where.toString();

	}
	
	public int getauthorID()
	{
		return author_Id;
	}
	public int getProjectID()
	{
		return project_Id;
	}
	
}
