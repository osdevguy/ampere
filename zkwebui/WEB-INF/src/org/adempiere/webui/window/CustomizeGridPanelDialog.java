package org.adempiere.webui.window;

import java.util.ArrayList;
import java.util.Map;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.GridPanel;
import org.adempiere.webui.component.QuickGridPanel;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.panel.CustomizeGridPanel;
import org.adempiere.webui.panel.QuickCustomizeGridPanel;
import org.compiere.util.Env;
import org.compiere.util.Msg;

public class CustomizeGridPanelDialog extends Window
{
	/**
	 * 
	 */
	private static final long		serialVersionUID	= 9077416157436132855L;

	public static final String		CUSTOMIZE_GRID		= "onCustomizeGrid";

	private CustomizeGridPanel		customizePanel;

	private QuickCustomizeGridPanel	quickCustomizePanel;

	private boolean					isQuickForm			= false;

	/**
	 * Standard Constructor
	 * 
	 * @param WindowNo window no
	 * @param AD_Tab_ID tab
	 * @param AD_User_ID user
	 * @param columnsWidth
	 * @param gridFieldIds
	 * @param isQuickForm
	 */
	public CustomizeGridPanelDialog(int windowNo, int AD_Tab_ID, int AD_User_ID, Map<Integer, String> columnsWidth,
			ArrayList<Integer> gridFieldIds, boolean isQuickForm)
	{
		setClosable(true);
		setTitle(Msg.getMsg(Env.getCtx(), "Customize"));
		this.isQuickForm = isQuickForm;

		initComponent(windowNo, AD_Tab_ID, AD_User_ID, columnsWidth, gridFieldIds);
	}

	/**
	 * Initialize component of customize panel
	 * 
	 * @param windowNo
	 * @param AD_Tab_ID
	 * @param AD_User_ID
	 * @param columnsWidth
	 * @param gridFieldIds
	 */
	private void initComponent(int windowNo, int AD_Tab_ID, int AD_User_ID, Map<Integer, String> columnsWidth,
			ArrayList<Integer> gridFieldIds)
	{
		if (isQuickForm)
			quickCustomizePanel = new QuickCustomizeGridPanel(windowNo, AD_Tab_ID, AD_User_ID, columnsWidth, gridFieldIds);
		else
			customizePanel = new CustomizeGridPanel(windowNo, AD_Tab_ID, AD_User_ID, columnsWidth, gridFieldIds);

		this.setStyle("position : absolute;");
		this.setBorder("normal");
		this.setSclass("popup-dialog");

		if (isQuickForm)
		{
			this.setWidth("400px");
			this.setHeight("310px");
			quickCustomizePanel.createUI();
			quickCustomizePanel.loadData();
			appendChild(quickCustomizePanel);
		}
		else
		{
			this.setWidth("600px");
			this.setHeight("500px");
			customizePanel.createUI();
			customizePanel.loadData();
			appendChild(customizePanel);
		}
	} // initComponent

	/**
	 * @return whether change have been successfully save to DB
	 */
	public boolean isSaved()
	{
		boolean isSaved;
		if (isQuickForm)
			isSaved = quickCustomizePanel.isSaved();
		else
			isSaved = customizePanel.isSaved();
		return isSaved;
	}

	/**
	 * @param gridPanel
	 */
	public void setGridPanel(GridPanel gridPanel)
	{
		customizePanel.setGridPanel(gridPanel);
	}

	/**
	 * @param quickGridPanel
	 */
	private void setQuickGridPanel(QuickGridPanel quickGridPanel)
	{
		quickCustomizePanel.setGridPanel(quickGridPanel);

	}

	/**
	 * Show User customize (modal)
	 * 
	 * @param WindowNo window no
	 * @param AD_Tab_ID
	 * @param columnsWidth
	 * @param gridFieldIds list fieldId current display in gridPanel
	 * @param gridPanel
	 * @param quickGridPanel
	 * @param isQuickForm
	 * @return true if save successfully
	 */
	public static boolean showCustomize(int WindowNo, int AD_Tab_ID, Map<Integer, String> columnsWidth,
			ArrayList<Integer> gridFieldIds, GridPanel gridPanel, QuickGridPanel quickGridPanel, boolean isQuickForm)
	{
		CustomizeGridPanelDialog customizeWindow = new CustomizeGridPanelDialog(WindowNo, AD_Tab_ID,
				Env.getAD_User_ID(Env.getCtx()), columnsWidth, gridFieldIds, isQuickForm);

		if (isQuickForm)
		{
			customizeWindow.setQuickGridPanel(quickGridPanel);
		}
		else
		{
			customizeWindow.setGridPanel(gridPanel);
		}
		AEnv.showWindow(customizeWindow);

		return customizeWindow.isSaved();
	} // showCustomize
}
