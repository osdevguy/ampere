package org.adempiere.webui.factory;

import org.adempiere.webui.AdempiereIdGenerator;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.theme.ITheme;
import org.adempiere.webui.theme.ThemeManager;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;

/**
 * Factory class to create button with consistent look and feel
 */
public class ButtonFactory
{
	/**
	 * Create named button with default style
	 * 
	 * @param name
	 * @return new button instance
	 */
	public static Button createNamedButton(String name)
	{
		return createNamedButton(name, false, true);
	}

	/**
	 * @param name button name. If withText is true, the name will be used to
	 *            lookup the button label from ad_message
	 * @param withText text button
	 * @param withImage image button
	 * @return new button instance
	 */
	public static Button createNamedButton(String name, boolean withText, boolean withImage)
	{
		Button button = new Button();
		button.setName("btn" + name);
		button.setId(name);
		button.setAttribute(AdempiereIdGenerator.ZK_COMPONENT_PREFIX_ATTRIBUTE, button.getId());

		String text = Util.cleanAmp(Msg.translate(Env.getCtx(), name));

		if (withText && text != null)
		{
			button.setLabel(text);
			if (withImage)
			{
				button.setImage(ThemeManager.getThemeResource(ITheme.IMAGE_FOLDER_ROOT + name + "16.png"));
			}
			LayoutUtils.addSclass("action-text-button", button);
		}
		else
		{
			button.setImage(ThemeManager.getThemeResource(ITheme.IMAGE_FOLDER_ROOT + name + "24.png"));
			if (text != null)
				button.setTooltiptext(text);
			LayoutUtils.addSclass("action-button", button);
		}

		return button;
	}

	/**
	 * create Button
	 * 
	 * @param label
	 * @param image
	 * @param tooltiptext
	 * @return new button instance
	 */
	public static Button createButton(String label, String image, String tooltiptext)
	{
		Button button = new Button();

		if (!Util.isEmpty(label))
		{
			button.setLabel(label);
			if (!Util.isEmpty(image))
			{
				button.setImage(image);
			}
			LayoutUtils.addSclass("action-text-button", button);
		}
		else if (!Util.isEmpty(image))
		{
			button.setImage(image);
			LayoutUtils.addSclass("action-button", button);
		}

		if (!Util.isEmpty(tooltiptext))
		{
			button.setTooltiptext(tooltiptext);
		}

		return button;
	}
}
