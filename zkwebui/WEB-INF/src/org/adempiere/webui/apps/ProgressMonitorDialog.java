package org.adempiere.webui.apps;

import java.util.Timer;
import java.util.TimerTask;

import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.session.SessionManager;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.util.TimeUtil;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.Vbox;


/**
 * Use as a substitute to BusyDialog
 * @author jobriant
 *
 */
public class ProgressMonitorDialog extends  Window   
{


	/**
	 * 
	 */
	private static final long serialVersionUID = -4728307200109353127L;
	private static CLogger log = CLogger.getCLogger(ProgressMonitorDialog.class);
	
	private static final int DELAY_INTERVAL = 5000; //15000;  //update user every 15sec

	Progressmeter meterMemoryUsage = new Progressmeter();
	Label lMeter = new Label();
	Label lTimeElapsed = new Label();
	Label fTimeElapsed = new Label();
	Timer timer = new Timer();
	
	ProcessInfo m_pi = null;
	int m_counter = 0;
	int errorCounter = 0;
	long m_timeStarted = System.currentTimeMillis();
	
	String s_TimeElapsed = "Waiting response from Server . . .";
	int i_progress = 0;
	int percentUsage = 0;
	boolean wait = true;
	
	private Desktop m_desktop;
	private Thread m_thread = null;
	
	public ProgressMonitorDialog(ProcessInfo p_info, Desktop desktop) {
		super();

		m_pi = p_info;
		Vbox vbox = new Vbox();
		
		Hbox box = new Hbox();
		box.setStyle("padding: 5px");
		lTimeElapsed.setWidth("150px");
		box.appendChild(lTimeElapsed);
		fTimeElapsed.setText(s_TimeElapsed);
		fTimeElapsed.setWidth("300px");
		box.appendChild(fTimeElapsed);
		
		vbox.appendChild(box);
		box = new Hbox();
		box.setStyle("padding: 5px");
		box.appendChild(lMeter);
		box.appendChild(meterMemoryUsage);
		vbox.appendChild(box);
		lMeter.setWidth("150px");
		meterMemoryUsage.setWidth("300px");
		meterMemoryUsage.setValue(percentUsage);	
		
		lTimeElapsed.setText("Statistics        :");
		lMeter.setText      ("Memory Usage      :");
		
		setPosition("center");
		setShadow(true);
		setStyle("background-color: #1f9bde; border: 5px solid #09437b;");
		m_desktop = desktop;
		if (m_desktop !=null && !m_desktop.isServerPushEnabled()) {
			m_desktop.enableServerPush(true);
		}
		if (m_desktop == null) {
			log.severe("Cannot activate Progress Monitor when Desktop is null.");
		}
		timer.schedule(getTask(), 0, DELAY_INTERVAL);
		
		appendChild(vbox);
		
		this.setZclass("progress-monitor");
		this.setWidth("650px");
		this.setHeight("105px");

	}
	
	public TimerTask getTask () {
		return new TimerTask() {
			public void run () {
				update();
			}
		};
	}

	@Override
	public void dispose() {
		timer.cancel();
		super.dispose();
	}
	
	public void update() {
		if (m_desktop == null) {
			return;
		}
		if (wait) {
			return;
		}
		updateMetrics();
		if (SessionManager.activateDesktop(m_desktop)) {
			updateNotification();
			SessionManager.releaseDesktop(m_desktop);
		} else {
			log.fine("Cannot activate desktop!  Some process is holding for " + ++errorCounter + " times. Thread = " + Thread.currentThread().toString());

			//process could have died unexpectedly
		}
		if (m_thread != null && errorCounter > 0 && !m_thread.isAlive()) {
			timer.cancel();

			
		}
		
	}
	
	private void updateMetrics() {
		long elapsedTime = 1000 * ((System.currentTimeMillis() - m_timeStarted ) / 1000) ;
//		i_progress = (int) (seconds) / m_pi.getEstSeconds() * 100;
//		i_progress = i_progress > 100 ? 100 : i_progress;
		Runtime rt = Runtime.getRuntime();
		long usedMemory = rt.totalMemory() - rt.freeMemory();
		percentUsage = (int) ((float)usedMemory / rt.totalMemory() * 100);
		log.fine("Total:" + rt.totalMemory() + " Free: " + rt.freeMemory() + " Percent=" + percentUsage);
		s_TimeElapsed = TimeUtil.formatElapsed(elapsedTime) 
				+ " elapsed " + usedMemory / 1024 / 1024 + " MB / " + rt.totalMemory()/1024/1024 + " MB used."  ;
		
		
	}

	public void updateNotification()
	{
		try {
			fTimeElapsed.setText(s_TimeElapsed);
//		meterMemoryUsage.setValue(i_progress);
			meterMemoryUsage.setValue(percentUsage);
		} catch (Exception e) {
			log.severe("Session desktop=" + AEnv.getDesktop().getId() + " Desktop=" + m_desktop.getId());
		}
	}
	
	public void fireTimer(Thread thread)
	{
		 m_timeStarted = System.currentTimeMillis();
		 wait = false;
		 m_thread = thread;
	}
	

}
	
