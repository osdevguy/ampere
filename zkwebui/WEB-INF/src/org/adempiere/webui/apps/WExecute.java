/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2007 Adempiere, Inc. All Rights Reserved.                	  *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.apps;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.MPInstance;
import org.compiere.model.MRole;
import org.compiere.model.MTable;
import org.compiere.print.MPrintFormatItem;
import org.compiere.print.ReportCtl;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;

/**
 * @author jobriant
 */
public class WExecute implements EventListener {


	public static final String LINK_ID = "Link_ID";


	/**
	 *	Constructor
	 *
	 *  @param AD_Table_ID table
	 *  @param invoker component to display popup (optional)
	 *  @param query query
	 *  @param parent The invoking parent window
	 *  @param WindowNo The invoking parent window number
	 */
	public WExecute (String display, String value, int AD_PrintFormatItem_ID, int AD_PInstance_ID, Component parent, 
			int WindowNo, Desktop desktop)
	{
/*		log.config("AD_Table_ID=" + AD_Table_ID + " " + query);
		if (!MRole.getDefault().isCanReport(AD_Table_ID))
		{
			FDialog.error(0, "AccessCannotReport", query.getTableName());
			return;
		}

		m_query = query; */
		this.parent = parent;
		this.WindowNo = WindowNo;
		this.m_desktop = desktop;
		m_value = value;
		m_display = display; 
		pfi = new MPrintFormatItem(Env.getCtx(), AD_PrintFormatItem_ID, null);
		pInstance = new MPInstance(Env.getCtx(), AD_PInstance_ID, null);

		getActions ();
	}	//	AReport

	private String m_display;
	private String m_value;
	private Menupopup 	m_popup;
	/**	The Option List					*/
	private ArrayList<KeyNamePair>	m_list = new ArrayList<KeyNamePair>();
	/** Override Parameter from the link */
	private HashMap<Integer, String>	m_overrideParamater =  new HashMap<Integer, String>();

	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(WExecute.class);
	/** The parent window for locking/unlocking during process execution */
	Component parent;
	/** The parent window number */
	int WindowNo;
	private MPrintFormatItem pfi = null;
	private MPInstance pInstance = null;
	private Desktop m_desktop = null;


	/**
	 * 	Get the Print Formats for the table.
	 *  Fill the list and the popup menu
	 * 	@param AD_Table_ID table
	 *  @param invoker component to display popup (optional)
	 */
	private void getActions ()
	{
		KeyNamePair pp = null;
		String sql = "SELECT p.ad_process_id, cp.name " +
	             "FROM ad_process p " +
	             "  INNER JOIN ad_columnprocess cp ON p.ad_process_id = cp.ad_process_id " +
	             "  INNER JOIN ad_printformatitem pfi ON cp.ad_column_id = pfi.ad_column_id " +
	             "WHERE pfi.ad_printformatitem_id = ? " +
	             "AND EXISTS (SELECT * FROM AD_Process_Access WHERE AD_Process_ID = p.AD_Process_ID AND AD_Role_ID = ?) " +
	             "ORDER BY p.name";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, pfi.getAD_PrintFormatItem_ID());
			pstmt.setInt(2, MRole.getDefault().getAD_Role_ID());
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				pp = new KeyNamePair (rs.getInt(1), rs.getString(2));
				m_list.add(pp);
			}
					
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally {
			DB.close(rs, pstmt);
			rs = null; 
			pstmt = null;
		}

		//	Nothing to execute
		if (m_list.size() == 0)
		{
			return;
		}
		//	Only one process to execute, just run it
		else if (m_list.size() == 1)
			executeProcess ((KeyNamePair)m_list.get(0));
		//	Multiple options exist, let the user decide
		else 
			showPopup();	//	below button
	}

	private void showPopup() {
		m_popup = new Menupopup();		
		for(int i = 0; i < m_list.size(); i++)
		{
			KeyNamePair pp = (KeyNamePair) m_list.get(i);
			Menuitem menuitem = new Menuitem(pp.getName());
			menuitem.setValue(i + "");
			menuitem.addEventListener(Events.ON_CLICK, this);
			m_popup.appendChild(menuitem);
		}
		m_popup.setPage(parent.getPage());
		m_popup.open(parent);
	}


	private void executeProcess(KeyNamePair pp) 
	{
		int ad_process_id = pp.getKey();

		boolean startWOasking = false;
		int table_ID = pInstance.get_Table_ID();
		int record_ID = pInstance.getRecord_ID();
		retrieveParameter(ad_process_id);
		ProcessModalDialog dialog = new ProcessModalDialog(null, WindowNo,
				ad_process_id, table_ID, record_ID, startWOasking, m_desktop);
		
		dialog.preloadParameter(pInstance);

		dialog.setLinkID(m_value);
		for (Map.Entry<Integer, String> entry : m_overrideParamater.entrySet()) {
			dialog.overrideParameter(entry.getKey(), entry.getValue());
		}
			
		dialog.setDrillSource(WindowNo + "-" + m_display);
		if (dialog.isValid())
		{
			dialog.setWidth("500px");
			
			dialog.setVisible(true);
			dialog.setPosition("center");
			AEnv.showWindow(dialog);
		}
	}

	/**************************************************************************
	 * 	Get AD_Table_ID for Table Name
	 * 	@param tableName table name
	 * 	@return AD_Table_ID or 0
	 */
	public static int getAD_Table_ID (String tableName)
	{
		return MTable.getTable_ID(tableName);
	}	//	getAD_Table_ID
	

	public void onEvent(Event event) {
		if(event.getTarget() instanceof Menuitem)
		{
			Menuitem mi = (Menuitem) event.getTarget();
			executeProcess(m_list.get(Integer.parseInt(mi.getValue().toString())));
		}
	}		
	
	private void retrieveParameter(int ad_process_id)
	{
		ResultSet rs = null;
		String sql = "SELECT p.ad_process_para_id, p.defaultvalue " +
	             "FROM ad_columnprocesspara p " +
	             "  INNER JOIN ad_columnprocess cp ON p.ad_columnprocess_id = cp.ad_columnprocess_id " +
	             "WHERE cp.ad_process_id = ?";
		PreparedStatement pstmt = DB.prepareStatement(sql, null);
		try {
			pstmt.setInt(1, ad_process_id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				m_overrideParamater.put(rs.getInt(1), rs.getString(2));
			}
		} catch (Exception e) {
			log.severe(e.getMessage());
		} finally {
			DB.close(rs, pstmt);
		}

	}
}
