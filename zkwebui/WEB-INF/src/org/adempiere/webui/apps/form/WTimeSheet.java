package org.adempiere.webui.apps.form;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Datebox;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.StatusBarPanel;
import org.adempiere.webui.window.CopyTimeSheetFrom;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MProduct;
import org.compiere.model.MProjectLine;
import org.compiere.model.MResource;
import org.compiere.model.MRole;
import org.compiere.model.MTimesheetEntry;
import org.compiere.model.MUser;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.zkforge.keylistener.Keylistener;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Space;

/**
 * @author Nikunj Panelia
 *
 */
public class WTimeSheet  extends ADForm implements EventListener,WTableModelListener
{
	
	private static final long serialVersionUID = -8669256486969882957L;
	
	private static CLogger log = CLogger.getCLogger(WTimeSheet.class);
	
	
	private Label  labelAuthor=new  Label("Author");
	private Listbox listBoxAuthor = ListboxFactory.newDropdownListbox();
	private Label  labelProject=new  Label("project");
	private Listbox listBoxProject = ListboxFactory.newDropdownListbox();
	private Label  labelDaterange=new  Label("Date");
	private Datebox startDate=new Datebox();
	private Label  labelRangeDays=new  Label("Days");
	private Textbox textRangeDays=new Textbox();
	private WListbox miniTable = ListboxFactory.newDataTable();
	private Label labelInternalNote=new Label("Internal Note:");
	private Textbox textInternalNote=new Textbox();
	private Label labelCustomerNote=new Label("Customer Note*:");
	private Textbox textCustomerNote=new Textbox();
	private Grid selNorthPanel = GridFactory.newGridLayout();
	private Grid selSouthPanel = GridFactory.newGridLayout();
	private Button copyButton=new Button("Copy From");
	
	private StatusBarPanel statusBar=new StatusBarPanel();
	private Panel southPanel=new Panel(); 
	private Keylistener keyListener;

	ArrayList<ArrayList<Object>> arrayListAll = new ArrayList<ArrayList<Object>>();
	private static final int  TOTAL_HOUR_COl = 5;
	
	/* (non-Javadoc)
	 * @see org.adempiere.webui.panel.ADForm#initForm()
	 */
	protected void initForm() 
	{		
		zkInit();
		Borderlayout contentPane = new Borderlayout();
		this.appendChild(contentPane);
		contentPane.setWidth("99%");
		contentPane.setHeight("100%");

		North north = new North();
		contentPane.appendChild(north);
		north.appendChild(selNorthPanel);

		Center center = new Center();
		center.setStyle("border: 2px");
		contentPane.appendChild(center);
		center.appendChild(miniTable);

		South south = new South();
		contentPane.appendChild(south);
		south.appendChild(southPanel);
		
		southPanel.appendChild(selSouthPanel);
		
		southPanel.appendChild(statusBar);
		southPanel.setHeight("100%");
		
		
	}
	
	
	private void zkInit()
	{
		String SQL = MRole.getDefault().addAccessSQL("SELECT r.s_resource_id, r.Name FROM S_Resource r "
				+ " JOIN AD_User u ON (u.AD_User_ID=r.AD_User_ID) WHERE r.IsActive='Y' AND "
				+ " u.C_BPartner_ID IS NOT NULL AND r.AD_Client_ID="+Env.getAD_Client_ID(Env.getCtx()),
						"r", MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO)+ " ORDER BY r.Name";
		for (KeyNamePair kn : DB.getKeyNamePairs(SQL, true)) 
		{
			listBoxAuthor.addItem(kn);
			String sql="Select AD_User_ID from S_Resource where S_Resource_ID=? and AD_Client_ID=?";
			int user_ID = DB.getSQLValue(null ,sql, kn.getKey(),Env.getAD_Client_ID(Env.getCtx()));
			if (user_ID == Env.getAD_User_ID(Env.getCtx()))
			 	listBoxAuthor.setSelectedKeyNamePair(kn);
		}
		listBoxAuthor.setMold("select");
		
		Row row = selNorthPanel.newRows().newRow();

		row.appendChild(labelAuthor.rightAlign());
		row.appendChild(listBoxAuthor);
		row.appendChild(new Space());
		row.appendChild(labelProject.rightAlign());
		row.appendChild(listBoxProject);
		row.appendChild(new Space());
		row.appendChild(labelDaterange.rightAlign());
		row.appendChild(startDate);
		row.appendChild(labelRangeDays.rightAlign());
		row.appendChild(textRangeDays);
		row.appendChild(copyButton);
		copyButton.addEventListener(Events.ON_CLICK, this);
		copyButton.setDisabled(true);
		row.setHeight("40px");
		row.setAlign("center");

		row = selSouthPanel.newRows().newRow();
		row.appendChild(labelInternalNote.rightAlign());
		row.appendChild(textInternalNote);
		
		row.appendChild(labelCustomerNote.rightAlign());
		row.appendChild(textCustomerNote);
		
		textInternalNote.setMultiline(true);
		textInternalNote.setRows(10);
		textInternalNote.setWidth("400px");
		textInternalNote.addEventListener(Events.ON_CHANGE, this);

		
		textCustomerNote.setMultiline(true);
		textCustomerNote.setRows(10);
		textCustomerNote.setWidth("400px");
		textCustomerNote.addEventListener(Events.ON_CHANGE, this);
		
		textRangeDays.setValue("5");
		daysrange = 5;
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		startDateValue = new Date(c.getTimeInMillis());
		startDate.setValue(new Date(c.getTimeInMillis()));
		
		listBoxAuthor.addEventListener(Events.ON_SELECT, this);
		startDate.addEventListener(Events.ON_CHANGE, this);
		textRangeDays.addEventListener(Events.ON_CHANGE, this);
		renderProjects();
		
	}
	

	/**
	 * @return sql where clause 
	 */
	protected String getSQLWhere(int resource_Id)
	{
		StringBuffer where = new StringBuffer();
		if (listBoxAuthor.getSelectedItem().toString() != null)
			where.append(" AND p.C_Project_id in (Select C_project_id from c_projectresource where s_resource_id="+resource_Id+")");

		return where.toString();

	}
	
	int rowSize;
	
	ArrayList<Integer> listProcessed=new ArrayList<Integer>();
	/**
	 * load projects of given author.
	 * @param AD_author_ID
	 */
	private void loadProjects()
	{
		rowSize=0;
		Vector<Vector> data = new Vector<Vector>();
		StringBuffer sb=new StringBuffer();
		sb.append("select p.c_project_id ,ph.c_projectphase_id,t.c_projecttask_id,p.name,ph.name,t.name, ")
				.append(" gettimesheetqty(t.c_projecttask_id,?,?,?),p.processed,ph.iscomplete,t.task_complete")
				.append(" from c_project p ")
				.append(" join c_projectphase ph on p.c_project_id=ph.c_project_id ")
				.append(" join c_projecttask t on ph.c_projectphase_id=t.c_projectphase_id ")
				.append(" where p.projectlinelevel = 'T' and p.IsActive='Y' ")
				.append( getSQLWhere((Integer)listBoxAuthor.getSelectedItem().getValue()));
				if((Integer)listBoxProject.getSelectedItem().getValue() > 0)
					sb.append(" and p.c_project_id =? ")
				.append(" order by p.name");
		
		PreparedStatement pstmt = DB.prepareStatement(sb.toString(), null);
		ResultSet rs = null;
		try 
		{					
			pstmt.setInt(1, (Integer)listBoxAuthor.getSelectedItem().getValue());
			pstmt.setTimestamp(2, dateList.get(0));
			pstmt.setInt(3, dateList.size());
			if((Integer)listBoxProject.getSelectedItem().getValue() > 0)
				pstmt.setInt(4, (Integer)listBoxProject.getSelectedItem().getValue());
			rs = pstmt.executeQuery();
			while (rs.next()) 
			{
				ArrayList<Object> arrayList = new ArrayList<Object>();
				arrayList.add(rs.getInt(1)); //project_id
				arrayList.add(rs.getInt(2)); //phase id
				arrayList.add(rs.getInt(3)); //task id
				Vector<Object> line = new Vector<Object>(20);
				line.add(new IDColumn(rs.getInt(1)));  //project id 
				line.add( rs.getString(4));  //project name
				line.add(rs.getString(5)); //phase name
				line.add(rs.getString(6)); //task name
				Array qty=rs.getArray(7);
				BigDecimal[] lineQty = (BigDecimal[])qty.getArray();
				line.add(lineQty[dateList.size()]);		
				boolean isProcessedZero=true;
				for(int i=0;i<lineQty.length;i++)
				{
					BigDecimal bd=BigDecimal.ZERO;
					if(lineQty[i]!=null)
						bd=lineQty[i];
					if(bd.compareTo(BigDecimal.ZERO) >0)
						isProcessedZero=false;
					line.add(bd);
				}
				boolean isProcessed= ((rs.getString(8) !=null && rs.getString(8).equals("Y")) || (rs.getString(9) !=null && rs.getString(9).equals("Y")) ||
						(rs.getString(10) !=null && rs.getString(10).equals("Y")) );
				if(isProcessedZero &&isProcessed)
					continue;
				if(isProcessed)
					listProcessed.add(rowSize);
				data.add(line);
				arrayListAll.add(arrayList);
				rowSize++;
			}
			Vector<Object> line = new Vector<Object>(20);
			line.add(new IDColumn(0));
			line.add(null);
			line.add(null);
			line.add(null);
		
			for(int i=4;i<dateList.size()+TOTAL_HOUR_COl;i++)
			{		
				line.add(getTotalVert(data,rowSize,i));
			}
			
			data.add(line);
			
			loadTable(data);
			
	
		} 
		catch (SQLException e)
		{
			log.log(Level.SEVERE, e.getLocalizedMessage());
		} 
		finally 
		{
			DB.close(rs, pstmt);
		}
	}
	/**
	 * Load data in table
	 * @param data
	 */
	private void loadTable(Vector<Vector> data)
	{
			Vector<String> columnNames = null;
			miniTable.clear();
			columnNames = new Vector<String>(6);
			columnNames.add("Select");
			columnNames.add("Project");
			columnNames.add("Site");
			columnNames.add("Task");
			columnNames.add("Total hours");
			
			DateFormat dateFormat = new SimpleDateFormat("EEE-dd/MM");
			for(int i=0;i<daysrange;i++)
			{			
				columnNames.add(dateFormat.format(dateList.get(i)));
			}
			
			ListModelTable model = new ListModelTable(data);
			
			miniTable.setData(model, columnNames);
			
			miniTable.setColumnClass(0, IDColumn.class, false, "Select"); 
			miniTable.setColumnClass(1, String.class, true); 
			miniTable.setColumnClass(2, String.class, true); 
			miniTable.setColumnClass(3, String.class, true);
			miniTable.setColumnClass(4, BigDecimal.class, true); 
			for(int i=TOTAL_HOUR_COl;i<daysrange+TOTAL_HOUR_COl;i++)
			{			
				miniTable.setColumnClass(i, BigDecimal.class, false); // 
				
			}			
			miniTable.setMultiSelection(false);
			miniTable.autoSize();
			miniTable.addActionListener(this);
			miniTable.repaint();
			miniTable.setAllowCalculator(false);
			model.addTableModelListener(this);
			
			disableProcessedLine(listProcessed);
			WListItemRenderer wrl=(WListItemRenderer)miniTable.getItemRenderer();
			wrl.addEventListener(this);
			
			
			if (keyListener != null)
        		keyListener.detach();
        	keyListener = new Keylistener();
        	this.appendChild(keyListener);
        	keyListener.setCtrlKeys("#left#right#up#down");
        	keyListener.addEventListener(Events.ON_CTRL_KEY, this);
        	keyListener.setAutoBlur(false);
	}
	
		
	private void disableProcessedLine(ArrayList<Integer> listProcessed) 
	{
	
		for (int i = 0; i < listProcessed.size(); i++) 
		{
			Listitem item=miniTable.getItemAtIndex(listProcessed.get(i));
			item.setClass("lt-disable-list-item");
			item.setDisabled(true);
		}
		Listitem item=miniTable.getItemAtIndex(rowSize);
		item.setClass("lt-disable-list-item");
		item.setDisabled(true);
		
		renderJavaSript();
	
	}


	private void renderJavaSript() 
	{
		String sScriptHead = "var outerdiv=document.querySelectorAll('div.z-listbox-body > table  tr.lt-disable-list-item input'); "
				+ "for (var i=0; i<outerdiv.length; i++) {"
				+ " outerdiv[i].setAttribute('readonly','readonly');"
				+ " outerdiv[i].setAttribute('style', 'color:#4E7AA5 !important;text-align: right;'); "
				+ "}"
				+ " var outerdiv1=document.querySelectorAll('div.z-listbox-body > table  tr.lt-disable-list-item td div');  "
				+ " for (var i=0; i<outerdiv1.length; i++) { "
				+ "	outerdiv1[i].setAttribute('style', 'color:#4E7AA5 !important;');  "
				+ "}"
				+ " var outerdiv2=document.querySelectorAll('div.z-listbox-body > table  tr td input[type=text]');  "
				+ " for (var i=0; i<outerdiv2.length; i++) { "
				+ "	 outerdiv2[i].onclick =function() { this.setSelectionRange(0, this.value.length); };  "
				+ "}";
		Clients.evalJavaScript(sScriptHead);	
		
	}

	/**
	 * manipulate vertical total from data
	 * 
	 * @param data
	 * @param size row size
	 * @param col column no
	 * @return vertical total
	 */
	private BigDecimal getTotalVert(Vector<Vector> data, int size, int col) 
	{
		BigDecimal totlaVert=BigDecimal.ZERO;
		for(int i=0;i<size;i++)
		{		
			totlaVert=totlaVert.add((BigDecimal)data.get(i).get(col));
		}
		return totlaVert;
	}

	int daysrange;
	Date startDateValue;
	int author_ID;
	int col = -1;
	int row = -1;
	Listcell cell;
	
	boolean isOk=true;
	public void onEvent(Event e)
	{
		if(e.getTarget() == listBoxAuthor  || e.getTarget() == startDate || e.getTarget() == textRangeDays || e.getTarget() == listBoxProject) 
		{
			if(startDate !=null)
				startDateValue=startDate.getValue();
			else
				startDateValue=null;
			if(textRangeDays.getValue()!=null && textRangeDays.getValue().length()>0)
				daysrange=Integer.valueOf(textRangeDays.getValue());
			else
				daysrange=0;
			if(listBoxAuthor.getSelectedItem()!=null && listBoxAuthor.getSelectedItem().getValue()!=null )
				author_ID=(Integer)listBoxAuthor.getSelectedItem().getValue();				
			else
				author_ID=0;
			if(e.getTarget() == listBoxAuthor && listBoxAuthor.getSelectedItem()!=null)
					renderProjects();
			if(startDateValue == null  || daysrange == 0 || author_ID == 0)
			{
				textCustomerNote.setValue(null);
				textInternalNote.setValue(null);
				miniTable.clear();
				copyButton.setDisabled(true);
				return;
			}	
			arrayListAll.clear();
			rowSize=0;
			col=0;
			row=0;
			cell=null;
			listProcessed.clear();
			copyButton.setDisabled(false);
			createDateRange();
			
			loadProjects();
		}
		else if (e.getTarget() == textInternalNote && textInternalNote.getValue()!=null && textInternalNote.getValue().length()>0 && 
				        textCustomerNote.getValue()!=null && textCustomerNote.getValue().length()>0) 
		{				
			insertData();
		} 
		else if (e.getTarget() == textCustomerNote && textCustomerNote.getValue()!=null && textCustomerNote.getValue().length()>0) 
		{				
			insertData();
		} 
		else if (e.getTarget() instanceof Decimalbox || e.getTarget() instanceof NumberBox)			
		{		
			Component source = e.getTarget();		
			if(getRowPosition(source) >= rowSize)
				return;
			if( Events.ON_FOCUS.equals(e.getName()) && getRowPosition(source) == -1 && getColumnPosition(source) > 4 && itemChanged !=null )
			{
				NumberBox box=((NumberBox)((Listcell) itemChanged.getChildren().get(getColumnPosition(source))).getChildren().get(0));
				box.focus();
				//itemChanged=null;
			}
			else if(Events.ON_FOCUS.equals(e.getName()) && getRowPosition(source) > -1 && getColumnPosition(source) > 4 )
			{
				if(row==getRowPosition(source) && col==getColumnPosition(source) && itemChanged ==null )
					return;
				if(cell!=null )
				{
					cell.setStyle("background-color:transparent;");
				}
				cell = findListcell(source);
				row = getRowPosition(source);
				col = getColumnPosition(source);
				cell.setStyle("background-color:yellow;");

				setNoteFields();
				if(listProcessed.contains(row))
				{
					textInternalNote.setReadonly(true);
					textCustomerNote.setReadonly(true);
				}
				else
				{
					textInternalNote.setReadonly(false);
					textCustomerNote.setReadonly(false);
				}
			}
			else if( (Events.ON_BLUR.equals(e.getName()) ) && cell.getValue() != null && row > -1 && col > 4 )
			{
				isOk=true;
				statusBar.setStatusLine(null);
				if(listProcessed.contains(row))
				{
					return;
				}
				cell.setStyle("background-color:yellow;");
				insertData();
				if(!isOk && itemChanged !=null  )
				{
					((NumberBox)((Listcell) miniTable.getItemAtIndex(row).getChildren().get(col)).getChildren().get(0)).focus();
					textCustomerNote.focus();
					statusBar.setStatusLine("Plase enter Customer Note.",true,false);
				}			
			}  
				
		} 		
		else if(e.getTarget() == copyButton)
		{
			openCopyForm();
		}
		else if(e.getTarget() == miniTable)
		{
			renderJavaSript();
			
		}
		else if(e.getName().equals(Events.ON_CTRL_KEY))
		{

			if(cell == null)
				return;
    		KeyEvent keyEvent = (KeyEvent) e;
    		int code=keyEvent.getKeyCode();   
    		
    		int col=cell.getColumnIndex();
			int row=((Listitem)cell.getParent()).getIndex();
			if(row == -1 && itemChanged !=null)
				row=((Listitem)itemChanged).getIndex();
			if(code == KeyEvent.DOWN)
			{
				row+=1;
			}
			else if(code == KeyEvent.LEFT)
			{
				col-=1;
			}
			else if(code == KeyEvent.RIGHT)
			{
				col+=1;
			}
			else if(code == KeyEvent.UP)
			{
				row-=1;
			}
			if(row < 0 || row > rowSize || col < TOTAL_HOUR_COl || col >= daysrange+TOTAL_HOUR_COl)
				return;
			if(box!=null)
			{
				box.focus();
				textCustomerNote.focus();
				return;
			}
				
			NumberBox box2=((NumberBox)((Listcell) miniTable.getItemAtIndex(row).getChildren().get(col)).getChildren().get(0));
			if(box2 != null )
				box2.focus();
			
		}			
	}

	private int copyProjectID;
	private int copyAuthorID;
	
	private void openCopyForm() 
	{
		copyProjectID= 0;
		copyAuthorID=0;
		CopyTimeSheetFrom form=new CopyTimeSheetFrom();
		copyProjectID=form.getProjectID();
		copyAuthorID=form.getauthorID();
		if(copyAuthorID>0)
		{
			copyLinesFrom();
		}
	}	
	
	private void copyLinesFrom()
	{		
		if(copyProjectID > 0 && !isProjectAssigned(copyProjectID))
			return;
		MResource resource=new MResource(Env.getCtx(), (Integer)listBoxAuthor.getSelectedItem().getValue(), null);
		String sql="Select M_Product_ID from M_Product where S_Resource_ID=? and AD_Client_ID=?";
		int m_Product_ID = DB.getSQLValue(null ,sql, resource.get_ID(),Env.getAD_Client_ID(Env.getCtx()));
		MUser user=new MUser(Env.getCtx(), resource.getAD_User_ID(), null);
		ArrayList<Integer> listTask=new ArrayList<Integer>();
		listTask=getTasksOfResource();
		
		if(listTask.size()<=0)
			return;
			
		MTimesheetEntry[] entries=getEntries(Env.getCtx(), listTask, copyAuthorID,dateList.get(0),dateList.get(dateList.size()-1));
		for (MTimesheetEntry copyEntry : entries )
		{
						
			MTimesheetEntry  entry=MTimesheetEntry.get(Env.getCtx(), copyEntry.getC_Project_ID(), copyEntry.getC_ProjectPhase_ID(),
					copyEntry.getC_ProjectTask_ID(), (Integer)listBoxAuthor.getSelectedItem().getValue(), copyEntry.getDateAcct(), null);
			if(entry!=null && (entry.getQtyEntered().compareTo(BigDecimal.ZERO) <=0 ))
			{
				PO.copyValues(copyEntry, entry);
				entry.setS_Resource_ID(resource.get_ID());
				entry.setC_BPartner_ID(user.getC_BPartner_ID());
				entry.setM_Product_ID(m_Product_ID);
				entry.save();
			}
			else if(entry==null)
			{
				MTimesheetEntry  newEntry=new MTimesheetEntry(Env.getCtx(),0,null);
				PO.copyValues(copyEntry, newEntry);
				newEntry.setS_Resource_ID(resource.get_ID());
				newEntry.setC_BPartner_ID(user.getC_BPartner_ID());
				newEntry.setM_Product_ID(m_Product_ID);
				newEntry.save();
					
			}				
		}
		loadProjects();
				
	}

	private ArrayList<Integer> getTasksOfResource()
	{
		ArrayList<Integer> taskList=new ArrayList<Integer>();
		PreparedStatement pst=null;
		ResultSet rs=null;
		
		StringBuffer sb=new StringBuffer();
		sb.append("select p.c_project_id ,t.c_projecttask_id, ")
				.append(" gettimesheetqty(t.c_projecttask_id,?,?,?),p.processed,ph.iscomplete,t.task_complete")
				.append(" from c_project p ")
				.append(" join c_projectphase ph on p.c_project_id=ph.c_project_id ")
				.append(" join c_projecttask t on ph.c_projectphase_id=t.c_projectphase_id ")
				.append(" where p.projectlinelevel = 'T' and p.IsActive='Y' ")
				.append( getSQLWhere(copyAuthorID));
				if(copyProjectID > 0)
					sb.append(" and p.c_project_id =? ")
				.append(" order by p.name");
		pst=DB.prepareStatement(sb.toString(),null);		
		try 
		{
			
			pst.setInt(1, copyAuthorID);
			pst.setTimestamp(2, dateList.get(0));
			pst.setInt(3, dateList.size());
			if(copyProjectID > 0)
				pst.setInt(4, (copyProjectID));
			rs=pst.executeQuery();
			while(rs.next())
			{
				boolean isProcessed= ((rs.getString(4) !=null && rs.getString(4).equals("Y")) || (rs.getString(5) !=null && rs.getString(5).equals("Y")) ||
						(rs.getString(6) !=null && rs.getString(6).equals("Y")) );
				if(isProjectAssigned(rs.getInt(1)) && !isProcessed)
				{
					Array qty=rs.getArray(3);
					BigDecimal[] lineQty = (BigDecimal[])qty.getArray();
					boolean hasLogTime=false;
					for(int i=0; i < lineQty.length; i++)
					{
						BigDecimal bd=BigDecimal.ZERO;
						if(lineQty[i]!=null)
							bd=lineQty[i];
						if(bd.compareTo(BigDecimal.ZERO) > 0)
							hasLogTime=true;
					}
					if(hasLogTime)
						taskList.add(rs.getInt(2));
				}
				
			}
		}
		catch (SQLException e) 
		{
			
			log.log(Level.SEVERE, e.getLocalizedMessage());
		}	
		return taskList;
				
	}
	private boolean isProjectAssigned(int project_ID) 
	{
		PreparedStatement pst=null;
		ResultSet rs=null;
		boolean isAssigned=false;
		
		String sql="SELECT S_Resource_ID from C_ProjectResource where C_Project_ID= ? and AD_Client_ID=? ";
		pst=DB.prepareStatement(sql,null);		
		try 
		{
			pst.setInt(1,project_ID);
			pst.setInt(2,Env.getAD_Client_ID(Env.getCtx()));
			rs=pst.executeQuery();
			while(rs.next())
			{
				if(rs.getInt("S_Resource_ID") == (Integer)listBoxAuthor.getSelectedItem().getValue())
				{
					isAssigned=true;			
				}
			}
		}
		catch (SQLException e) 
		{
			
			log.log(Level.SEVERE, e.getLocalizedMessage());
		}			
				
		return isAssigned;
	}
	


	private void renderProjects()
	{
		listBoxProject.removeAllItems();
		String SQL = "SELECT  DISTINCT p.C_Project_ID, p.Name FROM C_Project p " +
				" join c_projectphase ph on p.c_project_id=ph.c_project_id " +
				" join c_projecttask t on ph.c_projectphase_id=t.c_projectphase_id " +
				" WHERE p.projectlinelevel = 'T' and p.IsActive='Y' " 
					+getSQLWhere((Integer)listBoxAuthor.getSelectedItem().getValue()) +" and p.AD_Client_ID="+Env.getAD_Client_ID(Env.getCtx())+ " ORDER BY Name";
		for (KeyNamePair kn : DB.getKeyNamePairs(SQL, true)) 
		{
			listBoxProject.addItem(kn);
		}
		
		listBoxProject.setMold("select");
		listBoxProject.addEventListener(Events.ON_SELECT, this);		
	}

	/**
	 * Create or update entry in TimeSheet 
	 */
	private void insertData()
	{
		
		if  (row > -1 && col > 4 )
		{
			Object value=miniTable.getValueAt(row, col);
			MTimesheetEntry entry=null;
			if(value!=null && ((BigDecimal)value).compareTo(BigDecimal.ZERO) >0 && (textCustomerNote.getValue()==null || textCustomerNote.getValue().length()<=0))
			{			
				isOk=false;
				return;
			}
			if(itemChanged!=null && (miniTable.getIndexOfItem(itemChanged)!=-1 || miniTable.getIndexOfItem(itemChanged) != row) && ((Listcell)box.getParent()).getColumnIndex() !=col)
				return;
			if(value!=null )
			{
				
				entry=MTimesheetEntry.get(Env.getCtx(),(Integer)arrayListAll.get(row).get(0),(Integer)arrayListAll.get(row).get(1),(Integer)arrayListAll.get(row).get(2),
							 (Integer)listBoxAuthor.getSelectedItem().getValue(),dateList.get(col-TOTAL_HOUR_COl),null);								
				
				if(entry != null)
				{
					//update existing
					if(((BigDecimal)value).compareTo(BigDecimal.ZERO)==0)
					{
						entry.setDescription(null);
						entry.setComments(null);
						textInternalNote.setValue(null);
						textCustomerNote.setValue(null);
					}
					else
					{
						entry.setDescription(textInternalNote.getValue());
						entry.setComments(textCustomerNote.getValue());
					}
					entry.setQtyEntered((BigDecimal)value);
					entry.save();
					
				}					
				else
				{
					//create new
					if(((BigDecimal)value).compareTo(BigDecimal.ZERO)==0)
						return ;
					MProduct product=MProduct.forS_Resource_ID(Env.getCtx(), (Integer)listBoxAuthor.getSelectedItem().getValue(),null);
					int bpartner_ID=DB.getSQLValue(null, "Select C_Bpartner_ID from AD_User  where AD_User_ID=(Select AD_User_ID from s_resource where s_resource_id=? ) " 
							,(Integer)listBoxAuthor.getSelectedItem().getValue() );
						
					MTimesheetEntry timeSheetEntry=new MTimesheetEntry(Env.getCtx(), 0, null);
					timeSheetEntry.setAD_Client_ID(Env.getAD_Client_ID(Env.getCtx()));
					timeSheetEntry.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
					timeSheetEntry.setS_Resource_ID((Integer)listBoxAuthor.getSelectedItem().getValue());
					timeSheetEntry.setM_Product_ID(product.get_ID());
					timeSheetEntry.setC_BPartner_ID(bpartner_ID);
					timeSheetEntry.setQtyEntered((BigDecimal)value);
					timeSheetEntry.setDescription(textInternalNote.getValue());
					timeSheetEntry.setComments(textCustomerNote.getValue());
					timeSheetEntry.setC_Project_ID((Integer)arrayListAll.get(row).get(0));
					timeSheetEntry.setC_ProjectPhase_ID((Integer)arrayListAll.get(row).get(1));
					timeSheetEntry.setC_ProjectTask_ID((Integer)arrayListAll.get(row).get(2));
					timeSheetEntry.setStartDate(dateList.get(col-TOTAL_HOUR_COl));
					timeSheetEntry.setDateAcct(dateList.get(col-TOTAL_HOUR_COl));
					timeSheetEntry.save();
												
				}	
				itemChanged=null;
				box=null;
			}
		}
	}	
	
	/**
	 * Retrieve activity if already entered.
	 * @return activity
	 */
	private void setNoteFields()
	{
		MTimesheetEntry entry=null;
		String custmerNote=null;
		String internalNote=null;
		if  (row > -1 && col > 4 )	{
		
			
			entry=MTimesheetEntry.get(Env.getCtx(),(Integer)arrayListAll.get(row).get(0),(Integer)arrayListAll.get(row).get(1),(Integer)arrayListAll.get(row).get(2),
						 (Integer)listBoxAuthor.getSelectedItem().getValue(),dateList.get(col-TOTAL_HOUR_COl),null);				
				
		
			if(entry!=null)
			{
				custmerNote=entry.getComments();
				internalNote=entry.getDescription();
			}
			else if(col>TOTAL_HOUR_COl)
			{
				
					entry=MTimesheetEntry.get(Env.getCtx(),(Integer)arrayListAll.get(row).get(0),(Integer)arrayListAll.get(row).get(1),(Integer)arrayListAll.get(row).get(2),
							 (Integer)listBoxAuthor.getSelectedItem().getValue(),dateList.get(col-6),null);				
					
				
					if(entry!=null)
					{
						custmerNote=entry.getComments();
						internalNote=entry.getDescription();
					}
			}
			textInternalNote.setValue(internalNote);
			textCustomerNote.setValue(custmerNote);
	
		}
	}
	Listitem itemChanged=null;
	NumberBox box=null;
	public void tableChanged(WTableModelEvent event)
	{

		if ((event.getType() == WTableModelEvent.CONTENTS_CHANGED))
		{			
				
			if(event.getColumn() < TOTAL_HOUR_COl || rowSize==event.getFirstRow())
			{
				return;
			}
			itemChanged=miniTable.getItemAtIndex(event.getFirstRow());
			
			if(((BigDecimal)miniTable.getValueAt(event.getFirstRow(), event.getColumn()) ==null))
			{
				miniTable.setValueAt(BigDecimal.ZERO, event.getFirstRow(),  event.getColumn());
			}
			//update horizontal total.
			BigDecimal valueHorizon=BigDecimal.ZERO;         
	
			for(int i=0;i<daysrange;i++)
			{
				valueHorizon=valueHorizon.add((BigDecimal)miniTable.getValueAt(event.getFirstRow(), i+TOTAL_HOUR_COl));
			}
		
			miniTable.setValueAt(valueHorizon, event.getFirstRow(), 4);
			//update vertical total.
			BigDecimal valueVericle=BigDecimal.ZERO;         
			
			for(int i=0;i<rowSize;i++)
			{
				valueVericle=valueVericle.add((BigDecimal)miniTable.getValueAt(i, event.getColumn()));
			}
		
			miniTable.setValueAt(valueVericle, rowSize, event.getColumn());
			//update both total.
			BigDecimal total=BigDecimal.ZERO;         
			
			for(int i=0;i<rowSize;i++)
			{
				total=total.add((BigDecimal)miniTable.getValueAt(i, 4));
			}
		
			miniTable.setValueAt(total, rowSize, 4);
			cell.setStyle("background-color:yellow;");
			Listitem item=miniTable.getItemAtIndex(rowSize);
			item.setClass("lt-disable-list-item");
			item.setStyle("color:black");
			item.setDisabled(true);
			renderJavaSript();
	
			if(itemChanged!=null)
				miniTable.renderItem(itemChanged);
			box=((NumberBox)((Listcell) itemChanged.getChildren().get(event.getColumn())).getChildren().get(0));
		
		}
	      
	}
	ArrayList<Timestamp> dateList=null;
	
	private void createDateRange() 
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:MM:SS");
		Date DateStart=null; 
		dateList=new ArrayList<Timestamp>();
		Calendar cal = Calendar.getInstance();  
	
		String date1=dateFormat.format(startDateValue);
		try 
		{
			DateStart = dateFormat.parse(date1);
		} 
		catch (ParseException e) 
		{
			log.log(Level.SEVERE, e.getLocalizedMessage());
		}		
		Timestamp timestamp = new Timestamp(DateStart.getTime());
		cal.setTime(timestamp);
		dateList.add(timestamp);
		for(int i=0; i<daysrange-1 ;i++)
		{
			cal.add(Calendar.DATE, 1);
			dateList.add(new Timestamp(cal.getTimeInMillis()));
		}
			
	}
	
	protected int getRowPosition(Component source)
	{
		Listcell cell;
		ListItem item;
		int row = -1;
		
		cell = findListcell(source);
		item = (ListItem)cell.getParent();
		row = item.getIndex();
	
		return row;
	}
	
	private Listcell findListcell(Component source) 
	{
		if (source instanceof Listcell)
			return (Listcell) source;
		Component c = source.getParent();
		while(c != null) 
		{
			if (c instanceof Listcell)
				return (Listcell) c;
			c = c.getParent();
		}
			return null;
	}
	
	protected int getColumnPosition(Component source)
	{
		Listcell cell;
		int col = -1;

		cell = findListcell(source);
		col = cell.getColumnIndex();

		return col;
	}
	
	public  MTimesheetEntry[] getEntries (Properties ctx,ArrayList<Integer> taskList,int s_Resourse_ID ,Timestamp dateFrom,Timestamp dateTo)
	{
		StringBuffer whereClause=new StringBuffer();
		StringBuffer inClause=new StringBuffer();
		Iterator<Integer> it=taskList.iterator();
		while(it.hasNext())
		{
			inClause.append(it.next());
			if(it.hasNext())
				inClause.append(",");
		}
		whereClause.append("C_ProjectTask_ID in (").append(inClause).append(")");
		whereClause.append(" and S_Resource_ID=? and ( DateAcct between ? and ? ) and AD_Client_ID=?");
		
		Query q = new Query(ctx, MTimesheetEntry.Table_Name, whereClause.toString(), null);
		
		q.setParameters( s_Resourse_ID,dateFrom,dateTo,Env.getAD_Client_ID(Env.getCtx()));
		
		List <MProjectLine> list =q.list(); 
		MTimesheetEntry[] retValue = new MTimesheetEntry[list.size()];
		list.toArray(retValue);
		return retValue;
	}
}