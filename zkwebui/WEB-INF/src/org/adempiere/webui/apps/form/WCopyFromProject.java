package org.adempiere.webui.apps.form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;

import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.SimpleTreeModel;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.theme.ITheme;
import org.adempiere.webui.util.TreeUtils;
import org.adempiere.webui.window.FDialog;
import org.compiere.grid.CreateFrom;
import org.compiere.grid.ICreateFrom;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.GridTab;
import org.compiere.model.MColumn;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MProject;
import org.compiere.model.MProjectLine;
import org.compiere.model.MProjectPhase;
import org.compiere.model.MProjectTask;
import org.compiere.model.MTreeNode;
import org.compiere.model.PO;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.East;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;
import org.zkoss.zkex.zul.West;
import org.zkoss.zul.SimpleTreeNode;
import org.zkoss.zul.Space;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;

public class WCopyFromProject extends CreateFrom implements ICreateFrom,
		ValueChangeListener, EventListener {

	private Panel selCenterPanel = new Panel();
	private Grid selNorthPanel = GridFactory.newGridLayout();
	private Window window;
	private Label projectLabel = new Label();
	private WEditor projectField;
	private Tree treeSource;
	private Tree treeTarget;
	private ConfirmPanel confirmPanel = new ConfirmPanel(true);
	
	private MProject targetProject;
	private MProject selectedProject;
	
	private Button buttonLeft=new Button();
	private Button buttonRight=new Button();
	private Button buttonUp=new Button();
	private Button buttonDown=new Button();
	
	private int currentLevel = -1;
	
	private static int LEVEL_Project	= 0;
	private static int LEVEL_Phase 		= 1;
	private static int LEVEL_Task	 	= 2;
	private static int LEVEL_TaskLine 	= 3;
	
	ArrayList<SimpleTreeNode> copiedPhaseNodes=new ArrayList<SimpleTreeNode>();
	ArrayList<SimpleTreeNode> copiedTaskNodes=new ArrayList<SimpleTreeNode>();
	ArrayList<SimpleTreeNode> copiedLineNodes=new ArrayList<SimpleTreeNode>();
	ArrayList<Treeitem>  copiedSeqChanged=new ArrayList<Treeitem>() ; 
	ArrayList<Treeitem> removedItems=new ArrayList<Treeitem>();

	public WCopyFromProject(GridTab mTab) {
		super(mTab);
		

		window = new Window();
		window.setAttribute("mode", "modal");

		try {
			if (!dynInit())
				return;
			zkInit();
			confirmPanel.addActionListener(this);
			setInitOK(true);
		} catch (Exception e) {
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
		}
		AEnv.showWindow(window);
	}

	protected void zkInit() {
		phaseMap.clear();
		lineMap.clear();
		taskMap.clear();
		copiedSeqChanged.clear();
		copiedTaskNodes.clear();
		copiedLineNodes.clear();
		copiedPhaseNodes.clear();
		removedItems.clear();
		nodesChanged.clear();
		
		window.setWidth("750px");
		window.setHeight("550px");
		window.setSizable(true);
		window.setBorder("normal");
		window.setTitle("Copy Project");
		window.setClosable(false);
		
		Borderlayout contentPane = new Borderlayout();
		window.appendChild(contentPane);
		contentPane.setWidth("99%");
		contentPane.setHeight("100%");

		projectLabel.setText("Project");
		Row row = selNorthPanel.newRows().newRow();

		North north = new North();
		contentPane.appendChild(north);
		north.appendChild(selNorthPanel);

		row.appendChild(projectLabel);
		if (projectField != null)
			row.appendChild(projectField.getComponent());

		Center center = new Center();
		contentPane.appendChild(center);
		center.appendChild(selCenterPanel);
		Borderlayout centerPane = new Borderlayout();
		selCenterPanel.appendChild(centerPane);
		centerPane.setWidth("100%");
		centerPane.setHeight("100%");
		selCenterPanel.setHeight("460px");
		selCenterPanel.setStyle("overflow: auto !important");
		center.setStyle("border: 2px;");

		West west=new West();
		centerPane.appendChild(west);
		Panel westPanel = new Panel();
		west.appendChild(westPanel);
		west.setWidth("47%");
		treeTarget =new Tree();
		treeTarget.addEventListener(Events.ON_SELECT, this);
		westPanel.appendChild(treeTarget);
		treeTarget.setStyle("overflow: auto !important;height: 440px;");

		East east=new East();
		centerPane.appendChild(east);
		Panel eastPanel = new Panel();
		east.appendChild(eastPanel);
		east.setWidth("47%");
		treeSource = new Tree();
		treeSource.addEventListener(Events.ON_SELECT, this);
		eastPanel.appendChild(treeSource);
		treeSource.setStyle("overflow: auto !important;height: 440px;");
		
		Center center2=new Center();
		centerPane.appendChild(center2);
		Panel centerPanel = new Panel();
		buttonLeft.setImage(ITheme.IMAGE_FOLDER + "Parent24.png");
		buttonRight.setImage(ITheme.IMAGE_FOLDER + "Detail24.png");
		buttonDown.setImage(ITheme.IMAGE_FOLDER + "Next24D.png");
		buttonUp.setImage(ITheme.IMAGE_FOLDER + "Previous24.png");
		centerPanel.appendChild(buttonLeft);
		centerPanel.appendChild(new Space());
		centerPanel.appendChild(buttonRight);
		centerPanel.appendChild(new Space());
		centerPanel.appendChild(new Space());
		centerPanel.appendChild(buttonUp);
		centerPanel.appendChild(new Space());
		centerPanel.appendChild(buttonDown);
		center2.appendChild(centerPanel);		
		
		buttonLeft.addEventListener(Events.ON_CLICK, this);
		buttonRight.addEventListener(Events.ON_CLICK, this);
		buttonDown.addEventListener(Events.ON_CLICK, this);
		buttonUp.addEventListener(Events.ON_CLICK, this);
		
		South south = new South();
		contentPane.appendChild(south);
		south.appendChild(confirmPanel);
		generatetargetTree();
	}
	
	SimpleTreeNode root=null;
	private void generatetargetTree() {	
		
		
		String trxName = Trx.createTrxName("CopyProject");
		targetProject = new MProject(Env.getCtx(), Integer.parseInt(getGridTab().get_ValueAsString("C_Project_ID")), trxName);
		treeTarget.clear();
		if(treeTarget.getChildren().size()>0)
			treeTarget.removeChild((Component) treeTarget.getChildren().get(0));
		treeTarget.clear();
		treeTarget.setMultiple(false);
		treeTarget.setCheckmark(false);		
		ArrayList<SimpleTreeNode> list= new ArrayList<SimpleTreeNode>();
		
		MTreeNode nodeProject=new MTreeNode(targetProject.get_ID(), 0,targetProject.getName()	, targetProject.getDescription(), 0, true,
				null, true, null);
		
		ArrayList<SimpleTreeNode> listPhase= new ArrayList<SimpleTreeNode>();
		MProjectPhase[] phases=targetProject.getPhases();
		for (MProjectPhase phase : phases)
		{	
			ArrayList<SimpleTreeNode> listTask= new ArrayList<SimpleTreeNode>();
			MTreeNode nodePhase=new MTreeNode(phase.get_ID(), phase.getSeqNo(),phase.getName()	, phase.getDescription(), targetProject.get_ID(), true,
					null, true, null);
			
			MProjectTask[] tasks=phase.getTasks();
			for (MProjectTask task : tasks)
			{
				ArrayList<SimpleTreeNode> listTaskLine= new ArrayList<SimpleTreeNode>();
				MTreeNode nodeTask=new MTreeNode(task.get_ID(), task.getSeqNo(),task.getName(), task.getDescription(), phase.get_ID(), true,
						null, true, null);
				
				MProjectLine[] lines=task.getLines();
				for(MProjectLine line: lines)
				{
					MTreeNode nodeTaskLine=new MTreeNode(line.get_ID(), line.getLine(),line.getDescription(), line.getDescription(), task.get_ID(), true,
							null, true, null);
					SimpleTreeNode simpleNodetaskLine=new SimpleTreeNode(nodeTaskLine,new ArrayList<Object>());
					listTaskLine.add(simpleNodetaskLine);
				}
				SimpleTreeNode simpleNodetask=new SimpleTreeNode(nodeTask,listTaskLine);
				listTask.add(simpleNodetask);
			}
			
			SimpleTreeNode simpleNodePhase=new SimpleTreeNode(nodePhase,listTask);			
			listPhase.add(simpleNodePhase);
					
		}	
	
		list.add(new SimpleTreeNode(nodeProject, listPhase));
		
		root=new SimpleTreeNode (targetProject, list);
	
		SimpleTreeModel model=new SimpleTreeModel(root);
//		treeTarget.setTreeitemRenderer(model);
		treeTarget.setModel(model);
		TreeUtils.expandAll(treeTarget);
	}

	@Override
	public boolean dynInit() throws Exception {
		MLookup lookupProject = MLookupFactory.get(Env.getCtx(), getGridTab()
				.getWindowNo(), getGridTab().getTabNo(), MColumn.getColumn_ID(
				MProject.Table_Name, "C_Project_ID"), DisplayType.TableDir);
		projectField = new WSearchEditor("C_Project_ID", true, false, true,
				lookupProject);

		projectField.addValueChangeListener(this);
		return true;
	}

	@Override
	public void info() {

	}

	@Override
	public boolean save(IMiniTable miniTable, String trxName) {
		return false;
	}

	public void valueChange(ValueChangeEvent e) {
		if (e.getPropertyName().equals("C_Project_ID")) {
			int C_Project_ID = e.getNewValue()==null?0:(Integer) e.getNewValue();
			if(C_Project_ID <= 0)
				return;
			selectedProject = new MProject(Env.getCtx(), C_Project_ID, null);

			generateTree();
		}

	}

	private void generateTree() {
		
		Treeitem item=treeTarget.getSelectedItem();
		if(item!=null)
		currentLevel=item.getLevel();
		treeSource.clear();
		if(treeSource.getChildren().size()>0)
			treeSource.removeChild((Component) treeSource.getChildren().get(0));
		selectedMap.clear();
		Treechildren rootTreeChild = new Treechildren();
		Treeitem rootTreeItem = new Treeitem();
		rootTreeItem.setValue(selectedProject.getC_Project_ID());
		rootTreeItem.setLabel(selectedProject.getName());
		if(currentLevel >= LEVEL_Project)
		{
			rootTreeItem.setCheckable(false);
			rootTreeItem.setDisabled(true);
			rootTreeItem.setStyle("color:#4E7AA5 !important");
		}

		rootTreeChild.appendChild(rootTreeItem);
		createNodes(rootTreeItem,selectedProject);
		treeSource.appendChild(rootTreeChild);
		treeSource.setMultiple(true);
		treeSource.setCheckmark(true);
	

	}

	private void createNodes(Treeitem rootTreeItem,MProject project) {

		Treechildren treeChildPhase = new Treechildren();

		MProjectPhase phases[] = project.getPhases();

		for (MProjectPhase phase : phases) {
			rootTreeItem.appendChild(treeChildPhase);
			Treeitem treeItemPhase = new Treeitem();
			treeChildPhase.appendChild(treeItemPhase);
			treeItemPhase.setLabel(phase.getName());
			treeItemPhase.setValue(phase.get_ID());
			if(currentLevel >= LEVEL_Phase)
			{
				treeItemPhase.setCheckable(false);
				treeItemPhase.setDisabled(true);
				treeItemPhase.setStyle("color:#4E7AA5 !important");
			}

			MProjectTask tasks[] = phase.getTasks();
			Treechildren treeChildTask = new Treechildren();
			for (MProjectTask task : tasks) {
				treeItemPhase.appendChild(treeChildTask);
				Treeitem treeItemTask = new Treeitem();
				treeChildTask.appendChild(treeItemTask);
				treeItemTask.setLabel(task.getName());
				treeItemTask.setValue(task.get_ID());
				if(currentLevel >= LEVEL_Task)
				{
					treeItemTask.setCheckable(false);
					treeItemTask.setDisabled(true);
					treeItemTask.setStyle("color:#4E7AA5 !important");
				}

				MProjectLine lines[] = task.getLines();
				Treechildren treeChildTaskLine = new Treechildren();
				for (MProjectLine line : lines) {
					treeItemTask.appendChild(treeChildTaskLine);
					Treeitem treeItemTaskLine = new Treeitem();
					treeChildTaskLine.appendChild(treeItemTaskLine);
					treeItemTaskLine.setLabel(line.getDescription());
					treeItemTaskLine.setValue(line.get_ID());
				}

			}
		}
	}

	Set<Object> selectedItems = new HashSet<Object>();
	Map<Object, Boolean> selectedMap = new HashMap<Object, Boolean>();
	int levelSource=0; 
	Treeitem selectedItem=null;
	@SuppressWarnings("unchecked")
	public void onEvent(Event e) throws Exception {
		if (e.getTarget().getId().equals(ConfirmPanel.A_OK)) {
			try {
//				Trx.run(new TrxRunnable() {
//					public void run(String trxName) {

						if (save())
						{
							window.dispose();
						}
//					}
//				});
			} catch (Exception ex) {
				FDialog.error(getGridTab().getWindowNo(), window, "Error",
						ex.getLocalizedMessage());
			}
		}
		// Cancel
		else if (e.getTarget().getId().equals(ConfirmPanel.A_CANCEL)) {
			window.dispose();
		} 
		else if (e.getTarget() instanceof Tree) {
			if(e.getTarget() == treeSource)
			{
				@SuppressWarnings("unused")
				Tree tree = (Tree) e.getTarget();
				Treeitem item = getChangedItem();
				if (item.getChildren().size() > 0) {
					selectChild(item);
					selectParent(item);
				}
			log.info(item.getValue() + item.getLabel());
			}
			else if(e.getTarget() == treeTarget && selectedProject !=null && selectedProject.get_ID() >0){
				Treeitem item=treeTarget.getSelectedItem();
				currentLevel=item.getLevel();
				generateTree();
				
			}
		}
			else if (e.getTarget() instanceof Button) {
				levelSource=0; 
				if(treeTarget.getSelectedCount() < 1)
					return;
				selectedItem=treeTarget.getSelectedItem();
				if(e.getTarget() == buttonLeft){
					
					if(treeSource.getSelectedCount() < 1)
						return;
					levelSource=selectedItem.getLevel();
					Set<Treeitem>  items=treeSource.getSelectedItems();
					SimpleTreeNode selectedNode=(SimpleTreeNode)selectedItem.getValue();
					
					for(Treeitem itemFrom :  items)
					{
						
						if(selectedItem.getLevel() == itemFrom.getLevel())
						{   
							SimpleTreeNode targetNode=(SimpleTreeNode)selectedItem.getParentItem().getValue();
							SimpleTreeNode exstingChild=getExsitingNode(itemFrom, targetNode);
							if(exstingChild!=null)
								continue;
							MTreeNode newNode=new MTreeNode((Integer)itemFrom.getValue(), 0, itemFrom.getLabel(), itemFrom.getLabel(),
									((MTreeNode)targetNode.getData()).getNode_ID(), false, null, false, null);
							SimpleTreeNode lineNode=new SimpleTreeNode(newNode, new ArrayList<Object>());
							targetNode.getChildren().add(lineNode);
							copiedLineNodes.add(lineNode);
							refreshTree();
						}
						else
						{
							/*if(removedNode.contains((SimpleTreeNode)itemFrom.getValue()))
								removedNode.remove(selectedNode);*/
							
							if(itemFrom.getLevel() == LEVEL_Phase){
								addPhase(itemFrom, selectedNode);
								
							}
							else if(itemFrom.getLevel() == LEVEL_Task){
								addTask(itemFrom,selectedNode);
								
							}
							else if(itemFrom.getLevel() == LEVEL_TaskLine){
								addTaskLine(itemFrom, selectedNode);
							}

						}				
					}
				}
				else if(e.getTarget() == buttonRight){
					
					removeFromTree(selectedItem);
				
				}
				else if(e.getTarget() == buttonUp){
					changeSeq(selectedItem,1);
				
				}
				else if(e.getTarget() == buttonDown ){
					changeSeq(selectedItem,-1);
				}
		}
		// Select All
		// Trifon
		
	}

	ArrayList<SimpleTreeNode> nodesChanged=new ArrayList<SimpleTreeNode>();
	private void changeSeq(Treeitem item,int replace) {
		if(item.getLevel() == LEVEL_Project)
			return;
		SimpleTreeNode sn=(SimpleTreeNode)item.getParentItem().getValue();
		SimpleTreeNode current=(SimpleTreeNode)item.getValue();
		@SuppressWarnings("unchecked")
		List<SimpleTreeNode> childList=sn.getChildren();
		int currentIndex=childList.indexOf(current);
		if((replace == 1  && currentIndex!=0) || ( replace == -1 && currentIndex!=childList.size()-1))
		{
			SimpleTreeNode prevItem=(SimpleTreeNode)childList.get(currentIndex-replace);
			childList.set(currentIndex, prevItem);
			childList.set(currentIndex-replace, current);
			refreshTree();
			if(!nodesChanged.contains((SimpleTreeNode)item.getValue()))
			{
				copiedSeqChanged.add(item);
				nodesChanged.add((SimpleTreeNode)item.getValue());
			}
		}	
	
	}

	ArrayList<SimpleTreeNode> removedNode=new ArrayList<SimpleTreeNode>();
	private void removeFromTree(Treeitem item)
	{
		while (item.getChildren().size() > 0) 
		{
				item.removeChild(item.getFirstChild());
		}
		
		int level=item.getLevel();
		SimpleTreeNode node=(SimpleTreeNode)item.getValue();
		
		SimpleTreeNode parentNode=(SimpleTreeNode)item.getParentItem().getValue();
		parentNode.getChildren().remove(node);
		boolean isRemoved=false;
		//check if in temp list(not in DB)
		if(level==1)
		{
			isRemoved=removePhase(node);
		}
		else if(level==2)
		{
			isRemoved=removetask(node);
		}
		else if(level==3)
		{
			isRemoved=removeLines(node);
		}
		if(!isRemoved)
			removedItems.add(item);
		
	}

	private boolean removePhase(SimpleTreeNode node) 
	{
		
		List<SimpleTreeNode> childList=node.getChildren();
		for(SimpleTreeNode nodeTask : childList )
		{
			removetask(nodeTask);
		}
		if(copiedPhaseNodes.contains(node))
		{
			copiedPhaseNodes.remove(node);
			return true;
		}
		return false;
		
	}

	private boolean removetask(SimpleTreeNode node)
	{
		List<SimpleTreeNode> childList=node.getChildren();
		for(SimpleTreeNode nodeLine : childList )
			removeLines(nodeLine);
		
		if(copiedTaskNodes.contains(node))
		{
			copiedTaskNodes.remove(node);
			return true;
		}
		return false;
	}

	private boolean  removeLines(SimpleTreeNode node) 
	{
		if(copiedLineNodes.contains(node))		
		{
			copiedLineNodes.remove(node);
			return true;
		}
		return false;
	}
	@SuppressWarnings("unchecked")
	private void addTaskLine(Treeitem itemFrom, 
			SimpleTreeNode taskChild) {
		
		SimpleTreeNode childTaskline=addTask(itemFrom.getParentItem(),  taskChild);		
		SimpleTreeNode exstingChild=getExsitingNode(itemFrom, childTaskline);
		if(exstingChild!=null)
			return;
		SimpleTreeNode lineToAdd = createNewNode(itemFrom,childTaskline);			
		childTaskline.getChildren().add(lineToAdd);
		refreshTree();
		copiedLineNodes.add(lineToAdd);
	}

	
	@SuppressWarnings("unchecked")
	private SimpleTreeNode addTask(Treeitem itemFrom, 
			SimpleTreeNode itemPh) {
		
		if(levelSource == LEVEL_Task)
			return itemPh;
		
		SimpleTreeNode childTask=addPhase(itemFrom.getParentItem(),  itemPh);
		
		SimpleTreeNode exstingChild=getExsitingNode(itemFrom,childTask);
		if(exstingChild!=null)
			return exstingChild;
			
		
		SimpleTreeNode taskToAdd = createNewNode(itemFrom,childTask);			
		childTask.getChildren().add(taskToAdd);
		refreshTree();
		copiedTaskNodes.add(taskToAdd);
		return taskToAdd;
	}	

	@SuppressWarnings("unchecked")
	private SimpleTreeNode addPhase(Treeitem itemFrom, 
			SimpleTreeNode childTop) 
	
	{
		if(levelSource == LEVEL_Phase)
			return childTop;
			
		SimpleTreeNode exstingChild=getExsitingNode(itemFrom,childTop);
		if(exstingChild!=null)
			return exstingChild;		
		
		SimpleTreeNode phaseToAdd = createNewNode(itemFrom,childTop);			
		childTop.getChildren().add(phaseToAdd);
		refreshTree();
		copiedPhaseNodes.add(phaseToAdd);
		return phaseToAdd;
	}

	@SuppressWarnings("unchecked")
	private SimpleTreeNode getExsitingNode(Treeitem itemFrom,SimpleTreeNode childTop)
	{
		List<SimpleTreeNode> nodes= childTop.getChildren();
		
		for(SimpleTreeNode nodeChild : nodes)
		{
			if(((MTreeNode)nodeChild.getData()).getName().equals(itemFrom.getLabel()))
				return nodeChild;
			
		}
		return null;
	}
	
	private SimpleTreeNode createNewNode(Treeitem itemFrom,SimpleTreeNode parentNode) {
		MTreeNode newNode=new MTreeNode((Integer)itemFrom.getValue(), 0, itemFrom.getLabel(), itemFrom.getLabel(),((MTreeNode) parentNode.getData()).getNode_ID(),
				true, null, false, null);
		SimpleTreeNode node=new SimpleTreeNode(newNode, new ArrayList<Object>());
		return node;
	}
	
	
	HashMap<Integer, MProjectPhase> phaseMap = new HashMap<Integer, MProjectPhase>();
	HashMap<Integer, MProjectTask> taskMap = new HashMap<Integer, MProjectTask>();
	HashMap<Integer, MProjectLine> lineMap = new HashMap<Integer, MProjectLine>();
	private boolean save() throws Exception {
		String trxName = Trx.createTrxName("CopyProject");
		Trx trx = Trx.get(trxName, true);
		
		try {
			if(copiedPhaseNodes.size() > 0)
			{
				for(SimpleTreeNode phaseNode : copiedPhaseNodes)
				{
					int phase_ID=((MTreeNode)phaseNode.getData()).getNode_ID();
					MProjectPhase phase=new MProjectPhase(Env.getCtx(), phase_ID, trxName);
					MProjectPhase phaseNew=new MProjectPhase(Env.getCtx(), 0, trxName);
					PO.copyValues(phase, phaseNew);
					MProject project=new MProject(Env.getCtx(), ((MTreeNode)phaseNode.getData()).getParent_ID(), trxName);
					phaseNew.setC_Project_ID(project.get_ID());
					phaseNew.setSeqNo(((project.getPhases().length)+1)*10);
					phaseNew.save();
					phaseMap.put(((MTreeNode)phaseNode.getData()).getNode_ID(), phaseNew);
				}
			}
			if(copiedTaskNodes.size() > 0)
			{
				for(SimpleTreeNode taskNode : copiedTaskNodes)
				{
					int task_ID=((MTreeNode)taskNode.getData()).getNode_ID();
					MProjectTask task=new MProjectTask(Env.getCtx(), task_ID, trxName);
					MProjectTask taskNew=new MProjectTask(Env.getCtx(), 0, trxName);
					PO.copyValues(task, taskNew);
					MProjectPhase phase=phaseMap.get(((MTreeNode)taskNode.getData()).getParent_ID());
					if(phase==null)
					{
						phase=new MProjectPhase(Env.getCtx(), ((MTreeNode)taskNode.getData()).getParent_ID(), trxName);
					}
						
					taskNew.setC_ProjectPhase_ID(phase.get_ID());
					taskNew.setSeqNo(((phase.getTasks().length)+1)*10);
					taskNew.save();
					taskMap.put(((MTreeNode)taskNode.getData()).getNode_ID(), taskNew);
					
				}
			}
			if(copiedLineNodes.size() > 0)
			{
				for(SimpleTreeNode lineNode : copiedLineNodes)
				{
					int line_ID=((MTreeNode)lineNode.getData()).getNode_ID();
					MProjectLine line=new MProjectLine(Env.getCtx(), line_ID, trxName);
					MProjectLine lineNew=new MProjectLine(Env.getCtx(), 0, trxName);
					PO.copyValues(line, lineNew);
					MProjectTask task=taskMap.get(((MTreeNode)lineNode.getData()).getParent_ID());
					if(task==null)
					{
						task=new MProjectTask(Env.getCtx(), ((MTreeNode)lineNode.getData()).getParent_ID() , trxName);
					}
					lineNew.setC_ProjectTask_ID(task.get_ID());
					lineNew.setLine(((task.getLines().length)+1)*10);
					lineNew.save();
					lineMap.put(((MTreeNode)lineNode.getData()).getNode_ID(), lineNew);
					
				}
			}		
			if(copiedSeqChanged.size() >0)
				changeInDB(trxName,true);
			if(removedItems.size() > 0)
				changeInDB(trxName,false);
			trx.commit();
		} catch (Exception e) {
			trx.rollback();
			throw e;
		}
		finally{
			trx.close();
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private void changeInDB(String trxName,boolean isSeqChanged) 
	{
		ArrayList<Treeitem> items=new ArrayList<Treeitem>();
		if(isSeqChanged)
			items=copiedSeqChanged;
		else
			items=removedItems;
		SimpleTreeNode nodeRemoved=null;
		for(Treeitem item: items)
		{
			int level=item.getParentItem().getLevel();
			if(!isSeqChanged)
				 nodeRemoved=(SimpleTreeNode)item.getValue();
			SimpleTreeNode node=(SimpleTreeNode)item.getParentItem().getValue();
			List<SimpleTreeNode> nodes=node.getChildren();
			if(level==0)
			{
				if(!isSeqChanged)
				{
					MTreeNode treeNode=(MTreeNode)nodeRemoved.getData();
					MProjectPhase phase=phaseMap.get(treeNode.getNode_ID());
					if(phase==null)
					{
						phase=new MProjectPhase(Env.getCtx(),treeNode.getNode_ID(), trxName);
					}
					phase.delete(true);
				}
				else
				{
					int i=1;
					for(SimpleTreeNode nodeChild : nodes)
					{
						MTreeNode treeNode=(MTreeNode)nodeChild.getData();
						MProjectPhase phase=phaseMap.get(treeNode.getNode_ID());
						if(phase==null)
						{
							phase=new MProjectPhase(Env.getCtx(),treeNode.getNode_ID(), trxName);
						}
					
						phase.setSeqNo(i*10);
						phase.setIsComplete(false);
						phase.save();
						i++;
					
					}				
				}
			}
			else if(level==1)
			{
				if(!isSeqChanged)
				{
					MTreeNode treeNode=(MTreeNode)nodeRemoved.getData();
					MProjectTask task=taskMap.get(treeNode.getNode_ID());
					if(task==null)
					{
						task=new MProjectTask(Env.getCtx(),treeNode.getNode_ID(), trxName);
					}
					task.delete(true);
				}
				else
				{
					int i=1;
					for(SimpleTreeNode nodeChild : nodes)
					{
						MTreeNode treeNode=(MTreeNode)nodeChild.getData();
						MProjectTask task=taskMap.get(treeNode.getNode_ID());
						if(task==null)
						{
							task=new MProjectTask(Env.getCtx(),treeNode.getNode_ID(), trxName);
						}
						task.set_ValueOfColumn("Task_Complete", "N");
						task.setSeqNo(i*10);
						task.save();
						i++;
					}
				}				
			}
			else if(level==2)
			{
				if(!isSeqChanged)
				{
					MTreeNode treeNode=(MTreeNode)nodeRemoved.getData();
					MProjectLine line=lineMap.get(treeNode.getNode_ID());
					if(line==null)
					{
						line=new MProjectLine(Env.getCtx(),treeNode.getNode_ID(), trxName);
					}
					line.delete(true);
				}
				else
				{
					int i=1;
					for(SimpleTreeNode nodeChild : nodes)
					{
						MTreeNode treeNode=(MTreeNode)nodeChild.getData();
						MProjectLine line=lineMap.get(treeNode.getNode_ID());
						if(line==null)
						{
							line=new MProjectLine(Env.getCtx(),treeNode.getNode_ID(), trxName);
						}
						line.setProcessed(false);
						line.setLine(i*10);
						line.save();
						i++;						
					}	
				}			
			}
		}		
	}

	
	private void selectChild(Treeitem item) {
		List<?> children = item.getChildren();
		for (Iterator<?> iterator = children.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			if (object instanceof Treechildren) {
				Collection<?> childItems = ((Treechildren) object).getItems();
				for (Object object2 : childItems) {
					if (object2 instanceof Treeitem) {
						Treeitem new_name = (Treeitem) object2;
						new_name.setSelected(item.isSelected());
						selectedMap.put(new_name, item.isSelected());
						selectChild(new_name);
					}
				}
			}
		}
	}

	private void selectParent(Treeitem item) {
		if(item.getParentItem() !=null && item.isSelected()==true)
		{
			if(item.getParentItem().isCheckable())
			{
				item.getParentItem().setSelected(item.isSelected());
				selectedMap.put(item.getParentItem(), item.isSelected());
			}
			selectParent(item.getParentItem());
		}
	}

	private Treeitem getChangedItem() {
		for (Object item : treeSource.getSelectedItems()) {
			if (!selectedMap.containsKey(item)) {
				selectedMap.put(item, true);
				adjustSelection();
				return (Treeitem) item;
			}
		}
		Set<?> selected = treeSource.getSelectedItems();
		for (Entry<Object, Boolean> entrySet : selectedMap.entrySet()) {
			if (!selected.contains(entrySet.getKey())
					&& entrySet.getValue() == true) {
				entrySet.setValue(false);
				adjustSelection();
				return (Treeitem) entrySet.getKey();
			}
			if (selected.contains(entrySet.getKey())
					&& entrySet.getValue() == false) {
				entrySet.setValue(true);
				adjustSelection();
				return (Treeitem) entrySet.getKey();
			}
		}
		return null;
	}

	private void adjustSelection() {
		Set<?> selected = treeSource.getSelectedItems();
		for (Entry<Object, Boolean> entrySet : selectedMap.entrySet()) {
			if (selected.contains(entrySet.getKey()))
				entrySet.setValue(true);
			else
				entrySet.setValue(false);
		}
	}

	public void refreshTree() 
	{
		treeTarget.setModel(treeTarget.getModel());
		TreeUtils.expandAll(treeTarget);
		SimpleTreeNode lastNode=(SimpleTreeNode)selectedItem.getValue();
		int[] path = treeTarget.getModel().getPath(root, lastNode);
		Treeitem ti = treeTarget.renderItemByPath(path);
		treeTarget.setSelectedItem(ti);
	}
}