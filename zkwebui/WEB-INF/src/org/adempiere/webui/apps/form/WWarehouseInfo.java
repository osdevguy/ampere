package org.adempiere.webui.apps.form;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Tab;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Tabpanels;
import org.adempiere.webui.component.Tabs;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.panel.StatusBarPanel;
import org.adempiere.webui.session.SessionManager;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MDocType;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MMovementLine;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProductionLine;
import org.compiere.model.MProjectIssue;
import org.compiere.model.MRole;
import org.compiere.util.CLogMgt;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;

/**
 * @author Nikunj Panelia
 */
public class WWarehouseInfo implements IFormController, EventListener
{

	private static CLogger		log					= CLogger.getCLogger(WWarehouseInfo.class);

	private Label				lblWarehouse;
	private Listbox				pickWarehouse;

	private Label				lblLocator;
	private Textbox				fieldLocator;

	private Label				lblValue;
	private Textbox				fieldValue;

	private Label				lblUPC;
	private Textbox				fieldUPC;
	
	private Label				lblProCat;
	private Listbox				fieldProCat;

	private Label				lblCurrentInvetory;
	private Checkbox			checkInventory;

	protected StatusBarPanel	statusBar			= new StatusBarPanel();

	private Borderlayout		borderlayout		= new Borderlayout();

	Tabbox						tabbedPane			= new Tabbox();
	WListbox					trxHistroryTbl		= ListboxFactory.newDataTable();
	String						m_sqlTrxHistory;
	WListbox					impActivityTbl		= ListboxFactory.newDataTable();
	String						m_sqlImpActivity;
	WListbox					purchaseTbl			= ListboxFactory.newDataTable();
	String						m_sqlPurchase;
	WListbox					salesTbl			= ListboxFactory.newDataTable();
	String						m_sqlSales;
	WListbox					shipmentTbl			= ListboxFactory.newDataTable();
	String						m_sqlShipment;
	WListbox					m_tableAtp			= ListboxFactory.newDataTable();

	ConfirmPanel				confirmPanel		= null;

	WListbox					miniTable			= ListboxFactory.newDataTable();
	private static ColumnInfo[]	s_warehouseLayout	= null;
	private CustomForm			form				= new CustomForm();

	private static final String	s_TableDataFrom		= "M_Storage s"
															+ " INNER JOIN M_Product p ON (p.M_Product_ID=s.M_Product_ID)"
															+ " LEFT OUTER JOIN M_Locator l ON (l.M_Locator_ID=s.M_Locator_ID)"
															+ " INNER JOIN M_Product_Category pc ON (pc.M_Product_Category_ID=p.M_Product_Category_ID)"
															+ " LEFT OUTER JOIN M_Warehouse w ON (w.M_Warehouse_ID=l.M_Warehouse_ID)"
															+ " LEFT OUTER JOIN M_AttributeSetInstance pai ON (s.M_AttributeSetInstance_ID=pai.M_AttributeSetInstance_ID)"
															+ " LEFT OUTER JOIN M_AttributeSet pa ON (pai.M_AttributeSet_ID=pa.M_AttributeSet_ID)";

	public WWarehouseInfo()
	{																			
		try
		{
			initComponents();
			dynInit();
			zkInit();

			tabbedPane.setSelectedIndex(0);
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "", e);
		}
	}

	/**
	 * initialize fields
	 */
	private void initComponents()
	{
		lblValue = new Label();
		lblValue.setValue(Util.cleanAmp(Msg.translate(Env.getCtx(), "Value")));

		lblWarehouse = new Label();
		lblWarehouse.setValue(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));

		lblLocator = new Label();
		lblLocator.setValue(Msg.translate(Env.getCtx(), "M_Locator_ID"));

		lblUPC = new Label();
		lblUPC.setValue(Msg.translate(Env.getCtx(), "UPC"));

		lblCurrentInvetory = new Label();
		lblCurrentInvetory.setValue("Current Inventory only");

		fieldValue = new Textbox();
		fieldValue.setMaxlength(40);
		fieldValue.addEventListener(Events.ON_CHANGE, this);

		fieldUPC = new Textbox();
		fieldUPC.setMaxlength(40);

		fieldLocator = new Textbox();
		fieldLocator.addEventListener(Events.ON_CHANGE, this);

		checkInventory = new Checkbox();
		checkInventory.addEventListener(Events.ON_CHECK, this);
		
		lblProCat = new Label();
		lblProCat.setValue(Msg.translate(Env.getCtx(), "M_Product_Category_ID"));

		fieldProCat = new Listbox();
		fieldProCat.setRows(0);
		fieldProCat.setMultiple(false);
		fieldProCat.setMold("select");
		fieldProCat.setWidth("150px");
		fieldProCat.addEventListener(Events.ON_SELECT, this);
		
		pickWarehouse = new Listbox();
		pickWarehouse.setRows(0);
		pickWarehouse.setMultiple(false);
		pickWarehouse.setMold("select");
		pickWarehouse.setWidth("150px");
		pickWarehouse.addEventListener(Events.ON_SELECT, this);

		confirmPanel = new ConfirmPanel(true, true, false, false, false, false);
		confirmPanel.addActionListener(Events.ON_CLICK, this);

		form.addEventListener(Events.ON_OK, this);

	} // initComponents

	private void zkInit()
	{
		Grid grid = GridFactory.newGridLayout();

		Rows rows = new Rows();
		grid.appendChild(rows);

		Row row = new Row();
		rows.appendChild(row);
		row.appendChild(lblWarehouse.rightAlign());
		row.appendChild(pickWarehouse);
		row.appendChild(lblLocator.rightAlign());
		row.appendChild(fieldLocator);
		row.appendChild(lblValue.rightAlign());
		row.appendChild(fieldValue);

		row = new Row();
		rows.appendChild(row);
		row.appendChild(lblProCat.rightAlign());
		row.appendChild(fieldProCat);
		row.appendChild(lblUPC.rightAlign());
		row.appendChild(fieldUPC);
		//row.appendChild(lblCurrentInvetory.rightAlign());
		//row.appendChild(checkInventory);

		row = new Row();
		rows.appendChild(row);
		row.appendChild(statusBar);
		row.setSpans("6");
		statusBar.setEastVisibility(false);

		String sqlDate = "t.movementdate, CASE WHEN COALESCE(t.m_inventoryline_id, 0) != 0 THEN t.m_inventoryline_id"
				+" WHEN COALESCE(t.m_movementline_id, 0) != 0 THEN t.m_movementline_id"
				+" WHEN COALESCE(t.m_inoutline_id, 0) != 0 THEN t.m_inoutline_id"	
				+" WHEN COALESCE(t.m_productionline_id, 0) != 0 THEN t.m_productionline_id"
				+" WHEN COALESCE(t.c_projectissue_id, 0) != 0 THEN t.c_projectissue_id"
				+" WHEN COALESCE(t.pp_cost_collector_id, 0) != 0 THEN t.pp_cost_collector_id END";
		
		String sqlDocument = "CASE WHEN COALESCE(t.m_inventoryline_id, 0) != 0 THEN (SELECT mi.documentno FROM m_inventoryline mil INNER JOIN m_inventory mi ON mil.m_inventory_id=mi.m_inventory_id WHERE mil.m_inventoryline_id=t.m_inventoryline_id)"
				+" WHEN COALESCE(t.m_movementline_id, 0) != 0 THEN (SELECT mm.documentno FROM m_movementline mml INNER JOIN m_movement mm ON mml.m_movement_id=mm.m_movement_id WHERE mml.m_movementline_id=t.m_movementline_id)"
				+" WHEN COALESCE(t.m_inoutline_id, 0) != 0 THEN (SELECT mi.documentno FROM m_inoutline mil INNER JOIN m_inout mi ON mil.m_inout_id=mi.m_inout_id WHERE mil.m_inoutline_id=t.m_inoutline_id)"
				+" WHEN COALESCE(t.m_productionline_id, 0) != 0 THEN (SELECT mp.documentno FROM m_productionline mpl INNER JOIN m_production mp ON mpl.m_production_id=mp.m_production_id WHERE mpl.m_productionline_id=t.m_productionline_id)"
				+" WHEN COALESCE(t.c_projectissue_id, 0) != 0 THEN (SELECT cp.value FROM c_projectissue cpi INNER JOIN c_project cp ON cpi.c_project_id=cp.c_project_id WHERE cpi.c_projectissue_id=t.c_projectissue_id)"
				+" WHEN COALESCE(t.pp_cost_collector_id, 0) != 0 THEN (SELECT documentno FROM pp_cost_collector ppcc WHERE ppcc.pp_cost_collector_id=t.pp_cost_collector_id) END";
		
		String sqlDocumentType = "CASE WHEN COALESCE(t.m_inventoryline_id, 0) != 0 THEN (SELECT cdt.name FROM m_inventoryline mil INNER JOIN m_inventory mi ON mil.m_inventory_id=mi.m_inventory_id INNER JOIN c_doctype cdt ON mi.c_doctype_id=cdt.c_doctype_id WHERE mil.m_inventoryline_id=t.m_inventoryline_id)"
				+" WHEN COALESCE(t.m_movementline_id, 0) != 0 THEN (SELECT cdt.name FROM m_movementline mml INNER JOIN m_movement mm ON mml.m_movement_id=mm.m_movement_id INNER JOIN c_doctype cdt ON mm.c_doctype_id=cdt.c_doctype_id WHERE mml.m_movementline_id=t.m_movementline_id)"
				+" WHEN COALESCE(t.m_inoutline_id, 0) != 0 THEN (SELECT cdt.name FROM m_inoutline mil INNER JOIN m_inout mi ON mil.m_inout_id=mi.m_inout_id INNER JOIN c_doctype cdt ON mi.c_doctype_id=cdt.c_doctype_id WHERE mil.m_inoutline_id=t.m_inoutline_id)"
				+" WHEN COALESCE(t.m_productionline_id, 0) != 0 THEN (SELECT 'Production'::character varying)"
				+" WHEN COALESCE(t.c_projectissue_id, 0) != 0 THEN (SELECT 'Project Issue'::character varying)"
				+" WHEN COALESCE(t.pp_cost_collector_id, 0) != 0 THEN (SELECT cdt.name FROM pp_cost_collector ppcc INNER JOIN c_doctype cdt ON ppcc.c_doctype_id=cdt.c_doctype_id WHERE ppcc.pp_cost_collector_id=t.pp_cost_collector_id) END";
		
		String sqlMovementQty = "movementqty, CASE WHEN COALESCE(t.m_inventoryline_id, 0) != 0 THEN (SELECT "+MInventoryLine.Table_ID+")"
				+" WHEN COALESCE(t.m_movementline_id, 0) != 0 THEN (SELECT "+MMovementLine.Table_ID+")"
				+" WHEN COALESCE(t.m_inoutline_id, 0) != 0 THEN (SELECT "+MInOutLine.Table_ID+")"
				+" WHEN COALESCE(t.m_productionline_id, 0) != 0 THEN (SELECT "+MProductionLine.Table_ID+")"
				+" WHEN COALESCE(t.c_projectissue_id, 0) != 0 THEN (SELECT "+MProjectIssue.Table_ID+") END";
		
		ColumnInfo[] s_layoutWarehouse = new ColumnInfo[] {
				new ColumnInfo(Msg.translate(Env.getCtx(), "M_Locator_ID"), "l.value", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Document"), sqlDocument, String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Document Type"), sqlDocumentType, String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Date"), sqlDate, KeyNamePair.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "MovementQty"), sqlMovementQty, KeyNamePair.class) };
		/** From Clause */
		String s_sqlFrom = " M_Transaction t ";
		s_sqlFrom = s_sqlFrom + " LEFT JOIN M_Locator l on t.M_locator_ID=l.M_Locator_ID ";
		/** Where Clause */
		String s_sqlWhere = "t.M_Product_ID = ?";
		m_sqlTrxHistory = trxHistroryTbl.prepareTable(s_layoutWarehouse, s_sqlFrom, s_sqlWhere, false, "t");
		m_sqlTrxHistory+= " Order By t.movementdate DESC";
		trxHistroryTbl.setMultiSelection(false);
		trxHistroryTbl.autoSize();
		trxHistroryTbl.setMold("paging");
		trxHistroryTbl.setPageSize(20);
		trxHistroryTbl.setPagingPosition("top");
		trxHistroryTbl.addEventListener(Events.ON_DOUBLE_CLICK, this);

		ColumnInfo[] s_layoutOrder = new ColumnInfo[] {
				new ColumnInfo("Order Line", "p.documentno||'-'||l.line", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Date"), "p.dateacct", Timestamp.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "QtyOrdered"), "l.qtyordered", Double.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "QtyDelivered"), "l.qtydelivered", Double.class)};
		/** From Clause */
		s_sqlFrom = " C_OrderLine l ";
		s_sqlFrom = s_sqlFrom + " INNER JOIN C_Order p on p.C_Order_ID=l.C_Order_ID ";
		/** Where Clause */
		s_sqlWhere = "l.M_Product_ID = ? AND p.isSoTrx='N' ";
		m_sqlPurchase = purchaseTbl.prepareTable(s_layoutOrder, s_sqlFrom, s_sqlWhere, false, "l");
		m_sqlPurchase+= " Order By p.dateacct DESC";
		purchaseTbl.setMultiSelection(false);
		purchaseTbl.autoSize();
		purchaseTbl.addEventListener(Events.ON_DOUBLE_CLICK, this);
		
		//sales tab
		
		s_sqlWhere = "l.M_Product_ID = ? AND p.isSoTrx='Y' ";
		m_sqlSales = salesTbl.prepareTable(s_layoutOrder, s_sqlFrom, s_sqlWhere, false, "l");
		m_sqlSales+= " Order By p.dateacct DESC";
		salesTbl.setMultiSelection(false);
		salesTbl.autoSize();
		salesTbl.addEventListener(Events.ON_DOUBLE_CLICK, this);
		
		//Shipment Tab
		ColumnInfo[] s_layoutShioment = new ColumnInfo[] {
				new ColumnInfo( "Shipment Line", "i.documentno||'-'||l.line", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "M_Locator_ID"), "loc.value", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Date"), "i.movementdate", Timestamp.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "MovementQty"), "l.movementqty", Double.class) };
		/** From Clause */
		s_sqlFrom = " M_InoutLine l ";
		s_sqlFrom = s_sqlFrom + " INNER JOIN M_Inout i on i.M_Inout_ID=l.M_Inout_ID ";
		s_sqlFrom = s_sqlFrom + " LEFT JOIN M_Locator loc on l.M_Locator_ID=loc.M_Locator_ID ";
		/** Where Clause */
		s_sqlWhere = "l.M_Product_ID = ? AND i.isSoTrx='Y' ";
		m_sqlShipment = shipmentTbl.prepareTable(s_layoutShioment, s_sqlFrom, s_sqlWhere, false, "l");
		m_sqlShipment+= " Order By i.movementdate DESC";
		shipmentTbl.setMultiSelection(false);
		shipmentTbl.autoSize();
		shipmentTbl.addEventListener(Events.ON_DOUBLE_CLICK, this);

		tabbedPane.setHeight("100%");
		Tabpanels tabPanels = new Tabpanels();
		tabbedPane.appendChild(tabPanels);
		Tabs tabs = new Tabs();
		tabbedPane.appendChild(tabs);

		Tab tab = new Tab("Transaction History");
		tabs.appendChild(tab);
		Tabpanel desktopTabPanel = new Tabpanel();
		desktopTabPanel.setHeight("100%");
		desktopTabPanel.appendChild(trxHistroryTbl);
		tabPanels.appendChild(desktopTabPanel);

		tab = new Tab("Purchase Order");
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		desktopTabPanel.setHeight("100%");
		desktopTabPanel.appendChild(purchaseTbl);
		tabPanels.appendChild(desktopTabPanel);
		
		tab = new Tab(Msg.getMsg(Env.getCtx(), "ATP"));
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		desktopTabPanel.setHeight("100%");
		desktopTabPanel.appendChild(m_tableAtp);
		tabPanels.appendChild(desktopTabPanel);
		
		tab = new Tab("Sales Orders");
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		desktopTabPanel.setHeight("100%");
		desktopTabPanel.appendChild(salesTbl);
		tabPanels.appendChild(desktopTabPanel);

		tab = new Tab("Shipment");
		tabs.appendChild(tab);
		desktopTabPanel = new Tabpanel();
		desktopTabPanel.setHeight("100%");
		desktopTabPanel.appendChild(shipmentTbl);
		tabPanels.appendChild(desktopTabPanel);

		borderlayout.setWidth("100%");
		borderlayout.setHeight("100%");

		int height = SessionManager.getAppDesktop().getClientInfo().desktopHeight * 90 / 100;

		Center center = new Center();
		center.setAutoscroll(true);
		center.setFlex(true);
		borderlayout.appendChild(center);
		center.appendChild(miniTable);
		South south = new South();
		int detailHeight = (height * 25 / 100);
		south.setHeight(detailHeight + "px");
		south.setCollapsible(true);
		south.setSplittable(true);
		south.setFlex(true);
		south.setTitle("Warehouse Information");
		borderlayout.appendChild(south);
		south.appendChild(tabbedPane);
		Borderlayout mainPanel = new Borderlayout();
		mainPanel.setWidth("100%");
		mainPanel.setHeight("100%");
		North north = new North();
		mainPanel.appendChild(north);
		north.appendChild(grid);
		center = new Center();
		mainPanel.appendChild(center);
		center.appendChild(borderlayout);
		south = new South();
		mainPanel.appendChild(south);
		south.appendChild(confirmPanel);
		south.setHeight("38px");
		// center.appendChild(borderlayout);

		form.appendChild(mainPanel);
	}

	private void dynInit()
	{
		// Warehouse
		String SQL = MRole.getDefault().addAccessSQL(
				"SELECT M_Warehouse_ID, Value || ' - ' || Name AS ValueName " + "FROM M_Warehouse "
						+ "WHERE IsActive='Y'", "M_Warehouse", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
						+ " AND AD_Client_ID=?"
						+ (Env.getAD_Org_ID(Env.getCtx()) != 0 ? " AND AD_Org_ID=?" : "")
				+ " ORDER BY Value";
		pickWarehouse.appendItem("", new Integer(0));
		PreparedStatement pstmt = DB.prepareStatement(SQL, null);
		ResultSet rs = null;
		try
		{
			pstmt.setInt(1, Env.getAD_Client_ID(Env.getCtx()));
			if(Env.getAD_Org_ID(Env.getCtx()) != 0)
			{
				pstmt.setInt(2, Env.getAD_Org_ID(Env.getCtx()));
			}
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				pickWarehouse.appendItem(rs.getString("ValueName"), new Integer(rs.getInt("M_Warehouse_ID")));
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, e.getLocalizedMessage());
		}
		
		
		SQL = MRole.getDefault().addAccessSQL(
				"SELECT M_Product_Category_ID, Value  AS ValueName " + "FROM M_Product_Category "
						+ "WHERE IsActive='Y'", "M_Product_Category", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
				+ " ORDER BY Value";
		fieldProCat.appendItem("", new Integer(0));
		pstmt = DB.prepareStatement(SQL, null);
		try
		{
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				fieldProCat.appendItem(rs.getString("ValueName"), new Integer(rs.getInt("M_Product_Category_ID")));
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, e.getLocalizedMessage());
		}


		miniTable.addEventListener(Events.ON_SELECT, this);
		loadProducts();
	}

	private String	sql	= null;

	private void loadProducts()
	{
		String where = getSQLWhere();
		sql = miniTable.prepareTable(getWarehouseLayout(), s_TableDataFrom, where, false, "s", false);
		sql+= " Order By w.name, l.value";
		Vector<Vector> data = new Vector<Vector>();
		PreparedStatement pstmt = DB.prepareStatement(sql, null);
		ResultSet rs = null;
		try
		{
			setParameters(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>(20);
				line.add(new IDColumn(rs.getInt(1))); // project id
				line.add(rs.getString(2));
				line.add(rs.getString(3));
				line.add(rs.getString(4));
				line.add(rs.getBigDecimal(5));
				line.add(rs.getBigDecimal(6));
				line.add(rs.getBigDecimal(7));
				line.add(rs.getString(8));

				data.add(line);
			}
			loadTable(data);

		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, e.getLocalizedMessage());
		}
		finally
		{
			DB.close(rs, pstmt);
		}
	}

	/**
	 * Load data in table
	 * 
	 * @param data
	 */
	private void loadTable(Vector<Vector> data)
	{
		Vector<String> columnNames = null;
		miniTable.clear();
		columnNames = new Vector<String>(8);
		columnNames.add("Select");
		columnNames.add("Warehouse");
		columnNames.add("Locator");
		columnNames.add("Product");
		columnNames.add("Qty Available");
		columnNames.add("Qty On Hand");
		columnNames.add("Qty Reserved");
		columnNames.add("Attribute");

		ListModelTable model = new ListModelTable(data);

		miniTable.setData(model, columnNames);

		miniTable.setColumnClass(0, IDColumn.class, false, "");
		miniTable.setColumnClass(1, String.class, true);
		miniTable.setColumnClass(2, String.class, true);
		miniTable.setColumnClass(3, String.class, true);
		miniTable.setColumnClass(4, BigDecimal.class, true);
		miniTable.setColumnClass(5, BigDecimal.class, true);
		miniTable.setColumnClass(6, BigDecimal.class, true);
		miniTable.setColumnClass(7, String.class, true);

		miniTable.setMultiSelection(false);
		//miniTable.autoSize();
		miniTable.addActionListener(this);
		miniTable.repaint();
		ListHeader header=(ListHeader)miniTable.getListHead().getChildren().get(0);
		header.setWidth("30px");
	}

	protected ColumnInfo[] getWarehouseLayout()
	{
		if (s_warehouseLayout != null)
			return s_warehouseLayout;
		//
		s_warehouseLayout = null;
		ArrayList<ColumnInfo> list = new ArrayList<ColumnInfo>();
		list.add(new ColumnInfo(" ", "p.M_Product_ID", IDColumn.class));

		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "M_Warehouse_ID"), "w.Name", String.class));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "M_Locator_ID"), "l.Value", String.class));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "Product"), "p.Value||'-'||p.Name", String.class));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "QtyAvailable"),
				"bomQtyAvailable(p.M_Product_ID,w.M_Warehouse_ID,l.M_Locator_ID) AS QtyAvailable", Double.class, true, true, null));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "QtyOnHand"), "s.QtyOnHand", Double.class));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "QtyReserved"), "s.QtyReserved", Double.class));
		list.add(new ColumnInfo(Msg.translate(Env.getCtx(), "M_AttributeSetInstance_ID"), "productAttribute(pai.M_AttributeSetInstance_ID)", String.class));

		s_warehouseLayout = new ColumnInfo[list.size()];
		list.toArray(s_warehouseLayout);

		return s_warehouseLayout;
	} // getWarehouseLayout

	public String getSQLWhere()
	{
		StringBuffer where = new StringBuffer();
		where.append(" p.IsActive='Y' ");
		ListItem listitem = pickWarehouse.getSelectedItem();
		if (Integer.valueOf(listitem.getValue().toString()) > 0)
		{
			where.append(" AND w.M_Warehouse_ID=? ");
		}
		
		listitem = fieldProCat.getSelectedItem();
		if (Integer.valueOf(listitem.getValue().toString()) > 0)
		{
			where.append(" AND pc.M_Product_Category_ID=? ");
		}
		// => Value
		String value = fieldValue.getText().toUpperCase();
		if (!value.equals("") && value.indexOf("%") == -1)
		{
			where.append(" AND UPPER(p.Value) = ?");
		}
		else if (!value.equals("") && value.indexOf("%") > -1)
			where.append(" AND UPPER(p.Value) LIKE ?");
		
		value = fieldUPC.getText().toUpperCase();
		if (!value.equals("") && value.indexOf("%") == -1)
		{
			where.append(" AND UPPER(p.UPC) = ?");
		}
		else if (!value.equals("") && value.indexOf("%") > -1)
			where.append(" AND UPPER(p.UPC) LIKE ?");

		// => Locator
		String locator = fieldLocator.getText().toUpperCase();
		if (!locator.equals("") && locator.indexOf("%") == -1)
		{
			where.append(" AND UPPER(l.Value) = ?");
		}
		else if (!locator.equals("") && locator.indexOf("%") > -1)
			where.append(" AND UPPER(l.Value) LIKE ?");

		where.append(" AND w.AD_Client_ID=?");
		if(Env.getAD_Org_ID(Env.getCtx()) != 0)
			where.append(" AND w.AD_Org_ID=?");
		return where.toString();
	}

	/**
	 * Set Parameters for Query (as defined in getSQLWhere) *
	 * 
	 * @param pstmt pstmt
	 * @throws SQLException
	 */
	protected void setParameters(PreparedStatement pstmt) throws SQLException
	{
		int index = 1;

		// => Warehouse
		int M_Warehouse_ID = 0;
		ListItem listitem = pickWarehouse.getSelectedItem();
		if (Integer.valueOf(listitem.getValue().toString()) > 0)
		{
			M_Warehouse_ID = (Integer) listitem.getValue();
			pstmt.setInt(index++, M_Warehouse_ID);
		}
		int m_ProCat_ID = 0;
		listitem = fieldProCat.getSelectedItem();
		if (Integer.valueOf(listitem.getValue().toString()) > 0)
		{
			m_ProCat_ID = (Integer) listitem.getValue();
			pstmt.setInt(index++, m_ProCat_ID);
		}
		// => Value
		String value = fieldValue.getText().toUpperCase();
		if (!value.equals(""))
		{

			pstmt.setString(index++, value);

		}
		value = fieldUPC.getText().toUpperCase();
		if (!value.equals(""))
		{

			pstmt.setString(index++, value);

		}
		// => Locator
		String locator = fieldLocator.getText().toUpperCase();
		if (!locator.equals(""))
		{
			pstmt.setString(index++, locator);

		}
		
		pstmt.setInt(index++, Env.getAD_Client_ID(Env.getCtx()));
		if(Env.getAD_Org_ID(Env.getCtx()) != 0)
		{
			pstmt.setInt(index++, Env.getAD_Org_ID(Env.getCtx()));
		}

	} // setParameters

	private void onRefresh(int m_product_ID)
	{
		String sql = m_sqlTrxHistory;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, (Integer) m_product_ID);
			rs = pstmt.executeQuery();
			trxHistroryTbl.loadTable(rs);
			rs = pstmt.executeQuery();

		}
		catch (Exception e)
		{
			log.log(Level.WARNING, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		sql = m_sqlPurchase;

		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, (Integer) m_product_ID);
			rs = pstmt.executeQuery();
			purchaseTbl.loadTable(rs);
			rs = pstmt.executeQuery();

		}
		catch (Exception e)
		{
			log.log(Level.WARNING, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}		
		
		sql = m_sqlSales;

		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, (Integer) m_product_ID);
			rs = pstmt.executeQuery();
			salesTbl.loadTable(rs);
			rs = pstmt.executeQuery();

		}
		catch (Exception e)
		{
			log.log(Level.WARNING, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		sql = m_sqlShipment;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, (Integer) m_product_ID);
			rs = pstmt.executeQuery();
			shipmentTbl.loadTable(rs);
			rs = pstmt.executeQuery();

		}
		catch (Exception e)
		{
			log.log(Level.WARNING, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		initAtpTab(m_product_ID);
	}
	
	/**
	 *	Query ATP
	 */
	private void initAtpTab (int m_M_Product_ID)
	{
		//	Header
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.translate(Env.getCtx(), "Date"));
		columnNames.add(Msg.translate(Env.getCtx(), "QtyOnHand"));
		columnNames.add(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "QtyOrdered"));
		columnNames.add(Msg.translate(Env.getCtx(), "QtyReserved"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_Locator_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_AttributeSetInstance_ID"));
		columnNames.add(Msg.translate(Env.getCtx(), "DocumentNo"));
		columnNames.add(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));

		//	Fill Storage Data
		String sql = "SELECT SUM(s.QtyOnHand), SUM(s.QtyReserved), SUM(s.QtyOrdered),"
				+ " productAttribute(s.M_AttributeSetInstance_ID), 0,";
		sql += " w.Name, l.Value "
			+ "FROM M_Storage s"
			+ " INNER JOIN M_Locator l ON (s.M_Locator_ID=l.M_Locator_ID)"
			+ " INNER JOIN M_Warehouse w ON (l.M_Warehouse_ID=w.M_Warehouse_ID) "
			+ "WHERE M_Product_ID=?";
	
		sql += " AND (s.QtyOnHand<>0 OR s.QtyReserved<>0 OR s.QtyOrdered<>0)";
		sql += " GROUP BY productAttribute(s.M_AttributeSetInstance_ID), w.Name, l.Value";
		sql += " ORDER BY l.Value";

		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		double qty = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_M_Product_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>(9);
				line.add(null);							//  Date
				double qtyOnHand = rs.getDouble(1);
				qty += qtyOnHand;
				line.add(new Double(qtyOnHand));  		//  Qty
				line.add(null);							//  BPartner
				line.add(new Double(rs.getDouble(3)));  //  QtyOrdered
				line.add(new Double(rs.getDouble(2)));  //  QtyReserved
				line.add(rs.getString(7));      		//  Locator
				String asi = rs.getString(4);
				line.add(asi);							//  ASI
				line.add(null);							//  DocumentNo
				line.add(rs.getString(6));  			//	Warehouse
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		//	Orders
		sql = "SELECT o.DatePromised, ol.QtyReserved,"
			+ " productAttribute(ol.M_AttributeSetInstance_ID), ol.M_AttributeSetInstance_ID,"
			+ " dt.DocBaseType, bp.Name,"
			+ " dt.PrintName || ' ' || o.DocumentNo As DocumentNo, w.Name "
			+ "FROM C_Order o"
			+ " INNER JOIN C_OrderLine ol ON (o.C_Order_ID=ol.C_Order_ID)"
			+ " INNER JOIN C_DocType dt ON (o.C_DocType_ID=dt.C_DocType_ID)"
			+ " INNER JOIN M_Warehouse w ON (ol.M_Warehouse_ID=w.M_Warehouse_ID)"
			+ " INNER JOIN C_BPartner bp  ON (o.C_BPartner_ID=bp.C_BPartner_ID) "
			+ "WHERE ol.QtyReserved<>0"
			+ " AND ol.M_Product_ID=?";
		
		sql += " ORDER BY o.DatePromised";
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, m_M_Product_ID);
			
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>(9);
				line.add(rs.getTimestamp(1));			//  Date
				double oq = rs.getDouble(2);
				String DocBaseType = rs.getString(5);
				Double qtyReserved = null;
				Double qtyOrdered = null;
				if (MDocType.DOCBASETYPE_PurchaseOrder.equals(DocBaseType))
				{
					qtyOrdered = new Double(oq);
					qty += oq;
				}
				else
				{
					qtyReserved = new Double(oq);
					qty -= oq;
				}
				line.add(new Double(qty)); 		 		//  Qty
				line.add(rs.getString(6));				//  BPartner
				line.add(qtyOrdered);					//  QtyOrdered
				line.add(qtyReserved);					//  QtyReserved
				line.add(null);				      		//  Locator
				String asi = rs.getString(3);
				line.add(asi);							//  ASI
				line.add(rs.getString(7));				//  DocumentNo
				line.add(rs.getString(8));  			//	Warehouse
				data.add(line);
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		//  Table
		ListModelTable model = new ListModelTable(data);
		m_tableAtp.setData(model, columnNames);
		//
		m_tableAtp.setColumnClass(0, Timestamp.class, true);   //  Date
		m_tableAtp.setColumnClass(1, Double.class, true);      //  Quantity
		m_tableAtp.setColumnClass(2, String.class, true);      //  Partner
		m_tableAtp.setColumnClass(3, Double.class, true);      //  Quantity
		m_tableAtp.setColumnClass(4, Double.class, true);      //  Quantity
		m_tableAtp.setColumnClass(5, String.class, true);   	  //  Locator
		m_tableAtp.setColumnClass(6, String.class, true);   	  //  ASI
		m_tableAtp.setColumnClass(7, String.class, true);      //  DocNo
		m_tableAtp.setColumnClass(8, String.class, true);   	  //  Warehouse
		//
		m_tableAtp.autoSize();
	}	//	initAtpTab

	@Override
	public void onEvent(Event event) throws Exception
	{
		Component component = event.getTarget();
		if(component.equals(trxHistroryTbl))
		{
			KeyNamePair keynamePair =(KeyNamePair) trxHistroryTbl.getValueAt(trxHistroryTbl.getSelectedRow(), 4);
			int tableId = keynamePair.getKey();
			keynamePair = (KeyNamePair) trxHistroryTbl.getValueAt(trxHistroryTbl.getSelectedRow(), 3);
			int recordId = keynamePair.getKey();
			AEnv.zoom(tableId, recordId);
		}
		if(component.equals(purchaseTbl))
		{
			String orderLine =(String) purchaseTbl.getValueAt(purchaseTbl.getSelectedRow(), 0);
			String[] array = orderLine.split("-");
			String sql = "SELECT c_orderline_id FROM c_orderline ol INNER JOIN c_order o ON ol.c_order_id=o.c_order_id WHERE o.documentno like ? AND ol.line=? AND ol.AD_Client_ID=?";
			int recordId = DB.getSQLValue(null, sql, array[0],Integer.parseInt(array[1]),Env.getAD_Client_ID(Env.getCtx()));
			AEnv.zoom(MOrderLine.Table_ID, recordId);
		}
		if(component.equals(salesTbl))
		{
			String orderLine =(String) salesTbl.getValueAt(salesTbl.getSelectedRow(), 0);
			String[] array = orderLine.split("-");
			String sql = "SELECT c_orderline_id FROM c_orderline ol INNER JOIN c_order o ON ol.c_order_id=o.c_order_id WHERE o.documentno like ? AND ol.line=? AND ol.AD_Client_ID=?";
			int recordId = DB.getSQLValue(null, sql, array[0],Integer.parseInt(array[1]),Env.getAD_Client_ID(Env.getCtx()));
			AEnv.zoom(MOrderLine.Table_ID, recordId);
		}
		if(component.equals(shipmentTbl))
		{
			String shipmentLine =(String) shipmentTbl.getValueAt(shipmentTbl.getSelectedRow(), 0);
			String[] array = shipmentLine.split("-");
			String sql = "SELECT m_inoutline_id FROM m_inoutline iol INNER JOIN m_inout io ON iol.m_inout_id=io.m_inout_id WHERE io.documentno like ? AND iol.line=? AND iol.AD_Client_ID=?";
			int recordId = DB.getSQLValue(null, sql, array[0],Integer.parseInt(array[1]),Env.getAD_Client_ID(Env.getCtx()));
			AEnv.zoom(MInOutLine.Table_ID, recordId);
		}
		if (component.equals(pickWarehouse) || component.equals(fieldProCat)  || component.equals(confirmPanel.getButton(ConfirmPanel.A_REFRESH)))
		{
			loadProducts();
		}
		if (component.equals(miniTable) && miniTable.getSelectedRow() >= 0)
		{
			IDColumn column = (IDColumn) miniTable.getValueAt(miniTable.getSelectedRow(), 0);
			onRefresh(column.getRecord_ID());
		}
		else
			loadProducts();
	}

	@Override
	public ADForm getForm()
	{
		return form;
	}
}