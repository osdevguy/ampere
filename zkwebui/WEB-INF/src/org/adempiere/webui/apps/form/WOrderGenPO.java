/******************************************************************************
 * Copyright (C) 2009 Low Heng Sin                                            *
 * Copyright (C) 2009 Idalica Corporation                                     *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.apps.form;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;

import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Datebox;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.IFormController;
import org.compiere.apps.form.OrderGenPO;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MLookupInfo;
import org.compiere.model.MOrder;
import org.compiere.model.MRole;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Space;

/**
 * Generate Invoice (manual) view class
 * 
 */
public class WOrderGenPO extends OrderGenPO implements IFormController, EventListener, ValueChangeListener, WTableModelListener
{
	private WGenForm form;
	
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(WOrderGenPO.class);
	//
	private Label lWarehouse = new Label("Warehouse");
	private Listbox cmbWarehouse = ListboxFactory.newDropdownListbox();
	private Label lCurrentBP = new Label("Curr Supplier");
	private Listbox cmbCurrentBP = ListboxFactory.newDropdownListbox();
	private Label     lPCat = new Label("Product Category");
	private Listbox  cmbPCat = ListboxFactory.newDropdownListbox();
	private Label	lIsComplete = new Label("Is Complete");
	private Checkbox chkIsComplete = new Checkbox();
	private Checkbox chkIsUrgentOnly = new Checkbox();
	private Label lSupplyHorizon = new Label("Supply Horizon");
	private Datebox dateSupplyHorizon = new Datebox();
	private Label lPriority = new Label("Priority");
	private WTableDirEditor cmbPriority;

	
	private Label lBuyFrom = new Label("Buy From");
	private Listbox cmbBuyFrom = ListboxFactory.newDropdownListbox();
	private Label lETA = new Label("ETA");
	private Datebox dateETA = new Datebox();
	
	private Label lBlank = new Label(".  .");
	private Button btnCheckAll = new Button("Check/Uncheck All");
	private Button btnUpdate = new Button("Update");
	private Button btnGenerate = new Button("Generate");
	private Button btnAddProduct = new Button("Add Product");

	private Label lProduct = new Label("Product");
	private WSearchEditor fProduct;
	int old_m_warehouse_id = 0;

	
	public WOrderGenPO()
	{
		log.info("");
		
		form = new WGenForm(this);
		Env.setContext(Env.getCtx(), form.getWindowNo(), "IsSOTrx", "Y");
		
		try
		{
			super.dynInit();
			dynInit();
			zkInit();
			
			form.postQueryEvent();
		}
		catch(Exception ex)
		{
			log.log(Level.SEVERE, "init", ex);
		}
	}	//	init
	
	/**
	 *	Static Init.
	 *  <pre>
	 *  selPanel (tabbed)
	 *      fOrg, fBPartner
	 *      scrollPane & miniTable
	 *  genPanel
	 *      info
	 *  </pre>
	 *  @throws Exception
	 */
	void zkInit() throws Exception
	{
		
		chkIsUrgentOnly.setText("Urgent Only");

		Row row = form.getParameterPanel().newRows().newRow();
		row.appendChild(new Space());
		row.appendChild(chkIsUrgentOnly);
		row = new Row();
		form.getParameterPanel().getRows().appendChild(row);		
		row.appendChild(lPriority.rightAlign());
		row.appendChild(cmbPriority.getComponent());
		row.appendChild(lSupplyHorizon.rightAlign());
		row.appendChild(dateSupplyHorizon);

		row = new Row();
		form.getParameterPanel().getRows().appendChild(row);		
		row.appendChild(lWarehouse.rightAlign());
		row.appendChild(cmbWarehouse);
		row.appendChild(new Space());
		row.appendChild(new Space());
//		row.appendChild(lBlank);
		row.appendChild(btnGenerate);
		row = new Row();
		form.getParameterPanel().getRows().appendChild(row);
		row.appendChild(lCurrentBP.rightAlign());
		row.appendChild(cmbCurrentBP);
		row.appendChild(lPCat.rightAlign());
		row.appendChild(cmbPCat);
		row.appendChild(btnCheckAll);

		row = new Row();
		form.getParameterPanel().getRows().appendChild(row);
		row.appendChild(lBuyFrom.rightAlign());
		row.appendChild(cmbBuyFrom);
		row.appendChild(lETA.rightAlign());
		row.appendChild(dateETA);
		row.appendChild(btnUpdate);


		row = new Row();
		form.getParameterPanel().getRows().appendChild(row);
		row.appendChild(lProduct.rightAlign());
		row.appendChild(fProduct.getComponent());
		row.appendChild(new Space());
		row.appendChild(new Space());
		row.appendChild(btnAddProduct);
		row = new Row();
		form.getParameterPanel().getRows().appendChild(row);
		row.appendChild(new Space());
	}	//	jbInit

	/**
	 *	Fill Picks.
	 *		Column_ID from C_Order
	 *  @throws Exception if Lookups cannot be initialized
	 */
	public void dynInit() throws Exception
	{
		MLookup pL = MLookupFactory.get (Env.getCtx(), form.getWindowNo(), 1, 2221, DisplayType.Search);		
		fProduct = new WSearchEditor ("M_Product_ID", true, false, true, pL);
		fProduct.addValueChangeListener(this);
		
		MLookupInfo priorityL = MLookupFactory.getLookup_List( Env.getLanguage(Env.getCtx()), 154);
		cmbPriority = new WTableDirEditor("Priority", true, false, true, new MLookup(priorityL, 0));
		cmbPriority.setValue(MOrder.PRIORITYRULE_High);
		cmbPriority.setReadWrite(false);
		dateSupplyHorizon.setEnabled(false);
		
		
		//Warehouse
		String SQL = "SELECT M_Warehouse_ID, Name FROM M_Warehouse WHERE IsActive='Y' ";
		
		int ad_org_id = Env.getAD_Org_ID(Env.getCtx());
		
		if (ad_org_id != 0) {
			SQL = SQL + " AND ad_org_id = " + ad_org_id + " ";
		}
		SQL = MRole.getDefault().addAccessSQL (
				SQL,
					"M_Warehouse", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
				+ " ORDER BY Name";
			for (KeyNamePair kn : DB.getKeyNamePairs(SQL, true)) {
				cmbWarehouse.addItem(kn);
			}		
		cmbWarehouse.addActionListener(this);
		
		cmbCurrentBP.addItem(new KeyNamePair(-1, ""));
		cmbPCat.addItem(new KeyNamePair(-1, ""));
		cmbBuyFrom.addItem(new KeyNamePair(-1, ""));
		
		//Current BP / Vendor
		//loadCurrentSupplier();		

		//Product Category
        lPCat.setText("Product Category");
        //loadProductCategory();		
        
        btnCheckAll.addActionListener(this);
        btnUpdate.addActionListener(this);
        btnGenerate.addActionListener(this);
        btnAddProduct.addActionListener(this);
        
        chkIsUrgentOnly.addActionListener(this);
        //TODO status bar
        form.getStatusBar().setStatusLine("Status bar here");
		
        cmbCurrentBP.setWidth("225px");
        cmbWarehouse.setWidth("225px");
        cmbBuyFrom.setWidth("225px");
        cmbPCat.setWidth("225px");
        btnCheckAll.setWidth("175px");
        btnCheckAll.setVisible(false);
        btnUpdate.setWidth("175px");
        btnGenerate.setWidth("175px");
        btnGenerate.setVisible(false);
        btnAddProduct.setWidth("175px");
        
        form.getMiniTable().getModel().addTableModelListener(this);
	}	//	fillPicks
    
	private void loadCurrentSupplier() {
		cmbCurrentBP.removeAllItems();
		String sql = "SELECT c_bpartner_id, name " +
	             "FROM c_bpartner " +
	             "WHERE c_bpartner_id IN " +
	             "(SELECT DISTINCT(current_c_bpartner_id) FROM t_replenish_po " +
	             "where AD_PInstance_ID = " + pi.getAD_PInstance_ID() + ") " +
	             //" ) " +
	             " ORDER BY Name";		
//		String SQL = MRole.getDefault().addAccessSQL (
//				"SELECT C_BPartner_ID, Name FROM C_BPartner WHERE IsActive='Y' AND IsVendor='Y'",
//					"C_BPartner", MRole.SQL_NOTQUALIFIED, MRole.SQL_RO)
//				+ " ORDER BY Name";
		for (KeyNamePair kn : DB.getKeyNamePairs(sql, true)) {
			cmbCurrentBP.addItem(kn);
		}
		
        cmbCurrentBP.addActionListener(this);
	}

	private void loadProductCategory() {
		cmbPCat.removeAllItems();
		String sql = "SELECT DISTINCT (pc.m_product_category_id), pc.name \n" +
	             "FROM m_product_category pc  \n" +
	             "  INNER JOIN m_product p ON pc.m_product_category_id = p.m_product_category_id  \n" +
	             "  INNER JOIN t_replenish_po r ON p.m_product_id = r.m_product_id \n" +
	             "WHERE r.AD_PInstance_ID = " + pi.getAD_PInstance_ID() + " ORDER BY pc.name";
			for (KeyNamePair kn : DB.getKeyNamePairs(sql, true)) {
				cmbPCat.addItem(kn);
			}
	        cmbPCat.addActionListener(this);
	}

	private void loadPOSupplier() {
		cmbBuyFrom.removeAllItems();
		//cmbBuyFrom.removeActionListener(this)
		String sql = "SELECT DISTINCT (p.c_bpartner_id), p.name " +
	             "FROM m_product_po po " +
	             "  INNER JOIN c_bpartner p ON po.c_bpartner_id = p.c_bpartner_id " +
	             "  INNER JOIN t_replenish_po r ON po.m_product_id = r.m_product_id " +
	             "WHERE r.AD_PInstance_ID = " + pi.getAD_PInstance_ID() + " ORDER BY p.name";
			for (KeyNamePair kn : DB.getKeyNamePairs(sql, true)) {
				cmbBuyFrom.addItem(kn);
			}
	        cmbBuyFrom.addActionListener(this);
	}

	/**
	 *  Query Info
	 */
	public void executeQuery()
	{
		
		int m_product_category_id = 0;
		int current_cbpartner_id = 0;
		int m_warehouse_id = 0;
		
		KeyNamePair pCatKNPair = cmbPCat.getSelectedItem().toKeyNamePair();
		if (pCatKNPair != null)
		{
			m_product_category_id = pCatKNPair.getKey();
			m_product_category_id = m_product_category_id == -1 ? 0 : m_product_category_id; 
		}
		
		KeyNamePair currentBPNPair = cmbCurrentBP.getSelectedItem().toKeyNamePair();
		if (currentBPNPair != null) {
			current_cbpartner_id = currentBPNPair.getKey();
			current_cbpartner_id = current_cbpartner_id == -1 ? 0 : current_cbpartner_id; 
		}
		
		KeyNamePair warehouseNPair = cmbWarehouse.getSelectedItem().toKeyNamePair();
		if (warehouseNPair != null )
		{
			m_warehouse_id = warehouseNPair.getKey();
			m_warehouse_id = m_warehouse_id == -1 ? 0 : m_warehouse_id; 
		}
		Timestamp ts = null;
		if (dateSupplyHorizon.getValue() != null) {
			ts = new Timestamp(dateSupplyHorizon.getValue().getTime());
		}
		executeQuery(0, m_warehouse_id, current_cbpartner_id, m_product_category_id, ts, form.getMiniTable());

		
//		executeQuery(warehouseNPair.getKey(), currentBPNPair.getKey(), pCatKNPair.getKey(), form.getMiniTable());		

		//executeQuery(docTypeKNPair, form.getMiniTable());
		form.getMiniTable().repaint();
//		form.invalidate();
	}   //  executeQuery

	/**
	 *	Action Listener
	 *  @param e event
	 */
	public void onEvent(Event e)
	{
		log.info("Cmd=" + e.getTarget().getId());

		if (chkIsUrgentOnly.equals(e.getTarget()))
		{
			if (!chkIsUrgentOnly.isSelected()) {
				dateSupplyHorizon.setValue(null);
				cmbPriority.setReadWrite(false);
				dateSupplyHorizon.setEnabled(false);
				return;
			}
			cmbPriority.setReadWrite(true);
			dateSupplyHorizon.setEnabled(true);
			Timestamp ts = new Timestamp((new Date()).getTime());
			Calendar cal = Calendar.getInstance();
			cal.setTime(ts);
			cal.add(Calendar.DAY_OF_WEEK, getM_supplyHorizon());
			dateSupplyHorizon.setValue(cal.getTime());
			return;
		}
		if (cmbWarehouse.equals(e.getTarget())) 
		{
			KeyNamePair warehouseNPair = cmbWarehouse.getSelectedItem().toKeyNamePair();

			if (warehouseNPair == null) { 
				return;
			}
			if (old_m_warehouse_id == warehouseNPair.getKey()) {
				return ;
			}
			old_m_warehouse_id = warehouseNPair.getKey();
			int priority = Integer.parseInt(cmbPriority.getValue().toString());
			createReplenishment(warehouseNPair.getKey(), pi.getAD_PInstance_ID(), chkIsUrgentOnly.isSelected(), priority);
			//executeQuery();
			loadCurrentSupplier();
			loadProductCategory();
			loadPOSupplier();
			form.postQueryEvent();
			return;
		}
		if (cmbPCat.equals(e.getTarget()) ||
			  cmbCurrentBP.equals(e.getTarget()))
		{
//			if (cmbPCat.getSelectedItem() == null) {
//				return;
//			}

			executeQuery();
			return;
		}
		
		if (btnCheckAll.equals(e.getTarget())) {
			checkAll(form.getMiniTable());
			return;
		}
//		if (cmbBuyFrom.equals(e.getTarget())) {
//			updatePOSupplier(form.getMiniTable(),
//					Integer.parseInt( cmbBuyFrom
//							.getSelectedItem().toKeyNamePair().getID()),
//							cmbBuyFrom
//							.getSelectedItem().toKeyNamePair().getName());
//			executeQuery();
//			cmbBuyFrom.setSelectedIndex(0);
//			return;
//		}
//		if (btnUpdate.equals(e.getTarget())) {
//			if (dateETA.getValue() != null)	{
//				updateETA(form.getMiniTable(), new Timestamp(dateETA.getValue().getTime()));
//			}
//			saveUpdates(form.getMiniTable());
//			executeQuery();
//			return;
//		}
		
		if (btnGenerate.equals(e.getTarget())) {
			generate();
			executeQuery();
			return;
		}
		
		if (cmbPCat.equals(e.getTarget()) || cmbCurrentBP.equals(e.getTarget())
				|| cmbWarehouse.equals(e.getTarget())) {
			form.postQueryEvent();
			return;
		}

		if (btnCheckAll.equals(e.getTarget())) {
			checkAll(form.getMiniTable());
			return;
		}
		if (cmbBuyFrom.equals(e.getTarget())) {
			updatePOSupplier(
					form.getMiniTable(),
					Integer.parseInt(cmbBuyFrom.getSelectedItem()
							.toKeyNamePair().getID()),
							cmbBuyFrom.getSelectedItem()
							.toKeyNamePair().getName());
			//form.postQueryEvent();  no need to update
			return;
		}
		if (btnUpdate.equals(e.getTarget())) {
			if (dateETA.getValue() != null) {
				updateETA(form.getMiniTable(), new Timestamp(dateETA.getValue()
						.getTime()));
			}
			//saveUpdates(form.getMiniTable());
			//form.postQueryEvent();  no need to update
			return;
		}
		
		if (btnAddProduct.equals(e.getTarget())) {
			addProduct();
			//form.postQueryEvent();  no need to update
			return;
		}

		if (ConfirmPanel.A_OK.equals(e.getTarget())) {
			return;
		}
		//
		validate();
	}	//	actionPerformed
	
	public void validate()
	{
//		form.saveSelection();
//		
//		ArrayList<Integer> selection = getSelection();
//		if (selection != null && selection.size() > 0 && isSelectionActive())
//			form.generate();
//		else
//			form.dispose();
		generate();
		executeQuery();
		
	}

	/**
	 *	Value Change Listener - requery
	 *  @param e event
	 */
	public void valueChange(ValueChangeEvent e)
	{
		log.info(e.getPropertyName() + "=" + e.getNewValue());
		if (e.getPropertyName().equals("AD_Org_ID"))
			m_M_Warehouse_ID = e.getNewValue();
		if (e.getPropertyName().equals("C_BPartner_ID"))
		{
			m_Current_C_BPartner_ID = e.getNewValue();
			cmbCurrentBP.setValue(m_Current_C_BPartner_ID);	//	display value
		}
		form.postQueryEvent();
	}	//	vetoableChange

	public void addProduct() {
		int M_Product_ID = 0;
		
		if (fProduct.getValue() != null) {
			M_Product_ID = (Integer) fProduct.getValue();
		}
		int po_ID = addProducttoReplenishment(M_Product_ID, old_m_warehouse_id,  pi.getAD_PInstance_ID());
		if ( po_ID !=0)
		{
			form.setTableOnQuery(true);
			Timestamp ts = null;
			if (dateSupplyHorizon.getValue() != null) {
				ts = new Timestamp(dateSupplyHorizon.getValue().getTime());
			}
			executeQuery(po_ID, 0, 0, 0, ts, form.getMiniTable());
			form.setTableOnQuery(false);
			form.getMiniTable().setSelectedIndex(form.getMiniTable().getRowCount() - 1);
		}
			
		log.fine("Add Product M_Product_ID=" + M_Product_ID );
	}
	
	public String generate()
	{
		return generate(form.getStatusBar());
	}
	
	public ADForm getForm()
	{
		return form;
	}


	public void tableChanged(WTableModelEvent event) {
		if (!form.isTableOnQuery()) {
			int row = event.getLastRow();
			if (event.getColumn() != 0) {
				log.fine("Update database - row#" +  row);
				saveRow(form.getMiniTable(), row);
			}
		}
	}
}