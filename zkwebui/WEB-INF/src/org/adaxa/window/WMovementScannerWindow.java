package org.adaxa.window;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adaxa.window.IOLineEditorDialog.Mode;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WStringEditor;
import org.adempiere.webui.editor.WYesNoEditor;
import org.adempiere.webui.panel.StatusBarPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ITheme;
import org.adempiere.webui.window.FDialog;
import org.adempiere.webui.window.SimplePDFViewer;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MPInstance;
import org.compiere.model.MPackage;
import org.compiere.model.MPackageLine;
import org.compiere.model.MPrintConfig;
import org.compiere.model.MProduct;
import org.compiere.model.Query;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.ValueNamePair;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Space;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;

/**
 * Based on Shipment scanner
 * 
 * @author Nikunj Panelia
 */
public class WMovementScannerWindow extends Window implements EventListener
{

	private static final long				serialVersionUID		= 2408550162834024611L;
	private static CLogger					log						= CLogger.getCLogger(WMovementScannerWindow.class);

	private static final String				MOVEMENT_WINDOW_BUTTON1			= "MOVEMENT_WINDOW_BUTTON1";
	private static final String				MOVEMENT_WINDOW_BUTTON2			= "MOVEMENT_WINDOW_BUTTON2";
	private static final String				MOVEMENT_WINDOW_BUTTON3			= "MOVEMENT_WINDOW_BUTTON3";
	/** Window No */
	private int								m_WindowNo				= 0;

	private HashMap<Integer, BigDecimal>	mapProductScanned		= new HashMap<Integer, BigDecimal>();
	private HashMap<Integer, BigDecimal>	mapProductToScan		= new HashMap<Integer, BigDecimal>();
	private HashMap<String, Integer>		mapProductSerial		= new HashMap<String, Integer>();

	private MMovement						movement				= null;
	private Properties						ctx						= null;
	private MProduct						product					= null;
	private boolean							isRemoveMode			= false;
	List<MPackage>							packList				= new ArrayList<MPackage>();
	List<MPackageLine>						packLines				= new ArrayList<MPackageLine>();
	ArrayList<MMovementLine>				moveLines				= new ArrayList<MMovementLine>();
	private String							m_trxName				= null;
	private int								totalItem				= 0;

//	private int								address_PrintFormat_ID	= 0;
//	private int								packing_PrintFormat_ID	= 0;
//	private int								missing_PrintFormat_ID	= 0;
	
	MPrintConfig 							m_PrintConfig1 			= null;
	MPrintConfig 							m_PrintConfig2 			= null;
	MPrintConfig 							m_PrintConfig3 			= null;
			
	// GUI
	private WListbox						table					= ListboxFactory.newDataTable();
	private Borderlayout					mainPanel				= new Borderlayout();
	private Grid							northPanel				= GridFactory.newGridLayout();
	private Grid							southPanel				= GridFactory.newGridLayout();
	private Grid							edgePanel				= GridFactory.newGridLayout();

	private Label							lMovementInfo			= new Label();
	private Label							lProductInfo			= new Label();
	private Label							lUPC					= new Label();
	private Label							lSerial					= new Label();
	private Label							lmode					= new Label();
	private Label							lProductKey				= new Label();

	private Label							lMessage				= new Label();
	private Label							lScanned				= new Label();
	private Label							lShort					= new Label();
	private Div								panelScanned			= new Div();
	private Div								panelShort				= new Div();
	private Div								panelMessage			= new Div();

	private WStringEditor					fUPC					= new WStringEditor();
	private WStringEditor					fSerial					= new WStringEditor();
	private WStringEditor					fProductKey				= new WStringEditor();
	private Div								modePanel				= new Div();
	private WYesNoEditor					cRemoveMode				= null;
	private Button							bPrintButton1				= null;
	private Button							bPrintButton2			= null;
	private Button							bPrintButton3			= null;
	private Button							bDelete					= null;
	private Button							bSave					= null;
	private Button							bComplete				= null;
	private MPInstance						pInstance;

	private int								AD_Process_ID			= 0;

	private StatusBarPanel					statusBar				= new StatusBarPanel();

	private ProcessInfo						pInfo;
	private int								m_M_Locator_ID;

	public WMovementScannerWindow(int windowNo, MMovement movementHeader)
	{
		m_WindowNo = windowNo;
		movement = movementHeader;
		ctx = movementHeader.getCtx();
		m_trxName = Trx.createTrxName("MovementScanner");
		movement.set_TrxName(m_trxName);
		for (MMovementLine iol : movement.getLines(true))
		{
			moveLines.add(iol);
		}
		try
		{
			init();
			dynInit();
		}
		catch (Exception ex)
		{
			log.log(Level.SEVERE, ex.getMessage());
		}
	}

	/**
	 * Static Init
	 * 
	 * @throws Exception
	 */

	void init() throws Exception
	{
		this.setWidth("1000px");
		this.setBorder("normal");
		this.setHeight("700px");
		this.setClosable(true);
		this.setTitle("Movement Scanner");
		this.setAttribute("mode", "modal");
		this.setSizable(true);
		this.appendChild(mainPanel);

		// North
		North north = new North();
		north.appendChild(northPanel);
		northPanel.setHeight("150px");
		mainPanel.appendChild(north);
		Rows rows = northPanel.newRows();
		// First Row
		Row row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(lMovementInfo);

		// Third Row
		row = rows.newRow();
		row.appendChild(new Space());
		row = rows.newRow();
		row.appendChild(new Space());

		row.appendChild(lUPC.rightAlign());
		row.appendChild(fUPC.getComponent());
		row.appendChild(lProductKey.rightAlign());
		row.appendChild(fProductKey.getComponent());
		// row.appendChild(new Space());
		row.appendChild(lSerial.rightAlign());
		row.appendChild(fSerial.getComponent());
		row.appendChild(new Space());
		// row.setSpans("1,1,1,1,1,2");

		// Second Row
		row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(lProductInfo);
		row.appendChild(new Space());
		row.setSpans("1,4");

		// Space Row
		// row = rows.newRow();
		// row.appendChild(new Space());

		// Fourth Row
		cRemoveMode = new WYesNoEditor("RemoveMode", "Remove Mode", "Scanned Items are removed", false, false, true);
		row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(modePanel);
		row.appendChild(cRemoveMode.getComponent());
		modePanel.appendChild(lmode);
		lmode.setStyle(WWindowScannerStyle.STATUS_TEXT_STYLE);

		modePanel.setStyle(WWindowScannerStyle.STATUS_NORMAL_BACKGROUND_STYLE);
		cRemoveMode.getComponent().addActionListener(this);
		row.setSpans("1,4,2");
		modePanel.setHeight("30px");

		// Center
		Center center = new Center();
		center.appendChild(table);
		mainPanel.appendChild(center);

		// South
		South south = new South();
		Vbox vbox = new Vbox();
		vbox.setWidth("100%");
		vbox.appendChild(southPanel);
		south.appendChild(vbox);

		rows = southPanel.newRows();
		row = rows.newRow();
		row.appendChild(new Space());
		Hbox buttonPanel = new Hbox();
		bDelete = createButton("Delete", "Delete", "Delete");

		buttonPanel.appendChild(bDelete);
		
		m_PrintConfig1 = MPrintConfig.get(MOVEMENT_WINDOW_BUTTON1);
		m_PrintConfig2 = MPrintConfig.get(MOVEMENT_WINDOW_BUTTON2);
		m_PrintConfig3 = MPrintConfig.get(MOVEMENT_WINDOW_BUTTON3);
		if (m_PrintConfig1.isVisible()) {
			bPrintButton1 = createButton(m_PrintConfig1.getValue(), "Print1", m_PrintConfig1.getName());
			buttonPanel.appendChild(bPrintButton1);
			bPrintButton1.setEnabled(m_PrintConfig1.getAD_PrintFormat_ID() != 0);
				
		}
		if (m_PrintConfig2.isVisible()) {
			bPrintButton2 = createButton(m_PrintConfig2.getValue(), "Print2", m_PrintConfig2.getName());
			buttonPanel.appendChild(bPrintButton2);
			bPrintButton2.setEnabled(m_PrintConfig2.getAD_PrintFormat_ID() != 0);
		}
		if (m_PrintConfig3.isVisible()) {
			bPrintButton3 = createButton(m_PrintConfig3.getValue(), "Print3", m_PrintConfig3.getName());
			buttonPanel.appendChild(bPrintButton3);
			bPrintButton3.setEnabled(m_PrintConfig3.getAD_PrintFormat_ID() != 0);
		}

		row.appendChild(buttonPanel);

		row.appendChild(new Space());
		lMessage.setStyle(WWindowScannerStyle.MESSAGE_TEXT_STYLE);
		panelMessage.setStyle(WWindowScannerStyle.STATUS_MESSAGE_BACKGROUND_STYLE);
		row.appendChild(new Space());
		row.appendChild(panelMessage);
		panelMessage.appendChild(lMessage);

		row.appendChild(new Space());
		bComplete = createButton("Complete", "Process", "Complete Shipment");
		row.appendChild(bComplete);
		row.appendChild(new Space());
		bSave = createButton("Save & Exit", "Save", "Save & Exit");
		row.appendChild(bSave);

		bSave.setDisabled(false);
		bComplete.setDisabled(false);
		bDelete.setDisabled(false);


		mainPanel.appendChild(south);

		lScanned.setValue("Scanned=3");
		lShort.setValue("Total=0");
		lScanned.setStyle(WWindowScannerStyle.PANEL_TEXT_STYLE);
		lShort.setStyle(WWindowScannerStyle.PANEL_TEXT_STYLE);

		panelScanned.setStyle(WWindowScannerStyle.PANEL_SCANNED_STYLE);
		panelScanned.appendChild(lScanned);

		// TODO LAYOUT
		panelShort.setStyle(WWindowScannerStyle.PANEL_SHORT_STYLE);
		panelShort.appendChild(lShort);

		rows = edgePanel.newRows();
		// First Row
		row = rows.newRow();
		row.setSpans("3");
		row.appendChild(panelScanned);
		row.appendChild(panelShort);
		vbox.appendChild(edgePanel);
		mainPanel.appendChild(south);

		lMessage.setWidth("400px");

		String sql = "SELECT t.ad_process_id FROM ad_tab t  INNER JOIN ad_table l ON l.ad_table_id = t.ad_table_id WHERE l.tablename = 'M_Movement'";
		AD_Process_ID = DB.getSQLValue(null, sql);
		pInstance = new MPInstance(Env.getCtx(), 0, "");
		pInstance.setAD_Process_ID(AD_Process_ID);
		pInstance.setRecord_ID(movement.getM_Movement_ID());
		pInstance.setAD_Client_ID(Env.getAD_Client_ID(ctx));
		pInstance.save();

		pInfo = new ProcessInfo("Print Label", AD_Process_ID);
		pInfo.setRecord_ID(movement.getM_Movement_ID());
		pInfo.setAD_PInstance_ID(pInstance.getAD_PInstance_ID());
		pInfo.setAD_Client_ID(Env.getAD_Client_ID(ctx));

	}

	private void dynInit()
	{

		StringBuilder movementInfo = new StringBuilder();

		this.addEventListener(Events.ON_CLOSE, this);

		movementInfo.append("Movement# ").append(movement.getDocumentNo());

		lMovementInfo.setText(movementInfo.toString());
		lMovementInfo.setStyle(WWindowScannerStyle.DOCUMENT_TEXT_STYLE);

		lProductInfo.setStyle(WWindowScannerStyle.PRODUCT_TEXT_STYLE);
		lUPC.setText(Msg.translate(ctx, "UPC"));
		lSerial.setText(Msg.translate(ctx, "Serial"));
		lProductKey.setText("Product Key");

		cRemoveMode.setValue(false);
		lmode.setText("ITEMS SCANNED WILL BE ADDED");

		fUPC.getComponent().addEventListener(Events.ON_OK, this);
		fSerial.getComponent().addEventListener(Events.ON_OK, this);
		fProductKey.getComponent().addEventListener(Events.ON_OK, this);

		ColumnInfo[] layout = new ColumnInfo[] { new ColumnInfo(" ", ".", IDColumn.class, false, false, ""),
				new ColumnInfo(Msg.translate(Env.getCtx(), "M_Product_ID"), ".", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "PCode"), ".", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Locator To"), ".", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Movement Qty"), ".", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Target Qty"), ".", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Serial"), ".", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Notes"), ".", String.class) };

		table.prepareTable(layout, "", "", false, "");
		table.setSclass("scannerlist");
		table.addEventListener(Events.ON_SELECT, this);
		layout[4].setColWidth(30);
		statusBar.setStatusDB("");

		String sql = "select sum(targetqty) from M_MovementLine where isActive='Y' and M_Movement_ID = ?";
		totalItem = DB.getSQLValue(null, sql, movement.getM_Movement_ID());
		lShort.setValue("Total=" + totalItem);

		fUPC.getComponent().focus();
		lMessage.setValue("");
		reloadTable();

	}

	// reload table from database
	private void reloadTable()
	{

		table.setRowCount(0);
		table.clear();
		String sql = "SELECT ml.M_MovementLine_ID,p.value as PCode,p.name as PName,l.value AS Locator,asi.serno, ml.movementqty,ml.targetqty"
				+ " , ml.description "
				+ " FROM M_MovementLine ml"
				+ " INNER JOIN M_Product p ON p.M_Product_ID=ml.M_Product_ID"
				+ " LEFT JOIN M_Locator l ON  l.M_Locator_ID=ml.m_LocatorTo_ID"
				+ " LEFT JOIN M_AttributeSetInstance asi on asi.m_attributesetinstance_id=ml.m_attributesetinstance_id"
				+ " WHERE ml.M_Movement_ID=? ";

		PreparedStatement pstmt = DB.prepareStatement(sql, m_trxName);
		ResultSet rs = null;
		try
		{
			pstmt.setInt(1, movement.getM_Movement_ID());
			rs = pstmt.executeQuery();

			while (rs.next())
			{

				addRow(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getBigDecimal(6), rs.getBigDecimal(7), true, rs.getString(8));
			}

		}
		catch (Exception e)
		{
			log.severe(e.getMessage());
		}
		finally
		{
			DB.close(rs, pstmt);
		}
		WListItemRenderer renderer = (WListItemRenderer) table.getItemRenderer();
		ArrayList<ListHeader> headers = renderer.getHeaders();
		headers.get(0).setWidth("30px");
		headers.get(1).setWidth("100px");
		headers.get(2).setWidth("100px");
		headers.get(3).setWidth("70px");
		headers.get(4).setWidth("40px");
		headers.get(5).setWidth("40px");
		headers.get(6).setWidth("50px");
		headers.get(7).setWidth("80px");
		updateItemCount();
		table.repaint();
		resetHashMap();
	}

	/**
	 * Add Receipt Line to table, if exists, just update with new value
	 * 
	 * @param m_movementline_id
	 * @param pCode
	 * @param pName
	 * @param locator
	 * @param serNo
	 * @param QtyToScan
	 * @param QtyScanned
	 * @param noCheck - for efficiency, no need to check when loading for the
	 *            first time
	 * @return row Position
	 */

	private int addRow(int m_movementline_id, String pCode, String pName, String locator, String serNo,
			BigDecimal QtyToScan, BigDecimal QtyScanned, boolean noCheck, String notes)
	{
		int numrows = table.getItemCount();
		int row = 0;
		if (!noCheck)
		{
			for (int i = 0; i < numrows; i++)
			{
				if (m_movementline_id == ((IDColumn) table.getValueAt(i, 0)).getRecord_ID())
				{
					row = i;
					break;
				}
			}

		}
		if (row == 0)
		{
			row = numrows;
			table.setRowCount(row + 1);
			table.setValueAt(new IDColumn(m_movementline_id), row, 0);
		}

		table.setValueAt(pCode, row, 1);
		table.setValueAt(pName, row, 2);
		table.setValueAt(locator, row, 3); // locator
		table.setValueAt(QtyToScan, row, 4);
		table.setValueAt(QtyScanned, row, 5);
		table.setValueAt(serNo, row, 6);
		table.setValueAt(notes, row, 7);
		table.repaint();
		return row;

	}

	private Button createButton(String name, String image, String tooltip)
	{
		Button btn = new Button("");
		btn.setName("btn" + name);
		btn.setImage(ITheme.IMAGE_FOLDER + image + "24.png");
		btn.setTooltiptext(tooltip);

		LayoutUtils.addSclass("action-button", btn);
		btn.setHeight("50px");
		btn.setWidth("60px");
		btn.setTabindex(0);
		btn.addEventListener(Events.ON_CLICK, this);
		btn.addActionListener(this);
		btn.setDisabled(true);

		return btn;
	}

	public void onEvent(Event event) throws Exception
	{
		if (Events.ON_CLICK.equals(event.getName()))
		{
			if (event.getTarget() == bSave)
			{
				onExit();
			}
			else if (event.getTarget() == bComplete)
			{
				onComplete();
			}
			else if (event.getTarget() == bDelete)
			{
				onDelete();
			}
			else if (event.getTarget() == bPrintButton1)
			{
				//onPrintLabel();
				print(m_PrintConfig1);
			}
			else if (event.getTarget() == bPrintButton2)
			{
				//onPrintPackingList();
				print(m_PrintConfig2);
			}
			else if (event.getTarget() == bPrintButton3)
			{
				//onPrintMissingList();
				print(m_PrintConfig3);
			}

		}
		else if (Events.ON_SELECT.equals(event.getName()))
		{
			if (event.getTarget() == this.table)
			{
				onEditRow();
				table.clearSelection();
			}
		}
		else if (Events.ON_OK.equals(event.getName()))
		{
			if (event.getTarget() == fSerial.getComponent())
			{
				onSerial();
			}
			else if (event.getTarget() == fUPC.getComponent())
			{
				onProductScan(fUPC.getComponent());
			}
			else if (event.getTarget() == fProductKey.getComponent())
			{
				onProductScan(fProductKey.getComponent());
			}
		}
		else if (Events.ON_CHECK.equals(event.getName()))
		{
			if (event.getTarget() == cRemoveMode.getComponent())
			{
				onModeTogle();
			}
		}
		else if (Events.ON_CLOSE.equals(event.getName()))
		{
			onExit();
		}
	}

	private void onModeTogle()
	{
		isRemoveMode = (Boolean) cRemoveMode.getValue();
		if (isRemoveMode)
		{
			modePanel.setStyle(WWindowScannerStyle.STATUS_HIGHLIGHTED_BACKGROUND_STYLE);
			lmode.setText("ITEMS SCANNED WILL BE REMOVED FROM THE LIST");
		}
		else
		{
			modePanel.setStyle(WWindowScannerStyle.STATUS_NORMAL_BACKGROUND_STYLE);
			lmode.setText("ITEMS SCANNED WILL BE ADDED");
		}
	}

	private void onEditRow()
	{
		int r = table.getSelectedRow();
		int M_MovementLine_ID = ((IDColumn) table.getValueAt(r, 0)).getRecord_ID();
		MMovementLine line = null;
		for (MMovementLine moveLine : moveLines)
		{
			if (moveLine.getM_MovementLine_ID() == M_MovementLine_ID)
			{
				line = moveLine;
				break;
			}
		}
		MProduct product = (MProduct) line.getM_Product();
		lProductInfo.setText(product.getValue() + "-" + product.getName());
		String serial = (String) table.getValueAt(r, 6);
		String locator = (String) table.getValueAt(r, 3);
		String notes = (String) table.getValueAt(r, 7);
		BigDecimal startQty = null; //(BigDecimal) line.getTargetQty().subtract(line.getMovementQty());
//		if (isRemoveMode)
			startQty = line.getMovementQty();
		boolean isSerial = false;
		if (serial != null)
		{
			isSerial = true;
		}
		Mode mode = Mode.EDIT; //jobriant
//		if (isRemoveMode)
//		{
//			mode = Mode.REMOVE;
//		}
		IOLineEditorDialog dlg = new IOLineEditorDialog(product.getValue() + "-" + product.getName(), startQty,
				isSerial, 100, !isSerial, mode, product, movement, line, notes);
		dlg.setLocator(locator);
		dlg.setAttribute(Window.MODE_KEY, Window.MODE_MODAL);
		SessionManager.getAppDesktop().showWindow(dlg);
		if (!dlg.isCancelled())
		{
			if (dlg.getM_Locator_ID() == 0)
			{
				showError("This is unusual, you shouldn't get Locator=0");
				return;
			}
			if (dlg.getM_Locator_ID() != 0 && dlg.getM_Locator_ID() != line.getM_Locator_ID())
				line.setM_LocatorTo_ID(dlg.getM_Locator_ID());

			line.setDescription(dlg.getNotes());
			
			if (isSerial)
			{
				// serial changed
				if (!serial.equals(dlg.getSerial()))
				{
					if (isSerialOK(product.getValue(), dlg.getSerial()))
					{
						MAttributeSetInstance asi = getAttributeSetInstance(product, dlg.getSerial());
						line.setM_AttributeSetInstance_ID(asi.getM_AttributeSetInstance_ID());
						table.setValueAt(dlg.getSerial(), r, 4);
					}
				}
				if (line.save())
				{
					table.setValueAt(dlg.getLocator(), r, 3); // locator
					table.setValueAt(dlg.getNotes(), r, 7);
					table.repaint();
					updateItemCount();
				}
				return;
			}

			//BigDecimal totalMoveQty = (line.getMovementQty()).add(dlg.getQty());
			//if (isRemoveMode)
			//	totalMoveQty = line.getMovementQty().subtract(dlg.getQty());
			BigDecimal totalMoveQty = dlg.getQty(); //jobriant - on edit - no calculation required
			if (totalMoveQty.compareTo(BigDecimal.ZERO) < 0)
			{
				showError("Process will result in negative Movement qty.");
			}
			else if ((totalMoveQty).compareTo(line.getTargetQty()) <= 0)
			{
				line.setMovementQty(totalMoveQty);
				table.setValueAt(totalMoveQty, r, 4);
				mapProductScanned.put(line.getM_Product_ID(), totalMoveQty);
			}
			else
			{
				showError("Process will result in More Movement qty then Target qty.");
			}
			if (line.save())
			{
				table.setValueAt(dlg.getLocator(), r, 3); // locator
				table.setValueAt(dlg.getNotes(), r, 7);
				table.repaint();
				updateItemCount();
			}
			reloadTable();
		}

	}

	private void onSerial()
	{

		String strSerial = (String) fSerial.getValue();
		processScan(product, strSerial);
	}

	private void onExit()
	{
		Trx trx = Trx.get(m_trxName, false);
		if (trx != null)
		{
			trx.commit();
			trx.close();
		}
		this.detach();
	}

	private void onComplete()
	{
		try
		{
			DB.commit(true, m_trxName);
			boolean isOk = true;
			boolean Complete = true;
			for (MMovementLine line : moveLines)
			{
				if (line.getTargetQty().compareTo(line.getMovementQty()) != 0)
				{
					isOk = false;
					Complete = false;
					break;
				}
			}

			if (!isOk
					&& FDialog.ask(m_WindowNo, null,
							"Some lines has less movement qty then target qty.<br/>Do you want to continue?"))
			{
				Complete = true;
			}
			if (Complete)
			{
				movement.processIt(MMovement.ACTION_Complete);
				if (movement.save(m_trxName))
				{
					FDialog.info(m_WindowNo, null, "Movement is Completed Successfully.");
					DB.commit(true, m_trxName);
					this.detach();
				}
			}
			else
			{
				FDialog.info(m_WindowNo, null, "Movement is not Completed.");
			}
		}
		catch (Exception e)
		{
			showLogError(e.getMessage());
			try
			{
				DB.rollback(false, m_trxName);
			}
			catch (Exception ex)
			{
				showError("Commit Error " + e.getMessage());
			}
		}
	}

//	private boolean printtoScreen(String title, int printFormatID)
//	{
//		MPrintFormat pf = new MPrintFormat(ctx, printFormatID, null);
//
//		ReportEngine re = ReportEngine.get(ctx, pInfo);
//		re.setPrintFormat(pf);
//
//		ReportCtl.createOutput(re, false, printerName);
//		try
//		{
//			String path = System.getProperty("java.io.tmpdir");
//			File file = File.createTempFile("MOVEMENT", ".pdf", new File(path));
//
//			// re.createPDF(file);
//			file = re.getPDF();
//
//			Clients.showBusy(null, false);
//			Window win = new SimplePDFViewer(title, new FileInputStream(file));
//			SessionManager.getAppDesktop().showWindow(win, "center");
//
//		}
//		catch (Exception e)
//		{
//			log.log(Level.SEVERE, e.getLocalizedMessage(), e);
//		}
// 
//		return true;
//	}

	private void print(MPrintConfig config) {
		
		if (config == null || config.getAD_PrintFormat_ID() == 0) {
			return;
		}
		MPrintFormat pf = new MPrintFormat(ctx, config.getAD_PrintFormat_ID(), null);

		ReportEngine re = ReportEngine.get(ctx, pInfo);
		re.setPrintFormat(pf);

		if (config.isDirectPrint()) {
			ReportCtl.createOutput(re, false, pf.getPrinterName());
		}
		else {
			Clients.showBusy("Preparing PDF", false);
			try {
				//String path = System.getProperty("java.io.tmpdir");
				//File file = File.createTempFile("MOVEMENT", ".pdf", new File(path));
				//file = re.getPDF();
				Window win = new SimplePDFViewer(config.getName(), new FileInputStream(re.getPDF()));
				SessionManager.getAppDesktop().showWindow(win, "center");
			} catch (Exception e) {
				log.severe(e.getMessage());
			}
		}
	}
//	private void onPrintLabel() throws Exception
//	{
//		if (printtoScreen("Button 1", address_PrintFormat_ID))
//		{
//			showError("Job Sent");
//		}
//	}
//
//	private void onPrintPackingList() throws Exception
//	{
//		if (printtoScreen("Button 2", packing_PrintFormat_ID))
//		{
//			showError("Job Sent");
//		}
//
//	}
//
//	private void onPrintMissingList() throws Exception
//	{
//		if (printtoScreen("Button 3", missing_PrintFormat_ID))
//		{
//			showError("Job Sent");
//		}
//
//	}

	private void onDelete()
	{
		boolean resetLine = false;
		if (moveLines.size() > 0)
		{
			if (FDialog.ask(m_WindowNo, null,
					"Selecting Ok will set movement qty to zero on all movement lines.<br/> Do you want continue?"))
			{
				resetLine = true;
			}
		}
		if (resetLine)
		{
			boolean isReset = true;
			for (MMovementLine line : moveLines)
			{
				line.setMovementQty(BigDecimal.ZERO);
				if (!line.save())
				{
					isReset = false;
				}
			}
			if (!isReset)
				FDialog.info(m_WindowNo, null, "Movement qty could not be set to zero one some lines.");
		}

		try
		{
			DB.commit(true, m_trxName);
		}
		catch (Exception e)
		{
			showError("Commit Error " + e.getMessage());
		}

		reloadTable();
	}

	private void updateItemCount()
	{
		String sql = "Select sum(movementqty) from M_MovementLine where M_Movement_id=?";
		int cnt = DB.getSQLValue(m_trxName, sql, movement.getM_Movement_ID());
		lScanned.setValue("Scanned=" + cnt);
	}

	private void showError(String msg)
	{
		lMessage.setValue(msg);
	}

	/**
	 * Serial OK to add - no duplicate
	 * 
	 * @param pCode
	 * @param serNo
	 * @return
	 */
	private boolean isSerialOK(String pCode, String serNo)
	{
		String key = pCode + "-" + serNo;
		boolean isExists = mapProductSerial.containsKey(key);
		if (isRemoveMode)
		{
			// we are trying to remove this item, OK if you can find it
			if (!isExists)
			{
				showError("You are trying to remove SerialNo=" + serNo + " that don't exists.");
			}
			return isExists;
		}
		else
		{
			// this is the opposite, we only allow Item to add if serial don't
			// exists
			if (isExists)
			{
				showError("You are trying to add SerialNo=" + serNo + " that already exists in the shipment.");
			}
			return !isExists;
		}
	}

	private MAttributeSetInstance getAttributeSetInstance(MProduct product, String serial)
	{
		if (!(serial == null))
		{
			MAttributeSetInstance asi = null;
			// check for existing serial
			String sql = "SELECT asi.m_attributesetinstance_id FROM m_attributesetinstance asi "
					+ "INNER JOIN m_product p ON p.m_attributeset_id = asi.m_attributeset_id "
					+ "WHERE asi.m_attributeset_id = ? AND  asi.serno = ? AND p.m_product_id = ?";
			int instance_id = DB.getSQLValue(null, sql, product.getM_AttributeSet_ID(), serial,
					product.getM_Product_ID());
			// use existing instance
			if (instance_id > 0)
			{
				asi = MAttributeSetInstance.get(ctx, instance_id, product.getM_Product_ID());
			}
			if (asi != null)
			{
				return asi;
			}
		}

		return null;
	}

	/**
	 * @param product_ID
	 * @param qty
	 * @param add - flag to add or deduct qty
	 * @return
	 */
	private boolean updateScannedQtyProductMap(int product_ID, BigDecimal qty, boolean add)
	{
		BigDecimal qtyAllowed = mapProductToScan.get(product_ID);
		BigDecimal newQty = Env.ZERO;

		if (mapProductScanned.containsKey(product_ID))
		{
			newQty = mapProductScanned.get(product_ID);
			if (add)
			{
				newQty = newQty.add(qty);
			}
			else
			{
				newQty = newQty.subtract(qty);
			}
		}

		if (newQty.signum() < 0)
		{
			return false; // you're taking more than possible
		}
		if (newQty.compareTo(qtyAllowed) > 0)
		{
			return false; // will result to over receipt
		}
		mapProductScanned.put(product_ID, newQty);
		return true; // mapping updated
	}

	private boolean isQtyOK(int product_ID, BigDecimal qty, boolean add)
	{
		BigDecimal qtyAllowed = mapProductToScan.get(product_ID);
		BigDecimal newQty = Env.ZERO;

		if (qtyAllowed == null)
		{
			qtyAllowed = Env.ZERO;
		}
		if (mapProductScanned.containsKey(product_ID))
		{
			newQty = mapProductScanned.get(product_ID);
			if (add)
			{
				newQty = newQty.add(qty);
			}
			else
			{
				newQty = newQty.subtract(qty);
			}
		}

		if (newQty.signum() < 0)
		{
			showError("Process will result in negative Movement qty.");
			return false;
		}
		if (newQty.compareTo(qtyAllowed) > 0)
		{
			showError("Process will result in More Movement qty then Target qty.");
			return false;
		}
		return true; // mapping updated
	}

	private void resetHashMap()
	{
		mapProductToScan.clear();
		mapProductScanned.clear();
		mapProductSerial.clear();
		String sql = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{

			sql = "SELECT m_movementline_id, movementqty " + "FROM m_movementline " + "WHERE m_movement_id = ?";
			pstmt = DB.prepareStatement(sql, m_trxName);
			pstmt.setInt(1, movement.getM_Movement_ID());
			rs = pstmt.executeQuery();
			while (rs.next())
			{
			}

			sql = "SELECT m_product_id, SUM(targetqty) " + "FROM m_movementline " + "WHERE m_movement_id = ?"
					+ "GROUP BY m_product_id";
			pstmt = DB.prepareStatement(sql, m_trxName);
			pstmt.setInt(1, movement.getM_Movement_ID());
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				mapProductToScan.put(rs.getInt(1), rs.getBigDecimal(2));
			}

			sql = "SELECT  m_product_id,sum(movementqty) " + "FROM m_movementline " + "WHERE m_movement_id = ? "
					+ "GROUP BY m_product_id";

			pstmt = DB.prepareStatement(sql, m_trxName);
			pstmt.setInt(1, movement.getM_Movement_ID());
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				mapProductScanned.put(rs.getInt(1), rs.getBigDecimal(2));
			}

			sql = "SELECT p.value || '-' || asi.serno, ml.m_movementline_id "
					+ "FROM m_movementline ml "
					+ "  INNER JOIN m_attributesetinstance asi ON asi.m_attributesetinstance_id = ml.m_attributesetinstance_id "
					+ "  INNER JOIN m_product p ON ml.m_product_id = p.m_product_id "
					+ "WHERE asi.m_attributesetinstance_id <> 0 " + "AND   ml.m_movement_id = ?";

			pstmt = DB.prepareStatement(sql, m_trxName);
			pstmt.setInt(1, movement.getM_Movement_ID());
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				mapProductSerial.put(rs.getString(1), rs.getInt(2));
			}

		}
		catch (Exception e)
		{
			log.severe(e.getMessage());
		}
		finally
		{
			DB.close(rs, pstmt);
		}
	}

	private MProduct findProduct(String barcode, boolean isUPC)
	{
		String key = barcode;
		List<MProduct> productsFound = null;

		if (isUPC)
		{
			productsFound = MProduct.getByUPC(ctx, key, m_trxName);
		}
		else
		{
			productsFound = new Query(ctx, MProduct.Table_Name, "value LIKE ?", m_trxName).setParameters(key).list();
		}

		if (productsFound.size() == 0)
		{
			showError("Invalid Product code : " + key);
			return null;
		}

		if (productsFound.size() > 1)
		{
			showError("Multiple products found with Bar Code: " + key);
			return null;
		}

		MProduct product = productsFound.get(0);

		return product;

	}

	/**
	 * At this stage, product is valid but needs to check qty
	 * 
	 * @param product
	 */
	private void processScan(MProduct product, String serialNo)
	{

		boolean isSerial = false;
		BigDecimal initialValue = Env.ONE;
		boolean isSerialOK = false;
		BigDecimal qtyScan = null;
		String sql = "SELECT l.Value from M_Locator l INNER JOIN M_MovementLine ml on ml.M_LocatorTo_ID=l.M_Locator_ID where ml.M_Product_ID=? AND ml.M_Movement_ID=?";

		// List
		String defaultLocator = DB.getSQLValueString(m_trxName, sql, product.getM_Product_ID(),
				movement.getM_Movement_ID());

		if (serialNo != null)
		{
			isSerial = true;
			qtyScan = getQtyReceived(isSerial, initialValue, true, product.getValue() + "-" + product.getName(),
					defaultLocator, product);
			if (!isQtyOK(product.getM_Product_ID(), qtyScan, !isRemoveMode))
			{
				return;
			}

			isSerialOK = isSerialOK(product.getValue(), serialNo);
			if (!isSerialOK)
			{
				return;
			}
		}
		else
		{
			initialValue = mapProductToScan.get(product.getM_Product_ID());
			BigDecimal productScanned = mapProductScanned.get(product.getM_Product_ID());

			if (initialValue == null)
			{
				initialValue = Env.ZERO;
			}
			if (productScanned == null)
			{
				productScanned = Env.ZERO;
			}

			initialValue = initialValue.subtract(productScanned);
			if (isRemoveMode)
				initialValue = productScanned;
			qtyScan = getQtyReceived(isSerial, initialValue, false, product.getValue() + "-" + product.getName(),
					defaultLocator, product);

			if (!isQtyOK(product.getM_Product_ID(), qtyScan, !isRemoveMode))
			{
				return;
			}
		}

		// this shouldn't happen - dialog shouldn't allow an invalid locator
		if (m_M_Locator_ID == 0)
		{
			return;
		}

		// everything works find, we can update the user of the new status and
		// proceed to next
		if (updateMovementLines(product, serialNo, qtyScan, m_M_Locator_ID, !isRemoveMode))
		{
			updateScannedQtyProductMap(product.getM_Product_ID(), qtyScan, !isRemoveMode);
			reloadTable();

		}
	}

	private BigDecimal getQtyReceived(boolean isSerial, BigDecimal initialValue, boolean readOnly, String title,
			String defaultLocator, MProduct product)
	{
		Mode mode = Mode.ADD;
		if (isRemoveMode)
		{
			mode = Mode.REMOVE;
		}
		m_M_Locator_ID = 0; // initialise

		IOLineEditorDialog dialog = new IOLineEditorDialog(title, initialValue, isSerial, 100, !readOnly, mode,
				product, movement, null, null);

		if (!isRemoveMode)
			dialog.setLocator(defaultLocator);
		dialog.setAttribute(Window.MODE_KEY, Window.MODE_MODAL);
		SessionManager.getAppDesktop().showWindow(dialog);

		if (!dialog.isCancelled())
		{

			m_M_Locator_ID = dialog.getM_Locator_ID();
			return dialog.getQty();
		}

		return BigDecimal.ZERO;
	}

	private boolean updateMovementLines(MProduct product, String serNo, BigDecimal qtyScanned, int M_Locator_ID,
			boolean add)
	{
		List<MMovementLine> moveLineCandidates = new ArrayList<MMovementLine>();
		for (MMovementLine line : moveLines)
		{
			if (line.getM_Product_ID() == product.getM_Product_ID())
			{
				moveLineCandidates.add(line);
			}
		}

		if (moveLineCandidates.size() == 0)
		{
			log.severe("loadItemToMovement - Various checks should prevent this to happen.");
			return false;
		}

		// deduct 1 item for the list - alway 1 item at time when removing
		if (!add)
		{
			if (serNo != null)
			{
				// it's serial, just look in the map
				return processRemoveSerial(product, serNo);
			}
			return processRemoveProduct(product, qtyScanned, M_Locator_ID, moveLineCandidates);
		}

		if (serNo != null)
		{
			return processAddSerial(product, serNo, M_Locator_ID, moveLineCandidates);
		}

		return processAddProduct(product, qtyScanned, M_Locator_ID, moveLineCandidates);
	}

	/**
	 * This is the heart of shipment process - make sure the scanned items are
	 * allocated properly
	 * 
	 * @param qtyScanned
	 * @param packLineCandidates
	 * @return
	 */
	private boolean processAddProduct(MProduct product, BigDecimal qtyScanned, int M_Locator_ID,
			List<MMovementLine> moveLineCandidates)
	{
		for (MMovementLine line : moveLineCandidates)
		{
			BigDecimal qtyMove = line.getMovementQty();
			if (line.getM_Product_ID() == product.getM_Product_ID() && qtyMove.compareTo(line.getTargetQty()) != 0)
			{
				BigDecimal qtyRemaining = ditributeQty(line, product, qtyScanned, M_Locator_ID);
				if (qtyRemaining.signum() != 0)
				{
					qtyScanned = qtyRemaining;
				}
				else
				{
					return true;
				}
			}
		}

		return true;
	}

	/**
	 * In case multiple lines with same product
	 * 
	 * @param line
	 * @param product
	 * @param qtyScanned
	 * @param M_Locator_ID
	 * @return
	 */
	private BigDecimal ditributeQty(MMovementLine line, MProduct product, BigDecimal qtyScanned, int M_Locator_ID)
	{
		BigDecimal qtyMove = line.getMovementQty();
		BigDecimal qtyToMove = BigDecimal.ZERO;
		BigDecimal qtyRemaining = BigDecimal.ZERO;
		BigDecimal qtyScannedTotal = qtyMove.add(qtyScanned);
		if (qtyScannedTotal.compareTo(line.getTargetQty()) <= 0)
			qtyToMove = qtyScannedTotal;
		else
		{
			qtyToMove = line.getTargetQty();
			qtyRemaining = qtyScannedTotal.subtract(line.getTargetQty());
		}
		line.setMovementQty(qtyToMove);
		if (line.getM_Locator_ID() != line.getM_LocatorTo_ID())
			line.setM_LocatorTo_ID(M_Locator_ID);
		if (!line.save())
		{

		}
		return qtyRemaining;
	}

	/**
	 * @param qtyScanned
	 * @param M_Locator_ID
	 * @param moveLineCandidates
	 */
	private boolean processRemoveProduct(MProduct product, BigDecimal qtyScanned, int M_Locator_ID,
			List<MMovementLine> moveLineCandidates)
	{
		for (MMovementLine line : moveLineCandidates)
		{
			BigDecimal qtyMove = line.getMovementQty();
			BigDecimal qtyRemaining = BigDecimal.ZERO;
			if (line.getM_Product_ID() == product.getM_Product_ID() && line.getM_LocatorTo_ID() == M_Locator_ID
					&& qtyMove.compareTo(BigDecimal.ZERO) > 0)
			{
				BigDecimal qty = BigDecimal.ZERO;
				if (qtyScanned.compareTo(qtyMove) <= 0)
					qty = qtyMove.subtract(qtyScanned);
				else
				{
					qtyRemaining = qtyScanned.subtract(qtyMove);
				}
				if (qty.signum() >= 0)
				{
					line.setMovementQty(qty);
					line.save();
				}
				if (qtyRemaining.signum() != 0)
				{
					qtyScanned = qtyRemaining;
				}
				else
				{
					return true;
				}

			}
		}

		return true;
	}

	/**
	 * @param product
	 * @param serNo
	 * @param M_Locator_ID
	 * @param ioLineCandidates
	 */
	private boolean processAddSerial(MProduct product, String serNo, int M_Locator_ID,
			List<MMovementLine> ioLineCandidates)
	{
		// it's Serial, we only need to add 1 item, so just chuck it in
		// to the first receipt line available
		for (MMovementLine line : ioLineCandidates)
		{
			BigDecimal scanned = line.getMovementQty();
			if (scanned.signum() == 0)
			{

				MAttributeSetInstance asi = getAttributeSetInstance(product, serNo);

				line.setMovementQty(Env.ONE);
				line.setM_Locator_ID(M_Locator_ID);
				line.setM_AttributeSetInstance_ID(asi.getM_AttributeSetInstance_ID());
				line.save(m_trxName);

				addSerialToProductMap(product.getValue(), serNo, line.getM_MovementLine_ID());

				return true;
			}
		}
		return false;
	}

	/**
	 * @param product
	 * @param serNo
	 * @param ioLineCandidates
	 */
	private boolean processRemoveSerial(MProduct product, String serNo)
	{
		int m_packageline_id = getLineFromProductSerial(product.getValue(), serNo);
		MPackageLine line = new MPackageLine(ctx, m_packageline_id, m_trxName);
		if (line != null)
		{
			line.delete(true);
			line.save(m_trxName);

			deleteSerialProductMap(product.getValue(), serNo);
		}
		return true; // all done, going home
	}

	/**
	 * Add Serial No to mapping table
	 * 
	 * @param pCode
	 * @param serNo
	 * @return true = added to map false = already SerNo already exists
	 */
	private boolean addSerialToProductMap(String pCode, String serNo, int M_MovementLine_ID)
	{
		String key = pCode + "-" + serNo;
		mapProductSerial.put(key, M_MovementLine_ID);
		return true;
	}

	/**
	 * @param pCode
	 * @param serNo
	 * @return M_PackageLine_ID
	 */
	private int getLineFromProductSerial(String pCode, String serNo)
	{
		String key = pCode + "-" + serNo;
		return mapProductSerial.get(key);
	}

	/**
	 * Remove the Serial from the mapping
	 * 
	 * @param pCode
	 * @param serNo
	 * @return
	 */
	private boolean deleteSerialProductMap(String pCode, String serNo)
	{
		String key = pCode + "-" + serNo;
		mapProductSerial.remove(key);
		return true;
	}

	private void onProductScan(Textbox text)
	{
		boolean isUPC = false;
		if (text == fUPC.getComponent())
		{
			isUPC = true;
		}

		MProduct product = findProduct(text.getValue(), isUPC);

		if (product == null)
		{
			lProductInfo.setText("");
			text.setValue("");
			return;
		}

		lProductInfo.setText(product.getValue() + "-" + product.getName());
		boolean isSerial = isProductSerialised(product);

		// statusBar.setStatusLine("", false, false);
		lMessage.setValue("");
		// still need to check the serial
		if (isSerial)
		{
			fSerial.setValue("");
			fSerial.setHasFocus(true);
			fSerial.getComponent().focus();
		}

		processScan(product, null);

		updateItemCount();
	}

	private boolean isProductSerialised(MProduct product)
	{
		if (product.getAttributeSet() != null)
		{
			return product.getAttributeSet().isSerNo();
		}
		return false;
	}

	private void showLogError(String msgDefault)
	{
		String msg = null;
		ValueNamePair err = CLogger.retrieveError();
		if (err != null)
			msg = err.getName();
		if (msg == null || msg.length() == 0)
			msg = msgDefault;

		showError(msg);
	}
}
