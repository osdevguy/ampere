package org.adaxa.window;


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.adaxa.form.WShipmentScannerForm;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WStringEditor;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MInOut;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;

/**
 * 
 * @author jobriant
 *
 */
public class ResponsiblePersonDialog extends Window implements EventListener{
	
	private static final String FIELD_WIDTH = "250px";
	private static final String FIELD_LABEL_WIDTH = "100px,200px";

	private static CLogger			log = CLogger.getCLogger (ResponsiblePersonDialog.class);
	
	private static final String HEADER_TEXT_STYLE = "color: darkblue; font-size: 16px;";

	public enum ShipmentAction {SHIP, COMPLETE}
	/**
	 * 
	 */
	private static final long serialVersionUID = -3852236029054284848L;
	private int max;
	private BigDecimal number;
	private boolean cancelled = false;
	private Label lHeader;
	private Label status;
	
	private Grid inputPanel = GridFactory.newGridLayout();
	
	private WStringEditor fDocumentList = new WStringEditor();
	private WStringEditor fDocumentNo = new WStringEditor();
	private Listbox fWarehouseStaff = ListboxFactory.newDropdownListbox();
	private WStringEditor fErrorMsg = new WStringEditor();
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private ConfirmPanel confirmPanel = null;
	private Timestamp m_CurrentTime = null;
	private int warehouseStaff = 0;
	
	private Properties m_ctx;
	private StringBuffer m_list = null;
	private ShipmentAction m_action = ShipmentAction.COMPLETE;
	private int m_windowNo = 0;
	
	private WListbox m_table = null;
	private ArrayList<Integer> m_scanned = new ArrayList<Integer>();
	private ArrayList<Integer> m_skip = new ArrayList<Integer>();
	private int m_targetStatus = 0; 
	
	/**
	 * 
	 * @param title
	 * @param text
	 * @param isSerial
	 * @param maxValue
	 */
	public ResponsiblePersonDialog(ShipmentAction action, int windowNo, WListbox table) {
		super();
		m_ctx = Env.getCtx();
		m_list = new StringBuffer();
		m_action = action;
		m_table = table;
		m_windowNo = windowNo;
		
		if (m_action == ShipmentAction.COMPLETE) {
			setTitle("Complete");
			m_targetStatus = Integer.valueOf(WShipmentScannerForm.STATUS_AWAITING_SHIPPING);
		} else if (m_action == ShipmentAction.SHIP) {
			setTitle("Ship");
			m_targetStatus = Integer.valueOf(WShipmentScannerForm.STATUS_SHIPPED);
		}
		
		
		init();

		
	}
	


	private void init() {
		setBorder("normal");
		
	
		lHeader = new Label();
		Date date = new Date();
		m_CurrentTime = new Timestamp(date.getTime());
		lHeader.setText(sdf.format(m_CurrentTime));
		lHeader.setStyle(HEADER_TEXT_STYLE);
		
		Rows rows = inputPanel.newRows();
		Row row = rows.newRow();
		row.appendChild(new Label("Date and Time").rightAlign());
		row.appendChild(lHeader);
		
		row = rows.newRow();
		row.appendChild(new Label("Document No").rightAlign());
		fDocumentNo.getComponent().setWidth(FIELD_WIDTH);
		row.appendChild(fDocumentNo.getComponent());

		row = rows.newRow();
		row.appendChild(new Label("Documents Scanned").rightAlign());
		fDocumentList.getComponent().setWidth(FIELD_WIDTH);
		fDocumentList.getComponent().setMultiline(true);
		fDocumentList.getComponent().setHeight("200px");
		fDocumentList.setReadWrite(false);
		row.appendChild(fDocumentList.getComponent());
		initDocumentList();
		
		
		row = rows.newRow();
		row.appendChild(new Label("Warehouse Staff").rightAlign());
		fWarehouseStaff.setWidth(FIELD_WIDTH);
		row.appendChild(fWarehouseStaff);
		initWarehouseStaff();

		row = rows.newRow();
		row.appendChild(new Label("Message").rightAlign());
		fErrorMsg.getComponent().setWidth(FIELD_WIDTH);
		fErrorMsg.getComponent().setMultiline(true);
		fErrorMsg.setReadWrite(false);
		fErrorMsg.getComponent().setHeight("100px");

		row.appendChild(fErrorMsg.getComponent()); 
		
		appendChild(inputPanel);
		fDocumentNo.getComponent().addEventListener(Events.ON_OK, this);
		
		confirmPanel = new ConfirmPanel(true);
		row = rows.newRow();
		//row.appendChild(new Space());
		row.setSpans("2");
		row.appendChild(confirmPanel);
		
		confirmPanel.addButton(confirmPanel.createButton(ConfirmPanel.A_RESET));
		confirmPanel.addActionListener(this);
		

	}

	private void initWarehouseStaff() {

		String sql = "SELECT u.ad_user_id, u.name " +
	             "FROM ad_user u " +
	             "  INNER JOIN c_bpartner bp ON u.c_bpartner_id = bp.c_bpartner_id " +
	             "WHERE bp.IsWarehouseStaff = 'Y' " +
	             "and bp.ad_client_id = ? " +
	             "ORDER BY bp.seqno, u.name";
		
		
		for (KeyNamePair kn : DB.getKeyNamePairs(sql, false, Env.getAD_Client_ID(m_ctx))) {
			fWarehouseStaff.addItem(kn);
		}		
	
	
	}

	private void initDocumentList() {
		for (int idx : m_table.getSelectedIndices()) {
			String docNo = (String) m_table.getValueAt(idx, WShipmentScannerForm.IDX_COL_SHIPMENTNO);
			IDColumn id = (IDColumn)m_table.getValueAt(idx, WShipmentScannerForm.IDX_COL_IDCOLUMN);
			m_scanned.add(id.getRecord_ID());
			m_list.append(docNo +  System.getProperty("line.separator"));
		}
		fDocumentList.setValue(m_list.toString());		
	}
	/**
	 * @param event
	 */
	public void onEvent(Event event) throws Exception {
		if (event.getTarget().getId().equals(ConfirmPanel.A_CANCEL)) {
			cancelled = true;
			detach();
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_RESET)) {
			fDocumentList.setValue("");
			m_list = new StringBuffer();
			m_scanned.clear();
			fErrorMsg.setValue("Scanned items cleared.");
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_OK)) {
			boolean resultOK = false;
			
			if (m_action == ShipmentAction.SHIP) {
				resultOK = ship();
				
			} else if (m_action == ShipmentAction.COMPLETE) {
				resultOK = complete();
			} else {
			}
			
			if  (resultOK) {
				detach();
			}
			
		}
		else if (Events.ON_FOCUS.equals(event.getName())){
		} else if (Events.ON_OK.equals(event.getName())) {
			if (event.getTarget() == fDocumentNo.getComponent()) {
				onDocumentScan();
			}
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_RESET)) {
		} else if (event.getName().equals(Events.ON_CHANGE)) {
		}
		
	}

	private boolean isScanValid(String docNo)
	{
		int idx = 0;
		IDColumn id = null;
		String shipNo = null;
		boolean found = false;
		for (int i = 0; i < m_table.getRowCount(); i++)
		{
			id = (IDColumn)m_table.getValueAt(i, WShipmentScannerForm.IDX_COL_IDCOLUMN);     
			shipNo  = (String) m_table.getValueAt(i, WShipmentScannerForm.IDX_COL_SHIPMENTNO);
			
			if (shipNo.equals(docNo)) {
				idx = i;
				found = true;
				break;
			}
		}
		if (!found) {
			fErrorMsg.setValue("Invalid Document=" +docNo);
			return false;
		}
		String status = (String) m_table.getValueAt(idx, WShipmentScannerForm.IDX_COL_STATUS);
		
		if (Integer.valueOf(status) > m_targetStatus) 
		{
			fErrorMsg.setValue("Shipment already advance in stage Document=" +docNo);
			return false;
		}
		
		if (m_action == ShipmentAction.COMPLETE) {
			if (status.equals(WShipmentScannerForm.STATUS_AWAITING_SHIPPING)) {
				fErrorMsg.setValue("Already Complete. Document=" + docNo);
				return false;
			}

		} else if (m_action == ShipmentAction.SHIP) {
			if (!status.equals(WShipmentScannerForm.STATUS_AWAITING_SHIPPING)) {
				fErrorMsg.setValue("Not Ready for shipping Document=" + docNo);
				return false;
			}
		}
		
		m_scanned.add(id.getRecord_ID());
		fErrorMsg.setValue("Success Document=" + docNo);
		return true;
	}
	
	/**
	 * 
	 */
	private void onDocumentScan() 
	{
		String docNo = fDocumentNo.getValue().toString().trim();
		
		if (isScanValid(docNo)) {
			m_list.insert(0, docNo + System.getProperty("line.separator"));
			fDocumentList.setValue(m_list.toString());
		}
		fDocumentNo.setValue("");
	}


	
	/**
	 * 
	 * @return boolean
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * no checking just mark as retrieved
	 * @return
	 */
	private boolean retrieve()
	{
		//String[] list = m_list.toString().split(System.getProperty("line.separator"));
		//where shipmentstatus = '0' is added to prevent overriding an updated one
		String sql = "UPDATE m_inout SET retrievedby = ?, retrievedate = COALESCE(retrievedate, ?), shipmentstatus = ? WHERE COALESCE(shipmentstatus, '0') IN ('0', '2') AND m_inout_id = ?";
		String trxName = Trx.createTrxName();
		PreparedStatement pstmt = DB.prepareStatement(sql, trxName);
		
		KeyNamePair warehouseNPair = fWarehouseStaff.getSelectedItem().toKeyNamePair();
		int ad_user_id = warehouseNPair.getKey();
		try {
			for (int inoutID : m_scanned) {
				pstmt.setInt(1, ad_user_id);
				pstmt.setTimestamp(2, m_CurrentTime);
				pstmt.setString(3, WShipmentScannerForm.STATUS_AWAITING_PICKING);
				pstmt.setInt(4, inoutID);
				pstmt.execute();
			}
			DB.commit(true, trxName);
			pstmt.close();
			Trx trx = Trx.get(trxName, false);
			trx.close();
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		
		fErrorMsg.setValue("Update successful");
		return true;
	}
	
	
	/**
	 * mark as picked, do preliminary checking for unretrieved documents, and prompt user before proceeding
	 * @return
	 */
	private boolean pick()
	{
		String trxName = Trx.createTrxName();
		String sql = "UPDATE m_inout SET retrievedby = COALESCE(retrievedby,?) " +
	             "   , retrievedate = COALESCE(retrievedate,?) " +
	             "   , pickedby = ? " +
	             "   , pickdate = ? " +
	             "   , shipmentstatus = ? " +
	             "WHERE COALESCE(shipmentstatus, '0') IN ('0', '2', '4') " +
	             "and m_inout_id = ?";
		
		
		PreparedStatement pstmt = DB.prepareStatement(sql, trxName);
		
		KeyNamePair warehouseNPair = fWarehouseStaff.getSelectedItem().toKeyNamePair();
		int ad_user_id = warehouseNPair.getKey();
		try {
			for (int inoutID : m_scanned) {
				pstmt.setInt(1, ad_user_id);
				pstmt.setTimestamp(2, m_CurrentTime);
				pstmt.setInt(3, ad_user_id);
				pstmt.setTimestamp(4, m_CurrentTime);
				pstmt.setString(5, WShipmentScannerForm.STATUS_PICKING_IN_PROGRESS);
				pstmt.setInt(6, inoutID);
				pstmt.execute();
			}
			DB.commit(true, trxName);
			Trx trx = Trx.get(trxName, false);
			trx.close();
			pstmt.close();
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		
		fErrorMsg.setValue("Update successful");
		return true;
		
	}
	
	/**
	 * document must be complete, mark as shipped and inform user of other documents
	 * @return
	 */
	private boolean ship()
	{
		String sql = "UPDATE m_inout " +
	             "SET shippedby = ? " +
	             ", shipdate = ? " +
	             ", packedby = ? " +
	             ", packdate = ? " +
	             ", shipmentstatus = ? " +
	             "WHERE shipmentstatus = '6' " +
	             "and m_inout_id = ?";		
		
		String trxName = Trx.createTrxName();
		PreparedStatement pstmt = DB.prepareStatement(sql, trxName);
		
		KeyNamePair warehouseNPair = fWarehouseStaff.getSelectedItem().toKeyNamePair();
		int ad_user_id = warehouseNPair.getKey();
		try {
			for (int inoutID : m_scanned) {
				pstmt.setInt(1, ad_user_id);
				pstmt.setTimestamp(2, m_CurrentTime);
				pstmt.setInt(3, ad_user_id);
				pstmt.setTimestamp(4, m_CurrentTime);
				pstmt.setString(5, WShipmentScannerForm.STATUS_SHIPPED);
				pstmt.setInt(6, inoutID);
				pstmt.execute();
			}
			DB.commit(true, trxName);
			Trx trx = Trx.get(trxName, false);
			trx.close();
			pstmt.close();
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		
		fErrorMsg.setValue("Update successful");
		return true;
	}
	

	private boolean  complete()
	{
		KeyNamePair warehouseNPair = fWarehouseStaff.getSelectedItem().toKeyNamePair();
		int ad_user_id = warehouseNPair.getKey();
		String trxName = Trx.createTrxName();
		for (int inoutID : m_scanned) {
			MInOut inout = new MInOut(Env.getCtx(), inoutID, trxName);
			
			if (inout.processIt(MInOut.ACTION_Complete)) {
//				inout.set_CustomColumn("ShipmentStatus", WShipmentScannerForm.STATUS_AWAITING_SHIPPING);
//				inout.set_CustomColumn("PickCompleteDate", m_CurrentTime);
//
//				if (inout.get_ValueAsInt("retrievedby") == 0) {
//					//inout.set_CustomColumn("retrievedby", ad_user_id);
//					inout.set_CustomColumn("retrievedate", m_CurrentTime);
//				}
//
//				if (inout.get_ValueAsInt("pickedby") == 0) {
//					//inout.set_CustomColumn("pickedby", ad_user_id);
//					inout.set_CustomColumn("pickdate", m_CurrentTime);
//				}

				inout.save();
			}
		}

		try {
			DB.commit(false, trxName);
			Trx trx = Trx.get(trxName, false);
			trx.close();
		} catch (Exception e) {
			log.severe(e.getMessage());
			return false;
		}
		fErrorMsg.setValue("Complete successful");
		return true;
	}
}
