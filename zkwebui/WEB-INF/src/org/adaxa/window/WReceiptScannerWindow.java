package org.adaxa.window;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adaxa.window.IOLineEditorDialog.Mode;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WStringEditor;
import org.adempiere.webui.editor.WYesNoEditor;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ITheme;
import org.adempiere.webui.window.FDialog;
import org.adempiere.webui.window.SimplePDFViewer;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MBPartner;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MLocator;
import org.compiere.model.MOrder;
import org.compiere.model.MProduct;
import org.compiere.model.MSysConfig;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.util.ASyncProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.ValueNamePair;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;
import org.zkoss.zul.Div;
import org.zkoss.zul.Space;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;



/**
 *	@author jobriant 
 */
public class WReceiptScannerWindow extends Window implements EventListener {

	//from Left to Right
	private static final String				RECEIPT_BUTTON1			= "RECEIPT_BUTTON1";
	private static final String				RECEIPT_BUTTON2			= "RECEIPT_BUTTON2";
	private static final String				RECEIPT_BUTTON3			= "RECEIPT_BUTTON3";
	
	private static final long serialVersionUID = -849734609756031494L;
	private static CLogger log = CLogger.getCLogger(WReceiptScannerWindow.class);

	private static final String BTN_WIDTH = "60px";
	private static final String BTN_HEIGHT = "50px";



	

	/** Window No */
	private int m_WindowNo = 0;

	//private HashMap<Integer, Integer> ioProdCount = new HashMap<Integer, Integer>();
	
	//m_product_id, Total QtyScanned
	private HashMap<Integer, BigDecimal> mapProductScanned = new HashMap<Integer, BigDecimal>();
	//m_product_id, Total QtyToScan 
	private HashMap<Integer, BigDecimal> mapProductToScan = new HashMap<Integer, BigDecimal>();
	//MProduct.Value-SerNo, null  PCode and SerNo is the key, value is M_InOutLine_ID for quick lookup
	private HashMap<String, Integer> mapProductSerial = new HashMap<String, Integer>();

	private MInOut m_InOut = null;
	private Properties ctx = null;
	private MProduct product = null;
	private boolean isRemoveMode = false;

	List<MInOutLine> ioLines =  new ArrayList<MInOutLine>();
	
	private String m_trxName = null;
	private boolean isAllowOverReceipt = true;

	// GUI
	private WListbox table = ListboxFactory.newDataTable();
	private Borderlayout mainPanel = new Borderlayout();
	private Grid northPanel = GridFactory.newGridLayout();
	private Grid southPanel = GridFactory.newGridLayout();
	private Grid edgePanel = GridFactory.newGridLayout();

	private Label lShipmentInfo = new Label();
	private Label lProductInfo = new Label();
	private Label lUPC = new Label();
	private Label lSerial = new Label();
	private Label lmode = new Label();
	private Label lProductKey = new Label();
	private Label lLocator = new Label();

	private Label lMessage = new Label();
	private Label lScanned = new Label();
	private Label lShort = new Label();
	private Label lExcess = new Label();
	private Label fScanned = new Label();
	private Label fShort = new Label();
	private Label fExcess = new Label();
	private Div panelScanned = new Div();
	private Div panelShort = new Div();
	private Div panelExcess = new Div();
	private Div panelMessage = new Div();
	
	private WStringEditor fLocator = new WStringEditor();
	private WStringEditor fUPC = new WStringEditor();
	private WStringEditor fSerial = new WStringEditor();
	private WStringEditor fProductKey = new WStringEditor();

	private Div modePanel = new Div();
	private WYesNoEditor cRemoveMode = null;
	private Button bAddress = null;
	private Button bPackingList = null;
	private Button bDelete = null;
	private Button bSave = null;
	private Button bComplete = null;
	
//	private StatusBarPanel statusBar = new StatusBarPanel();
	private int m_M_Locator_ID = 0;
	private MInOutLine m_ExcessLine = null;  //active excess line
	private int excessDeliveryLocator_ID = 0;

	private boolean isCrossDock = false;  //do not use
	
	private boolean m_IsPreLoad ;
	
//	private MPInstance pInstance;

	//private ProcessInfo pInfo;
//	private int AD_Process_ID = 0;

	public WReceiptScannerWindow(int windowNo, MInOut inout) {
		m_WindowNo = windowNo;
		this.m_InOut = inout;
		
		m_IsPreLoad  = MSysConfig.getBooleanValue("PRELOAD_SHIPMENT", false, 
				inout.getAD_Client_ID(), inout.getAD_Org_ID());
		
		setTempLineHolder();
		//ioLines = Arrays.asList(inout.getLines(true));
		for (MInOutLine iol : inout.getLines(true)) {
			ioLines.add(iol);
		}
		ctx = inout.getCtx();
		// Creating transaction for importing order
		m_trxName = Trx.createTrxName("ReceiptScanner");

		try {
			
			MWarehouse wh = (MWarehouse)m_InOut.getM_Warehouse();
			int excess_locator_id = wh.get_ValueAsInt("ExcessDelivery_Locator_ID");
			if (excess_locator_id == 0) {
				MLocator locator = new MLocator(wh, "ExcessLocator");
				locator.save();
				wh.set_ValueOfColumn("ExcessDelivery_Locator_ID", locator.getM_Locator_ID());
				wh.save();
				excess_locator_id = wh.get_ValueAsInt("ExcessDelivery_Locator_ID");
			}
			excessDeliveryLocator_ID = excess_locator_id;

			init();
			dynInit();
		} catch (Exception ex) {
			log.log(Level.SEVERE, ex.getMessage());
			this.detach();
		}
	}

	/**
	 * Static Init
	 * 
	 * @throws Exception
	 */

	void init() throws Exception {
		this.setWidth("1200px");
		this.setBorder("normal");
		this.setHeight("650px");
		this.setClosable(true);
		this.setTitle("Warehouse Receipt Scanner");
		this.setAttribute("mode", "modal");
		this.setSizable(true);
		this.appendChild(mainPanel);

		// North
		North north = new North();
		north.appendChild(northPanel);
		mainPanel.appendChild(north);
		Rows rows = northPanel.newRows();
		// First Row
		Row row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(lShipmentInfo);
		row.setSpans("1,4");

//		// Second Row
//		row = rows.newRow();
//		row.appendChild(new Space());
//		row.appendChild(lPackageInfo);
//		row.appendChild(new Space());
//		row.setSpans("1,4");

		// Third Row
//		row = rows.newRow();
//		row.appendChild(new Space());
		row = rows.newRow();
		row.appendChild(new Space());
//		row.appendChild(lLocator.rightAlign());
//		row.appendChild(fLocator.getComponent());
		row.appendChild(lUPC.rightAlign());
		row.appendChild(fUPC.getComponent());
		row.appendChild(lProductKey.rightAlign());
		row.appendChild(fProductKey.getComponent());
		// row.appendChild(new Space());
		row.appendChild(lSerial.rightAlign());
		row.appendChild(fSerial.getComponent());
		row.appendChild(new Space());
//		row.setSpans("1,1,1,1,1,2");

		// Second Row
		row = rows.newRow();
		row.setHeight("30px");
		row.appendChild(new Space());
		row.appendChild(lProductInfo);
		row.appendChild(new Space());
		row.setSpans("1,4");

		// Space Row
//		row = rows.newRow();
//		row.appendChild(new Space());
		
		// Fourth Row
		cRemoveMode = new WYesNoEditor("RemoveMode", "Remove Mode",
				"Scanned Items will be deducted from the list.", false, false, true);
		row = rows.newRow();
		row.appendChild(new Space());
		row.appendChild(modePanel);
		row.appendChild(cRemoveMode.getComponent());
		modePanel.appendChild(lmode);
		lmode.setStyle(WWindowScannerStyle.STATUS_TEXT_STYLE);

		modePanel.setStyle(WWindowScannerStyle.STATUS_NORMAL_BACKGROUND_STYLE);
		cRemoveMode.getComponent().addActionListener(this);
		row.setSpans("1,4");
		modePanel.setHeight("30px");

		// Center
		Center center = new Center();
		center.appendChild(table);
		mainPanel.appendChild(center);

		// South
		South south = new South();
		Vbox vbox = new Vbox();
		vbox.setWidth("100%");
		vbox.appendChild(southPanel);
		south.appendChild(vbox);

		rows = southPanel.newRows();
		row = rows.newRow();
		row.setSpans("0");
		row.appendChild(new Space());
		bDelete = createButton("Delete", "Delete", "Delete");
		bDelete.setHeight(BTN_HEIGHT);
		bDelete.setWidth(BTN_WIDTH);
		bDelete.addActionListener(this);
		row.appendChild(bDelete);
		bAddress = createButton("Address", "Print", "Address");
		bAddress.setHeight(BTN_HEIGHT);
		bAddress.setWidth(BTN_WIDTH);
		bAddress.setVisible(false);  //hide in the meantime
		bAddress.addActionListener(this);
		row.appendChild(bAddress);
		
		bPackingList = createButton("Packing List", "SelectAll", "Packing List");
		bPackingList.setHeight(BTN_HEIGHT);
		bPackingList.setWidth(BTN_WIDTH);
		bPackingList.addActionListener(this);
		row.appendChild(bPackingList);
		
		
		row.appendChild(new Space());
		lMessage.setStyle(WWindowScannerStyle.MESSAGE_TEXT_STYLE);
		panelMessage.setStyle(WWindowScannerStyle.STATUS_MESSAGE_BACKGROUND_STYLE);
		row.appendChild(new Space());
		row.appendChild(panelMessage);
		panelMessage.appendChild(lMessage);
		
		bComplete = createButton("Complete", "Process", "Complete Shipment");
		bComplete.setHeight(BTN_HEIGHT);
		bComplete.setWidth(BTN_WIDTH);
		bComplete.addActionListener(this);
		row.appendChild(bComplete);
		row.appendChild(new Space());
		bSave = createButton("Save & Exit", "Save", "Save & Exit");
		bSave.setHeight(BTN_HEIGHT);
		bSave.setWidth(BTN_WIDTH);
		row.appendChild(bSave);
		bSave.addActionListener(this);

		bSave.setDisabled(false);
		bComplete.setDisabled(false);
		bDelete.setDisabled(false);
		bAddress.setDisabled(false);
		bPackingList.setDisabled(false);

		//TODO - fill up edge Panel
		//row = rows.newRow();
		//row.appendChild(new Space());
		//row.appendChild(statusBar);
		//row.appendChild(edgePanel);
		//row.setSpans("0");
		
		lScanned.setValue("Scanned=3");
		lShort.setValue("Short=0");
		lExcess.setValue("Excess=1");
		lScanned.setStyle(WWindowScannerStyle.PANEL_TEXT_STYLE);
		lShort.setStyle(WWindowScannerStyle.PANEL_TEXT_STYLE);
		lExcess.setStyle(WWindowScannerStyle.PANEL_TEXT_STYLE);

//		fScanned.setStyle(PANEL_STATUS_STYLE);
//		fShort.setStyle(PANEL_STATUS_STYLE);
//		fExcess.setStyle(PANEL_STATUS_STYLE);
//		fScanned.setValue("0");
//		fShort.setValue("0");
//		fExcess.setValue("0");

		panelScanned.setStyle(WWindowScannerStyle.PANEL_SCANNED_STYLE);
		panelScanned.appendChild(lScanned);

		//TODO LAYOUT
		panelShort.setStyle(WWindowScannerStyle.PANEL_SHORT_STYLE);
		panelShort.appendChild(lShort);
//		panelShort.appendChild(fShort);

		panelExcess.setStyle(WWindowScannerStyle.PANEL_EXCESS_STYLE);
		panelExcess.appendChild(lExcess);
	//	panelExcess.appendChild(fExcess);

		rows = edgePanel.newRows();
		// First Row
		row = rows.newRow();
		row.setSpans("3");
		row.appendChild(panelScanned);
		row.appendChild(panelShort);
		row.appendChild(panelExcess);
		vbox.appendChild(edgePanel);
		mainPanel.appendChild(south);
		


	}

	private void dynInit() {

		StringBuilder shipmentInfo = new StringBuilder();
		MBPartner partner = m_InOut.getBPartner();

		shipmentInfo.append(partner.getName()).append(" - Order# ");
		int C_Order_ID = m_InOut.getC_Order_ID();
		if (C_Order_ID > 0) {
			MOrder order = new MOrder(ctx, C_Order_ID, null);
			shipmentInfo.append(order.getDocumentNo());
		}

		this.addEventListener(Events.ON_CLOSE, this);

		shipmentInfo.append(" - Shipment# ").append(m_InOut.getDocumentNo());

		lShipmentInfo.setText(shipmentInfo.toString());
		lShipmentInfo.setStyle(WWindowScannerStyle.DOCUMENT_TEXT_STYLE);

		lProductInfo.setStyle(WWindowScannerStyle.PRODUCT_TEXT_STYLE);
		lUPC.setText(Msg.translate(ctx, "UPC"));
		lSerial.setText(Msg.translate(ctx, "Serial"));
		lProductKey.setText("Product Key");
		lLocator.setText("Locator");

		cRemoveMode.setValue(false);
		lmode.setText("ITEMS SCANNED WILL BE ADDED");

		fUPC.getComponent().addEventListener(Events.ON_OK, this);
		fSerial.getComponent().addEventListener(Events.ON_OK, this);
		fProductKey.getComponent().addEventListener(Events.ON_OK, this);
		fLocator.getComponent().addEventListener(Events.ON_OK, this);

		ColumnInfo[] layout = new ColumnInfo[] {	
				new ColumnInfo(" ", ".", IDColumn.class, false, false, ""),
				new ColumnInfo(Msg.translate(Env.getCtx(), "M_Product_ID"),
						".", String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "PCode"), ".",
						String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Locator"), ".",
						String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Status"), ".",
						String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Scanned"), ".",
								Integer.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Serial"), ".",
						String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "PO No"), ".",
						String.class),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Notes"), ".",
						String.class) };

		table.prepareTable(layout, "", "", false, "");
		table.setSclass("scannerlist");
		table.addEventListener(Events.ON_SELECT, this);
		table.autoSize();
		//statusBar.setStatusDB("");
		lMessage.setWidth("600px");
		lMessage.setValue("");
		
		resetHashMap();
		reloadTable();

		fUPC.getComponent().focus();
	}


	/**
	 * Add Serial No to mapping table
	 * @param pCode
	 * @param serNo
	 * @return 
	 * 		true = added to map
	 * 		false = already SerNo already exists
	 */
	private boolean addSerialToProductMap(String pCode, String serNo, int M_InOutLine_ID)
	{
		String  key  = pCode + "-" + serNo;
		mapProductSerial.put(key, M_InOutLine_ID);
		return true;
	}
	
	/**
	 * Remove the Serial from the mapping
	 * @param pCode
	 * @param serNo
	 * @return
	 */
	private boolean deleteSerialProductMap(String pCode, String serNo)
	{
		String  key  = pCode + "-" + serNo;
		mapProductSerial.remove(key);
		return true;
	}
	
	/**
	 * 
	 * @param pCode
	 * @param serNo
	 * @return
	 */
	private int getLineFromProductSerial(String pCode, String serNo)
	{
		String  key  = pCode + "-" + serNo;
		return  mapProductSerial.get(key);
	}
	
	/**
	 * Serial OK to add - no duplicate
	 * @param pCode
	 * @param serNo
	 * @return
	 */
	private boolean isSerialOK(String pCode, String serNo)
	{
		String  key  = pCode + "-" + serNo;
		boolean isExists = mapProductSerial.containsKey(key);
		if (isRemoveMode) {
			// we are trying to remove this item, OK if you can find it
			if (!isExists) {
				showError("You are trying to remove SerialNo=" + serNo + " that don't exists.");
			}
			return isExists;
		}	
		else {
			// this is the opposite, we only allow Item to add if serial don't exists
			if (isExists) {
				showError("You are trying to add SerialNo=" + serNo + " that already exists in the receipt.");
			}
			return !isExists; 
		}	
	}
	
	/**
	 * Add acceptable product and qty to receive
	 * @param product_ID
	 * @param qty
	 * @return
	 * 		true = added to map
	 * 		false = duplicate
	 */
	private boolean addProductToQtyToScanMap(int product_ID, BigDecimal qty)
	{
		if (mapProductToScan.containsKey(product_ID)) {
			return false;
		}
		mapProductToScan.put(product_ID, qty);
		return true;
	}
	
	/**
	 * 
	 * @param product_ID
	 * @param qty
	 * @param add - flag to add or deduct qty
	 * @return
	 */
	private boolean updateScannedQtyProductMap(int product_ID, BigDecimal qty, boolean add)
	{
		BigDecimal qtyAllowed = mapProductToScan.get(product_ID);
		BigDecimal newQty = Env.ZERO;
		
		if (mapProductScanned.containsKey(product_ID)) {
			newQty = mapProductScanned.get(product_ID);
			if (add) {
				newQty = newQty.add(qty);
			}
			else {
				newQty = newQty.subtract(qty);
			}
		}
		
		if (newQty.signum() < 0) {
			return false;  //you're taking more than possible
		}
		if  (newQty.compareTo(qtyAllowed) > 0 && !isAllowOverReceipt) {
			return false;  // will result to over receipt
		}
		mapProductScanned.put(product_ID, newQty);
		return true; //mapping updated
	}
	
	private boolean isQtyOK(int product_ID, BigDecimal qty, boolean add)
	{
		BigDecimal qtyAllowed = mapProductToScan.get(product_ID);
		BigDecimal newQty = Env.ZERO;
		
		if (qtyAllowed == null) {
			qtyAllowed = Env.ZERO;
		}
		if (mapProductScanned.containsKey(product_ID)) {
			newQty = mapProductScanned.get(product_ID);
			if (add) {
				newQty = newQty.add(qty);
			}
			else {
				newQty = newQty.subtract(qty);
			}
		}
		
		if (newQty.signum() < 0) {
			showError("Process will result in negative receive qty.");
			return false;  //you're taking more than possible
		}
		if  (newQty.compareTo(qtyAllowed) > 0 && !isAllowOverReceipt) {
			showError("Process will result in Over Receipt.");
			return false;  // will result to over receipt
		}
		return true; //mapping updated
	}
	

	private void resetHashMap()
	{
		mapProductToScan.clear();
		mapProductScanned.clear();
		mapProductSerial.clear();
		
		String sql = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			sql = "SELECT m_product_id, SUM(qtytoscan) " +
 	              "FROM m_inoutline " +
		          "WHERE m_inout_id = ?" +
		          "GROUP BY m_product_id";
			pstmt = DB.prepareStatement(sql, m_trxName);
			pstmt.setInt(1, m_InOut.getM_InOut_ID());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				mapProductToScan.put(rs.getInt(1), rs.getBigDecimal(2));
			}
			
			sql = "SELECT m_product_id, SUM(qtyscanned) " +
			      "FROM m_inoutline " +
			      "WHERE m_inout_id = ? " +
			      "GROUP BY m_product_id";
				
			pstmt = DB.prepareStatement(sql, m_trxName);
			pstmt.setInt(1, m_InOut.getM_InOut_ID());
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				mapProductScanned.put(rs.getInt(1), rs.getBigDecimal(2));
			}
		
			sql = "SELECT p.value || '-' || asi.serno, iol.m_inoutline_id " +
			          "FROM m_inoutline iol " +
			          "  INNER JOIN m_attributesetinstance asi ON asi.m_attributesetinstance_id = iol.m_attributesetinstance_id " +
			          "  INNER JOIN m_product p ON iol.m_product_id = p.m_product_id " +
			          "WHERE asi.m_attributesetinstance_id <> 0 " +
			          "AND   iol.m_inout_id = ?";
				
			pstmt = DB.prepareStatement(sql, m_trxName);
			pstmt.setInt(1, m_InOut.getM_InOut_ID());
			rs = pstmt.executeQuery();
				
			while (rs.next()) {
				mapProductSerial.put(rs.getString(1), rs.getInt(2));
			}
							
		}
		catch (Exception e){
			
		}
		finally {
			DB.close(rs, pstmt);
		}
		


		
	}
	
	
	//reload table from database
	private void reloadTable() {


		table.setRowCount(0);

		String sql = "SELECT iol.m_inoutline_id, p.value as PCode, p.name as PName, l.value AS Locator, asi.serno, iol.qtytoscan " +
	             "  , iol.qtyscanned, COALESCE(o.documentno, 'NIL') AS OrderNo " +
	             "FROM m_inoutline iol " +
	             "  LEFT JOIN c_orderline ol ON iol.c_orderline_id = ol.c_orderline_id " +
	             "  LEFT JOIN c_order o ON ol.c_order_id = o.c_order_id " +
	             "  INNER JOIN m_product p ON iol.m_product_id = p.m_product_id " +
	             "  INNER JOIN m_locator l ON iol.m_locator_id = l.m_locator_id " +
	             "  LEFT JOIN m_attributesetinstance asi ON iol.m_attributesetinstance_id = asi.m_attributesetinstance_id " +
	             "WHERE iol.m_inout_id = ? " +
	             "ORDER BY p.value, holder_inoutline_id, m_inoutline_id";
		
		PreparedStatement pstmt = DB.prepareStatement(sql, m_trxName);
		ResultSet rs = null;
		try {
			pstmt.setInt(1, m_InOut.getM_InOut_ID());
			rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				        //int m_inoutline_id, String pCode, String pName, String locator
						// String serNo, BigDecimal QtyToScan, BigDecimal QtyScanned, String orderNo, boolean noCheck
				addRow(rs.getInt(1), rs.getString(2),  rs.getString(3),  rs.getString(4)
						,  rs.getString(5), rs.getBigDecimal(6),  rs.getBigDecimal(7), rs.getString(8), true);
			}

		}
		catch (Exception e) {
			log.severe(e.getMessage());
		}
		finally {
			DB.close(rs, pstmt);
		}
		
		updateItemCount();
		table.repaint();
	}
	
	/**
	 * Add Receipt Line to table, if exists, just update with new value
	 * @param m_inoutline_id
	 * @param pCode
	 * @param pName
	 * @param locator
	 * @param serNo
	 * @param QtyToScan
	 * @param QtyScanned
	 * @param orderNo
	 * @param noCheck - for efficiency, no need to check when loading for the first time
	 * @return row Position
	 */

	private int addRow(int m_inoutline_id, String pCode, String pName,
			String locator, String serNo, BigDecimal QtyToScan, BigDecimal QtyScanned,
			String orderNo, boolean noCheck) {
		int numrows = table.getItemCount();
		int row = 0;
		if (!noCheck) {
			for (int i= 0; i < numrows; i++)
			{
				if (m_inoutline_id == ((IDColumn)table.getValueAt(i, 0)).getRecord_ID())
				{
					//row already exists - just update
					row = i;
					break;
				}
			}
			
		}
		if (row == 0) {
			//nothing found, create new
			row = numrows ;
			table.setRowCount(row+1);
			table.setValueAt(new IDColumn(m_inoutline_id), row, 0);
		}
		
		table.setValueAt(pCode, row, 1);
		table.setValueAt(pName, row, 2);
		table.setValueAt(locator, row, 3);  //locator		
		table.setValueAt(QtyToScan, row, 4);
		table.setValueAt(QtyScanned, row, 5);
		table.setValueAt(serNo, row, 6);
		table.setValueAt(orderNo, row, 7);
		table.repaint();
		return row;

	}

	private Button createButton(String name, String image, String tooltip) {
		Button btn = new Button("");
		btn.setName("btn" + name);
		btn.setImage(ITheme.IMAGE_FOLDER + image + "24.png");
		btn.setTooltiptext(Msg.getMsg(Env.getCtx(), tooltip));

		LayoutUtils.addSclass("action-button", btn);

		btn.setTabindex(0);
		btn.addEventListener(Events.ON_CLICK, this);
		btn.setDisabled(true);

		return btn;
	}

	public void onEvent(Event event) throws Exception {
		//clean up erro message.
		//statusBar.setStatusLine("", false, false);
		lMessage.setValue("");
		if (Events.ON_CLICK.equals(event.getName())) {
			if (event.getTarget() == bSave) {
				onExit();
			} else if (event.getTarget() == bComplete) {
				onComplete();
			} else if (event.getTarget() == bDelete) {
				onDelete();
			} else if (event.getTarget() == bAddress) {
				onPrintLabel();
			} else if (event.getTarget() == bPackingList) {
				onPrintPackingList();
			}
		} else if (Events.ON_SELECT.equals(event.getName())) {
			if (event.getTarget() == this.table) {
				onEditRow();
				table.clearSelection();
			}

		} else if (Events.ON_OK.equals(event.getName())) {
			if (event.getTarget() == fSerial.getComponent()) {
				onSerial();
			} else if (event.getTarget() == fUPC.getComponent()) {
				onProductScan(fUPC.getComponent());
			} else if (event.getTarget() == fProductKey.getComponent()) {
				onProductScan(fProductKey.getComponent());
			}
		} else if (Events.ON_CHECK.equals(event.getName())) {
			if (event.getTarget() == cRemoveMode.getComponent()) {
				onModeTogle();
			}
		} else if (Events.ON_CLOSE.equals(event.getName())) {
			onExit();
		}
	}


	private void onEditRow() {
		int r = table.getSelectedRow();
		int M_InOutLine_ID = ((IDColumn)table.getValueAt(r, 0)).getRecord_ID();
		MInOutLine line = null;
		for (MInOutLine search : ioLines) {
			if (search.getM_InOutLine_ID() == M_InOutLine_ID) {
				line = search;
				break;
			}
		}
		if (line == null) {
			log.severe("This is not possible. We're just editing existing line.");
			return;
		}
		MProduct product = line.getProduct();

		String serial = (String) table.getValueAt(r, 6);
		String locator = (String) table.getValueAt(r, 3);
		String notes = (String) table.getValueAt(r, 8);
		BigDecimal startQty = (BigDecimal) line.get_Value("QtyScanned");
		boolean isSerial = false;
		if (serial != null) {
			isSerial = true;
		}
		IOLineEditorDialog dlg = new IOLineEditorDialog(product.toDisplayString()
				, startQty, isSerial, 100, !isSerial, Mode.EDIT, product, isCrossDock, notes);
		
		dlg.setLocator(locator);
		dlg.setAttribute(Window.MODE_KEY, Window.MODE_MODAL);
		SessionManager.getAppDesktop().showWindow(dlg);
		if (!dlg.isCancelled()) {
			if (dlg.getM_Locator_ID() == 0) {
				showError("This is unusual, you shouldn't get Locator=0");
				return;
			}
				
			line.setM_Locator_ID(dlg.getM_Locator_ID());
			line.setDescription(dlg.getNotes());
			table.setValueAt(dlg.getLocator(), r, 3);  //locator		
			if (isSerial) {
				//serial no changed
				if (!serial.equals(dlg.getSerial())) {  
					if (isSerialOK(product.getValue(), dlg.getSerial())) {
						MAttributeSetInstance asi = getAttributeSetInstance(product, dlg.getSerial());
						line.setM_AttributeSetInstance_ID(asi.getM_AttributeSetInstance_ID());
						
						table.setValueAt(dlg.getSerial(), r, 6);
					}
				}
				if (line.save(m_trxName)) {
					table.setValueAt(dlg.getNotes(), r, 8);
				}
				table.repaint();
				updateItemCount();
				return;
			}
			//non serial
			BigDecimal changed = dlg.getQty().subtract(startQty);
//			if (changed.signum() > 0) {
				//check if qty limit permits
				if (updateScannedQtyProductMap(product.getM_Product_ID(), changed, true)) {
					line.set_ValueOfColumn("QtyScanned", dlg.getQty());
					table.setValueAt(dlg.getQty(), r, 5);
				}
				else {
					showError("Update not possible. Over receipt.");
				}
			//}
			if (line.save(m_trxName)) {
				table.setValueAt(dlg.getNotes(), r, 8);

			}
			updateItemCount();
			table.repaint();
		}

	}

	private void onModeTogle() {
		isRemoveMode = (Boolean) cRemoveMode.getValue();
		if (isRemoveMode) {
			modePanel.setStyle(WWindowScannerStyle.STATUS_HIGHLIGHTED_BACKGROUND_STYLE);
			lmode.setText("ITEMS SCANNED WILL BE REMOVED FROM THE LIST");
		} else {
			modePanel.setStyle(WWindowScannerStyle.STATUS_NORMAL_BACKGROUND_STYLE);
			lmode.setText("ITEMS SCANNED WILL BE ADDED");
			//setInoutLine();
		}
	}

	private MProduct findProduct(String barcode, boolean isUPC)
	{
		String key = barcode;
		if ((barcode.length() > WPackageScannerWindow.PARTNO_MAXLENGTH) ) {		
			key = barcode.substring(0,  WPackageScannerWindow.PARTNO_MAXLENGTH) + "%";
		}
		List<MProduct> productsFound = null;

		if (isUPC) {
			productsFound = MProduct.getByUPC(ctx, key, m_trxName);
		}
		else {
			productsFound = new Query(ctx, MProduct.Table_Name, "value LIKE ?",
					m_trxName).setParameters(key).list();
		}
		
		if (productsFound.size() == 0) {
			showError("Invalid Product code : " + key);
			return null;
		}
		
		if (productsFound.size() > 1) {
			showError("Multiple products found with trucated Bar Code: " + key);
			return null;
		}
		
		MProduct product = productsFound.get(0);
		
		if (!isAllowOverReceipt) {
			if (!mapProductToScan.containsKey(product.getM_Product_ID())) {
				showError( product.toString() + "is not on list.");
				return null;
			}
		}
		
		return product;
		
	}
	
	
	private void onProductScan(Textbox text)
	{
		boolean isUPC = false;
		if (text == fUPC.getComponent()) {
			isUPC = true;
		}
		
		MProduct product = findProduct(text.getValue(),isUPC);
		
		if (product == null)
		{
			lProductInfo.setText("");
			text.setValue("");
			return;
		}

		lProductInfo.setText(product.toDisplayString());
		boolean isSerial = isProductSerialised(product);
		
		//statusBar.setStatusLine("", false, false);
		lMessage.setValue("");
		//still need to check the serial
		if (isSerial) {
			fSerial.setValue("");
			fSerial.setHasFocus(true);
			fSerial.getComponent().focus();
		}
		
		processScan(product, null);
		
		updateItemCount();
	}
	
	/**
	 * At this stage, product is valid but needs to check qty
	 * @param product
	 */
	private void processScan(MProduct product, String serialNo)
	{
		
		boolean isSerial = false;
		BigDecimal initialValue = Env.ONE;
		boolean isSerialOK = false;
		BigDecimal qtyScan = null;
		String sql = "SELECT value " +
	             "FROM m_locator " +
	             "WHERE m_locator_id IN (SELECT MAX(iol.m_locator_id) " +
	             "                       FROM m_inoutline iol " +
	             "                       WHERE iol.m_product_id = ? " +
	             "						 AND iol.holder_inoutline_id = iol.m_inoutline_id " + 
	             "						 AND iol.isExcessReceipt = 'N'	" +
	             "                       AND   iol.m_inout_id = " + m_InOut.getM_InOut_ID() +
	             "                       )";		
		
		//List
		String defaultLocator = DB.getSQLValueString(m_trxName, sql, product.getM_Product_ID());
		
		if (serialNo != null) {
			isSerial = true;
			qtyScan = getQtyReceived(isSerial, initialValue, true, product.toDisplayString(), defaultLocator, product);
			if (!isQtyOK(product.getM_Product_ID(), qtyScan, !isRemoveMode)) {
				return;
			}
			
			isSerialOK = isSerialOK(product.getValue(), serialNo);
			if (!isSerialOK) {
				return;
			}
		}
		else {
			initialValue = mapProductToScan.get(product.getM_Product_ID());
			BigDecimal productScanned = mapProductScanned.get(product.getM_Product_ID());
			boolean isNotOnList = false;
			
			if (initialValue == null) {
				defaultLocator = MLocator.get(ctx, excessDeliveryLocator_ID).getValue(); 
				initialValue = Env.ZERO;  //excess receipt
				isNotOnList = true;
			}
			if (productScanned == null) {
				productScanned = Env.ZERO;  //excess receipt
			}

			initialValue = initialValue.subtract(productScanned);
			qtyScan = getQtyReceived(isSerial, initialValue, false, product.toDisplayString(), defaultLocator, product);
			
			// not on the list just save it to db
			if (isNotOnList) {
				addExcessReceiptLine(product, qtyScan);
				return;
			}
			
			if (!isQtyOK(product.getM_Product_ID(), qtyScan, !isRemoveMode)) {
				return;
			}
		}
			
		// this shouldn't happen - dialog shouldn't allow an invalid locator
		if (m_M_Locator_ID == 0) {
			return;
		}
		
		//everything works find, we can update the user of the new status and proceed to next 
		if (loadItemToMaterialReceipt(product, serialNo, qtyScan, m_M_Locator_ID, !isRemoveMode))
		{
			updateScannedQtyProductMap(product.getM_Product_ID(), qtyScan, !isRemoveMode);
			reloadTable();
		}
	}

	/**
	 * @param product
	 * @param qtyScan
	 */
	private void addExcessReceiptLine(MProduct product, BigDecimal qtyScan) {
		//this is not on the list - create a receipt line with 0 movementqty
		if (isAllowOverReceipt) {
			MInOutLine dummy = null;
			
			if (m_ExcessLine == null) {
				dummy = new MInOutLine(m_InOut);
				dummy.setC_Activity_ID(m_InOut.getC_Activity_ID());
				dummy.setM_Product_ID(product.getM_Product_ID());
				dummy.setM_AttributeSetInstance_ID(0);
				dummy.setM_Locator_ID(m_M_Locator_ID);
				dummy.setLine((ioLines.size() + 1) * 10);
				dummy.set_ValueOfColumn("QtyScanned", qtyScan);
				dummy.setQty(Env.ZERO);
				dummy.set_ValueOfColumn("QtyToScan", Env.ZERO);
				dummy.set_ValueOfColumn("IsExcessReceipt", "Y");
				dummy.save(m_trxName);
				ioLines.add(dummy);
			}
			else {
				dummy = m_ExcessLine;
				BigDecimal scanned = (BigDecimal) dummy.get_Value("QtyScanned");
				dummy.set_ValueOfColumn("QtyScanned", scanned.add(qtyScan));
				dummy.saveEx(m_trxName);
				
			}

			updateScannedQtyProductMap(product.getM_Product_ID(), qtyScan, !isRemoveMode);
			reloadTable();
		}
	}

	/**
	 * Match the scanned items to Material Receipt and save
	 * @param product
	 * @param serNo
	 * @param qtyScanned
	 * @param M_Locator_ID
	 * @param add
	 * @return
	 */
	private boolean loadItemToMaterialReceipt(MProduct product, String serNo,
			BigDecimal qtyScanned, int M_Locator_ID, boolean add)
	{
		List<MInOutLine> ioLineCandidates = new ArrayList<MInOutLine>();
		for (MInOutLine line : ioLines) {
			if (line.getM_Product_ID() == product.getM_Product_ID()) {
				//just list all lines that have this product
				ioLineCandidates.add(line);
			}
		}
		
		if (ioLineCandidates.size() == 0 && !isAllowOverReceipt) {
			log.severe("loadItemToMaterialReceipt - Various checks should prevent this to happen.");
			return false;
		}


		//deduct 1 item for the list - alway 1 item at time when removing
		if (!add) 	{
			if (serNo != null) {
				//it's serial, just look in the map
				return processRemoveSerial(product, serNo, ioLineCandidates);
			}
			return processRemoveProduct(qtyScanned, M_Locator_ID, ioLineCandidates);
		}

		if (serNo != null) 	{
			return processAddSerial(product, serNo, M_Locator_ID, ioLineCandidates);
		}
		
		return processAddProduct(qtyScanned, M_Locator_ID, ioLineCandidates);
	}

	/**
	 * This is the heart of receipting process - make sure the received items are allocated properly
	 * @param qtyScanned
	 * @param ioLineCandidates
	 * @return
	 */
	private boolean processAddProduct(BigDecimal qtyScanned, int M_Locator_ID,
			List<MInOutLine> ioLineCandidates) {
		//at this stage, we expect received qtyScanned to fit in
		BigDecimal qtyLeftToDistribute = qtyScanned;

//		Comparator<MInOutLine> ascHolder_ID = new BeanComparator("holder_inoutline_id"); 
//		Comparator<MInOutLine> ascM_InOutLine_ID = new BeanComparator("m_inoutline_id"); 
//		Collections.sort(ioLineCandidates, ascHolder_ID);
		// just keep on allocating the qty scanned

		//we must fulfill all the original lines requirement
		//before proceeding to the next set of line
		
		//find on existing lines first, and if empty, change locator if necessary and use it 
		int lastHolder_InoutLine_ID = 0;
		BigDecimal runningQtyScanned = null;
		BigDecimal allowedQty= null;
		List<MInOutLine> activeLines = null; //they contain all lines having same holder_inoutline_id
		for (MInOutLine line : ioLineCandidates) {
			if (lastHolder_InoutLine_ID != line.get_ValueAsInt("holder_inoutline_id"))
			{
				if (lastHolder_InoutLine_ID != 0) {
					qtyLeftToDistribute = distributeToReceiptLines(
							M_Locator_ID, qtyLeftToDistribute,
							lastHolder_InoutLine_ID, runningQtyScanned,
							allowedQty, activeLines);

					if (qtyLeftToDistribute.signum() == 0) {
						//log.severe("Unable to distribute " + product.toDisplayString() + "=" + qtyLeftToDistribute);
						return true;
					}
				}

				//reset and start all over again
				lastHolder_InoutLine_ID = line.get_ValueAsInt("holder_inoutline_id");
				activeLines = new ArrayList<MInOutLine>();
				runningQtyScanned = Env.ZERO;
				allowedQty = Env.ZERO;
			}
			
			activeLines.add(line);
			runningQtyScanned = runningQtyScanned.add((BigDecimal)line.get_Value("QtyScanned"));
			allowedQty= allowedQty.add((BigDecimal)line.get_Value("QtyToScan"));
			

		}
		if (runningQtyScanned == null) {
			runningQtyScanned = Env.ZERO;
		}
		if (allowedQty == null) {
			allowedQty = Env.ZERO;
		}
		if (activeLines == null) {
			activeLines = new ArrayList<MInOutLine>();
		}
		qtyLeftToDistribute = distributeToReceiptLines(
				M_Locator_ID, qtyLeftToDistribute,
				lastHolder_InoutLine_ID, runningQtyScanned,
				allowedQty, activeLines);

		if (qtyLeftToDistribute.signum() != 0) {
			log.severe("Unable to distribute " + product.toDisplayString() + "=" + qtyLeftToDistribute);
			return false;
		}

		return true;
	}

	/**
	 * @param M_Locator_ID
	 * @param qtyLeftToDistribute
	 * @param lastHolder_InoutLine_ID
	 * @param runningQtyScanned
	 * @param allowedQty
	 * @param activeLines
	 * @param line
	 * @return
	 */
	private BigDecimal distributeToReceiptLines(int M_Locator_ID,
			BigDecimal qtyLeftToDistribute, int lastHolder_InoutLine_ID,
			BigDecimal runningQtyScanned, BigDecimal allowedQty,
			List<MInOutLine> activeLines) 
	{
		MInOutLine originalLine = null;
		BigDecimal distributeQty = allowedQty.subtract(runningQtyScanned);

		if (qtyLeftToDistribute.compareTo(distributeQty) < 1) {
			distributeQty = qtyLeftToDistribute; // don't distribute more than you're asked to
		}
		// Collections.sort(activeLines, ascM_InOutLine_ID);

		int nrow = 0;
		// process activelines
		for (MInOutLine activeLine : activeLines) {
			if (qtyLeftToDistribute.signum() == 0) {
				// nothing to distribute
				return Env.ZERO;
			}
			// only the main line can be altered to follow
			// M_Locator_ID for the first time
			if (activeLine.get_ValueAsInt("m_inoutline_id") == lastHolder_InoutLine_ID) 
			{
				originalLine = activeLine;
				if (((BigDecimal) activeLine.get_Value("QtyScanned")).signum() == 0) 
				{
					BigDecimal qty = distributeQty;
					// allocate qty that will fit in
					activeLine.setM_Locator_ID(M_Locator_ID);
					activeLine.set_ValueOfColumn("QtyScanned", qty);
					activeLine.saveEx(m_trxName);
					distributeQty = Env.ZERO;
					qtyLeftToDistribute = qtyLeftToDistribute
							.subtract(qty);

					if (qtyLeftToDistribute.signum() == 0) {
						// exit immediately - no need to process
						// other stuff
						return Env.ZERO;
					}
					continue; // next active line
				}
			}

			if (activeLine.getM_Locator_ID() == M_Locator_ID) {
				BigDecimal qty = distributeQty;
				BigDecimal scanned = (BigDecimal) activeLine.get_Value("QtyScanned");
				activeLine.set_ValueOfColumn("QtyScanned", scanned.add(qty));
				activeLine.saveEx(m_trxName);
				distributeQty = distributeQty.subtract(qty);
				qtyLeftToDistribute = qtyLeftToDistribute.subtract(qty);

				if (qtyLeftToDistribute.signum() == 0) {
					// exit immediately - no need to process other
					// stuff
					return Env.ZERO;
				}
			}

			nrow++;
		}
		
		if (originalLine == null) {
			log.severe("It shouldn't reach this part - no original receipt line.");
			return new BigDecimal(-1);
		}

		// you reached this part - still have qty to distribute
		// create new line with this as holder
		// create new line - same but different locator
		if (distributeQty.signum() > 0) {
			MInOutLine newLine = new MInOutLine(
					(MInOut) originalLine.getM_InOut());
			MInOutLine.copyValues(originalLine, newLine);
			newLine.setC_Activity_ID(m_InOut.getC_Activity_ID());
			newLine.setLine(newLine.getLine() + nrow);
			newLine.setM_Locator_ID(M_Locator_ID);
			newLine.setQty(Env.ZERO);
			newLine.set_ValueOfColumn("QtyToScan", Env.ZERO);
			newLine.set_ValueOfColumn("QtyScanned", distributeQty);
			newLine.set_ValueOfColumn("IsExcessReceipt", "N");
			newLine.save(m_trxName);
			qtyLeftToDistribute = qtyLeftToDistribute
					.subtract(distributeQty);
			ioLines.add(newLine);
		}

		if (qtyLeftToDistribute.signum() == 0) {
			// exit immediately - no need to process other stuff
			return Env.ZERO;
		}
		// create new line - excess
		int excess_locator_id = ((MWarehouse)m_InOut.getM_Warehouse()).get_ValueAsInt("ExcessDelivery_Locator_ID");
		if (excess_locator_id == 0) {
			showError("Excess Delivery Locator not set in the warehouse");
			return new BigDecimal(-1);
		}
		m_M_Locator_ID = excess_locator_id;
		addExcessReceiptLine(originalLine.getProduct(), qtyLeftToDistribute);

		return Env.ZERO;
	}

	/**
	 * @param product
	 * @param serNo
	 * @param M_Locator_ID
	 * @param ioLineCandidates
	 */
	private boolean processAddSerial(MProduct product, String serNo,
			int M_Locator_ID, List<MInOutLine> ioLineCandidates) {
		//it's Serial, we only need to add 1 item, so just chuck it in
		//to the first receipt line available
		for (MInOutLine line : ioLineCandidates)
		{
			BigDecimal scanned = (BigDecimal)line.get_Value("QtyScanned");
			if (scanned.signum() == 0) {
				
				MAttributeSetInstance asi = getAttributeSetInstance(product, serNo);
				
				line.set_ValueOfColumn("QtyScanned", Env.ONE);
				line.setM_Locator_ID(M_Locator_ID);
				line.setM_AttributeSetInstance_ID(asi.getM_AttributeSetInstance_ID());
				line.save(m_trxName);
				
				addSerialToProductMap(product.getValue(), serNo, line.getM_InOutLine_ID());
				
				return true;
			}
		}
		return false;
	}

	/**
	 * @param qtyScanned
	 * @param M_Locator_ID
	 * @param ioLineCandidates
	 */
	private boolean processRemoveProduct(BigDecimal qtyScanned, int M_Locator_ID,
			List<MInOutLine> ioLineCandidates) {
		for (MInOutLine line : ioLineCandidates) {
			if (line.getM_Locator_ID() == M_Locator_ID) {
				BigDecimal qty = (BigDecimal)line.get_Value("QtyScanned");
				qty = qty.subtract(qtyScanned);
				if (qty.signum() >= 0) 
				{
					//we are able to remove back the scanned item from the receipt
					line.set_ValueOfColumn("QtyScanned", qty);
					line.save(m_trxName);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param product
	 * @param serNo
	 * @param ioLineCandidates
	 */
	private boolean processRemoveSerial(MProduct product, String serNo,
			List<MInOutLine> ioLineCandidates) {
		MInOutLine line = null;
		int m_inoutline_id = getLineFromProductSerial(product.getValue(), serNo);
		for (MInOutLine l : ioLineCandidates) 
		{
			if (l.getM_InOutLine_ID() == m_inoutline_id) 
			{
				line = l;
				break;
			}
		}
		if (line != null) {
			MAttributeSetInstance asi = (MAttributeSetInstance) line.getM_AttributeSetInstance();
			line.setM_AttributeSetInstance_ID(0);
			line.set_ValueOfColumn("QtyScanned", Env.ZERO);
			line.save(m_trxName);
			//delete MAttributeSetInstance as well to prevent from piling up
			asi.delete(true);
			asi.save(m_trxName);
			deleteSerialProductMap(product.getValue(), serNo);
			return true; //all done, going home
		}
		log.severe("Trying to remove a Serial No from list that don't exists.");
		return false; //should reach here in
	}

	/*
	private void onUPC(boolean isKey) {
		String strUPC = (String) fUPC.getValue();
		if (isKey) {
			strUPC = (String) fProductKey.getValue();
		}
		if (strUPC == null || "".equals(strUPC)) {
			lProductInfo.setText("");
			return;
		}
		if ((strUPC.length() > WPackageScanner.PARTNO_MAXLENGTH) && (!isKey)) {		
			strUPC = strUPC.substring(0,  WPackageScanner.PARTNO_MAXLENGTH) + "%";
		}
//		if (m_Locator_ID == 0) {
//			showError("No Locator set");
//			return;
//		}

		List<MProduct> productList = null;
		if (isKey) {
			productList = new Query(ctx, MProduct.Table_Name, "value LIKE ?",
					m_trxName).setParameters(strUPC).list();
			
		} else {
			productList = MProduct.getByUPC(ctx, strUPC, m_trxName);
		}
		if (productList.size() == 0) {
			fUPC.setValue("");
			fSerial.setValue("");
			if (isKey) {
				showError("Invalid Product code : " + strUPC);
			} else {
				showError("Invalid UPC code : " + strUPC);
			}

			lProductInfo.setText("");
			return;
		}
		if (productList.size() > 1) {
			fUPC.setValue("");
			fSerial.setValue("");
			showError("Multiple product with UPC code : " + strUPC);
			return;
		}

		product = productList.get(0);

		lProductInfo.setText("[" + product.getValue() + "]" + " - " + product.getName());
		MInOutLine ioLine = matchLine(product, null);

		if (ioLine == null) {
			// Shipment line not found and there is no scanned product
			showError("UPC " + strUPC + " not on shipment");
			fUPC.setValue("");
			fUPC.getComponent().focus();
			product = null;
		} else {
			if (isProductSerialised(product)) {
				fSerial.setValue("");
				fSerial.setHasFocus(true);
				fSerial.getComponent().focus();
				statusBar.setStatusLine("", false, false);
			} else {
				
				//hightlight UPC text to be overriden by the next scan
				fUPC.setHasFocus(true);
				Textbox textField = fUPC.getComponent();
				textField.select();
				
				// Scan product with UPC code only
				if (isRemoveMode) {
					m_Locator_ID = 0;
					BigDecimal received = getQtyReceived(Env.ONE, false);
					lineDelete(product, Env.ONE, null);
				} else {
					BigDecimal remaining = (BigDecimal)ioLine.get_Value("QtyToScan");
					remaining = remaining.subtract(new BigDecimal (mapLineScanned.get(ioLine.get_ValueAsInt("holder_inoutline_id"))));
					
					BigDecimal received = getQtyReceived(remaining, true);
					String msg = addScan(ioLine, received, null);
					if (msg != null) {
						showError(msg);
					}
				}
				try {
					DB.commit(true, m_trxName);
				} catch (Exception e) {
					showError("Commit Error " + e.getMessage());
					return;
				}
				updateItemCount();
			}
		}
	}

*/
	private BigDecimal getQtyReceived(boolean isSerial, BigDecimal initialValue
			, boolean readOnly, String title, String defaultLocator, MProduct product) 
	{
		
		Mode mode = Mode.ADD;
		if (isRemoveMode) {
			mode = Mode.REMOVE;
		}
		m_M_Locator_ID = 0; //initialise
		
		IOLineEditorDialog dialog = new IOLineEditorDialog(
				title, initialValue,
				isSerial, 100, !readOnly, mode, product, isCrossDock, null);
		
		dialog.setLocator(defaultLocator );
		dialog.setAttribute(Window.MODE_KEY, Window.MODE_MODAL);
		SessionManager.getAppDesktop().showWindow(dialog);
		
		if (!dialog.isCancelled()) {
			m_M_Locator_ID = dialog.getM_Locator_ID();
			return dialog.getQty();
		}

		return BigDecimal.ZERO;
	}

/*	private MInOutLine matchLine(MProduct product, String serial) {

		if (product == null) {
			showError("No Matched Product");
			return null;
		}

		if (serial == null) {
			for (MInOutLine line : ioLines) {
				if (line.getM_Product_ID() == product.getM_Product_ID()) {
					return line;
				}
			}
		}

		//for serialised - need to match serial no as well
		MInOutLine matchedLine = null;
		for (MInOutLine line : ioLines) {
			if (line.getM_Product_ID() == product.getM_Product_ID()) {
				if (line.getQtyEntered().compareTo((BigDecimal) line.get_Value("QtyToScan")) >= 0) {
					continue;
				}
				return line;
			}
		}
		
		return matchedLine;
	}*/

	private void onSerial() {
		
		String strSerial = (String) fSerial.getValue();
		processScan(product, strSerial);
		
	}
	
	
	private MAttributeSetInstance getAttributeSetInstance(MProduct product, String serial) {
		if (!(serial== null)) {
			MAttributeSetInstance asi = null;
			//check for existing serial
			String sql = "SELECT asi.m_attributesetinstance_id FROM m_attributesetinstance asi " + 
					"INNER JOIN m_product p ON p.m_attributeset_id = asi.m_attributeset_id " + 
					"WHERE asi.m_attributeset_id = ? AND  asi.serno = ? AND p.m_product_id = ?";
			int instance_id = DB.getSQLValue(null, sql, product.getM_AttributeSet_ID(), serial, product.getM_Product_ID());
			//use existing instance
			if (instance_id > 0) {
				asi = MAttributeSetInstance.get(ctx, instance_id, product.getM_Product_ID());
			}
			if (asi != null) {
				return asi;
			}
			asi = MAttributeSetInstance.create(ctx, product, m_trxName);
			asi.setSerNo(serial);
			asi.setDescription(serial);
			asi.save();
			return asi;
		}
		return MAttributeSetInstance.create(ctx, product, m_trxName);
	}
	
	private void onExit() {
		Trx trx = Trx.get(m_trxName, false);
		
		if (trx != null) {
			trx.close();
		}
		this.detach();
	}

	private void onComplete() 
	{
		try 
		{
			runCleanup();
			DB.commit(true, m_trxName);
			

			
			//reset qty(MovementQty, QtyEntered) = QtyScanned
			String sql = "UPDATE m_inoutline " +
		          "   SET MovementQty = QtyScanned, QtyEntered = QtyScanned " +
		          "WHERE m_product_id <> 0 AND QtyScanned IS NOT NULL AND isExcessReceipt = 'N' AND m_inout_id = ?";
			DB.executeUpdate(sql, m_InOut.getM_InOut_ID(), m_trxName);

			m_InOut.set_TrxName(m_trxName);
			if ("IN".equals(m_InOut.getDocStatus())) {
				if (!m_InOut.processIt(MInOut.DOCACTION_Prepare)) {
					showError(Msg.parseTranslation(ctx, m_InOut.getProcessMsg()));
					try {
						DB.rollback(false, m_trxName);
					} catch (Exception ex) {
					}
					m_InOut.set_TrxName(null);
					return;
				}
			}

			
			if (!m_InOut.processIt(MInOut.DOCACTION_Complete)) {
				showError(Msg.parseTranslation(ctx, m_InOut.getProcessMsg()));
				try {
					DB.rollback(false, m_trxName);
				} catch (Exception ex) {
				}
				m_InOut.set_TrxName(null);
				return;
			}
			if (!m_InOut.save(m_trxName)) {
				showLogError("Can not save shipment");
				try {
					DB.rollback(false, m_trxName);
				} catch (Exception ex) {
				}
				m_InOut.set_TrxName(null);
				return;
			}
			try {
				DB.commit(true, m_trxName);
			} catch (Exception e) {
				showError("Rollback Error -" + e.getMessage());
				m_InOut.set_TrxName(null);
				return;
			}

			onExit();
		} catch (Exception e) {
			showLogError(e.getMessage());
			try 
			{
				DB.rollback(false, m_trxName);
			}
			catch (Exception ex) 
			{
			}
		}
	}


	private boolean printMaterialReceipt(ProcessInfo pi, ASyncProcess parent ) {
		return ReportCtl.startDocumentPrint(ReportEngine.SHIPMENT, pi.getRecord_ID(), parent, m_WindowNo, !pi.isPrintPreview());

	}
	
	private boolean printtoScreen(String title)
	{
		//MPrintFormat pf = new MPrintFormat(ctx, printFormatID, null);

//		ReportEngine  re = ReportEngine.get(ctx, pInfo);
		ReportEngine re = ReportEngine.get(ctx, ReportEngine.SHIPMENT, m_InOut.getM_InOut_ID(), null);
		if (re == null) {
			return false;
		}
//		re.setPrintFormat(pf);

		try {
			String path = System.getProperty("java.io.tmpdir");
			File file = File.createTempFile("RECEIPT", ".pdf", new File(path));
			
			//re.createPDF(file);
			file = re.getPDF();
			
			Clients.showBusy(null, false);
			Window win = new SimplePDFViewer(title, new FileInputStream(file));
			SessionManager.getAppDesktop().showWindow(win, "center");
			
		} catch (Exception e) {
			log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		}

		return true;
	}
	
	private void onPrintLabel() throws Exception 
	{
		if (printtoScreen("Print Label")) {
			showError("Job Sent");
		}
	}
	
	private void onPrintPackingList() throws Exception {
		if (printtoScreen("Print Label")) {
			showError("Job Sent");
		}
		//printMaterialReceipt(pInfo, null);
		
	}
	
	private void onDelete() {
		
		if(table.getRowCount()>0 && FDialog.ask(m_WindowNo, null, "Do you want to delete the Contents?")){
			//deletePackage = true;
//			deleteLines = true;
			//cleanup the lines
			//list through the lines and clear it
			for (MInOutLine line : ioLines) {
				line.set_ValueOfColumn("QtyScanned", Env.ZERO);
				line.setM_AttributeSetInstance_ID(0);
				line.save(m_trxName);
			}
			
			try {
				DB.commit(true, m_trxName);
				runCleanup();
			} catch (Exception e) {
				showError("Commit Error " + e.getMessage());
				return;
			}
			resetHashMap();
			reloadTable();
		}
	}

	/**
	 * 
	 */
	private void runCleanup() {
		String sql = "delete from m_inoutline " +
				"where QtyScanned = 0 " +
				" and isExcessReceipt='Y'" +
				" and m_inout_id = ?";
		DB.executeUpdate(sql, m_InOut.getM_InOut_ID(), m_trxName);
	}

	private MLocator getNewLocator(MWarehouse whse, MLocator existingLocator) {
		
		MLocator locator = null;

		locator = MLocator.get(ctx, whse.getM_Warehouse_ID(), existingLocator.getValue(), existingLocator.getX(), existingLocator.getY(), existingLocator.getZ());
		if (locator != null) {
			return locator;
		}
		locator = new MLocator(whse, existingLocator.getValue());
		locator.setX(existingLocator.getX());
		locator.setY(existingLocator.getY());
		//locator.setZ(existingLocator.getZ());
		locator.setPriorityNo(locator.getPriorityNo());
		locator.setIsDefault(false);
		locator.set_TrxName(m_trxName);
		if (locator.save()) {
			return locator;
		}
			
		return null;
	}

	/**
	 * Update Scanned, Short, and Excess
	 */
	private void updateItemCount() {
		String sql = "SELECT SUM(QtyScanned) FROM M_InOutLine WHERE M_InOut_ID = ?";
		int cnt = DB.getSQLValue(m_trxName, sql,m_InOut.getM_InOut_ID());
		lScanned.setValue("Scanned=" + cnt);
		sql = "SELECT SUM(COALESCE(QtyToScan,0) - COALESCE(QtyScanned,0)) FROM M_InOutLine WHERE M_Locator_ID <>" + excessDeliveryLocator_ID + " AND M_InOut_ID = ?";
		cnt = DB.getSQLValue(m_trxName, sql,m_InOut.getM_InOut_ID());
		lShort.setValue("Short=" + (cnt<0?0:cnt));
		sql = "SELECT SUM(QtyScanned) FROM M_InOutLine WHERE M_Locator_ID = " + excessDeliveryLocator_ID + " AND M_InOut_ID = ?";
		int excess = DB.getSQLValue(m_trxName, sql,m_InOut.getM_InOut_ID());
		lExcess.setValue("Excess=" + excess);
		//lMessage.setText("Total Items Scanned = " + cnt);
	}

	private void showLogError(String msgDefault) {
		String msg = null;
		ValueNamePair err = CLogger.retrieveError();
		if (err != null)
			msg = err.getName();
		if (msg == null || msg.length() == 0)
			msg = msgDefault;

		showError(msg);
	}

	private void showError(String msg) {
		//statusBar.setStatusLine(msg, true, true);
		lMessage.setValue(msg);
	}

	
	
	private boolean isProductSerialised(MProduct product) {
		if (product.getAttributeSet() != null) {
			return product.getAttributeSet().isSerNo();
		}
		return false;
	}
	
	
	private void setTempLineHolder() {
		String sql = null;
		if (m_IsPreLoad) {
			sql = "UPDATE m_inoutline " +
		             "   SET holder_inoutline_id = m_inoutline_id, qtytoscan = qtyentered, qtyscanned = qtyentered " +
		             "WHERE m_inout_id = ? " +
		             "AND   holder_inoutline_id IS NULL";
		} else {
			sql = "UPDATE m_inoutline " +
		             "   SET holder_inoutline_id = m_inoutline_id, qtytoscan = qtyentered, qtyscanned = 0 " +
		             "WHERE m_inout_id = ? " +
		             "AND   holder_inoutline_id IS NULL";
		}
		 
		DB.executeUpdate(sql, m_InOut.getM_InOut_ID(), null);
				
	}
}
