package org.adaxa.window;


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.adaxa.form.WShipmentScannerForm;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WStringEditor;
import org.adempiere.webui.window.FDialog;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MManifest;
import org.compiere.model.MManifestLine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;

/**
 * 
 * @author jobriant
 *
 */
public class ShipmentManifestDialog extends Window implements EventListener{
	
	private static final String FIELD_WIDTH = "250px";
	private static final String FIELD_LABEL_WIDTH = "100px,200px";

	private static CLogger			log = CLogger.getCLogger (ShipmentManifestDialog.class);
	
	private static final String HEADER_TEXT_STYLE = "color: darkblue; font-size: 16px;";

	public enum ManifestAction {UPDATE}
	/**
	 * 
	 */
	private static final long serialVersionUID = -3852236029054284848L;
	private int max;
	private BigDecimal number;
	private boolean cancelled = false;
	private Label lHeader;
	private Label status;
	
	private Grid inputPanel = GridFactory.newGridLayout();
	
	private WStringEditor fDocumentList = new WStringEditor();
	private WStringEditor fDocumentNo = new WStringEditor();
	private Listbox fManifest = ListboxFactory.newDropdownListbox();
	private WStringEditor fErrorMsg = new WStringEditor();
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private ConfirmPanel confirmPanel = null;
	private Timestamp m_CurrentTime = null;
	
	private Properties m_ctx;
	private StringBuffer m_list = null;
	private ManifestAction m_action = ManifestAction.UPDATE;
	
	private WListbox m_table = null;
	private ArrayList<Integer> m_scanned = new ArrayList<Integer>();
	private int m_targetStatus = 0; 
	
	/**
	 * 
	 * @param title
	 * @param text
	 * @param isSerial
	 * @param maxValue
	 */
	public ShipmentManifestDialog(ManifestAction action, int windowNo, WListbox table) {
		super();
		m_ctx = Env.getCtx();
		m_list = new StringBuffer();
		m_action = action;
		m_table = table;
		
		if (m_action == ManifestAction.UPDATE) {
			setTitle("Assign Customer shipment to new Shipment Manifest");
		}
		
		init();

		
	}
	


	private void init() {
		setBorder("normal");
		
	
		lHeader = new Label();
		Date date = new Date();
		m_CurrentTime = new Timestamp(date.getTime());
		lHeader.setText(sdf.format(m_CurrentTime));
		lHeader.setStyle(HEADER_TEXT_STYLE);
		
		Rows rows = inputPanel.newRows();
		Row row = rows.newRow();
		row.appendChild(new Label("Date and Time").rightAlign());
		row.appendChild(lHeader);
		
		row = rows.newRow();
		row.appendChild(new Label("Document No").rightAlign());
		fDocumentNo.getComponent().setWidth(FIELD_WIDTH);
		row.appendChild(fDocumentNo.getComponent());

		row = rows.newRow();
		row.appendChild(new Label("Documents Scanned").rightAlign());
		fDocumentList.getComponent().setWidth(FIELD_WIDTH);
		fDocumentList.getComponent().setMultiline(true);
		fDocumentList.getComponent().setHeight("200px");
		fDocumentList.setReadWrite(false);
		row.appendChild(fDocumentList.getComponent());
		initDocumentList();
		
		
		row = rows.newRow();
		row.appendChild(new Label("Shipment Manifest").rightAlign());
		fManifest.setWidth(FIELD_WIDTH);
		row.appendChild(fManifest);
		initShipmentManifest();

		row = rows.newRow();
		row.appendChild(new Label("Message").rightAlign());
		fErrorMsg.getComponent().setWidth(FIELD_WIDTH);
		fErrorMsg.getComponent().setMultiline(true);
		fErrorMsg.setReadWrite(false);
		fErrorMsg.getComponent().setHeight("100px");

		row.appendChild(fErrorMsg.getComponent()); 
		
		appendChild(inputPanel);
		fDocumentNo.getComponent().addEventListener(Events.ON_OK, this);
		
		confirmPanel = new ConfirmPanel(true);
		row = rows.newRow();
		//row.appendChild(new Space());
		row.setSpans("2");
		row.appendChild(confirmPanel);
		
		confirmPanel.addButton(confirmPanel.createButton(ConfirmPanel.A_RESET));
		confirmPanel.addActionListener(this);
		

	}

	private void initShipmentManifest() {

		String sql = "SELECT mf.m_inout_manifest_id, mf.documentno " +
	             "FROM m_inout_manifest mf " +
	             "WHERE mf.processed = 'N' AND mf.ad_client_id = ? "  +
	             "ORDER BY mf.documentno ";
		
		
		for (KeyNamePair kn : DB.getKeyNamePairs(sql, false, Env.getAD_Client_ID(m_ctx))) {
			fManifest.addItem(kn);
		}		
	
	
	}

	private void initDocumentList() {
		for (int idx : m_table.getSelectedIndices()) {
			String docNo = (String) m_table.getValueAt(idx, WShipmentScannerForm.IDX_COL_SHIPMENTNO);
			IDColumn id = (IDColumn)m_table.getValueAt(idx, WShipmentScannerForm.IDX_COL_IDCOLUMN);
			m_scanned.add(id.getRecord_ID());
			m_list.append(docNo +  System.getProperty("line.separator"));
		}
		fDocumentList.setValue(m_list.toString());		
	}
	/**
	 * @param event
	 */
	public void onEvent(Event event) throws Exception {
		if (event.getTarget().getId().equals(ConfirmPanel.A_CANCEL)) {
			cancelled = true;
			detach();
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_RESET)) {
			fDocumentList.setValue("");
			m_list = new StringBuffer();
			m_scanned.clear();
			fErrorMsg.setValue("Scanned items cleared.");
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_OK)) {
			boolean resultOK = false;
			
			if (m_action == ManifestAction.UPDATE) {
				resultOK = update();
			}
			
			if  (resultOK) {
				detach();
			}
			
		}
		else if (Events.ON_FOCUS.equals(event.getName())){
		} else if (Events.ON_OK.equals(event.getName())) {
			if (event.getTarget() == fDocumentNo.getComponent()) {
				onDocumentScan();
			}
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_RESET)) {
		} else if (event.getName().equals(Events.ON_CHANGE)) {
		}
		
	}

	private boolean isScanValid(String docNo)
	{
		int idx = 0;
		IDColumn id = null;
		String shipNo = null;
		boolean found = false;
		for (int i = 0; i < m_table.getRowCount(); i++)
		{
			id = (IDColumn)m_table.getValueAt(i, WShipmentScannerForm.IDX_COL_IDCOLUMN);     
			shipNo  = (String) m_table.getValueAt(i, WShipmentScannerForm.IDX_COL_SHIPMENTNO);
			
			if (shipNo.equals(docNo)) {
				idx = i;
				found = true;
				break;
			}
		}
		if (!found) {
			fErrorMsg.setValue("Invalid Document=" +docNo);
			return false;
		}
		
		m_scanned.add(id.getRecord_ID());
		fErrorMsg.setValue("Success Document=" + docNo);
		return true;
	}
	
	/**
	 * 
	 */
	private void onDocumentScan() 
	{
		String docNo = fDocumentNo.getValue().toString().trim();
		
		if (isScanValid(docNo)) {
			m_list.insert(0, docNo + System.getProperty("line.separator"));
			fDocumentList.setValue(m_list.toString());
		}
		fDocumentNo.setValue("");
	}


	
	/**
	 * 
	 * @return boolean
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * 
	 * @return
	 */
	private boolean update()
	{
		String sql = "UPDATE m_inout_manifestline " +
	             "SET m_inout_manifest_id = ? " +
	             "WHERE " +
	             "m_inout_id = ?";		
		
		String trxName = Trx.createTrxName();
		PreparedStatement pstmt = DB.prepareStatement(sql, trxName);
		String error = "";
		
		if (fManifest.getSelectedItem() == null) {
			error = "No shipment manifest selected.\n";
		}
		if (m_scanned.size() == 0) {
			error = error + " No shipment document selected.";
		}
		
		if (error.length() > 0) {
			FDialog.warn(0,  error);
			return false;
		}
		KeyNamePair manifestPair = fManifest.getSelectedItem().toKeyNamePair();
		int manifest_ID = manifestPair.getKey();
		try {
			for (int inoutID : m_scanned) {
				pstmt.setInt(1, manifest_ID);
				pstmt.setInt(2, inoutID);
				int n = pstmt.executeUpdate();
				if  (n == 0) {
					MManifest manifest = new MManifest(m_ctx, manifest_ID, trxName);
					if (manifest != null) {
						manifest.getLines();
						MManifestLine manifestLine = new MManifestLine(manifest);
						manifestLine.setM_InOut_ID(inoutID);
						manifestLine.setDescription("Added through shipment scanner");
						manifestLine.setLine(manifest.getLastLineNo() + 10);
						manifestLine.setSeqNo(manifest.getLastSeqNo() + 10);
						manifestLine.saveEx();
					}
					
				}
				
			}
			DB.commit(true, trxName);
			pstmt.close();
		} catch (Exception e) {
			log.severe(e.getMessage());
		} finally {
			Trx trx = Trx.get(trxName, false);
			trx.close();
		}
		
		fErrorMsg.setValue("Update successful");
		return true;
	}
	
}
