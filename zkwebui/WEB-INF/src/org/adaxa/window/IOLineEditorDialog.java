package org.adaxa.window;


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.HashMap;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.VerticalBox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WStringEditor;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MProduct;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.zkoss.zk.ui.SuspendNotAllowedException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Separator;

/**
 * 
 * @author jobriant
 *
 */
public class IOLineEditorDialog extends Window implements EventListener{
	
	private static final String FIELD_WIDTH = "200px";
	private static final String FIELD_LABEL_WIDTH = "100px,200px";

	private static final String HEADER_TEXT_STYLE = "color: darkblue; font-size: 16px;";
	
	private static CLogger log = CLogger.getCLogger(IOLineEditorDialog.class);
	
	public enum Mode {ADD, REMOVE, EDIT}
	/**
	 * 
	 */
	private static final long serialVersionUID = -3852236029054284848L;
	private int max;
	private BigDecimal number;
	private boolean cancelled = false;
	private Tabbox tabbox;
	private NumberBox fQty;
	private Label lHeader;
	private Label status;
	
	private WStringEditor fProductName = new WStringEditor();
	private WStringEditor fPCode = new WStringEditor();
	private Combobox fLocator = new Combobox();
	private WStringEditor fSerial = new WStringEditor();
	private Textbox fErrorMsg = new Textbox();
	private Textbox fNotes = new Textbox();
	private Checkbox fLimitToProduct = new Checkbox();
	
	private boolean isCrossDock = false;
	private boolean isSerial = false;
	private boolean isQtyUpdatable = false;
	private int m_Locator_ID = 0;
	private int m_PackageLine_ID = 0;
	private String m_notes = null;
	private MProduct m_Product = null;
	private ConfirmPanel confirmPanel = null;
	private boolean isMovement=false;
	private MMovement movement=null;
	private MMovementLine line=null;
	Mode mode=null;
	//Key = Locator + QOH, Value = 
	private HashMap<String, String> mapProductQOH = new HashMap<String, String>();
	private HashMap<String, Integer> mapReceiptLine = new HashMap<String, Integer>();

	//Key = Locator + Product_ID, Value = movement qty
	private HashMap<String, BigDecimal> mapLocatorQty=new HashMap<String, BigDecimal>();
	
	public IOLineEditorDialog(String title, BigDecimal value, boolean isSerial, int maxValue, boolean isQtyUpdatable
			, Mode mode, MProduct product, MMovement movement, MMovementLine line, String notes)
	{
		this(title, value, isSerial, maxValue, isQtyUpdatable, mode, product, false, notes);
		isMovement=true;
		this.movement=movement;
		this.line=line;
		this.mode=mode;
		if(mode==Mode.REMOVE)
			fillLocatorRemoveMode();
		else
			fillLocator();
	}
	/**
	 * 
	 * @param title
	 * @param text
	 * @param isSerial
	 * @param maxValue
	 */
	public IOLineEditorDialog(String title, BigDecimal value, boolean isSerial, int maxValue, boolean isQtyUpdatable
			, Mode mode, MProduct product, boolean isCrossDock, String notes) {
		super();
		setTitle(title);
		this.isSerial = isSerial;
	
		this.m_Product = product;
		this.max = maxValue;
		this.number = value;
		this.isQtyUpdatable = isQtyUpdatable;
		this.isCrossDock = isCrossDock;
		this.m_notes = notes;
		init();
		switch (mode) {
		case ADD:
			lHeader.setText("ITEMS TO BE ADDED");
			break;
		case REMOVE:
			lHeader.setText("ITEMS TO BE REMOVED");
			break;
		case EDIT:
			lHeader.setText("ITEMS WILL BE UPDATED");
			break;
		}
		this.mode=mode;
		fillLocator();
	}
	
	private void fillLocatorRemoveMode()
	{
		fLocator.removeAllItems();
		mapLocatorQty.clear();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		StringBuffer sql = new StringBuffer();
		
			sql.append("SELECT l.Value, ml.movementqty FROM M_Locator l ");
			sql.append(" INNER JOIN M_MovementLine ml on l.M_Locator_ID=ml.M_LocatorTo_ID " );
				sql.append("WHERE ml.m_product_id = ?  AND ml.M_Movement_ID=? AND ml.movementqty >0 ");
				if(line!=null)
					sql.append( " AND ml.M_MovementLine_ID=? " );
				
		try {
			pstmt = DB.prepareStatement(sql.toString(), movement.get_TrxName());
			pstmt.setInt(1, m_Product.getM_Product_ID());
			pstmt.setInt(2, movement.getM_Movement_ID());
			if(line!=null)
				pstmt.setInt(3, line.getM_MovementLine_ID());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String locValue= rs.getString(1);
				BigDecimal qoh = rs.getBigDecimal(2);
				if(mapLocatorQty.get(locValue+""+m_Product.getM_Product_ID()) !=null)
				{
					mapLocatorQty.put(locValue+""+m_Product.getM_Product_ID(), mapLocatorQty.get(locValue+""+m_Product.getM_Product_ID()).add(qoh));
					continue;
				}
				else
					mapLocatorQty.put(locValue+""+m_Product.getM_Product_ID(), qoh);
				fLocator.appendItem(locValue);
			}
			if(mapLocatorQty.size() > 0)
			{
				fLocator.setSelectedIndex(0);
				BigDecimal qty=mapLocatorQty.get(fLocator.getValue()+""+m_Product.getM_Product_ID());
				fQty.setValue(qty);
				number=qty;
			}
			
		}
		catch (Exception e){
			log.severe(e.getMessage());
		}
		finally {
			DB.close(rs, pstmt);
		}
	
		
	}

	private void init() {
		setBorder("normal");
		
		VerticalBox vbox = new VerticalBox();
		appendChild(vbox);
	
		lHeader = new Label();
		lHeader.setText("  --  ");
		lHeader.setStyle(HEADER_TEXT_STYLE);
		vbox.appendChild(lHeader);
		
		Hbox hbox = null;
	
		if (isSerial) {
			hbox = new Hbox();
			hbox.appendChild(new Label("Serial").rightAlign());
			hbox.setWidths(FIELD_LABEL_WIDTH);
			hbox.appendChild(fSerial.getComponent());
			vbox.appendChild(hbox);
			fSerial.getComponent().setWidth(FIELD_WIDTH);
			fSerial.setReadWrite(isSerial);
		}
		
		hbox = new Hbox();
		if (isCrossDock) {
			hbox.appendChild(new Label("Package").rightAlign());
		} else {
			hbox.appendChild(new Label("Locator").rightAlign());
		}
		hbox.setWidths(FIELD_LABEL_WIDTH);
		hbox.appendChild(fLocator);
		vbox.appendChild(hbox);
		fLocator.setWidth(FIELD_WIDTH);
		fLocator.setReadonly(false);
		fLocator.addEventListener(Events.ON_OK, this);
		fLocator.addEventListener(Events.ON_FOCUS, this);
		fLocator.addEventListener(Events.ON_CHANGE, this);
		//fLocator.setHeight("100px");

		
		fQty = new NumberBox(false);
		hbox = new Hbox();
		hbox.appendChild(new Label("Qty").rightAlign());
		hbox.setWidths(FIELD_LABEL_WIDTH);
		hbox.appendChild(fQty);
		vbox.appendChild(hbox);
		fQty.setEnabled(isQtyUpdatable);
		fQty.setWidth(FIELD_WIDTH);
		fQty.setValue(number);
		fQty.getDecimalbox().setWidth("100px");
		DecimalFormat format = DisplayType.getNumberFormat(DisplayType.Quantity, AEnv.getLanguage(Env.getCtx()));
		fQty.setFormat(format);
		fQty.addEventListener(Events.ON_OK, this);
	
		
		if (isMovement) {
			fLimitToProduct.setChecked(false);
		} else {
			fLimitToProduct.setChecked(true);
		}
		hbox = new Hbox();
		hbox.appendChild(new Label(".").rightAlign());
		fLimitToProduct.setLabel("Only Locators Used by Product");
		hbox.setWidths(FIELD_LABEL_WIDTH);
		hbox.appendChild(fLimitToProduct);
		vbox.appendChild(hbox);
		fLimitToProduct.addActionListener(this);
		
		hbox = new Hbox();
		hbox.appendChild(new Label("Notes").rightAlign());
		hbox.setWidths(FIELD_LABEL_WIDTH);
		hbox.appendChild(fNotes);
		vbox.appendChild(hbox);
		fNotes.setWidth(FIELD_WIDTH);
		fNotes.setMultiline(true);
		fNotes.addEventListener(Events.ON_OK, this);
		fNotes.setHeight("100px");
		fNotes.setValue(m_notes);
		vbox.appendChild(new Separator());

		//hbox = new Hbox();
		Label msgLabel = new Label("System Message");
		msgLabel.setStyle(HEADER_TEXT_STYLE);
		vbox.appendChild(msgLabel);
//		hbox.setWidths(FIELD_LABEL_WIDTH);
		vbox.appendChild(fErrorMsg);
//		vbox.appendChild(hbox);
//		fErrorMsg.setWidth(FIELD_WIDTH);
		fErrorMsg.setReadonly(true);
		fErrorMsg.setMultiline(true);
		fErrorMsg.setWidth("100%");
		//fErrorMsg.addEventListener(Events.ON_OK, this);
		fErrorMsg.setHeight("50px");
		
		confirmPanel = new ConfirmPanel(true);
		vbox.appendChild(confirmPanel);
		confirmPanel.addButton(confirmPanel.createButton(ConfirmPanel.A_RESET));
		if (!isCrossDock) {
			confirmPanel.addButton(confirmPanel.createButton(ConfirmPanel.A_PROCESS));
			confirmPanel.getButton(ConfirmPanel.A_PROCESS).setTooltip("Set as Product Default Locator");
			confirmPanel.getButton(ConfirmPanel.A_PROCESS).setTooltiptext("Set as Product Default Locator");
		}
		confirmPanel.getButton(ConfirmPanel.A_RESET).setTooltip("Ignore changes.");
		confirmPanel.getButton(ConfirmPanel.A_RESET).setTooltiptext("Ignore changes.");
		confirmPanel.addActionListener(this);
		

		if (max > 0) {
			status = new Label();			
			appendChild(status);
			updateStatus(0);
			
			status.setStyle("margin-top:10px;");
			//textBox.addEventListener(Events.ON_CHANGE, this);
			fQty.addEventListener(Events.ON_CHANGE, this);
		}		

//		tabbox.addEventListener(Events.ON_SELECT, this);
	}

	@Override
	public void doModal() throws InterruptedException,
			SuspendNotAllowedException {

		super.doModal();
	}

	/**
	 * @param event
	 */
	public void onEvent(Event event) throws Exception {
		if (event.getTarget().getId().equals(ConfirmPanel.A_CANCEL)) {
			cancelled = true;
			detach();
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_PROCESS)) {
			if (m_Locator_ID == 0) {
				return;
			}
			if (m_Product == null) {
				return; // avoid NPE
			}
			m_Product.setM_Locator_ID(m_Locator_ID);
			m_Product.save();
			fErrorMsg.setText(m_Product.getValue() + " Default Locator set to " + fLocator.getText() + ". " );
			
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_OK)) {
			if (!isLocatorValid()) {
				return;
			}
			if (!isSerial) {
				number = fQty.getValue();
			}
			m_notes = fNotes.getValue();
			detach();
		} else if (event.getTarget().getId().equals(fLimitToProduct)) {
			if(mode==Mode.REMOVE)
				fillLocatorRemoveMode();
			else
				fillLocator();
		}
		else if (Events.ON_FOCUS.equals(event.getName())){
			if (event.getTarget() == fLocator) {
				fLocator.select();
			}
		}
		else if (Events.ON_CHANGE.equals(event.getName())){
			if (event.getTarget() == fLocator) {
				isLocatorValid();
				if(isMovement && mode==Mode.REMOVE)
				{
					String loc=fLocator.getValue();
					BigDecimal qty=mapLocatorQty.get(loc+""+m_Product.getM_Product_ID());
					fQty.setValue(qty);
					number=qty;
				}
				return;
			}
			
		} else if (Events.ON_OK.equals(event.getName())) {
			if (event.getTarget() == fSerial.getComponent()) {
				onSerial();
			} else if (event.getTarget() == fLocator) {
				onLocator();
			}
			else if (event.getTarget() == fQty.getDecimalbox()) {
				onQty();
			}
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_RESET)) {
			fQty.setValue(number);
			fNotes.setValue(m_notes);
		} else if (event.getName().equals(Events.ON_CHANGE)) {
		}
	}

	private void fillLocator()
	{
		mapProductQOH.clear();
		fLocator.removeAllItems();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		StringBuffer sql = new StringBuffer();
		
		if (isCrossDock) {
			//at the moment, Movement scanner is only for Cross Dock Stuff
			//if (isMovement) {
				sql.append("SELECT packageno, movementqty - qtyscanned AS availableqty, m_inoutline_id " 
						+  "  FROM m_inout_line_crossdock_v " 
						+  "WHERE isputaway = 'N' AND m_product_id = ?");
			//}
		} else {
			if (!fLimitToProduct.isChecked()) {
				sql.append(
						"SELECT l.value, SUM(s.qtyonhand) FROM m_locator l " 
					  + "  LEFT JOIN m_storage s ON l.m_locator_id = s.m_locator_id "
					  + "  GROUP BY l.value "
					  + " ORDER BY l.value");			
			}
			else {
				sql.append(
						"SELECT l.value, SUM(s.qtyonhand) FROM m_locator l " 
					  + "  INNER JOIN m_storage s ON l.m_locator_id = s.m_locator_id "
					  + "WHERE s.m_product_id = ?"								
					  + "  GROUP BY l.value "
					  + " ORDER BY l.value");			
			}
			
		}
		
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			if(fLimitToProduct.isChecked()) {
				pstmt.setInt(1, m_Product.getM_Product_ID());
			}
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String locValue= rs.getString(1);
				BigDecimal qoh = rs.getBigDecimal(2);
				String s = locValue + (!isMovement ? "(" + qoh +")" : "");
				mapProductQOH.put(s, locValue);
				fLocator.appendItem(s);
				if (isCrossDock) {
					mapReceiptLine.put(s, rs.getInt(3));
				}
			}
		}
		catch (Exception e){
			log.severe(e.getMessage());
		}
		finally {
			DB.close(rs, pstmt);
		}
	}
	
	/**
	 * 
	 */
	private boolean isLocatorValid() {
		String strLocator = (String) fLocator.getValue();
		String sql = "SELECT M_Locator_ID from M_Locator WHERE value = ? AND AD_Client_ID = ?"; 
		m_Locator_ID = DB.getSQLValue(null,sql, strLocator, Env.getAD_Client_ID(Env.getCtx()));
		if(!fLimitToProduct.isChecked())
		{
			if (m_Locator_ID <= 0) {
				fLocator.setFocus(true);
				fLocator.select();
				fErrorMsg.setValue("Unknown Locator");
				return false;
			}
			else {
				setM_Locator_ID(m_Locator_ID);
				return true;
			}
		}

		if (isCrossDock) {
			if (mapProductQOH.containsKey(strLocator)) {
				//TODO - check and send some message
				return true;
			}
			fLocator.setFocus(true);
			fLocator.select();
			fErrorMsg.setValue("Invalid PackageNo");
			return false;
		}
		
		if (mapProductQOH.containsKey(strLocator)) 
		{
			strLocator = mapProductQOH.get(strLocator);
		}
		m_Locator_ID = DB.getSQLValue(null,sql, strLocator, Env.getAD_Client_ID(Env.getCtx()));
		if (m_Locator_ID <= 0) {
			//fLocator.set highlight text
			fLocator.setFocus(true);
			fLocator.select();
			fErrorMsg.setValue("Unknown Locator");
			return false;
		}
		else {
			updateErrorMessage();
		}
		
		return true;
	}
	
	private void onQty() {
		if (!isSerial) {
			number = fQty.getValue();
		}
		if (!isLocatorValid()) {
			return;
		}
		detach();
	}

	private void onLocator() {
		
		if(isMovement)
			return;

		String strLocator = (String) fLocator.getValue();
		boolean isValid = true;
		if (mapProductQOH.containsKey(strLocator)) 
		{
			strLocator = mapProductQOH.get(strLocator);
		}
		String sql; 
		if (isCrossDock) 	{
			sql = "SELECT COUNT(*) from M_InOutLine_CrossDock_V WHERE PackageNo = ? AND M_Product_ID = ?"; 
			int cnt = DB.getSQLValue(null,sql, strLocator, m_Product.getM_Product_ID());
			if (cnt <= 0) {
				fErrorMsg.setValue("Unknown Package");
				isValid = false;
			}
				
		} else {
			sql = "SELECT M_Locator_ID from M_Locator WHERE value = ? AND AD_Client_ID = ?";
			m_Locator_ID = DB.getSQLValue(null,sql, strLocator, Env.getAD_Client_ID(Env.getCtx()));
			if (m_Locator_ID <= 0) {
				fErrorMsg.setValue("Unknown Locator");
				isValid = false;
			}			
		}

		if (isValid) {
			updateErrorMessage();
		}
		if (isQtyUpdatable)
			fQty.setFocus(true);
		else 
			detach();

		
	}

	/**
	 * 
	 */
	private void updateErrorMessage() {
		String msg = "";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			if (isCrossDock) {
				sql = "SELECT MovementQty FROM M_InOutLine_CrossDock_V WHERE PackageNo = ? AND M_Product_ID = ?"; 
				BigDecimal qty = DB.getSQLValueBD(null,sql, getLocator(), m_Product.getM_Product_ID());
				msg = "Original Qty Received = " + qty.toString() + System.getProperty("line.separator") ;
				sql = "SELECT 'USED IN ' || io.documentno || ' | ' || pl.qty " +
		             "FROM m_inout io  " +
		             "  INNER JOIN m_inoutline iol ON io.m_inout_id = iol.m_inout_id  " +
		             "  INNER JOIN m_packageline pl ON iol.m_inoutline_id = pl.m_inoutline_id  " +
		             "WHERE pl.receiptline_id = ?  ";
				pstmt = DB.prepareStatement(sql, null);
				pstmt.setInt(1, getReceiptLine_ID());
				rs = pstmt.executeQuery();
				while (rs.next()) {
					String res = rs.getString(1);
					msg = msg + System.getProperty("line.separator") + res;
				}
				
			} else {
				sql = "SELECT 'RESERVED IN ' || io.documentno || ' | ' || pl.qty " +
			          "FROM m_inout io " +
			          "  INNER JOIN m_inoutline iol ON io.m_inout_id = iol.m_inout_id " +
			          "  INNER JOIN m_packageline pl ON iol.m_inoutline_id = pl.m_inoutline_id " +
			             //"";
			          "WHERE io.docstatus NOT IN ('CO','CL','RE') " +
			          "AND   iol.m_product_id = ? " +
			          "AND   pl.m_locator_id = ? ";
				if (m_PackageLine_ID > 0) {
					sql = sql + " AND pl.m_packageline_id <> ?";
				}
				pstmt = DB.prepareStatement(sql, null);
				pstmt.setInt(1, m_Product.getM_Product_ID());
				pstmt.setInt(2, m_Locator_ID);
				if (m_PackageLine_ID > 0) {
					pstmt.setInt(3, m_PackageLine_ID);
				}
				rs = pstmt.executeQuery();
				while (rs.next()) {
					String res = rs.getString(1);
					msg = msg + System.getProperty("line.separator") + res;
				}
			}
			
		
		
		}
		catch (Exception e){
			log.severe(e.getMessage());
		}
		finally {
			DB.close(rs, pstmt);
		}
		fErrorMsg.setValue(msg.trim());
	}

	private void onSerial() {
		fLocator.setFocus(true);
		
	}

	private void updateStatus(int newLength) {
//		if (status != null && maxSize > 0) {
//			StringBuffer msg = new StringBuffer();
//			msg.append(newLength);
//			if (newLength == maxSize)
//				msg.append(" = ");
//			else if (newLength < maxSize)
//				msg.append(" < ");
//			else
//				msg.append(" > ");
//			msg.append(maxSize);
//			
//			status.setValue(msg.toString());	
//		}
		status.setValue("");
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public boolean isCancelled() {
		return cancelled;
	}
	
	/**
	 * 
	 * @return text
	 */
	public BigDecimal getQty() {
		return number;
	}

	public String getProductName() {
		return (String) fProductName.getValue();
	}

	public void setProductName(String productName) {
		this.fProductName.setValue(productName);
	}

	public String getPCode() {
		return (String) fPCode.getValue();
	}

	public void setPCode(String pCode) {
		this.fPCode.setValue(pCode);
	}

	public String getSerial() {
		return (String) fSerial.getValue();
	}

	public void setSerial(String serial) {
		this.fSerial.setValue(serial);
	}

	public int getReceiptLine_ID()
	{
		if (isCrossDock) {
			if (mapProductQOH.containsKey(fLocator.getValue())) 
			{
				return mapReceiptLine.get(fLocator.getValue());
			}
		}
			
		return 0;
	}
	public String getLocator() {
		if (mapProductQOH.containsKey(fLocator.getValue())) 
		{
			return mapProductQOH.get(fLocator.getValue());
		}
		return (String) fLocator.getValue();
	}

	public void setLocator(String locator) {
		this.fLocator.setValue(locator);
	}

	public int getM_Locator_ID() {
		return m_Locator_ID;
	}

	public void setM_Locator_ID(int m_Locator_ID) {
		this.m_Locator_ID = m_Locator_ID;
	}

	public void setM_PackageLine_ID(int M_PackageLine_ID) {
		this.m_PackageLine_ID = M_PackageLine_ID;
	}
	public String getNotes() {
		return m_notes;
	}
	public void setNotes(String m_notes) {
		this.m_notes = m_notes;
	}

}
