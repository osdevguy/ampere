package org.adaxa.form;

import java.awt.Color;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;

import org.adempiere.barcode.GS1_128;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.util.DateUtil;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Messagebox;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WStringEditor;
//import org.adempiere.webui.event.BarcodeEvent;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ITheme;
import org.adempiere.webui.window.FDialog;
import org.adempiere.webui.window.SimplePDFViewer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MPInstance;
import org.compiere.model.MPrintConfig;
import org.compiere.model.MProduct;
import org.compiere.model.MProduction;
import org.compiere.model.MQuery;
import org.compiere.model.MSysConfig;
import org.compiere.model.PrintInfo;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.East;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;
import org.zkoss.zkex.zul.West;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Vbox;



/**
 * 
 * @author jtrinidad
 *
 */
public class WProductionScannerForm extends ADForm
			implements EventListener, ValueChangeListener, WTableModelListener 

{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9097717314295551437L;
	private static CLogger			log = CLogger.getCLogger (WProductionScannerForm.class);
	
	private static final String				PRODUCTION_SCANNER_PROCESS = "PRODUCTION_SCANNER_PROCESS";

	
	//GUI
	/** Model table */
	private WListbox table = ListboxFactory.newDataTable();
	private Borderlayout formPanel = new Borderlayout();
	private Grid eastPanel = GridFactory.newGridLayout();
	private Borderlayout centerPanel = new Borderlayout();
	private ConfirmPanel formSouthPanel = new ConfirmPanel(true,true,false,false,false,false);

	private Button lScanHere = new Button();
	private WStringEditor fScanHere = new WStringEditor();

	private Button lUPC = new Button();
	private WStringEditor fUPC = new WStringEditor();
	private Button lPValue = new Button();
	private WStringEditor fPValue = new WStringEditor();
	private Button lQty = new Button();
	private WStringEditor fQty = new WStringEditor();
	private Button lBatch = new Button();
	private WStringEditor fBatch = new WStringEditor();
	private Button lDocumentNo = new Button();
	private WStringEditor fDocumentNo = new WStringEditor();
	private Button lUseByDate = new Button();
	private WStringEditor fUseByDate = new WStringEditor();
	private Button lMfgProductionLine = new Button();
	private WStringEditor fMfgProductionLine = new WStringEditor();
	private Button lSSCC = new Button();
	private WStringEditor fSSCC = new WStringEditor();
	private WStringEditor lMessage = new WStringEditor();
	
	private Button btnProcess = new Button();
	
	
	private ArrayList<KeyNamePair> productionList = new ArrayList<KeyNamePair>();
	private WEditor m_lastFocus;

	private int m_product_id = 0;
	private int m_production_id = 0;
	
	private BigDecimal m_Qty = null;
	private Timestamp m_ubd = null;
	private String m_MfgLine = null;
	private String m_batch = null;
	
	private boolean m_PrintEnable = false;
	private MPrintConfig m_PrintConfig = null;
	private MPrintFormat m_pf = null; 
	private ReportEngine m_re = null;
	private boolean jsBarcodeEvent = false; 
	
	@Override
	protected void initForm() {
		initZk();
		dynInit();
		printInit();
		focusOnField(fUPC);
		table.getModel().addTableModelListener(this);

	}

	private void initZk() {

		lScanHere.setLabel("Scan Here");

		lUPC.setLabel("UPC");
		lPValue.setLabel("SKU");
		lDocumentNo.setLabel("Work Order");
		lQty.setLabel("Qty");
		lBatch.setLabel("Batch");
		lUseByDate.setLabel("Use by Date");
		lMfgProductionLine.setLabel("Line Actual");
		lSSCC.setLabel("SSCC");

		lScanHere.setZclass("flat-button");
		lUPC.setZclass("flat-button");
		lPValue.setZclass("flat-button");
		lDocumentNo.setZclass("flat-button");
		lQty.setZclass("flat-button");
		lBatch.setZclass("flat-button");
		lUseByDate.setZclass("flat-button");
		lMfgProductionLine.setZclass("flat-button");
		lSSCC.setZclass("flat-button");
		
		
		lMessage.setReadWrite(false);
		
		formPanel.setWidth("99%");
		formPanel.setHeight("99%");
		formPanel.setStyle("border: none; position: absolute");

		fDocumentNo.setMandatory(true);

		formSouthPanel.getButton(ConfirmPanel.A_OK).setTooltiptext("Complete the Production Order.");
		formSouthPanel.getButton(ConfirmPanel.A_CANCEL).setTooltiptext("Clear all Entries");
		formSouthPanel.setZclass("touch-screen-friendly");
		
		formSouthPanel.getButton(ConfirmPanel.A_CANCEL).setImage(ITheme.IMAGE_FOLDER_ROOT + "Undo64.png");
		formSouthPanel.getButton(ConfirmPanel.A_OK).setImage(ITheme.IMAGE_FOLDER_ROOT + "Ok64.png");
		formSouthPanel.getButton(ConfirmPanel.A_REFRESH).setImage(ITheme.IMAGE_FOLDER_ROOT + "Refresh64.png");
		Row row = eastPanel.newRows().newRow();
		row.appendChild(lUPC);
		row.appendChild(fUPC.getComponent());

		row = new Row();
		eastPanel.getRows().appendChild(row);
		row.appendChild(lPValue);
		row.appendChild(fPValue.getComponent());
		
		row = new Row();
		eastPanel.getRows().appendChild(row);
		row.appendChild(lPValue);
		row.appendChild(fPValue.getComponent());
		
		row = new Row();
		eastPanel.getRows().appendChild(row);
		row.appendChild(lDocumentNo);
		row.appendChild(fDocumentNo.getComponent());

		row = new Row();
		eastPanel.getRows().appendChild(row);
		row.appendChild(lQty);
		row.appendChild(fQty.getComponent());

		row = new Row();
		eastPanel.getRows().appendChild(row);
		row.appendChild(lBatch);
		row.appendChild(fBatch.getComponent());
		
		row = new Row();
		eastPanel.getRows().appendChild(row);
		row.appendChild(lUseByDate);
		row.appendChild(fUseByDate.getComponent());
//		fUseByDate.getComponent().setAction("onClick : datepickerdialog.showdialogbox('#"
//				+ fUseByDate.getComponent().getId() + "', '#" + this.getId() + "', 'dd/mm/yyyy')");
		
		fUseByDate.setAsDatePicker(this.getId());
		
		row = new Row();
		eastPanel.getRows().appendChild(row);
		row.appendChild(lMfgProductionLine);
		row.appendChild(fMfgProductionLine.getComponent());

		row = new Row();
//		eastPanel.getRows().appendChild(row); hide for now
		row.appendChild(lSSCC);
		row.appendChild(fSSCC.getComponent());

		row = new Row();
		eastPanel.getRows().appendChild(row);
		row.appendChild(lMessage.getComponent());
		row.setSpans("2");
		lMessage.getComponent().setWidth("100%");
		lMessage.getComponent().setHeight("160px");
		lMessage.getComponent().setZclass("message-screen-info");
		lMessage.getComponent().setMultiline(true);


		Center tblPanel = new Center();
		tblPanel.appendChild(table);
		table.setWidth("100%");
		table.setHeight("100%");

		
		eastPanel.setWidth("500px");
		East eastForm = new East();
		eastForm.appendChild(eastPanel);
		
		centerPanel.appendChild(tblPanel); //LEFT CENTER
		centerPanel.appendChild(eastForm);  //RIGH CENTER

		
		
		North north = new North();
		north.setStyle("border: none");
		Grid msgPanel = GridFactory.newGridLayout();
		msgPanel.setZclass("touch-screen-friendly");
		msgPanel.setHeight("50px");
		msgPanel.setWidth("60%");
		row = msgPanel.newRows().newRow();;
		row.appendChild(lScanHere);
		row.appendChild(fScanHere.getComponent());
		
		north.appendChild(msgPanel);
		formPanel.appendChild(north); //TOP

		centerPanel.setZclass("touch-screen-friendly");

		centerPanel.setWidth("100%");
		centerPanel.setHeight("100%");
		
		Center center = new Center();
		center.appendChild(centerPanel);
		formPanel.appendChild(center); //CENTER
		
		South south = new South();
		south.setHeight("100px");
		south.appendChild(formSouthPanel);
		formPanel.appendChild(south);  //BOTTOM

		this.appendChild(formPanel);
		
		
		lScanHere.addEventListener(Events.ON_CLICK, this);
		lUPC.addEventListener(Events.ON_CLICK, this);
		lPValue.addEventListener(Events.ON_CLICK, this);
		lDocumentNo.addEventListener(Events.ON_CLICK, this);
		lQty.addEventListener(Events.ON_CLICK, this);
		lBatch.addEventListener(Events.ON_CLICK, this);
		lUseByDate.addEventListener(Events.ON_CLICK, this);
		lMfgProductionLine.addEventListener(Events.ON_CLICK, this);
		lSSCC.addEventListener(Events.ON_CLICK, this);
				
		fScanHere.getComponent().addEventListener(Events.ON_OK, this);
		fUPC.getComponent().addEventListener(Events.ON_OK, this);
		fPValue.getComponent().addEventListener(Events.ON_OK, this);
		fDocumentNo.getComponent().addEventListener(Events.ON_OK, this);
		fQty.getComponent().addEventListener(Events.ON_OK, this);
		fBatch.getComponent().addEventListener(Events.ON_OK, this);
		fUseByDate.getComponent().addEventListener(Events.ON_OK, this);
		fMfgProductionLine.getComponent().addEventListener(Events.ON_OK, this);
		fSSCC.getComponent().addEventListener(Events.ON_OK, this);

		formSouthPanel.addActionListener(this);
		table.addEventListener(Events.ON_SELECT, this);
		
		this.addEventListener(Events.ON_FOCUS, this);
//		this.addEventListener(BarcodeEvent.ON_BARCODE_EVENT, this);
		
		jsBarcodeEvent = MSysConfig.getBooleanValue("ENABLE_BARCODE_EVENT", false);

	}
	
	private void dynInit() {
		ColumnInfo[] layout = new ColumnInfo[] {
				new ColumnInfo(" ",                                         
						".", IDColumn.class, false, false, ""),
				new ColumnInfo("Work Order",
						".", String.class), //  1
				new ColumnInfo("SKU", ".",
						String.class), //2
				new ColumnInfo("Date", ".",
						Timestamp.class), //3
				new ColumnInfo("Line", ".",
						String.class), //4
				new ColumnInfo("Status", ".",
						String.class), //5
				new ColumnInfo("Qty", ".",
						Double.class), //  6
				}; 

		table.prepareTable(layout, "", "", false, "");

		loadProductionInProcess();
		
		//set column width
		WListItemRenderer renderer = (WListItemRenderer) table.getItemRenderer();
		ArrayList<ListHeader> headers = renderer.getHeaders();
		headers.get(0).setWidth("60px");
		headers.get(1).setWidth("30%");
		headers.get(2).setWidth("20%");
		headers.get(3).setWidth("20%");
		headers.get(4).setWidth("20%");
		
		
	}
	
	private void printInit()
	{
		
		m_PrintConfig = MPrintConfig.get(PRODUCTION_SCANNER_PROCESS);
		if (m_PrintConfig.getAD_PrintFormat_ID() == 0) {
			m_PrintEnable = false;
			return;
		}
		
		
		m_pf = new MPrintFormat(Env.getCtx(), m_PrintConfig.getAD_PrintFormat_ID(), null);
		
		m_PrintEnable = true;

	}

	private void loadProductionInProcess() {
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		int AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());
		
		String sql = "SELECT p.m_production_id, p.documentno, mp.value " +
				 ", round(pb.qtycompleted) || '/' || round(pb.targetqty)  as status , p.productionqty " +
				 ", mp.upc, p.datepromised, p.filling_line  " +
	             "FROM m_production p " +
	             "  INNER JOIN m_product mp ON p.m_product_id = mp.m_product_id " +
	             "  INNER JOIN m_production_batch pb ON pb.m_production_batch_id = p.m_production_batch_id " +
	             "WHERE p.AD_Client_ID = ? AND ? IN (p.AD_Org_ID, 0) AND pb.docstatus IN ('CO') AND p.docstatus IN ('DR', 'IN', 'IP')";
		
		if (m_product_id > 0) {
			sql = sql + " AND p.m_product_id = ? ";
		}
		if (m_production_id > 0) {
			sql = sql + " AND p.m_production_id = ? ";
		}
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			int i = 1;
			pstmt.setInt(i++, AD_Client_ID);
			pstmt.setInt(i++, AD_Org_ID);
			if (m_product_id > 0) {
				pstmt.setInt(i++, m_product_id);
			}
			if (m_production_id > 0) {
				pstmt.setInt(i++, m_production_id);
			}
			rs = pstmt.executeQuery();

			table.clearTable();
			int row = table.getItemCount();
			table.setRowCount(row);
			productionList.clear();

			while (rs.next()) {

				table.setRowCount(row + 1);
				table.setValueAt(new IDColumn(rs.getInt(1)), row, 0);
				table.setValueAt(rs.getString(2), row, 1);
				table.setValueAt(rs.getString(3), row, 2);
				table.setValueAt(rs.getString(4), row, 5);
				table.setValueAt(rs.getDouble(5), row, 6);
				//6 = UPC
				table.setValueAt(rs.getTimestamp(7), row, 3);
				table.setValueAt(rs.getString(8), row, 4);

				productionList
						.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));

				row++;
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, sql, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		table.repaint();
		this.invalidate();
	}

	
	@Override
	public void onEvent(Event event) throws Exception {
		if (Events.ON_CLICK.equals(event.getName())) {
			if (event.getTarget() == lScanHere) {
				focusOnField(fScanHere);
			} else if (event.getTarget() == lUPC) {
				focusOnField(fUPC);
			} else if (event.getTarget() == lPValue) {
				focusOnField(fPValue);
			} else if (event.getTarget() == lDocumentNo) {
				focusOnField(fDocumentNo);
			} else if (event.getTarget() == lQty) {
				focusOnField(fQty);
			} else if (event.getTarget() == lBatch) {
				focusOnField(fBatch);
			} else if (event.getTarget() == lUseByDate) {
				focusOnField(fUseByDate);
			} else if (event.getTarget() == lMfgProductionLine) {
				focusOnField(fMfgProductionLine);
			} else if (event.getTarget() == lSSCC) {
				focusOnField(fSSCC);
			} else if (event.getTarget() == formSouthPanel.getButton(ConfirmPanel.A_REFRESH)) {
				m_lastFocus = null;
				loadProductionInProcess();
			} else if (event.getTarget() == formSouthPanel.getButton(ConfirmPanel.A_CANCEL)) {
				m_lastFocus = null;
				reset();
				loadProductionInProcess();
			} else if (event.getTarget() == formSouthPanel.getButton(ConfirmPanel.A_OK)) {
				m_lastFocus = null;
				if (isValid()) {
					process();
				}
//			} else if (event.getTarget() == formSouthPanel.getButton(ConfirmPanel.A_OK)) {
//				//TODO: Restore menu
//				if (FDialog.ask(0, null, "Do you want to close the window?")) {
//					SessionManager.getAppDesktop().closeActiveWindow();
//				}
//				return;
			} else {
				m_lastFocus = null;
			}
		} else if (Events.ON_OK.equals(event.getName())) {
			if (event.getTarget() == fPValue.getComponent()) {
				isProductValid();
				loadProductionInProcess();
				focusOnField(fScanHere);
			}
			Textbox text =  (Textbox) event.getTarget();
			String scanStr = text.getValue();
			GS1_128 scan = GS1_128.parse(scanStr);
			if (scan != null) {
				text.setValue("");
				updateFields(scan);
			} else {
				if (!jsBarcodeEvent  || scanStr.length() >= 10) {
					fUPC.setValue(scanStr);
					text.setValue("");
					isProductValid();
					isProductionValid();
					loadProductionInProcess();
				}
			}
//			if (event.getTarget() == fPValue.getComponent() || event.getTarget() == fUPC.getComponent()) {
//			} else if (event.getTarget() == fDocumentNo.getComponent()) {
//				isProductionValid();
//				if (m_production_id > 0) {
//					//loadProductionInProcess();
//					//ListItem item = table.getItems().
//					for (ListItem item : table.getItems()) {
//						if (item.gev)
//					}
//							
//					table.setSelectedItem(item);
//				}
//			}
			focusOnField(fScanHere);

		} else if (Events.ON_SELECT.equals(event.getName())) {
			if (event.getTarget() == table) {
				m_production_id = table.getSelectedRowKey();
				String docNo = (String) table.getValueAt(table.getSelectedIndex(), 1);
				fDocumentNo.setValue(docNo);
				lMessage.setValue(docNo + " Manually selected from the list."); 
//				loadProductionInProcess();
				focusOnField(fScanHere);
			}
//		} else if (BarcodeEvent.ON_BARCODE_EVENT.equals(event.getName()))
//		{
//			String[] data = (String[]) event.getData();
//			GS1_128 scan = GS1_128.parse(data[0]);
//			if (scan != null) {
//				updateFields(scan);
//			}
//			focusOnField(fScanHere);
		}

	}



	private void focusOnField(WStringEditor fieldEditor) {
//		if (fieldEditor.isHasFocus() || fieldEditor == m_lastFocus) {
//			return;
//		}
		fieldEditor.setHasFocus(true);
		fieldEditor.getComponent().setFocus(true);
		fieldEditor.getComponent().select();
		m_lastFocus = fieldEditor;
	}

	private boolean isValid()
	{
//		if (!isProductValid()) {
//			return false;
//		}
		isProductValid();
		isProductionValid();
		
		try {
			m_ubd = new Timestamp(DisplayType.getDateFormat().parse(fUseByDate.getValue().toString()).getTime());
		} catch (Exception e) {
//			displayMessage(Messagebox.EXCLAMATION, 
//					"Invalid date format. Must comply to GS1-128 Specification - YYMMDD");
			m_ubd = null;
			return false;
		}
		try {
			m_Qty = new BigDecimal (fQty.getValue().toString());
		} catch (Exception e) {
			displayMessage(Messagebox.EXCLAMATION, 
					"Invalid qty entered.");
			return false;
		}
		StringBuffer sb = new StringBuffer();
		m_MfgLine = fMfgProductionLine.getValue().toString();
		m_batch = fBatch.getValue().toString();
		if (StringUtils.isBlank(m_MfgLine)) {
			sb.append("Filling line is empty.\n");
		}
		if (StringUtils.isBlank(m_batch)) {
			sb.append("Batch is empty.\n");
		}
		if (m_ubd == null) {
			sb.append("Use by Date is empty.\n");
		}
		
		if (sb.length() > 0) {
			displayMessage(Messagebox.EXCLAMATION, 
					sb.toString());
			return false;
		}
		return true;
	}

	private void reset()
	{
		fUPC.setValue(null);
		fPValue.setValue(null);
		fDocumentNo.setValue(null);
		fQty.setValue(null);
		fBatch.setValue(null);
		fUseByDate.setValue(null);
		fMfgProductionLine.setValue(null);
		fSSCC.setValue(null);
		displayMessage(Messagebox.INFORMATION, "Scanned items reset.");
		m_product_id = 0;
		m_production_id = 0;
		focusOnField(fScanHere);

		
	}
	
	private boolean isProductValid()
	{
		m_product_id = 0;
		lMessage.setValue(null);
		if (StringUtils.isNotBlank((String) fUPC.getValue())) {
			String sql = "SELECT m_product_id FROM m_product WHERE IsActive = 'Y' AND ad_client_id = ? AND   upc = ? ";
			m_product_id = DB.getSQLValue(null, sql, Env.getAD_Client_ID(Env.getCtx()), fUPC.getValue());
		} else if (StringUtils.isNotBlank((String) fPValue.getValue())) {
			String sql = "SELECT m_product_id FROM m_product WHERE IsActive = 'Y' AND ad_client_id = ? AND value ILIKE ? ";
			m_product_id = DB.getSQLValue(null, sql, Env.getAD_Client_ID(Env.getCtx()), fPValue.getValue());
		}
		if (m_product_id > 0) {
			MProduct p = MProduct.get(Env.getCtx(), m_product_id);
			fPValue.setValue(p.getValue());
			if (StringUtils.isBlank((String) fUseByDate.getValue())
					&& p.get_ValueAsInt("guaranteedays") > 0) {
				fUseByDate.setValue(DisplayType.getDateFormat().format(TimeUtil.addDays(null, p.get_ValueAsInt("guaranteedays") )));

			}
			
//			if (StringUtils.isNotBlank((String) fPValue.getValue())) {
//				if (!p.getValue().equals(fPValue.getValue())) {
//					displayMessage(Messagebox.EXCLAMATION, p.getValue() + "-"+ p.getUPC()  + " and " + fPValue.getValue() + " are in conflict. Please check UPC or PCode again.");
//					return false;
//				}
//			}
			displayMessage(Messagebox.INFORMATION, p.toDisplayString() + " Successfully scanned.");
			return true;
			
		}
		fUPC.setValue(null);
		fPValue.setValue(null);
//		displayMessage(Messagebox.ERROR, "WARNING: Invalid Product Scanned");
		return false;
	}
	
	private boolean isProductionValid()
	{
		m_production_id = 0;
		if (StringUtils.isNotBlank((String) fDocumentNo.getValue())) {
			String sql = "SELECT m_production_id FROM m_production WHERE IsActive = 'Y' AND processed ='N' AND ad_client_id = ? AND documentno LIKE ?";
			m_production_id = DB.getSQLValue(null, sql, Env.getAD_Client_ID(Env.getCtx()), fDocumentNo.getValue() + "%");
			if (m_production_id == 0) {
				displayMessage(Messagebox.ERROR, "WARNING: Invalid Production Order Scanned");
				return false;
			}
		}
		if (m_production_id > 0) {
			displayMessage(Messagebox.INFORMATION, fDocumentNo.getValue() + " - Successfully scanned.");
			return true;
		}
			
		return false;
	}
	
	private boolean process() {
		String trx = Trx.createTrxName();
		MProduction prod = new MProduction(Env.getCtx(), m_production_id, trx);
		
		if (prod.getM_Product_ID() != m_product_id) {
			displayMessage(Messagebox.ERROR, "No production Order with that Product Number.");
			return false;
		}
		try {
			if (prod.getProductionQty().compareTo(m_Qty) != 0) {
				prod.setProductionQty(m_Qty );
				prod.deleteLines(trx);
				prod.save();
				prod.createLines(false);
			}
			prod.set_ValueOfColumn("UBD", m_ubd);
			prod.set_ValueOfColumn("filling_line", m_MfgLine);
			prod.setMovementDate(new Timestamp(System.currentTimeMillis()));
			prod.setDescription(m_batch);
			prod.save();
			if (prod.processIt(MProduction.DOCACTION_Complete)) {
				DB.commit(true, trx);
				
				doPostProcess();
				
				reset();
				loadProductionInProcess();
				displayMessage(Messagebox.INFORMATION, prod.getDocumentNo() + " successfully completed.");
				return true;
			} else {
				displayMessage(Messagebox.ERROR, prod.getProcessMsg());
				return false;
			}
		
		} catch (Exception e) {
			try {
				DB.rollback(true, trx);
			} catch (SQLException e1) {
			}
			displayMessage(Messagebox.ERROR, e.getMessage());
			return false;
		}
	}

	@Override
	public void tableChanged(WTableModelEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void valueChange(ValueChangeEvent evt) {
		// TODO Auto-generated method stub
		displayMessage(Messagebox.ERROR, "Data changed");
		log.severe("Table data changed");
	}

	
	private void displayMessage(String category, String message)
	{
		if (Messagebox.ERROR.equals(category)) {
			lMessage.getComponent().setZclass("message-screen-error");
		} else if (Messagebox.EXCLAMATION.equals(category)) {
			lMessage.getComponent().setZclass("message-screen-warning");
		} else {
			lMessage.getComponent().setZclass("message-screen-info");
		}
		lMessage.setValue(message);
		
		
	}
	

	private void updateFields(GS1_128 scan) {
		if (scan.containID(GS1_128.GS1_128_GTIN)) {
			fUPC.setValue(scan.getGTIN());
		}
		if (scan.containID(GS1_128.GS1_128_BATCHNO)) {
			fBatch.setValue(scan.getBatchLot());
		}
		if (scan.containID(GS1_128.GS1_128_NO_UNITS_CONTAINED)) {
			fQty.setValue(scan.getNumberofUnits());
		}
		if (scan.containID(GS1_128.GS1_128_EXPIRATION_DATE)) {
			fUseByDate.setValue(DisplayType.getDateFormat().format(scan.getExpirationDate()));
		}
		if (scan.containID(GS1_128.GS1_128_ICC_MFG_LINE)) {
			fMfgProductionLine.setValue(scan.getICCMfgLine());
		}
		if (scan.containID(GS1_128.GS1_128_SSCC)) {
			fMfgProductionLine.setValue(scan.getSSCC());
		}
		if (scan.containID(GS1_128.GS1_128_ICC_PRODUCTION_DOCNO)) {
			fDocumentNo.setValue(scan.getICCProductionOrderNo());
		}
		isProductValid();
		isProductionValid();
		loadProductionInProcess();
		
	}

	private void doPostProcess()
	{
		print();
		
	}
	
	private void print()
	{
		if (!m_PrintEnable) {
			return;
		}

		PrintInfo info = new PrintInfo(PRODUCTION_SCANNER_PROCESS, MProduction.Table_ID, m_production_id);
		MQuery query = new MQuery(m_pf.getAD_Table_ID());
		query.addRestriction(MProduction.COLUMNNAME_M_Production_ID, MQuery.EQUAL, m_production_id);
		m_re = new ReportEngine(Env.getCtx(), m_pf, query, info);
		
				
		if (m_PrintConfig.isDirectPrint()) {
			ReportCtl.createOutput(m_re, false, m_pf.getPrinterName());
		}
		else {
			Clients.showBusy("Preparing PDF", false);
			try {
				Window win = new SimplePDFViewer(m_PrintConfig.getName(), new FileInputStream(m_re.getPDF()));
				SessionManager.getAppDesktop().showWindow(win, "center");
			} catch (Exception e) {
				log.severe(e.getMessage());
			}
		}
	}
}
