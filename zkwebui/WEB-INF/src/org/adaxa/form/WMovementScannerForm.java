package org.adaxa.form;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;

import org.adaxa.window.WMovementScannerWindow;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WStringEditor;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.window.FDialog;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MMovement;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;

public class WMovementScannerForm extends ADForm implements EventListener, ValueChangeListener
{

	/**
	 * 
	 */
	private static final long		serialVersionUID	= -4975904775238855353L;
	/** Logger */
	private static CLogger			log					= CLogger.getCLogger(WMovementScannerForm.class);

	/** Window No */
	private int						m_WindowNo			= 0;

	private ArrayList<KeyNamePair>	movementList		= new ArrayList<KeyNamePair>();

	// GUI
	/** Model table */
	private WListbox				table				= ListboxFactory.newDataTable();
	private Borderlayout			selPanel			= new Borderlayout();
	private Grid					selNorthPanel		= GridFactory.newGridLayout();
	private ConfirmPanel			selSouthPanel		= new ConfirmPanel(false, true, false, false, false, false);

	private Label					lDocumentNo			= new Label();
	private WStringEditor			fDocumentNo			= new WStringEditor();
	private Label					lBPartner			= new Label();
	private WSearchEditor			fBPartner			= null;

	public WMovementScannerForm()
	{
		super();
	}

	@Override
	protected void initForm()
	{
		initZk();
		dynInit();
	}

	private void initZk()
	{
		lDocumentNo.setText(Msg.translate(Env.getCtx(), "DocumentNo"));

		selPanel.setWidth("99%");
		selPanel.setHeight("95%");
		selPanel.setStyle("border: none; position: absolute");

		MLookup orgL = MLookupFactory.get(Env.getCtx(), m_WindowNo, 0, 2893, DisplayType.TableDir);
		fBPartner = new WSearchEditor("C_BPartner_ID", true, false, true, orgL);
		lBPartner.setText(Msg.translate(Env.getCtx(), "C_BPartner_ID"));

		fBPartner.addValueChangeListener(this);

		Row row = selNorthPanel.newRows().newRow();
		row.appendChild(lBPartner.rightAlign());
		row.appendChild(fBPartner.getComponent());
		row.appendChild(lDocumentNo.rightAlign());
		row.appendChild(fDocumentNo.getComponent());

		North north = new North();
		north.setStyle("border: none");
		north.appendChild(selNorthPanel);
		selPanel.appendChild(north);
		Center center = new Center();
		// center.setStyle("border: none");
		Panel p = new Panel();
		p.appendChild(table);
		table.setWidth("100%");
		table.setHeight("100%");
		center.appendChild(p);
		center.setFlex(true);
		selPanel.appendChild(center);

		selSouthPanel.addActionListener(this);

		South south = new South();
		south.appendChild(selSouthPanel);
		south.setHeight("34px");
		selPanel.appendChild(south);

		this.appendChild(selPanel);
		fDocumentNo.getComponent().addEventListener(Events.ON_OK, this);
	}

	private void dynInit()
	{
		ColumnInfo[] layout = new ColumnInfo[] {
				 new ColumnInfo(" ", ".", IDColumn.class, false, false, ""),
				new ColumnInfo(Msg.translate(Env.getCtx(), "Document No"), ".", String.class), //1
				new ColumnInfo(Msg.translate(Env.getCtx(), "Description"), ".", String.class), //2
				new ColumnInfo(Msg.translate(Env.getCtx(), "DocStatus"), ".", String.class) }; //3

		table.prepareTable(layout, "", "", true, "");

		// table.addEventListener(Events.ON_SELECT, this);
		loadMovement();
	}

	private void loadMovement()
	{

		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());

		String sql = "select mm.M_Movement_ID , mm.documentno, mm.description, mm.docstatus  from M_Movement mm "
				+ "where mm.docstatus IN ('DR', 'IP') and mm.AD_Client_ID=?";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, AD_Client_ID);

			rs = pstmt.executeQuery();

			table.clearTable();
			int row = table.getItemCount();
			table.setRowCount(row);
			movementList.clear();

			while (rs.next())
			{

				table.setRowCount(row + 1);
				 table.setValueAt(new IDColumn(rs.getInt(1)), row, 0);
				table.setValueAt(rs.getString(2), row, 1);  //documentno
				table.setValueAt(rs.getString(3), row, 2); //description
				String status = rs.getString(4);  //status
				if ("IP".equals(status))
					table.setValueAt("In Progress", row, 3);
				else if ("DR".equals(status))
					table.setValueAt("Drafted", row, 3);
				else
					table.setValueAt(status, row, 3);

				movementList.add(new KeyNamePair(rs.getInt(1), rs.getString(2)));

				row++;
			}

		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		table.repaint();
		this.invalidate();
	}

	@Override
	public void onEvent(Event event) throws Exception
	{
		if (Events.ON_CLICK.equals(event.getName()))
		{
			if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_REFRESH))
			{
				loadMovement();
			}
			else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_OK))
			{
				SessionManager.getAppDesktop().closeActiveWindow();
				return;
			}
		}
		else if (event.getTarget() == fDocumentNo.getComponent() && Events.ON_OK.equals(event.getName()))
		{
			KeyNamePair selection = null;
			String movementNo = (String) fDocumentNo.getValue();
			if (movementNo == null || "".equals(movementNo.trim()))
				return;
			movementNo = movementNo.trim();
			for (int i = 0; i < movementList.size(); i++)
			{
				KeyNamePair pair = movementList.get(i);
				if (movementNo.equals(pair.getName()))
				{
					selection = pair;
					break;
				}
			}

			if (selection == null)
			{
				FDialog.error(m_WindowNo, "Movement " + movementNo + " not available to confirm");
				return;
			}
			int M_Movement_ID = selection.getKey();
			MMovement inout = new MMovement(Env.getCtx(), M_Movement_ID, null);

			WMovementScannerWindow wps = new WMovementScannerWindow(m_WindowNo, inout);
			wps.setVisible(true);
			AEnv.showWindow(wps);
			// Reload shipments
			loadMovement();
			fDocumentNo.setValue("");
			fDocumentNo.getComponent().focus();
		}
		else
		{
			super.onEvent(event);
		}
	}

	public void valueChange(ValueChangeEvent evt)
	{
		loadMovement();
	}
}
