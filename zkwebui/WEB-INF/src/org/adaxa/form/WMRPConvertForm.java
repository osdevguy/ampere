package org.adaxa.form;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;

import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WDateEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.window.FDialog;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MLookupInfo;
import org.compiere.model.MProduction;
import org.compiere.model.MProductionBatch;
import org.compiere.model.MQuery;
import org.compiere.model.MTable;
import org.compiere.model.PrintInfo;
import org.compiere.print.MPrintFormat;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkforge.keylistener.Keylistener;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Listcell;

public class WMRPConvertForm extends ADForm implements EventListener,
		ValueChangeListener, WTableModelListener {

	private static final int COL_TARGET_QTY = 8;
	private static final int COL_ISREADONLY = 9;
	private static final int GRID_PAGE_SIZE = 12;
	/**
	 * 
	 */
	private static final long serialVersionUID = -4975904775238855353L;
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(WMRPConvertForm.class);

	/**	Window No					*/
	private int m_WindowNo = 53127;

	private ArrayList<KeyNamePair> documentList = new ArrayList<KeyNamePair>();

	private enum PhantomFilter { 
		 ALL, YES, NO
		}
	//GUI
	/** Model table */
	private WListbox table = ListboxFactory.newDataTable();
	private Borderlayout selPanel = new Borderlayout();
	private Grid selNorthPanel = GridFactory.newGridLayout();
	private ConfirmPanel selSouthPanel = new ConfirmPanel(false,true,false,false,false,false);

	private Label     lPhantom = new Label();
	private Listbox  cmbPhantom =  ListboxFactory.newDropdownListbox();
	private Label lDocumentTypeTo = new Label();
	private WTableDirEditor fDocumentTypeTo = null;
	private Label lDocumentType = new Label();
	private WTableDirEditor fDocumentType = null;
	private StringBuffer statusMessage = new StringBuffer();
	private Label dateFromLabel = new Label();
	private Label dateToLabel = new Label();
	private WDateEditor dateFrom = new WDateEditor("DateFrom", false, false, true, "DateFrom");
	private WDateEditor dateTo = new WDateEditor("DateTo", false, false, true, "DateTo");


	private Label     lPCat = new Label("Product Category");
	private WTableDirEditor  fPCategory = null;
	private Label lProduct = new Label("Product");
	private WSearchEditor fProduct;
	private Keylistener keyListener;
	private int currentRow;
	
	public WMRPConvertForm() {
		super();
	}

	@Override
	protected void initForm() {
		initZk();
		dynInit();
	}

	private void initZk() {

		selPanel.setWidth("99%");
		selPanel.setHeight("98%");
		selPanel.setStyle("border: none; position: absolute");
		
		dateFromLabel.setText(Msg.translate(Env.getCtx(), "DateFrom"));
		dateToLabel.setText(Msg.translate(Env.getCtx(), "DateTo"));
		
		MLookupInfo plannedOrderInfo = MLookupFactory.getLookupInfo (Env.getCtx(), m_WindowNo, 78218, DisplayType.TableDir,
				Env.getLanguage(Env.getCtx()), "C_DocType_ID", 0, false
				, "C_DocType.DocBaseType IN ('MPO', 'MOP') AND C_DocType.IsSOTrx='N'");
		MLookup lookupPO = new MLookup(plannedOrderInfo, 0);
		MLookup lookupMO = MLookupFactory.get(Env.getCtx(), m_WindowNo, 0, 78218,
				DisplayType.TableDir);
		fDocumentType = new WTableDirEditor("C_DocType_ID", true, false, true,
				lookupPO);
		
		fDocumentTypeTo = new WTableDirEditor("C_DocType_ID", true, false, true,
				lookupMO);
		if (lookupPO.size() == 1) {
			KeyNamePair ppo = (KeyNamePair) lookupPO.getElementAt(1);
			fDocumentType.setValue(ppo.getKey());
		}
		if (lookupMO.size() == 1) {
			KeyNamePair mpo = (KeyNamePair) lookupMO.getElementAt(1);
			fDocumentTypeTo.setValue(mpo.getKey());
		}
		lDocumentType.setText("Doc Type Filter");
		lDocumentTypeTo.setText("To Doc Type");
		//fDocumentType.addValueChangeListener(this);
		//fDocumentTypeTo.addValueChangeListener(this);

		MLookup pL = MLookupFactory.get (Env.getCtx(), m_WindowNo, 0, 78208, DisplayType.Search);		
		fProduct = new WSearchEditor ("M_Product_ID", true, false, true, pL);
		fProduct.addValueChangeListener(this);
		
		MLookup pcL = MLookupFactory.get (Env.getCtx(), m_WindowNo, 0, 2012, DisplayType.TableDir);
		fPCategory = new WTableDirEditor("M_Product_Category_ID", true, false, true, pcL);
		fPCategory.addValueChangeListener(this);
		
		Row row = selNorthPanel.newRows().newRow();
		lPhantom.setText(Msg.translate(Env.getCtx(), "IsPhantom"));
		row.appendChild(lPhantom.rightAlign());
		row.appendChild(cmbPhantom);
		

		row = new Row();
		selNorthPanel.getRows().appendChild(row);
		row.appendChild(dateFromLabel.rightAlign());		
		row.appendChild(dateFrom.getComponent());
		row.appendChild(dateToLabel.rightAlign());
		row.appendChild(dateTo.getComponent());
		
		row = new Row();
		selNorthPanel.getRows().appendChild(row);
		row.appendChild(lPCat.rightAlign());
		row.appendChild(fPCategory.getComponent());
		row.appendChild(lProduct.rightAlign());
		row.appendChild(fProduct.getComponent());

		row = new Row();
		selNorthPanel.getRows().appendChild(row);
		row.appendChild(lDocumentType.rightAlign());
		row.appendChild(fDocumentType.getComponent());
		row.appendChild(lDocumentTypeTo.rightAlign());
		row.appendChild(fDocumentTypeTo.getComponent());
		
		North north = new North();
		north.setStyle("border: none");
		north.appendChild(selNorthPanel);
		selPanel.appendChild(north);
		Center center = new Center();
		//center.setStyle("border: none");
		Panel p = new Panel();
		p.appendChild(table);
		table.setWidth("100%");
		table.setHeight("100%");
		center.appendChild(p);
		center.setFlex(true);
		selPanel.appendChild(center);

		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(ConfirmPanel.A_DELETE));
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(ConfirmPanel.A_ZOOM));
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(ConfirmPanel.A_PRINT));
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(ConfirmPanel.A_PROCESS));
		selSouthPanel.addActionListener(this);

		selSouthPanel.getButton(ConfirmPanel.A_PROCESS).setTooltiptext("Convert Document");
		South south = new South();
		south.setHeight("35px");
		south.appendChild(selSouthPanel);
		selPanel.appendChild(south);

		this.appendChild(selPanel);

	}

	private void dynInit() {
		ColumnInfo[] layout = new ColumnInfo[] {
				new ColumnInfo(" ",                                         
						".", IDColumn.class, false, false, ""),
				new ColumnInfo(Msg.translate(Env.getCtx(), "IsPhantom"), ".",
						Boolean.class), //1
				new ColumnInfo(Msg.translate(Env.getCtx(), "MovementDate"), ".",
						String.class), //2
				new ColumnInfo(Msg.translate(Env.getCtx(), "C_DocType_ID"),
						".", String.class), //  3
				new ColumnInfo(Msg.translate(Env.getCtx(), "DocumentNo"), ".",
						String.class), //4
				new ColumnInfo("Product Category", ".",
						String.class), //  5
				new ColumnInfo("PartNo", ".",
						String.class), //  6
				new ColumnInfo("Part Name", ".",
						String.class), //  7
				new ColumnInfo("Target Qty", ".",
						BigDecimal.class, false, true, ""), //8
				new ColumnInfo("ReadOnly", ".",
						Boolean.class) }; //9

		table.prepareTable(layout, "", "", true, "");
		table.setSclass("scannerlist");

		loadDocuments();
		
		//set column width
		WListItemRenderer renderer = (WListItemRenderer) table.getItemRenderer();
		ArrayList<ListHeader> headers = renderer.getHeaders();
		headers.get(1).setWidth("50px");
		headers.get(2).setWidth("60px");
		headers.get(3).setWidth("80px");
		headers.get(4).setWidth("80px");
		headers.get(5).setWidth("80px");
		headers.get(6).setWidth("80px");
		headers.get(7).setWidth("150px");
		headers.get(8).setWidth("70px");
		headers.get(9).setWidth("60px");

		renderer.addEventListener(this);
//		table.addEventListener(Events.ON_CHANGE, this);
//		WListItemRenderer wrl=(WListItemRenderer)table.getItemRenderer();
//		wrl.addEventListener(this);
		
		table.getModel().addTableModelListener(this);
		table.setMold("paging");
		table.setPageSize(GRID_PAGE_SIZE);
		if (keyListener != null)
    		keyListener.detach();
    	keyListener = new Keylistener();
    	this.appendChild(keyListener);
    	keyListener.setCtrlKeys("#up#down");
    	keyListener.addEventListener(Events.ON_CTRL_KEY, this);
    	keyListener.setAutoBlur(false);
    	

		cmbPhantom.addItem(new KeyNamePair(PhantomFilter.ALL.ordinal(), "*"));
		cmbPhantom.addItem(new KeyNamePair(PhantomFilter.YES.ordinal(), "Yes"));
		cmbPhantom.addItem(new KeyNamePair(PhantomFilter.NO.ordinal(), "No"));
		cmbPhantom.setSelectedIndex(0);
		cmbPhantom.addActionListener(this);
		
		
	}

	private void loadDocuments() {

		updateStatus();
		
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		int C_DocType_ID = 0;
		int M_Product_Category_ID = 0;
		int M_Product_ID = 0;
		
		if (fDocumentType.getValue() != null) {
			C_DocType_ID = (Integer) fDocumentType.getValue();
		}
		if (fPCategory.getValue() != null) {
			M_Product_Category_ID = (Integer) fPCategory.getValue();
		}
		if (fProduct.getValue() != null) {
			M_Product_ID = (Integer) fProduct.getValue();
			M_Product_Category_ID = 0;  // override category
		}
		
		Timestamp from = dateFrom.getValue()!=null?(Timestamp)dateFrom.getValue():null;
		Timestamp to = dateTo.getValue()!=null?(Timestamp)dateTo.getValue():null;
		
		String sql = "SELECT b.m_production_batch_id \n" +
				 ", p.isphantom \n" +
				 ", to_char(b.movementdate, 'dd/MM/yyyy') AS movementdate \n" +
				 ", dt.name, b.documentno, pc.name, p.value, p.name, b.targetqty \n" +
				 ", CASE WHEN dt.docbasetype = 'MOP' THEN 'Y' ELSE 'N' END AS readonly \n" +
	             "FROM m_production_batch b \n" +
	             "  INNER JOIN m_product p ON p.m_product_id = b.m_product_id \n" +
	             "  INNER JOIN c_doctype dt ON b.c_doctype_id = dt.c_doctype_id \n" +
	             "  INNER JOIN m_product_category pc ON pc.m_product_category_id = p.m_product_category_id \n" +	             
	             "WHERE \n " +
	             "(SELECT COUNT(*) FROM M_Production pp WHERE pp.m_production_batch_id = b.m_production_batch_id AND pp.processed = 'Y') = 0 AND \n" +
	             " b.ad_client_id = ?";
		
		if (C_DocType_ID > 0) {
			sql += " and  b.C_DocType_ID=?";
		}
		if (M_Product_ID > 0) {
			sql += " and  b.M_Product_ID=?";
		}
		if (M_Product_Category_ID > 0) {
			sql += " and  p.M_Product_Category_ID=?";
		}
		if (cmbPhantom.getSelectedIndex() > 0) {
			KeyNamePair knPair = cmbPhantom.getSelectedItem().toKeyNamePair();
			if (knPair.getKey() ==  PhantomFilter.YES.ordinal()) {
				sql +=  " and p.isphantom='Y'";
			} else {
				sql +=  " and p.isphantom='N'";
			}
		}
		
		if (from != null) {
			sql += " and CAST(b.movementdate as DATE) >= ?";
		}
		if (to != null) {
			sql += " and CAST(b.movementdate as DATE) <= ?";
		}
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			int i = 1;
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(i++, AD_Client_ID);
			if (C_DocType_ID > 0) {
				pstmt.setInt(i++, C_DocType_ID);
			}
			if (M_Product_ID > 0) {
				pstmt.setInt(i++, M_Product_ID);
			}
			if (M_Product_Category_ID > 0) {
				pstmt.setInt(i++, M_Product_Category_ID);
			}
			if (from != null) {
				pstmt.setTimestamp(i++, from);
			}
			if (to != null) {
				pstmt.setTimestamp(i++, to);
			}
			rs = pstmt.executeQuery();

			table.clearTable();
			int row = table.getItemCount();
			table.setRowCount(row);
			documentList.clear();

			while (rs.next()) {

				table.setRowCount(row + 1);
				table.setValueAt(new IDColumn(rs.getInt(1)), row, 0);
				table.setValueAt(rs.getString(2).equals("Y"), row, 1);
				table.setValueAt(rs.getString(3), row, 2);
				table.setValueAt(rs.getString(4), row, 3);
				table.setValueAt(rs.getString(5), row, 4);
				table.setValueAt(rs.getString(6), row, 5);
				table.setValueAt(rs.getString(7), row, 6);
				table.setValueAt(rs.getString(8), row, 7);
				table.setValueAt(rs.getBigDecimal(9), row, 8);
				boolean readonly = rs.getString(10).equals("Y");
				table.setValueAt(readonly, row, 9);
//				if (readonly) {
//					Listcell listcell = (Listcell) table.getItemAtIndex(row).getChildren().get(COL_TARGET_QTY);
//					//listcell.focus();
//					NumberBox box2 =((NumberBox)listcell.getChildren().get(0));
//					if(box2 != null ) {
//						box2.getDecimalbox().setReadonly(true);
//					}
//				}
				row++;
				
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, sql, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		table.repaint();
		this.invalidate();
		
		//clear message 
		statusMessage = new StringBuffer(); 
	}

	@Override
	public void onEvent(Event event) throws Exception { 

		
		if (Events.ON_CLICK.equals(event.getName())) {
			if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_REFRESH)) {
				loadDocuments();
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_PROCESS)) {
				if (convertDocuments()) {
					loadDocuments();
				}
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_DELETE)) {
				if (deleteSelectedDocument()) {
					loadDocuments();
				}
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_PRINT)) {
				printProduction();
				return;
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_ZOOM)) {
				zoom();
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_OK)) {
				if (hasSelections()) {
					return;  //avoid user from accidentally closing the window
				}
				SessionManager.getAppDesktop().closeActiveWindow();
				return;
			} 
		} else if (Events.ON_FOCUS.equals(event.getName())) {
			if (event.getTarget() instanceof Decimalbox) {
				WListItemRenderer renderer = (WListItemRenderer) table.getItemRenderer();
				currentRow = renderer.getRowPosition(event.getTarget());
				Listcell listcell = (Listcell) table.getItemAtIndex(currentRow).getChildren().get(COL_TARGET_QTY);
				//listcell.focus();
				Boolean readonly = (Boolean) table.getValueAt(currentRow, COL_ISREADONLY);
				if (readonly) {
					NumberBox box2 =((NumberBox)listcell.getChildren().get(0));
					if(box2 != null ) {
						box2.getDecimalbox().setReadonly(true);
					}
				}				
				return;
			}
		} else if (event.getTarget() == fDocumentType.getComponent()
				&& Events.ON_OK.equals(event.getName())) {
			loadDocuments();
		} else if (event.getTarget() == cmbPhantom) {
			loadDocuments();
		} else if(event.getName().equals(Events.ON_CTRL_KEY)) {
    		KeyEvent keyEvent = (KeyEvent) event;
    		int code=keyEvent.getKeyCode();
    		int page = KeyEvent.PAGE_DOWN;
    		//ListCell cell = (ListCell) event.getTarget();
			if(code == KeyEvent.DOWN) {
				currentRow+=1;
				if  (currentRow >= table.getRowCount()) {
					currentRow = table.getRowCount() - 1;
				}
			} 	else if(code == KeyEvent.UP) {
				currentRow-=1;
				page = KeyEvent.PAGE_UP;
				if (currentRow < 0) {
					currentRow = 0;
				}
			} else {
				return;
			}
			
			if(currentRow < 0 || currentRow > table.getRowCount() 
					|| currentRow >= GRID_PAGE_SIZE * (table.getActivePage() + 1)) {
				if (currentRow < 0) {
					currentRow = 0;
				} else {
					currentRow--;
				}
				return;
			}
//			if (currentRow % 8 == 0 ) {
////				Component c = (Component) selPanel.getCenter().getChildren().get(0);
////				KeyEvent e = new KeyEvent(Events.ON_CTRL_KEY, c, page, false, false, false);
////				Events.sendEvent(e);
//				table.getModel().updateComponent(currentRow);
//				
//				
//			}
			
			//table.
			Listcell listcell = (Listcell) table.getItemAtIndex(currentRow).getChildren().get(COL_TARGET_QTY);
			//listcell.focus();
			NumberBox box2 =((NumberBox)listcell.getChildren().get(0));
			if(box2 != null ) {
				box2.focus();
				Boolean readonly = (Boolean) table.getValueAt(currentRow, COL_ISREADONLY);
				if (readonly) {
					box2.getDecimalbox().setReadonly(true);
				} else {
					box2.getDecimalbox().select();
				}
			}
		} else {
			super.onEvent(event);
		}
	}

	private void updateStatus()
	{
		//TODO - update status bar to display message
		if (statusMessage.length() > 0) {
			
			FDialog.info(m_WindowNo, null, statusMessage.toString());
		}

	}
	
	public void valueChange(ValueChangeEvent evt) {
		if (fProduct.equals(evt.getSource())) { //fix timing event problem
			fProduct.setValue(evt.getNewValue());
		}
		loadDocuments();

	}
	
	private boolean hasSelections()
	{
		if (table.getSelectedCount() > 0) {
			return true;
		}
		return false;
	}
	
	private boolean deleteSelectedDocument() {
		if (table.getSelectedCount() == 0) {
			return false;
		}
		if (!FDialog.ask(m_WindowNo, null, table.getSelectedCount() + " Record(s) will be deleted?")) {
			return false;
		}
		String trx = Trx.createTrxName();
		try {
			
			for 
			(int i = 0;  i < table.getRowCount(); i++)
			{
				IDColumn id = (IDColumn)table.getValueAt(i, 0);     //  ID in column 0
				if (id != null && id.isSelected()) {
					int batch_id = id.getRecord_ID();
					MProductionBatch batch = new MProductionBatch(Env.getCtx(), batch_id, trx);
					batch.delete(false);
				}
				DB.commit(true, trx);
			}
		} catch (Exception e) {
			log.severe(e.getMessage());
			return false;
		}
		return true;	
	}
	
	private boolean convertDocuments() {
		StringBuffer msg = new StringBuffer();
		String trx = Trx.createTrxName();
		String status = "";
		int n = 0;
		int deleted = 0;
		boolean success = false;
		
		if ( fDocumentTypeTo.getValue() == null) {
			FDialog.error(m_WindowNo, "Please select 'To Doc Type'.");
			return false;
		}
		if (table.getSelectedCount() == 0) {
			return false;
		}
		String ask = table.getSelectedCount() + " Document(s) will be converted?";
		ask += "\nPlease note Planned Production set to 0 will be deleted automatically.";
		if (!FDialog.ask(m_WindowNo, null, ask)) {
			return false;
		}
		
		int newDocType_ID = (Integer) fDocumentTypeTo.getValue();
		try {
			for (int i = 0;  i < table.getRowCount(); i++)
			{
				IDColumn id = (IDColumn)table.getValueAt(i, 0);     //  ID in column 0
				//	log.fine( "Row=" + i + " - " + id);
				if (id != null) {
					int batch_id = id.getRecord_ID();
					MProductionBatch batch = new MProductionBatch(Env.getCtx(), batch_id, trx);
					if (batch.getC_DocType_ID() == newDocType_ID) {
						continue;  
					}
					Boolean readonly = (Boolean) table.getValueAt(i, COL_ISREADONLY);
					if (!readonly && batch.getTargetQty().signum() == 0) {
						batch.delete(false);
						deleted++;
						log.fine("Batch=" + batch.getDocumentNo() + " deleted.");
					} else 	if (id.isSelected()) {
						convertBatch(batch,newDocType_ID, trx);
						n++;
					}
				}
			}
			DB.commit(true, trx);
			
			if (n > 0) {
				statusMessage.append("\nDocuments Converted=" + n);
				statusMessage.append("\nDeleted Documents=" + deleted);
				success = true;
			}
		} catch (Exception e) {
			log.severe(e.getMessage());
			return false;
		}

		if (msg.length() > 0) {
			
			FDialog.error(m_WindowNo, msg.toString());
		}

		return success;
	}


	public void zoom()
	{
		log.info("");

		if (table.getSelectedCount() == 0) {
			return;
		}
		if (table.getSelectedCount() > 1) {
			FDialog.warn(m_WindowNo, "Can't zoom to more than one record.");
		}
		IDColumn id = (IDColumn)table.getValueAt(table.getSelectedIndex(), 0);
		int batch_id = id.getRecord_ID();

		MQuery query = new MQuery("M_Production_Batch");
		query.addRestriction("M_Production_Batch_ID", MQuery.EQUAL, batch_id);
		query.setRecordCount(1);
		query.setZoomColumnName("M_Production_Batch_ID");
		query.setZoomTableName("M_Production_Batch");
		query.setZoomValue(batch_id);
		AEnv.zoom (query);
	}
	
	//	zoom
	private void convertBatch(MProductionBatch batch, int C_DocType_ID, String trx) 
			throws Exception {
		String docNo = DB.getDocumentNo(C_DocType_ID, trx, false, batch);
		boolean bQtyChanged = batch.getQtyOrdered().compareTo(batch.getTargetQty()) != 0;
		String oldDocNo = batch.getDocumentNo();
		batch.setDocumentNo(docNo);
		batch.setDescription(batch.getDescription() + " converted from " + oldDocNo);
		batch.setC_DocType_ID(C_DocType_ID);

		batch.saveEx();
		
		MProduction[] headers = batch.getHeaders(true);
		int counter = 1;
		
		for (MProduction production : headers) {
			production.setDocumentNo(docNo + String.format("-%02d",counter));
			production.setC_DocType_ID(C_DocType_ID);
			if (bQtyChanged) {
				production.setProductionQty(batch.getTargetQty());
			}
			production.saveEx();
			//TODO - may be slow, we could just calculate the update
			if (bQtyChanged) {
				production.deleteLines(trx);
				production.saveEx(trx);
				production.createLines(false);
			}
			if (batch.isCreateMove()) { 
				production.createMovement();
			}
			counter++;
		}
		
		if (DocAction.STATUS_Completed.equals(batch.completeIt())) {
			batch.saveEx();
		}
	}


	private void printProduction() {
		int n = 0;
		for (int i = 0; i < table.getRowCount(); i++)
		{
			IDColumn id = (IDColumn)table.getValueAt(i, 0);     //  ID in column 0
		//	log.fine( "Row=" + i + " - " + id);
			if (id != null && id.isSelected()) {
				int batch_id = id.getRecord_ID();
				MProductionBatch batch = new MProductionBatch(Env.getCtx(), batch_id, null);
				MProduction[] productions = batch.getHeaders(true);
				if (productions.length == 0) {
					FDialog.error(m_WindowNo, "No Production Headers for Production Batch=" + batch.getDocumentNo());
					continue;
				}
				MPrintFormat pf = (MPrintFormat) batch.getC_DocType().getAD_PrintFormat();
				if (pf == null || pf.getAD_PrintFormat_ID() == 0) {
					FDialog.error(m_WindowNo, "No Print Format defined for " + batch.getC_DocType().getName() );
					return;
				}
				int M_Production_ID = productions[0].getM_Production_ID();
				MTable table = MTable.get(Env.getCtx(),pf.getAD_Table_ID());
				MQuery query = new MQuery(table.getTableName());
				query.addRestriction(table.getTableName() + "_ID", MQuery.EQUAL, M_Production_ID);
				//	Engine
				PrintInfo info = new PrintInfo(table.getTableName(),table.get_Table_ID(), M_Production_ID);               
				ReportEngine re = new ReportEngine(Env.getCtx(), pf, query, info);
				
				String printerName = re.getPrintFormat().getPrinterName();
				if (printerName != null) {
					ReportCtl.createOutput(re, false, printerName);			
				} else {
					ReportCtl.createOutput(re,true, null);
				}
				n++;
			}
		}

		
		if (n > 0) {
			FDialog.info(m_WindowNo, null, n + " Document(s) printed." );
		}
	}

	@Override
	public void tableChanged(WTableModelEvent event) {

		int row = event.getFirstRow();
		int col = event.getColumn();
		if (col != COL_TARGET_QTY) {
			return;
		}
		IDColumn id = (IDColumn)table.getValueAt(row, 0);     //  ID in column 0
		BigDecimal qty =  (BigDecimal) table.getValueAt(row, col);
//		boolean readonly = (boolean) table.getValueAt(row, COL_ISREADONLY);
//		if (readonly) {
//			
//			return;
//		}
		MProductionBatch batch = new MProductionBatch(Env.getCtx(), id.getRecord_ID(), null); //persist immediately
		batch.setTargetQty(qty);
		batch.saveEx();
		log.fine("Row changed for BatchID=" + id.getRecord_ID());
		
		
				
	}

}
