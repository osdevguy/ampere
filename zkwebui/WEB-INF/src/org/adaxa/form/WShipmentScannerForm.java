package org.adaxa.form;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;

import org.adaxa.window.ResponsiblePersonDialog;
import org.adaxa.window.ShipmentManifestDialog;
import org.adaxa.window.WPackageScannerWindow;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WStringEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.theme.ITheme;
import org.adempiere.webui.window.FDialog;
import org.apache.commons.lang.math.NumberUtils;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MInOut;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MQuery;
import org.compiere.model.MSysConfig;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Vbox;

public class WShipmentScannerForm extends ADForm implements EventListener,
		ValueChangeListener, WTableModelListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4975904775238855353L;
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(WShipmentScannerForm.class);

	/**	Window No					*/
	private int m_WindowNo = 0;

	private ArrayList<KeyNamePair> shipmentList = new ArrayList<KeyNamePair>();

	private static final String USE_MANIFEST_ON_SHIPPING = "UseManifestOnShipping";


	public static int OPTION_ALL = 0;

	// commonly used column index
	public static final int			IDX_COL_IDCOLUMN				= 0;
	public static final int			IDX_COL_C_BPARTNER_ID			= 1;
	public static final int			IDX_COL_ADDRESS					= 2;
	public static final int			IDX_COL_ORDER_NO				= 3;
	public static final int			IDX_COL_SHIPMENTNO				= 4;
	public static final int			IDX_COL_NOOFPACKAGES			= 5;
	public static final int			IDX_COL_LINES					= 6;
	public static final int			IDX_COL_ITEMS					= 7;
	public static final int			IDX_COL_DATE_MOVEMENT			= 8;
	public static final int			IDX_COL_DATE_CREATED			= 9;
	public static final int			IDX_COL_DATE_PROMISED			= 10;
	public static final int			IDX_COL_STATUS					= 11;
	public static final int			IDX_COL_MENIFEST				= 12;
	public static final int			IDX_COL_SEQNO					= 13;
	public static final int			IDX_COL_SM_REQ					= 14;
	
	public static String STATUS_AWAITING_RETRIEVAL = "0";
	public static String STATUS_AWAITING_PICKING = "2";
	public static String STATUS_PICKING_IN_PROGRESS = "4";
	public static String STATUS_AWAITING_SHIPPING = "6";
	public static String STATUS_SHIPPED = "9";
	
	private static final String TEXT_STYLE_NORMAL = "text-decoration: none; ";
	private static final String TEXT_STYLE_UNDERLINE = "text-decoration: underline; ";
	
	private static final String ROW_STYLE_SELECTED = "background-color: burlywood;";
	private static final String ROW_STYLE_AWAITING_RETRIEVAL = "background-color: white";
	private static final String ROW_STYLE_AWAITING_PICKING = "background-color: coral;";
	private static final String ROW_STYLE_PICKING_IN_PROGRESS = "background-color: plum;";
			//" border: 1px solid black;";
	private static final String ROW_STYLE_AWAITING_SHIPPING = "background-color: aqua;";
	private static final String STYLE_ALL_ACTIVE = "background-color: cornflowerblue;";
	
	private static final String STYLE_COMMENT = "width: 80%; height: 5%;	margin: 3px 10px 2px; text-align: left;" +
							"vertical-align: text-top; font-size: 14px;";
	//GUI
	/** Model table */
	private WListbox table = ListboxFactory.newDataTable();
	private Borderlayout selPanel = new Borderlayout();
	private Grid selNorthPanel = GridFactory.newGridLayout();
	private ConfirmPanel selSouthPanel = new ConfirmPanel(false,true,false,false,false,false);

	private Label lDocumentNo = new Label();
	private WStringEditor fDocumentNo = new WStringEditor();
	private Label lWarehouse = new Label();
	private WTableDirEditor fWarehouse = null;
	private Label lShipmentType = new Label();
	private Listbox fShipmentType = ListboxFactory.newDropdownListbox();
	private ListModelTable m_modelAll;
	
	private Button optAllActiveShipment = new Button();
	private Button optAwaitingRetrieval = new Button();
	private Button optAwaitingPicking = new Button();
	private Button optPickingInProgress = new Button();
	private Button optAwaitingShipping = new Button();
	private Button btnUpdate = null;
	private Textbox txtComment = new Textbox();
	private boolean isTableOnQuery = false;
	
	private MInOut m_ActiveMInOut = null;
	private ListItem m_selectedRow = null;
	private String m_lastStyle = null;
	
	private boolean m_isUseManifest = false;
	
	public WShipmentScannerForm() {
		super();
	}

	@Override
	protected void initForm() {
		
		m_isUseManifest = MSysConfig.getBooleanValue(USE_MANIFEST_ON_SHIPPING, false, Env.getAD_Client_ID(Env.getCtx()));
		
		initZk();
		dynInit();
		fDocumentNo.getComponent().focus();

	}

	private void initZk() {
		lDocumentNo.setText("DocumentNo");

		selPanel.setWidth("99%");
		selPanel.setHeight("99%");
		selPanel.setStyle("border: collapse; position: absolute");

		
		optAllActiveShipment.setStyle(STYLE_ALL_ACTIVE);
		optAwaitingRetrieval.setStyle(ROW_STYLE_AWAITING_RETRIEVAL);
		optAwaitingPicking.setStyle(ROW_STYLE_AWAITING_PICKING);
		optPickingInProgress.setStyle(ROW_STYLE_PICKING_IN_PROGRESS);
		optAwaitingShipping.setStyle(ROW_STYLE_AWAITING_SHIPPING);
		
		
		optAllActiveShipment.setSclass("quickButtons");
		optAwaitingRetrieval.setSclass("quickButtons");
		optAwaitingPicking.setSclass("quickButtons");
		optPickingInProgress.setSclass("quickButtons");
		optAwaitingShipping.setSclass("quickButtons");
		
		
		MLookup orgL = MLookupFactory.get(Env.getCtx(), m_WindowNo, 0, 2223,
				DisplayType.TableDir);
		fWarehouse = new WTableDirEditor("M_Warehouse_ID", true, false, true,
				orgL);
		lWarehouse.setText(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
		fWarehouse.setValue(Env
				.getContextAsInt(Env.getCtx(), "#M_Warehouse_ID"));
		fWarehouse.addValueChangeListener(this);

		lShipmentType.setText("ShipmentType");
		fShipmentType.addItem(new KeyNamePair(OPTION_ALL, "All Shipments"));
		fShipmentType.addActionListener(this);
		
		lShipmentType.setWidth("250px");
		lDocumentNo.setWidth("250px");
		
		Vbox vbox = new Vbox();
		vbox.setWidth("100%");
		Row row = selNorthPanel.newRows().newRow();
		row.appendChild(lDocumentNo.rightAlign());
		row.appendChild(fDocumentNo.getComponent());
		row.appendChild(lWarehouse.rightAlign());
		row.appendChild(fWarehouse.getComponent());
		row.appendChild(lShipmentType.rightAlign());
		row.appendChild(fShipmentType);

		vbox.appendChild(selNorthPanel);		
		selNorthPanel.setHeight("45px");

		North north = new North();
		north.setStyle("border: none");
		north.appendChild(vbox);
		selPanel.appendChild(north);
		Center center = new Center();
		center.setStyle("border: none");
		Panel p = new Panel();
		p.appendChild(table);
		table.setWidth("100%");
		table.setHeight("99%");			
		table.setOddRowSclass(null);
		table.setSclass("advance_layout");
//		
//		txtComment.setMultiline(true);
//		txtComment.setStyle(STYLE_COMMENT);
//		p.appendChild(new Label("Comment"));
//		p.appendChild(txtComment);
//		btnUpdate = createButton("Update", "Save", "Update comment.");
//		p.appendChild(btnUpdate);
		
		center.appendChild(p);
		center.setFlex(true);
		selPanel.appendChild(center);

		optAllActiveShipment.addActionListener(this);
		optAwaitingRetrieval.addActionListener(this);
		optAwaitingPicking.addActionListener(this);
		optPickingInProgress.addActionListener(this);
		optAwaitingShipping.addActionListener(this);
		
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(ConfirmPanel.A_PRINT));
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(ConfirmPanel.A_PROCESS));
//		selSouthPanel.addComponentsLeft(selSouthPanel.createButton("Ship"));
//		selSouthPanel.getButton("Ship").setTooltiptext("Left Premise");
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton("Manifest"));
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(ConfirmPanel.A_SAVE));
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(ConfirmPanel.A_ZOOM));
		selSouthPanel.getButton("Manifest").setTooltiptext("Update Manifest");
		selSouthPanel.getButton("Save").setTooltiptext("Update No of Packages for selected shipments.");
		selSouthPanel.getButton(ConfirmPanel.A_PROCESS).setTooltiptext("Complete shipment directly.");
		selSouthPanel.addActionListener(this);

		Hbox bottom = new Hbox();
		//bottom.setHeight("100px");
		//bottom.appendChild(selSouthPanel);
		//bottom.appendChild(new Label("Comment"));
		//bottom.appendChild(txtComment);
		South south = new South();
		south.appendChild(selSouthPanel);
		south.setHeight("34px");	
		selPanel.appendChild(south);
		


		this.appendChild(selPanel);
		fDocumentNo.getComponent().addEventListener(Events.ON_OK, this);
		table.getModel().addTableModelListener(this);
	}

	private void dynInit() {
		ColumnInfo[] layout = new ColumnInfo[] { new ColumnInfo(" ", ".", IDColumn.class, false, false, ""), // 0
				new ColumnInfo(Msg.translate(Env.getCtx(), "C_BPartner_ID"), ".", String.class), // 1
				new ColumnInfo(Msg.translate(Env.getCtx(), "Address"), ".", String.class), // 2
				new ColumnInfo(Msg.translate(Env.getCtx(), "OrderNo"), ".", String.class), // 3
				new ColumnInfo(Msg.translate(Env.getCtx(), "ShipmentNo"), ".", String.class), // 4
				new ColumnInfo(Msg.translate(Env.getCtx(), "NoofPackages"), "NoPackages", Integer.class, false), // 5
				new ColumnInfo("Lines", ".", Integer.class), // 6
				new ColumnInfo("Items", ".", Integer.class), // 7
				new ColumnInfo("Movement Date", ".", Timestamp.class, false), // 8
				new ColumnInfo("Doc Date Created", ".", String.class), // 9
				new ColumnInfo("Date Promised", ".", String.class), // 10
				new ColumnInfo("ShipStatus", ".", String.class), // 11
				new ColumnInfo("Manifest", ".", String.class), // 12
				new ColumnInfo("SeqNo", ".", Integer.class), // 13
				new ColumnInfo("SM Req", ".", Boolean.class), // 14
		};

		table.prepareTable(layout, "", "", true, "");

		// table.addEventListener(Events.ON_SELECT, this);
		loadShipments();

		// set column width
		WListItemRenderer renderer = (WListItemRenderer) table.getItemRenderer();
		ArrayList<ListHeader> headers = renderer.getHeaders();
		headers.get(IDX_COL_ORDER_NO).setWidth("70px");
		headers.get(IDX_COL_SHIPMENTNO).setWidth("70px");
		headers.get(IDX_COL_NOOFPACKAGES).setWidth("70px");
		headers.get(IDX_COL_LINES).setWidth("50px");
		headers.get(IDX_COL_ITEMS).setWidth("50px");
		headers.get(IDX_COL_DATE_MOVEMENT).setWidth("120px");
		headers.get(IDX_COL_DATE_CREATED).setWidth("120px");
		headers.get(IDX_COL_DATE_PROMISED).setWidth("120px");
		headers.get(IDX_COL_STATUS).setVisible(false);

		if (!m_isUseManifest)
		{
			headers.get(IDX_COL_MENIFEST).setVisible(false);
			headers.get(IDX_COL_SEQNO).setVisible(false);
			headers.get(IDX_COL_SM_REQ).setVisible(false);
		}
	}

	private void loadShipments() {

		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		int M_Warehouse_ID = 0;
		isTableOnQuery = true;
		if (fWarehouse.getValue() != null)
			M_Warehouse_ID = (Integer) fWarehouse.getValue();

		String sql = "select io.M_Inout_ID,bp.name " + 
				 ", bpl.name as address" +
				 ", o.documentno as orderNo,io.documentno  " +
	             ", s.lines, s.items  " +
	             ", to_char(io.created, 'dd/MM/yyyy HH:MI') as Created   " +
	             ", to_char(o.datepromised, 'dd/MM/yyyy HH24:MI') as datepromised  " +
	             ", '9' as shipstatus, io.nopackages ";
		
		if (m_isUseManifest) {
			sql += ", (select mf.documentno " +
					"from m_inout_manifest mf inner join m_inout_manifestline mfl " +
					"on mf.m_inout_manifest_id=mfl.m_inout_manifest_id " +
					"where mfl.m_inout_id = io.m_inout_id) as manifest";
			
			sql += ",(select mfl.seqno from m_inout_manifestline mfl where mfl.m_inout_id = io.m_inout_id) as seqno ";
			
			sql += ", COALESCE((select ss.isshipmentmanifestmandatory from m_shipper ss where ss.m_shipper_id = io.m_shipper_id), 'N') ";
		}
		sql += " , io.movementdate ";
	    sql +=   "from M_Inout io   " +
	             "left join C_Order o on o.C_Order_ID=io.C_Order_ID   " +
	             "left join C_BPartner bp on bp.C_BPartner_ID=io.C_BPartner_ID   " +
	             "LEFT JOIN C_BPartner_Location bpl ON o.C_BPartner_Location_ID = bpl.C_BPartner_Location_ID " +	             
	             "left join (  " +
	             " select m_inout_id, count(qtyentered) as lines, sum(qtyentered) as items  " +
	             " from m_inoutline where qtyentered > 0 and coalesce(m_product_id,0) <> 0 group by m_inout_id) s  " +
	             " on s.m_inout_id = io.m_inout_id  ";
		
	    sql +=    "where  io.isSoTrx='Y' and io.docstatus IN ('IP', 'IN') " +
	             "and COALESCE(io.m_shipper_id,0) > 0 " +
	             "and io.ad_client_id = ? ";

		
		
		if (M_Warehouse_ID > 0)
			sql += " and  io.M_Warehouse_ID=?";

        sql = sql + " ORDER BY io.documentno";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int idx = 2;
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, AD_Client_ID);
			if (M_Warehouse_ID > 0) {
				pstmt.setInt(idx, M_Warehouse_ID);
				idx++;
			}
			rs = pstmt.executeQuery();

			if (m_modelAll != null) {
				table.setModel(m_modelAll);
			}
			table.clearTable();
			int row = table.getItemCount();
			table.setRowCount(row);
			shipmentList.clear();
			
			while (rs.next()) {

				table.setRowCount(row + 1);
				table.setValueAt(new IDColumn(rs.getInt(1)), row, IDX_COL_IDCOLUMN);  //m_inout_id
				table.setValueAt(rs.getString(2), row, IDX_COL_C_BPARTNER_ID);  // bpname
				table.setValueAt(rs.getString(3), row, IDX_COL_ADDRESS);  // bpaddress
				table.setValueAt(rs.getString(4), row, IDX_COL_ORDER_NO); // orderno
				table.setValueAt(rs.getString(5), row, IDX_COL_SHIPMENTNO); //shipmenno
				table.setValueAt(rs.getInt(6), row, IDX_COL_LINES); //lines
				table.setValueAt(rs.getInt(7), row, IDX_COL_ITEMS); //items
				table.setValueAt(rs.getTimestamp("MovementDate"), row, IDX_COL_DATE_MOVEMENT); // MovementDate
				table.setValueAt(rs.getString(8), row, IDX_COL_DATE_CREATED);  //date created
				table.setValueAt(rs.getString(9), row, IDX_COL_DATE_PROMISED);  //date promised
				
				table.setValueAt(rs.getString(11), row, IDX_COL_NOOFPACKAGES);  //NoOfPackages
				if (m_isUseManifest) { 
					table.setValueAt(rs.getString(12), row, IDX_COL_MENIFEST); //manifest
					table.setValueAt(rs.getInt(13), row, IDX_COL_SEQNO); //seq no
					boolean isReq = rs.getString(14) != null && rs.getString(14).equals("Y");
					table.setValueAt(isReq, row, IDX_COL_SM_REQ); //Manifest Mandatory
				}
//				String status = rs.getString(5);
//				if ("IP".equals(status))
//					table.setValueAt("In Progress", row, 4);
//				else if ("DR".equals(status))
//					table.setValueAt("Drafted", row, 4);
//				else
//					table.setValueAt(status, row, 4);
//
//				table.setValueAt(rs.getString(6), row, 5);
//				table.setValueAt(rs.getString(7), row, 6);
				shipmentList.add(new KeyNamePair(rs.getInt(1), rs.getString(5)));

				row++;
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, sql, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		table.repaint();
		this.invalidate();
		
		m_modelAll = table.getModel();
		isTableOnQuery = false;
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (Events.ON_CLICK.equals(event.getName())) {
			if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_REFRESH)) {
				loadShipments();
				fDocumentNo.getComponent().focus();
			}
			else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_ZOOM)) {
				zoom();
			}
			else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_SAVE)) {
				saveNoPackages();
			}
			else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_PRINT)) {
				printShipments();
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_RESET)) {
				resetStatus();
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_OK)) {
				SessionManager.getAppDesktop().closeActiveWindow();
				return;
			}
			else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_PROCESS)) {
				//ListItem item = table.getItems().get(0);
				//item.setSelected(true);
				ResponsiblePersonDialog dlg = 
						new ResponsiblePersonDialog(ResponsiblePersonDialog.ShipmentAction.COMPLETE, getWindowNo(), table);
				SessionManager.getAppDesktop().showWindow(dlg);
				if (!dlg.isCancelled()) {
					loadShipments();  //reload and update table
				}
				
			}

			else if (event.getTarget() == selSouthPanel.getButton("Manifest")) {
				ShipmentManifestDialog dlg = 
						new ShipmentManifestDialog(ShipmentManifestDialog.ManifestAction.UPDATE, getWindowNo(), table);

				SessionManager.getAppDesktop().showWindow(dlg);
				if (!dlg.isCancelled()) {
					loadShipments();  //reload and update table
				}
			}
			else if (event.getTarget() == selSouthPanel.getButton("Ship")) {
				ResponsiblePersonDialog dlg = 
						new ResponsiblePersonDialog(ResponsiblePersonDialog.ShipmentAction.SHIP, getWindowNo(), table);
				SessionManager.getAppDesktop().showWindow(dlg);
				if (!dlg.isCancelled()) {
					loadShipments();  //reload and update table
				}
				
			}
		}
		else if (event.getTarget().equals(fShipmentType))
		{
			KeyNamePair pp = fShipmentType.getSelectedItem().toKeyNamePair();
			if (pp == null)
				;
			else
			{
//				int id = pp.getKey();
//				if (id == OPTION_PARTSONLY) {
//					m_isVehicle  = "N";
//				}
//				else if (id == OPTION_VEHICLEONLY) {
//					m_isVehicle = "Y";
//				}
				return;
			}
		} else if (event.getTarget() == fDocumentNo.getComponent()
				&& Events.ON_OK.equals(event.getName())) {
			KeyNamePair selection = null;
			String shipNo = (String) fDocumentNo.getValue();
			if (shipNo == null || "".equals(shipNo.trim()))
				return;
			shipNo = shipNo.trim();
			for (int i = 0; i < shipmentList.size(); i++) {
				KeyNamePair pair = shipmentList.get(i);
				if (shipNo.equals(pair.getName())) {
					selection = pair;
					break;
				}
			}

			if (selection == null) {
				FDialog.error(m_WindowNo, "Shipment " + shipNo
						+ " not available to confirm");
				return;
			}
			int M_Inout_ID = selection.getKey();
			MInOut inout = new MInOut(Env.getCtx(), M_Inout_ID, null);
			String status = inout.getDocStatus();
			if ("CO".equals(status) || "CL".equals(status) || "RE".equals(status)) {
				FDialog.error(m_WindowNo, "Shipment " + shipNo
						+ " is already completed.");
				return;
			}
//			filtering is done on the query
//			if (!"DR".equals(inout.getDocStatus())
//					&& !"IN".equals(inout.getDocStatus())) {
//				FDialog.error(m_WindowNo, "Shipment " + shipNo
//						+ " not available to confirm");
//				return;
//			}
			WPackageScannerWindow wps = new WPackageScannerWindow(m_WindowNo, inout);
			wps.setVisible(true);
			AEnv.showWindow(wps);
			//Reload shipments
			loadShipments();
			fDocumentNo.setValue("");
			fDocumentNo.getComponent().focus();
		} else {
			super.onEvent(event);
		}
	}

	private void saveNoPackages() throws IllegalStateException, SQLException {
		int rows = table.getRowCount();
		String trxName = Trx.createTrxName();
		String msg = "";
		for (int i = 0; i < rows; i++)
		{
			IDColumn id = (IDColumn) table.getValueAt(i, IDX_COL_IDCOLUMN); // ID in column
			if (id != null && id.isSelected()) {
				int inout_ID = id.getRecord_ID();
				MInOut io = new MInOut(Env.getCtx(), inout_ID, trxName);
				try {
					int noPackages = Integer.parseInt((table.getValueAt(i, IDX_COL_NOOFPACKAGES).toString()));
					Timestamp movementDate = new Timestamp(
							((Date) table.getValueAt(i, IDX_COL_DATE_MOVEMENT)).getTime());
					io.setNoPackages(noPackages);
					io.setMovementDate(movementDate);
					io.saveEx(trxName);
				} catch (Exception e) {
					String s = table.getValueAt(i, IDX_COL_NOOFPACKAGES).toString();
					
					if (!NumberUtils.isNumber(s)) {
						msg = msg + s + " is not numeric in Shipment No=" + io.getDocumentNo() + ". Please check RowNo=" + (i+1) + "\n";
					}
					
					if (((Date) table.getValueAt(i, IDX_COL_DATE_MOVEMENT)) == null)
					{
						msg = msg + " MovementDate is invalid in Shipment No=" + io.getDocumentNo()
								+ ". Please check RowNo=" + (i + 1) + "\n";
					}
				}
			
			}
		}
		DB.commit(true, trxName);
		if (msg.length() > 0) {
			FDialog.warn(getWindowNo(), msg);
		}
	}

	private void resetStatus() {
		
		if (!FDialog
				.ask(getWindowNo(), null, "Reset Selected Items to Awaiting Retrieval?")) {
			return;
		}
		
		
		String trxName = Trx.createTrxName();
		String sql = "UPDATE m_inout SET retrievedby = NULL" +
	             "   , retrievedate = NULL " +
	             "   , pickedby = NULL " +
	             "   , pickdate = NULL " +
	             "   , shipmentstatus = '0' " +
	             "WHERE shipmentstatus IN ('2', '4') " +
	             "and m_inout_id = ?";
		
		
		PreparedStatement pstmt = DB.prepareStatement(sql, trxName);

		try {
			for (int i = 0; i < table.getRowCount(); i++)
			{
				IDColumn id = (IDColumn)table.getValueAt(i, IDX_COL_IDCOLUMN);     //  ID in column 0
			//	log.fine( "Row=" + i + " - " + id);
				if (id != null && id.isSelected()) {
					pstmt.setInt(1, id.getRecord_ID());
					pstmt.execute();
				}
			}
			DB.commit(true, trxName);
			pstmt.close();
		} catch (Exception e) {
			log.severe(e.getMessage());
		}

		loadShipments();
	}

	/**
	 * 
	 */

	


	private void printShipments() {
		for (int i = 0; i < table.getRowCount(); i++)
		{
			IDColumn id = (IDColumn)table.getValueAt(i, IDX_COL_IDCOLUMN);     //  ID in column 0
		//	log.fine( "Row=" + i + " - " + id);
			if (id != null && id.isSelected()) {
				ReportEngine re = ReportEngine.get (Env.getCtx(), ReportEngine.SHIPMENT, id.getRecord_ID());
				String printerName = re.getPrintFormat().getPrinterName();
				if (printerName != null) {
					ReportCtl.createOutput(re, false, printerName);			
				} else {
					ReportCtl.createOutput(re,true, null);
				}
			}
		}

		
	}
	
	private void zoom() {
		log.info("");
		Integer M_InOut_ID = table.getSelectedRowKey();
		if (M_InOut_ID == null)
			return;

		MQuery query = new MQuery("M_InOut");
		query.addRestriction("M_InOut_ID", MQuery.EQUAL, M_InOut_ID);
		query.setTableName("M_InOut");
		query.setZoomColumnName("M_InOut_ID");
		query.setZoomValue(M_InOut_ID);
		query.setRecordCount(1);
		AEnv.zoom(query);
	}
	
	public void valueChange(ValueChangeEvent evt) {
		loadShipments();

	}

	private Button createButton(String name, String image, String tooltip) {
		Button btn = new Button("");
		btn.setName("btn" + name);
		btn.setImage(ITheme.IMAGE_FOLDER + image + "24.png");
		btn.setTooltiptext(Msg.getMsg(Env.getCtx(), tooltip));

		LayoutUtils.addSclass("action-button", btn);

		btn.setTabindex(0);
		btn.addEventListener(Events.ON_CLICK, this);
		btn.setDisabled(false);

		return btn;
	}

	public void tableChanged(WTableModelEvent event) {
		if (!isTableOnQuery ) 
		{
			
			//display comment only when 1 item is selected
			if (table.getSelectedCount() == 1) {
			
				if (m_selectedRow != null) {
					//return previous state
					m_selectedRow.setStyle(m_lastStyle);
				}
				//TODO - autosave comment?
				int idx = table.getSelectedIndex();
				ListItem item = table.getItems().get(idx);
				m_lastStyle = item.getStyle();
				m_selectedRow = item;
				
			}
			else {
//				btnUpdate.setDisabled(true);
//				txtComment.setValue("");
			}
			
//			if (IDX_COL_DATE_MOVEMENT == event.getColumn()
//					&& table.getValueAt(event.getIndex0(), event.getColumn()) instanceof Date)
//			{
//				Timestamp ts = new Timestamp(((Date) table.getValueAt(event.getIndex0(), event.getColumn())).getTime());
////				table.setValueAt(ts, event.getIndex0(), event.getColumn());
//			}
		}
	}
}
