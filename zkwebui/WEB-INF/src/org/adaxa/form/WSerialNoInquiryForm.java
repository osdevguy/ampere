package org.adaxa.form;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.editor.WStringEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.window.FDialog;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MRequest;
import org.compiere.print.ReportCtl;
import org.compiere.print.ReportEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkforge.keylistener.Keylistener;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;

public class WSerialNoInquiryForm extends ADForm implements EventListener,
		ValueChangeListener {

	private static final String BTN_PRINT_ORDER = "Printw1";
	private static final String BTN_PRINT_SHIPMENT = "Printw2";
	private static final String BTN_PRINT_INVOICE = "Printw3";
	//private static final int COL_TARGET_QTY = 8;
	//private static final int COL_ISREADONLY = 9;
	private static final int GRID_PAGE_SIZE = 12;
	/**
	 * 
	 */
	private static final long serialVersionUID = -4975904775238855353L;
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(WSerialNoInquiryForm.class);

	/**	Window No					*/
	private int m_WindowNo = 53127;

	private ArrayList<KeyNamePair> documentList = new ArrayList<KeyNamePair>();

	//GUI
	/** Model table */
	private WListbox table = ListboxFactory.newDataTable();
	private Borderlayout selPanel = new Borderlayout();
	private Grid selNorthPanel = GridFactory.newGridLayout();
	private ConfirmPanel selSouthPanel = new ConfirmPanel(false,true,false,false,false,false);

	private Label lBPartner = new Label();
	private WSearchEditor fBPartner;
	private Label     lPCat = new Label("Product Category");
	private WTableDirEditor  fPCategory = null;
	private Label lProduct = new Label("Product");
	private WSearchEditor fProduct;
	private Label     lSerNo = new Label("Serial Number");
	private WStringEditor fSerno = new WStringEditor();
	
	private StringBuffer statusMessage = new StringBuffer();
	private Keylistener keyListener;
	private int currentRow;
	
	public int				m_AD_Org_ID = Env.getAD_Org_ID(Env.getCtx());
	public int				m_C_BPartner_ID = 0;
	public int				m_M_Product_ID = 0;
	public String			m_SerNo = null;
	public int				m_PCat_ID = 0;
	
	public WSerialNoInquiryForm() {
		super();
	}

	@Override
	protected void initForm() {
		initZk();
		dynInit();
	}

	private void initZk() {

		this.setTitle(Msg.translate(Env.getCtx(), "SerialNoInquiryForm"));
		selPanel.setWidth("99%");
		selPanel.setHeight("99%");
		selPanel.setStyle("border: none; position: absolute");
		

		MLookup pL = MLookupFactory.get (Env.getCtx(), m_WindowNo, 0, 78208, DisplayType.Search);		
		fProduct = new WSearchEditor ("M_Product_ID", true, false, true, pL);
		fProduct.addValueChangeListener(this);
		
		MLookup pcL = MLookupFactory.get (Env.getCtx(), m_WindowNo, 0, 2012, DisplayType.TableDir);
		fPCategory = new WTableDirEditor("M_Product_Category_ID", true, false, true, pcL);
		fPCategory.addValueChangeListener(this);
		
		MLookup bpL = MLookupFactory.get (Env.getCtx(), m_WindowNo, 0, 2762, DisplayType.Search);
		fBPartner = new WSearchEditor ("C_BPartner_ID", false, false, true, bpL);
		lBPartner.setText(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		fBPartner.addValueChangeListener(this);
		
		fSerno.getComponent().addEventListener(Events.ON_OK, this);
		
		Row row = selNorthPanel.newRows().newRow();
		lSerNo.setText(Msg.translate(Env.getCtx(), "SerNo"));
		row.appendChild(lSerNo.rightAlign());
		row.appendChild(fSerno.getComponent());
		row.appendChild(lBPartner.rightAlign());
		row.appendChild(fBPartner.getComponent());

		row = new Row();
		selNorthPanel.getRows().appendChild(row);
		row.appendChild(lPCat.rightAlign());
		row.appendChild(fPCategory.getComponent());
		row.appendChild(lProduct.rightAlign());
		row.appendChild(fProduct.getComponent());

		
		North north = new North();
		north.setStyle("border: none");
		north.appendChild(selNorthPanel);
		selPanel.appendChild(north);
		Center center = new Center();
		//center.setStyle("border: none");
		Panel p = new Panel();
		p.appendChild(table);
		table.setWidth("100%");
		table.setHeight("100%");
		center.appendChild(p);
		center.setFlex(true);
		selPanel.appendChild(center);

		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(BTN_PRINT_ORDER));
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(BTN_PRINT_SHIPMENT));
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(BTN_PRINT_INVOICE));
		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(ConfirmPanel.A_PROCESS));
		selSouthPanel.addActionListener(this);

		selSouthPanel.getButton(BTN_PRINT_ORDER).setTooltiptext("Print Sales Order");
		selSouthPanel.getButton(BTN_PRINT_SHIPMENT).setTooltiptext("Print Customer Shipment");
		selSouthPanel.getButton(BTN_PRINT_INVOICE).setTooltiptext("Print Customer Invoice");
		selSouthPanel.getButton(ConfirmPanel.A_PROCESS).setTooltiptext("Create Request");
		
		selSouthPanel.setHeight("38px");
		South south = new South();
		south.appendChild(selSouthPanel);
		selPanel.appendChild(south);

		this.appendChild(selPanel);

	}

	private void dynInit() {
		ColumnInfo[] layout = new ColumnInfo[] {
				new ColumnInfo(" ",                                         
						".", IDColumn.class, false, false, "M_AttributeSetInstance_ID"),
				//new ColumnInfo(Msg.translate(Env.getCtx(), "M_AttributeSetInstance_ID"), ".", Integer.class),       //1
				new ColumnInfo(Msg.translate(Env.getCtx(), "SerNo"), ".", String.class),       //1
				new ColumnInfo(Msg.translate(Env.getCtx(), "BPValue"), ".", String.class),       //2
				new ColumnInfo(Msg.translate(Env.getCtx(), "BPName"), ".", String.class),       //3
				new ColumnInfo(Msg.translate(Env.getCtx(), "PValue"), ".", String.class),       //4
				new ColumnInfo(Msg.translate(Env.getCtx(), "PDescription"), ".", String.class),       //5
				new ColumnInfo(Msg.translate(Env.getCtx(), "PCatName"), ".", String.class),       //6
				new ColumnInfo(Msg.translate(Env.getCtx(), "ShipmentNo"), ".", String.class),       //7
				new ColumnInfo(Msg.translate(Env.getCtx(), "OrderNo"), ".", String.class),       //8
				new ColumnInfo(Msg.translate(Env.getCtx(), "InvoiceNo"), ".", String.class),       //9
				new ColumnInfo(Msg.translate(Env.getCtx(), "M_Inout_ID"), ".", String.class),       //10
				new ColumnInfo(Msg.translate(Env.getCtx(), "C_Order_ID"), ".", String.class),       //11
				new ColumnInfo(Msg.translate(Env.getCtx(), "C_Invoice_ID"), ".", String.class),       //12
				}; 

		table.prepareTable(layout, "", "", true, "");
		table.setSclass("scannerlist");

		loadDocuments();
		
		//set column width
		WListItemRenderer renderer = (WListItemRenderer) table.getItemRenderer();
		ArrayList<ListHeader> headers = renderer.getHeaders();
		int idx = 0;
		//headers.get(idx++).setVisible(false);
		
		headers.get(idx++).setWidth("30px");  
		headers.get(idx++).setWidth("70px");    //SerNo
		headers.get(idx++).setWidth("70px");    //BPValue
		headers.get(idx++).setWidth("120px");    //BPName
		headers.get(idx++).setWidth("70px");    //PValue
		headers.get(idx++).setWidth("120px");    //PDescription
		headers.get(idx++).setWidth("100px");    //PCatName
		headers.get(idx++).setWidth("60px");    //ShipmentNo
		headers.get(idx++).setWidth("60px");    //OrderNo
		headers.get(idx++).setWidth("60px");    //InvoiceNo
		headers.get(idx++).setVisible(false);    //M_Inout_ID
		headers.get(idx++).setVisible(false);   //C_Order_ID
		headers.get(idx++).setVisible(false);    //C_Invoice_ID

		renderer.addEventListener(this);
//		table.addEventListener(Events.ON_CHANGE, this);
//		WListItemRenderer wrl=(WListItemRenderer)table.getItemRenderer();
//		wrl.addEventListener(this);
		
		table.setMold("paging");
		//table.setPageSize(GRID_PAGE_SIZE);
		table.setMultiSelection(false);
//		if (keyListener != null)
//    		keyListener.detach();
//    	keyListener = new Keylistener();
//    	this.appendChild(keyListener);
//    	keyListener.setCtrlKeys("#up#down");
////    	keyListener.addEventListener(Events.ON_CTRL_KEY, this);
//    	keyListener.setAutoBlur(false);
    	
		
	}

	/**
	 * Get SQL to retrieve Serial Number
	 * @return
	 */
	private String getSQL()
	{
	    StringBuffer sql = new StringBuffer(
	    	             "SELECT asi.m_attributesetinstance_id, asi.serno, bp.value, bp.name, p.value AS PValue, p.name AS PDescription, " +
	    	             "       pc.name AS PCatName, io.documentno AS shipmentno " +
	    	             "       , (SELECT o.documentno FROM c_order o WHERE o.c_order_id = io.c_order_id) AS orderno " +
	    	             "       , (SELECT i.documentno  FROM c_invoice i " +
	    	             "          INNER JOIN c_invoiceline il ON i.c_invoice_id = il.c_invoice_id " +
	    	             "          WHERE il.m_inoutline_id = iol.m_inoutline_id) AS invoiceno " +
	    	             "       , io.m_inout_id, io.c_order_id " +
	    	             "       , (SELECT c_invoice_id FROM c_invoiceline WHERE m_inoutline_id = iol.m_inoutline_id) AS c_invoice_id " +
	    	             "FROM m_attributesetinstance asi " +
	    	             "  INNER JOIN m_inoutline iol ON asi.m_attributesetinstance_id = iol.m_attributesetinstance_id " +
	    	             "  INNER JOIN m_inout io ON io.m_inout_id = iol.m_inout_id " +
	    	             "  INNER JOIN c_bpartner bp ON io.c_bpartner_id = bp.c_bpartner_id " +
	    	             "  INNER JOIN m_product p ON iol.m_product_id = p.m_product_id " +
	    	             "  INNER JOIN m_product_category pc ON p.m_product_category_id = pc.m_product_category_id " +
	    	             "WHERE asi.serno IS NOT NULL AND asi.AD_Client_ID = ?");
	    
	    if (m_AD_Org_ID > 0) {
	    	sql.append(" AND p.AD_Org_ID=?");
	    }
	    if (m_C_BPartner_ID > 0) {
	    	sql.append(" AND bp.C_BPartner_ID=?");
	    }
	    if (m_PCat_ID > 0) {
	    	sql.append(" AND pc.M_Product_Category_ID=?");
	    }
	    if (m_M_Product_ID > 0) {
	    	sql.append(" AND p.M_Product_ID=?");
	    }

	    if (m_SerNo != null && m_SerNo.length() > 0) {
	    	sql.append(" AND asi.serno ILIKE ?");
	    }
	    
        sql.append(" ORDER BY asi.serno");
	    return sql.toString();
	}
	
	private void loadDocuments() {

		updateStatus();
		
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());

		
		if (fBPartner.getValue() != null) {
			m_C_BPartner_ID = (Integer) fBPartner.getValue();
		}
		if (fPCategory.getValue() != null) {
			m_PCat_ID = (Integer) fPCategory.getValue();
		}
		if (fProduct.getValue() != null) {
			m_M_Product_ID = (Integer) fProduct.getValue();
			m_PCat_ID = 0;  // override category
		}
		if (fSerno.getValue() != null) {
			m_SerNo = fSerno.getValue().toString();
		}

		String sql = getSQL();
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			int i = 1;
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(i++, AD_Client_ID);
		    if (m_AD_Org_ID > 0) {
		    	pstmt.setInt(i++, m_AD_Org_ID);
		    }
		    if (m_C_BPartner_ID > 0) {
		    	pstmt.setInt(i++, m_C_BPartner_ID);
		    }
		    if (m_PCat_ID > 0) {
		    	pstmt.setInt(i++, m_PCat_ID);
		    }
		    if (m_M_Product_ID > 0) {
		    	pstmt.setInt(i++, m_M_Product_ID);
		    }
		    if (m_SerNo != null && m_SerNo.length() > 0) {
		    	pstmt.setString(i++, "%" + m_SerNo + "%");
		    }
			rs = pstmt.executeQuery();

			table.clearTable();
			int row = table.getItemCount();
			table.setRowCount(row);
			documentList.clear();

			while (rs.next()) {

				table.setRowCount(row + 1);
				int idx = 0;				
				table.setValueAt(new IDColumn(rs.getInt(idx+1)), row, idx++);   //  M_Attributesetinstance_ID
				//table.setValueAt(rs.getInt(idx+1), row, idx++);                //SerNo
				table.setValueAt(rs.getString(idx+1), row, idx++);                //SerNo
				table.setValueAt(rs.getString(idx+1), row, idx++);                //BPValue
				table.setValueAt(rs.getString(idx+1), row, idx++);                //BPName
				table.setValueAt(rs.getString(idx+1), row, idx++);                //PValue
				table.setValueAt(rs.getString(idx+1), row, idx++);                //PDescription
				table.setValueAt(rs.getString(idx+1), row, idx++);                //PCatName
				table.setValueAt(rs.getString(idx+1), row, idx++);                //ShipmentNo
				table.setValueAt(rs.getString(idx+1), row, idx++);                //OrderNo
				table.setValueAt(rs.getString(idx+1), row, idx++);                //InvoiceNo
				table.setValueAt(rs.getString(idx+1), row, idx++);                //M_Inout_ID
				table.setValueAt(rs.getString(idx+1), row, idx++);                //C_Order_ID
				table.setValueAt(rs.getString(idx+1), row, idx++);                //C_Invoice_ID				
				row++;
				
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, sql, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		table.repaint();
		this.invalidate();
		
		//clear message 
		statusMessage = new StringBuffer(); 
	}

	@Override
	public void onEvent(Event event) throws Exception { 

		
		if (Events.ON_CLICK.equals(event.getName())) {
			if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_REFRESH)) {
				loadDocuments();
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_PROCESS)) {
				generateRequest();
			}else if (event.getTarget() == selSouthPanel.getButton(BTN_PRINT_ORDER)) {
				print(ReportEngine.ORDER);
			}else if (event.getTarget() == selSouthPanel.getButton(BTN_PRINT_SHIPMENT)) {
				print(ReportEngine.SHIPMENT);
			}else if (event.getTarget() == selSouthPanel.getButton(BTN_PRINT_INVOICE)) {
				print(ReportEngine.INVOICE);
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_OK)) {
				if (hasSelections()) {
					return;  //avoid user from accidentally closing the window
				}
				SessionManager.getAppDesktop().closeActiveWindow();
				return;
			} 
		} else if (event.getTarget() == fSerno.getComponent()) {
			loadDocuments();
		} else if (event.getTarget() == fProduct.getComponent()
				&& Events.ON_OK.equals(event.getName())) {
			loadDocuments();
		} else {
			super.onEvent(event);
		}
	}

	private void generateRequest() {
		if (table.getSelectedIndex() < 0) {
			FDialog.info(m_WindowNo, null, "No selected item to generate.");
		}
		String trxName = Trx.createTrxName("SerialNoInquiry");
		try {
			int M_AttributeSetInstance_ID = table.getSelectedRowKey();
			MRequest request = MRequest.getBySerialNo(Env.getCtx(), M_AttributeSetInstance_ID, trxName );
			if (request != null) {
				AEnv.zoom(MRequest.Table_ID, request.get_ID());
			}
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		
	}

	private void updateStatus()
	{
		//TODO - update status bar to display message
		if (statusMessage.length() > 0) {
			
			FDialog.info(m_WindowNo, null, statusMessage.toString());
		}

	}
	
	public void valueChange(ValueChangeEvent evt) {
		if (fProduct.equals(evt.getSource())) { //fix timing event problem
			fProduct.setValue(evt.getNewValue());
			if (evt.getNewValue() == null) {
				m_M_Product_ID = 0;
			}
		} else 	if (fBPartner.equals(evt.getSource())) { 
			fBPartner.setValue(evt.getNewValue());
			if (evt.getNewValue() == null) {
				m_C_BPartner_ID = 0;
			}
		}
		
		loadDocuments();

	}
	
	private boolean hasSelections()
	{
		if (table.getSelectedCount() > 0) {
			return true;
		}
		return false;
	}


	

	private void print(int documentType)
	{
		String recordID = null;
		
		int index = table.getSelectedIndex();
		if (index < 0) {
			FDialog.info(m_WindowNo, null, "No selected item to Print.");
			return;
		}
		if (documentType == ReportEngine.ORDER) {
			recordID = (String) table.getValueAt(index, 11);
		} else if (documentType == ReportEngine.SHIPMENT) {
			recordID = (String) table.getValueAt(index, 10);
		} else if (documentType == ReportEngine.INVOICE) {
			recordID = (String) table.getValueAt(index, 12);
		} else {
			//unexpected document
			return;
		}
		ReportEngine re = ReportEngine.get (Env.getCtx(), documentType, Integer.valueOf(recordID));
		if (re == null) {
			throw new AdempiereException("Unable to create a Report Engine.");
		}
		String printerName = re.getPrintFormat().getPrinterName();
		if (printerName != null) {
			ReportCtl.createOutput(re, false, printerName);			
		} else {
			ReportCtl.createOutput(re,true, null);
		}

	}
}