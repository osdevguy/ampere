package org.adaxa.form;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;

import org.adaxa.window.WReceiptScannerWindow;
import org.adempiere.exceptions.ValueChangeEvent;
import org.adempiere.exceptions.ValueChangeListener;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WStringEditor;
import org.adempiere.webui.editor.WTableDirEditor;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.window.FDialog;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MInOut;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkex.zul.Borderlayout;
import org.zkoss.zkex.zul.Center;
import org.zkoss.zkex.zul.North;
import org.zkoss.zkex.zul.South;

public class WReceiptScannerForm extends ADForm implements EventListener,
		ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4975904775238855353L;
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(WReceiptScannerForm.class);

	/**	Window No					*/
	private int m_WindowNo = 0;

	private ArrayList<KeyNamePair> shipmentList = new ArrayList<KeyNamePair>();

	//GUI
	/** Model table */
	private WListbox table = ListboxFactory.newDataTable();
	private Borderlayout selPanel = new Borderlayout();
	private Grid selNorthPanel = GridFactory.newGridLayout();
	private ConfirmPanel selSouthPanel = new ConfirmPanel(false,true,false,false,false,false);

	private Label lDocumentNo = new Label();
	private WStringEditor fDocumentNo = new WStringEditor();
	private Label lWarehouse = new Label();
	private WTableDirEditor fWarehouse = null;
	private StringBuffer statusMessage = new StringBuffer();

	public WReceiptScannerForm() {
		super();
	}

	@Override
	protected void initForm() {
		initZk();
		dynInit();
	}

	private void initZk() {
		lDocumentNo.setText(Msg.translate(Env.getCtx(), "DocumentNo"));

		selPanel.setWidth("99%");
		selPanel.setHeight("98%");
		selPanel.setStyle("border: none; position: absolute");

		MLookup orgL = MLookupFactory.get(Env.getCtx(), m_WindowNo, 0, 2223,
				DisplayType.TableDir);
		fWarehouse = new WTableDirEditor("M_Warehouse_ID", true, false, true,
				orgL);
		lWarehouse.setText(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
		fWarehouse.setValue(Env
				.getContextAsInt(Env.getCtx(), "#M_Warehouse_ID"));
		fWarehouse.addValueChangeListener(this);

		Row row = selNorthPanel.newRows().newRow();
		row.appendChild(lWarehouse.rightAlign());
		row.appendChild(fWarehouse.getComponent());
		row.appendChild(lDocumentNo.rightAlign());
		row.appendChild(fDocumentNo.getComponent());
		
		North north = new North();
		north.setStyle("border: none");
		north.appendChild(selNorthPanel);
		selPanel.appendChild(north);
		Center center = new Center();
		//center.setStyle("border: none");
		Panel p = new Panel();
		p.appendChild(table);
		table.setWidth("100%");
		table.setHeight("100%");
		center.appendChild(p);
		center.setFlex(true);
		selPanel.appendChild(center);

		selSouthPanel.addComponentsLeft(selSouthPanel.createButton(ConfirmPanel.A_PROCESS));
		selSouthPanel.addActionListener(this);

		South south = new South();
		selSouthPanel.setHeight("40px");
		south.appendChild(selSouthPanel);
		selPanel.appendChild(south);

		this.appendChild(selPanel);
		fDocumentNo.getComponent().addEventListener(Events.ON_OK, this);
	}

	private void dynInit() {
		ColumnInfo[] layout = new ColumnInfo[] {
				new ColumnInfo(" ",                                         
						".", IDColumn.class, false, false, ""),
				new ColumnInfo(Msg.translate(Env.getCtx(), "C_BPartner_ID"),
						".", String.class), //  1
				new ColumnInfo(Msg.translate(Env.getCtx(), "OrderNo"), ".",
						String.class), //2
				new ColumnInfo(Msg.translate(Env.getCtx(), "ShipmentNO"), ".",
						String.class), //  3
				new ColumnInfo("Doc Status", ".",
						String.class), //  4
				new ColumnInfo(Msg.translate(Env.getCtx(), "AWB/Con No"), ".",
						String.class) }; //5

		table.prepareTable(layout, "", "", true, "");

		loadReceipts();
		
		//set column width
		WListItemRenderer renderer = (WListItemRenderer) table.getItemRenderer();
		ArrayList<ListHeader> headers = renderer.getHeaders();
		headers.get(1).setWidth("100px");
		headers.get(2).setWidth("80px");
		headers.get(3).setWidth("80px");
		headers.get(4).setWidth("70px");

		//table.addEventListener(Events.ON_SELECT, this);
	}

	private void loadReceipts() {

		updateStatus();
		
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		int M_Warehouse_ID = 0;
		if (fWarehouse.getValue() != null)
			M_Warehouse_ID = (Integer) fWarehouse.getValue();

		String sql = "select io.M_Inout_ID,bp.name,o.documentno as orderNo,io.documentno,io.docstatus " 
				+ ", io.consignmentno"
				+ "  from M_Inout io "
				+ "left join C_Order o on o.C_Order_ID=io.C_Order_ID "
				+ "left join C_BPartner bp on bp.C_BPartner_ID=io.C_BPartner_ID "
				+ "where  io.isSoTrx='N' and io.processed = 'N' and io.AD_Client_ID=?";
		if (M_Warehouse_ID > 0)
			sql += " and  io.M_Warehouse_ID=?";

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, AD_Client_ID);
			if (M_Warehouse_ID > 0)
				pstmt.setInt(2, M_Warehouse_ID);

			rs = pstmt.executeQuery();

			table.clearTable();
			int row = table.getItemCount();
			table.setRowCount(row);
			shipmentList.clear();

			while (rs.next()) {

				table.setRowCount(row + 1);
				table.setValueAt(new IDColumn(rs.getInt(1)), row, 0);
				table.setValueAt(rs.getString(2), row, 1);
				table.setValueAt(rs.getString(3), row, 2);
				table.setValueAt(rs.getString(4), row, 3);
				String status = rs.getString(5);
				if ("IP".equals(status))
					table.setValueAt("In Progress", row, 4);
				else if ("DR".equals(status))
					table.setValueAt("Drafted", row, 4);
				else
					table.setValueAt(status, row, 4);

				shipmentList
						.add(new KeyNamePair(rs.getInt(1), rs.getString(4)));

				table.setValueAt(rs.getString(6), row, 5);
				row++;
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, sql, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		table.repaint();
		this.invalidate();
		
		//clear message 
		statusMessage = new StringBuffer(); 
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (Events.ON_CLICK.equals(event.getName())) {
			if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_REFRESH)) {
				loadReceipts();
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_PROCESS)) {
				completeReceipts();
				loadReceipts();
			}else if (event.getTarget() == selSouthPanel.getButton(ConfirmPanel.A_OK)) {
				SessionManager.getAppDesktop().closeActiveWindow();
				return;
			}
		} else if (event.getTarget() == fDocumentNo.getComponent()
				&& Events.ON_OK.equals(event.getName())) {
			KeyNamePair selection = null;
			String shipNo = (String) fDocumentNo.getValue();
			if (shipNo == null || "".equals(shipNo.trim()))
				return;
			shipNo = shipNo.trim();
			for (int i = 0; i < shipmentList.size(); i++) {
				KeyNamePair pair = shipmentList.get(i);
				if (shipNo.equals(pair.getName())) {
					selection = pair;
					break;
				}
			}

			if (selection == null) {
				FDialog.error(m_WindowNo, "Shipment " + shipNo
						+ " not available to confirm");
				return;
			}
			int M_Inout_ID = selection.getKey();
			MInOut inout = new MInOut(Env.getCtx(), M_Inout_ID, null);
//			filtering is done on the query
//			if (!"DR".equals(inout.getDocStatus())
//					&& !"IN".equals(inout.getDocStatus())) {
//				FDialog.error(m_WindowNo, "Shipment " + shipNo
//						+ " not available to confirm");
//				return;
//			}
			WReceiptScannerWindow wps = new WReceiptScannerWindow(m_WindowNo, inout);
			wps.setVisible(true);
			AEnv.showWindow(wps);
			//Reload shipments
			loadReceipts();
			fDocumentNo.setValue("");
			fDocumentNo.getComponent().focus();
		} else {
			super.onEvent(event);
		}
	}

	private void updateStatus()
	{
		//TODO - update status bar to display message
		if (statusMessage.length() > 0) {
			
			FDialog.info(m_WindowNo, null, statusMessage.toString());
		}

	}
	
	public void valueChange(ValueChangeEvent evt) {
		loadReceipts();

	}
	
	private void completeReceipts() {
		StringBuffer msg = new StringBuffer();
		String trx = Trx.createTrxName();
		String status = "";
		int n = 0;
		try {
			for (int i = 0;  i < table.getRowCount(); i++)
			{
				IDColumn id = (IDColumn)table.getValueAt(i, 0);     //  ID in column 0
				//	log.fine( "Row=" + i + " - " + id);
				if (id != null && id.isSelected()) {
					int m_inout_id = id.getRecord_ID();
					MInOut inout = new MInOut(Env.getCtx(), m_inout_id, trx);
					boolean bComplete = true;

					if  (bComplete) {
						if (inout.processIt(MInOut.ACTION_Complete)) {
							if (n > 0) {
								status = status + ", ";
							}
							status = status + inout.getDocumentNo();
							n++;
							inout.save();
						}
					}
				}
			}
			DB.commit(true, trx);
			
			if (n > 0) {
				statusMessage.append("Documents Completed=" + n + " [" + status + "]<br>");
			}
		} catch (Exception e) {
			log.severe(e.getMessage());
		}

		if (msg.length() > 0) {
			
			FDialog.error(m_WindowNo, msg.toString());
		}

	}

}
