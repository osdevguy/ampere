/******************************************************************************
 * Copyright (C) 2009 Low Heng Sin                                            *
 * Copyright (C) 2009 Idalica Corporation                                     *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.compiere.apps.form;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;

import org.compiere.apps.IStatusBar;
import org.compiere.minigrid.IDColumn;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MPInstance;
import org.compiere.model.MProcess;
import org.compiere.model.MProductPO;
import org.compiere.model.MReplenishPO;
import org.compiere.model.MSysConfig;
import org.compiere.model.X_T_Replenish_PO;
import org.compiere.print.ReportEngine;
import org.compiere.process.SpecialFormReplenishReport;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

/**
 * Generate Invoice (manual) controller class
 * 
 */
public class OrderGenPO extends GenForm
{
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(OrderGenPO.class);
	//
	
	public Object 			m_M_Warehouse_ID = null;
	public Object 			m_Current_C_BPartner_ID = null;
	public Object			m_M_Product_Category_ID = null;
	
	protected MPInstance pi;
	
	private boolean checked = true;
	private String old_sql = "";
	private int m_supplyHorizon = 0;
	
	public void dynInit() throws Exception
	{
		setTitle("OrderCreationFromReplenishment");
		setReportEngineType(ReportEngine.ORDER);
		setAskPrintMsg("PrintOrder");
		
		m_supplyHorizon = MSysConfig.getIntValue("SUPPLY_HORIZON", 10, Env.getAD_Client_ID(Env.getCtx()) , Env.getAD_Org_ID(Env.getCtx()));
		pi = new MPInstance(Env.getCtx(),0, "");
		int m_AD_Process_ID = MProcess.getProcess_ID("ResetUsage", null);
		pi.setAD_Process_ID(m_AD_Process_ID);
		pi.setRecord_ID(0);
		pi.save();
	}
	
	public void configureMiniTable(IMiniTable miniTable)
	{
			
		//  create Columns
		miniTable.addColumn("T_Replenish_PO_ID");
		miniTable.addColumn("IsComp");
		miniTable.addColumn("CurrSupp");
		miniTable.addColumn("PO Supplier");
		miniTable.addColumn("Part No");
		miniTable.addColumn("Part Description");
		miniTable.addColumn("HoPOQty");
		miniTable.addColumn("QOH");
		miniTable.addColumn("QRes");
		miniTable.addColumn("QOrd");
		miniTable.addColumn("PO Qty");
		miniTable.addColumn("POPrice");
		miniTable.addColumn("ETA");
		miniTable.addColumn("Remarks");
		miniTable.addColumn("REF");
		//
		miniTable.setMultiSelection(true);
		//  set details
		//miniTable.setColumnClass(0, IDColumn.class, false, " ");
		int idx= 0;
		miniTable.setColumnClass(idx++, IDColumn.class, true, "ID");
		miniTable.setColumnClass(idx++, Boolean.class, false, "Comp");
		//miniTable.setColumnClass(idx++, Integer.class, true, Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
		miniTable.setColumnClass(idx++, String.class, true, "CurrSupp");
		miniTable.setColumnClass(idx++, String.class, true, "POSupplier");
		miniTable.setColumnClass(idx++, String.class, true, "Part No");
		miniTable.setColumnClass(idx++, String.class, true, "Part Description");
		miniTable.setColumnClass(idx++, BigDecimal.class, true, "HoPOQty");
		miniTable.setColumnClass(idx++, BigDecimal.class, true, "QOH");
		miniTable.setColumnClass(idx++, BigDecimal.class, true, "QtyRes");
		miniTable.setColumnClass(idx++, BigDecimal.class, true, "QtyOrd");
		miniTable.setColumnClass(idx++, BigDecimal.class, false, "PO Qty");
		miniTable.setColumnClass(idx++, BigDecimal.class, false, "PO Price");
		miniTable.setColumnClass(idx++, Timestamp.class, true, "ETA");
		miniTable.setColumnClass(idx++, String.class, true, "Remarks");
		miniTable.setColumnClass(idx++, String.class, true, "REF");
		//
		miniTable.autoSize();
		
	}
	
	/**
	 * Get SQL for Replenishment to be updated
	 * @return sql
	 */
	private String getReplenishSQL(int T_Replenish_PO_ID, int M_Warehouse_ID, 
			int Current_BPartner_ID, 
			int M_Product_Category_ID,
			Timestamp dateHorizon
			
			)
	{
		if (M_Warehouse_ID == 0) {
			return null;
		}
//	    StringBuffer sql = new StringBuffer(
//	            "SELECT ic.C_Order_ID, o.Name, dt.Name, DocumentNo, bp.Name, DateOrdered, TotalLines "
//	            + "FROM C_Invoice_Candidate_v ic, AD_Org o, C_BPartner bp, C_DocType dt "
//	            + "WHERE ic.AD_Org_ID=o.AD_Org_ID"
//	            + " AND ic.C_BPartner_ID=bp.C_BPartner_ID"
//	            + " AND ic.C_DocType_ID=dt.C_DocType_ID"
//	            + " AND ic.AD_Client_ID=?");

		String strDateParam = "";
		String strPOQty =  "r.poqty as poqty"; 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if (dateHorizon != null) {  //urgent orders
			strDateParam = " AND o.datepromised <= to_date('" + sdf.format(dateHorizon) + "', 'dd/MM/yyyy') ";
			strPOQty = "r.poqty - coalesce(po.qtyonpo,0) as poqty";
		}
		StringBuffer sql = new StringBuffer("select r.t_replenish_po_id, r.iscomplete, cb.name as currsupplier, b.name as posupplier, p.value as partno, p.name as partdescription \n" +
				 ", coalesce(po.qtyonpo,0) as qtyonpo \n" +
				 ", r.qtyonhand, r.qtyreserved, r.qtytoorder, " + strPOQty + ", r.poprice, r.datepromised, r.remarks \n" +
				 ", pending.ref " +
	             "from t_replenish_po r \n" +
	             "inner join m_product p on r.m_product_id=p.m_product_id \n" +
	             "inner join c_bpartner cb on r.current_c_bpartner_id=cb.c_bpartner_id \n" +
	             "left join c_bpartner b on r.c_bpartner_id=b.c_bpartner_id \n" +
	             "left join (" +
	             " select ol.m_product_id, sum(ol.qtyentered-qtydelivered) as qtyonpo" + 
	             " from c_order o inner join c_orderline ol on o.c_order_id = ol.c_order_id" + 
	             "	  where o.issotrx = 'N' AND ol.qtyentered > ol.qtydelivered AND o.docstatus IN ('CO')" + strDateParam +
	             "		  GROUP BY ol.m_product_id" +
	             " 		) po ON po.m_product_id = r.m_product_id " + 
	             "left join (" +
	             "		  select xol.m_product_id, array_agg(xo.documentno || ' ' || to_char(xo.datepromised, 'dd/MM/yyyy') || ' qty=' || xol.qtyentered-xol.qtydelivered) as ref" +
	             "		  From c_order xo inner join c_orderline xol on xo.c_order_id = xol.c_order_id " +
	             "		  where xo.issotrx = 'N' AND xol.qtyentered > xol.qtydelivered AND xo.docstatus IN ('CO') " + 
	             "		  GROUP BY xol.m_product_id " +
	             "		) pending ON pending.m_product_id = r.m_product_id " + 
	             //"WHERE r.ad_pinstance_id=? and r.processed='N' AND r.poqty - coalesce(po.qtyonpo,0)  > 0");
    			 "WHERE r.ad_pinstance_id=? and r.processed='N' AND r.poqty  > 0");
		
//	    if (M_Warehouse_ID != 0 || Current_BPartner_ID != 0 || M_Product_Category_ID != 0) {
//	    	sql.append(" WHERE ");
//	    }

        if (T_Replenish_PO_ID != 0)
            sql.append(" AND r.T_Replenish_PO_ID=").append(T_Replenish_PO_ID);
        if (M_Warehouse_ID != 0)
            sql.append(" AND r.M_Warehouse_ID=").append(M_Warehouse_ID);
        if (Current_BPartner_ID != 0)
            sql.append(" AND r.Current_C_BPartner_ID=").append(Current_BPartner_ID);
        if (M_Product_Category_ID != 0)
            sql.append(" AND p.M_Product_Category_ID=").append(M_Product_Category_ID);

        //
        sql.append(" ORDER BY p.Name");
        
        return sql.toString();
	}
	
	
	public void createReplenishment(int M_Warehouse_ID, int AD_PInstance_ID, boolean isUrgent, int priority)
	{
		if (M_Warehouse_ID == -1)
		{
			return;
		}
			
		SpecialFormReplenishReport process = new SpecialFormReplenishReport();
		
		process.createReplenishment(M_Warehouse_ID, AD_PInstance_ID, isUrgent, priority);
		
	}
	
	public int addProducttoReplenishment(int M_Product_ID, int M_Warehouse_ID, int AD_PInstance_ID) 
	{
		if (M_Product_ID <= 0) {
			return 0;
		}
		
		if (M_Warehouse_ID <= 0) {
			return 0;
		}
		String trxName = Trx.createTrxName("poadd");
		Trx trx = Trx.get(trxName, true);	//trx needs to be committed too
		
		MReplenishPO po = new MReplenishPO(Env.getCtx(), 0, trxName);
		MProductPO[] ppos = MProductPO.getOfProduct(Env.getCtx(), M_Product_ID, trxName);
		MProductPO productPO = null;
		for (MProductPO ppo : ppos) {
			if  (ppo.isCurrentVendor()) {
				productPO = ppo;
				break;
			}
		}
		po.setAD_PInstance_ID(AD_PInstance_ID);
		po.setC_BPartner_ID(productPO.getC_BPartner_ID());
		po.setCurrent_C_BPartner_ID(po.getC_BPartner_ID());
		po.setM_Product_ID(M_Product_ID);
		po.setM_Warehouse_ID(M_Warehouse_ID);
		po.setPOPrice(BigDecimal.ZERO);
		po.setPOQty(BigDecimal.ZERO);
		po.setLevel_Min(BigDecimal.ZERO);
		po.setLevel_Max(BigDecimal.ZERO);
		po.setOrder_Min(BigDecimal.ZERO);
		po.setOrder_Pack(BigDecimal.ZERO);
		po.setIsComplete(false);
		//po.setIsActive(true);
		po.setReplenishType("3");
		po.save();
		trx.commit();
		return po.getT_Replenish_PO_ID();
	}
	/**
	 *  Query Info
	 */
	public void executeQuery(int T_Replenish_PO_ID, int M_Warehouse_ID, 
			int Current_BPartner_ID, 
			int M_Product_Category_ID, 
			Timestamp dateHorizon, 
			IMiniTable miniTable)
	{
		log.info("");
		
		String sql = getReplenishSQL(T_Replenish_PO_ID, M_Warehouse_ID, Current_BPartner_ID,M_Product_Category_ID, dateHorizon);

		int row = 0;
		
		if (M_Warehouse_ID == 0)
		{
			miniTable.setRowCount(row);
			miniTable.autoSize();
			return;
		}
			
		if (T_Replenish_PO_ID != 0) {
			row = miniTable.getRowCount();
		}
		else {
			//  reset table
			miniTable.setRowCount(row);
		}
		//  Execute
		try
		{
			PreparedStatement pstmt = DB.prepareStatement(sql.toString(), null);
			pstmt.setInt(1, pi.getAD_PInstance_ID());
			ResultSet rs = pstmt.executeQuery();
			//
			while (rs.next())
			{
				//  extend table
				miniTable.setRowCount(row+1);
				//  set values
				setRowValue(rs, row, miniTable);
				//  prepare next
				row++;
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		//
		miniTable.autoSize();
	//TODO  set status bar	
	//	statusBar.setStatusDB(String.valueOf(miniTable.getRowCount()));
	}   //  executeQuery
	
	private void setRowValue(ResultSet rs, int row, IMiniTable miniTable)
	{
		int idx=0;
		int col=1;
		try {
			miniTable.setValueAt(new IDColumn(rs.getInt(col++)), row, idx++);   //  t_replenish_po_id
			miniTable.setValueAt(rs.getString(col++).equals("Y") ? true : false, row, idx++);              //  iscomplete
			miniTable.setValueAt(rs.getString(col++), row, idx++);              //  Curr Supplier
			miniTable.setValueAt(rs.getString(col++), row, idx++);              //  PO Supplier
			miniTable.setValueAt(rs.getString(col++), row, idx++);              //  Part No
			miniTable.setValueAt(rs.getString(col++), row, idx++);              //  Part Description
			miniTable.setValueAt(rs.getBigDecimal(col++), row, idx++);          //  Qty on PO
			miniTable.setValueAt(rs.getBigDecimal(col++), row, idx++);          //  Qty on Hand
			miniTable.setValueAt(rs.getBigDecimal(col++), row, idx++);          //  Qty Reserved
			miniTable.setValueAt(rs.getBigDecimal(col++), row, idx++);          //  Qty to Order
			miniTable.setValueAt(rs.getBigDecimal(col++), row, idx++);          //  PO Qty
			miniTable.setValueAt(rs.getBigDecimal(col++), row, idx++);          //  PO Price
			miniTable.setValueAt(rs.getTimestamp(col++), row, idx++);          //  Date Promised
			miniTable.setValueAt(rs.getString(col++), row, idx++);          //  Remarks
			miniTable.setValueAt(rs.getString(col++), row, idx++);          //  REF
		}
		catch (SQLException e) {
			log.severe(e.getMessage());
		}
	}
	
	
	public void saveUpdates(IMiniTable miniTable) {
		int rows = miniTable.getRowCount();
		for (int i = 0; i < rows; i++)
		{
			saveRow(miniTable, i);
		}
		
	}
	
	public void saveRow(IMiniTable miniTable, int row)
	{
		IDColumn id = (IDColumn)miniTable.getValueAt(row, 0);     //  ID in column 0
		X_T_Replenish_PO rep = new X_T_Replenish_PO(Env.getCtx(), id.getRecord_ID(), null);
		rep.setIsComplete((Boolean)miniTable.getValueAt(row, 1)); //iscomplete
		rep.setPOQty((BigDecimal)miniTable.getValueAt(row, 10));  //po qty 
		rep.setPOPrice((BigDecimal)miniTable.getValueAt(row, 11));  //po price
		rep.saveEx();
		
	}
	
	public void updateETA(IMiniTable miniTable, Timestamp ETA) {
		int rows = miniTable.getRowCount();
		for (int i = 0; i < rows; i++) {
			IDColumn id = (IDColumn) miniTable.getValueAt(i, 0); // ID in column
																	// 0
			// log.fine( "Row=" + i + " - " + id);
			if (id != null && id.isSelected()) {
				X_T_Replenish_PO rep = new X_T_Replenish_PO(Env.getCtx(), id.getRecord_ID(), null);
				rep.setDatePromised(ETA);
				rep.saveEx();
				//save on the grid
				miniTable.setValueAt(ETA, i, 12);  //datepromised
			}
		}

	}

	public void updatePOSupplier(IMiniTable miniTable, int C_BPartner_ID, String vendorName) {
		int rows = miniTable.getRowCount();
		for (int i = 0; i < rows; i++) {
			IDColumn id = (IDColumn) miniTable.getValueAt(i, 0); // ID in column
																	// 0
			// log.fine( "Row=" + i + " - " + id);
			if (id != null && id.isSelected()) {
				X_T_Replenish_PO rep = new X_T_Replenish_PO(Env.getCtx(), id.getRecord_ID(), null);
				rep.setC_BPartner_ID(C_BPartner_ID);
				rep.saveEx();
				miniTable.setValueAt(vendorName, i, 3);  //PO Supplier
				
			}
		}

	}
	
	public void checkAll(IMiniTable miniTable) {
	
		int rows = miniTable.getRowCount();
		for (int i = 0; i < rows; i++) {
			IDColumn id = (IDColumn) miniTable.getValueAt(i, 0); // ID in column
			id.setSelected(checked);
			miniTable.setValueAt(id, i, 0);
		}
		
		checked = !checked;
	}
	/**
	 *	Save Selection & return selecion Query or ""
	 *  @return where clause like C_Order_ID IN (...)
	 */
	public void saveSelection(IMiniTable miniTable)
	{
		log.info("");
		//  Array of Integers
		ArrayList<Integer> results = new ArrayList<Integer>();
		setSelection(null);

		//	Get selected entries
		int rows = miniTable.getRowCount();
		for (int i = 0; i < rows; i++)
		{
			IDColumn id = (IDColumn)miniTable.getValueAt(i, 0);     //  ID in column 0
		//	log.fine( "Row=" + i + " - " + id);
			if (id != null && id.isSelected())
				results.add(id.getRecord_ID());
		}

		if (results.size() == 0)
			return;
		log.config("Selected #" + results.size());
		setSelection(results);
	}	//	saveSelection

	
	/**************************************************************************
	 *	Generate Invoices
	 */
	public String generate(IStatusBar statusBar)
	{
		String info = "";
		
		info = SpecialFormReplenishReport.createPO(pi.getAD_PInstance_ID());
		statusBar.setStatusLine(info);
		//statusBar.setStatusDB(String.valueOf(getSelection().size()));

		/*		String trxName = Trx.createTrxName("IVG");
		Trx trx = Trx.get(trxName, true);	//trx needs to be committed too
		
		setSelectionActive(false);  //  prevents from being called twice
		statusBar.setStatusLine(Msg.getMsg(Env.getCtx(), "InvGenerateGen"));
		statusBar.setStatusDB(String.valueOf(getSelection().size()));

		//	Prepare Process
		int AD_Process_ID = 0;
        
        if (docTypeKNPair.getKey() == MRMA.Table_ID)
        {
            AD_Process_ID = 52002; // C_Invoice_GenerateRMA - org.adempiere.process.InvoiceGenerateRMA
        }
        else
        {
            AD_Process_ID = 134;  // HARDCODED    C_InvoiceCreate
        }
		MPInstance instance = new MPInstance(Env.getCtx(), AD_Process_ID, 0);
		if (!instance.save())
		{
			info = Msg.getMsg(Env.getCtx(), "ProcessNoInstance");
			return info;
		}
		
		//insert selection
		StringBuffer insert = new StringBuffer();
		insert.append("INSERT INTO T_SELECTION(AD_PINSTANCE_ID, T_SELECTION_ID) ");
		int counter = 0;
		for(Integer selectedId : getSelection())
		{
			counter++;
			if (counter > 1)
				insert.append(" UNION ");
			insert.append("SELECT ");
			insert.append(instance.getAD_PInstance_ID());
			insert.append(", ");
			insert.append(selectedId);
			insert.append(" FROM DUAL ");
			
			if (counter == 1000) 
			{
				if ( DB.executeUpdate(insert.toString(), trxName) < 0 )
				{
					String msg = "No Invoices";     //  not translated!
					info = msg;
					log.config(msg);
					trx.rollback();
					return info;
				}
				insert = new StringBuffer();
				insert.append("INSERT INTO T_SELECTION(AD_PINSTANCE_ID, T_SELECTION_ID) ");
				counter = 0;
			}
		}
		if (counter > 0)
		{
			if ( DB.executeUpdate(insert.toString(), trxName) < 0 )
			{
				String msg = "No Invoices";     //  not translated!
				info = msg;
				log.config(msg);
				trx.rollback();
				return info;
			}
		}
		
		ProcessInfo pi = new ProcessInfo ("", AD_Process_ID);
		pi.setAD_PInstance_ID (instance.getAD_PInstance_ID());

		//	Add Parameters
		MPInstancePara para = new MPInstancePara(instance, 10);
		para.setParameter("Selection", "Y");
		if (!para.save())
		{
			String msg = "No Selection Parameter added";  //  not translated
			info = msg;
			log.log(Level.SEVERE, msg);
			return info;
		}
		
		para = new MPInstancePara(instance, 20);
		para.setParameter("DocAction", docActionSelected);
		
		if (!para.save())
		{
			String msg = "No DocAction Parameter added";  //  not translated
			info = msg;
			log.log(Level.SEVERE, msg);
			return info;
		}
		
		setTrx(trx);
		setProcessInfo(pi);
		*/
		return info;
	}	//	generateInvoices

	public int getM_supplyHorizon() {
		return m_supplyHorizon;
	}

	public void setM_supplyHorizon(int m_supplyHorizon) {
		this.m_supplyHorizon = m_supplyHorizon;
	}
}