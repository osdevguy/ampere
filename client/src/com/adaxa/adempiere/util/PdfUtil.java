package com.adaxa.adempiere.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

public class PdfUtil {
	
	/**
	 * merge more pdfs in one pdf file.
	 * 
	 * @param pdfList list of pdf
	 * @param outFile output file
	 * @throws IOException
	 * @throws DocumentException
	 * @throws FileNotFoundException
	 */
	public static void mergePdf(List<File> pdfList, File outFile)
			throws IOException, DocumentException, FileNotFoundException {
		Document document = null;
		PdfWriter copy = null;
		for (File f : pdfList) {
			PdfReader reader = new PdfReader(f.getAbsolutePath());
			if (document == null) {
				document = new Document(reader.getPageSizeWithRotation(1));
				copy = PdfWriter.getInstance(document, new FileOutputStream(
						outFile));
				document.open();
			}
			int pages = reader.getNumberOfPages();
			PdfContentByte cb = copy.getDirectContent();
			for (int i = 1; i <= pages; i++) {
				document.newPage();
				PdfImportedPage page = copy.getImportedPage(reader, i);
				cb.addTemplate(page, 0, 0);
			}
		}
		document.close();
	}  // mergePdf

}
