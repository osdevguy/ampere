package org.adempiere.process;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.compiere.model.MBPGroup;
import org.compiere.model.MBPartner;
import org.compiere.model.MMRPRun;
import org.compiere.model.MOrder;
import org.compiere.model.MOrg;
import org.compiere.model.MOrgInfo;
import org.compiere.model.MProduct;
import org.compiere.model.MProduction;
import org.compiere.model.MRequisition;
import org.compiere.model.X_MRP_Run;
import org.compiere.model.X_MRP_RunLine;
import org.compiere.process.ProcessInfoLog;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.TimeUtil;
import org.compiere.util.Trx;

import com.mchange.v2.c3p0.C3P0ProxyConnection;

/**
 * CalculateMiniMRP.Java
 * 
 * @author hitesh.panchani, www.logilite.com
 * @author <a href="mailto:scn.bhimani12@gmail.com"> Sachin D Bhimani </a>
 */
public class CalculateMiniMRP extends SvrProcess
{
	public static final String			TYPE_CLOSING_BALANCE_LINE				= "Closing Balance";
	public static final String			TYPE_OPENING_BALANCE_LINE				= "Opening Balance";
	private static final String			TYPE_SUPPLY_LINE_PO						= "Supply - Purchasing";
	private static final String			TYPE_SUPPLY_LINE_RQ						= "Supply - Requisition";
	private static final String			TYPE_SUPPLY_LINE_KB						= "Supply - Kanban";	
	private static final String			TYPE_SUPPLY_LINE_MO_PLANNED				= "Planned Production";
	private static final String			TYPE_SUPPLY_LINE_MO_NONPLANNED			= "Confirmed Production";
	private static final String			TYPE_TOTAL_DEMAND_LINE					= "Total Demand";
	private static final String			TYPE_TOTAL_SUPPLY_LINE					= "Total Supply";
	private static final String			TYPE_TOTAL_SUPPLY_LINE_PO				= "Total Supply - PO";
	private static final String			TYPE_TOTAL_SUPPLY_LINE_RQ				= "Total Supply - Requisition";
	private static final String			TYPE_TOTAL_SUPPLY_LINE_KB				= "Total Supply - Kanban";
	private static final String			TYPE_TOTAL_SUPPLY_PLANNED_LINE			= "Total Planned Production";
	private static final String			TYPE_TOTAL_SUPPLY_NONPLANNED_LINE		= "Total Confirmed Production";
	private static final String			TYPE_PRODUCT_ORDER_DEMAND				= "Demand";
	private static final String			REPLENISH_TYPE_MRP_CALCULATED			= "4";
	private static final String			TYPE_RQ									= "RQ";
	private static final String			TYPE_PO									= "PO";
	private static final String			TYPE_MO									= "MO";
	private int							lineNo									= 10;
	int									countProd								= 0;
	int									countReq								= 0;
	private int							mrpRunID								= 0;
	private int							weekRound								= 1;
	private int							START_WEEK;
	private int							END_WEEK;																																											// Week
	private int							AD_Client_ID;
	private int							AD_Org_ID;
	private int							AD_User_ID;
	private int							M_WarehouseID;
	private int							M_LocatorID;
	private int							docType_PlannedOrder;
	private int							docType_ConfirmedOrder;
	private int							docType_PurchaseOrder;
	private int							docType_MRPRequisition;
	private int							M_PriceList_ID;
	private Timestamp					dateFrom;
	private Timestamp					dateTo;
	private Calendar					calendar								= Calendar.getInstance();

	private Properties					ctx;
	private String						trx;

	private boolean						isExclKanbanPrd;
	private String						aggrReqBPG_CV							= "";
	private String						aggrReqByPeriod							= "";

	// Available Inventory - ProductID, Qty
	private Map<Integer, BigDecimal>	availableInventory						= new TreeMap<Integer, BigDecimal>();

	// Map Demand for SalesOrder_DatePromise, ProductID, RequiredQTY.
	Map<Date, Map<Integer, BigDecimal>>	mapDemand								= new TreeMap<Date, Map<Integer, BigDecimal>>();

	// map use for DatePromise for create planned Production, Product reference,
	// planned qty [ProductID, RequiredDate, DemandQty]
	Map<Integer, Map<Date, BigDecimal>>	mapRequirePlanningQty					= new TreeMap<Integer, Map<Date, BigDecimal>>();

	// Map for exclude kanban products to prevent creating requisition lines.
	// ProductID, Date, Kanban
	Map<Integer, Map<Date, KanbanProduct>>	mapKanbanProduct					= new TreeMap<Integer, Map<Date, KanbanProduct>>();

	StringBuilder						infoMsg									= new StringBuilder();
	StringBuilder						productionDocs							= new StringBuilder();
	StringBuilder						requisitionDocs							= new StringBuilder();

	private static String				SQL_PRODUCTWISE_CONFIRM_PRODUCTION_QTY	= " SELECT ADDDAYS(?::DATE, (CASE WHEN DatePromised < ? THEN 0 ELSE (DAYSBETWEEN(DatePromised, ?) / 7 / ?) * ? * 7 END)) AS DatePromised, "
																						+ " ProductionQty AS Qty, M_Production_ID FROM M_Production "
																						+ " WHERE Processed = 'N' AND M_Product_ID = ? AND DatePromised <= ? AND (C_DocType_ID IS NULL OR C_DocType_ID != ? )"
																						+ " ORDER BY DatePromised ";

	private static String				SQL_PRODUCTWISE_CONFIRM_PO_RQ_QTY		= " SELECT ADDDAYS(?::DATE, (CASE WHEN DateRequired < ? THEN 0 ELSE (DAYSBETWEEN(DateRequired, ?) / 7 / ?) * ? * 7 END)) AS DatePromised, "
																						+ " SUM(Qty) AS Qty FROM (( "
																						+ " SELECT ol.DatePromised AS DateRequired, ol.QtyOrdered - QtyDelivered AS Qty	FROM C_Order o "
																						+ " INNER JOIN C_OrderLine ol ON (ol.C_Order_ID = o.C_Order_ID AND ol.M_Product_ID = ?) "
																						+ " WHERE o.DocStatus IN ('CO', 'CL') AND o.IsSoTrx = 'N' AND (ol.QtyOrdered - ol.QtyDelivered) != 0 AND o.DatePromised <= ? "
																						+ " ORDER BY o.DatePromised )		UNION ALL	( SELECT r.DateRequired, SUM(rl.Qty) AS QtyOrdered	FROM M_Requisition r "
																						+ " INNER JOIN M_RequisitionLine rl ON (r.M_Requisition_ID = rl.M_Requisition_ID AND rl.IsActive = 'Y' AND rl.M_Product_ID = ?) "
																						+ " INNER JOIN M_Product mp ON (mp.M_Product_ID = rl.M_Product_ID AND mp.IsActive = 'Y') "
																						+ " WHERE r.DocStatus IN ('DR') AND r.DateRequired <= ? AND mp.AD_Client_ID = ? AND r.C_DocType_ID = ? "
																						+ " GROUP BY mp.M_Product_ID, r.M_Requisition_ID	ORDER BY mp.M_Product_ID, r.DateRequired )) AS qtyOrdered	GROUP BY DateRequired ";

	private static String				SQL_GET_PRODUCTION_SUBPRODUCTWISE		= "SELECT DISTINCT p.M_Production_ID		FROM M_Production p "
																						+ " INNER JOIN M_ProductionLine pl ON (pl.M_Production_ID = p.M_Production_ID AND pl.IsEndProduct = 'N' AND pl.M_Product_ID = ?) "
																						+ " WHERE p.Processed = 'N' AND p.DatePromised <= ? AND (C_DocType_ID IS NULL OR C_DocType_ID != ?) ";

	public static String				SQL_GET_ISO_WEEKNO						= "SELECT ( CASE WHEN TO_CHAR(?::DATE, 'IYYY') <> TO_CHAR(?::DATE, 'IYYY') "
																						+ "	THEN EXTRACT(WEEK FROM (DATE_TRUNC('YEAR', ?::DATE) + interval '-1 day')) "
																						+ " ELSE 0 END )	+	EXTRACT(WEEK FROM ?::DATE) ";

	public static String				SQL_GET_PRODUCTIONLINE_INFO				= "SELECT p.M_Product_ID, SUM(QtyUsed) AS QtyUsed, mp.DatePromised	FROM M_Production mp "
																						+ " INNER JOIN M_ProductionLine pl ON (pl.M_Production_ID = mp.M_Production_ID AND pl.IsEndProduct = 'N') "
																						+ " INNER JOIN M_Product p ON (p.M_Product_ID = pl.M_Product_ID AND p.IsPhantom = 'N') "
																						+ " WHERE pl.IsActive='Y' AND pl.M_Production_ID = ? "
																						+ " GROUP BY mp.M_Production_ID, p.M_Product_ID	ORDER BY p.M_Product_ID ";
	
	public static String				SQL_PRODUCTWISE_INFO_FROM_PRODUCTION	= "SELECT p.M_Production_ID, p.DocumentNo, CASE WHEN p.DatePromised < ? THEN ? ELSE p.DatePromised END AS DatePromised, SUM(QtyUsed) AS QtyUsed	FROM M_Production p "
																						+ " INNER JOIN M_ProductionLine pl ON (pl.M_Production_ID = p.M_Production_ID AND pl.IsEndProduct = 'N' AND pl.M_Product_ID = ?) "
																						+ " WHERE p.Processed='N' AND p.C_DocType_ID=? AND DatePromised <= ? "
																						+ " GROUP BY p.M_Production_ID	ORDER BY p.DatePromised ";

	public static String				SQL_GET_BOMLINE_FOR_PROCESS				= "SELECT pb.M_ProductBOM_ID, pb.BomQTY, p.M_Product_Category_ID, p.Name AS ProductName, p.IsBOM, p.IsVerified, p.IsPurchased, "
																						+ "		p.IsPhantom, mpo.C_BPartner_ID, COALESCE(ppr.pricelist,  0.00) AS PricePO, mpo.DeliveryTime_Promised, "
																						+ "		COALESCE(r.QtyBatchSize, 0) AS QtyBatchSize, COALESCE(r.Level_Min, 0) AS Level_Min, r.ReplenishType, SUM(ms.qtyonhand) AS Available, "
																						+ " 	bp.C_BP_Group_ID, p.IsKanban, COALESCE(bp.PO_PriceList_ID, ?) AS PO_PriceList_ID "
																						+ " FROM M_Product_BOM pb "
																						+ " INNER JOIN M_Product p ON (p.M_Product_ID = pb.M_ProductBOM_ID AND pb.M_Product_ID = ? AND p.IsActive = 'Y' AND p.AD_Client_ID = ?) "
																						+ " INNER JOIN M_Replenish r	ON (r.M_Product_ID = p.M_Product_ID AND r.M_Warehouse_ID = ?)  "
																						+ " LEFT JOIN M_Product_PO mpo ON (mpo.M_Product_ID = p.M_Product_ID AND mpo.C_BPartner_ID = "
																						+ "		(SELECT po.C_BPartner_ID FROM M_Product_PO po "
																						+ " WHERE (po.M_Product_ID = p.M_Product_ID) ORDER BY IsCurrentVendor DESC  FETCH FIRST ROW ONLY)) "
																						+ " LEFT JOIN C_BPartner bp ON (bp.C_BPartner_ID = mpo.C_BPartner_ID) "
																						+ " LEFT JOIN M_ProductPrice ppr ON  (p.m_product_id = ppr.m_product_id)  AND  ppr.m_pricelist_version_id = "
																						+ " (SELECT plv.m_pricelist_version_id FROM m_pricelist_version plv "
																					    + " LEFT JOIN c_bpartner bpx ON plv.m_pricelist_id = bpx.po_pricelist_id "
																					    + " WHERE (bpx.c_bpartner_id = mpo.c_bpartner_id AND plv.ValidFrom <= now())  ORDER BY plv.m_pricelist_version_id DESC FETCH FIRST ROW ONLY) "
																					    + " LEFT OUTER JOIN M_Storage ms ON (ms.M_Product_ID = p.M_Product_ID) "
																						+ " GROUP BY	pb.M_Product_ID, pb.M_ProductBOM_ID, pb.BomQTY, p.M_Product_ID, "
																						+ "		mpo.C_BPartner_ID, bp.C_BPartner_ID, mpo.PricePO, mpo.DeliveryTime_Promised, r.M_Product_ID, r.M_Warehouse_ID, ppr.pricelist "
																						+ " ORDER BY pb.M_ProductBOM_ID ";

	public static final String			SQL_GET_MRP_RUN_CAPACITY_SUMMARY		= "INSERT INTO T_MRP_RUN_Capacity_Summary (T_MRP_RUN_Capacity_Summary_ID, AD_Client_ID, AD_Org_ID, IsActive, Created, Updated, CreatedBy, UpdatedBy, "
																						+ " 	MRP_Run_ID, M_WorkCentre_ID, M_Product_ID, DateStart, Week, SuggestedQty, ConfirmedQty, TotalDemandQty, "
																						+ " 	CapacityQty, SurplusShortageQty, CumulatedDemand, CumulatedCapacity, RemainingCumulativeCapacity) "
																						+ " SELECT 	NEXTVAL('t_mrp_run_capacity_summary_seq'),AD_Client_ID, AD_Org_ID, IsActive, Created, Updated, CreatedBy, UpdatedBy,  "
																						+ " 	MRP_Run_ID, M_WorkCentre_ID, M_Product_ID, DateStart, Week, SuggestedQty, ConfirmedQty, TotalDemandQty, "
																						+ " 	CapacityQty, SurplusShortageQty, CumulatedDemand, CumulatedCapacity, RemainingCumulativeCapacity "
																						+ " FROM ( "
																						+ " 	SELECT	w.AD_Client_ID, w.AD_Org_ID, w.IsActive, w.Created, w.Updated, w.CreatedBy, w.UpdatedBy, s2.MRP_Run_ID, s2.M_WorkCentre_ID, "
																						+ " 		s2.M_Product_ID, s2.DateStart, COALESCE(w.CapacityInTimeUOM, 1) AS CapacityQty, "
																						+ " 		s2.MPO AS SuggestedQty, s2.MOP AS ConfirmedQty,	s2.MPO + s2.MOP AS TotalDemandQty, "
																						+ " 		COALESCE(w.CapacityInTimeUOM, 1) - s2.MPO - s2.MOP AS SurplusShortageQty, "
																						+ " 		SUM(COALESCE(w.CapacityInTimeUOM, 1)) OVER (PARTITION BY s2.M_WorkCentre_ID, s2.M_Product_ID ORDER BY s2.DateStart) AS CumulatedCapacity, "
																						+ " 		SUM(s2.MPO + s2.MOP) OVER (PARTITION BY s2.M_WorkCentre_ID, s2.M_Product_ID ORDER BY s2.DateStart) AS CumulatedDemand, "
																						+ " 		SUM(COALESCE(w.CapacityInTimeUOM, 1) - s2.MPO - s2.MOP) OVER (PARTITION BY s2.M_WorkCentre_ID, s2.M_Product_ID ORDER BY s2.DateStart) AS RemainingCumulativeCapacity, "
																						+ " 		Week "
																						+ " 	FROM ( "
																						+ " 		SELECT 	ss.MRP_Run_ID, ss.M_WorkCentre_ID, ss.M_Product_ID, ss.DateStart, ss.week, "
																						+ " 			SUM(CASE WHEN ss.DocBaseType = 'MPO'::bpchar THEN ss.qty ELSE 0 END) AS MPO, "
																						+ " 			SUM(CASE WHEN ss.DocBaseType = 'MOP'::bpchar THEN ss.qty ELSE 0 END) AS MOP "
																						+ " 		FROM ( "
																						+ " 			SELECT 	rl.MRP_Run_ID, p.M_WorkCentre_ID, p.M_Product_ID, "
																						+ " 				unnest(ARRAY['week1', 'week2', 'week3', 'week4', 'week5', 'week6', 'week7', 'week8', 'week9', 'week10', 'week11', 'week12', 'week13', 'week14', 'week15', 'week16', 'week17', 'week18', 'week19', 'week20', 'week21', 'week22', 'week23', 'week24']) AS week, "
																						+ " 				adddays(rl.DateStart::timestamp with time zone, ((replace(unnest(ARRAY['week1', 'week2', 'week3', 'week4', 'week5', 'week6', 'week7', 'week8', 'week9', 'week10', 'week11', 'week12', 'week13', 'week14', 'week15', 'week16', 'week17', 'week18', 'week19', 'week20', 'week21', 'week22', 'week23', 'week24']), 'week', '')::integer - 1) * 7)::numeric) AS DateStart, "
																						+ " 				unnest(ARRAY[rl.week1, rl.week2, rl.week3, rl.week4, rl.week5, rl.week6, rl.week7, rl.week8, rl.week9, rl.week10, rl.week11, rl.week12, rl.week13, rl.week14, rl.week15, rl.week16, rl.week17, rl.week18, rl.week19, rl.week20, rl.week21, rl.week22, rl.week23, rl.week24]) AS Qty, "
																						+ " 				(SELECT dt.DocBaseType FROM M_Production pp JOIN C_DocType dt ON pp.C_DocType_ID = dt.C_DocType_ID WHERE rl.M_Production_ID = pp.M_Production_ID) AS DocBaseType "
																						+ " 			FROM MRP_RunLine rl "
																						+ " 			INNER JOIN M_Product p ON rl.M_Product_ID = p.M_Product_ID "
																						+ " 			WHERE p.M_WorkCentre_ID IS NOT NULL AND rl.recordtype = 'Demand' "
																						+ " 		) ss "
																						+ " 		GROUP BY ss.MRP_Run_ID, ss.M_WorkCentre_ID, ss.M_Product_ID, ss.DateStart, ss.week "
																						+ " 		ORDER BY ss.DateStart "
																						+ " 	) s2 "
																						+ " 	JOIN M_WorkCentre w ON s2.M_WorkCentre_ID = w.M_WorkCentre_ID "
																						+ " 	GROUP BY s2.MRP_Run_ID, w.M_WorkCentre_ID, s2.M_WorkCentre_ID, s2.DateStart, s2.MPO, s2.MOP, s2.Week, s2.M_Product_ID "
																						+ " UNION ALL "
																						+ " 	SELECT	w.AD_Client_ID, w.AD_Org_ID, w.IsActive, w.Created, w.Updated, w.CreatedBy, w.UpdatedBy, s2.MRP_Run_ID, s2.M_WorkCentre_ID,  "
																						+ " 		0 AS M_Product_ID, s2.DateStart, COALESCE(w.CapacityInTimeUOM, 1) AS CapacityQty,  "
																						+ " 		s2.MPO AS SuggestedQty, s2.MOP AS ConfirmedQty,	s2.MPO + s2.MOP AS TotalDemandQty, "
																						+ " 		COALESCE(w.CapacityInTimeUOM, 1) - s2.MPO - s2.MOP AS SurplusShortageQty, "
																						+ " 		SUM(COALESCE(w.CapacityInTimeUOM, 1)) OVER (PARTITION BY s2.M_WorkCentre_ID ORDER BY s2.DateStart) AS CumulatedCapacity, "
																						+ " 		SUM(s2.MPO + s2.MOP) OVER (PARTITION BY s2.M_WorkCentre_ID ORDER BY s2.DateStart) AS CumulatedDemand, "
																						+ " 		SUM(COALESCE(w.CapacityInTimeUOM, 1) - s2.MPO - s2.MOP) OVER (PARTITION BY s2.M_WorkCentre_ID ORDER BY s2.DateStart) AS RemainingCumulativeCapacity, "
																						+ " 		Week "
																						+ " 	FROM ( "
																						+ " 		SELECT 	ss.MRP_Run_ID, ss.M_WorkCentre_ID, ss.DateStart, ss.week, "
																						+ " 			SUM(CASE WHEN ss.DocBaseType = 'MPO'::bpchar THEN ss.qty ELSE 0 END) AS MPO, "
																						+ " 			SUM(CASE WHEN ss.DocBaseType = 'MOP'::bpchar THEN ss.qty ELSE 0 END) AS MOP "
																						+ " 		FROM ( "
																						+ " 			SELECT 	rl.MRP_Run_ID, p.M_WorkCentre_ID, "
																						+ " 				unnest(ARRAY['week1', 'week2', 'week3', 'week4', 'week5', 'week6', 'week7', 'week8', 'week9', 'week10', 'week11', 'week12', 'week13', 'week14', 'week15', 'week16', 'week17', 'week18', 'week19', 'week20', 'week21', 'week22', 'week23', 'week24']) AS week, "
																						+ " 				adddays(rl.DateStart::timestamp with time zone, ((replace(unnest(ARRAY['week1', 'week2', 'week3', 'week4', 'week5', 'week6', 'week7', 'week8', 'week9', 'week10', 'week11', 'week12', 'week13', 'week14', 'week15', 'week16', 'week17', 'week18', 'week19', 'week20', 'week21', 'week22', 'week23', 'week24']), 'week', '')::integer - 1) * 7)::numeric) AS DateStart, "
																						+ " 				unnest(ARRAY[rl.week1, rl.week2, rl.week3, rl.week4, rl.week5, rl.week6, rl.week7, rl.week8, rl.week9, rl.week10, rl.week11, rl.week12, rl.week13, rl.week14, rl.week15, rl.week16, rl.week17, rl.week18, rl.week19, rl.week20, rl.week21, rl.week22, rl.week23, rl.week24]) AS Qty, "
																						+ " 				(SELECT dt.DocBaseType FROM M_Production pp JOIN C_DocType dt ON pp.C_DocType_ID = dt.C_DocType_ID WHERE rl.M_Production_ID = pp.M_Production_ID) AS DocBaseType "
																						+ " 			FROM MRP_RunLine rl "
																						+ " 			INNER JOIN M_Product p ON rl.M_Product_ID = p.M_Product_ID "
																						+ " 			WHERE p.M_WorkCentre_ID IS NOT NULL AND rl.recordtype = 'Demand' "
																						+ " 		) ss "
																						+ "			WHERE ss.DateStart <= ? "
																						+ " 		GROUP BY ss.MRP_Run_ID, ss.M_WorkCentre_ID, ss.DateStart, ss.Week "
																						+ " 	ORDER BY ss.DateStart "
																						+ " 	) s2 "
																						+ " 	JOIN M_WorkCentre w ON s2.M_WorkCentre_ID = w.M_WorkCentre_ID "
																						+ " ) MyData ";
			
	SimpleDateFormat					requiredFormat							= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat					sdf										= new SimpleDateFormat("HH:mm:ss.SSS");

	// RequiredDate, AggrID, Requisition
	Map<Date, Map<Integer, MRequisition>>	mapRequisition						= new TreeMap<Date, Map<Integer, MRequisition>>();

	@Override
	protected void prepare()
	{
		printlog("Process Prepare : Start:" + sdf.format(new Date()));
		ctx = getCtx();
		trx = get_TrxName();
		mrpRunID = getRecord_ID();
		AD_User_ID = Env.getAD_User_ID(ctx);
	}

	@Override
	protected String doIt() throws Exception
	{
		Timestamp t1 = new Timestamp(System.currentTimeMillis());
		log.fine("Start DoIt: " + sdf.format(t1));

		if (mrpRunID == 0) {
			return "MRP ID is 0. Invalid state.";
		}
		MMRPRun run = new MMRPRun(ctx, mrpRunID, null);
		isExclKanbanPrd = run.isExcludeKanbanProduct();
		
		if(isExclKanbanPrd)
		{
			aggrReqBPG_CV = run.getAggregateRequisition();
			aggrReqByPeriod = run.getAggregateReqByPeriod();

			if (aggrReqByPeriod.equals(X_MRP_Run.AGGREGATEREQBYPERIOD_2Weeks))
				weekRound = 2;
			else if (aggrReqByPeriod.equals(X_MRP_Run.AGGREGATEREQBYPERIOD_4Weeks))
				weekRound = 4;
			else if (aggrReqByPeriod.equals(X_MRP_Run.AGGREGATEREQBYPERIOD_8Weeks))
				weekRound = 8;
		}
		
		if (run.isIsProcessing()) {
			return "MRP Calculate Process currently in progress.";
		}
		
		AD_Client_ID = run.getAD_Client_ID();
		AD_Org_ID = run.getAD_Org_ID();
		
		if (AD_Org_ID == 0) {
			return ("Org must not be *.");
		}
		
		MOrgInfo orgInfo = MOrgInfo.get(ctx, AD_Org_ID, null);
		
		if (orgInfo.getM_Warehouse_ID() == 0) {
			return ("No Warehouse set in OrgInfo. for " + MOrg.get(ctx, AD_Org_ID).getName());
		}
		if (orgInfo.getFGLocator_ID() == 0) {
			return ("No Finished Goods Locator set in OrgInfo. for " + MOrg.get(ctx, AD_Org_ID).getName());
		}
		M_WarehouseID = orgInfo.getM_Warehouse_ID();
		M_LocatorID = orgInfo.getFGLocator_ID();
		
		run.setIsProcessing(true);
		run.save();
		StringBuilder error = new StringBuilder();
		dateFrom = run.getDateStart();
		dateTo = run.getDateFinish();
		M_PriceList_ID = run.getM_PriceList_ID();

		docType_PlannedOrder = run.getC_DocType_PlannedOrder_ID();
		docType_ConfirmedOrder = run.getC_DocType_ConfirmedOrder_ID();
		docType_PurchaseOrder = run.getC_DocType_PO_ID();
		docType_MRPRequisition = run.getC_DocType_MRPRequisition_ID();
		
		if (docType_PlannedOrder <= 0)
			error.append("No Mfg Planned Order Document set. \n");
		if (docType_ConfirmedOrder <= 0)
			error.append("No Confirmed Mfg Order Document set. \n");
		if (docType_PurchaseOrder <= 0)
			error.append("No Purchase Order Document set. \n");
		if (docType_MRPRequisition <= 0)
			error.append("No MRP Requisition Document set. \n");

		if (error.length() > 0) {
			throw new Exception(error.toString());
		}
		String sql = "DELETE FROM MRP_RunLine WHERE MRP_Run_ID=? AND AD_Client_ID=?";
		int noOfLinesDeleted = DB.executeUpdateEx(sql, new Object[] { mrpRunID, Env.getAD_Client_ID(ctx) }, null);
		printlog("No. of MRP lines deleted : " + noOfLinesDeleted);

		if (dateFrom == null)
			throw new IllegalArgumentException(Msg.translate(ctx, "FillMandatory") + Msg.translate(ctx,
					"DatePosted - From"));
		if (dateTo == null)
			throw new IllegalArgumentException(Msg.translate(ctx, "FillMandatory") + Msg.translate(ctx,
					"DatePosted - To"));
		if (M_PriceList_ID == 0)
			throw new IllegalArgumentException(Msg.translate(ctx, "FillMandatory") + Msg.translate(ctx,
					"M_PriceList_ID"));

		int isAfterDate = dateTo.compareTo(dateFrom);

		if (isAfterDate > 0)
		{

			START_WEEK = DB.getSQLValue(trx, "SELECT EXTRACT( WEEK FROM ?::Timestamp )", dateFrom) - 2;
			END_WEEK = DB.getSQLValue(trx, SQL_GET_ISO_WEEKNO, dateFrom, dateTo, dateTo, dateTo) + 2;

			if (START_WEEK == 0)
			{
				START_WEEK = DB.getSQLValue(trx,
						"SELECT EXTRACT(WEEK FROM (DATE_TRUNC('YEAR', ?::DATE) + interval '-1 day')) ", dateFrom);
				END_WEEK += START_WEEK;
			}

			Calendar cal = Calendar.getInstance();
			cal.setFirstDayOfWeek(Calendar.MONDAY);
			cal.setTime(dateFrom);
			cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			cal.add(Calendar.WEEK_OF_YEAR, -2);

			dateFrom.setTime(cal.getTimeInMillis());

			int weekDifference = END_WEEK - START_WEEK;

			cal.setTime(dateFrom);
			cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
			cal.add(Calendar.WEEK_OF_YEAR, weekDifference);

			dateTo.setTime(cal.getTimeInMillis());

			if (weekDifference > 24)
			{
				throw new IllegalArgumentException(Msg.translate(ctx,
						"Week difference should not be greater than 20 for selected horizon."));
			}
		}
		else
		{
			throw new IllegalArgumentException(Msg.translate(ctx, "ToDate must me greater than selected FromDate"));
		}
		
		sql = "DELETE FROM T_MRP_RUN_Capacity_Summary WHERE MRP_RUN_ID = ? ";
		noOfLinesDeleted =  DB.executeUpdate(sql, mrpRunID, null);
		log.fine("No. of lines from T_MRP_RUN_Capacity_Summary deleted : " + noOfLinesDeleted);

		if (run.isDeleteUnconfirmedProduction())
		{
			sql = "DELETE FROM M_ProductionLine pl USING M_Production p WHERE (p.M_Production_ID=pl.M_Production_ID AND p.Processed='N' AND p.C_DocType_ID = ?)";
			noOfLinesDeleted = DB.executeUpdate(sql, docType_PlannedOrder, null);
			log.fine("No. of planned production line deleted : " + noOfLinesDeleted);

			sql = "DELETE FROM M_Production	WHERE Processed='N' AND C_DocType_ID = ? ";
			noOfLinesDeleted = DB.executeUpdate(sql, docType_PlannedOrder, null);
			log.fine("No. of planned production deleted : " + noOfLinesDeleted);

			sql = "DELETE FROM m_production_batch b  " +
					"WHERE b.c_doctype_id = ? " +
		            "AND   NOT EXISTS (SELECT * " +
		             "                  FROM m_production " +
		             "                  WHERE m_production_batch_id = b.m_production_batch_id)";
			noOfLinesDeleted = DB.executeUpdate(sql, docType_PlannedOrder, null);
			log.fine("No. of Production Batch deleted " + noOfLinesDeleted);

			sql = "DELETE FROM M_MovementLine ml 	USING M_Movement m "
					+ " WHERE m.M_Production_Batch_ID IS NOT NULL AND m.Processed = 'N' "
					+ " AND NOT EXISTS (SELECT * FROM M_Production_Batch b WHERE b.M_Production_Batch_ID = m.M_Production_Batch_ID)";
			noOfLinesDeleted = DB.executeUpdate(sql, null);
			log.fine("No. of Movement Lines cleaned : " + noOfLinesDeleted);

			sql = "DELETE FROM M_Movement m " + " WHERE m.M_Production_Batch_ID IS NOT NULL AND m.Processed = 'N'"
					+ "	AND NOT EXISTS (SELECT * FROM M_Production_Batch b WHERE b.M_Production_Batch_ID = m.M_Production_Batch_ID)";
			noOfLinesDeleted = DB.executeUpdate(sql, null);
			log.fine("No. of Inventory Movements cleaned : " + noOfLinesDeleted);
		}

		if (run.isDeletePlannedPO())
		{
			String selectRequisition = " (SELECT M_Requisition_ID FROM M_Requisition WHERE DocStatus IN ('DR') AND Processed='N' AND AD_Client_ID = ? AND C_DocType_ID = ?) ";

			sql = "DELETE FROM PP_MRP	WHERE M_Requisition_ID IN " + selectRequisition;
			noOfLinesDeleted = DB.executeUpdateEx(sql, new Object[] { AD_Client_ID, docType_MRPRequisition }, null);
			log.fine("No. of Material Requirement Planning (PP_MRP) Line deleted : " + noOfLinesDeleted);

			sql = "DELETE FROM M_RequisitionLine	WHERE M_Requisition_ID IN " + selectRequisition;
			noOfLinesDeleted = DB.executeUpdateEx(sql, new Object[] { AD_Client_ID, docType_MRPRequisition }, null);
			log.fine("No. of MRP Requisition Line deleted : " + noOfLinesDeleted);

			sql = "DELETE FROM M_Requisition WHERE DocStatus IN ('DR') AND Processed='N' AND AD_Client_ID = ? AND C_DocType_ID = ? ";
			noOfLinesDeleted = DB.executeUpdateEx(sql, new Object[] { AD_Client_ID, docType_MRPRequisition }, null);
			log.fine("No. of MRP Requisition deleted : " + noOfLinesDeleted);
		}

		Map<Integer, MiniMRPProduct> miniMrpProducts = new TreeMap<Integer, MiniMRPProduct>();
		Set<Integer> productIds = new TreeSet<Integer>();

		// Collect all the Products required to be processed.
		printlog("START generateProductInfo:" + sdf.format(new Date()));
		generateProductInfo(miniMrpProducts, productIds);
		printlog("END generateProductInfo:" + sdf.format(new Date()));

		// Process Demand
		printlog("START doRunProductsSO:" + sdf.format(new Date()));
		doRunProductsSO(miniMrpProducts, productIds);
		printlog("END doRunProductsSO:" + sdf.format(new Date()));

		Trx.get(trx, false).commit();
		// Creating Requisition Order and Planned Production Order
		printlog("START doRunCreatePOandProductionOrder:" + sdf.format(new Date()));
		doRunCreatePOandProductionOrder(miniMrpProducts);
		printlog("END doRunCreatePOandProductionOrder:" + sdf.format(new Date()));

		// Save Requisition Lines to Database
		for (Date date : mapRequisition.keySet())
		{
			Map<Integer, MRequisition> aggrMap = mapRequisition.get(date);
			for (int aggrID : aggrMap.keySet())
			{
				MRequisition requisition = aggrMap.get(aggrID);
				log.fine("START: Write to DB Requisition Line " + requisition.toString() + " " + sdf.format(new Date()));
				requisition.saveLineQueue();
				log.fine("END: Write to DB Requisition Line " + requisition.toString() + " " + sdf.format(new Date()));
			}
		}

		Trx.get(trx, false).commit();

		// Process Supply
		printlog("START doRunOpenOrders:TYPE_PO" + sdf.format(new Date()));
		doRunOpenOrders(miniMrpProducts, productIds, TYPE_PO);
		printlog("END doRunOpenOrders:TYPE_MO" + sdf.format(new Date()));

		printlog("START doRunOpenOrders:TYPE_MO" + sdf.format(new Date()));
		doRunOpenOrders(miniMrpProducts, productIds, TYPE_MO);
		printlog("END doRunOpenOrders:TYPE_MO" + sdf.format(new Date()));

		printlog("START doRunOpenOrders:TYPE_RQ" + sdf.format(new Date()));
		doRunOpenOrders(miniMrpProducts, productIds, TYPE_RQ);
		printlog("END doRunOpenOrders:TYPE_RQ" + sdf.format(new Date()));


		Trx.get(trx, false).commit();

		printlog("START renderPeggingReport" + sdf.format(new Date()));
		renderPeggingReport(miniMrpProducts);
		printlog("END renderPeggingReport" + sdf.format(new Date()));

		printlog("START updateHasSupplyDemand" + sdf.format(new Date()));
		updateHasSupplyDemand();
		printlog("END updateHasSupplyDemand" + sdf.format(new Date()));

		Timestamp t2 = new Timestamp(System.currentTimeMillis());
		printlog("END DoIt: " + sdf.format(t2) + "\n\n Time Diff Millis: " + new DecimalFormat("###,###").format(
				t2.getTime() - t1.getTime()));

		run.setIsProcessing(false);
		run.save();

		// Insert summary lines of Work capacity as per product and work centre
		// wise on T_MRP_RUN_Capacity_Summary table.
		int no = DB.executeUpdateEx(SQL_GET_MRP_RUN_CAPACITY_SUMMARY, new Object[] { dateTo }, trx);
		log.fine("No. of lines on T_MRP_RUN_Capacity_Summary Inserted : " + no);

		return infoMsg.toString();
	}

	/*
	 * Set flag to show products where demand and/or supply exists in the MRP
	 * period, used to filter the MRP report to exclude products with no
	 * movement
	 */
	private void updateHasSupplyDemand()
	{

		String sql = "WITH HasSupply AS "
				+ "(SELECT M_Product_ID, MRP_Run_ID, AD_Client_ID, COUNT (M_Product_ID) "
				+ " FROM MRP_RunLine	WHERE MRP_Run_ID = ? AND AD_Client_ID = ? "
				+ " GROUP BY AD_Client_ID, MRP_Run_ID, M_Product_ID "
				+ " HAVING COUNT (M_Product_ID) > 2	"
				+ ") "
				+ "UPDATE MRP_RunLine rl SET HasSupplyDemand = 'Y' FROM HasSupply hs "
				+ "WHERE hs.MRP_Run_ID = rl.MRP_Run_ID AND hs.AD_Client_ID = rl.AD_Client_ID AND hs.M_Product_ID = rl.M_Product_ID";

		int updated = DB.executeUpdateEx(sql, new Object[] { mrpRunID, Env.getAD_Client_ID(ctx) }, trx);

		log.fine("Lines with supply/demand updated: " + updated);

	}

	/**
	 * This method will process to create Production Order and Purchase order as
	 * per required quantity.
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProducts
	 */
	private void doRunCreatePOandProductionOrder(Map<Integer, MiniMRPProduct> miniMrpProducts)
	{
		MRequisition requisition = null;

		// Calculate Required and production QTY
		runProcessCalculatePlannedQty(miniMrpProducts);

		// Create Requisition Order with Lines
		for (Integer productID : mapRequirePlanningQty.keySet())
		{
			MiniMRPProduct mrp = miniMrpProducts.get(productID);
			Map<Date, BigDecimal> weeklyProductionQty = new TreeMap<Date, BigDecimal>(
					mapRequirePlanningQty.get(productID));
			for (Date date : weeklyProductionQty.keySet())
			{
				if (weeklyProductionQty.get(date).compareTo(Env.ZERO) == 0)
					continue;

				// TODO: Handle kanban controlled product
				if (isExclKanbanPrd && !mrp.isBOM() && mrp.isKanban() && !mrp.isPhantom())
				{
					addToKanbanMap(mrp, date, weeklyProductionQty.get(date));
				}
				else if (!mrp.isBOM() && mrp.isPurchased() && !mrp.isPhantom())
				{
					requisition = createRequisitionHeader(date, mrp);
					createRequisitionLine(requisition, mrp, weeklyProductionQty.get(date));
				}
				else if (mrp.isBOM() && !mrp.isPhantom())
				{
					createProductionOrder(mrp, weeklyProductionQty.get(date), new Timestamp(date.getTime()));
				}

				Trx.get(trx, false).commit();

			}
		}

		infoMsg.append(" No of Docs created: Requisition:" + countReq);
		infoMsg.append(" and Planned Production:" + countProd);
		infoMsg.append("\n Requisition Docs List: " + requisitionDocs);
		infoMsg.append("\n Planned Docs List: " + productionDocs);

		log.log(Level.INFO, infoMsg.toString());
	}

	private void runProcessCalculatePlannedQty(Map<Integer, MiniMRPProduct> miniMrpProducts)
	{
		for (Date date : mapDemand.keySet())
		{
			Map<Integer, BigDecimal> mapOrderQty = mapDemand.get(date);
			for (Integer productID : mapOrderQty.keySet())
			{
				BigDecimal demandQty = mapOrderQty.get(productID);
				if (demandQty.compareTo(Env.ZERO) != 0)
				{
					MiniMRPProduct mrp = miniMrpProducts.get(productID);
					if (mrp == null) {
						MProduct p = MProduct.get(ctx, productID);
						String error = "Please check Product=" + p.getValue() + " replenishment parameters may not be setup properly.";
						log.severe(error);
						throw new AdempiereException(error);
					}
				
					Integer nonPhantomProduct = (mrp.isPhantom() && mrp.isBOM() ? 0 : productID);
					createPlannedQtyMap(miniMrpProducts, date, productID, demandQty, 0, nonPhantomProduct); // (MRP,DateOfDemand,PID,DQty,Level,NonPhontomPID)
				}
			}
		}
	}

	private void createPlannedQtyMap(Map<Integer, MiniMRPProduct> miniMrpProducts, Date date, Integer productID,
			BigDecimal demandQty, int level, Integer nonPhantomProduct)
	{
		if (miniMrpProducts.containsKey(productID))
		{
			MiniMRPProduct mrp = miniMrpProducts.get(productID);

			// check QtyOnHand
			if (mrp.getQtyOnHand().compareTo(demandQty) >= 0)
			{
				mrp.setQtyOnHand(mrp.getQtyOnHand().subtract(demandQty));
				demandQty = Env.ZERO;
				return;
			}
			else if (mrp.getQtyOnHand().compareTo(Env.ZERO) != 0 && mrp.getQtyOnHand().compareTo(demandQty) < 0)
			{
				demandQty = demandQty.subtract(mrp.getQtyOnHand());
				mrp.setQtyOnHand(Env.ZERO);
			}

			Map<Date, BigDecimal> confirmProductQty = mrp.getMapConfirmProductQty();

			// Check Confirmed Production Order
			if (demandQty.compareTo(Env.ZERO) > 0 && confirmProductQty != null && !confirmProductQty.isEmpty())
			{
				for (Date dateProduction : confirmProductQty.keySet())
				{
					BigDecimal confirmQty = confirmProductQty.get(dateProduction);
					if (confirmQty.compareTo(Env.ZERO) > 0 && date.compareTo(dateProduction) >= 0)
					{
						if (confirmQty.compareTo(demandQty) >= 0)
						{
							confirmQty = confirmQty.subtract(demandQty);
							confirmProductQty.put(dateProduction, confirmQty);
							demandQty = Env.ZERO;
							break;
						}
						else
						{
							demandQty = demandQty.subtract(confirmQty);
							confirmProductQty.put(dateProduction, Env.ZERO);
						}
					}
				}
			}

			if (mrp.isReplenishTypeMRPCalculated() && mrp.getExtraQtyPlanned().compareTo(Env.ZERO) != 0)
			{
				if (mrp.getExtraQtyPlanned().compareTo(demandQty) < 0)
				{
					demandQty = demandQty.subtract(mrp.getExtraQtyPlanned());
					mrp.setExtraQtyPlanned(Env.ZERO);
				}
				else
				{
					mrp.setExtraQtyPlanned(mrp.getExtraQtyPlanned().subtract(demandQty));
					demandQty = Env.ZERO;
					return;
				}
			}

			if (demandQty.compareTo(Env.ZERO) > 0)
			{
				if (!mrp.isPhantom() && mrp.isBOM())
				{
					level++;
					nonPhantomProduct = mrp.getM_Product_ID();
				}
				else if (!mrp.isPhantom() && !mrp.isBOM() && nonPhantomProduct != 0)
				{
					level++;
				}

				calendar.setTimeInMillis(date.getTime());
				calendar.add(Calendar.DAY_OF_MONTH, (level * -7));
				calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
				Timestamp plannedDate = new Timestamp(calendar.getTimeInMillis());

				if (plannedDate.before(dateFrom))
					plannedDate = dateFrom;

				if (!mrp.isPhantom() && nonPhantomProduct != 0 && mrp.isBOM())
				{
					if (mrp.isReplenishTypeMRPCalculated())
						demandQty = calculateBatchSizeWiseOrderCreation(mrp, demandQty, plannedDate);
					else
						setRequirePlanningQty(mrp, plannedDate, demandQty);
				}

				if (mrp.isBOM())
				{
					planBOMTree(miniMrpProducts, productID, date, demandQty, level, nonPhantomProduct);
				}
				else if (!mrp.isPhantom())
				{
					if (mrp.isReplenishTypeMRPCalculated())
						demandQty = calculateBatchSizeWiseOrderCreation(mrp, demandQty, plannedDate);
					else
						setRequirePlanningQty(mrp, plannedDate, demandQty);
				}
			}
		}
	}

	/**
	 * Create Planned Production Order
	 * 
	 * @param mrp
	 * @param qty
	 * @param date
	 */
	private void createProductionOrder(MiniMRPProduct mrp, BigDecimal qty, Timestamp date)
	{
		
//		int last_id = DB.getSQLValue(trx, "select max(m_productionline_id) from m_productionline");
//		log.severe("DEBUG ID=" + last_id);
		MProduction mProd = new MProduction(ctx, 0, trx);
		mProd.setAD_Client_ID(AD_Client_ID);
		mProd.setAD_Org_ID(AD_Org_ID);
		mProd.setM_Product_ID(mrp.getM_Product_ID());
		mProd.setProductionQty(qty);
		mProd.setM_Locator_ID(M_LocatorID);
		mProd.setC_DocType_ID(docType_PlannedOrder);
		mProd.setName("Planned Production Order");
		mProd.setDescription("Creating from MiniMRP");
		mProd.setDatePromised(date);
		mProd.setMovementDate(date);
		mProd.setDocumentNo("<>");
		mProd.saveEx();

		mProd.createLines(false);
		mProd.setIsCreated(true);
		mProd.saveEx();

		countProd++;
		productionDocs.append(mProd.getDocumentNo()).append(",");
	}

	/**
	 * Create Planned Production / Requisition Order as per QtyBatchSize on
	 * Product.
	 * 
	 * @param mrp
	 * @param qty
	 * @param date
	 * @return qtyForPlan
	 */
	private BigDecimal calculateBatchSizeWiseOrderCreation(MiniMRPProduct mrp, BigDecimal qty, Timestamp date)
	{
		int createNoOfOrder = 1;
		BigDecimal QtyPlan = qty;

		if (mrp.getQtyBatchSize().compareTo(Env.ZERO) == 0)
		{
			setRequirePlanningQty(mrp, date, qty);
			return qty;
		}
		else
		{
			createNoOfOrder = qty.divide(mrp.getQtyBatchSize(), 0, BigDecimal.ROUND_CEILING).intValue();
			QtyPlan = mrp.getQtyBatchSize();
		}

		BigDecimal qtyPlanned = QtyPlan.multiply(new BigDecimal(createNoOfOrder));
		mrp.setExtraQtyPlanned(mrp.getExtraQtyPlanned().add(qtyPlanned).subtract(qty));

		for (int i = 0; i < createNoOfOrder; i++)
		{
			if (mrp.isBOM())
				createProductionOrder(mrp, QtyPlan, date);
			else
			{
				// TODO: Handle kanban controlled product
				if (isExclKanbanPrd && mrp.isKanban())
				{
					addToKanbanMap(mrp, date, QtyPlan);
				}
				else
				{
					MRequisition requisition = createRequisitionHeader(date, mrp);
					createRequisitionLine(requisition, mrp, QtyPlan);
				}
			}
			
			Trx.get(trx, false).commit();
		}

		return qtyPlanned; 
	}

	private void addToKanbanMap(MiniMRPProduct mrp, Date date, BigDecimal qtyPlan)
	{
		date = aggrDateIntoPeriod(date);
		Map<Date, KanbanProduct> mapDatewise;
		if (mapKanbanProduct.containsKey(mrp.getM_Product_ID()))
		{
			mapDatewise = mapKanbanProduct.get(mrp.getM_Product_ID());
			if (mapDatewise.containsKey(date))
			{
				KanbanProduct kp = mapDatewise.get(date);
				kp.setQty(kp.getQty().add(qtyPlan));
			}
			else
			{
				KanbanProduct kp = new KanbanProduct(mrp.getM_Product_ID(), qtyPlan);
				kp.setM_Product_Category_ID(mrp.getM_Product_Category_ID());
				kp.setC_BPartner_ID(mrp.getC_BPartner_ID());
				kp.setC_BP_Group_ID(mrp.getC_BP_Group_ID());
				kp.setWeek(date, dateFrom);
				mapDatewise.put(date, kp);
			}
		}
		else
		{
			mapDatewise = new HashMap<Date, KanbanProduct>();
			KanbanProduct kp = new KanbanProduct(mrp.getM_Product_ID(), qtyPlan);
			kp.setM_Product_Category_ID(mrp.getM_Product_Category_ID());
			kp.setC_BPartner_ID(mrp.getC_BPartner_ID());
			kp.setC_BP_Group_ID(mrp.getC_BP_Group_ID());
			kp.setWeek(date, dateFrom);
			mapDatewise.put(date, kp);
			mapKanbanProduct.put(mrp.getM_Product_ID(), mapDatewise);
		}
	}

	private void setRequirePlanningQty(MiniMRPProduct mrp, Date date, BigDecimal demandQty)
	{
		if (mrp.isPhantom())
			return;
		if (mapRequirePlanningQty.containsKey(mrp.getM_Product_ID()))
		{
			Map<Date, BigDecimal> weekly = mapRequirePlanningQty.get(mrp.getM_Product_ID());
			if (weekly.containsKey(date))
				weekly.put(date, weekly.get(date).add(demandQty));
			else
				weekly.put(date, demandQty);
		}
		else
		{
			Map<Date, BigDecimal> weekly = new HashMap<Date, BigDecimal>();
			weekly.put(date, demandQty);
			mapRequirePlanningQty.put(mrp.getM_Product_ID(), weekly);
		}
	}

	private void planBOMTree(Map<Integer, MiniMRPProduct> miniMrpProducts, Integer productID, Date date,
			BigDecimal demandQty, int level, Integer nonPhantomProduct)
	{
		String sql = getQueryForBOMProductExplode();

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, trx);
			pstmt.setBigDecimal(1, demandQty);
			pstmt.setInt(2, productID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				BigDecimal requiredQty = rs.getBigDecimal("RequiredQty");
				if (requiredQty.compareTo(Env.ZERO) > 0)
					createPlannedQtyMap(miniMrpProducts, date, rs.getInt("M_ProductBOM_ID"), requiredQty, level,
							nonPhantomProduct);
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException("Could not process BOM product explode of requiring Qty.");
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}

	/**
	 * Create Requisition without implementation of QtyBatchSize logic
	 * 
	 * @param date
	 * @param mrp
	 * @return requisition
	 */
	private MRequisition createRequisitionHeader(Date date, MiniMRPProduct mrp)
	{
		int aggrID = 0;
		String desc = "With Kanban Product";
		MRequisition requisition = null;
		date = aggrDateIntoPeriod(date);

		if (isExclKanbanPrd && aggrReqBPG_CV.equals(X_MRP_Run.AGGREGATEREQUISITION_CurrentVendor))
		{
			// Current vendor wise weekly aggregate
			desc = "Aggr CurrVendor [" + weekRound + " Week] :";
			aggrID = mrp.getC_BPartner_ID();
			if (aggrID > 0)
			{
				MBPartner bp = MBPartner.get(ctx, aggrID);
				desc += bp.getValue() + "-" + bp.getName();
			}
			else
				desc = "-";
		}
		else if (isExclKanbanPrd && aggrReqBPG_CV.equals(X_MRP_Run.AGGREGATEREQUISITION_VendorBPGroup))
		{
			// Current Vendor's BP Group wise weekly aggregate
			desc = "Aggr BPGroup[" + weekRound + " Week] :";
			aggrID = mrp.getC_BP_Group_ID();
			if (aggrID > 0)
			{
				MBPGroup bpGroup = MBPGroup.get(ctx, aggrID);
				desc += bpGroup.getValue() + "-" + bpGroup.getName();
			}
			else
				desc = "-";
		}

		Map<Integer, MRequisition> aggrMap;
		if (mapRequisition.containsKey(date))
		{
			aggrMap = mapRequisition.get(date);
			if (aggrMap.containsKey(aggrID))
			{
				requisition = aggrMap.get(aggrID);
			}
			else
			{
				requisition = createRequisition(date, desc);
				aggrMap.put(aggrID, requisition);
				log.fine("New Requisition Header created. " + requisition.toString());
			}
		}
		else
		{
			aggrMap = new HashMap<Integer, MRequisition>();
			requisition = createRequisition(date, desc);
			aggrMap.put(aggrID, requisition);
			mapRequisition.put(date, aggrMap);
			log.fine("New Requisition Header created. " + requisition.toString());
		}

		return requisition;
	}

	/**
	 * @param date
	 * @return date rounded by weeks
	 */
	private Date aggrDateIntoPeriod(Date date)
	{
		if (!isExclKanbanPrd)
			return date;

		int days = ((TimeUtil.getDaysBetween(dateFrom, new Timestamp(date.getTime())) / 7) / weekRound) * weekRound * 7;
		date = TimeUtil.addDays(dateFrom, days);
		return date;
	}

	/**
	 * Create Requisition Order
	 * 
	 * @param date
	 * @param desc 
	 * @return
	 */
	private MRequisition createRequisition(Date date, String desc)
	{
		MRequisition requisition;
		requisition = new MRequisition(ctx, 0, trx);
		requisition.setAD_Client_ID(AD_Client_ID);
		requisition.setM_Warehouse_ID(M_WarehouseID);
		requisition.setC_DocType_ID(docType_MRPRequisition);
		requisition.setAD_User_ID(AD_User_ID);
		requisition.setDescription(desc);
		requisition.setHelp("Created from MiniMRP.");
		requisition.setDateRequired(new Timestamp(date.getTime()));
		requisition.setDateDoc(new Timestamp(date.getTime()));
		requisition.setM_PriceList_ID(M_PriceList_ID);
		requisition.saveEx();

		countReq++;
		requisitionDocs.append(requisition.getDocumentNo()).append(",");

		return requisition;
	}

	private void createRequisitionLine(MRequisition requisition, MiniMRPProduct mrp, BigDecimal qty)
	{
		requisition.addLinetoQueue(mrp.getM_Product_ID(), mrp.getC_BPartner_ID(), mrp.getPO_PriceList_ID(), qty, mrp.getPriceActual());
/*		MRequisitionLine rLine = new MRequisitionLine(requisition);
		rLine.setM_Product_ID(mrp.getM_Product_ID());
		rLine.setC_BPartner_ID(mrp.getC_BPartner_ID());
		rLine.setPriceActual(mrp.getPriceActual());
		rLine.setQty(qty);
		rLine.saveEx();
*/	}

	/**
	 * Product wise Get confirmed Qty. If Product is BOM then Production else
	 * from PO.
	 * 
	 * @param mrp
	 */
	private void setConfirmProductQty(MiniMRPProduct mrp)
	{
		int productID = mrp.getM_Product_ID();
		boolean isBOM = mrp.isBOM();

		Map<Date, BigDecimal> mapConfirmQty = new TreeMap<Date, BigDecimal>();

		String sql = (isBOM ? SQL_PRODUCTWISE_CONFIRM_PRODUCTION_QTY : SQL_PRODUCTWISE_CONFIRM_PO_RQ_QTY);

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			int paramCount = 1;
			pstmt = DB.prepareStatement(sql, trx);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setInt(paramCount++, weekRound);
			pstmt.setInt(paramCount++, weekRound);
			pstmt.setInt(paramCount++, productID);
			pstmt.setTimestamp(paramCount++, dateTo);

			if (isBOM)
				pstmt.setInt(paramCount++, docType_PlannedOrder);
			else
			{
				pstmt.setInt(paramCount++, productID);
				pstmt.setTimestamp(paramCount++, dateTo);
				pstmt.setInt(paramCount++, AD_Client_ID);
				pstmt.setInt(paramCount++, docType_MRPRequisition);
			}

			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Date datePromised = rs.getDate("DatePromised");
				if (datePromised.before(dateFrom))
					datePromised = dateFrom;
				BigDecimal qty = rs.getBigDecimal("Qty");

				if (mapConfirmQty.containsKey(datePromised))
					mapConfirmQty.put(datePromised, mapConfirmQty.get(datePromised).add(qty));
				else
					mapConfirmQty.put(datePromised, qty);

				if (mrp.isBOM())
				{
					// Add demand for substitute product of Confirmed product from Production order.
					PreparedStatement pstatement = null;
					ResultSet rset = null;
					try
					{
						pstatement = DB.prepareStatement(SQL_GET_PRODUCTIONLINE_INFO, trx);
						pstatement.setInt(1, rs.getInt("M_Production_ID"));

						rset = pstatement.executeQuery();
						while (rset.next())
						{
							setQtyAsDemand(rset.getInt("M_Product_ID"), rset.getBigDecimal("QtyUsed"),
									rset.getTimestamp("DatePromised"));
						}
					}
					catch (SQLException e)
					{
						throw new AdempiereException("Could not process, Retrieve Confirm production line info.", e);
					}
					finally
					{
						DB.close(rset, pstatement);
						rset = null;
						pstatement = null;
					}
				}
			}
		}
		catch (SQLException e)
		{
			throw new AdempiereException("Could not process, Retrieve weekly confirm production qty of Product:"
					+ mrp.getName(), e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		mrp.setMapConfirmProductQty(mapConfirmQty);
	}

	public void addAvailableInventory(int mProductId, BigDecimal qty)
	{
		availableInventory.put(mProductId, qty);
	}

	/**
	 * @author Sachin Bhimani
	 * @param miniMrpProduct
	 * @param line
	 * @param type
	 * @return miniMRP_RunLine
	 */
	private X_MRP_RunLine getX_MRP_RunLine(MiniMRPProduct miniMrpProduct, int line, String type)
	{
		X_MRP_RunLine miniMRP = new X_MRP_RunLine(ctx, 0, trx);
		miniMRP.setM_Product_ID(miniMrpProduct.getM_Product_ID());
		miniMRP.setM_Product_Category_ID(miniMrpProduct.getM_Product_Category_ID());
		miniMRP.setProductName(miniMrpProduct.getName());
		miniMRP.setLine(line);
		miniMRP.setMRP_Run_ID(mrpRunID);
		miniMRP.setRecordType(type);
		miniMRP.setDateStart(dateFrom);
		miniMRP.setDateFinish(dateTo);
		return miniMRP;
	}


	public void calculateSuggestedOrders(MiniMRPProduct miniMrpProduct, Map<Integer, BigDecimal> totalDemandLine,
			Map<Integer, BigDecimal> totalSupplyLine, Map<Integer, BigDecimal> openingBalanceLine,
			Map<Integer, BigDecimal> closingBalanceLine)
	{
		int horizon = END_WEEK - START_WEEK + 1;

		for (int i = 0; i < horizon; i++)
		{
			int week = START_WEEK + i;

			BigDecimal totalDemand = Env.ZERO;
			BigDecimal totalSupply = Env.ZERO;
			BigDecimal openingBalance = Env.ZERO;

			if (totalDemandLine.containsKey(week))
			{
				totalDemand = totalDemandLine.get(week);
				if (totalDemand == null)
				{
					totalDemand = Env.ZERO;
				}
			}
			if (totalSupplyLine.containsKey(week))
			{
				totalSupply = totalSupplyLine.get(week);
				if (totalSupply == null)
				{
					totalSupply = Env.ZERO;
				}
			}
			if (openingBalanceLine.containsKey(week))
			{
				openingBalance = openingBalanceLine.get(week);
				if (openingBalance == null)
				{
					openingBalance = Env.ZERO;
				}
			}

			BigDecimal sugQty = totalDemand.subtract((totalSupply.add(openingBalance)));

			if (sugQty.compareTo(Env.ZERO) > 0)
			{
				openingBalanceLine.put(week + 1, Env.ZERO);
				closingBalanceLine.put(week, Env.ZERO);
			}
			else
			{
				openingBalanceLine.put(week + 1, (sugQty.negate()));
				closingBalanceLine.put(week, (sugQty.negate()));
			}
		}
	}

	public Map<Integer, BigDecimal> insertOrderDemands(MiniMRPProduct miniMrpProduct)
	{
		Map<Integer, Map<Integer, BigDecimal>> orderDemands = miniMrpProduct.getDemand();
		Map<Integer, BigDecimal> totalDemandLine = new HashMap<Integer, BigDecimal>();
		// From MRP Demand MAP
		if (!orderDemands.isEmpty())
		{
			for (Integer orderId : orderDemands.keySet())
			{
				Map<Integer, BigDecimal> weeklyDemand = orderDemands.get(orderId);
				if (weeklyDemand.size() > 0)
				{
					X_MRP_RunLine miniMRP = getX_MRP_RunLine(miniMrpProduct, lineNo, TYPE_PRODUCT_ORDER_DEMAND);
					miniMRP.setC_Order_ID(orderId);
					MOrder order = new MOrder(ctx, orderId, trx);

					String datePromised = requiredFormat.format(order.getDatePromised());

					miniMRP.setOrderInfo(order.getDocumentNo() + " - " + datePromised);
					for (Integer week : weeklyDemand.keySet())
					{
						BigDecimal qty = weeklyDemand.get(week);
						setWeeklyData(miniMRP, week, qty);
						if (totalDemandLine.containsKey(week))
						{
							qty = qty.add(totalDemandLine.get(week));
						}
						totalDemandLine.put(week, qty);
					}
					miniMRP.saveEx();
				}
				lineNo += 10;
			}
		}

		// Get Planned Production Demand as per product wise.
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int paramCount = 1;
		try
		{
			pstmt = DB.prepareStatement(SQL_PRODUCTWISE_INFO_FROM_PRODUCTION, trx);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setInt(paramCount++, miniMrpProduct.getM_Product_ID());
			pstmt.setInt(paramCount++, docType_PlannedOrder);
			pstmt.setTimestamp(paramCount++, dateTo);

			rs = pstmt.executeQuery();
			while (rs.next())
			{
				insertProductionDemand(miniMrpProduct, totalDemandLine, rs.getInt("M_Production_ID"),
						rs.getString("DocumentNo"), rs.getTimestamp("DatePromised"), rs.getBigDecimal("QtyUsed"),
						"Planned");
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, SQL_PRODUCTWISE_INFO_FROM_PRODUCTION, e);
			throw new AdempiereException(e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		// Add demand for SubLevel Product which is Confirmed production.
		int[] productionID = DB.getIDsEx(trx, SQL_GET_PRODUCTION_SUBPRODUCTWISE,
				miniMrpProduct.getM_Product_ID(), dateTo, docType_PlannedOrder);
		for (int mProdID : productionID)
		{
			MProduction production = new MProduction(ctx, mProdID, trx);
			BigDecimal qty = DB.getSQLValueBD(trx,
					"SELECT SUM(QtyUsed) FROM M_ProductionLine WHERE M_Production_ID = ? AND M_Product_ID = ? AND AD_Client_ID=?",
					mProdID, miniMrpProduct.getM_Product_ID(), AD_Client_ID);
			if (qty == null)
			{
				qty = Env.ZERO;
			}
			insertProductionDemand(miniMrpProduct, totalDemandLine, production.getM_Production_ID(),
					production.getDocumentNo(),
					(production.getDatePromised().before(dateFrom) ? dateFrom : production.getDatePromised()), qty,
					"Confirm");
		}

		if (totalDemandLine != null && !totalDemandLine.isEmpty())
			insertTotalLine(miniMrpProduct, totalDemandLine, TYPE_TOTAL_DEMAND_LINE);

		return totalDemandLine;
	}

	/**
	 * @param miniMrpProduct
	 * @param totalDemandLine
	 * @param mProduction_ID
	 * @param documentNo
	 * @param promisedDate
	 * @param qtyUsed
	 * @param prodType
	 */
	private void insertProductionDemand(MiniMRPProduct miniMrpProduct, Map<Integer, BigDecimal> totalDemandLine,
			int mProduction_ID, String documentNo, Timestamp promisedDate, BigDecimal qtyUsed, String prodType)
	{
		X_MRP_RunLine miniMRP = getX_MRP_RunLine(miniMrpProduct, lineNo, TYPE_PRODUCT_ORDER_DEMAND);
		miniMRP.setM_Production_ID(mProduction_ID);
		String datePromised = null;
		int week = 0;

		datePromised = requiredFormat.format(promisedDate);
		week = DB.getSQLValue(trx, SQL_GET_ISO_WEEKNO, dateFrom, promisedDate, promisedDate, promisedDate);

		miniMRP.setProductionInfo(documentNo + " - " + datePromised + " - " + prodType);

		setWeeklyData(miniMRP, week, qtyUsed);
		if (totalDemandLine.containsKey(week))
		{
			BigDecimal totalDemand = totalDemandLine.get(week);
			if (totalDemand == null)
			{
				totalDemand = Env.ZERO;
			}
			qtyUsed = qtyUsed.add(totalDemand);
		}
		totalDemandLine.put(week, qtyUsed);
		miniMRP.saveEx();
		lineNo += 10;
	}

	/**
	 * Insert total line [Demand/Supply line]
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProduct
	 * @param totalLine
	 * @param lineType
	 */
	public void insertTotalLine(MiniMRPProduct miniMrpProduct, Map<Integer, BigDecimal> totalLine, String lineType)
	{
		if (!totalLine.isEmpty())
		{
			X_MRP_RunLine miniMRP = getX_MRP_RunLine(miniMrpProduct, lineNo, lineType);
			for (Integer week : totalLine.keySet())
			{
				BigDecimal qty = totalLine.get(week);
				setWeeklyData(miniMRP, week, qty);
			}
			miniMRP.saveEx();
		}
		lineNo += 10;
	}

	/**
	 * Insert all supply lines
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProduct
	 * @return totalSupplyLine
	 */
	public Map<Integer, BigDecimal> insertSupplyLines(MiniMRPProduct miniMrpProduct)
	{
		Map<Integer, BigDecimal> totalSupplyLine = new HashMap<Integer, BigDecimal>();
		Map<Integer, BigDecimal> subTotalSupplyLine = new HashMap<Integer, BigDecimal>();

		// Insert Production for Non Planned Orders (Confirmed) & Total Line
		insertSupplyLineMO(miniMrpProduct, totalSupplyLine, subTotalSupplyLine, false);
		insertTotalSupplyProductionLine(miniMrpProduct, subTotalSupplyLine, false);

		// Insert Production Supply for Planned Orders & Total Line
		subTotalSupplyLine.clear();
		insertSupplyLineMO(miniMrpProduct, totalSupplyLine, subTotalSupplyLine, true);
		insertTotalSupplyProductionLine(miniMrpProduct, subTotalSupplyLine, true);

		// Insert Purchase Order Confirmed Lines
		subTotalSupplyLine.clear();
		insertSupplyLinePO(miniMrpProduct, totalSupplyLine, subTotalSupplyLine, true);
		insertTotalSupplyLine(miniMrpProduct, subTotalSupplyLine, TYPE_TOTAL_SUPPLY_LINE_PO);

		// Insert Requisition Lines
		subTotalSupplyLine.clear();
		insertSupplyLinePO(miniMrpProduct, totalSupplyLine, subTotalSupplyLine, false);
		insertTotalSupplyLine(miniMrpProduct, subTotalSupplyLine, TYPE_TOTAL_SUPPLY_LINE_RQ);
		
		if (isExclKanbanPrd)
		{
			// Insert Kanban Product Lines instead of requisition lines
			subTotalSupplyLine.clear();
			insertSupplyLineKB(miniMrpProduct, totalSupplyLine, subTotalSupplyLine, TYPE_SUPPLY_LINE_KB);
			insertTotalSupplyLine(miniMrpProduct, subTotalSupplyLine, TYPE_TOTAL_SUPPLY_LINE_KB);
		}
		
		// Insert Total Supply Line
		if (totalSupplyLine != null && !totalSupplyLine.isEmpty())
			insertTotalLine(miniMrpProduct, totalSupplyLine, TYPE_TOTAL_SUPPLY_LINE);

		return totalSupplyLine;
	}

	/**
	 * Insert Production for Non Planned Orders (Confirmed) and Planned Orders
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProduct
	 * @param totalSupplyLine
	 * @param subTotalSupplyLine
	 * @param isPlannedOrder
	 */
	public void insertSupplyLineMO(MiniMRPProduct miniMrpProduct, Map<Integer, BigDecimal> totalSupplyLine,
			Map<Integer, BigDecimal> subTotalSupplyLine, boolean isPlannedOrder)
	{
		Map<Integer, Map<Integer, BigDecimal>> supplyLineMO = new TreeMap<Integer, Map<Integer, BigDecimal>>(
				miniMrpProduct.getSupplyLineMO());

		if (!supplyLineMO.isEmpty())
		{
			for (Integer M_Production_ID : supplyLineMO.keySet())
			{
				Map<Integer, BigDecimal> weeklyDemand = supplyLineMO.get(M_Production_ID);
				MProduction production = new MProduction(ctx, M_Production_ID, trx);
				if ((isPlannedOrder == false && production.getC_DocType_ID() != docType_PlannedOrder)
						|| (isPlannedOrder && production.getC_DocType_ID() == docType_PlannedOrder))
				{
					if (weeklyDemand.size() > 0)
					{
						X_MRP_RunLine miniMRP = getX_MRP_RunLine(miniMrpProduct, lineNo, isPlannedOrder
								? TYPE_SUPPLY_LINE_MO_PLANNED : TYPE_SUPPLY_LINE_MO_NONPLANNED);
						miniMRP.setM_Production_ID(M_Production_ID);

						String datePromished = requiredFormat.format(production.getDatePromised());

						miniMRP.setProductionInfo(production.getDocumentNo() + " - " + datePromished);
						for (Integer week : weeklyDemand.keySet())
						{
							BigDecimal qty = weeklyDemand.get(week);
							setWeeklyData(miniMRP, week, qty);
							if (totalSupplyLine.containsKey(week))
							{
								qty = qty.add(totalSupplyLine.get(week));
							}
							totalSupplyLine.put(week, qty);

							BigDecimal qtyProd = weeklyDemand.get(week);
							if (subTotalSupplyLine.containsKey(week))
							{
								qtyProd = qtyProd.add(subTotalSupplyLine.get(week));
							}
							subTotalSupplyLine.put(week, qtyProd);
						}
						miniMRP.saveEx();
					}
					lineNo += 10;
				}
			}
		}
	}

	/**
	 * Insert Purchase Order and Requisition lines
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProduct
	 * @param totalSupplyLine
	 * @param subTotalSupplyLine
	 * @param isPO
	 */
	public void insertSupplyLinePO(MiniMRPProduct miniMrpProduct, Map<Integer, BigDecimal> totalSupplyLine,
			Map<Integer, BigDecimal> subTotalSupplyLine, boolean isPO)
	{
		Map<Integer, Map<Integer, BigDecimal>> supplyLine;
		String typeSupply;
		if (isPO)
		{
			supplyLine = miniMrpProduct.getSupplyLinePO();
			typeSupply = TYPE_SUPPLY_LINE_PO;
		}
		else
		{
			supplyLine = miniMrpProduct.getSupplyLineRQ();
			typeSupply = TYPE_SUPPLY_LINE_RQ;
		}
		if (!supplyLine.isEmpty())
		{
			// columnID is C_Order_ID or M_Requisition_ID
			for (Integer columnID : supplyLine.keySet())
			{
				Map<Integer, BigDecimal> weeklyDemand = new TreeMap<Integer, BigDecimal>(supplyLine.get(columnID));
				if (weeklyDemand.size() > 0)
				{
					X_MRP_RunLine miniMRP = getX_MRP_RunLine(miniMrpProduct, lineNo, typeSupply);
					String info = "";
					if (isPO)
					{
						miniMRP.setC_Order_ID(columnID);
						MOrder order = new MOrder(ctx, columnID, trx);
						info = order.getDocumentNo() + "-" + requiredFormat.format(order.getDatePromised());
					}
					else
					{
						miniMRP.setM_Requisition_ID(columnID);
						MRequisition req = new MRequisition(ctx, columnID, trx);
						info = req.getDocumentNo() + "-" + requiredFormat.format(req.getDateRequired()) + ", "
								+ req.getDescription();
					}

					miniMRP.setOrderInfo(info);
					for (Integer week : weeklyDemand.keySet())
					{
						BigDecimal qty = weeklyDemand.get(week);
						setWeeklyData(miniMRP, week, qty);
						if (totalSupplyLine.containsKey(week))
						{
							qty = qty.add(totalSupplyLine.get(week));
						}
						totalSupplyLine.put(week, qty);

						BigDecimal qtyProd = weeklyDemand.get(week);
						if (subTotalSupplyLine.containsKey(week))
						{
							qtyProd = qtyProd.add(subTotalSupplyLine.get(week));
						}
						subTotalSupplyLine.put(week, qtyProd);
					}
					miniMRP.saveEx();
				}
				lineNo += 10;
			}
		}
	}

	public void insertSupplyLineKB(MiniMRPProduct miniMrpProduct, Map<Integer, BigDecimal> totalSupplyLine,
			Map<Integer, BigDecimal> subTotalSupplyLine, String typeSupply)
	{
		if (mapKanbanProduct.get(miniMrpProduct.getM_Product_ID()) == null)
			return;

		Map<Date, KanbanProduct> line = new TreeMap<Date, KanbanProduct>(
				mapKanbanProduct.get(miniMrpProduct.getM_Product_ID()));
		for (Date date : line.keySet())
		{
			KanbanProduct kp = line.get(date);
			X_MRP_RunLine miniMRP = getX_MRP_RunLine(miniMrpProduct, lineNo, typeSupply);
			miniMRP.setOrderInfo("Kanban PRD" + " - " + requiredFormat.format(date));

			int week = kp.getWk();
			BigDecimal qty = kp.getQty();
			setWeeklyData(miniMRP, week, qty);
			if (totalSupplyLine.containsKey(week))
			{
				qty = qty.add(totalSupplyLine.get(week));
			}
			totalSupplyLine.put(week, qty);

			BigDecimal qtyProd = kp.getQty();
			if (subTotalSupplyLine.containsKey(week))
			{
				qtyProd = qtyProd.add(subTotalSupplyLine.get(week));
			}
			subTotalSupplyLine.put(week, qtyProd);
			miniMRP.saveEx();
			lineNo += 10;
		}
	}
	/**
	 * Insert total Supply line, summation of confirmed purchase order and
	 * Requisition
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProduct
	 * @param subTotalSupplyLine
	 * @param isPO
	 */
	public void insertTotalSupplyLine(MiniMRPProduct miniMrpProduct, Map<Integer, BigDecimal> subTotalSupplyLine,
			String typeTotalSupply)
	{
		if (!subTotalSupplyLine.isEmpty())
		{
			X_MRP_RunLine miniMRP = getX_MRP_RunLine(miniMrpProduct, lineNo, typeTotalSupply);

			// Insert Total Supply line.
			for (Integer week : subTotalSupplyLine.keySet())
			{
				BigDecimal qty = subTotalSupplyLine.get(week);
				setWeeklyData(miniMRP, week, qty);
			}
			miniMRP.saveEx();
		}
		lineNo += 10;
	}

	/**
	 * Insert total supply of production line
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProduct
	 * @param subTotalSupplyLine
	 * @param isPlanned
	 */
	public void insertTotalSupplyProductionLine(MiniMRPProduct miniMrpProduct,
			Map<Integer, BigDecimal> subTotalSupplyLine, boolean isPlanned)
	{
		if (!subTotalSupplyLine.isEmpty())
		{
			X_MRP_RunLine miniMRP = getX_MRP_RunLine(miniMrpProduct, lineNo, isPlanned ? TYPE_TOTAL_SUPPLY_PLANNED_LINE
					: TYPE_TOTAL_SUPPLY_NONPLANNED_LINE);

			// Insert Total Supply Production as per docType.
			for (Integer week : subTotalSupplyLine.keySet())
			{
				BigDecimal qty = subTotalSupplyLine.get(week);
				setWeeklyData(miniMRP, week, qty);
			}
			miniMRP.saveEx();
		}
		lineNo += 10;
	}

	/**
	 * Insert Opening Balance Line
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProduct
	 * @param openingBalanceLine
	 */
	public void insertOpeningBalanceLine(MiniMRPProduct miniMrpProduct, Map<Integer, BigDecimal> openingBalanceLine)
	{
		if (!openingBalanceLine.isEmpty())
		{
			X_MRP_RunLine miniMRP = getX_MRP_RunLine(miniMrpProduct, 10, TYPE_OPENING_BALANCE_LINE
					+ (miniMrpProduct.isPhantom ? " - Phantom Product" : ""));
			for (Integer week : openingBalanceLine.keySet())
			{
				setWeeklyData(miniMRP, week, openingBalanceLine.get(week));
			}
			miniMRP.saveEx();
		}
	}

	/**
	 * Insert closing Balance Line.
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProduct
	 * @param closingBalanceLine
	 */
	public void insertClosingBalanceLine(MiniMRPProduct miniMrpProduct, Map<Integer, BigDecimal> closingBalanceLine)
	{
		if (!closingBalanceLine.isEmpty())
		{
			X_MRP_RunLine miniMRP = getX_MRP_RunLine(miniMrpProduct, lineNo, TYPE_CLOSING_BALANCE_LINE);
			for (Integer week : closingBalanceLine.keySet())
			{
				setWeeklyData(miniMRP, week, closingBalanceLine.get(week));
			}
			miniMRP.saveEx();
		}
	}

	/**
	 * Create a MRP Lines as per product wise processing
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProducts
	 */
	private void renderPeggingReport(Map<Integer, MiniMRPProduct> miniMrpProducts)
	{

		for (MiniMRPProduct miniMrpProduct : miniMrpProducts.values())
		{
			lineNo = 20;
			Map<Integer, BigDecimal> openingBalanceLine = new HashMap<Integer, BigDecimal>();
			Map<Integer, BigDecimal> closingBalanceLine = new HashMap<Integer, BigDecimal>();
			if (availableInventory.containsKey(miniMrpProduct.getM_Product_ID()))
			{
				BigDecimal qty = availableInventory.get(miniMrpProduct.getM_Product_ID());
				openingBalanceLine.put(START_WEEK, qty);
			}

			// Insert Demand lines & Calculate Total Demand line.
			Map<Integer, BigDecimal> totalDemandLine = insertOrderDemands(miniMrpProduct);

			// Insert SupplyLines - MO/PO & Total
			Map<Integer, BigDecimal> totalSupplyLine = insertSupplyLines(miniMrpProduct);

			calculateSuggestedOrders(miniMrpProduct, totalDemandLine, totalSupplyLine, openingBalanceLine,
					closingBalanceLine);

			// Insert Closing Balance Line
			insertClosingBalanceLine(miniMrpProduct, closingBalanceLine);

			// Insert Opening Balance Line
			insertOpeningBalanceLine(miniMrpProduct, openingBalanceLine);
		}
	}

	/**
	 * @author Sachin Bhimani
	 * @param miniMrpProducts
	 * @param productIds
	 * @param type
	 */
	private void doRunOpenOrders(Map<Integer, MiniMRPProduct> miniMrpProducts, Set<Integer> productIds, String type)
	{
		String sql = null;
		if (type.equals(TYPE_MO))
			sql = getQueryForOpenMO();
		else if (type.equals(TYPE_PO))
			sql = getQueryForOpenPO();
		else if (type.equals(TYPE_RQ))
			sql = getQueryForOpenRequisition();

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int paramCount = 1;
		try
		{
			pstmt = DB.prepareStatement(sql, trx);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setInt(paramCount++, weekRound);
			pstmt.setInt(paramCount++, weekRound);
			pstmt.setTimestamp(paramCount++, dateTo);
			pstmt.setInt(paramCount++, AD_Client_ID);
			pstmt.setArray(paramCount++, getSqlArray(productIds.toArray(), "numeric", DB.getConnectionRW(false))); // Productids
			if (type.equals(TYPE_RQ))
				pstmt.setInt(paramCount++, docType_MRPRequisition);

			rs = pstmt.executeQuery();
			while (rs.next())
			{
				int M_Product_ID = rs.getInt("m_product_id");
				BigDecimal orderQty = rs.getBigDecimal("orderedqty");
				int weekPromised = rs.getInt("weekordered");

				MiniMRPProduct product = miniMrpProducts.get(M_Product_ID);
				if (type.equals(TYPE_PO))
				{
					int c_order_id = rs.getInt("c_order_id");
					product.setSupplyLinePO(c_order_id, weekPromised, orderQty);
				}
				else if (type.equals(TYPE_MO))
				{
					int m_production_id = rs.getInt("m_production_id");
					product.setSupplyLineMO(m_production_id, weekPromised, orderQty);
				}
				else if (type.equals(TYPE_RQ))
				{
					int M_Requisition_ID = rs.getInt("M_Requisition_ID");
					product.setSupplyLineRQ(M_Requisition_ID, weekPromised, orderQty);
				}
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
			getProcessInfo().setError(true);
			getProcessInfo().addLog(
					new ProcessInfoLog(getProcessInfo().getAD_Process_ID(), new Timestamp(System.currentTimeMillis()),
							null, "Failed to fetch products for mini MRP >> " + e.getMessage()));
			throw new AdempiereException(e);

		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}

	/**
	 * @param data
	 * @param sqlTypeName
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	private Array getSqlArray(Object[] data, String sqlTypeName, Connection conn) throws SQLException
	{
		Array array;
		if (conn instanceof C3P0ProxyConnection)
		{
			C3P0ProxyConnection proxy = (C3P0ProxyConnection) conn;
			try
			{
				Method m = Connection.class.getMethod("createArrayOf", String.class, Object[].class);
				Object[] args = { sqlTypeName, data };
				array = (Array) proxy.rawConnectionOperation(m, C3P0ProxyConnection.RAW_CONNECTION, args);
			}
			catch (Exception e)
			{
				throw new SQLException(e);
			}
		}
		else
		{
			array = conn.createArrayOf(sqlTypeName, data);
		}
		return array;
	}

	/**
	 * @param miniMrpProducts
	 * @param productIds
	 * @param productSO
	 */
	@SuppressWarnings("resource")
	private void doRunProductsSO(Map<Integer, MiniMRPProduct> miniMrpProducts, Set<Integer> productIds)
	{
		String sql = getQueryForProductSO();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int paramCount = 1;
		try
		{
			pstmt = DB.prepareStatement(sql, trx);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateFrom);
			pstmt.setTimestamp(paramCount++, dateTo);
			pstmt.setInt(paramCount++, AD_Client_ID);
			pstmt.setArray(paramCount++, getSqlArray(productIds.toArray(), "numeric", DB.getConnectionRW(false))); // Productids
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				int mProductID = rs.getInt("m_product_id");
				int mOrderID = rs.getInt("c_order_id");
				BigDecimal orderQty = rs.getBigDecimal("orderedqty");
				int weekPromised = rs.getInt("weekordered");
				BigDecimal qty = rs.getBigDecimal("orderedqty");
				Date date = rs.getDate("datepromised");

				if (miniMrpProducts.containsKey(mProductID))
				{
					MiniMRPProduct mrpProduct = miniMrpProducts.get(mProductID);

					if (mrpProduct.isPhantom())
						continue;

					if (!mrpProduct.isPhantom)
						setQtyAsDemand(mProductID, qty, date);

					mrpProduct.explodeDemand(mOrderID, orderQty, weekPromised, miniMrpProducts);
				}
				else
				{
					log.log(Level.SEVERE, "Error in miniMrpProducts setup.");
					getProcessInfo().setError(true);
					getProcessInfo().addLog(
							new ProcessInfoLog(getProcessInfo().getAD_Process_ID(), new Timestamp(System
									.currentTimeMillis()), null, "Error in miniMrpProducts setup.>> "));
					rs.close();
					pstmt.close();
					throw new AdempiereException("Error in miniMrpProducts setup.");
				}
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
			getProcessInfo().setError(true);
			getProcessInfo().addLog(new ProcessInfoLog(getProcessInfo().getAD_Process_ID(), new Timestamp(System
					.currentTimeMillis()), null, "Failed to fetch products for mini MRP >> " + e.getMessage()));
			throw new AdempiereException(e);

		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}

	/**
	 * Create Product Demand map.
	 * 
	 * @param mProductID
	 * @param qty
	 * @param date
	 */
	private void setQtyAsDemand(int mProductID, BigDecimal qty, Date date)
	{
		if (date.before(dateFrom))
			date = dateFrom;
		
		if (mapDemand.containsKey(date))
		{
			Map<Integer, BigDecimal> mapOrderQty = mapDemand.get(date);
			if (mapOrderQty.containsKey(mProductID))
				mapOrderQty.put(mProductID, mapOrderQty.get(mProductID).add(qty));
			else
				mapOrderQty.put(mProductID, qty);
		}
		else
		{
			Map<Integer, BigDecimal> mapOrderQty = new HashMap<Integer, BigDecimal>();
			mapOrderQty.put(mProductID, qty);
			mapDemand.put(date, mapOrderQty);
		}
	}

	/**
	 * This method will fetch all the products based on selected category or the
	 * product. This will process BOMLine if product is finished good. It will
	 * fill the miniMRPProducts list & id of each product(including BOMlines) in
	 * productIds
	 * 
	 * @param miniMrpProducts
	 * @param productIds
	 */
	private void generateProductInfo(Map<Integer, MiniMRPProduct> miniMrpProducts, Set<Integer> productIds)
	{
		String sql = getQueryForProductDetails();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, trx);
			pstmt.setInt(1, M_PriceList_ID);
			pstmt.setInt(2, M_WarehouseID);
			pstmt.setInt(3, AD_Client_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				int mProductID = rs.getInt("m_product_id");

				if (!miniMrpProducts.containsKey(mProductID))
				{
					MiniMRPProduct miniMrpProduct = addProductToProcess(mProductID, rs, miniMrpProducts, productIds);

					/* If Product is FG(Finished Goods) calculate it's BOMlines */
					if (miniMrpProduct.isBOM() && miniMrpProduct.isVerified())
					{
						processBOMLines(miniMrpProducts, productIds, miniMrpProduct.getM_Product_ID());
					}
				}
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
			getProcessInfo().setError(true);
			getProcessInfo().addLog(new ProcessInfoLog(getProcessInfo().getAD_Process_ID(), new Timestamp(System
					.currentTimeMillis()), null, "Failed to fetch products for mini MRP >> " + e.getMessage()));
			throw new AdempiereException(e);

		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}

	/**
	 * @param mProductID
	 * @param rs
	 * @param miniMrpProducts
	 * @param productIds
	 * @return
	 * @throws SQLException
	 */
	private MiniMRPProduct addProductToProcess(int mProductID, ResultSet rs,
			Map<Integer, MiniMRPProduct> miniMrpProducts, Set<Integer> productIds) throws SQLException
	{
		String productName = rs.getString("ProductName");
		int mProductCategoryID = rs.getInt("M_Product_Category_ID");
		int projectedLeadTime = 0; // rs.getInt("deliverytime_promised");

		boolean isBOM = "Y".equalsIgnoreCase(rs.getString("IsBOM")) ? true : false;
		boolean isVerified = "Y".equalsIgnoreCase(rs.getString("IsVerified")) ? true : false;
		boolean isPurchased = "Y".equalsIgnoreCase(rs.getString("IsPurchased")) ? true : false;
		boolean isPhantom = "Y".equalsIgnoreCase(rs.getString("IsPhantom")) ? true : false;
		boolean isKanban = "Y".equalsIgnoreCase(rs.getString("IsKanban")) ? true : false;		

		BigDecimal Level_Min = rs.getBigDecimal("Level_Min");
		BigDecimal availableInventory = rs.getBigDecimal("Available");

		if (availableInventory == null)
			availableInventory = Env.ZERO;

		if (isPhantom)
		{
			Level_Min = Env.ZERO;
			availableInventory = Env.ZERO;
		}

		MiniMRPProduct miniMrpProduct = new MiniMRPProduct(mProductID);
		miniMrpProduct.setAvailableQty(availableInventory);
		miniMrpProduct.setName(productName);
		miniMrpProduct.setM_Product_Category_ID(mProductCategoryID);
		miniMrpProduct.setProjectedLeadTime(projectedLeadTime);
		miniMrpProduct.setBOM(isBOM);
		miniMrpProduct.setVerified(isVerified);
		miniMrpProduct.setPurchased(isPurchased);
		miniMrpProduct.setPhantom(isPhantom);
		miniMrpProduct.setKanban(isKanban);
		miniMrpProduct.setC_BPartner_ID(rs.getInt("C_BPartner_ID"));
		miniMrpProduct.setC_BP_Group_ID(rs.getInt("C_BP_Group_ID"));
		miniMrpProduct.setPO_PriceList_ID(rs.getInt("PO_PriceList_ID"));
		miniMrpProduct.setPriceActual(rs.getBigDecimal("PricePO"));
		miniMrpProduct.setQtyBatchSize(rs.getBigDecimal("QtyBatchSize"));
		miniMrpProduct.setLevel_Min(Level_Min);
		miniMrpProduct.setReplenishTypeMRPCalculated(rs.getString("ReplenishType")
				.equals(REPLENISH_TYPE_MRP_CALCULATED));
		if (miniMrpProduct.isReplenishTypeMRPCalculated())
			miniMrpProduct.setQtyOnHand(availableInventory.subtract(Level_Min));
		else
			miniMrpProduct.setQtyOnHand(availableInventory);

		// Check product QTY is negative then create demand.
		if (miniMrpProduct.getQtyOnHand().compareTo(Env.ZERO) < 0 && !isPhantom)
		{
			setQtyAsDemand(mProductID, miniMrpProduct.getQtyOnHand().negate(), dateFrom);
			miniMrpProduct.setQtyOnHand(Env.ZERO);
		}

		// Manage Inventory & ProductId blueprint here.
		productIds.add(mProductID);
		miniMrpProducts.put(mProductID, miniMrpProduct);
		addAvailableInventory(mProductID, availableInventory);

		// Retrieve Confirmed Product QTY. If non BOM product then retrieve all
		// requisition data for docType is 'MRP Requisition'.
		setConfirmProductQty(miniMrpProduct);

		return miniMrpProduct;
	}

	/**
	 * @param miniMrpProduct
	 * @param parentProduct
	 * @param qtyBom
	 */
	private void explodeRequiredMaterials(MiniMRPProduct miniMrpProduct, MiniMRPProduct parentProduct, BigDecimal qtyBom)
	{
		Map<Integer, BigDecimal> requiredProdMaterials = miniMrpProduct.getRequiredProdMaterials();
		for (Integer mProdId : requiredProdMaterials.keySet())
		{
			BigDecimal qty = requiredProdMaterials.get(mProdId);
			parentProduct.addMatireals(mProdId, qtyBom.multiply(qty));
		}
	}

	/**
	 * Process of BOM Product lines
	 * 
	 * @author Sachin Bhimani
	 * @param miniMrpProducts
	 * @param productIds
	 * @param M_Product_ID
	 */
	public void processBOMLines(Map<Integer, MiniMRPProduct> miniMrpProducts, Set<Integer> productIds, int M_Product_ID)
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			if (M_Product_ID > 0)
			{
				pstmt = DB.prepareStatement(SQL_GET_BOMLINE_FOR_PROCESS, trx);
				pstmt.setInt(1, M_PriceList_ID);
				pstmt.setInt(2, M_Product_ID);
				pstmt.setInt(3, AD_Client_ID);
				pstmt.setInt(4, M_WarehouseID);

				rs = pstmt.executeQuery();
				while (rs.next())
				{
					int mProductID = rs.getInt("M_ProductBOM_ID");
					BigDecimal qtyBom = rs.getBigDecimal("BomQTY");

					MiniMRPProduct parentProduct = miniMrpProducts.get(M_Product_ID);
					parentProduct.addMatireals(mProductID, qtyBom);

					MiniMRPProduct miniMrpProduct = null;

					// If material is already exploded.
					if (miniMrpProducts.containsKey(mProductID))
					{
						miniMrpProduct = miniMrpProducts.get(mProductID);
						explodeRequiredMaterials(miniMrpProduct, parentProduct, qtyBom);
					}
					else
					{
						miniMrpProduct = addProductToProcess(mProductID, rs, miniMrpProducts, productIds);
						if (miniMrpProduct.isBOM() && miniMrpProduct.isVerified())
						{
							processBOMLines(miniMrpProducts, productIds, mProductID);
							explodeRequiredMaterials(miniMrpProduct, parentProduct, qtyBom);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, SQL_GET_BOMLINE_FOR_PROCESS, e);
			getProcessInfo().setError(true);
			getProcessInfo().addLog(new ProcessInfoLog(getProcessInfo().getAD_Process_ID(), new Timestamp(System
					.currentTimeMillis()), null, "Failed to process BOMLine : >> " + e.getMessage()));
			throw new AdempiereException(e);

		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}

	/**
	 * @return
	 */
	private String getQueryForProductDetails()
	{
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT	p.M_Product_ID, p.Name AS ProductName, p.M_Product_Category_ID, mpo.DeliveryTime_Promised, p.IsBOM, p.IsVerified, SUM(ms.QtyOnHand) AS Available, p.IsPurchased, "
				+ " mpo.C_BPartner_ID, COALESCE(ppr.pricelist, 0) AS PricePO, p.IsPhantom, COALESCE(r.QtyBatchSize, 0) AS QtyBatchSize, COALESCE(r.Level_Min, 0) AS Level_Min, r.ReplenishType, " 
				+ " bp.C_BP_Group_ID, p.IsKanban, COALESCE(bp.PO_PriceList_ID, ?) AS PO_PriceList_ID ");			
		sql.append(" FROM M_Product p ");
		sql.append(" INNER JOIN M_Product_Category mpc ON (p.M_Product_Category_ID = mpc.M_Product_Category_ID) ");
		sql.append(" INNER JOIN M_Replenish r ON (r.M_Product_ID = p.M_Product_ID AND r.M_Warehouse_ID = ? )");
		sql.append(" LEFT JOIN M_Product_PO mpo ON (mpo.M_Product_ID = p.M_Product_ID AND mpo.C_BPartner_ID = ");
		sql.append("		(SELECT po.C_BPartner_ID FROM M_Product_PO po WHERE po.M_Product_ID=p.M_Product_ID AND po.IsActive = 'Y' ORDER BY IsCurrentVendor Desc FETCH FIRST ROW ONLY)) ");
		sql.append(" LEFT JOIN C_BPartner bp ON (bp.C_BPartner_ID = mpo.C_BPartner_ID) ");
		sql.append(" LEFT JOIN M_ProductPrice ppr ON  (p.m_product_id = ppr.m_product_id)  AND  ppr.m_pricelist_version_id = ");
		sql.append(" (SELECT plv.m_pricelist_version_id FROM m_pricelist_version plv ");
		sql.append(" LEFT JOIN c_bpartner bpx ON plv.m_pricelist_id = bpx.po_pricelist_id ");
		sql.append(" WHERE (bpx.c_bpartner_id = mpo.c_bpartner_id AND plv.ValidFrom <= now())  ORDER BY plv.m_pricelist_version_id DESC FETCH FIRST ROW ONLY) ");
		sql.append(" LEFT OUTER JOIN M_Storage ms ON (ms.M_Product_ID = p.M_Product_ID) ");
		sql.append(" WHERE p.IsActive='Y' AND p.AD_Client_ID = ? ");
		sql.append(" GROUP BY p.M_Product_ID, mpo.DeliveryTime_Promised, mpo.C_BPartner_ID, bp.C_BPartner_ID, ppr.pricelist, r.M_Product_ID, r.M_Warehouse_ID ");
		sql.append(" ORDER BY p.M_Product_ID ");

		return sql.toString();
	}

	/**
	 * @return
	 */
	private String getQueryForProductSO()
	{
		StringBuilder sql = new StringBuilder(
				"SELECT col.c_order_id, mp.m_product_id, mp.name as productname, (col.qtyordered-col.qtydelivered) as orderedqty,");
		sql.append(" CASE WHEN ?::DATE >= col.datepromised::DATE THEN EXTRACT(WEEK FROM ?::DATE) ELSE( ");
		sql.append("(CASE WHEN TO_CHAR(?::DATE, 'IYYY') <> TO_CHAR(col.datepromised::DATE, 'IYYY')	THEN EXTRACT(WEEK FROM (DATE_TRUNC('YEAR', col.datepromised::DATE) + interval '-1 day')) ELSE 0 END) + EXTRACT(WEEK FROM col.datepromised::DATE)) END AS weekordered,");
		sql.append(" co.datepromised FROM c_order co JOIN c_orderline col ON (co.c_order_id=col.c_order_id) JOIN m_product mp ON(mp.m_product_id=col.m_product_id) JOIN m_product_category mpc ON mp.m_product_category_id = mpc.m_product_category_id WHERE mp.isactive='Y'  AND co.issotrx='Y' AND col.datepromised <= ? ");
		sql.append(" AND co.DocStatus IN ('CO') ");
		sql.append(" AND (col.qtyordered - col.qtydelivered) != 0");
		sql.append(" AND mp.AD_Client_ID=?");
		sql.append(" AND col.m_product_id = ANY(?)");
		sql.append(" ORDER BY co.datepromised ");
		return sql.toString();
	}

	private String getQueryForOpenPO()
	{
		String sql = "SELECT C_Order_ID, M_Product_ID, ProductName, SUM(OrderedQty) AS OrderedQty, "
				+ " 	CASE WHEN ?::DATE >= DtPromised::DATE THEN EXTRACT(WEEK FROM ?::DATE) ELSE (  "
				+ " 	(CASE WHEN TO_CHAR(?::DATE, 'IYYY') <> TO_CHAR(DtPromised::DATE, 'IYYY')"
				+ "		THEN EXTRACT(WEEK FROM (DATE_TRUNC('YEAR', DtPromised::DATE) + interval '-1 day')) ELSE 0 END) + EXTRACT(WEEK FROM DtPromised::DATE)) END AS WeekOrdered "
				+ " FROM ("
				+ "		SELECT co.C_Order_ID, mp.M_Product_ID, mp.Name AS ProductName, SUM(col.QtyOrdered - col.QtyDelivered) AS OrderedQty, "
				+ "			ADDDAYS(?::TIMESTAMP, (CASE WHEN col.DatePromised < ? THEN 0 ELSE (DAYSBETWEEN(col.DatePromised, ?) / 7 / ?) * ? * 7 END)) AS DtPromised "
				+ "		FROM C_Order co "
				+ "		JOIN C_Orderline col ON (co.C_Order_ID=col.C_Order_ID) "
				+ "		JOIN M_Product mp ON(mp.M_Product_ID=col.M_Product_ID) "
				+ "		WHERE mp.IsActive='Y' AND co.IsSOTrx='N' AND DocStatus IN ('CO','CL') AND (col.QtyOrdered-col.QtyDelivered)!=0 AND col.DatePromised<=? AND mp.AD_Client_ID=? AND col.M_Product_ID=ANY(?) "
				+ "		GROUP BY DtPromised, mp.M_Product_ID, co.C_Order_ID "
				+ " ) AS OpenPO "
				+ " GROUP BY C_Order_ID, M_Product_ID, ProductName, WeekOrdered, DtPromised "
				+ " ORDER BY M_Product_ID, DtPromised ";
		return sql;
	}

	private String getQueryForOpenMO()
	{
		String sql = "SELECT M_Production_ID, M_Product_ID, ProductName, SUM(OrderedQty) AS OrderedQty, "
				+ "		CASE WHEN ?::DATE >= DatePromised::DATE THEN EXTRACT(WEEK FROM ?::DATE) "
				+ " 	ELSE ((CASE WHEN TO_CHAR( ?::DATE, 'IYYY') <> TO_CHAR(DatePromised::DATE, 'IYYY') "
				+ "				THEN EXTRACT(WEEK FROM (DATE_TRUNC('YEAR', DatePromised::DATE) + interval '-1 day')) ELSE 0 END) + EXTRACT(WEEK FROM DatePromised::DATE)) END AS WeekOrdered "
				+ "	FROM ( "
				+ "		SELECT mprod.M_Production_ID, mp.M_Product_ID, mp.Name AS ProductName, SUM(mprod.ProductionQty) AS OrderedQty, "
				+ "			ADDDAYS(?::TIMESTAMP, (CASE WHEN mprod.DatePromised < ? THEN 0 ELSE (DAYSBETWEEN(mprod.DatePromised, ?) / 7 / ?) * ? * 7 END)) AS DatePromised "
				+ "		FROM M_Production mprod" 
				+ " 	JOIN M_Product mp ON(mp.M_Product_ID=mprod.M_Product_ID) "
				+ "		JOIN M_Product_Category mpc ON mp.M_Product_Category_ID = mpc.M_Product_Category_ID "
				+ " 	WHERE mp.IsActive='Y' AND mprod.IsActive='Y' AND mprod.Processed='N' AND mprod.DatePromised <= ? AND mprod.AD_Client_ID = ? AND mprod.m_product_id =ANY(?) "
				+ " 	GROUP BY mp.M_Product_ID, mprod.M_Production_ID" 
				+ "	) AS OpenMO "
				+ " GROUP BY WeekOrdered, M_Product_ID, ProductName, M_Production_ID, DatePromised " 
				+ " ORDER BY M_Product_ID, DatePromised ";

		return sql;
	}

	private String getQueryForBOMProductExplode()
	{
		StringBuilder sql = new StringBuilder("SELECT M_ProductBOM_ID, BOMQty * ?::Numeric AS RequiredQty ")
				.append("FROM M_Product_BOM WHERE M_Product_ID = ?");

//		StringBuilder sql = new StringBuilder(" WITH RECURSIVE supplytree(M_Product_ID) AS ( ")
//				.append(" 	SELECT M_Product_ID, M_ProductBOM_ID, BOMQty, BOMQty * ?::Numeric AS RequiredQty, 1 AS Level ")
//				.append("	FROM M_Product_BOM	WHERE M_Product_ID = ? ")
//				.append(" UNION ALL ")
//				.append("	SELECT b.M_Product_ID, b.M_ProductBOM_ID, b.BOMQty, b.BOMQty * sp.RequiredQty AS RequiredQty, Level + 1 ")
//				.append("	FROM M_Product_BOM AS b INNER JOIN supplytree AS sp ON (sp.M_ProductBOM_ID = b.M_Product_ID)	WHERE b.IsActive='Y' ")
//				.append(" ) ")
//				.append(" SELECT  M_Product_ID, M_ProductBOM_ID, BOMQty, RequiredQty, Level		FROM supplytree	WHERE Level = 1ORDER BY Level ");

		return sql.toString();
	}


	private String getQueryForOpenRequisition()
	{
		String sql = "SELECT M_Requisition_ID, M_Product_ID, ProductName, SUM(OrderedQty) AS OrderedQty, "
				+ "		CASE WHEN ?::DATE >= DateRequired::DATE THEN EXTRACT(WEEK FROM ?::DATE) ELSE ( "
				+ "		(CASE WHEN TO_CHAR(?::DATE, 'IYYY') <> TO_CHAR(DateRequired::DATE, 'IYYY') "
				+ "			THEN EXTRACT(WEEK FROM (DATE_TRUNC('YEAR', DateRequired::DATE) + interval '-1 day')) ELSE 0 END) + EXTRACT(WEEK FROM DateRequired::DATE)) END AS WeekOrdered "
				+ " FROM ( "
				+ "		SELECT r.M_Requisition_ID, mp.M_Product_ID, mp.Name AS ProductName, SUM(rl.Qty) AS OrderedQty, "
				+ "		ADDDAYS(?::TIMESTAMP, (CASE WHEN r.DateRequired < ? THEN 0 ELSE (DAYSBETWEEN(r.DateRequired, ?) / 7 / ?) * ? * 7 END)) AS DateRequired "
				+ "		FROM M_Requisition r "
				+ "		INNER JOIN M_RequisitionLine rl ON (r.M_Requisition_ID = rl.M_Requisition_ID AND rl.IsActive = 'Y') "
				+ "		INNER JOIN M_Product mp ON (mp.M_Product_ID = rl.M_Product_ID AND mp.IsActive = 'Y') "
				+ "		WHERE  r.DocStatus IN ('DR') AND r.DateRequired <= ? AND mp.AD_Client_ID = ? AND rl.M_Product_ID =ANY(?) AND r.C_DocType_ID = ? "
				+ "		GROUP BY mp.M_Product_ID, r.M_Requisition_ID "
				+ " ) AS OpenRQ "
				+ " GROUP BY M_Requisition_ID, M_Product_ID, ProductName, DateRequired "
				+ " ORDER BY M_Product_ID, DateRequired ";
		return sql;
	}

	class MiniMRPProduct
	{
		private MiniMRPProduct							parent;

		private int										M_Product_ID;
		private int										projectedLeadTimeInWeek	= 1;
		private int										M_Product_Category_ID;
		private int										projectedLeadTime;
		private int										C_BPartner_ID;
		private int										C_BP_Group_ID;
		private int										PO_PriceList_ID;
		private String									name;
		private boolean									isBOM;
		private boolean									isVerified;
		private boolean									isPurchased;
		private boolean									isPhantom;
		private boolean									isKanban;
		private boolean									isReplenishTypeMRPCalculated;
		private BigDecimal								availableQty;
		private BigDecimal								qtyBal;
		private BigDecimal								qtyOnHand				= Env.ZERO;
		private BigDecimal								priceActual				= Env.ZERO;
		private BigDecimal								qtyBatchSize			= Env.ZERO;
		private BigDecimal								level_Min				= Env.ZERO;
		private BigDecimal								extraQtyPlanned			= Env.ZERO;
		private Map<Integer, BigDecimal>				requiredProdMaterials	= new HashMap<Integer, BigDecimal>();
		private Map<Integer, Map<Integer, BigDecimal>>	supplyLinePO			= new TreeMap<Integer, Map<Integer, BigDecimal>>();
		private Map<Integer, Map<Integer, BigDecimal>>	supplyLineMO			= new TreeMap<Integer, Map<Integer, BigDecimal>>();
		private Map<Integer, Map<Integer, BigDecimal>>	supplyLineRQ			= new TreeMap<Integer, Map<Integer, BigDecimal>>();
		private Map<Integer, Map<Integer, BigDecimal>>	demand					= new LinkedHashMap<Integer, Map<Integer, BigDecimal>>();
		private Map<Date, BigDecimal>					mapConfirmedProductQty	= new HashMap<Date, BigDecimal>();

		public String getName()
		{
			return name;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public Map<Integer, Map<Integer, BigDecimal>> getSupplyLinePO()
		{
			return supplyLinePO;
		}

		public Map<Integer, Map<Integer, BigDecimal>> getSupplyLineMO()
		{
			return supplyLineMO;
		}

		public Map<Integer, Map<Integer, BigDecimal>> getSupplyLineRQ()
		{
			return supplyLineRQ;
		}

		public int hashCode()
		{
			return new HashCodeBuilder(17, 31).append(M_Product_ID).toHashCode();
		}

		public boolean equals(Object obj)
		{
			if (!(obj instanceof MiniMRPProduct))
				return false;
			if (obj == this)
				return true;

			MiniMRPProduct rhs = (MiniMRPProduct) obj;
			return new EqualsBuilder().append(M_Product_ID, rhs.M_Product_ID).isEquals();
		}

		public void addMatireals(int productId, BigDecimal qty)
		{
			if (requiredProdMaterials.containsKey(productId))
				qty = qty.add(requiredProdMaterials.get(productId));
			requiredProdMaterials.put(productId, qty);
		}

		/**
		 * @param weekPromised
		 * @return
		 */
		private int getOrderProjectedWeek(int weekPromised)
		{
			int projectdOrderWeek = 0;
			if (START_WEEK > END_WEEK)
			{
				weekPromised = weekPromised + 52;
			}
			if (START_WEEK == weekPromised || (weekPromised - getProjectedLeadTimeInWeek()) < START_WEEK)
			{
				projectdOrderWeek = START_WEEK;
			}
			else
			{
				projectdOrderWeek = weekPromised - getProjectedLeadTimeInWeek();
			}

			return projectdOrderWeek;
		}

		public void addDemand(int mOrderID, BigDecimal netRequiredQty, int projectdOrderWeek)
		{
			Map<Integer, BigDecimal> weekDemand = getOrderDemand(mOrderID);
			weekDemand.put(projectdOrderWeek, netRequiredQty);
		}

		public void explodeDemand(int mOrderID, BigDecimal orderQty, int weekPromised,
				Map<Integer, MiniMRPProduct> miniMrpProducts)
		{
			BigDecimal netRequiredQty = orderQty; // explod(orderQty);
			// int projectdOrderWeek = getOrderProjectedWeek(weekPromised);
			addDemand(mOrderID, netRequiredQty, weekPromised);
			// explodMaterialDemand(mOrderID, orderQty, weekPromised,
			// miniMrpProducts);
		}

		public Map<Integer, Map<Integer, BigDecimal>> getDemand()
		{
			return demand;
		}

		public Map<Integer, BigDecimal> getOrderDemand(int mOrderID)
		{
			Map<Integer, BigDecimal> weekDemand = null;
			if (demand.containsKey(mOrderID))
			{
				weekDemand = demand.get(mOrderID);
			}
			else
			{
				weekDemand = new HashMap<Integer, BigDecimal>();
				demand.put(mOrderID, weekDemand);
			}
			return weekDemand;
		}

		public void setSupplyLinePO(int c_order_id, int week, BigDecimal openPOqty)
		{
			Map<Integer, BigDecimal> weekDemand = null;
			if (supplyLinePO.containsKey(c_order_id))
			{
				weekDemand = supplyLinePO.get(c_order_id);
				openPOqty = openPOqty.add(getWeekDemand(weekDemand, week));
				weekDemand.put(week, openPOqty);
			}
			else
			{
				weekDemand = new HashMap<Integer, BigDecimal>();
				weekDemand.put(week, openPOqty);
			}
			supplyLinePO.put(c_order_id, weekDemand);
		}

		public void setSupplyLineMO(int m_production_id, int week, BigDecimal openMOqty)
		{
			Map<Integer, BigDecimal> weekDemand = null;
			if (supplyLineMO.containsKey(m_production_id))
			{
				weekDemand = supplyLineMO.get(m_production_id);
				openMOqty = openMOqty.add(getWeekDemand(weekDemand, week));
				weekDemand.put(week, openMOqty);
			}
			else
			{
				weekDemand = new HashMap<Integer, BigDecimal>();
				weekDemand.put(week, openMOqty);
			}
			supplyLineMO.put(m_production_id, weekDemand);
		}

		public void setSupplyLineRQ(int m_requisition_id, int week, BigDecimal openRQqty)
		{
			Map<Integer, BigDecimal> weekDemand = null;
			if (supplyLineRQ.containsKey(m_requisition_id))
			{
				weekDemand = supplyLineRQ.get(m_requisition_id);
				openRQqty = openRQqty.add(getWeekDemand(weekDemand, week));
				weekDemand.put(week, openRQqty);
			}
			else
			{
				weekDemand = new HashMap<Integer, BigDecimal>();
				weekDemand.put(week, openRQqty);
			}
			supplyLineRQ.put(m_requisition_id, weekDemand);
		}

		private BigDecimal getWeekDemand(Map<Integer, BigDecimal> weekDemand, int week) {
			BigDecimal demand = weekDemand.get(week);
			if (demand == null) {
				return Env.ZERO;
			}
			return demand;
		}

		public BigDecimal explod(BigDecimal totalRequiredQty)
		{
			if (getQtyBal().compareTo(Env.ZERO) != 0)
			{
				BigDecimal avaibleInventory = getAvailableQty();
				if (avaibleInventory.compareTo(totalRequiredQty) > 0)
				{
					setAvailableQty(avaibleInventory.subtract(totalRequiredQty));
					totalRequiredQty = Env.ZERO;
				}
				else
				{
					totalRequiredQty = totalRequiredQty.subtract(avaibleInventory);
					setAvailableQty(Env.ZERO);
				}
			}
			return totalRequiredQty;
		}

		/**
		 * @param mOrderID
		 * @param orderQty
		 * @param miniMrpProducts
		 * @param projectdOrderWeek
		 */
		public void explodMaterialDemand(int mOrderID, BigDecimal orderQty, int weekPromised,
				Map<Integer, MiniMRPProduct> miniMrpProducts)
		{
			if (!requiredProdMaterials.isEmpty())
			{
				for (Integer productId : requiredProdMaterials.keySet())
				{
					MiniMRPProduct materials = miniMrpProducts.get(productId);
					BigDecimal totalRequiredQty = requiredProdMaterials.get(productId).multiply(orderQty);
					int projectdOrderWeek = materials.getOrderProjectedWeek(weekPromised);
					materials.addDemand(mOrderID, totalRequiredQty, projectdOrderWeek);
				}
			}
		}

		public boolean hasParent()
		{
			return parent != null;
		}

		public int getM_Product_ID()
		{
			return M_Product_ID;
		}

		public MiniMRPProduct(int M_Product_ID)
		{
			this.M_Product_ID = M_Product_ID;
		}

		public boolean isVerified()
		{
			return isVerified;
		}

		public void setVerified(boolean isVerified)
		{
			this.isVerified = isVerified;
		}

		public int getProjectedLeadTime()
		{
			return projectedLeadTime;
		}

		public int getProjectedLeadTimeInWeek()
		{
			return projectedLeadTimeInWeek;
		}

		public void setProjectedLeadTime(int projectedLeadTime)
		{
			this.projectedLeadTime = projectedLeadTime;

			if (!isBOM()) // For other "project Lead Time in Week" will be 1
							// week flat.
			{
				if (projectedLeadTime > 0)
				{
					float projectedLTimeInWeek = projectedLeadTime / 7f;
					if ((projectedLTimeInWeek - (int) projectedLTimeInWeek) > 0)
					{
						setProjectedLeadTimeInWeek(((int) projectedLTimeInWeek) + 1);
					}
					else
					{
						setProjectedLeadTimeInWeek(((int) projectedLTimeInWeek));
					}
				}
			}
		}

		public void setProjectedLeadTimeInWeek(int projectedLeadTimeWeek)
		{
			this.projectedLeadTimeInWeek = projectedLeadTimeWeek;
		}

		public BigDecimal getAvailableQty()
		{
			return availableQty;
		}

		public void setAvailableQty(BigDecimal availableQty)
		{
			this.availableQty = availableQty;
			setQtyBal(availableQty);
		}

		public int getM_Product_Category_ID()
		{
			return M_Product_Category_ID;
		}

		public void setM_Product_Category_ID(int m_Product_Category_ID)
		{
			M_Product_Category_ID = m_Product_Category_ID;
		}

		public Map<Integer, BigDecimal> getRequiredProdMaterials()
		{
			return requiredProdMaterials;
		}

		public boolean isBOM()
		{
			return isBOM;
		}

		public void setBOM(boolean isBOM)
		{
			this.isBOM = isBOM;
		}

		public BigDecimal getQtyBal()
		{
			return qtyBal;
		}

		public void setQtyBal(BigDecimal qtyBal)
		{
			this.qtyBal = qtyBal;
		}

		public BigDecimal getQtyOnHand()
		{
			return qtyOnHand;
		}

		public void setQtyOnHand(BigDecimal qtyOnHand)
		{
			this.qtyOnHand = qtyOnHand;
		}

		public Map<Date, BigDecimal> getMapConfirmProductQty()
		{
			return mapConfirmedProductQty;
		}

		public void setMapConfirmProductQty(Map<Date, BigDecimal> mapConfirmProductionQty)
		{
			this.mapConfirmedProductQty = mapConfirmProductionQty;
		}

		public void addConfirmProductionQty(Date date, BigDecimal qty)
		{
			if (mapConfirmedProductQty.containsKey(date))
				qty = mapConfirmedProductQty.get(date).add(qty);
			mapConfirmedProductQty.put(date, qty);
		}

		public boolean isPurchased()
		{
			return isPurchased;
		}

		public void setPurchased(boolean isPurchased)
		{
			this.isPurchased = isPurchased;
		}

		public int getC_BPartner_ID()
		{
			return C_BPartner_ID;
		}

		public void setC_BPartner_ID(int c_BPartner_ID)
		{
			C_BPartner_ID = c_BPartner_ID;
		}

		public BigDecimal getPriceActual()
		{
			return priceActual;
		}

		public void setPriceActual(BigDecimal priceActual)
		{
			this.priceActual = priceActual;
		}

		public boolean isPhantom()
		{
			return isPhantom;
		}

		public void setPhantom(boolean isPhantom)
		{
			this.isPhantom = isPhantom;
		}

		public BigDecimal getQtyBatchSize()
		{
			return qtyBatchSize;
		}

		public void setQtyBatchSize(BigDecimal qtyBatchSize)
		{
			this.qtyBatchSize = qtyBatchSize;
		}

		public BigDecimal getLevel_Min()
		{
			return level_Min;
		}

		public void setLevel_Min(BigDecimal level_Min)
		{
			this.level_Min = level_Min;
		}

		public BigDecimal getExtraQtyPlanned()
		{
			return extraQtyPlanned;
		}

		public void setExtraQtyPlanned(BigDecimal extraQtyPlanned)
		{
			this.extraQtyPlanned = extraQtyPlanned;
		}

		public boolean isReplenishTypeMRPCalculated()
		{
			return isReplenishTypeMRPCalculated;
		}

		public void setReplenishTypeMRPCalculated(boolean isReplenishTypeMRPCalculated)
		{
			this.isReplenishTypeMRPCalculated = isReplenishTypeMRPCalculated;
		}

		public int getC_BP_Group_ID()
		{
			return C_BP_Group_ID;
		}

		public void setC_BP_Group_ID(int c_BP_Group_ID)
		{
			C_BP_Group_ID = c_BP_Group_ID;
		}

		public boolean isKanban()
		{
			return isKanban;
		}

		public void setKanban(boolean isKanban)
		{
			this.isKanban = isKanban;
		}

		public int getPO_PriceList_ID()
		{
			return PO_PriceList_ID;
		}

		public void setPO_PriceList_ID(int pO_PriceList_ID)
		{
			PO_PriceList_ID = pO_PriceList_ID;
		}
	}

	/**
	 * @param week
	 * @param qty
	 */
	public void setWeeklyData(X_MRP_RunLine miniMRP, int week, BigDecimal qty)
	{
		int wk = week - START_WEEK;

		switch (wk)
		{
			case 0:
				miniMRP.setWeek1(qty);
				break;
			case 1:
				miniMRP.setWeek2(qty);
				break;
			case 2:
				miniMRP.setWeek3(qty);
				break;
			case 3:
				miniMRP.setWeek4(qty);
				break;
			case 4:
				miniMRP.setWeek5(qty);
				break;
			case 5:
				miniMRP.setWeek6(qty);
				break;
			case 6:
				miniMRP.setWeek7(qty);
				break;
			case 7:
				miniMRP.setWeek8(qty);
				break;
			case 8:
				miniMRP.setWeek9(qty);
				break;
			case 9:
				miniMRP.setWeek10(qty);
				break;
			case 10:
				miniMRP.setWeek11(qty);
				break;
			case 11:
				miniMRP.setWeek12(qty);
				break;
			case 12:
				miniMRP.setWeek13(qty);
				break;
			case 13:
				miniMRP.setWeek14(qty);
				break;
			case 14:
				miniMRP.setWeek15(qty);
				break;
			case 15:
				miniMRP.setWeek16(qty);
				break;
			case 16:
				miniMRP.setWeek17(qty);
				break;
			case 17:
				miniMRP.setWeek18(qty);
				break;
			case 18:
				miniMRP.setWeek19(qty);
				break;
			case 19:
				miniMRP.setWeek20(qty);
				break;
			case 20:
				miniMRP.setWeek21(qty);
				break;
			case 21:
				miniMRP.setWeek22(qty);
				break;
			case 22:
				miniMRP.setWeek23(qty);
				break;
			case 23:
				miniMRP.setWeek24(qty);
				break;
			default:
				break;
		}
	}

	/**
	 * makes it easier to print debug info for optimization
	 * @param msg
	 */
	private void printlog(String msg)
	{
		log.severe(msg);
	}
	
	class KanbanProduct
	{
		private int			M_Product_ID;
		private int			M_Product_Category_ID;
		private int			C_BPartner_ID;
		private int			C_BP_Group_ID;
		private Date		RequiredDate;
		private BigDecimal	Qty;
		private int			wk;

		public KanbanProduct(int M_Product_ID, BigDecimal Qty)
		{
			this.M_Product_ID = M_Product_ID;
			this.setQty(Qty);
			this.setRequiredDate(RequiredDate);
		}

		public void setWeek(Date date, Timestamp dateFrom)
		{
			int wk = DB.getSQLValue(trx,
					"SELECT CASE WHEN ?::DATE >= ?::DATE THEN EXTRACT(WEEK FROM ?::DATE) ELSE ((CASE WHEN TO_CHAR(?::DATE, 'IYYY') <> TO_CHAR(?::DATE, 'IYYY')"
							+ " THEN EXTRACT(WEEK FROM (DATE_TRUNC('YEAR', ?::DATE) + interval '-1 day')) ELSE 0 END) + EXTRACT(WEEK FROM ?::DATE)) END AS week ",
					dateFrom, date, dateFrom, dateFrom, date, date, date);
			setRequiredDate(date);
			setWk(wk);
		}

		public int hashCode()
		{
			return new HashCodeBuilder(17, 31).append(M_Product_ID).toHashCode();
		}

		public boolean equals(Object obj)
		{
			if (!(obj instanceof KanbanProduct))
				return false;
			if (obj == this)
				return true;

			KanbanProduct rhs = (KanbanProduct) obj;
			return new EqualsBuilder().append(M_Product_ID, rhs.M_Product_ID).isEquals();
		}

		public int getM_Product_Category_ID()
		{
			return M_Product_Category_ID;
		}

		public void setM_Product_Category_ID(int m_Product_Category_ID)
		{
			M_Product_Category_ID = m_Product_Category_ID;
		}

		public int getC_BPartner_ID()
		{
			return C_BPartner_ID;
		}

		public void setC_BPartner_ID(int c_BPartner_ID)
		{
			C_BPartner_ID = c_BPartner_ID;
		}

		public int getC_BP_Group_ID()
		{
			return C_BP_Group_ID;
		}

		public void setC_BP_Group_ID(int c_BP_Group_ID)
		{
			C_BP_Group_ID = c_BP_Group_ID;
		}

		public Date getRequiredDate()
		{
			return RequiredDate;
		}

		public void setRequiredDate(Date requiredDate)
		{
			RequiredDate = requiredDate;
		}

		public BigDecimal getQty()
		{
			return Qty;
		}

		public void setQty(BigDecimal qty)
		{
			Qty = qty;
		}

		public int getWk()
		{
			return wk;
		}

		public void setWk(int wk)
		{
			this.wk = wk;
		}
	}
}
