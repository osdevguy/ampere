/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Low Heng Sin                                         * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Low Heng Sin (hengsin@users.sourceforge.net)                    *
 *                                                                    *
 * Sponsors:                                                          *
 *  - Idalica (http://www.idalica.com/)                               *
 **********************************************************************/
package org.adempiere.process;

import org.compiere.model.X_C_EDIFormat_LineElement;
import org.compiere.model.X_C_EDIFormat_LineElementComp;

/**
 * @author Paul Bowden, Adaxa Pty Ltd
 * 
 * based on LookupEventParam
 * @author Low Heng Sin
 */
public class ElementEventParam {

	private X_C_EDIFormat_LineElement lineElement;
	private X_C_EDIFormat_LineElementComp[] lineElementComps;
	private String element;
	private String[] components;
	
	public String getElement() {
		return element;
	}

	public String[] getComponents() {
		return components;
	}

	/**
	 * 
	 * @param lineElement Line Element
	 * @param input String value
	 */
	public ElementEventParam(X_C_EDIFormat_LineElement lineElement, String element) {
		super();
		this.lineElement = lineElement;
		this.element = element;
	}

	/**
	 * 
	 * @param lineElementComps Line Element Components
	 * @param input String array of components
	 */
	public ElementEventParam(X_C_EDIFormat_LineElement lineElement, String element, X_C_EDIFormat_LineElementComp[] lineElementComps, String[] components) {
		super();
		this.lineElement = lineElement;
		this.element = element;
		this.lineElementComps = lineElementComps;
		this.components = components;
	}

	/**
	 * @return lineElement
	 */
	public X_C_EDIFormat_LineElement getLineElement() {
		return lineElement;
	}
	
	public X_C_EDIFormat_LineElementComp[] getLineElementComps() {
		return lineElementComps;
	}

	
}
