package org.adempiere.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPGroup;
import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.METLSetup;
import org.compiere.model.MGLCategory;
import org.compiere.model.MLocation;
import org.compiere.model.MLocator;
import org.compiere.model.MOrg;
import org.compiere.model.MPriceList;
import org.compiere.model.MProductCategory;
import org.compiere.model.MRole;
import org.compiere.model.MSequence;
import org.compiere.model.MShipper;
import org.compiere.model.MTax;
import org.compiere.model.MUser;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class SetupMagentoIntegration extends SvrProcess {

	
	private static final String DESC_AUTO_GENERATE = "Generated from Magento Integration Setup";
	private int m_AD_Client_ID = 0;
	private int m_AD_Org_ID = 0;
	private int m_M_Locator_ID = 0;
	private int m_M_Warehouse_ID = 0;
	private int m_M_Product_Category_ID = 0;
	private int m_C_BP_Group_ID = 0;
	private int m_Discount_Charge_ID = 0;
	private int m_Freight_Charge_ID = 0;
	private int m_M_Shipper_ID = 0;
	private int m_C_BankAccount_ID = 0;
	private int m_GST_Tax_ID = 0;
	private int m_GST_Exempt_Tax_ID = 0;
	private int m_C_Currency_ID = 0;

	private int m_C_Order_DocType_ID = 0;
	private int m_C_Invoice_DocType_ID = 0;
	private int m_M_PriceList_ID = 0;
	private Timestamp m_M_PriceList_ValidFrom = null;

	private 
	final static String CTX_AD_CLIENT_ID = "ad_client_id";
	final static String CTX_AD_CLIENT_NAME = "ad_client_name";
	final static String CTX_AD_LANGUAGE = "ad_language";
	
	final static String CTX_AD_MAGENTOLOCATOR = "ad_magentolocator";
	final static String CTX_AD_MGINVOICEDOCTYPE = "ad_mgInvoiceDocType";
	final static String CTX_AD_MGORDERDOCTYPE = "ad_mgOrderDocType";
	final static String CTX_AD_MGPRICELIST_ID = "ad_mgPriceList_ID";
	final static String CTX_AD_MGPRODUCTCATEGORY = "ad_mgProductCategory";
	final static String CTX_AD_M_LOCATOR_ID = "ad_m_locator_id";
	final static String CTX_AD_ORG_ID = "ad_org_id";
	final static String CTX_AD_ORG_NAME = "ad_org_name";

	final static String CTX_AD_PL_VALIDFROM = "ad_pl_validfrom";
	final static String CTX_AD_PRINTER = "ad_printer";
	final static String CTX_AD_PROPERTYFILE = "ad_propertyfile";
	final static String CTX_AD_ROLE_NAME = "ad_role_name";
	final static String CTX_AD_USERNAME = "ad_username";
	final static String CTX_AD_WAREHOUSE_NAME = "ad_warehouse_name";
	final static String CTX_C_BANKACCOUNT_ID = "c_bankaccount_id";
	final static String CTX_C_BP_GROUP_ID = "c_bp_group_id";
	final static String CTX_DISCOUNT_CHARGE_ID = "discount_charge_id";
	final static String CTX_FREIGHT_CHARGE_ID = "freight_charge_id";
	final static String CTX_GST_TAX_ID = "gst_tax_id";
	final static String CTX_MG_APIKEY = "mg_apikey";
	final static String CTX_MG_URL = "mg_url";
	final static String CTX_MG_USERNAME = "mg_username";
	final static String CTX_M_SHIPPER_ID = "m_shipper_id";
	final static String CTX_NONGST_TAX_ID = "nongst_tax_id";


	private String m_MagentoInstance = null;
	private String m_MagentoURL = null;
	private String m_MagentoUser = null;
	private String m_MagentoPassword = null;
	private String m_ShipperName = null;
	
	private BigDecimal m_TaxRate = null;

		
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("AD_Org_ID"))
				m_AD_Org_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("M_Warehouse_ID"))
				m_M_Warehouse_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Locator_ID"))
				m_M_Locator_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Product_Category_ID"))
				m_M_Product_Category_ID= para[i].getParameterAsInt();
			else if (name.equals("C_DoctTypeOrder_ID"))
				m_C_Order_DocType_ID = para[i].getParameterAsInt();
			else if (name.equals("C_DocTypeInvoice_ID"))
				m_C_Invoice_DocType_ID = para[i].getParameterAsInt();
			else if (name.equals("M_PriceList_ID"))
				m_M_PriceList_ID = para[i].getParameterAsInt();
			else if (name.equals("C_BP_Group_ID"))
				m_C_BP_Group_ID = para[i].getParameterAsInt();
			else if (name.equals("Discount_Charge_ID"))
				m_Discount_Charge_ID = para[i].getParameterAsInt();
			else if (name.equals("Freight_Charge_ID"))
				m_Freight_Charge_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Shipper_ID"))
				m_M_Shipper_ID = para[i].getParameterAsInt();
			else if (name.equals("C_BankAccount_ID"))
				m_C_BankAccount_ID = para[i].getParameterAsInt();
			else if (name.equals("GST_Tax_ID"))
				m_GST_Tax_ID = para[i].getParameterAsInt();
			else if (name.equals("GSTExempt_Tax_ID"))
				m_GST_Exempt_Tax_ID = para[i].getParameterAsInt();
			else if (name.equals("C_Currency_ID"))
				m_C_Currency_ID = para[i].getParameterAsInt();
			else if (name.equals("TaxRate"))
				m_TaxRate = para[i].getParameterAsBigDecimal();
			else if (name.equals("ShipperName"))
				m_ShipperName = para[i].getParameterAsString();
			else if (name.equals("MagentoURL"))
				m_MagentoURL = para[i].getParameterAsString();
			else if (name.equals("MagentoUser"))
				m_MagentoUser = para[i].getParameterAsString();
			else if (name.equals("MagentoPassword"))
				m_MagentoPassword = para[i].getParameterAsString();
			else if (name.equals("MagentoInstance"))
				m_MagentoInstance = para[i].getParameterAsString();
			else if (name.equals("ValidFrom"))
				m_M_PriceList_ValidFrom = para[i].getParameterAsTimestamp();

			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		m_AD_Client_ID = getAD_Client_ID();
		if (m_MagentoInstance != null) {
			m_MagentoInstance = m_MagentoInstance.replaceAll("\\s","");
		}
	}

	@Override
	protected String doIt() throws Exception {
		
		String sql = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		sql = "SELECT COUNT(*) FROM ETL_Setup WHERE AD_Org_ID=?";
		int etlNo = DB.getSQLValue(null, sql, m_AD_Org_ID) + 1;
		METLSetup setup = METLSetup.get(getCtx(), get_TrxName(), m_MagentoInstance, m_AD_Org_ID);
		if (setup == null) {
			setup = new METLSetup(getCtx(), 0, get_TrxName());
			setup.setAD_Client_ID(m_AD_Client_ID);
			setup.setAD_Org_ID(m_AD_Org_ID);
			setup.setName(m_MagentoInstance);
			setup.setDescription("Magento Integration connection profile for " + m_MagentoInstance);
			setup.saveEx();
		} else {
			setup.deleteSetupLines();
		}
		
		MOrg org = MOrg.get(getCtx(), m_AD_Org_ID);
		MClient client = MClient.get(getCtx(), m_AD_Client_ID);
		setup.addSetupLine(CTX_AD_CLIENT_ID, String.valueOf(m_AD_Client_ID));
		setup.addSetupLine(CTX_AD_CLIENT_NAME, client.getName());
		setup.addSetupLine(CTX_AD_ORG_ID, String.valueOf(org.getAD_Org_ID()));
		setup.addSetupLine(CTX_AD_ORG_NAME, org.getName());
		setup.addSetupLine(CTX_AD_LANGUAGE, Env.getAD_Language(getCtx()));
		setup.addSetupLine(CTX_AD_ROLE_NAME, MRole.get(getCtx(), Env.getAD_Role_ID(getCtx())).getName());
		setup.addSetupLine(CTX_AD_PRINTER, Env.getContext(Env.getCtx(), "#Printer"));
		setup.addSetupLine(CTX_AD_USERNAME, MUser.get(getCtx()).getName());
		
		setup.addSetupLine(CTX_AD_PL_VALIDFROM,  sdf.format(m_M_PriceList_ValidFrom));
		setup.addSetupLine(CTX_MG_URL, m_MagentoURL);
		setup.addSetupLine(CTX_MG_USERNAME, m_MagentoUser);
		setup.addSetupLine(CTX_MG_APIKEY, m_MagentoPassword);
		//add warehouse
		MWarehouse wh = null;
		if (m_M_Warehouse_ID == 0) {
			wh = new MWarehouse(getCtx(), 0, get_TrxName());
			wh.setAD_Client_ID(m_AD_Client_ID);
			wh.setAD_Org_ID(m_AD_Org_ID);
			wh.setName("WH" + etlNo + "-" + m_MagentoInstance);
			wh.setValue("WH" + etlNo + "-" + m_MagentoInstance);
			MLocation whloc = new MLocation(getCtx(), org.getInfo().getC_Location().getC_Country_ID()
					, org.getInfo().getC_Location().getC_Region_ID()
					, org.getInfo().getC_Location().getCity(), get_TrxName());
			whloc.setPostal(org.getInfo().getC_Location().getPostal());
			whloc.setAddress1(m_MagentoInstance);
			whloc.saveEx();
			wh.setC_Location_ID(whloc.getC_Location_ID());
			wh.saveEx();
			
			log.info("New Warehouse Created WH=" + wh.getName());
		} else {
			wh = MWarehouse.get(getCtx(), m_M_Warehouse_ID);
		}
		setup.addSetupLine(CTX_AD_WAREHOUSE_NAME, wh.getName());
		
		//add locator
		MLocator loc = null;
		if (m_M_Locator_ID == 0) {
			loc = new MLocator(wh, m_MagentoInstance);
			loc.setX("MG-" + m_MagentoInstance);
			loc.saveEx();
		} else {
			loc = MLocator.get(getCtx(), m_M_Locator_ID);
		}
		setup.addSetupLine(CTX_AD_M_LOCATOR_ID, String.valueOf(loc.getM_Locator_ID()));
		setup.addSetupLine(CTX_AD_MAGENTOLOCATOR, String.valueOf(loc.getValue()));
		
		String docInvoiceName = "AR Invoice - " + "MG" + etlNo;
		MDocType docInvoice  = new Query(getCtx(), MDocType.Table_Name, "Name=? AND AD_Org_ID=?", get_TrxName())
		.setParameters(docInvoiceName, m_AD_Org_ID)
		.first();
		if (docInvoice == null) {
			//throw new AdempiereException("Not yet supported. m_C_Invoice_DocType_ID == 0");
			docInvoice = new MDocType(getCtx(), MDocType.DOCBASETYPE_ARInvoice,
					docInvoiceName, get_TrxName());
			MGLCategory glc = new Query(getCtx(), MGLCategory.Table_Name, "Name=? AND AD_Org_ID=?", get_TrxName())
				.setParameters("AR Invoice", m_AD_Org_ID)
				.first();
					
			if (glc == null) {
				throw new AdempiereException("Adempiere Client setup should have created GLCategory=ARI Invoice");
			}
							
			docInvoice.setAD_Org_ID(m_AD_Org_ID);
			docInvoice.setGL_Category_ID(glc.getGL_Category_ID());
			docInvoice.setIsSOTrx(true);
			docInvoice.saveEx();
		}
		setup.addSetupLine(CTX_AD_MGINVOICEDOCTYPE, docInvoice.getName());
		
		String docShipName = "MM Shipment - " + "MG" + etlNo;
		MDocType docShipment  =  new Query(getCtx(), MDocType.Table_Name, "Name=? AND AD_Org_ID=?", get_TrxName())
			.setParameters(docShipName, m_AD_Org_ID)
			.first();
		
		if (docShipment == null) {
			docShipment =  new MDocType(getCtx(), MDocType.DOCBASETYPE_MaterialDelivery,
					docShipName, get_TrxName());
			MGLCategory glc = new Query(getCtx(), MGLCategory.Table_Name, "Name=? AND AD_Org_ID=?", get_TrxName())
			.setParameters("Material Management", m_AD_Org_ID)
			.first();
				
			if (glc == null) {
				throw new AdempiereException("Adempiere Client setup should have created GLCategory=Material Management");
			}
							
			docShipment.setAD_Org_ID(m_AD_Org_ID);
			docShipment.setGL_Category_ID(glc.getGL_Category_ID());
			docShipment.setIsSOTrx(true);
			docShipment.setIsDocNoControlled(true);
			MSequence seq = new MSequence(getCtx(), m_AD_Client_ID, docShipName, 10000, get_TrxName());
			seq.saveEx();
			docShipment.setDocNoSequence_ID(seq.getAD_Sequence_ID());
			docShipment.saveEx();
		}
		
		
		String docOrderName = "Standard Order - " + "MG" + etlNo;
		MDocType docOrder =  new Query(getCtx(), MDocType.Table_Name, "Name=? AND AD_Org_ID=?", get_TrxName())
			.setParameters(docOrderName, m_AD_Org_ID)
			.first();
		if (docOrder == null) {
			
			docOrder = new MDocType(getCtx()
					, MDocType.DOCBASETYPE_SalesOrder
					, docOrderName
					, get_TrxName());

			MGLCategory glc = new Query(getCtx(), MGLCategory.Table_Name, "Name=? AND AD_Org_ID=?", get_TrxName())
			.setParameters("None", m_AD_Org_ID)
			.first();
				
			if (glc == null) {
				throw new AdempiereException("Adempiere Client setup should have created GLCategory=None");
			}
			docOrder.setAD_Org_ID(m_AD_Org_ID);
			docOrder.setIsDocNoControlled(false);
			docOrder.setGL_Category_ID(glc.getGL_Category_ID());
			docOrder.setDocSubTypeSO(MDocType.DOCSUBTYPESO_StandardOrder);
			docOrder.setIsSOTrx(true);
			docOrder.setC_DocTypeInvoice_ID(docInvoice.getC_DocType_ID());
			docOrder.setC_DocTypeShipment_ID(docShipment.getC_DocType_ID());
			docOrder.saveEx();
		}

		setup.addSetupLine(CTX_AD_MGORDERDOCTYPE, docOrder.getName());
		
		String plName = "SPL - " + "MG" + etlNo;
		MPriceList pl = new Query(getCtx(), MPriceList.Table_Name, "Name=? AND AD_Org_ID=?", get_TrxName())
			.setParameters(plName, m_AD_Org_ID)
			.first();
		if (pl == null) {
			//throw new AdempiereException("Not yet supported. m_M_PriceList_ID == 0 ");
			pl = new MPriceList(getCtx(), 0, get_TrxName());
			pl.setAD_Org_ID(m_AD_Org_ID);
			pl.setName(plName);
			pl.setC_Currency_ID(m_C_Currency_ID);
			pl.setDescription(DESC_AUTO_GENERATE);
			pl.setIsSOPriceList(true);
			pl.saveEx();
			
		}
		setup.addSetupLine(CTX_AD_MGPRICELIST_ID, String.valueOf(pl.getM_PriceList_ID()));
		
		
		String grpName = "Customers - " + "MG-" + m_MagentoInstance;
		MBPGroup grp = new Query(getCtx(), MBPGroup.Table_Name, "Value=?", get_TrxName())
			.setParameters(grpName)
			.first();
		
		if (grp == null) {
			grp = new MBPGroup(getCtx(), 0, get_TrxName());
			grp.setValue(grpName);
			grp.setName(grpName);
			grp.setM_PriceList_ID(pl.getM_PriceList_ID());
			grp.saveEx();
		}
		setup.addSetupLine(CTX_C_BP_GROUP_ID, String.valueOf(grp.getC_BP_Group_ID()));

		if (m_C_BankAccount_ID == 0) {
			throw new AdempiereException("Not yet supported. m_C_BankAccount_ID == 0 ");
		} else {
			setup.addSetupLine(CTX_C_BANKACCOUNT_ID, String.valueOf(m_C_BankAccount_ID));
		}
		
		
		MShipper shipper = new Query(getCtx(), MShipper.Table_Name, "Name=? ", get_TrxName())
			.setParameters(m_ShipperName)
			.first();
		if (shipper == null) {
			shipper = new MShipper(getCtx(), 0, get_TrxName());
			shipper.setName(m_ShipperName);
			shipper.setDescription(DESC_AUTO_GENERATE);
			shipper.saveEx();
		}
		setup.addSetupLine(CTX_M_SHIPPER_ID, String.valueOf(shipper.getM_Shipper_ID()));

		String pcName =  "No Category - " + "MG" + etlNo;
		MProductCategory pc = new Query(getCtx(), MProductCategory.Table_Name, "Value=? AND AD_Org_ID=?", get_TrxName())
		.setParameters(pcName, m_AD_Org_ID)
		.first();
		if (pc == null) {
			pc = new MProductCategory(getCtx(), 0, get_TrxName());
			pc.setValue(pcName);
			pc.setName(pcName);
			pc.setDescription(DESC_AUTO_GENERATE);
			pc.saveEx();
		}
		setup.addSetupLine(CTX_AD_MGPRODUCTCATEGORY, pc.getName());

		int tax_Category_ID = DB.getSQLValue(get_TrxName(), "SELECT MAX(C_TaxCategory_ID) " 
				+ "FROM C_TaxCategory WHERE IsDefault='Y' AND AD_Client_ID=?", m_AD_Client_ID);
		
		if (tax_Category_ID == 0) {
			throw new AdempiereException("Unusual - no default Tax Category for Client.");
		}
		Timestamp validFrom =new  Timestamp(sdf.parse("01/01/2000").getTime());
		MTax tax0 = new Query(getCtx(), MTax.Table_Name, "Rate=? AND AD_Client_ID=? ", get_TrxName())
		.setParameters(Env.ZERO, m_AD_Client_ID)
		.first();
		
		if (tax0 == null) {
			tax0 = new MTax(getCtx(), "Zero Rated", Env.ZERO, tax_Category_ID, get_TrxName());
			tax0.setSOPOType(MTax.SOPOTYPE_Both);
			tax0.setIsDocumentLevel(true);
			tax0.setValidFrom(validFrom);
			tax0.saveEx();
		}
		setup.addSetupLine(CTX_NONGST_TAX_ID, String.valueOf(tax0.getC_Tax_ID()));
			
		MTax tax1 = new Query(getCtx(), MTax.Table_Name, "Rate=?  AND AD_Client_ID=? ", get_TrxName())
		.setParameters(m_TaxRate, m_AD_Client_ID)
		.first();
		
		if (tax1 == null) {
			String tName =  "Tax-" + m_TaxRate.setScale(2).toPlainString();
			tax1 = new MTax(getCtx(), tName, m_TaxRate, tax_Category_ID, get_TrxName());
			tax1.setSOPOType(MTax.SOPOTYPE_Both);
			tax1.setIsDocumentLevel(true);
			tax1.setValidFrom(validFrom);
			tax1.saveEx();

		}
		setup.addSetupLine(CTX_GST_TAX_ID, String.valueOf(tax1.getC_Tax_ID()));

		setup.addSetupLine(CTX_FREIGHT_CHARGE_ID, String.valueOf(m_Freight_Charge_ID));
		setup.addSetupLine(CTX_DISCOUNT_CHARGE_ID, String.valueOf(m_Discount_Charge_ID));

		int no = setup.getLines().size();
		addLog (0, null, new BigDecimal (no), "Context lines created");
		commitEx();
		return "";
		
	}
	


}
