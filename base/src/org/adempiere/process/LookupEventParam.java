/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Low Heng Sin                                         * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Low Heng Sin (hengsin@users.sourceforge.net)                    *
 *                                                                    *
 * Sponsors:                                                          *
 *  - Idalica (http://www.idalica.com/)                               *
 **********************************************************************/
package org.adempiere.process;

import org.compiere.model.MColumn;
import org.compiere.model.X_C_EDIFormat_LineElement;
import org.compiere.model.X_C_EDIFormat_LineElementComp;

/**
 * @author Low Heng Sin
 */
public class LookupEventParam {

	private X_C_EDIFormat_LineElement lineElement;
	private X_C_EDIFormat_LineElementComp lineElementComp;
	private MColumn column;
	private Object input;
	private String lookupColumn;
	private String sql;
	
	/**
	 * 
	 * @param lineElement
	 * @param column
	 * @param input
	 * @param lookupColumn
	 * @param sql
	 */
	public LookupEventParam(X_C_EDIFormat_LineElement lineElement, MColumn column, Object input, 
			String lookupColumn, String sql) {
		super();
		this.lineElement = lineElement;
		this.column = column;
		this.input = input;
		this.lookupColumn = lookupColumn;
		this.sql = sql;
	}
	
	public LookupEventParam(X_C_EDIFormat_LineElementComp lineElementComp, MColumn column, Object input, 
			String lookupColumn, String sql) {
		super();
		this.lineElementComp = lineElementComp;
		this.column = column;
		this.input = input;
		this.lookupColumn = lookupColumn;
		this.sql = sql;
	}

	/**
	 * @return lineElement
	 */
	public X_C_EDIFormat_LineElement getLineElement() {
		return lineElement;
	}
	
	public X_C_EDIFormat_LineElementComp getLineElementComp() {
		return lineElementComp;
	}

	/**
	 * 
	 * @return column
	 */
	public MColumn getColumn() {
		return column;
	}

	/**
	 * 
	 * @return input
	 */
	public Object getInput() {
		return input;
	}

	/**
	 * 
	 * @return lookup column
	 */
	public String getLookupColumn() {
		return lookupColumn;
	}

	/**
	 * 
	 * @return lookup sql
	 */
	public String getSql() {
		return sql;
	}
	
	
}
