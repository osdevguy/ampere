package org.adempiere.process;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;

import org.compiere.model.Query;
import org.compiere.model.X_ETL_SetupLine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

public class LoadETLContext extends SvrProcess {

	private int m_setup_id = 0;
	private String m_filename = null;
	private boolean isOverWriteContext = false;
	
	@Override
	protected void prepare() {
		m_setup_id = getRecord_ID();
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			
			String name = para[i].getParameterName();
			if (name.equals("Filename")) {
				m_filename = (String)para[i].getParameter();
			}
			else if (name.equals("IsOverWriteContext")) {
				isOverWriteContext = para[i].getParameterAsBoolean();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}


	@Override
	protected String doIt() throws Exception {

		if (m_setup_id == 0) {
			 throw new Exception("No ETL setup definition.");
		}
		
		InputStream in = new FileInputStream(m_filename);
		if (in == null) {
			return m_filename + "not found.";
		}
		Properties prop = new Properties();
		prop.load(in);
		in.close();
		
		int noInsert = 0;
		int noUpdate = 0;
		
		String whereClause = "ETL_Setup_ID=?";
		List<X_ETL_SetupLine>  setupLines = new Query(getCtx(), X_ETL_SetupLine.Table_Name, whereClause, get_TrxName())
    		.setParameters(m_setup_id)
    		.setClient_ID()
    		.setOnlyActiveRecords(true)
    		.list();
			
		Set items = prop.keySet();
		Iterator itr = items.iterator();
		boolean isNew = false;
		while (itr.hasNext())
		{
			isNew = true;
			String key = (String) itr.next();
			for (X_ETL_SetupLine line : setupLines) {
				if (line.getName().equals(key)) {
					isNew = false;
					if  (isOverWriteContext) {
						line.setValue(prop.getProperty(key));
						noUpdate++;
					}
					break;
				}
	
			}
			if (isNew) {
				X_ETL_SetupLine newLine = new X_ETL_SetupLine(getCtx(), 0, get_TrxName());
				newLine.setETL_Setup_ID(m_setup_id);
				newLine.setAD_Client_ID(getAD_Client_ID());
				newLine.setAD_Org_ID(0);
				newLine.setName(key);
				newLine.setValue(prop.getProperty(key));
				newLine.saveEx();
				setupLines.add(newLine);
				noInsert++;
				
			}
		}
		
		addLog (0, null, new BigDecimal (noInsert), "Context Created: @Inserted@");
		addLog (0, null, new BigDecimal (noUpdate), "Context Created: @Updated@");

		return "";
		
	}

}
