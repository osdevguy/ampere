package org.adempiere.process;

import java.util.logging.Level;

import org.compiere.model.MMRPRun;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

public class MRPClearState extends SvrProcess {

	int MRP_Run_ID = 0;
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			
			String name = para[i].getParameterName();
			if (name.equals("MRP_Run_ID")) {
				MRP_Run_ID = para[i].getParameterAsInt();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}

	}

	@Override
	protected String doIt() throws Exception {
		MMRPRun run = new MMRPRun(getCtx(), MRP_Run_ID, null);
		
		run.setIsProcessing(false);
		run.save();
		
		return run.getName() + " reset.";
	}

}
