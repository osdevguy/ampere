package org.adempiere.process;

import org.compiere.model.MSequence;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class SequenceHelper {
	
	private int AD_Client_ID = 0;

	public SequenceHelper(int AD_Client_ID) {
		this.AD_Client_ID  = AD_Client_ID;
	}
	
	public int nextID(String seqName) {
		int nextID = -1;
		int AD_Sequence_ID = DB.getSQLValue(null, "SELECT AD_Sequence_ID From AD_Sequence Where AD_Client_ID = " + AD_Client_ID + " AND Name = '" + seqName + "'");
		if (AD_Sequence_ID > 0) {
			MSequence seq = new MSequence(Env.getCtx(), AD_Sequence_ID, null);
			nextID = seq.getNextID();
			seq.save();
		}
		return nextID;
	}
	
}
