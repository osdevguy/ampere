/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 Adempiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *
 * Copyright (C) 2005 Robert Klein. robeklein@hotmail.com
 * _____________________________________________
 *****************************************************************************/
package org.adempiere.pipo;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.compiere.util.CLogger;





/**
 * Compress package
 * 
 * @author Rob Klein
 * @version 	$Id: ImportFAJournal2.java,v 1.0 $
 * 
 */
public class CreateZipFile {

	private static CLogger log = CLogger.getCLogger(CreateZipFile.class);

	/**
	 * Zip the srcFolder into the destFileZipFile. All the folder subtree of the src folder is added to the destZipFile
	* archive.
	* 
	*
	* @param srcFolder File, the path of the srcFolder
	* @param destZipFile File, the path of the destination zipFile. This file will be created or erased.
	*/
	static public void zipFolder(File srcFolder, File destZipFile, String includesdir) 
	{   
		try
		{
		FileOutputStream fos = new FileOutputStream(destZipFile);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		zipFile(srcFolder, srcFolder.getName(), zipOut);
	    System.out.println(destZipFile);
		}
		catch (Exception e) {
		    log.log(Level.SEVERE, "Creating zip archive failed", e);
		}
	 }
	
	 /**
	  * Utility code from example at https://www.baeldung.com/java-compress-and-uncompress
	  * @param fileToZip
	  * @param fileName
	  * @param zipOut
	  * @throws IOException
	  */
	private static void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
		if (fileToZip.isHidden()) {
			return;
		}
		if (fileToZip.isDirectory()) {
			if (fileName.endsWith("/")) {
				zipOut.putNextEntry(new ZipEntry(fileName));
				zipOut.closeEntry();
			} else {
				zipOut.putNextEntry(new ZipEntry(fileName + "/"));
				zipOut.closeEntry();
			}
			File[] children = fileToZip.listFiles();
			for (File childFile : children) {
				zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
			}
			return;
		}
		FileInputStream fis = new FileInputStream(fileToZip);
		ZipEntry zipEntry = new ZipEntry(fileName);
		zipOut.putNextEntry(zipEntry);
		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zipOut.write(bytes, 0, length);
		}
		fis.close();
	}
	
	static public void unpackFile(File zipFilePath, File destDir)
    {
		 FileInputStream fis;
	        //buffer for read and write data to file
	        byte[] buffer = new byte[1024];
	        try {
	            fis = new FileInputStream(zipFilePath);
	            ZipInputStream zis = new ZipInputStream(fis);
	            ZipEntry ze = zis.getNextEntry();
	            while(ze != null){
	                String fileName = ze.getName();
	                File newFile = new File(destDir + File.separator + fileName);
	                System.out.println("Unzipping to "+newFile.getAbsolutePath());
	                //create directories for sub directories in zip
	                new File(newFile.getParent()).mkdirs();
	                FileOutputStream fos = new FileOutputStream(newFile);
	                int len;
	                while ((len = zis.read(buffer)) > 0) {
	                fos.write(buffer, 0, len);
	                }
	                fos.close();
	                //close this ZipEntry
	                zis.closeEntry();
	                ze = zis.getNextEntry();
	            }
	            //close last ZipEntry
	            zis.closeEntry();
	            zis.close();
	            fis.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
     }
	static public String getParentDir(File zipFilepath)
    {
		try {
		ZipFile zipFile = new ZipFile(zipFilepath);
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		ZipEntry entry = entries.nextElement();
		File tempfile = new File(entry.getName());
		while (tempfile.getParent()!=null)
			tempfile = tempfile.getParentFile();		
		return tempfile.getName();
		} catch (IOException ioe) {
		      System.err.println("Unhandled exception:");
		      ioe.printStackTrace();
		      return "";
	    }		
     }
	}//	CreateZipFile


