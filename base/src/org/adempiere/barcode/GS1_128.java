package org.adempiere.barcode;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;

import javax.xml.crypto.dsig.keyinfo.KeyValue;

/*
 * 	Reference = https://en.wikipedia.org/wiki/GS1-128
	Code 	Description 									data length (without AI)
	00 		Serial Shipping Container Code (SSCC) 			18
	01 		Global Trade Item Number (GTIN) 				14
	02 		GTIN of Contained Trade Items 					14
	10 		Batch/Lot Number 								variable, up to 20
	11 		Production Date 								6
	12 		Due Date 										6
	13 		Packaging Date 									6
	15 		Best Before Date (YYMMDD) 						6
	17 		Expiration Date 								6
	20 		Product Variant 								2
	21 		Serial Number 									variable, up to 20
	22 		Secondary Data Fields 							variable, up to 29
	23n 	Lot number n 									variable, up to 19
	240 	Additional Product Identification 				variable, up to 30
	241 	Customer Part Number 							variable, up to 30
	242 	Made-to-Order Variation Number 					variable, up to 6
	250 	Secondary Serial Number 						variable, up to 30
	251 	Reference to Source Entity 						variable, up to 30
	253 	Global Document Type Identifier 				variable, 13–17
	254 	GLN Extension Component 						variable, up to 20
	255 	Global Coupon Number (GCN) 						variable, 13–25
	30 		Count of items 									variable, up to 8
	310y 	Product Net Weight in kg 						6
	311y 	Product Length/1st Dimension, in meters 		6
	312y 	Product Width/Diameter/2nd Dimension, in meters	6
	313y 	Product Depth/Thickness/Height/3rd Dimension, in meters 	6
	314y 	Product Area, in square meters 	6
	315y 	Product Net Volume, in liters 	6
	316y 	Product Net Volume, in cubic meters 	6
	320y 	Product Net Weight, in pounds 	6
	321y 	Product Length/1st Dimension, in inches 	6
	322y 	Product Length/1st Dimension, in feet 	6
	323y 	Product Length/1st Dimension, in yards 	6
	324y 	Product Width/Diameter/2nd Dimension, in inches 	6
	325y 	Product Width/Diameter/2nd Dimension, in feet 	6
	326y 	Product Width/Diameter/2nd Dimension, in yards 	6
	327y 	Product Depth/Thickness/Height/3rd Dimension, in inches 	6
	328y 	Product Depth/Thickness/Height/3rd Dimension, in feet 	6
	329y 	Product Depth/Thickness/3rd Dimension, in yards 	6
	330y 	Container Gross Weight (kg) 	6
	331y 	Container Length/1st Dimension (Meters) 	6
	332y 	Container Width/Diameter/2nd Dimension (Meters) 	6
	333y 	Container Depth/Thickness/3rd Dimension (Meters) 	6
	334y 	Container Area (Square Meters) 	6
	335y 	Container Gross Volume (Liters) 	6
	336y 	Container Gross Volume (Cubic Meters) 	6
	340y 	Container Gross Weight (Pounds) 	6
	341y 	Container Length/1st Dimension, in inches 	6
	342y 	Container Length/1st Dimension, in feet 	6
	343y 	Container Length/1st Dimension in, in yards 	6
	344y 	Container Width/Diameter/2nd Dimension, in inches 	6
	345y 	Container Width/Diameter/2nd Dimension, in feet 	6
	346y 	Container Width/Diameter/2nd Dimension, in yards 	6
	347y 	Container Depth/Thickness/Height/3rd Dimension, in inches 	6
	348y 	Container Depth/Thickness/Height/3rd Dimension, in feet 	6
	349y 	Container Depth/Thickness/Height/3rd Dimension, in yards 	6
	350y 	Product Area (Square Inches) 	6
	351y 	Product Area (Square Feet) 	6
	352y 	Product Area (Square Yards) 	6
	353y 	Container Area (Square Inches) 	6
	354y 	Container Area (Square Feet) 	6
	355y 	Container Area (Square Yards) 	6
	356y 	Net Weight (Troy Ounces) 	6
	357y 	Net Weight/Volume (Ounces) 	6
	360y 	Product Volume (Quarts) 	6
	361y 	Product Volume (Gallons) 	6
	362y 	Container Gross Volume (Quarts) 	6
	363y 	Container Gross Volume (U.S. Gallons) 	6
	364y 	Product Volume (Cubic Inches) 	6
	365y 	Product Volume (Cubic Feet) 	6
	366y 	Product Volume (Cubic Yards) 	6
	367y 	Container Gross Volume (Cubic Inches) 	6
	368y 	Container Gross Volume (Cubic Feet) 	6
	369y 	Container Gross Volume (Cubic Yards) 	6
	37 	Number of Units Contained 	variable, up to 8
	390y 	Amount payable (local currency) 	variable, up to 15
	391y 	Amount payable (with ISO currency code) 	variable, 3–18
	392y 	Amount payable per single item (local currency) 	variable, up to 15
	393y 	Amount payable per single item (with ISO currency code) 	variable, 3–18
	400 	Customer Purchase Order Number 	variable, up to 30
	401 	Consignment Number 	variable, up to 30
	402 	Bill of Lading number 	17
	403 	Routing code 	variable, up to 30
	410 	Ship To/Deliver To Location Code (Global Location Number) 	13
	411 	Bill To/Invoice Location Code (Global Location Number) 	13
	412 	Purchase From Location Code (Global Location Number) 	13
	413 	Ship for, Deliver for, or Forward to Location Code (Global Location Number) 	13
	414 	Identification of a physical location (Global Location Number) 	13
	420 	Ship To/Deliver To Postal Code (Single Postal Authority) 	variable, up to 20
	421 	Ship To/Deliver To Postal Code (with ISO country code) 	variable, 3–15
	422 	Country of Origin (ISO country code) 	3
	423 	Country or countries of initial processing 	variable, 3–15
	424 	Country of processing 	3
	425 	Country of disassembly 	3
	426 	Country of full process chain 	3
	7001 	NATO Stock Number (NSN) 	13
	7002 	UN/ECE Meat Carcasses and cuts classification 	variable, up to 30
	7003 	expiration date and time 	10
	7004 	Active Potency 	variable, up to 4
	703n 	Processor approval (with ISO country code); n indicates sequence number of several processors 	variable, 3–30
	8001 	Roll Products: Width/Length/Core Diameter/Direction/Splices 	14
	8002 	Mobile phone identifier 	variable, up to 20
	8003 	Global Returnable Asset Identifier 	variable, 14–30
	8004 	Global Individual Asset Identifier 	variable, up to 30
	8005 	Price per Unit of Measure 	6
	8006 	identification of the components of an item 	18
	8007 	International Bank Account Number 	variable, up to 30
	8008 	Date/time of production 	variable, 8–12
	8018 	Global Service Relationship Number 	18
	8020 	Payment slip reference number 	variable, up to 25
	8100 	Coupon Extended Code: Number System and Offer 	6
	8101 	Coupon Extended Code: Number System, Offer, End of Offer 	10
	8102 	Coupon Extended Code: Number System preceded by 0 	2
	8110 	Coupon code ID (North America) 	variable, up to 30
	8200 	Extended Packaging URL 	variable, up to 70
	90 	Mutually Agreed Between Trading Partners 	variable, up to 30
	91–99 	Internal Company Codes 	variable, up to 30
*/

/**
 * Translate a barcode number to GS1-128 if it conforms to GS1-128 specification
 * Limitation -Group Separator (char(29)) is not a printable text character
 * @author jtrinidad
 *
 */
public class GS1_128 {
	

	public static final String GS1_128_SSCC 												= "00";
	public static final String GS1_128_GTIN 												= "01";
	public static final String GS1_128_BATCHNO 												= "10";
	public static final String GS1_128_PRODUCTION_DATE 										= "11";
	public static final String GS1_128_DUEDATE 												= "12";
	public static final String GS1_128_PACKAGING_DATE 										= "13";
	public static final String GS1_128_BEST_BEFORE_DATE 									= "15";
	public static final String GS1_128_EXPIRATION_DATE 										= "17";
	public static final String GS1_128_PRODUCT_VARIANT 										= "20";
	public static final String GS1_128_SERIAL_NO 											= "21";
	public static final String GS1_128_NO_UNITS_CONTAINED									= "37";
	
	//Internal Codes
	public static final String GS1_128_ICC_MFG_LINE 										= "91";
	public static final String GS1_128_ICC_PRODUCTION_DOCNO										= "92";



	public static final String BARCODE_DATE_FORMAT = "yyMMdd";
	
	private ArrayList<AIDictionary> supported = new ArrayList<AIDictionary>();
	private HashMap<String, String> found = new HashMap<String, String>();
//	private String lastCode = null;
//	private String lastValue = null;
//	
//	private String m_SSCC = null;
//	private String m_GTIN = null;
//	private String m_BatchLot = null;
//	private Timestamp m_ProductionDate = null; 
//	private Timestamp m_DueDate = null;
//	private Timestamp m_PackagingDate = null;
//	private Timestamp m_BestBeforeDate = null;
//	private Timestamp m_ExpirationDate = null;
//	private String m_ProductVariant = null;
//	private String m_SerialNumber = null;
	
	private SimpleDateFormat sdf = new SimpleDateFormat(BARCODE_DATE_FORMAT);
	
	private boolean m_IsValid = false;
	
	public GS1_128() {
		super();
		addSupportedAI();
	}

	public static GS1_128 parse(String scannedString) 
	{
		GS1_128 barcode = new GS1_128();
		if (barcode.match(scannedString)) {
			return barcode;
			
		}
		
		return null;
	}

	/**
	 * Add supported Application Identifiers
	 * Not all identifiers will be needed immediately
	 * Add only what is needed or deemed necessary
	 * @param ai
	 * @param description
	 * @param lenght
	 */
	private void addSupportedAI()
	{
		add(GS1_128_SSCC, "Serial Shipping Container Code (SSCC)", 18, false);
		add(GS1_128_GTIN, "Global Trade Item Number (GTIN)", 14, false);
		add(GS1_128_BATCHNO, "Batch/Lot Number", 20, true);
		add(GS1_128_PRODUCTION_DATE, "Production Date", 6, false);
		add(GS1_128_DUEDATE, "Due Date", 6, false);
		add(GS1_128_PACKAGING_DATE, "Packaging Date", 6, false);
		add(GS1_128_BEST_BEFORE_DATE, "Best Before Date", 6, false);
		add(GS1_128_EXPIRATION_DATE, "Expiration Date", 6, false);
		add(GS1_128_PRODUCT_VARIANT, "Product Variant", 2, false);
		add(GS1_128_SERIAL_NO, "Serial Number", 20, true);
		add(GS1_128_NO_UNITS_CONTAINED, "Number of Units Contained", 8, true);
		add(GS1_128_ICC_MFG_LINE, "ICC Mfg Line", 8, true);
		add(GS1_128_ICC_PRODUCTION_DOCNO, "ICC Production Order No", 8, true);

	}
	
	
	private void add(String code, String description, int length, boolean isVariable)
	{
		supported.add(new AIDictionary(code, description, length, isVariable));
	}
	
	
	private boolean match(String barcode)
	{
		for (AIDictionary ai :  supported) 
		{
			if (barcode.startsWith(ai.m_Code))
			{
				String stripped = StringUtils.right(barcode, barcode.length() - ai.m_Code.length());
				if (stripped.length() < ai.m_DataLength)
				{
					if (ai.isVariable()) {
						found.put(ai.m_Code, stripped);
						return true;
					} else {
						//it does not conform to specification
						return false;
					}
				} else {
					String value = StringUtils.left(stripped, ai.m_DataLength);
					found.put(ai.m_Code, value);
					String remaining = StringUtils.right(stripped, stripped.length() - value.length());
					if (remaining.length() == 0) {
						return true;
					}
					if (!match(remaining)) {
						return false;
					}
					return true;
				}
			}
		}
		return false;
	}
	
	


	public String getBatchLot() {
		return found.get(GS1_128_BATCHNO);
	}

	public String getSSCC() {
		return found.get(GS1_128_SSCC);
	}

	public String getGTIN() {
		return found.get(GS1_128_GTIN);
	}

	public Timestamp getProductionDate() {
		return getValueToTimestamp(GS1_128_PRODUCTION_DATE);
	}

	public Timestamp getDueDate() {
		return getValueToTimestamp(GS1_128_DUEDATE);
	}

	public Timestamp getPackagingDate() {
		return getValueToTimestamp(GS1_128_PACKAGING_DATE);
	}

	public Timestamp getBestBeforeDate() {
		return getValueToTimestamp(GS1_128_BEST_BEFORE_DATE);
	}

	public Timestamp getExpirationDate() {
		return getValueToTimestamp(GS1_128_EXPIRATION_DATE);
	}

	public String getProductVariant() {
		return found.get(GS1_128_PRODUCT_VARIANT);
	}

	public String getSerialNumber() {
		return found.get(GS1_128_SERIAL_NO);
	}

	public int getNumberofUnits()
	{
		return Integer.parseInt(found.get(GS1_128_NO_UNITS_CONTAINED));
	}
	
	public String getICCMfgLine()
	{
		return found.get(GS1_128_ICC_MFG_LINE);
	}
	
	public String getICCProductionOrderNo()
	{
		return found.get(GS1_128_ICC_PRODUCTION_DOCNO);
	}
	
	public String getValue(String ID) 
	{
		return found.get(ID);
	}
	
	public boolean containID(String ID)
	{
		return found.containsKey(ID);
	}
	
	public boolean isValid() {
		return m_IsValid;
	}	
	
	private Timestamp getValueToTimestamp(String code)
	{
		try {
			Timestamp ts = new Timestamp(sdf.parse(found.get(code)).getTime());
			return ts;
		} catch (ParseException e) {
			//TODO : add logging
			return null;
		}
	}
	
	
	/**
	 * Application Identifier Dictionary
	 * @author jtrinidad
	 *
	 */
	public class AIDictionary 
	{
		private String m_Code = null;
		private String m_Description = null;
		private int m_DataLength = 0;
		private boolean m_IsVariable = false;

		public AIDictionary(String code, String description, int dataLength, boolean isVariable) {
			super();
			this.m_Code = code;
			this.m_Description = description;
			this.m_DataLength = dataLength;
			this.m_IsVariable = isVariable;
		}

		public String getCode() {
			return m_Code;
		}

		public String getDescription() {
			return m_Description;
		}

		public int getDataLength() {
			return m_DataLength;
		}
		
		public boolean isVariable() {
			return m_IsVariable;
					
		}

	}

}



