package org.compiere.model;

import java.util.Properties;

import org.compiere.util.DB;

public class MCustomPrintSetup extends X_AX_Custom_PrintSetup {

	/**
	 * @author jobriant
	 */
	private static final long serialVersionUID = 6860231157390758608L;

	public MCustomPrintSetup(Properties ctx, int AX_Custom_PrintSetup_ID,
			String trxName) {
		super(ctx, AX_Custom_PrintSetup_ID, trxName);
	}
	
	public static int getPrintFormatID(String ButtonName) {

		String sql = "SELECT ad_printformat_id FROM ax_custom_printsetup WHERE value = ?";
		return DB.getSQLValue(null, sql, ButtonName);
	}
	
	public static String getPrinter(int AD_PrintFormat_ID, String hostName) {
		String sql = "SELECT b.printername FROM ax_custom_printsetup a   " +
	             "INNER JOIN ax_custom_printsetupline b ON a.ax_custom_printsetup_id = b.ax_custom_printsetup_id " +
	             "WHERE a.ad_printformat_id = ? AND b.host = ?";
		return DB.getSQLValueString(null, sql, AD_PrintFormat_ID, hostName);
	}

}
