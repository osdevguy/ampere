/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for C_EDI_Ack
 *  @author Adempiere (generated) 
 *  @version 1.5.0
 */
public interface I_C_EDI_Ack 
{

    /** TableName=C_EDI_Ack */
    public static final String Table_Name = "C_EDI_Ack";

    /** AD_Table_ID=53428 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_EDI_Ack_ID */
    public static final String COLUMNNAME_C_EDI_Ack_ID = "C_EDI_Ack_ID";

	/** Set EDI Acknowledgement	  */
	public void setC_EDI_Ack_ID (int C_EDI_Ack_ID);

	/** Get EDI Acknowledgement	  */
	public int getC_EDI_Ack_ID();

    /** Column name C_EDI_Interchange_ID */
    public static final String COLUMNNAME_C_EDI_Interchange_ID = "C_EDI_Interchange_ID";

	/** Set EDI Interchange	  */
	public void setC_EDI_Interchange_ID (int C_EDI_Interchange_ID);

	/** Get EDI Interchange	  */
	public int getC_EDI_Interchange_ID();

	public I_C_EDI_Interchange getC_EDI_Interchange() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name FunctionalIdentifier */
    public static final String COLUMNNAME_FunctionalIdentifier = "FunctionalIdentifier";

	/** Set Functional Identifier	  */
	public void setFunctionalIdentifier (String FunctionalIdentifier);

	/** Get Functional Identifier	  */
	public String getFunctionalIdentifier();

    /** Column name InterchangeControlNo */
    public static final String COLUMNNAME_InterchangeControlNo = "InterchangeControlNo";

	/** Set Interchange Control Number	  */
	public void setInterchangeControlNo (String InterchangeControlNo);

	/** Get Interchange Control Number	  */
	public String getInterchangeControlNo();

    /** Column name InterchangeSender */
    public static final String COLUMNNAME_InterchangeSender = "InterchangeSender";

	/** Set Interchange Sender ID	  */
	public void setInterchangeSender (String InterchangeSender);

	/** Get Interchange Sender ID	  */
	public String getInterchangeSender();

    /** Column name InterchangeSenderQualifier */
    public static final String COLUMNNAME_InterchangeSenderQualifier = "InterchangeSenderQualifier";

	/** Set InterchangeSenderQualifier	  */
	public void setInterchangeSenderQualifier (String InterchangeSenderQualifier);

	/** Get InterchangeSenderQualifier	  */
	public String getInterchangeSenderQualifier();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsSubmitted */
    public static final String COLUMNNAME_IsSubmitted = "IsSubmitted";

	/** Set Submitted	  */
	public void setIsSubmitted (boolean IsSubmitted);

	/** Get Submitted	  */
	public boolean isSubmitted();

    /** Column name LinesAccepted */
    public static final String COLUMNNAME_LinesAccepted = "LinesAccepted";

	/** Set Lines Accepted	  */
	public void setLinesAccepted (BigDecimal LinesAccepted);

	/** Get Lines Accepted	  */
	public BigDecimal getLinesAccepted();

    /** Column name LinesIncluded */
    public static final String COLUMNNAME_LinesIncluded = "LinesIncluded";

	/** Set Lines Included	  */
	public void setLinesIncluded (BigDecimal LinesIncluded);

	/** Get Lines Included	  */
	public BigDecimal getLinesIncluded();

    /** Column name LinesReceived */
    public static final String COLUMNNAME_LinesReceived = "LinesReceived";

	/** Set Lines Received	  */
	public void setLinesReceived (BigDecimal LinesReceived);

	/** Get Lines Received	  */
	public BigDecimal getLinesReceived();

    /** Column name SenderGroupControlNo */
    public static final String COLUMNNAME_SenderGroupControlNo = "SenderGroupControlNo";

	/** Set Sender Group Control Number	  */
	public void setSenderGroupControlNo (String SenderGroupControlNo);

	/** Get Sender Group Control Number	  */
	public String getSenderGroupControlNo();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
