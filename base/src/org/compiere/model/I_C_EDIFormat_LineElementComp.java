/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for C_EDIFormat_LineElementComp
 *  @author Adempiere (generated) 
 *  @version 1.5.0
 */
public interface I_C_EDIFormat_LineElementComp 
{

    /** TableName=C_EDIFormat_LineElementComp */
    public static final String Table_Name = "C_EDIFormat_LineElementComp";

    /** AD_Table_ID=53426 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Column_ID */
    public static final String COLUMNNAME_AD_Column_ID = "AD_Column_ID";

	/** Set Column.
	  * Column in the table
	  */
	public void setAD_Column_ID (int AD_Column_ID);

	/** Get Column.
	  * Column in the table
	  */
	public int getAD_Column_ID();

	public I_AD_Column getAD_Column() throws RuntimeException;

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_Reference_ID */
    public static final String COLUMNNAME_AD_Reference_ID = "AD_Reference_ID";

	/** Set Reference.
	  * System Reference and Validation
	  */
	public void setAD_Reference_ID (int AD_Reference_ID);

	/** Get Reference.
	  * System Reference and Validation
	  */
	public int getAD_Reference_ID();

	public I_AD_Reference getAD_Reference() throws RuntimeException;

    /** Column name C_EDIFormat_LineElementComp_ID */
    public static final String COLUMNNAME_C_EDIFormat_LineElementComp_ID = "C_EDIFormat_LineElementComp_ID";

	/** Set EDI Format Line Element Component	  */
	public void setC_EDIFormat_LineElementComp_ID (int C_EDIFormat_LineElementComp_ID);

	/** Get EDI Format Line Element Component	  */
	public int getC_EDIFormat_LineElementComp_ID();

    /** Column name C_EDIFormat_LineElement_ID */
    public static final String COLUMNNAME_C_EDIFormat_LineElement_ID = "C_EDIFormat_LineElement_ID";

	/** Set EDI Format Line Element	  */
	public void setC_EDIFormat_LineElement_ID (int C_EDIFormat_LineElement_ID);

	/** Get EDI Format Line Element	  */
	public int getC_EDIFormat_LineElement_ID();

	public I_C_EDIFormat_LineElement getC_EDIFormat_LineElement() throws RuntimeException;

    /** Column name ConstValue */
    public static final String COLUMNNAME_ConstValue = "ConstValue";

	/** Set Const Value	  */
	public void setConstValue (String ConstValue);

	/** Get Const Value	  */
	public String getConstValue();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateFormat */
    public static final String COLUMNNAME_DateFormat = "DateFormat";

	/** Set Date Format.
	  * Date format used in the input format
	  */
	public void setDateFormat (String DateFormat);

	/** Get Date Format.
	  * Date format used in the input format
	  */
	public String getDateFormat();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name ElementType */
    public static final String COLUMNNAME_ElementType = "ElementType";

	/** Set Type.
	  * Element Type (account or user defined)
	  */
	public void setElementType (String ElementType);

	/** Get Type.
	  * Element Type (account or user defined)
	  */
	public String getElementType();

    /** Column name Header_Column_ID */
    public static final String COLUMNNAME_Header_Column_ID = "Header_Column_ID";

	/** Set Header Column	  */
	public void setHeader_Column_ID (int Header_Column_ID);

	/** Get Header Column	  */
	public int getHeader_Column_ID();

	public I_AD_Column getHeader_Column() throws RuntimeException;

    /** Column name Help */
    public static final String COLUMNNAME_Help = "Help";

	/** Set Comment/Help.
	  * Comment or Hint
	  */
	public void setHelp (String Help);

	/** Get Comment/Help.
	  * Comment or Hint
	  */
	public String getHelp();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsMandatory */
    public static final String COLUMNNAME_IsMandatory = "IsMandatory";

	/** Set Mandatory.
	  * Data entry is required in this column
	  */
	public void setIsMandatory (boolean IsMandatory);

	/** Get Mandatory.
	  * Data entry is required in this column
	  */
	public boolean isMandatory();

    /** Column name LookupColumn */
    public static final String COLUMNNAME_LookupColumn = "LookupColumn";

	/** Set Lookup Column	  */
	public void setLookupColumn (String LookupColumn);

	/** Get Lookup Column	  */
	public String getLookupColumn();

    /** Column name MinFieldLength */
    public static final String COLUMNNAME_MinFieldLength = "MinFieldLength";

	/** Set MinFieldLength	  */
	public void setMinFieldLength (int MinFieldLength);

	/** Get MinFieldLength	  */
	public int getMinFieldLength();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Position */
    public static final String COLUMNNAME_Position = "Position";

	/** Set Position	  */
	public void setPosition (int Position);

	/** Get Position	  */
	public int getPosition();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();

    /** Column name VariableValue */
    public static final String COLUMNNAME_VariableValue = "VariableValue";

	/** Set VariableValue	  */
	public void setVariableValue (String VariableValue);

	/** Get VariableValue	  */
	public String getVariableValue();
}
