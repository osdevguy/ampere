/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for AX_Seasonality
 *  @author Adempiere (generated) 
 *  @version Release 3.5.3a - $Id$ */
public class X_AX_Seasonality extends PO implements I_AX_Seasonality, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

    /** Standard Constructor */
    public X_AX_Seasonality (Properties ctx, int AX_Seasonality_ID, String trxName)
    {
      super (ctx, AX_Seasonality_ID, trxName);
      /** if (AX_Seasonality_ID == 0)
        {
			setAX_Seasonality_ID (0);
			setName (null);
        } */
    }

    /** Load Constructor */
    public X_AX_Seasonality (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_AX_Seasonality[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set April.
		@param Apr April	  */
	public void setApr (int Apr)
	{
		set_Value (COLUMNNAME_Apr, Integer.valueOf(Apr));
	}

	/** Get April.
		@return April	  */
	public int getApr () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Apr);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set August.
		@param Aug August	  */
	public void setAug (int Aug)
	{
		set_Value (COLUMNNAME_Aug, Integer.valueOf(Aug));
	}

	/** Get August.
		@return August	  */
	public int getAug () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Aug);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Seasonality.
		@param AX_Seasonality_ID Seasonality	  */
	public void setAX_Seasonality_ID (int AX_Seasonality_ID)
	{
		if (AX_Seasonality_ID < 1)
			 throw new IllegalArgumentException ("AX_Seasonality_ID is mandatory.");
		set_ValueNoCheck (COLUMNNAME_AX_Seasonality_ID, Integer.valueOf(AX_Seasonality_ID));
	}

	/** Get Seasonality.
		@return Seasonality	  */
	public int getAX_Seasonality_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AX_Seasonality_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set December.
		@param Dec December	  */
	public void setDec (int Dec)
	{
		set_Value (COLUMNNAME_Dec, Integer.valueOf(Dec));
	}

	/** Get December.
		@return December	  */
	public int getDec () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Dec);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set February.
		@param Feb February	  */
	public void setFeb (int Feb)
	{
		set_Value (COLUMNNAME_Feb, Integer.valueOf(Feb));
	}

	/** Get February.
		@return February	  */
	public int getFeb () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Feb);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set January.
		@param Jan January	  */
	public void setJan (int Jan)
	{
		set_Value (COLUMNNAME_Jan, Integer.valueOf(Jan));
	}

	/** Get January.
		@return January	  */
	public int getJan () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Jan);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set July.
		@param Jul July	  */
	public void setJul (int Jul)
	{
		set_Value (COLUMNNAME_Jul, Integer.valueOf(Jul));
	}

	/** Get July.
		@return July	  */
	public int getJul () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Jul);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set June.
		@param Jun June	  */
	public void setJun (int Jun)
	{
		set_Value (COLUMNNAME_Jun, Integer.valueOf(Jun));
	}

	/** Get June.
		@return June	  */
	public int getJun () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Jun);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set March.
		@param Mar March	  */
	public void setMar (int Mar)
	{
		set_Value (COLUMNNAME_Mar, Integer.valueOf(Mar));
	}

	/** Get March.
		@return March	  */
	public int getMar () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Mar);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set May.
		@param May May	  */
	public void setMay (int May)
	{
		set_Value (COLUMNNAME_May, Integer.valueOf(May));
	}

	/** Get May.
		@return May	  */
	public int getMay () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_May);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		if (Name == null)
			throw new IllegalArgumentException ("Name is mandatory.");
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set November.
		@param Nov November	  */
	public void setNov (int Nov)
	{
		set_Value (COLUMNNAME_Nov, Integer.valueOf(Nov));
	}

	/** Get November.
		@return November	  */
	public int getNov () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Nov);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set October.
		@param Oct October	  */
	public void setOct (int Oct)
	{
		set_Value (COLUMNNAME_Oct, Integer.valueOf(Oct));
	}

	/** Get October.
		@return October	  */
	public int getOct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Oct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set September.
		@param Sep September	  */
	public void setSep (int Sep)
	{
		set_Value (COLUMNNAME_Sep, Integer.valueOf(Sep));
	}

	/** Get September.
		@return September	  */
	public int getSep () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Sep);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}