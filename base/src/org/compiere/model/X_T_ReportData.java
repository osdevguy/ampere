/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for T_ReportData
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_T_ReportData extends PO implements I_T_ReportData, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_T_ReportData (Properties ctx, int T_ReportData_ID, String trxName)
    {
      super (ctx, T_ReportData_ID, trxName);
      /** if (T_ReportData_ID == 0)
        {
			setAD_PInstance_ID (0);
        } */
    }

    /** Load Constructor */
    public X_T_ReportData (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 4 - System 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_T_ReportData[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_AD_PInstance getAD_PInstance() throws RuntimeException
    {
		return (I_AD_PInstance)MTable.get(getCtx(), I_AD_PInstance.Table_Name)
			.getPO(getAD_PInstance_ID(), get_TrxName());	}

	/** Set Process Instance.
		@param AD_PInstance_ID 
		Instance of the process
	  */
	public void setAD_PInstance_ID (int AD_PInstance_ID)
	{
		if (AD_PInstance_ID < 1) 
			set_Value (COLUMNNAME_AD_PInstance_ID, null);
		else 
			set_Value (COLUMNNAME_AD_PInstance_ID, Integer.valueOf(AD_PInstance_ID));
	}

	/** Get Process Instance.
		@return Instance of the process
	  */
	public int getAD_PInstance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_PInstance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Amount 0.
		@param Amt_0 Amount 0	  */
	public void setAmt_0 (BigDecimal Amt_0)
	{
		set_Value (COLUMNNAME_Amt_0, Amt_0);
	}

	/** Get Amount 0.
		@return Amount 0	  */
	public BigDecimal getAmt_0 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amt_0);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Amount 1.
		@param Amt_1 Amount 1	  */
	public void setAmt_1 (BigDecimal Amt_1)
	{
		set_Value (COLUMNNAME_Amt_1, Amt_1);
	}

	/** Get Amount 1.
		@return Amount 1	  */
	public BigDecimal getAmt_1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amt_1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Amount 2.
		@param Amt_2 Amount 2	  */
	public void setAmt_2 (BigDecimal Amt_2)
	{
		set_Value (COLUMNNAME_Amt_2, Amt_2);
	}

	/** Get Amount 2.
		@return Amount 2	  */
	public BigDecimal getAmt_2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amt_2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Amount 3.
		@param Amt_3 Amount 3	  */
	public void setAmt_3 (BigDecimal Amt_3)
	{
		set_Value (COLUMNNAME_Amt_3, Amt_3);
	}

	/** Get Amount 3.
		@return Amount 3	  */
	public BigDecimal getAmt_3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amt_3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Amount 4.
		@param Amt_4 Amount 4	  */
	public void setAmt_4 (BigDecimal Amt_4)
	{
		set_Value (COLUMNNAME_Amt_4, Amt_4);
	}

	/** Get Amount 4.
		@return Amount 4	  */
	public BigDecimal getAmt_4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amt_4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Date 0.
		@param Date_0 Date 0	  */
	public void setDate_0 (Timestamp Date_0)
	{
		set_Value (COLUMNNAME_Date_0, Date_0);
	}

	/** Get Date 0.
		@return Date 0	  */
	public Timestamp getDate_0 () 
	{
		return (Timestamp)get_Value(COLUMNNAME_Date_0);
	}

	/** Set Date 1.
		@param Date_1 Date 1	  */
	public void setDate_1 (Timestamp Date_1)
	{
		set_Value (COLUMNNAME_Date_1, Date_1);
	}

	/** Get Date 1.
		@return Date 1	  */
	public Timestamp getDate_1 () 
	{
		return (Timestamp)get_Value(COLUMNNAME_Date_1);
	}

	/** Set Level no.
		@param LevelNo Level no	  */
	public void setLevelNo (int LevelNo)
	{
		set_Value (COLUMNNAME_LevelNo, Integer.valueOf(LevelNo));
	}

	/** Get Level no.
		@return Level no	  */
	public int getLevelNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_LevelNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity 0.
		@param Qty_0 Quantity 0	  */
	public void setQty_0 (BigDecimal Qty_0)
	{
		set_Value (COLUMNNAME_Qty_0, Qty_0);
	}

	/** Get Quantity 0.
		@return Quantity 0	  */
	public BigDecimal getQty_0 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty_0);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity 1.
		@param Qty_1 Quantity 1	  */
	public void setQty_1 (BigDecimal Qty_1)
	{
		set_Value (COLUMNNAME_Qty_1, Qty_1);
	}

	/** Get Quantity 1.
		@return Quantity 1	  */
	public BigDecimal getQty_1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty_1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Sequence.
		@param SeqNo 
		Method of ordering records; lowest number comes first
	  */
	public void setSeqNo (int SeqNo)
	{
		set_Value (COLUMNNAME_SeqNo, Integer.valueOf(SeqNo));
	}

	/** Get Sequence.
		@return Method of ordering records; lowest number comes first
	  */
	public int getSeqNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SeqNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Text 0.
		@param Text_0 Text 0	  */
	public void setText_0 (String Text_0)
	{
		set_Value (COLUMNNAME_Text_0, Text_0);
	}

	/** Get Text 0.
		@return Text 0	  */
	public String getText_0 () 
	{
		return (String)get_Value(COLUMNNAME_Text_0);
	}

	/** Set Text 1.
		@param Text_1 Text 1	  */
	public void setText_1 (String Text_1)
	{
		set_Value (COLUMNNAME_Text_1, Text_1);
	}

	/** Get Text 1.
		@return Text 1	  */
	public String getText_1 () 
	{
		return (String)get_Value(COLUMNNAME_Text_1);
	}

	/** Set Text 2.
		@param Text_2 Text 2	  */
	public void setText_2 (String Text_2)
	{
		set_Value (COLUMNNAME_Text_2, Text_2);
	}

	/** Get Text 2.
		@return Text 2	  */
	public String getText_2 () 
	{
		return (String)get_Value(COLUMNNAME_Text_2);
	}

	/** Set Text 3.
		@param Text_3 Text 3	  */
	public void setText_3 (String Text_3)
	{
		set_Value (COLUMNNAME_Text_3, Text_3);
	}

	/** Get Text 3.
		@return Text 3	  */
	public String getText_3 () 
	{
		return (String)get_Value(COLUMNNAME_Text_3);
	}

	/** Set Text 4.
		@param Text_4 Text 4	  */
	public void setText_4 (String Text_4)
	{
		set_Value (COLUMNNAME_Text_4, Text_4);
	}

	/** Get Text 4.
		@return Text 4	  */
	public String getText_4 () 
	{
		return (String)get_Value(COLUMNNAME_Text_4);
	}
}