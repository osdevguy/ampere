/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for GAAS_Asset_Group_Acct
 *  @author Adempiere (generated) 
 *  @version 1.03
 */
public interface I_GAAS_Asset_Group_Acct 
{

    /** TableName=GAAS_Asset_Group_Acct */
    public static final String Table_Name = "GAAS_Asset_Group_Acct";

    /** AD_Table_ID=53574 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name A_Asset_Group_ID */
    public static final String COLUMNNAME_A_Asset_Group_ID = "A_Asset_Group_ID";

	/** Set Asset Group.
	  * Group of Assets
	  */
	public void setA_Asset_Group_ID (int A_Asset_Group_ID);

	/** Get Asset Group.
	  * Group of Assets
	  */
	public int getA_Asset_Group_ID();

	public I_A_Asset_Group getA_Asset_Group() throws RuntimeException;

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_AcctSchema_ID */
    public static final String COLUMNNAME_C_AcctSchema_ID = "C_AcctSchema_ID";

	/** Set Accounting Schema.
	  * Rules for accounting
	  */
	public void setC_AcctSchema_ID (int C_AcctSchema_ID);

	/** Get Accounting Schema.
	  * Rules for accounting
	  */
	public int getC_AcctSchema_ID();

	public I_C_AcctSchema getC_AcctSchema() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Default_Depn_Periods */
    public static final String COLUMNNAME_Default_Depn_Periods = "Default_Depn_Periods";

	/** Set Default_Depn_Periods.
	  * Default Depn Periods
	  */
	public void setDefault_Depn_Periods (BigDecimal Default_Depn_Periods);

	/** Get Default_Depn_Periods.
	  * Default Depn Periods
	  */
	public BigDecimal getDefault_Depn_Periods();

    /** Column name DepreciationRate */
    public static final String COLUMNNAME_DepreciationRate = "DepreciationRate";

	/** Set Depreciation Rate	  */
	public void setDepreciationRate (BigDecimal DepreciationRate);

	/** Get Depreciation Rate	  */
	public BigDecimal getDepreciationRate();

    /** Column name GAAS_Accum_Depr_Acct */
    public static final String COLUMNNAME_GAAS_Accum_Depr_Acct = "GAAS_Accum_Depr_Acct";

	/** Set Accumulated Depreciation	  */
	public void setGAAS_Accum_Depr_Acct (int GAAS_Accum_Depr_Acct);

	/** Get Accumulated Depreciation	  */
	public int getGAAS_Accum_Depr_Acct();

	public I_C_ValidCombination getGAAS_Accum_Depr_A() throws RuntimeException;

    /** Column name GAAS_Accum_Depr_OffSet_Acct */
    public static final String COLUMNNAME_GAAS_Accum_Depr_OffSet_Acct = "GAAS_Accum_Depr_OffSet_Acct";

	/** Set Accumulated Depr OffSet	  */
	public void setGAAS_Accum_Depr_OffSet_Acct (int GAAS_Accum_Depr_OffSet_Acct);

	/** Get Accumulated Depr OffSet	  */
	public int getGAAS_Accum_Depr_OffSet_Acct();

	public I_C_ValidCombination getGAAS_Accum_Depr_OffSet_A() throws RuntimeException;

    /** Column name GAAS_Asset_Cost_Acct */
    public static final String COLUMNNAME_GAAS_Asset_Cost_Acct = "GAAS_Asset_Cost_Acct";

	/** Set Asset Cost	  */
	public void setGAAS_Asset_Cost_Acct (int GAAS_Asset_Cost_Acct);

	/** Get Asset Cost	  */
	public int getGAAS_Asset_Cost_Acct();

	public I_C_ValidCombination getGAAS_Asset_Cost_A() throws RuntimeException;

    /** Column name GAAS_Asset_Group_Acct_ID */
    public static final String COLUMNNAME_GAAS_Asset_Group_Acct_ID = "GAAS_Asset_Group_Acct_ID";

	/** Set Asset Group Accounting	  */
	public void setGAAS_Asset_Group_Acct_ID (int GAAS_Asset_Group_Acct_ID);

	/** Get Asset Group Accounting	  */
	public int getGAAS_Asset_Group_Acct_ID();

    /** Column name GAAS_Depreciation_Expense_Acct */
    public static final String COLUMNNAME_GAAS_Depreciation_Expense_Acct = "GAAS_Depreciation_Expense_Acct";

	/** Set Depreciation Expense	  */
	public void setGAAS_Depreciation_Expense_Acct (int GAAS_Depreciation_Expense_Acct);

	/** Get Depreciation Expense	  */
	public int getGAAS_Depreciation_Expense_Acct();

	public I_C_ValidCombination getGAAS_Depreciation_Expense_A() throws RuntimeException;

    /** Column name GAAS_Depreciation_Type */
    public static final String COLUMNNAME_GAAS_Depreciation_Type = "GAAS_Depreciation_Type";

	/** Set Depreciation Method	  */
	public void setGAAS_Depreciation_Type (String GAAS_Depreciation_Type);

	/** Get Depreciation Method	  */
	public String getGAAS_Depreciation_Type();

    /** Column name GAAS_Disposal_Gain_Acct */
    public static final String COLUMNNAME_GAAS_Disposal_Gain_Acct = "GAAS_Disposal_Gain_Acct";

	/** Set Disposal Gain	  */
	public void setGAAS_Disposal_Gain_Acct (int GAAS_Disposal_Gain_Acct);

	/** Get Disposal Gain	  */
	public int getGAAS_Disposal_Gain_Acct();

	public I_C_ValidCombination getGAAS_Disposal_Gain_A() throws RuntimeException;

    /** Column name GAAS_Disposal_Loss_Acct */
    public static final String COLUMNNAME_GAAS_Disposal_Loss_Acct = "GAAS_Disposal_Loss_Acct";

	/** Set Disposal Loss	  */
	public void setGAAS_Disposal_Loss_Acct (int GAAS_Disposal_Loss_Acct);

	/** Get Disposal Loss	  */
	public int getGAAS_Disposal_Loss_Acct();

	public I_C_ValidCombination getGAAS_Disposal_Loss_A() throws RuntimeException;

    /** Column name GAAS_InTransfer_Incoming_Acct */
    public static final String COLUMNNAME_GAAS_InTransfer_Incoming_Acct = "GAAS_InTransfer_Incoming_Acct";

	/** Set In Transfer Incoming	  */
	public void setGAAS_InTransfer_Incoming_Acct (int GAAS_InTransfer_Incoming_Acct);

	/** Get In Transfer Incoming	  */
	public int getGAAS_InTransfer_Incoming_Acct();

	public I_C_ValidCombination getGAAS_InTransfer_Incoming_A() throws RuntimeException;

    /** Column name GAAS_InTransfer_Outgoing_Acct */
    public static final String COLUMNNAME_GAAS_InTransfer_Outgoing_Acct = "GAAS_InTransfer_Outgoing_Acct";

	/** Set In Transfer Outgoing	  */
	public void setGAAS_InTransfer_Outgoing_Acct (int GAAS_InTransfer_Outgoing_Acct);

	/** Get In Transfer Outgoing	  */
	public int getGAAS_InTransfer_Outgoing_Acct();

	public I_C_ValidCombination getGAAS_InTransfer_Outgoing_A() throws RuntimeException;

    /** Column name GAAS_Revaluation_Expense_Acct */
    public static final String COLUMNNAME_GAAS_Revaluation_Expense_Acct = "GAAS_Revaluation_Expense_Acct";

	/** Set Revaluation Expense	  */
	public void setGAAS_Revaluation_Expense_Acct (int GAAS_Revaluation_Expense_Acct);

	/** Get Revaluation Expense	  */
	public int getGAAS_Revaluation_Expense_Acct();

	public I_C_ValidCombination getGAAS_Revaluation_Expense_A() throws RuntimeException;

    /** Column name GAAS_Revaluation_Gain_Acct */
    public static final String COLUMNNAME_GAAS_Revaluation_Gain_Acct = "GAAS_Revaluation_Gain_Acct";

	/** Set Revaluation Gain	  */
	public void setGAAS_Revaluation_Gain_Acct (int GAAS_Revaluation_Gain_Acct);

	/** Get Revaluation Gain	  */
	public int getGAAS_Revaluation_Gain_Acct();

	public I_C_ValidCombination getGAAS_Revaluation_Gain_A() throws RuntimeException;

    /** Column name GAAS_Revaluation_Loss_Acct */
    public static final String COLUMNNAME_GAAS_Revaluation_Loss_Acct = "GAAS_Revaluation_Loss_Acct";

	/** Set Revaluation Loss	  */
	public void setGAAS_Revaluation_Loss_Acct (int GAAS_Revaluation_Loss_Acct);

	/** Get Revaluation Loss	  */
	public int getGAAS_Revaluation_Loss_Acct();

	public I_C_ValidCombination getGAAS_Revaluation_Loss_A() throws RuntimeException;

    /** Column name GAAS_Revaluation_OffSet_Acct */
    public static final String COLUMNNAME_GAAS_Revaluation_OffSet_Acct = "GAAS_Revaluation_OffSet_Acct";

	/** Set Revaluation OffSet	  */
	public void setGAAS_Revaluation_OffSet_Acct (int GAAS_Revaluation_OffSet_Acct);

	/** Get Revaluation OffSet	  */
	public int getGAAS_Revaluation_OffSet_Acct();

	public I_C_ValidCombination getGAAS_Revaluation_OffSet_A() throws RuntimeException;

    /** Column name GAAS_Revaluation_Reserve_Acct */
    public static final String COLUMNNAME_GAAS_Revaluation_Reserve_Acct = "GAAS_Revaluation_Reserve_Acct";

	/** Set Revaluation Reserve	  */
	public void setGAAS_Revaluation_Reserve_Acct (int GAAS_Revaluation_Reserve_Acct);

	/** Get Revaluation Reserve	  */
	public int getGAAS_Revaluation_Reserve_Acct();

	public I_C_ValidCombination getGAAS_Revaluation_Reserve_A() throws RuntimeException;

    /** Column name GAAS_Unidentified_Add_Acct */
    public static final String COLUMNNAME_GAAS_Unidentified_Add_Acct = "GAAS_Unidentified_Add_Acct";

	/** Set Unidentified Additions	  */
	public void setGAAS_Unidentified_Add_Acct (int GAAS_Unidentified_Add_Acct);

	/** Get Unidentified Additions	  */
	public int getGAAS_Unidentified_Add_Acct();

	public I_C_ValidCombination getGAAS_Unidentified_Add_A() throws RuntimeException;

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name PostingType */
    public static final String COLUMNNAME_PostingType = "PostingType";

	/** Set PostingType.
	  * The type of posted amount for the transaction
	  */
	public void setPostingType (String PostingType);

	/** Get PostingType.
	  * The type of posted amount for the transaction
	  */
	public String getPostingType();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
