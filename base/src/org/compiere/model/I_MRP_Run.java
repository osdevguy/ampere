/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for MRP_Run
 *  @author Adempiere (generated) 
 *  @version 1.6.3
 */
public interface I_MRP_Run 
{

    /** TableName=MRP_Run */
    public static final String Table_Name = "MRP_Run";

    /** AD_Table_ID=53974 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AggregateReqByPeriod */
    public static final String COLUMNNAME_AggregateReqByPeriod = "AggregateReqByPeriod";

	/** Set Aggregate Requisition By Period.
	  * Aggregate Requisition Into Period
	  */
	public void setAggregateReqByPeriod (String AggregateReqByPeriod);

	/** Get Aggregate Requisition By Period.
	  * Aggregate Requisition Into Period
	  */
	public String getAggregateReqByPeriod();

    /** Column name AggregateRequisition */
    public static final String COLUMNNAME_AggregateRequisition = "AggregateRequisition";

	/** Set Aggregate Requisition	  */
	public void setAggregateRequisition (String AggregateRequisition);

	/** Get Aggregate Requisition	  */
	public String getAggregateRequisition();

    /** Column name C_DocType_ConfirmedOrder_ID */
    public static final String COLUMNNAME_C_DocType_ConfirmedOrder_ID = "C_DocType_ConfirmedOrder_ID";

	/** Set Confirmed Mfg Order Doc Type	  */
	public void setC_DocType_ConfirmedOrder_ID (int C_DocType_ConfirmedOrder_ID);

	/** Get Confirmed Mfg Order Doc Type	  */
	public int getC_DocType_ConfirmedOrder_ID();

	public I_C_DocType getC_DocType_ConfirmedOrder() throws RuntimeException;

    /** Column name C_DocType_MRPRequisition_ID */
    public static final String COLUMNNAME_C_DocType_MRPRequisition_ID = "C_DocType_MRPRequisition_ID";

	/** Set MRP Requisition Doc Type	  */
	public void setC_DocType_MRPRequisition_ID (int C_DocType_MRPRequisition_ID);

	/** Get MRP Requisition Doc Type	  */
	public int getC_DocType_MRPRequisition_ID();

	public I_C_DocType getC_DocType_MRPRequisition() throws RuntimeException;

    /** Column name C_DocType_PlannedOrder_ID */
    public static final String COLUMNNAME_C_DocType_PlannedOrder_ID = "C_DocType_PlannedOrder_ID";

	/** Set Planned Mfg Order Doc Type	  */
	public void setC_DocType_PlannedOrder_ID (int C_DocType_PlannedOrder_ID);

	/** Get Planned Mfg Order Doc Type	  */
	public int getC_DocType_PlannedOrder_ID();

	public I_C_DocType getC_DocType_PlannedOrder() throws RuntimeException;

    /** Column name C_DocType_PO_ID */
    public static final String COLUMNNAME_C_DocType_PO_ID = "C_DocType_PO_ID";

	/** Set Purchase Order Doc Type	  */
	public void setC_DocType_PO_ID (int C_DocType_PO_ID);

	/** Get Purchase Order Doc Type	  */
	public int getC_DocType_PO_ID();

	public I_C_DocType getC_DocType_PO() throws RuntimeException;

    /** Column name ChartCapacity */
    public static final String COLUMNNAME_ChartCapacity = "ChartCapacity";

	/** Set Chart Capacity	  */
	public void setChartCapacity (Object ChartCapacity);

	/** Get Chart Capacity	  */
	public Object getChartCapacity();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateFinish */
    public static final String COLUMNNAME_DateFinish = "DateFinish";

	/** Set Finish Date.
	  * Finish or (planned) completion date
	  */
	public void setDateFinish (Timestamp DateFinish);

	/** Get Finish Date.
	  * Finish or (planned) completion date
	  */
	public Timestamp getDateFinish();

    /** Column name DateStart */
    public static final String COLUMNNAME_DateStart = "DateStart";

	/** Set Date Start.
	  * Date Start for this Order
	  */
	public void setDateStart (Timestamp DateStart);

	/** Get Date Start.
	  * Date Start for this Order
	  */
	public Timestamp getDateStart();

    /** Column name GenerateReport */
    public static final String COLUMNNAME_GenerateReport = "GenerateReport";

	/** Set Generate Report	  */
	public void setGenerateReport (String GenerateReport);

	/** Get Generate Report	  */
	public String getGenerateReport();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsDeletePlannedPO */
    public static final String COLUMNNAME_IsDeletePlannedPO = "IsDeletePlannedPO";

	/** Set Delete Planned Purchase Orders	  */
	public void setIsDeletePlannedPO (boolean IsDeletePlannedPO);

	/** Get Delete Planned Purchase Orders	  */
	public boolean isDeletePlannedPO();

    /** Column name IsDeleteUnconfirmedProduction */
    public static final String COLUMNNAME_IsDeleteUnconfirmedProduction = "IsDeleteUnconfirmedProduction";

	/** Set Delete Unconfirmed Production	  */
	public void setIsDeleteUnconfirmedProduction (boolean IsDeleteUnconfirmedProduction);

	/** Get Delete Unconfirmed Production	  */
	public boolean isDeleteUnconfirmedProduction();

    /** Column name IsExcludeKanbanProduct */
    public static final String COLUMNNAME_IsExcludeKanbanProduct = "IsExcludeKanbanProduct";

	/** Set Exclude Kanban Product	  */
	public void setIsExcludeKanbanProduct (boolean IsExcludeKanbanProduct);

	/** Get Exclude Kanban Product	  */
	public boolean isExcludeKanbanProduct();

    /** Column name IsProcessing */
    public static final String COLUMNNAME_IsProcessing = "IsProcessing";

	/** Set Processing	  */
	public void setIsProcessing (boolean IsProcessing);

	/** Get Processing	  */
	public boolean isIsProcessing();

    /** Column name M_PriceList_ID */
    public static final String COLUMNNAME_M_PriceList_ID = "M_PriceList_ID";

	/** Set Price List.
	  * Unique identifier of a Price List
	  */
	public void setM_PriceList_ID (int M_PriceList_ID);

	/** Get Price List.
	  * Unique identifier of a Price List
	  */
	public int getM_PriceList_ID();

	public I_M_PriceList getM_PriceList() throws RuntimeException;

    /** Column name MRP_Run_ID */
    public static final String COLUMNNAME_MRP_Run_ID = "MRP_Run_ID";

	/** Set MRP_Run ID	  */
	public void setMRP_Run_ID (int MRP_Run_ID);

	/** Get MRP_Run ID	  */
	public int getMRP_Run_ID();

    /** Column name MRPInitialSetup */
    public static final String COLUMNNAME_MRPInitialSetup = "MRPInitialSetup";

	/** Set MRP Initial Setup	  */
	public void setMRPInitialSetup (String MRPInitialSetup);

	/** Get MRP Initial Setup	  */
	public String getMRPInitialSetup();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name PlannedProductionReport */
    public static final String COLUMNNAME_PlannedProductionReport = "PlannedProductionReport";

	/** Set Generate Planned Production Report	  */
	public void setPlannedProductionReport (String PlannedProductionReport);

	/** Get Generate Planned Production Report	  */
	public String getPlannedProductionReport();

    /** Column name Processing */
    public static final String COLUMNNAME_Processing = "Processing";

	/** Set Process Now	  */
	public void setProcessing (boolean Processing);

	/** Get Process Now	  */
	public boolean isProcessing();

    /** Column name SuggestedRequisitionReport */
    public static final String COLUMNNAME_SuggestedRequisitionReport = "SuggestedRequisitionReport";

	/** Set Generate Suggested Requisition Report	  */
	public void setSuggestedRequisitionReport (String SuggestedRequisitionReport);

	/** Get Generate Suggested Requisition Report	  */
	public String getSuggestedRequisitionReport();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
