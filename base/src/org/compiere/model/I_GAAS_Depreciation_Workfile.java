/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for GAAS_Depreciation_Workfile
 *  @author Adempiere (generated) 
 *  @version 1.03
 */
public interface I_GAAS_Depreciation_Workfile 
{

    /** TableName=GAAS_Depreciation_Workfile */
    public static final String Table_Name = "GAAS_Depreciation_Workfile";

    /** AD_Table_ID=53579 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name A_Asset_ID */
    public static final String COLUMNNAME_A_Asset_ID = "A_Asset_ID";

	/** Set Asset.
	  * Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID);

	/** Get Asset.
	  * Asset used internally or by customers
	  */
	public int getA_Asset_ID();

	public I_A_Asset getA_Asset() throws RuntimeException;

    /** Column name A_Base_Amount */
    public static final String COLUMNNAME_A_Base_Amount = "A_Base_Amount";

	/** Set Base Amount	  */
	public void setA_Base_Amount (BigDecimal A_Base_Amount);

	/** Get Base Amount	  */
	public BigDecimal getA_Base_Amount();

    /** Column name AccumulatedDepr */
    public static final String COLUMNNAME_AccumulatedDepr = "AccumulatedDepr";

	/** Set Accumulated Depreciation	  */
	public void setAccumulatedDepr (BigDecimal AccumulatedDepr);

	/** Get Accumulated Depreciation	  */
	public BigDecimal getAccumulatedDepr();

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name A_QTY_Current */
    public static final String COLUMNNAME_A_QTY_Current = "A_QTY_Current";

	/** Set Quantity	  */
	public void setA_QTY_Current (BigDecimal A_QTY_Current);

	/** Get Quantity	  */
	public BigDecimal getA_QTY_Current();

    /** Column name A_QTY_Original */
    public static final String COLUMNNAME_A_QTY_Original = "A_QTY_Original";

	/** Set Original Qty	  */
	public void setA_QTY_Original (BigDecimal A_QTY_Original);

	/** Get Original Qty	  */
	public BigDecimal getA_QTY_Original();

    /** Column name AssetCost */
    public static final String COLUMNNAME_AssetCost = "AssetCost";

	/** Set Asset Cost	  */
	public void setAssetCost (BigDecimal AssetCost);

	/** Get Asset Cost	  */
	public BigDecimal getAssetCost();

    /** Column name AssetDepreciationDate */
    public static final String COLUMNNAME_AssetDepreciationDate = "AssetDepreciationDate";

	/** Set Asset Depreciation Date.
	  * Date of last depreciation
	  */
	public void setAssetDepreciationDate (Timestamp AssetDepreciationDate);

	/** Get Asset Depreciation Date.
	  * Date of last depreciation
	  */
	public Timestamp getAssetDepreciationDate();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name CurrentDeprExpense */
    public static final String COLUMNNAME_CurrentDeprExpense = "CurrentDeprExpense";

	/** Set Current Depreciation Expense	  */
	public void setCurrentDeprExpense (BigDecimal CurrentDeprExpense);

	/** Get Current Depreciation Expense	  */
	public BigDecimal getCurrentDeprExpense();

    /** Column name CurrentLifeYear */
    public static final String COLUMNNAME_CurrentLifeYear = "CurrentLifeYear";

	/** Set Current Life Year	  */
	public void setCurrentLifeYear (int CurrentLifeYear);

	/** Get Current Life Year	  */
	public int getCurrentLifeYear();

    /** Column name CurrentPeriod */
    public static final String COLUMNNAME_CurrentPeriod = "CurrentPeriod";

	/** Set Current Period	  */
	public void setCurrentPeriod (int CurrentPeriod);

	/** Get Current Period	  */
	public int getCurrentPeriod();

    /** Column name Expense_SL */
    public static final String COLUMNNAME_Expense_SL = "Expense_SL";

	/** Set SL Expense/Period	  */
	public void setExpense_SL (BigDecimal Expense_SL);

	/** Get SL Expense/Period	  */
	public BigDecimal getExpense_SL();

    /** Column name GAAS_BuildDepreciation */
    public static final String COLUMNNAME_GAAS_BuildDepreciation = "GAAS_BuildDepreciation";

	/** Set Build Depreciation Expense	  */
	public void setGAAS_BuildDepreciation (String GAAS_BuildDepreciation);

	/** Get Build Depreciation Expense	  */
	public String getGAAS_BuildDepreciation();

    /** Column name GAAS_Depreciation_Workfile_ID */
    public static final String COLUMNNAME_GAAS_Depreciation_Workfile_ID = "GAAS_Depreciation_Workfile_ID";

	/** Set Depreciation Workfile	  */
	public void setGAAS_Depreciation_Workfile_ID (int GAAS_Depreciation_Workfile_ID);

	/** Get Depreciation Workfile	  */
	public int getGAAS_Depreciation_Workfile_ID();

    /** Column name InitialAccumulatedDepr */
    public static final String COLUMNNAME_InitialAccumulatedDepr = "InitialAccumulatedDepr";

	/** Set Initial Accumulated Depreciation	  */
	public void setInitialAccumulatedDepr (BigDecimal InitialAccumulatedDepr);

	/** Get Initial Accumulated Depreciation	  */
	public BigDecimal getInitialAccumulatedDepr();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsDepreciationProcessed */
    public static final String COLUMNNAME_IsDepreciationProcessed = "IsDepreciationProcessed";

	/** Set Depreciation Processed	  */
	public void setIsDepreciationProcessed (boolean IsDepreciationProcessed);

	/** Get Depreciation Processed	  */
	public boolean isDepreciationProcessed();

    /** Column name LifeYears */
    public static final String COLUMNNAME_LifeYears = "LifeYears";

	/** Set Life Years	  */
	public void setLifeYears (int LifeYears);

	/** Get Life Years	  */
	public int getLifeYears();

    /** Column name PostingType */
    public static final String COLUMNNAME_PostingType = "PostingType";

	/** Set PostingType.
	  * The type of posted amount for the transaction
	  */
	public void setPostingType (String PostingType);

	/** Get PostingType.
	  * The type of posted amount for the transaction
	  */
	public String getPostingType();

    /** Column name RemainingAmt */
    public static final String COLUMNNAME_RemainingAmt = "RemainingAmt";

	/** Set Remaining Amt.
	  * Remaining Amount
	  */
	public void setRemainingAmt (BigDecimal RemainingAmt);

	/** Get Remaining Amt.
	  * Remaining Amount
	  */
	public BigDecimal getRemainingAmt();

    /** Column name SalvageValue */
    public static final String COLUMNNAME_SalvageValue = "SalvageValue";

	/** Set Salvage Value	  */
	public void setSalvageValue (BigDecimal SalvageValue);

	/** Get Salvage Value	  */
	public BigDecimal getSalvageValue();

    /** Column name StartPeriod */
    public static final String COLUMNNAME_StartPeriod = "StartPeriod";

	/** Set Start Period	  */
	public void setStartPeriod (int StartPeriod);

	/** Get Start Period	  */
	public int getStartPeriod();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name UseLifeMonths */
    public static final String COLUMNNAME_UseLifeMonths = "UseLifeMonths";

	/** Set Usable Life - Months.
	  * Months of the usable life of the asset
	  */
	public void setUseLifeMonths (int UseLifeMonths);

	/** Get Usable Life - Months.
	  * Months of the usable life of the asset
	  */
	public int getUseLifeMonths();

    /** Column name UseLifePeriods */
    public static final String COLUMNNAME_UseLifePeriods = "UseLifePeriods";

	/** Set UseLife Periods	  */
	public void setUseLifePeriods (int UseLifePeriods);

	/** Get UseLife Periods	  */
	public int getUseLifePeriods();

    /** Column name UseLifeYears */
    public static final String COLUMNNAME_UseLifeYears = "UseLifeYears";

	/** Set Usable Life - Years.
	  * Years of the usable life of the asset
	  */
	public void setUseLifeYears (int UseLifeYears);

	/** Get Usable Life - Years.
	  * Years of the usable life of the asset
	  */
	public int getUseLifeYears();
}
