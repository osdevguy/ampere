/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for GAAS_Depreciation_Workfile
 *  @author Adempiere (generated) 
 *  @version 1.03 - $Id$ */
public class X_GAAS_Depreciation_Workfile extends PO implements I_GAAS_Depreciation_Workfile, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20131008L;

    /** Standard Constructor */
    public X_GAAS_Depreciation_Workfile (Properties ctx, int GAAS_Depreciation_Workfile_ID, String trxName)
    {
      super (ctx, GAAS_Depreciation_Workfile_ID, trxName);
      /** if (GAAS_Depreciation_Workfile_ID == 0)
        {
			setA_Asset_ID (0);
			setAssetCost (Env.ZERO);
// 0
			setCurrentDeprExpense (Env.ZERO);
// 0
			setGAAS_Depreciation_Workfile_ID (0);
			setInitialAccumulatedDepr (Env.ZERO);
// 0
			setLifeYears (0);
// 0
			setPostingType (null);
// A
			setSalvageValue (Env.ZERO);
// 0
			setUseLifeMonths (0);
// 0
			setUseLifeYears (0);
// 0
        } */
    }

    /** Load Constructor */
    public X_GAAS_Depreciation_Workfile (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_GAAS_Depreciation_Workfile[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_A_Asset getA_Asset() throws RuntimeException
    {
		return (I_A_Asset)MTable.get(getCtx(), I_A_Asset.Table_Name)
			.getPO(getA_Asset_ID(), get_TrxName());	}

	/** Set Asset.
		@param A_Asset_ID 
		Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID)
	{
		if (A_Asset_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_ID, Integer.valueOf(A_Asset_ID));
	}

	/** Get Asset.
		@return Asset used internally or by customers
	  */
	public int getA_Asset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Base Amount.
		@param A_Base_Amount Base Amount	  */
	public void setA_Base_Amount (BigDecimal A_Base_Amount)
	{
		set_Value (COLUMNNAME_A_Base_Amount, A_Base_Amount);
	}

	/** Get Base Amount.
		@return Base Amount	  */
	public BigDecimal getA_Base_Amount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_Base_Amount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Accumulated Depreciation.
		@param AccumulatedDepr Accumulated Depreciation	  */
	public void setAccumulatedDepr (BigDecimal AccumulatedDepr)
	{
		throw new IllegalArgumentException ("AccumulatedDepr is virtual column");	}

	/** Get Accumulated Depreciation.
		@return Accumulated Depreciation	  */
	public BigDecimal getAccumulatedDepr () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AccumulatedDepr);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param A_QTY_Current Quantity	  */
	public void setA_QTY_Current (BigDecimal A_QTY_Current)
	{
		throw new IllegalArgumentException ("A_QTY_Current is virtual column");	}

	/** Get Quantity.
		@return Quantity	  */
	public BigDecimal getA_QTY_Current () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_QTY_Current);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Original Qty.
		@param A_QTY_Original Original Qty	  */
	public void setA_QTY_Original (BigDecimal A_QTY_Original)
	{
		throw new IllegalArgumentException ("A_QTY_Original is virtual column");	}

	/** Get Original Qty.
		@return Original Qty	  */
	public BigDecimal getA_QTY_Original () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_A_QTY_Original);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Asset Cost.
		@param AssetCost Asset Cost	  */
	public void setAssetCost (BigDecimal AssetCost)
	{
		set_Value (COLUMNNAME_AssetCost, AssetCost);
	}

	/** Get Asset Cost.
		@return Asset Cost	  */
	public BigDecimal getAssetCost () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AssetCost);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Asset Depreciation Date.
		@param AssetDepreciationDate 
		Date of last depreciation
	  */
	public void setAssetDepreciationDate (Timestamp AssetDepreciationDate)
	{
		set_Value (COLUMNNAME_AssetDepreciationDate, AssetDepreciationDate);
	}

	/** Get Asset Depreciation Date.
		@return Date of last depreciation
	  */
	public Timestamp getAssetDepreciationDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_AssetDepreciationDate);
	}

	/** Set Current Depreciation Expense.
		@param CurrentDeprExpense Current Depreciation Expense	  */
	public void setCurrentDeprExpense (BigDecimal CurrentDeprExpense)
	{
		set_Value (COLUMNNAME_CurrentDeprExpense, CurrentDeprExpense);
	}

	/** Get Current Depreciation Expense.
		@return Current Depreciation Expense	  */
	public BigDecimal getCurrentDeprExpense () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CurrentDeprExpense);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Current Life Year.
		@param CurrentLifeYear Current Life Year	  */
	public void setCurrentLifeYear (int CurrentLifeYear)
	{
		set_Value (COLUMNNAME_CurrentLifeYear, Integer.valueOf(CurrentLifeYear));
	}

	/** Get Current Life Year.
		@return Current Life Year	  */
	public int getCurrentLifeYear () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_CurrentLifeYear);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Current Period.
		@param CurrentPeriod Current Period	  */
	public void setCurrentPeriod (int CurrentPeriod)
	{
		set_Value (COLUMNNAME_CurrentPeriod, Integer.valueOf(CurrentPeriod));
	}

	/** Get Current Period.
		@return Current Period	  */
	public int getCurrentPeriod () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_CurrentPeriod);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set SL Expense/Period.
		@param Expense_SL SL Expense/Period	  */
	public void setExpense_SL (BigDecimal Expense_SL)
	{
		throw new IllegalArgumentException ("Expense_SL is virtual column");	}

	/** Get SL Expense/Period.
		@return SL Expense/Period	  */
	public BigDecimal getExpense_SL () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Expense_SL);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Build Depreciation Expense.
		@param GAAS_BuildDepreciation Build Depreciation Expense	  */
	public void setGAAS_BuildDepreciation (String GAAS_BuildDepreciation)
	{
		set_Value (COLUMNNAME_GAAS_BuildDepreciation, GAAS_BuildDepreciation);
	}

	/** Get Build Depreciation Expense.
		@return Build Depreciation Expense	  */
	public String getGAAS_BuildDepreciation () 
	{
		return (String)get_Value(COLUMNNAME_GAAS_BuildDepreciation);
	}

	/** Set Depreciation Workfile.
		@param GAAS_Depreciation_Workfile_ID Depreciation Workfile	  */
	public void setGAAS_Depreciation_Workfile_ID (int GAAS_Depreciation_Workfile_ID)
	{
		if (GAAS_Depreciation_Workfile_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_GAAS_Depreciation_Workfile_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_GAAS_Depreciation_Workfile_ID, Integer.valueOf(GAAS_Depreciation_Workfile_ID));
	}

	/** Get Depreciation Workfile.
		@return Depreciation Workfile	  */
	public int getGAAS_Depreciation_Workfile_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Depreciation_Workfile_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Initial Accumulated Depreciation.
		@param InitialAccumulatedDepr Initial Accumulated Depreciation	  */
	public void setInitialAccumulatedDepr (BigDecimal InitialAccumulatedDepr)
	{
		set_Value (COLUMNNAME_InitialAccumulatedDepr, InitialAccumulatedDepr);
	}

	/** Get Initial Accumulated Depreciation.
		@return Initial Accumulated Depreciation	  */
	public BigDecimal getInitialAccumulatedDepr () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_InitialAccumulatedDepr);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Depreciation Processed.
		@param IsDepreciationProcessed Depreciation Processed	  */
	public void setIsDepreciationProcessed (boolean IsDepreciationProcessed)
	{
		throw new IllegalArgumentException ("IsDepreciationProcessed is virtual column");	}

	/** Get Depreciation Processed.
		@return Depreciation Processed	  */
	public boolean isDepreciationProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_IsDepreciationProcessed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Life Years.
		@param LifeYears Life Years	  */
	public void setLifeYears (int LifeYears)
	{
		set_Value (COLUMNNAME_LifeYears, Integer.valueOf(LifeYears));
	}

	/** Get Life Years.
		@return Life Years	  */
	public int getLifeYears () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_LifeYears);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** PostingType AD_Reference_ID=125 */
	public static final int POSTINGTYPE_AD_Reference_ID=125;
	/** Actual = A */
	public static final String POSTINGTYPE_Actual = "A";
	/** Budget = B */
	public static final String POSTINGTYPE_Budget = "B";
	/** Commitment = E */
	public static final String POSTINGTYPE_Commitment = "E";
	/** Statistical = S */
	public static final String POSTINGTYPE_Statistical = "S";
	/** Reservation = R */
	public static final String POSTINGTYPE_Reservation = "R";
	/** Set PostingType.
		@param PostingType 
		The type of posted amount for the transaction
	  */
	public void setPostingType (String PostingType)
	{

		set_Value (COLUMNNAME_PostingType, PostingType);
	}

	/** Get PostingType.
		@return The type of posted amount for the transaction
	  */
	public String getPostingType () 
	{
		return (String)get_Value(COLUMNNAME_PostingType);
	}

	/** Set Remaining Amt.
		@param RemainingAmt 
		Remaining Amount
	  */
	public void setRemainingAmt (BigDecimal RemainingAmt)
	{
		throw new IllegalArgumentException ("RemainingAmt is virtual column");	}

	/** Get Remaining Amt.
		@return Remaining Amount
	  */
	public BigDecimal getRemainingAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RemainingAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Salvage Value.
		@param SalvageValue Salvage Value	  */
	public void setSalvageValue (BigDecimal SalvageValue)
	{
		set_Value (COLUMNNAME_SalvageValue, SalvageValue);
	}

	/** Get Salvage Value.
		@return Salvage Value	  */
	public BigDecimal getSalvageValue () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SalvageValue);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Start Period.
		@param StartPeriod Start Period	  */
	public void setStartPeriod (int StartPeriod)
	{
		set_Value (COLUMNNAME_StartPeriod, Integer.valueOf(StartPeriod));
	}

	/** Get Start Period.
		@return Start Period	  */
	public int getStartPeriod () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_StartPeriod);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Usable Life - Months.
		@param UseLifeMonths 
		Months of the usable life of the asset
	  */
	public void setUseLifeMonths (int UseLifeMonths)
	{
		set_Value (COLUMNNAME_UseLifeMonths, Integer.valueOf(UseLifeMonths));
	}

	/** Get Usable Life - Months.
		@return Months of the usable life of the asset
	  */
	public int getUseLifeMonths () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UseLifeMonths);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set UseLife Periods.
		@param UseLifePeriods UseLife Periods	  */
	public void setUseLifePeriods (int UseLifePeriods)
	{
		throw new IllegalArgumentException ("UseLifePeriods is virtual column");	}

	/** Get UseLife Periods.
		@return UseLife Periods	  */
	public int getUseLifePeriods () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UseLifePeriods);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Usable Life - Years.
		@param UseLifeYears 
		Years of the usable life of the asset
	  */
	public void setUseLifeYears (int UseLifeYears)
	{
		set_Value (COLUMNNAME_UseLifeYears, Integer.valueOf(UseLifeYears));
	}

	/** Get Usable Life - Years.
		@return Years of the usable life of the asset
	  */
	public int getUseLifeYears () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UseLifeYears);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}