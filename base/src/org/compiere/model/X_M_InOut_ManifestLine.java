/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for M_InOut_ManifestLine
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_M_InOut_ManifestLine extends PO implements I_M_InOut_ManifestLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_M_InOut_ManifestLine (Properties ctx, int M_InOut_ManifestLine_ID, String trxName)
    {
      super (ctx, M_InOut_ManifestLine_ID, trxName);
      /** if (M_InOut_ManifestLine_ID == 0)
        {
			setLine (0);
// @SQL=SELECT NVL(MAX(Line),0)+10 AS DefaultValue FROM M_InOut_ManifestLine WHERE M_InOut_Manifest_ID=@M_InOut_Manifest_ID@
			setM_InOut_ID (0);
			setM_InOut_Manifest_ID (0);
			setM_InOut_ManifestLine_ID (0);
			setSeqNo (0);
        } */
    }

    /** Load Constructor */
    public X_M_InOut_ManifestLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_M_InOut_ManifestLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		throw new IllegalArgumentException ("C_BPartner_ID is virtual column");	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_BPartner_Location getC_BPartner_Location() throws RuntimeException
    {
		return (I_C_BPartner_Location)MTable.get(getCtx(), I_C_BPartner_Location.Table_Name)
			.getPO(getC_BPartner_Location_ID(), get_TrxName());	}

	/** Set Partner Location.
		@param C_BPartner_Location_ID 
		Identifies the (ship to) address for this Business Partner
	  */
	public void setC_BPartner_Location_ID (int C_BPartner_Location_ID)
	{
		throw new IllegalArgumentException ("C_BPartner_Location_ID is virtual column");	}

	/** Get Partner Location.
		@return Identifies the (ship to) address for this Business Partner
	  */
	public int getC_BPartner_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_Location_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Order getC_Order() throws RuntimeException
    {
		return (I_C_Order)MTable.get(getCtx(), I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		throw new IllegalArgumentException ("C_Order_ID is virtual column");	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_Value (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_InOut getM_InOut() throws RuntimeException
    {
		return (I_M_InOut)MTable.get(getCtx(), I_M_InOut.Table_Name)
			.getPO(getM_InOut_ID(), get_TrxName());	}

	/** Set Shipment/Receipt.
		@param M_InOut_ID 
		Material Shipment Document
	  */
	public void setM_InOut_ID (int M_InOut_ID)
	{
		if (M_InOut_ID < 1) 
			set_Value (COLUMNNAME_M_InOut_ID, null);
		else 
			set_Value (COLUMNNAME_M_InOut_ID, Integer.valueOf(M_InOut_ID));
	}

	/** Get Shipment/Receipt.
		@return Material Shipment Document
	  */
	public int getM_InOut_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_InOut_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_InOut_Manifest getM_InOut_Manifest() throws RuntimeException
    {
		return (I_M_InOut_Manifest)MTable.get(getCtx(), I_M_InOut_Manifest.Table_Name)
			.getPO(getM_InOut_Manifest_ID(), get_TrxName());	}

	/** Set Shipment Manifest ID.
		@param M_InOut_Manifest_ID Shipment Manifest ID	  */
	public void setM_InOut_Manifest_ID (int M_InOut_Manifest_ID)
	{
		if (M_InOut_Manifest_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_InOut_Manifest_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_InOut_Manifest_ID, Integer.valueOf(M_InOut_Manifest_ID));
	}

	/** Get Shipment Manifest ID.
		@return Shipment Manifest ID	  */
	public int getM_InOut_Manifest_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_InOut_Manifest_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Shipment Manifest Lines ID.
		@param M_InOut_ManifestLine_ID Shipment Manifest Lines ID	  */
	public void setM_InOut_ManifestLine_ID (int M_InOut_ManifestLine_ID)
	{
		if (M_InOut_ManifestLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_InOut_ManifestLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_InOut_ManifestLine_ID, Integer.valueOf(M_InOut_ManifestLine_ID));
	}

	/** Get Shipment Manifest Lines ID.
		@return Shipment Manifest Lines ID	  */
	public int getM_InOut_ManifestLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_InOut_ManifestLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set No of Pick Slips.
		@param NoOfPickSlips No of Pick Slips	  */
	public void setNoOfPickSlips (BigDecimal NoOfPickSlips)
	{
		throw new IllegalArgumentException ("NoOfPickSlips is virtual column");	}

	/** Get No of Pick Slips.
		@return No of Pick Slips	  */
	public BigDecimal getNoOfPickSlips () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NoOfPickSlips);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set No Packages.
		@param NoPackages 
		Number of packages shipped
	  */
	public void setNoPackages (int NoPackages)
	{
		throw new IllegalArgumentException ("NoPackages is virtual column");	}

	/** Get No Packages.
		@return Number of packages shipped
	  */
	public int getNoPackages () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_NoPackages);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Sequence.
		@param SeqNo 
		Method of ordering records; lowest number comes first
	  */
	public void setSeqNo (int SeqNo)
	{
		set_Value (COLUMNNAME_SeqNo, Integer.valueOf(SeqNo));
	}

	/** Get Sequence.
		@return Method of ordering records; lowest number comes first
	  */
	public int getSeqNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SeqNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Quantity.
		@param TotalQty 
		Total Quantity
	  */
	public void setTotalQty (BigDecimal TotalQty)
	{
		throw new IllegalArgumentException ("TotalQty is virtual column");	}

	/** Get Total Quantity.
		@return Total Quantity
	  */
	public BigDecimal getTotalQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}