/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for AD_ImportColumn
 *  @author Adempiere (generated) 
 *  @version 1.5.0
 */
public interface I_AD_ImportColumn 
{

    /** TableName=AD_ImportColumn */
    public static final String Table_Name = "AD_ImportColumn";

    /** AD_Table_ID=53369 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 4 - System 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(4);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_ImportColumn_ID */
    public static final String COLUMNNAME_AD_ImportColumn_ID = "AD_ImportColumn_ID";

	/** Set Import Column Mapping	  */
	public void setAD_ImportColumn_ID (int AD_ImportColumn_ID);

	/** Get Import Column Mapping	  */
	public int getAD_ImportColumn_ID();

    /** Column name AD_Import_ID */
    public static final String COLUMNNAME_AD_Import_ID = "AD_Import_ID";

	/** Set Data Import	  */
	public void setAD_Import_ID (int AD_Import_ID);

	/** Get Data Import	  */
	public int getAD_Import_ID();

	public I_AD_Import getAD_Import() throws RuntimeException;

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DefaultValue */
    public static final String COLUMNNAME_DefaultValue = "DefaultValue";

	/** Set Default Logic.
	  * Default value hierarchy, separated by ;

	  */
	public void setDefaultValue (String DefaultValue);

	/** Get Default Logic.
	  * Default value hierarchy, separated by ;

	  */
	public String getDefaultValue();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsGroupBy */
    public static final String COLUMNNAME_IsGroupBy = "IsGroupBy";

	/** Set Group by.
	  * After a group change, totals, etc. are printed
	  */
	public void setIsGroupBy (boolean IsGroupBy);

	/** Get Group by.
	  * After a group change, totals, etc. are printed
	  */
	public boolean isGroupBy();

    /** Column name IsKey */
    public static final String COLUMNNAME_IsKey = "IsKey";

	/** Set Key column.
	  * This column is the key in this table
	  */
	public void setIsKey (boolean IsKey);

	/** Get Key column.
	  * This column is the key in this table
	  */
	public boolean isKey();

    /** Column name IsOrderBy */
    public static final String COLUMNNAME_IsOrderBy = "IsOrderBy";

	/** Set Order by.
	  * Include in sort order
	  */
	public void setIsOrderBy (boolean IsOrderBy);

	/** Get Order by.
	  * Include in sort order
	  */
	public boolean isOrderBy();

    /** Column name SortNo */
    public static final String COLUMNNAME_SortNo = "SortNo";

	/** Set Record Sort No.
	  * Determines in what order the records are displayed
	  */
	public void setSortNo (int SortNo);

	/** Get Record Sort No.
	  * Determines in what order the records are displayed
	  */
	public int getSortNo();

    /** Column name Source_Column_ID */
    public static final String COLUMNNAME_Source_Column_ID = "Source_Column_ID";

	/** Set Source Column.
	  * Source column for import
	  */
	public void setSource_Column_ID (int Source_Column_ID);

	/** Get Source Column.
	  * Source column for import
	  */
	public int getSource_Column_ID();

	public I_AD_Column getSource_Column() throws RuntimeException;

    /** Column name Target_Column_ID */
    public static final String COLUMNNAME_Target_Column_ID = "Target_Column_ID";

	/** Set Target Column.
	  * Target column for import
	  */
	public void setTarget_Column_ID (int Target_Column_ID);

	/** Get Target Column.
	  * Target column for import
	  */
	public int getTarget_Column_ID();

	public I_AD_Column getTarget_Column() throws RuntimeException;

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
