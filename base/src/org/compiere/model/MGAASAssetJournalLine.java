/**
 * 
 */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Ashley G Ramdass
 * 
 */
public class MGAASAssetJournalLine extends X_GAAS_AssetJournalLine
{
    private static final long serialVersionUID = 6557266947499529626L;
    private static final CLogger logger = CLogger
            .getCLogger(MGAASAssetJournalLine.class);

    private boolean isNewLine = false;
    private MGAASAssetJournalLineDet[] details = null;

    public MGAASAssetJournalLine(Properties ctx, int GAAS_AssetJournalLine_ID,
            String trxName)
    {
        super(ctx, GAAS_AssetJournalLine_ID, trxName);
    }

    public MGAASAssetJournalLine(Properties ctx, ResultSet rs, String trxName)
    {
        super(ctx, rs, trxName);
    }

    public static MGAASAssetJournalLine getCreateLine(
            MGAASAssetJournal assetJournal, MInvoiceLine invoiceLine,
            String trxName)
    {
        String sql = "SELECT GAAS_AssetJournalLine_ID FROM GAAS_AssetJournalLine"
                + " WHERE GAAS_AssetJournal_ID=? AND C_InvoiceLine_ID=?";

        logger.fine("Asset Journal ID: " + assetJournal.getGAAS_AssetJournal_ID()
                + ", Invoice Line ID: " + invoiceLine.getC_InvoiceLine_ID());

        int assetJournalLineId = DB.getSQLValue(trxName, sql,
                assetJournal.getGAAS_AssetJournal_ID(), invoiceLine.getC_InvoiceLine_ID());

        if (assetJournalLineId > 0)
        {
            logger.fine("Asset Journal line already exists");
            MGAASAssetJournalLine assetJournalLine = new MGAASAssetJournalLine(
                    assetJournal.getCtx(), assetJournalLineId, trxName);
            assetJournalLine.setNewLine(false);
            return assetJournalLine;
        }

        MGAASAssetJournalLine assetJournalLine = new MGAASAssetJournalLine(
                assetJournal.getCtx(), 0, trxName);
        assetJournalLine.setClientOrg(assetJournal);
        assetJournalLine.setGAAS_AssetJournal_ID(assetJournal.getGAAS_AssetJournal_ID());
        assetJournalLine.setDescription(invoiceLine.getDescription());
        assetJournalLine.setCostAmt(invoiceLine.getLineNetAmt());
        assetJournalLine.setC_InvoiceLine_ID(invoiceLine.getC_InvoiceLine_ID());
        assetJournalLine.setNewLine(true);
        assetJournalLine.saveEx();
        logger.fine("Asset Journal line created");
        return assetJournalLine;
    }

    public MGAASAssetJournal getJournal()
    {
        if (getGAAS_AssetJournal_ID() <= 0)
        {
            return null;
        }

        return new MGAASAssetJournal(getCtx(), getGAAS_AssetJournal_ID(),
                get_TrxName());
    }

    /**
     * @return the isNewLine
     */
    public boolean isNewLine()
    {
        return isNewLine;
    }

    /**
     * @param isNewLine
     *            the isNewLine to set
     */
    protected void setNewLine(boolean isNewLine)
    {
        this.isNewLine = isNewLine;
    }

    
    private BigDecimal getLineTotal() throws Exception
    {
        MGAASAssetJournal journal = new MGAASAssetJournal(getCtx(), getGAAS_AssetJournal_ID(), get_TrxName());
        
        String column = null;
        String sql = null;
        
        if (journal.getEntryType().equals(MGAASAssetJournal.ENTRYTYPE_Disposal))
        {
            column = "DisposalAmt";
        }
        else if (journal.getEntryType().equals(MGAASAssetJournal.ENTRYTYPE_New))
        {
            column = "CostAmt";
        }
        else if (journal.getEntryType().equals(MGAASAssetJournal.ENTRYTYPE_Revaluation))
        {
            column = "CostAmt";
        }
        else if (journal.getEntryType().equals(
                MGAASAssetJournal.ENTRYTYPE_Depreciation))
        {
            sql = "SELECT COALESCE(SUM(dep.DepreciationExpenseAmt),0)"
                    + " FROM GAAS_AssetJournalLine ajl"
                    + " INNER JOIN GAAS_Depreciation_Expense dep"
                    + " ON ajl.GAAS_Depreciation_Expense_ID=dep.GAAS_Depreciation_Expense_ID"
                    + " WHERE ajl.GAAS_AssetJournal_ID=? AND ajl.IsActive='Y'";
        }
        else if (journal.getEntryType().equals(
                MGAASAssetJournal.ENTRYTYPE_Transfers))
        {
            sql = "SELECT SUM(COALESCE(dwf.AssetCost-AssetDep.DepAmt, 0))"
                    + " FROM ( "
                    + " SELECT COALESCE(SUM(dep.DepreciationExpenseAmt),0) as DepAmt,"
                    + " ajl.GAAS_AssetJournalLine_ID, ajl.A_Asset_ID"
                    + " FROM GAAS_AssetJournalLine ajl"
                    + " LEFT JOIN GAAS_Depreciation_Expense dep"
                    + " ON (ajl.A_Asset_ID=dep.A_Asset_ID AND dep.Processed='Y')"
                    + " WHERE ajl.GAAS_AssetJournal_ID=? AND ajl.IsActive='Y'"
                    + " GROUP BY ajl.GAAS_AssetJournalLine_ID, ajl.A_Asset_ID) AS AssetDep"
                    + " INNER JOIN GAAS_Depreciation_Workfile dwf ON dwf.A_Asset_ID=AssetDep.A_Asset_ID";
        }
        else
        {
            throw new Exception("Entry type not supported: " + journal.getEntryType());
        }
        
        if (column != null)
        {
            sql = "SELECT COALESCE(SUM(ajl." + column + "),0) "
                    + " FROM GAAS_AssetJournalLine ajl"
                    + " WHERE ajl.GAAS_AssetJournal_ID=? AND ajl.IsActive='Y'";
        }

        BigDecimal sum = Env.ZERO;

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(1, getGAAS_AssetJournal_ID());
            rs = pstmt.executeQuery();

            if (rs.next())
            {
                sum = rs.getBigDecimal(1);
            }
        }
        finally
        {
            DB.close(rs, pstmt);
        }
        
        if (sum == null)
        {
            sum = Env.ZERO;
        }
        
        return sum;
    }
    
    /**
     * Update the Asset Journal Header Statement Difference and Ending Balance
     * 
     * @return true if success
     */
    private boolean updateHeader()
    {
        BigDecimal sum = Env.ZERO;
        
        try
        {
            sum = getLineTotal();
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, "Could not get line total", ex);
            return false;
        }
        
        String sql = "UPDATE GAAS_AssetJournal "
                + "SET EndingBalance = BeginningBalance + ?, "
                + "StatementDifference=? WHERE GAAS_AssetJournal_ID=?";
        
        Object[] params = new Object[3];
        params[0] = sum;
        params[1] = sum;
        params[2] = getGAAS_AssetJournal_ID();

        int no = DB.executeUpdate(sql, params, false, get_TrxName());
        if (no != 1)
        {
            logger.warning("Incorrect number of records updated: " + no);
        }

        return true;
    }

    public boolean hasDetails()
    {
        return (getDetails(false).length > 0);
    }

    public BigDecimal getQty()
    {
        if (getC_InvoiceLine_ID() <= 0)
        {
            return Env.ZERO;
        }

        String sql = "SELECT QtyInvoiced FROM C_InvoiceLine "
                + "WHERE C_InvoiceLine_ID=?";

        return DB.getSQLValueBD(null, sql, getC_InvoiceLine_ID());
    }

    public MGAASAssetJournalLineDet[] getDetails(boolean requery)
    {
        if (details != null && !requery)
        {
            return details;
        }

        ArrayList<MGAASAssetJournalLineDet> list = new ArrayList<MGAASAssetJournalLineDet>();
        String sql = "SELECT * FROM GAAS_AssetJournalLine_Det WHERE GAAS_AssetJournalLine_ID=?";
        PreparedStatement pstmt = null;
        try
        {
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(1, getGAAS_AssetJournalLine_ID());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                list.add(new MGAASAssetJournalLineDet(getCtx(), rs,
                        get_TrxName()));
            }
            rs.close();
            pstmt.close();
            pstmt = null;
        }
        catch (Exception e)
        {
            log.log(Level.SEVERE, sql, e);
        }
        try
        {
            if (pstmt != null)
                pstmt.close();
            pstmt = null;
        }
        catch (Exception e)
        {
            pstmt = null;
        }

        details = new MGAASAssetJournalLineDet[list.size()];
        list.toArray(details);
        return details;

    } // getDetails

    public void setInitialCost()
    {
        if (getA_Asset_ID() > 0)
        {
            MGAASDepreciationWorkfile workfile = MGAASDepreciationWorkfile.get(
                    getCtx(), getA_Asset_ID(), get_TrxName());
            setInitialCost(workfile.getAssetCost());
        }
    }

    public void setAccumulatedDepreciationAmt()
    {
        if (getA_Asset_ID() > 0)
        {
            MGAASDepreciationWorkfile workfile = MGAASDepreciationWorkfile.get(
                    getCtx(), getA_Asset_ID(), get_TrxName());

            String sql = "SELECT NVL(SUM(DepreciationExpenseAmt),0)"
                    + " FROM GAAS_Depreciation_Expense WHERE A_Asset_ID=?"
                    + " AND Processed='Y'";

            BigDecimal amount = DB.getSQLValueBD(get_TrxName(), sql,
                    getA_Asset_ID());
            setAccumulatedDepreciationAmt(amount);
        }
    }

    /**
     * Before Save
     * 
     * @param newRecord
     * @return true/false
     */
    @Override
    protected boolean beforeSave(boolean newRecord)
    {
        if (is_ValueChanged("C_InvoiceLine_ID"))
        {
            MInvoiceLine invoiceLine = new MInvoiceLine(getCtx(),
                    getC_InvoiceLine_ID(), get_TrxName());
            setCostAmt(invoiceLine.getLineNetAmt());
        }

        MGAASAssetJournal journal = getJournal();

        if (journal == null)
        {
            log.saveError("Error", "No Journal");
            return false;
        }

        if (is_ValueChanged("A_Asset_ID") && getA_Asset_ID() > 0)
        {
            MAsset asset = new MAsset(getCtx(), getA_Asset_ID(), get_TrxName());
            if (MGAASAssetJournal.ENTRYTYPE_Transfers.equals(journal
                    .getEntryType()))
            {
                setOld_Asset_Group_ID(asset.getA_Asset_Group_ID());
            }

            if (MGAASAssetJournal.ENTRYTYPE_Disposal.equals(journal
                    .getEntryType()))
            {
                setInitialCost();
                setAccumulatedDepreciationAmt();
            }
            
            if (MGAASAssetJournal.ENTRYTYPE_Revaluation.equals(journal
                    .getEntryType()))
            {
                setInitialCost();
            }
        }

        if (getOld_Asset_Group_ID() != 0
                && getOld_Asset_Group_ID() == getA_Asset_Group_ID())
        {
            log.saveError("Error", "Same group");
            return false;
        }

        return super.beforeSave(newRecord);
    }

    /**
     * After Save
     * 
     * @param newRecord
     * @param success
     * @return success
     */
    @Override
    protected boolean afterSave(boolean newRecord, boolean success)
    {
        if (!success)
            return success;
        return updateHeader();
    } // afterSave
    
    protected boolean afterDelete(boolean success)
    {
        if (!success)
            return success;
        return updateHeader();
    }
}
