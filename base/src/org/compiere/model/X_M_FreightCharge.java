/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for M_FreightCharge
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_M_FreightCharge extends PO implements I_M_FreightCharge, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_M_FreightCharge (Properties ctx, int M_FreightCharge_ID, String trxName)
    {
      super (ctx, M_FreightCharge_ID, trxName);
      /** if (M_FreightCharge_ID == 0)
        {
			setC_Currency_ID (0);
			setIsTaxIncluded (false);
// N
			setM_FreightCategory_ID (0);
			setM_FreightCharge_ID (0);
			setM_FreightRegion_ID (0);
			setM_Shipper_ID (0);
			setTo_Region_ID (0);
			setValidFrom (new Timestamp( System.currentTimeMillis() ));
        } */
    }

    /** Load Constructor */
    public X_M_FreightCharge (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_M_FreightCharge[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Additional Package Charge.
		@param AdditionalPackageCharge 
		Charge for each additional package.
	  */
	public void setAdditionalPackageCharge (BigDecimal AdditionalPackageCharge)
	{
		set_Value (COLUMNNAME_AdditionalPackageCharge, AdditionalPackageCharge);
	}

	/** Get Additional Package Charge.
		@return Charge for each additional package.
	  */
	public BigDecimal getAdditionalPackageCharge () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AdditionalPackageCharge);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Base Charge.
		@param BaseCharge 
		Charge for initial package
	  */
	public void setBaseCharge (BigDecimal BaseCharge)
	{
		set_Value (COLUMNNAME_BaseCharge, BaseCharge);
	}

	/** Get Base Charge.
		@return Charge for initial package
	  */
	public BigDecimal getBaseCharge () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BaseCharge);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_Currency getC_Currency() throws RuntimeException
    {
		return (I_C_Currency)MTable.get(getCtx(), I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cubic Factor.
		@param CubicFactor 
		Factor to multiply weight by when calculating charges
	  */
	public void setCubicFactor (BigDecimal CubicFactor)
	{
		set_Value (COLUMNNAME_CubicFactor, CubicFactor);
	}

	/** Get Cubic Factor.
		@return Factor to multiply weight by when calculating charges
	  */
	public BigDecimal getCubicFactor () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CubicFactor);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Price includes Tax.
		@param IsTaxIncluded 
		Tax is included in the price 
	  */
	public void setIsTaxIncluded (boolean IsTaxIncluded)
	{
		set_Value (COLUMNNAME_IsTaxIncluded, Boolean.valueOf(IsTaxIncluded));
	}

	/** Get Price includes Tax.
		@return Tax is included in the price 
	  */
	public boolean isTaxIncluded () 
	{
		Object oo = get_Value(COLUMNNAME_IsTaxIncluded);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public I_M_FreightCategory getM_FreightCategory() throws RuntimeException
    {
		return (I_M_FreightCategory)MTable.get(getCtx(), I_M_FreightCategory.Table_Name)
			.getPO(getM_FreightCategory_ID(), get_TrxName());	}

	/** Set Freight Category.
		@param M_FreightCategory_ID 
		Category of the Freight
	  */
	public void setM_FreightCategory_ID (int M_FreightCategory_ID)
	{
		if (M_FreightCategory_ID < 1) 
			set_Value (COLUMNNAME_M_FreightCategory_ID, null);
		else 
			set_Value (COLUMNNAME_M_FreightCategory_ID, Integer.valueOf(M_FreightCategory_ID));
	}

	/** Get Freight Category.
		@return Category of the Freight
	  */
	public int getM_FreightCategory_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_FreightCategory_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Freight Charge.
		@param M_FreightCharge_ID Freight Charge	  */
	public void setM_FreightCharge_ID (int M_FreightCharge_ID)
	{
		if (M_FreightCharge_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_FreightCharge_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_FreightCharge_ID, Integer.valueOf(M_FreightCharge_ID));
	}

	/** Get Freight Charge.
		@return Freight Charge	  */
	public int getM_FreightCharge_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_FreightCharge_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_FreightRegion getM_FreightRegion() throws RuntimeException
    {
		return (I_M_FreightRegion)MTable.get(getCtx(), I_M_FreightRegion.Table_Name)
			.getPO(getM_FreightRegion_ID(), get_TrxName());	}

	/** Set Freight Region.
		@param M_FreightRegion_ID Freight Region	  */
	public void setM_FreightRegion_ID (int M_FreightRegion_ID)
	{
		if (M_FreightRegion_ID < 1) 
			set_Value (COLUMNNAME_M_FreightRegion_ID, null);
		else 
			set_Value (COLUMNNAME_M_FreightRegion_ID, Integer.valueOf(M_FreightRegion_ID));
	}

	/** Get Freight Region.
		@return Freight Region	  */
	public int getM_FreightRegion_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_FreightRegion_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Minimum Charge.
		@param MinimumCharge 
		The minimum charge amount
	  */
	public void setMinimumCharge (BigDecimal MinimumCharge)
	{
		set_Value (COLUMNNAME_MinimumCharge, MinimumCharge);
	}

	/** Get Minimum Charge.
		@return The minimum charge amount
	  */
	public BigDecimal getMinimumCharge () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_MinimumCharge);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_M_Shipper getM_Shipper() throws RuntimeException
    {
		return (I_M_Shipper)MTable.get(getCtx(), I_M_Shipper.Table_Name)
			.getPO(getM_Shipper_ID(), get_TrxName());	}

	/** Set Shipper.
		@param M_Shipper_ID 
		Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID)
	{
		if (M_Shipper_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Shipper_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Shipper_ID, Integer.valueOf(M_Shipper_ID));
	}

	/** Get Shipper.
		@return Method or manner of product delivery
	  */
	public int getM_Shipper_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Shipper_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getM_Shipper_ID()));
    }

	/** Set Surcharge %.
		@param Surcharge 
		Percentage charge on total
	  */
	public void setSurcharge (BigDecimal Surcharge)
	{
		set_Value (COLUMNNAME_Surcharge, Surcharge);
	}

	/** Get Surcharge %.
		@return Percentage charge on total
	  */
	public BigDecimal getSurcharge () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Surcharge);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_M_FreightRegion getTo_Region() throws RuntimeException
    {
		return (I_M_FreightRegion)MTable.get(getCtx(), I_M_FreightRegion.Table_Name)
			.getPO(getTo_Region_ID(), get_TrxName());	}

	/** Set To.
		@param To_Region_ID 
		Receiving Region
	  */
	public void setTo_Region_ID (int To_Region_ID)
	{
		if (To_Region_ID < 1) 
			set_Value (COLUMNNAME_To_Region_ID, null);
		else 
			set_Value (COLUMNNAME_To_Region_ID, Integer.valueOf(To_Region_ID));
	}

	/** Get To.
		@return Receiving Region
	  */
	public int getTo_Region_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_To_Region_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Valid from.
		@param ValidFrom 
		Valid from including this date (first day)
	  */
	public void setValidFrom (Timestamp ValidFrom)
	{
		set_Value (COLUMNNAME_ValidFrom, ValidFrom);
	}

	/** Get Valid from.
		@return Valid from including this date (first day)
	  */
	public Timestamp getValidFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ValidFrom);
	}

	/** Set Weight Multiple.
		@param WeightMultiple 
		Weight charged in multiples of this number of kilograms
	  */
	public void setWeightMultiple (BigDecimal WeightMultiple)
	{
		set_Value (COLUMNNAME_WeightMultiple, WeightMultiple);
	}

	/** Get Weight Multiple.
		@return Weight charged in multiples of this number of kilograms
	  */
	public BigDecimal getWeightMultiple () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_WeightMultiple);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Weight Multiple Rate.
		@param WeightMultipleRate 
		Additional amount charged per weight multiple
	  */
	public void setWeightMultipleRate (BigDecimal WeightMultipleRate)
	{
		set_Value (COLUMNNAME_WeightMultipleRate, WeightMultipleRate);
	}

	/** Get Weight Multiple Rate.
		@return Additional amount charged per weight multiple
	  */
	public BigDecimal getWeightMultipleRate () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_WeightMultipleRate);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Weight Unit Rate.
		@param WeightUnitRate 
		Additional amount charged per weight unit
	  */
	public void setWeightUnitRate (BigDecimal WeightUnitRate)
	{
		set_Value (COLUMNNAME_WeightUnitRate, WeightUnitRate);
	}

	/** Get Weight Unit Rate.
		@return Additional amount charged per weight unit
	  */
	public BigDecimal getWeightUnitRate () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_WeightUnitRate);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}