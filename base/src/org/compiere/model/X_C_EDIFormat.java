/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for C_EDIFormat
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_C_EDIFormat extends PO implements I_C_EDIFormat, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_C_EDIFormat (Properties ctx, int C_EDIFormat_ID, String trxName)
    {
      super (ctx, C_EDIFormat_ID, trxName);
      /** if (C_EDIFormat_ID == 0)
        {
			setAD_Table_ID (0);
			setC_EDI_DocType_ID (0);
			setC_EDIFormat_ID (0);
			setName (null);
			setValue (null);
        } */
    }

    /** Load Constructor */
    public X_C_EDIFormat (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_EDIFormat[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_AD_Table getAD_Table() throws RuntimeException
    {
		return (I_AD_Table)MTable.get(getCtx(), I_AD_Table.Table_Name)
			.getPO(getAD_Table_ID(), get_TrxName());	}

	/** Set Table.
		@param AD_Table_ID 
		Database Table information
	  */
	public void setAD_Table_ID (int AD_Table_ID)
	{
		if (AD_Table_ID < 1) 
			set_Value (COLUMNNAME_AD_Table_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Table_ID, Integer.valueOf(AD_Table_ID));
	}

	/** Get Table.
		@return Database Table information
	  */
	public int getAD_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_EDI_DocType getC_EDI_DocType() throws RuntimeException
    {
		return (I_C_EDI_DocType)MTable.get(getCtx(), I_C_EDI_DocType.Table_Name)
			.getPO(getC_EDI_DocType_ID(), get_TrxName());	}

	/** Set EDI Doc Type.
		@param C_EDI_DocType_ID EDI Doc Type	  */
	public void setC_EDI_DocType_ID (int C_EDI_DocType_ID)
	{
		if (C_EDI_DocType_ID < 1) 
			set_Value (COLUMNNAME_C_EDI_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_EDI_DocType_ID, Integer.valueOf(C_EDI_DocType_ID));
	}

	/** Get EDI Doc Type.
		@return EDI Doc Type	  */
	public int getC_EDI_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDI_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDI Format.
		@param C_EDIFormat_ID EDI Format	  */
	public void setC_EDIFormat_ID (int C_EDIFormat_ID)
	{
		if (C_EDIFormat_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDIFormat_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDIFormat_ID, Integer.valueOf(C_EDIFormat_ID));
	}

	/** Get EDI Format.
		@return EDI Format	  */
	public int getC_EDIFormat_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDIFormat_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Copy From.
		@param CopyFrom 
		Copy From Record
	  */
	public void setCopyFrom (String CopyFrom)
	{
		set_Value (COLUMNNAME_CopyFrom, CopyFrom);
	}

	/** Get Copy From.
		@return Copy From Record
	  */
	public String getCopyFrom () 
	{
		return (String)get_Value(COLUMNNAME_CopyFrom);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set FieldSeparator.
		@param FieldSeparator FieldSeparator	  */
	public void setFieldSeparator (String FieldSeparator)
	{
		set_Value (COLUMNNAME_FieldSeparator, FieldSeparator);
	}

	/** Get FieldSeparator.
		@return FieldSeparator	  */
	public String getFieldSeparator () 
	{
		return (String)get_Value(COLUMNNAME_FieldSeparator);
	}

	/** Set Functional Identifier.
		@param FunctionalIdentifier Functional Identifier	  */
	public void setFunctionalIdentifier (String FunctionalIdentifier)
	{
		set_Value (COLUMNNAME_FunctionalIdentifier, FunctionalIdentifier);
	}

	/** Get Functional Identifier.
		@return Functional Identifier	  */
	public String getFunctionalIdentifier () 
	{
		return (String)get_Value(COLUMNNAME_FunctionalIdentifier);
	}

	public I_AD_Table getHeader_Table() throws RuntimeException
    {
		return (I_AD_Table)MTable.get(getCtx(), I_AD_Table.Table_Name)
			.getPO(getHeader_Table_ID(), get_TrxName());	}

	/** Set Header Table.
		@param Header_Table_ID Header Table	  */
	public void setHeader_Table_ID (int Header_Table_ID)
	{
		if (Header_Table_ID < 1) 
			set_Value (COLUMNNAME_Header_Table_ID, null);
		else 
			set_Value (COLUMNNAME_Header_Table_ID, Integer.valueOf(Header_Table_ID));
	}

	/** Get Header Table.
		@return Header Table	  */
	public int getHeader_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Header_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Comment/Help.
		@param Help 
		Comment or Hint
	  */
	public void setHelp (String Help)
	{
		set_Value (COLUMNNAME_Help, Help);
	}

	/** Get Comment/Help.
		@return Comment or Hint
	  */
	public String getHelp () 
	{
		return (String)get_Value(COLUMNNAME_Help);
	}

	/** Set IsAcknowledgement.
		@param IsAcknowledgement IsAcknowledgement	  */
	public void setIsAcknowledgement (boolean IsAcknowledgement)
	{
		set_Value (COLUMNNAME_IsAcknowledgement, Boolean.valueOf(IsAcknowledgement));
	}

	/** Get IsAcknowledgement.
		@return IsAcknowledgement	  */
	public boolean isAcknowledgement () 
	{
		Object oo = get_Value(COLUMNNAME_IsAcknowledgement);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set View.
		@param IsView 
		This is a view
	  */
	public void setIsView (boolean IsView)
	{
		throw new IllegalArgumentException ("IsView is virtual column");	}

	/** Get View.
		@return This is a view
	  */
	public boolean isView () 
	{
		Object oo = get_Value(COLUMNNAME_IsView);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Line Separator.
		@param LineSeparator Line Separator	  */
	public void setLineSeparator (String LineSeparator)
	{
		set_Value (COLUMNNAME_LineSeparator, LineSeparator);
	}

	/** Get Line Separator.
		@return Line Separator	  */
	public String getLineSeparator () 
	{
		return (String)get_Value(COLUMNNAME_LineSeparator);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	public I_AD_Table getReal_Table() throws RuntimeException
    {
		return (I_AD_Table)MTable.get(getCtx(), I_AD_Table.Table_Name)
			.getPO(getReal_Table_ID(), get_TrxName());	}

	/** Set Table.
		@param Real_Table_ID Table	  */
	public void setReal_Table_ID (int Real_Table_ID)
	{
		if (Real_Table_ID < 1) 
			set_Value (COLUMNNAME_Real_Table_ID, null);
		else 
			set_Value (COLUMNNAME_Real_Table_ID, Integer.valueOf(Real_Table_ID));
	}

	/** Get Table.
		@return Table	  */
	public int getReal_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Real_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set SegmentSeparator.
		@param SegmentSeparator SegmentSeparator	  */
	public void setSegmentSeparator (String SegmentSeparator)
	{
		set_Value (COLUMNNAME_SegmentSeparator, SegmentSeparator);
	}

	/** Get SegmentSeparator.
		@return SegmentSeparator	  */
	public String getSegmentSeparator () 
	{
		return (String)get_Value(COLUMNNAME_SegmentSeparator);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getValue());
    }

	/** Set Sql WHERE.
		@param WhereClause 
		Fully qualified SQL WHERE clause
	  */
	public void setWhereClause (String WhereClause)
	{
		set_Value (COLUMNNAME_WhereClause, WhereClause);
	}

	/** Get Sql WHERE.
		@return Fully qualified SQL WHERE clause
	  */
	public String getWhereClause () 
	{
		return (String)get_Value(COLUMNNAME_WhereClause);
	}
}