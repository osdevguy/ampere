/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for X_Conv_Currency
 *  @author Adempiere (generated) 
 *  @version 1.03 - $Id$ */
public class X_X_Conv_Currency extends PO implements I_X_Conv_Currency, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20150717L;

    /** Standard Constructor */
    public X_X_Conv_Currency (Properties ctx, int X_Conv_Currency_ID, String trxName)
    {
      super (ctx, X_Conv_Currency_ID, trxName);
      /** if (X_Conv_Currency_ID == 0)
        {
			setC_ConversionType_ID (0);
			setC_Currency_ID (0);
			setprimary_currency_rate (Env.ZERO);
// 1.00000
			setValidFrom (new Timestamp( System.currentTimeMillis() ));
			setX_Conv_Currency_ID (0);
        } */
    }

    /** Load Constructor */
    public X_X_Conv_Currency (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_X_Conv_Currency[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_ConversionType getC_ConversionType() throws RuntimeException
    {
		return (I_C_ConversionType)MTable.get(getCtx(), I_C_ConversionType.Table_Name)
			.getPO(getC_ConversionType_ID(), get_TrxName());	}

	/** Set Conv Rate Type.
		@param C_ConversionType_ID 
		Currency Conversion Rate Type
	  */
	public void setC_ConversionType_ID (int C_ConversionType_ID)
	{
		if (C_ConversionType_ID < 1) 
			set_Value (COLUMNNAME_C_ConversionType_ID, null);
		else 
			set_Value (COLUMNNAME_C_ConversionType_ID, Integer.valueOf(C_ConversionType_ID));
	}

	/** Get Conv Rate Type.
		@return Currency Conversion Rate Type
	  */
	public int getC_ConversionType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_ConversionType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Currency getC_Currency() throws RuntimeException
    {
		return (I_C_Currency)MTable.get(getCtx(), I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Primary Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Primary Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Create Rate.
		@param Create_Rate Create Rate	  */
	public void setCreate_Rate (String Create_Rate)
	{
		set_Value (COLUMNNAME_Create_Rate, Create_Rate);
	}

	/** Get Create Rate.
		@return Create Rate	  */
	public String getCreate_Rate () 
	{
		return (String)get_Value(COLUMNNAME_Create_Rate);
	}

	/** Set Primary Currency Rate.
		@param primary_currency_rate 
		Primary Currency Rate
	  */
	public void setprimary_currency_rate (BigDecimal primary_currency_rate)
	{
		set_Value (COLUMNNAME_primary_currency_rate, primary_currency_rate);
	}

	/** Get Primary Currency Rate.
		@return Primary Currency Rate
	  */
	public BigDecimal getprimary_currency_rate () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_primary_currency_rate);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Valid From.
		@param ValidFrom 
		Valid from including this date (first day)
	  */
	public void setValidFrom (Timestamp ValidFrom)
	{
		set_Value (COLUMNNAME_ValidFrom, ValidFrom);
	}

	/** Get Valid From.
		@return Valid from including this date (first day)
	  */
	public Timestamp getValidFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ValidFrom);
	}

	/** Set Conversion Currency.
		@param X_Conv_Currency_ID Conversion Currency	  */
	public void setX_Conv_Currency_ID (int X_Conv_Currency_ID)
	{
		if (X_Conv_Currency_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_X_Conv_Currency_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_X_Conv_Currency_ID, Integer.valueOf(X_Conv_Currency_ID));
	}

	/** Get Conversion Currency.
		@return Conversion Currency	  */
	public  int getX_Conv_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_X_Conv_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}