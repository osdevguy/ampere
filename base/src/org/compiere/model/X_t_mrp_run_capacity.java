/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for t_mrp_run_capacity
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_t_mrp_run_capacity extends PO implements I_t_mrp_run_capacity, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_t_mrp_run_capacity (Properties ctx, int t_mrp_run_capacity_ID, String trxName)
    {
      super (ctx, t_mrp_run_capacity_ID, trxName);
      /** if (t_mrp_run_capacity_ID == 0)
        {
			setMRP_Run_ID (0);
			setM_WorkCentre_ID (0);
			setT_MRP_Run_Capacity_ID (0);
        } */
    }

    /** Load Constructor */
    public X_t_mrp_run_capacity (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_t_mrp_run_capacity[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Date Start.
		@param DateStart 
		Date Start for this Order
	  */
	public void setDateStart (Timestamp DateStart)
	{
		set_Value (COLUMNNAME_DateStart, DateStart);
	}

	/** Get Date Start.
		@return Date Start for this Order
	  */
	public Timestamp getDateStart () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateStart);
	}

	public I_MRP_Run getMRP_Run() throws RuntimeException
    {
		return (I_MRP_Run)MTable.get(getCtx(), I_MRP_Run.Table_Name)
			.getPO(getMRP_Run_ID(), get_TrxName());	}

	/** Set MRP_Run ID.
		@param MRP_Run_ID MRP_Run ID	  */
	public void setMRP_Run_ID (int MRP_Run_ID)
	{
		if (MRP_Run_ID < 1) 
			set_Value (COLUMNNAME_MRP_Run_ID, null);
		else 
			set_Value (COLUMNNAME_MRP_Run_ID, Integer.valueOf(MRP_Run_ID));
	}

	/** Get MRP_Run ID.
		@return MRP_Run ID	  */
	public int getMRP_Run_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_MRP_Run_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_WorkCentre getM_WorkCentre() throws RuntimeException
    {
		return (I_M_WorkCentre)MTable.get(getCtx(), I_M_WorkCentre.Table_Name)
			.getPO(getM_WorkCentre_ID(), get_TrxName());	}

	/** Set Work Centre.
		@param M_WorkCentre_ID Work Centre	  */
	public void setM_WorkCentre_ID (int M_WorkCentre_ID)
	{
		if (M_WorkCentre_ID < 1) 
			set_Value (COLUMNNAME_M_WorkCentre_ID, null);
		else 
			set_Value (COLUMNNAME_M_WorkCentre_ID, Integer.valueOf(M_WorkCentre_ID));
	}

	/** Get Work Centre.
		@return Work Centre	  */
	public int getM_WorkCentre_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_WorkCentre_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set T_MRP_Run_Capacity ID.
		@param T_MRP_Run_Capacity_ID T_MRP_Run_Capacity ID	  */
	public void setT_MRP_Run_Capacity_ID (int T_MRP_Run_Capacity_ID)
	{
		if (T_MRP_Run_Capacity_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_T_MRP_Run_Capacity_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_T_MRP_Run_Capacity_ID, Integer.valueOf(T_MRP_Run_Capacity_ID));
	}

	/** Get T_MRP_Run_Capacity ID.
		@return T_MRP_Run_Capacity ID	  */
	public int getT_MRP_Run_Capacity_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_T_MRP_Run_Capacity_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total WC Capacity.
		@param TotalCapacity 
		Total Work Centre capacity
	  */
	public void setTotalCapacity (BigDecimal TotalCapacity)
	{
		set_Value (COLUMNNAME_TotalCapacity, TotalCapacity);
	}

	/** Get Total WC Capacity.
		@return Total Work Centre capacity
	  */
	public BigDecimal getTotalCapacity () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalCapacity);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Mfg Order.
		@param TotalMOP Total Mfg Order	  */
	public void setTotalMOP (BigDecimal TotalMOP)
	{
		set_Value (COLUMNNAME_TotalMOP, TotalMOP);
	}

	/** Get Total Mfg Order.
		@return Total Mfg Order	  */
	public BigDecimal getTotalMOP () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalMOP);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Planned Mfg Order.
		@param TotalMPO Total Planned Mfg Order	  */
	public void setTotalMPO (BigDecimal TotalMPO)
	{
		set_Value (COLUMNNAME_TotalMPO, TotalMPO);
	}

	/** Get Total Planned Mfg Order.
		@return Total Planned Mfg Order	  */
	public BigDecimal getTotalMPO () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalMPO);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Production in Queue.
		@param TotalProduction Total Production in Queue	  */
	public void setTotalProduction (BigDecimal TotalProduction)
	{
		set_Value (COLUMNNAME_TotalProduction, TotalProduction);
	}

	/** Get Total Production in Queue.
		@return Total Production in Queue	  */
	public BigDecimal getTotalProduction () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalProduction);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}