/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for AD_ImportValidation
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_AD_ImportValidation extends PO implements I_AD_ImportValidation, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_AD_ImportValidation (Properties ctx, int AD_ImportValidation_ID, String trxName)
    {
      super (ctx, AD_ImportValidation_ID, trxName);
      /** if (AD_ImportValidation_ID == 0)
        {
			setAD_Import_ID (0);
			setAD_ImportValidation_ID (0);
			setName (null);
			setSeqNo (0);
// @SQL=SELECT COALESCE(MAX(SeqNo),0)+10 AS DefaultValue FROM AD_ImportValidation WHERE AD_Import_ID=@AD_Import_ID@
			setSQLStatement (null);
        } */
    }

    /** Load Constructor */
    public X_AD_ImportValidation (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 4 - System 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_AD_ImportValidation[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_AD_Import getAD_Import() throws RuntimeException
    {
		return (I_AD_Import)MTable.get(getCtx(), I_AD_Import.Table_Name)
			.getPO(getAD_Import_ID(), get_TrxName());	}

	/** Set Data Import.
		@param AD_Import_ID Data Import	  */
	public void setAD_Import_ID (int AD_Import_ID)
	{
		if (AD_Import_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_Import_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Import_ID, Integer.valueOf(AD_Import_ID));
	}

	/** Get Data Import.
		@return Data Import	  */
	public int getAD_Import_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Import_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Import Validation.
		@param AD_ImportValidation_ID Import Validation	  */
	public void setAD_ImportValidation_ID (int AD_ImportValidation_ID)
	{
		if (AD_ImportValidation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_ImportValidation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_ImportValidation_ID, Integer.valueOf(AD_ImportValidation_ID));
	}

	/** Get Import Validation.
		@return Import Validation	  */
	public int getAD_ImportValidation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_ImportValidation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Sequence.
		@param SeqNo 
		Method of ordering records; lowest number comes first
	  */
	public void setSeqNo (int SeqNo)
	{
		set_Value (COLUMNNAME_SeqNo, Integer.valueOf(SeqNo));
	}

	/** Get Sequence.
		@return Method of ordering records; lowest number comes first
	  */
	public int getSeqNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SeqNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set SQLStatement.
		@param SQLStatement SQLStatement	  */
	public void setSQLStatement (String SQLStatement)
	{
		set_Value (COLUMNNAME_SQLStatement, SQLStatement);
	}

	/** Get SQLStatement.
		@return SQLStatement	  */
	public String getSQLStatement () 
	{
		return (String)get_Value(COLUMNNAME_SQLStatement);
	}
}