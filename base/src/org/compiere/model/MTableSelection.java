/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Victor Perez	                                      * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Victor Perez (victor.perez@e-evolution.com	 )                *
 *                                                                    *
 * Sponsors:                                                          *
 *  - e-Evolution (http://www.e-evolution.com/)                       *
 **********************************************************************/
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.Properties;

import org.compiere.util.CLogger;

/**
 * Class Model for Table Selection based on Smart Browser
 * @author victor.perez@e-evoluton.com, e-Evolution
 *
 */
public class MTableSelection extends X_AD_TableSelection 
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 3097647999717804581L;
	/**	Logger	*/
	private static CLogger	s_log = CLogger.getCLogger (MTableSelection.class);
	private MTable m_table = null;
	
	/**************************************************************************
	 * 	Asset Constructor
	 *	@param ctx context
	 *	@param AD_TableSelection_ID  InOutBound ID
	 *	@param trxName transaction name 
	 */
	public MTableSelection (Properties ctx, int AD_TableSelection_ID, String trxName)
	{
		super (ctx, AD_TableSelection_ID, trxName);
		if (AD_TableSelection_ID == 0)
		{
		}
	}

	/**
	 *	@param ctx context
	 *	@param AD_TableSelection_ID Cahs Flow ID
	 */
	public MTableSelection (Properties ctx, int AD_TableSelection_ID)
	{
		this (ctx, AD_TableSelection_ID, null);
	}

	/**
	 *  Load Constructor
	 *  @param ctx context
	 *  @param rs result set record
	 *	@param trxName transaction
	 */
	public MTableSelection (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	

	/**
	 * 	String representation
	 *	@return info
	 */
	@Override
	public String toString ()
	{
		StringBuffer sb = new StringBuffer (" MTableSelection[")
			.append (get_ID ())
			.append ("-")
			.append (getName())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * get Criteria columns
	 * @return collection columns
	 */
	public Collection <MTableSelectionColumn> getCriteriaColumns()
	{
				
		String whereClause = org.compiere.model.MTableSelection.COLUMNNAME_AD_TableSelection_ID + "=? AND "
							+ MTableSelectionColumn.COLUMNNAME_IsDisplayed 	+ "=? AND "
							+ MTableSelectionColumn.COLUMNNAME_IsQueryCriteria 	+ "=?";
						
		
		return new Query(getCtx(),MTableSelectionColumn.Table_Name, whereClause, get_TrxName())
		.setParameters(new Object[]{get_ID(),"Y","Y"})
		.setOnlyActiveRecords(true)
		.setOrderBy(MTableSelectionColumn.COLUMNNAME_SeqNo)
		.list();		
	}
	
	/**
	 * get columns
	 * @return Collection columns
	 */
	public Collection <MTableSelectionColumn> getColumns()
	{
				
		String whereClause = MTableSelection.COLUMNNAME_AD_TableSelection_ID + "=? AND "
							+ MTableSelectionColumn.COLUMNNAME_IsDisplayed 	+ "=? ";		
		return new Query(getCtx(),MTableSelectionColumn.Table_Name, whereClause, get_TrxName())
		.setParameters(new Object[]{get_ID(),"Y"})
		.setOnlyActiveRecords(true)
		.setOrderBy(MTableSelectionColumn.COLUMNNAME_SeqNo)
		.list();		
	}
	
	/**
	 * get column using name
	 * @param column name
	 * @return field 
	 */
	public MTableSelectionColumn getColumn(String name)
	{
		for (MTableSelectionColumn field : getColumns())
		{
			if(field.getName().equals(name))
			{
				return field;
			}
		}
		return null;
	}
	
	/**
	 * get MTable
	 */
	public MTable getAD_Table()
	{
		if(m_table == null)
		{
			m_table = new MTable(getCtx(), getAD_Table_ID(), get_TrxName());
		}
		return m_table;
	}
}	
