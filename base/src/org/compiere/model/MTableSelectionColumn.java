/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Victor Perez	                                      * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Victor Perez (victor.perez@e-evolution.com	 )                *
 *                                                                    *
 * Sponsors:                                                          *
 *  - e-Evolution (http://www.e-evolution.com/)                       *
 **********************************************************************/
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.CLogger;

/**
 * Class Model for Table Selection Column based on Browse Field
 * 
 * @author victor.perez@e-evoluton.com, e-Evolution
 *
 */
public class MTableSelectionColumn extends X_AD_TableSelection_Column
{

	/**
	 * get Selection Column based on Column
	 * @param Table Selection
	 * @param column
	 * @return MTableSelectionColumn
	 */
	public static MTableSelectionColumn get (MTableSelection ts, MColumn column)
	{
		String whereClause  = MTableSelectionColumn.COLUMNNAME_AD_TableSelection_ID + "=? AND "
							+ MTableSelectionColumn.COLUMNNAME_AD_Column_ID + "=?";
		return new Query(column.getCtx(),MTableSelectionColumn.Table_Name, whereClause, column.get_TrxName())
		.setOnlyActiveRecords(true)
		.setParameters(new Object[]{ts.getAD_TableSelection_ID(), column.getAD_Column_ID()})
		.first();
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -740931492029914957L;
	/**	Logger							*/
	private static CLogger	s_log = CLogger.getCLogger (MTableSelectionColumn.class);
	/** Element 						*/
	private M_Element m_element = null ;
	/** MColumn 						*/
	private MColumn m_column = null ;
	
	/**************************************************************************
	 * 	Asset Constructor
	 *	@param ctx context
	 *	@param AD_TableSelection_Column_ID  InOutBound ID
	 *	@param trxName transaction name 
	 */
	public MTableSelectionColumn (Properties ctx, int AD_TableSelection_Column_ID, String trxName)
	{
		super (ctx, AD_TableSelection_Column_ID, trxName);
		if (AD_TableSelection_Column_ID == 0)
		{
		}
	}

	/**
	 *	@param ctx context
	 *	@param AD_TableSelection_Column_ID Cahs Flow ID
	 */
	public MTableSelectionColumn (Properties ctx, int AD_TableSelection_Column_ID)
	{
		this (ctx, AD_TableSelection_Column_ID, null);
	}

	/**
	 *  Load Constructor
	 *  @param ctx context
	 *  @param rs result set record
	 *	@param trxName transaction
	 */
	public MTableSelectionColumn (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	
	
	/**
	 * get MColumn base on MColumn
	 * @param column
	 * @return 
	 */
	public MTableSelectionColumn (MTableSelection ts,MColumn column)
	{		
		super(column.getCtx(), 0 , column.get_TrxName());
		setAD_TableSelection_ID(ts.getAD_TableSelection_ID());
		setAD_Column_ID(column.getAD_Column_ID());
		setName(column.getColumnName());
		setDescription(column.getDescription());
		setHelp(column.getHelp());
		setIsActive(true);
		setIsIdentifier(column.isIdentifier());
		setIsRange(false);
		setIsQueryCriteria(false);
		setIsKey(false);
		setIsDisplayed(true);
	}
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{
		if(is_ValueChanged(COLUMNNAME_IsKey))
		{	
			/*if(getFieldKey() != null)
			{
				throw new AdempiereException("Only can have one field as key");
			}*/
		}
		//
		return true;
	}	//	beforeSave
	
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success)
	{
		if (!success)
		{
			return false;
		}
		
		return success;
	}	

	public M_Element getElement()
	{
		if(m_element == null)
		{
			m_element =  new M_Element(getCtx(), getAD_Column().getAD_Element_ID(), get_TrxName());			
		}
		return m_element;		
	}
	
	public MColumn getAD_Column()
	{
		if(m_column == null)
		{
			m_column = new MColumn(getCtx(), getAD_Column_ID(), get_TrxName());			
		}
		return m_column;		
	}
	
	/**
	 * 
	 * @return
	 */
	public MTableSelectionColumn getKeyColumn()
	{
		final String whereClause = MTableSelection.COLUMNNAME_AD_TableSelection_ID + "=? AND "
								 + MTableSelectionColumn.COLUMNNAME_IsKey + "=? AND "
								 + MTableSelectionColumn.COLUMNNAME_Name + "!=? ";
		return new Query(getCtx(),MTableSelectionColumn.Table_Name,whereClause, get_TrxName())
		.setParameters(new Object[]{this.getAD_TableSelection_ID(),"Y", getName()})
		.firstOnly();
	}
	/**
	 * 	String representation
	 *	@return info
	 */
	@Override
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ("MTableSelectionColumn")
			.append (get_ID ())
			.append ("-")
			.append (getName())
			.append ("]");
		return sb.toString ();
	}	//	toString
}	