/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

/** Generated Model for AD_ImportColumn
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_AD_ImportColumn extends PO implements I_AD_ImportColumn, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_AD_ImportColumn (Properties ctx, int AD_ImportColumn_ID, String trxName)
    {
      super (ctx, AD_ImportColumn_ID, trxName);
      /** if (AD_ImportColumn_ID == 0)
        {
			setAD_ImportColumn_ID (0);
			setAD_Import_ID (0);
			setIsGroupBy (false);
			setIsKey (false);
// N
			setIsOrderBy (false);
			setTarget_Column_ID (0);
        } */
    }

    /** Load Constructor */
    public X_AD_ImportColumn (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 4 - System 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_AD_ImportColumn[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Import Column Mapping.
		@param AD_ImportColumn_ID Import Column Mapping	  */
	public void setAD_ImportColumn_ID (int AD_ImportColumn_ID)
	{
		if (AD_ImportColumn_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_ImportColumn_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_ImportColumn_ID, Integer.valueOf(AD_ImportColumn_ID));
	}

	/** Get Import Column Mapping.
		@return Import Column Mapping	  */
	public int getAD_ImportColumn_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_ImportColumn_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_AD_Import getAD_Import() throws RuntimeException
    {
		return (I_AD_Import)MTable.get(getCtx(), I_AD_Import.Table_Name)
			.getPO(getAD_Import_ID(), get_TrxName());	}

	/** Set Data Import.
		@param AD_Import_ID Data Import	  */
	public void setAD_Import_ID (int AD_Import_ID)
	{
		if (AD_Import_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_Import_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Import_ID, Integer.valueOf(AD_Import_ID));
	}

	/** Get Data Import.
		@return Data Import	  */
	public int getAD_Import_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Import_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Default Logic.
		@param DefaultValue 
		Default value hierarchy, separated by ;
	  */
	public void setDefaultValue (String DefaultValue)
	{
		set_Value (COLUMNNAME_DefaultValue, DefaultValue);
	}

	/** Get Default Logic.
		@return Default value hierarchy, separated by ;
	  */
	public String getDefaultValue () 
	{
		return (String)get_Value(COLUMNNAME_DefaultValue);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Group by.
		@param IsGroupBy 
		After a group change, totals, etc. are printed
	  */
	public void setIsGroupBy (boolean IsGroupBy)
	{
		set_Value (COLUMNNAME_IsGroupBy, Boolean.valueOf(IsGroupBy));
	}

	/** Get Group by.
		@return After a group change, totals, etc. are printed
	  */
	public boolean isGroupBy () 
	{
		Object oo = get_Value(COLUMNNAME_IsGroupBy);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Key column.
		@param IsKey 
		This column is the key in this table
	  */
	public void setIsKey (boolean IsKey)
	{
		set_Value (COLUMNNAME_IsKey, Boolean.valueOf(IsKey));
	}

	/** Get Key column.
		@return This column is the key in this table
	  */
	public boolean isKey () 
	{
		Object oo = get_Value(COLUMNNAME_IsKey);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Order by.
		@param IsOrderBy 
		Include in sort order
	  */
	public void setIsOrderBy (boolean IsOrderBy)
	{
		set_Value (COLUMNNAME_IsOrderBy, Boolean.valueOf(IsOrderBy));
	}

	/** Get Order by.
		@return Include in sort order
	  */
	public boolean isOrderBy () 
	{
		Object oo = get_Value(COLUMNNAME_IsOrderBy);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Record Sort No.
		@param SortNo 
		Determines in what order the records are displayed
	  */
	public void setSortNo (int SortNo)
	{
		set_Value (COLUMNNAME_SortNo, Integer.valueOf(SortNo));
	}

	/** Get Record Sort No.
		@return Determines in what order the records are displayed
	  */
	public int getSortNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SortNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_AD_Column getSource_Column() throws RuntimeException
    {
		return (I_AD_Column)MTable.get(getCtx(), I_AD_Column.Table_Name)
			.getPO(getSource_Column_ID(), get_TrxName());	}

	/** Set Source Column.
		@param Source_Column_ID 
		Source column for import
	  */
	public void setSource_Column_ID (int Source_Column_ID)
	{
		if (Source_Column_ID < 1) 
			set_Value (COLUMNNAME_Source_Column_ID, null);
		else 
			set_Value (COLUMNNAME_Source_Column_ID, Integer.valueOf(Source_Column_ID));
	}

	/** Get Source Column.
		@return Source column for import
	  */
	public int getSource_Column_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Source_Column_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_AD_Column getTarget_Column() throws RuntimeException
    {
		return (I_AD_Column)MTable.get(getCtx(), I_AD_Column.Table_Name)
			.getPO(getTarget_Column_ID(), get_TrxName());	}

	/** Set Target Column.
		@param Target_Column_ID 
		Target column for import
	  */
	public void setTarget_Column_ID (int Target_Column_ID)
	{
		if (Target_Column_ID < 1) 
			set_Value (COLUMNNAME_Target_Column_ID, null);
		else 
			set_Value (COLUMNNAME_Target_Column_ID, Integer.valueOf(Target_Column_ID));
	}

	/** Get Target Column.
		@return Target column for import
	  */
	public int getTarget_Column_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Target_Column_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}