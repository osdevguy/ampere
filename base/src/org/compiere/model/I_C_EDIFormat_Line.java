/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for C_EDIFormat_Line
 *  @author Adempiere (generated) 
 *  @version 1.5.0
 */
public interface I_C_EDIFormat_Line 
{

    /** TableName=C_EDIFormat_Line */
    public static final String Table_Name = "C_EDIFormat_Line";

    /** AD_Table_ID=53424 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Column_ID */
    public static final String COLUMNNAME_AD_Column_ID = "AD_Column_ID";

	/** Set Column.
	  * Column in the table
	  */
	public void setAD_Column_ID (int AD_Column_ID);

	/** Get Column.
	  * Column in the table
	  */
	public int getAD_Column_ID();

	public I_AD_Column getAD_Column() throws RuntimeException;

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_EDIFormat_ID */
    public static final String COLUMNNAME_C_EDIFormat_ID = "C_EDIFormat_ID";

	/** Set EDI Format	  */
	public void setC_EDIFormat_ID (int C_EDIFormat_ID);

	/** Get EDI Format	  */
	public int getC_EDIFormat_ID();

	public I_C_EDIFormat getC_EDIFormat() throws RuntimeException;

    /** Column name C_EDIFormat_Line_ID */
    public static final String COLUMNNAME_C_EDIFormat_Line_ID = "C_EDIFormat_Line_ID";

	/** Set EDI Format Line	  */
	public void setC_EDIFormat_Line_ID (int C_EDIFormat_Line_ID);

	/** Get EDI Format Line	  */
	public int getC_EDIFormat_Line_ID();

    /** Column name C_EmbeddedEDIFormat_ID */
    public static final String COLUMNNAME_C_EmbeddedEDIFormat_ID = "C_EmbeddedEDIFormat_ID";

	/** Set Embedded EDI Format	  */
	public void setC_EmbeddedEDIFormat_ID (int C_EmbeddedEDIFormat_ID);

	/** Get Embedded EDI Format	  */
	public int getC_EmbeddedEDIFormat_ID();

	public I_C_EDIFormat getC_EmbeddedEDIFormat() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name EDIName */
    public static final String COLUMNNAME_EDIName = "EDIName";

	/** Set EDIName	  */
	public void setEDIName (String EDIName);

	/** Get EDIName	  */
	public String getEDIName();

    /** Column name Help */
    public static final String COLUMNNAME_Help = "Help";

	/** Set Comment/Help.
	  * Comment or Hint
	  */
	public void setHelp (String Help);

	/** Get Comment/Help.
	  * Comment or Hint
	  */
	public String getHelp();

    /** Column name IsAckLine */
    public static final String COLUMNNAME_IsAckLine = "IsAckLine";

	/** Set Acknowledgement Line	  */
	public void setIsAckLine (boolean IsAckLine);

	/** Get Acknowledgement Line	  */
	public boolean isAckLine();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsMandatory */
    public static final String COLUMNNAME_IsMandatory = "IsMandatory";

	/** Set Mandatory.
	  * Data entry is required in this column
	  */
	public void setIsMandatory (boolean IsMandatory);

	/** Get Mandatory.
	  * Data entry is required in this column
	  */
	public boolean isMandatory();

    /** Column name IsOneToMany */
    public static final String COLUMNNAME_IsOneToMany = "IsOneToMany";

	/** Set Repeating Line.
	  * Line might repeat one or more time in the source document
	  */
	public void setIsOneToMany (boolean IsOneToMany);

	/** Get Repeating Line.
	  * Line might repeat one or more time in the source document
	  */
	public boolean isOneToMany();

    /** Column name IsReset */
    public static final String COLUMNNAME_IsReset = "IsReset";

	/** Set IsReset	  */
	public void setIsReset (boolean IsReset);

	/** Get IsReset	  */
	public boolean isReset();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Position */
    public static final String COLUMNNAME_Position = "Position";

	/** Set Position	  */
	public void setPosition (int Position);

	/** Get Position	  */
	public int getPosition();

    /** Column name Type */
    public static final String COLUMNNAME_Type = "Type";

	/** Set Type.
	  * Type of Validation (SQL, Java Script, Java Language)
	  */
	public void setType (String Type);

	/** Get Type.
	  * Type of Validation (SQL, Java Script, Java Language)
	  */
	public String getType();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();

    /** Column name VariableName */
    public static final String COLUMNNAME_VariableName = "VariableName";

	/** Set VariableName	  */
	public void setVariableName (String VariableName);

	/** Get VariableName	  */
	public String getVariableName();
}
