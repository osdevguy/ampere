/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for C_BPartner_EDI
 *  @author Adempiere (generated) 
 *  @version 1.5.0
 */
public interface I_C_BPartner_EDI 
{

    /** TableName=C_BPartner_EDI */
    public static final String Table_Name = "C_BPartner_EDI";

    /** AD_Table_ID=53421 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_Table_ID */
    public static final String COLUMNNAME_AD_Table_ID = "AD_Table_ID";

	/** Set Table.
	  * Database Table information
	  */
	public void setAD_Table_ID (int AD_Table_ID);

	/** Get Table.
	  * Database Table information
	  */
	public int getAD_Table_ID();

	public I_AD_Table getAD_Table() throws RuntimeException;

    /** Column name C_BPartner_EDI_ID */
    public static final String COLUMNNAME_C_BPartner_EDI_ID = "C_BPartner_EDI_ID";

	/** Set BPartner EDI	  */
	public void setC_BPartner_EDI_ID (int C_BPartner_EDI_ID);

	/** Get BPartner EDI	  */
	public int getC_BPartner_EDI_ID();

    /** Column name C_BPartner_ID */
    public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";

	/** Set Business Partner .
	  * Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID);

	/** Get Business Partner .
	  * Identifies a Business Partner
	  */
	public int getC_BPartner_ID();

	public I_C_BPartner getC_BPartner() throws RuntimeException;

    /** Column name C_DocType_ID */
    public static final String COLUMNNAME_C_DocType_ID = "C_DocType_ID";

	/** Set Document Type.
	  * Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID);

	/** Get Document Type.
	  * Document type or rules
	  */
	public int getC_DocType_ID();

	public I_C_DocType getC_DocType() throws RuntimeException;

    /** Column name C_EDI_DocType_ID */
    public static final String COLUMNNAME_C_EDI_DocType_ID = "C_EDI_DocType_ID";

	/** Set EDI Doc Type	  */
	public void setC_EDI_DocType_ID (int C_EDI_DocType_ID);

	/** Get EDI Doc Type	  */
	public int getC_EDI_DocType_ID();

	public I_C_EDI_DocType getC_EDI_DocType() throws RuntimeException;

    /** Column name C_EDIFormat_ID */
    public static final String COLUMNNAME_C_EDIFormat_ID = "C_EDIFormat_ID";

	/** Set EDI Format	  */
	public void setC_EDIFormat_ID (int C_EDIFormat_ID);

	/** Get EDI Format	  */
	public int getC_EDIFormat_ID();

	public I_C_EDIFormat getC_EDIFormat() throws RuntimeException;

    /** Column name C_EDIProcessor_ID */
    public static final String COLUMNNAME_C_EDIProcessor_ID = "C_EDIProcessor_ID";

	/** Set EDI Processor	  */
	public void setC_EDIProcessor_ID (int C_EDIProcessor_ID);

	/** Get EDI Processor	  */
	public int getC_EDIProcessor_ID();

	public I_C_EDIProcessor getC_EDIProcessor() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name EDIEventListener */
    public static final String COLUMNNAME_EDIEventListener = "EDIEventListener";

	/** Set EDI Event Listener	  */
	public void setEDIEventListener (String EDIEventListener);

	/** Get EDI Event Listener	  */
	public String getEDIEventListener();

    /** Column name EDI_Sender */
    public static final String COLUMNNAME_EDI_Sender = "EDI_Sender";

	/** Set EDI Sender Id.
	  * EDI Sender Identification
	  */
	public void setEDI_Sender (String EDI_Sender);

	/** Get EDI Sender Id.
	  * EDI Sender Identification
	  */
	public String getEDI_Sender();

    /** Column name EDI_Sequence_ID */
    public static final String COLUMNNAME_EDI_Sequence_ID = "EDI_Sequence_ID";

	/** Set EDI Interchange Sequence.
	  * EDI Interchange Sequence
	  */
	public void setEDI_Sequence_ID (int EDI_Sequence_ID);

	/** Get EDI Interchange Sequence.
	  * EDI Interchange Sequence
	  */
	public int getEDI_Sequence_ID();

	public I_AD_Sequence getEDI_Sequence() throws RuntimeException;

    /** Column name Help */
    public static final String COLUMNNAME_Help = "Help";

	/** Set Comment/Help.
	  * Comment or Hint
	  */
	public void setHelp (String Help);

	/** Get Comment/Help.
	  * Comment or Hint
	  */
	public String getHelp();

    /** Column name Inbound */
    public static final String COLUMNNAME_Inbound = "Inbound";

	/** Set Inbound	  */
	public void setInbound (boolean Inbound);

	/** Get Inbound	  */
	public boolean isInbound();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();
}
