/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for T_MiniMRP
 *  @author Adempiere (generated) 
 *  @version 1.03
 */
public interface I_T_MiniMRP 
{

    /** TableName=T_MiniMRP */
    public static final String Table_Name = "T_MiniMRP";

    /** AD_Table_ID=53846 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AD_PInstance_ID */
    public static final String COLUMNNAME_AD_PInstance_ID = "AD_PInstance_ID";

	/** Set Process Instance.
	  * Instance of the process
	  */
	public void setAD_PInstance_ID (int AD_PInstance_ID);

	/** Get Process Instance.
	  * Instance of the process
	  */
	public int getAD_PInstance_ID();

	public I_AD_PInstance getAD_PInstance() throws RuntimeException;

    /** Column name C_Order_ID */
    public static final String COLUMNNAME_C_Order_ID = "C_Order_ID";

	/** Set Order.
	  * Order
	  */
	public void setC_Order_ID (int C_Order_ID);

	/** Get Order.
	  * Order
	  */
	public int getC_Order_ID();

	public I_C_Order getC_Order() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateFinish */
    public static final String COLUMNNAME_DateFinish = "DateFinish";

	/** Set Finish Date.
	  * Finish or (planned) completion date
	  */
	public void setDateFinish (Timestamp DateFinish);

	/** Get Finish Date.
	  * Finish or (planned) completion date
	  */
	public Timestamp getDateFinish();

    /** Column name DateStart */
    public static final String COLUMNNAME_DateStart = "DateStart";

	/** Set Date Start.
	  * Date Start for this Order
	  */
	public void setDateStart (Timestamp DateStart);

	/** Get Date Start.
	  * Date Start for this Order
	  */
	public Timestamp getDateStart();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public I_M_Product getM_Product() throws RuntimeException;

    /** Column name ProductName */
    public static final String COLUMNNAME_ProductName = "ProductName";

	/** Set Product Name.
	  * Name of the Product
	  */
	public void setProductName (String ProductName);

	/** Get Product Name.
	  * Name of the Product
	  */
	public String getProductName();

    /** Column name RecordType */
    public static final String COLUMNNAME_RecordType = "RecordType";

	/** Set RecordType	  */
	public void setRecordType (String RecordType);

	/** Get RecordType	  */
	public String getRecordType();

    /** Column name T_MiniMRP_ID */
    public static final String COLUMNNAME_T_MiniMRP_ID = "T_MiniMRP_ID";

	/** Set T_MiniMRP	  */
	public void setT_MiniMRP_ID (int T_MiniMRP_ID);

	/** Get T_MiniMRP	  */
	public int getT_MiniMRP_ID();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Week1 */
    public static final String COLUMNNAME_Week1 = "Week1";

	/** Set Week1	  */
	public void setWeek1 (int Week1);

	/** Get Week1	  */
	public int getWeek1();

    /** Column name Week10 */
    public static final String COLUMNNAME_Week10 = "Week10";

	/** Set Week10	  */
	public void setWeek10 (int Week10);

	/** Get Week10	  */
	public int getWeek10();

    /** Column name Week11 */
    public static final String COLUMNNAME_Week11 = "Week11";

	/** Set Week11	  */
	public void setWeek11 (int Week11);

	/** Get Week11	  */
	public int getWeek11();

    /** Column name Week12 */
    public static final String COLUMNNAME_Week12 = "Week12";

	/** Set Week12	  */
	public void setWeek12 (int Week12);

	/** Get Week12	  */
	public int getWeek12();

    /** Column name Week13 */
    public static final String COLUMNNAME_Week13 = "Week13";

	/** Set Week13	  */
	public void setWeek13 (int Week13);

	/** Get Week13	  */
	public int getWeek13();

    /** Column name Week14 */
    public static final String COLUMNNAME_Week14 = "Week14";

	/** Set Week14	  */
	public void setWeek14 (int Week14);

	/** Get Week14	  */
	public int getWeek14();

    /** Column name Week15 */
    public static final String COLUMNNAME_Week15 = "Week15";

	/** Set Week15	  */
	public void setWeek15 (int Week15);

	/** Get Week15	  */
	public int getWeek15();

    /** Column name Week16 */
    public static final String COLUMNNAME_Week16 = "Week16";

	/** Set Week16	  */
	public void setWeek16 (int Week16);

	/** Get Week16	  */
	public int getWeek16();

    /** Column name Week17 */
    public static final String COLUMNNAME_Week17 = "Week17";

	/** Set Week17	  */
	public void setWeek17 (int Week17);

	/** Get Week17	  */
	public int getWeek17();

    /** Column name Week18 */
    public static final String COLUMNNAME_Week18 = "Week18";

	/** Set Week18	  */
	public void setWeek18 (int Week18);

	/** Get Week18	  */
	public int getWeek18();

    /** Column name Week19 */
    public static final String COLUMNNAME_Week19 = "Week19";

	/** Set Week19	  */
	public void setWeek19 (int Week19);

	/** Get Week19	  */
	public int getWeek19();

    /** Column name Week2 */
    public static final String COLUMNNAME_Week2 = "Week2";

	/** Set Week2	  */
	public void setWeek2 (int Week2);

	/** Get Week2	  */
	public int getWeek2();

    /** Column name Week20 */
    public static final String COLUMNNAME_Week20 = "Week20";

	/** Set Week20	  */
	public void setWeek20 (int Week20);

	/** Get Week20	  */
	public int getWeek20();

    /** Column name Week21 */
    public static final String COLUMNNAME_Week21 = "Week21";

	/** Set Week21	  */
	public void setWeek21 (int Week21);

	/** Get Week21	  */
	public int getWeek21();

    /** Column name Week22 */
    public static final String COLUMNNAME_Week22 = "Week22";

	/** Set Week22	  */
	public void setWeek22 (int Week22);

	/** Get Week22	  */
	public int getWeek22();

    /** Column name Week23 */
    public static final String COLUMNNAME_Week23 = "Week23";

	/** Set Week23	  */
	public void setWeek23 (int Week23);

	/** Get Week23	  */
	public int getWeek23();

    /** Column name Week24 */
    public static final String COLUMNNAME_Week24 = "Week24";

	/** Set Week24	  */
	public void setWeek24 (int Week24);

	/** Get Week24	  */
	public int getWeek24();

    /** Column name Week3 */
    public static final String COLUMNNAME_Week3 = "Week3";

	/** Set Week3	  */
	public void setWeek3 (int Week3);

	/** Get Week3	  */
	public int getWeek3();

    /** Column name Week4 */
    public static final String COLUMNNAME_Week4 = "Week4";

	/** Set Week4	  */
	public void setWeek4 (int Week4);

	/** Get Week4	  */
	public int getWeek4();

    /** Column name Week5 */
    public static final String COLUMNNAME_Week5 = "Week5";

	/** Set Week5	  */
	public void setWeek5 (int Week5);

	/** Get Week5	  */
	public int getWeek5();

    /** Column name Week6 */
    public static final String COLUMNNAME_Week6 = "Week6";

	/** Set Week6	  */
	public void setWeek6 (int Week6);

	/** Get Week6	  */
	public int getWeek6();

    /** Column name Week7 */
    public static final String COLUMNNAME_Week7 = "Week7";

	/** Set Week7	  */
	public void setWeek7 (int Week7);

	/** Get Week7	  */
	public int getWeek7();

    /** Column name Week8 */
    public static final String COLUMNNAME_Week8 = "Week8";

	/** Set Week8	  */
	public void setWeek8 (int Week8);

	/** Get Week8	  */
	public int getWeek8();

    /** Column name Week9 */
    public static final String COLUMNNAME_Week9 = "Week9";

	/** Set Week9	  */
	public void setWeek9 (int Week9);

	/** Get Week9	  */
	public int getWeek9();
}
