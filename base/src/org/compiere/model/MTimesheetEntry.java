package org.compiere.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.util.CCache;
import org.compiere.util.Env;

public class MTimesheetEntry extends X_S_TimesheetEntry {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2924020425212317199L;

	public MTimesheetEntry(Properties ctx, int S_TimesheetEntry_ID, String trxName) {
		super(ctx, S_TimesheetEntry_ID, trxName);
		
		if (S_TimesheetEntry_ID == 0) {
			setAD_User_ID(Env.getAD_User_ID(ctx));
			setProcessed(false);
		}
	}

	public MTimesheetEntry(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	@Override
	protected boolean beforeSave(boolean newRecord) {
		if ( getS_Resource_ID() > 0 && getAD_User_ID() <= 0 )
		{
			setAD_User_ID(getS_Resource().getAD_User_ID());
		}

		if ( getAD_User_ID() > 0 && getC_BPartner_ID() <= 0 )
		{
			setC_BPartner_ID(getS_Resource().getAD_User().getC_BPartner_ID());
		}
		
		return true;
	}
	
	/**
	 * 	Before Delete
	 *	@return true if acct was deleted
	 */
	protected boolean beforeDelete ()
	{
		if (isPosted())
		{
			MPeriod.testPeriodOpen(getCtx(), getDateAcct(), MDocType.DOCBASETYPE_TimesheetEntry, getAD_Org_ID());
			setPosted(false);
			MFactAcct.deleteEx (Table_ID, get_ID(), get_TrxName());
		}
		return true;
	}	//	beforeDelete

	public MProject getProject() {
		if ( getC_Project_ID() > 0 )
			return (MProject) getC_Project();
		return null;
	}

	public boolean process() {
		
		if ( !isProcessed() )
			setProcessed(true);
		
		saveEx();
		
		return true;
		
	}
		
	private static CCache<String,MTimesheetEntry> s_cache = 
			new CCache<String,MTimesheetEntry> ("TimesheetEntry", 10);
	
	public static MTimesheetEntry get (Properties ctx,int c_Project_ID,int c_ProjectPhase_ID,int c_ProjectTask_ID,
											int s_Resourse_ID ,Timestamp date,String trxName)
	{
		String key=date+"-"+c_ProjectTask_ID+"-"+s_Resourse_ID;
		
		MTimesheetEntry retValue = (MTimesheetEntry) s_cache.get (key);
		if (retValue != null)
			return retValue;
		
		StringBuffer where=new StringBuffer();
		where.append( "C_Project_ID=? and C_ProjectPhase_Id=? and C_ProjectTask_Id=? and S_Resource_ID=? ");
		where.append(" and date_trunc('day', startdate)= date_trunc('day', ?::timestamp) ");
		
		Query q = new Query(ctx, Table_Name, where.toString(), trxName);
		q.setParameters(c_Project_ID,c_ProjectPhase_ID,c_ProjectTask_ID,s_Resourse_ID,date);		
		
		s_cache.put (key, (MTimesheetEntry)q.first());
		return(q.first());
	}	//	get
}
