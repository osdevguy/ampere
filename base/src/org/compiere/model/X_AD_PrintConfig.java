/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for AD_PrintConfig
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_AD_PrintConfig extends PO implements I_AD_PrintConfig, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_AD_PrintConfig (Properties ctx, int AD_PrintConfig_ID, String trxName)
    {
      super (ctx, AD_PrintConfig_ID, trxName);
      /** if (AD_PrintConfig_ID == 0)
        {
			setAD_PrintConfig_ID (0);
			setValue (null);
        } */
    }

    /** Load Constructor */
    public X_AD_PrintConfig (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_AD_PrintConfig[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Print Config ID.
		@param AD_PrintConfig_ID Print Config ID	  */
	public void setAD_PrintConfig_ID (int AD_PrintConfig_ID)
	{
		if (AD_PrintConfig_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_PrintConfig_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_PrintConfig_ID, Integer.valueOf(AD_PrintConfig_ID));
	}

	/** Get Print Config ID.
		@return Print Config ID	  */
	public int getAD_PrintConfig_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_PrintConfig_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_AD_PrintFormat getAD_PrintFormat() throws RuntimeException
    {
		return (I_AD_PrintFormat)MTable.get(getCtx(), I_AD_PrintFormat.Table_Name)
			.getPO(getAD_PrintFormat_ID(), get_TrxName());	}

	/** Set Print Format.
		@param AD_PrintFormat_ID 
		Data Print Format
	  */
	public void setAD_PrintFormat_ID (int AD_PrintFormat_ID)
	{
		if (AD_PrintFormat_ID < 1) 
			set_Value (COLUMNNAME_AD_PrintFormat_ID, null);
		else 
			set_Value (COLUMNNAME_AD_PrintFormat_ID, Integer.valueOf(AD_PrintFormat_ID));
	}

	/** Get Print Format.
		@return Data Print Format
	  */
	public int getAD_PrintFormat_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_PrintFormat_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Direct print.
		@param IsDirectPrint 
		Print without dialog
	  */
	public void setIsDirectPrint (boolean IsDirectPrint)
	{
		set_Value (COLUMNNAME_IsDirectPrint, Boolean.valueOf(IsDirectPrint));
	}

	/** Get Direct print.
		@return Print without dialog
	  */
	public boolean isDirectPrint () 
	{
		Object oo = get_Value(COLUMNNAME_IsDirectPrint);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Visible.
		@param IsVisible 
		Show in Screen
	  */
	public void setIsVisible (boolean IsVisible)
	{
		set_Value (COLUMNNAME_IsVisible, Boolean.valueOf(IsVisible));
	}

	/** Get Visible.
		@return Show in Screen
	  */
	public boolean isVisible () 
	{
		Object oo = get_Value(COLUMNNAME_IsVisible);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getValue());
    }
}