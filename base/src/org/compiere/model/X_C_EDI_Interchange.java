/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for C_EDI_Interchange
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_C_EDI_Interchange extends PO implements I_C_EDI_Interchange, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_C_EDI_Interchange (Properties ctx, int C_EDI_Interchange_ID, String trxName)
    {
      super (ctx, C_EDI_Interchange_ID, trxName);
      /** if (C_EDI_Interchange_ID == 0)
        {
			setC_EDI_Interchange_ID (0);
        } */
    }

    /** Load Constructor */
    public X_C_EDI_Interchange (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_EDI_Interchange[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Action AD_Reference_ID=53456 */
	public static final int ACTION_AD_Reference_ID=53456;
	/** Acknowledge all = 1 */
	public static final String ACTION_AcknowledgeAll = "1";
	/** Reject all = 4 */
	public static final String ACTION_RejectAll = "4";
	/** Acknowledged = 7 */
	public static final String ACTION_Acknowledged = "7";
	/** Set Action.
		@param Action 
		Indicates the Action to be performed
	  */
	public void setAction (String Action)
	{

		set_Value (COLUMNNAME_Action, Action);
	}

	/** Get Action.
		@return Indicates the Action to be performed
	  */
	public String getAction () 
	{
		return (String)get_Value(COLUMNNAME_Action);
	}

	public I_C_EDI_Ack getC_EDI_Ack() throws RuntimeException
    {
		return (I_C_EDI_Ack)MTable.get(getCtx(), I_C_EDI_Ack.Table_Name)
			.getPO(getC_EDI_Ack_ID(), get_TrxName());	}

	/** Set EDI Acknowledgement.
		@param C_EDI_Ack_ID EDI Acknowledgement	  */
	public void setC_EDI_Ack_ID (int C_EDI_Ack_ID)
	{
		if (C_EDI_Ack_ID < 1) 
			set_Value (COLUMNNAME_C_EDI_Ack_ID, null);
		else 
			set_Value (COLUMNNAME_C_EDI_Ack_ID, Integer.valueOf(C_EDI_Ack_ID));
	}

	/** Get EDI Acknowledgement.
		@return EDI Acknowledgement	  */
	public int getC_EDI_Ack_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDI_Ack_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDI Interchange.
		@param C_EDI_Interchange_ID EDI Interchange	  */
	public void setC_EDI_Interchange_ID (int C_EDI_Interchange_ID)
	{
		if (C_EDI_Interchange_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDI_Interchange_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDI_Interchange_ID, Integer.valueOf(C_EDI_Interchange_ID));
	}

	/** Get EDI Interchange.
		@return EDI Interchange	  */
	public int getC_EDI_Interchange_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDI_Interchange_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** ErrorMsg AD_Reference_ID=53457 */
	public static final int ERRORMSG_AD_Reference_ID=53457;
	/** Invalid Value = 12 */
	public static final String ERRORMSG_InvalidValue = "12";
	/** Missing = 13 */
	public static final String ERRORMSG_Missing = "13";
	/** Unspecified error = 18 */
	public static final String ERRORMSG_UnspecifiedError = "18";
	/** Unknown interchange sender = 23 */
	public static final String ERRORMSG_UnknownInterchangeSender = "23";
	/** Duplicate detected = 26 */
	public static final String ERRORMSG_DuplicateDetected = "26";
	/** Control count does not match number of instances = 29 */
	public static final String ERRORMSG_ControlCountDoesNotMatchNumberOfInstances = "29";
	/** Unknown interchange recipient = 43 */
	public static final String ERRORMSG_UnknownInterchangeRecipient = "43";
	/** Data segment missing/invalid = 6 */
	public static final String ERRORMSG_DataSegmentMissingInvalid = "6";
	/** Interchange recipient not actual recipient = 7 */
	public static final String ERRORMSG_InterchangeRecipientNotActualRecipient = "7";
	/** Set Error Msg.
		@param ErrorMsg Error Msg	  */
	public void setErrorMsg (String ErrorMsg)
	{

		set_Value (COLUMNNAME_ErrorMsg, ErrorMsg);
	}

	/** Get Error Msg.
		@return Error Msg	  */
	public String getErrorMsg () 
	{
		return (String)get_Value(COLUMNNAME_ErrorMsg);
	}

	/** Set Inbound.
		@param Inbound Inbound	  */
	public void setInbound (boolean Inbound)
	{
		set_ValueNoCheck (COLUMNNAME_Inbound, Boolean.valueOf(Inbound));
	}

	/** Get Inbound.
		@return Inbound	  */
	public boolean isInbound () 
	{
		Object oo = get_Value(COLUMNNAME_Inbound);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Interchange Control Number.
		@param InterchangeControlNo Interchange Control Number	  */
	public void setInterchangeControlNo (String InterchangeControlNo)
	{
		set_Value (COLUMNNAME_InterchangeControlNo, InterchangeControlNo);
	}

	/** Get Interchange Control Number.
		@return Interchange Control Number	  */
	public String getInterchangeControlNo () 
	{
		return (String)get_Value(COLUMNNAME_InterchangeControlNo);
	}

	/** Set Interchange Sender ID.
		@param InterchangeSender Interchange Sender ID	  */
	public void setInterchangeSender (String InterchangeSender)
	{
		set_Value (COLUMNNAME_InterchangeSender, InterchangeSender);
	}

	/** Get Interchange Sender ID.
		@return Interchange Sender ID	  */
	public String getInterchangeSender () 
	{
		return (String)get_Value(COLUMNNAME_InterchangeSender);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getInterchangeSender());
    }

	/** Set InterchangeSenderQualifier.
		@param InterchangeSenderQualifier InterchangeSenderQualifier	  */
	public void setInterchangeSenderQualifier (String InterchangeSenderQualifier)
	{
		set_Value (COLUMNNAME_InterchangeSenderQualifier, InterchangeSenderQualifier);
	}

	/** Get InterchangeSenderQualifier.
		@return InterchangeSenderQualifier	  */
	public String getInterchangeSenderQualifier () 
	{
		return (String)get_Value(COLUMNNAME_InterchangeSenderQualifier);
	}
}