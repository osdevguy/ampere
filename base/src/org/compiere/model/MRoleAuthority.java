package org.compiere.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.CLogger;
import org.compiere.util.DB;

/**
 * @author Nikunj Panelia
 *
 */
public class MRoleAuthority extends X_X_RoleAuthority
{

	private static final long	serialVersionUID	= 1L;
	/** Log								*/
	private static CLogger		s_log = CLogger.getCLogger (MRoleAuthority.class);

	public static MRoleAuthority get (Properties ctx, int authorityType_ID, int role_ID , String trxName)
	{
		MRoleAuthority retValue= null;

		String sql = "SELECT * FROM X_RoleAuthority "
			+ "WHERE X_AuthorityType_ID=? AND AD_Role_ID=?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement (sql, trxName);
			pstmt.setInt (1, authorityType_ID);
			pstmt.setInt (2, role_ID);
			rs = pstmt.executeQuery ();
			if (rs.next ())
				retValue = new MRoleAuthority (ctx, rs, trxName);
		}
		catch (SQLException ex)
		{
			s_log.log(Level.SEVERE, sql, ex);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		if (retValue == null)
			s_log.fine("Not Found - X_AuthorityType_ID=" + authorityType_ID 
				+ ", AD_Role_ID=" + role_ID);
		else
			s_log.fine("- X_AuthorityType_ID=" + authorityType_ID 
				+ ", AD_Role_ID=" + role_ID);
		return retValue;
		
	}

	/**
	 * 	Load Constructor
	 *	@param ctx context
	 *	@param rs result set
	 *	@param trxName transaction
	 */
	public MRoleAuthority (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MRoleAuthority
	
	public MRoleAuthority(Properties ctx, int ignored, String trxName)
	{
		super(ctx, ignored, trxName);
	}
	
	/**
	 * 	String Representation
	 *	@return info
	 */
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ("MRoleAuthority[");
		sb.append ("AD_Client_ID=").append (getAD_Client_ID());
		if (getAD_Org_ID() != 0)
			sb.append (",AD_Org_ID=").append (getAD_Org_ID());
		sb.append (",AD_Role_ID=").append (getAD_Role_ID())
		.append(",X_AuthorityType_ID=").append(getX_AuthorityType_ID())
		.append ("]");
		return sb.toString ();
	}	//	toString
}
