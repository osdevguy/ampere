/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

/** Generated Model for AX_Custom_PrintSetup
 *  @author Adempiere (generated) 
 *  @version 1.03 - $Id$ */
public class X_AX_Custom_PrintSetup extends PO implements I_AX_Custom_PrintSetup, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20121010L;

    /** Standard Constructor */
    public X_AX_Custom_PrintSetup (Properties ctx, int AX_Custom_PrintSetup_ID, String trxName)
    {
      super (ctx, AX_Custom_PrintSetup_ID, trxName);
      /** if (AX_Custom_PrintSetup_ID == 0)
        {
			setAX_Custom_PrintSetup_ID (0);
        } */
    }

    /** Load Constructor */
    public X_AX_Custom_PrintSetup (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_AX_Custom_PrintSetup[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_AD_PrintFormat getAD_PrintFormat() throws RuntimeException
    {
		return (I_AD_PrintFormat)MTable.get(getCtx(), I_AD_PrintFormat.Table_Name)
			.getPO(getAD_PrintFormat_ID(), get_TrxName());	}

	/** Set Print Format.
		@param AD_PrintFormat_ID 
		Data Print Format
	  */
	public void setAD_PrintFormat_ID (int AD_PrintFormat_ID)
	{
		if (AD_PrintFormat_ID < 1) 
			set_Value (COLUMNNAME_AD_PrintFormat_ID, null);
		else 
			set_Value (COLUMNNAME_AD_PrintFormat_ID, Integer.valueOf(AD_PrintFormat_ID));
	}

	/** Get Print Format.
		@return Data Print Format
	  */
	public int getAD_PrintFormat_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_PrintFormat_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Custom Print Setup.
		@param AX_Custom_PrintSetup_ID Custom Print Setup	  */
	public void setAX_Custom_PrintSetup_ID (int AX_Custom_PrintSetup_ID)
	{
		if (AX_Custom_PrintSetup_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AX_Custom_PrintSetup_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AX_Custom_PrintSetup_ID, Integer.valueOf(AX_Custom_PrintSetup_ID));
	}

	/** Get Custom Print Setup.
		@return Custom Print Setup	  */
	public int getAX_Custom_PrintSetup_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AX_Custom_PrintSetup_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}