/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

/** Generated Model for C_Recurring_Document
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_C_Recurring_Document extends PO implements I_C_Recurring_Document, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_C_Recurring_Document (Properties ctx, int C_Recurring_Document_ID, String trxName)
    {
      super (ctx, C_Recurring_Document_ID, trxName);
      /** if (C_Recurring_Document_ID == 0)
        {
			setC_Recurring_Document_ID (0);
			setC_Recurring_ID (0);
			setRecurringType (null);
			setRunsRemaining (0);
        } */
    }

    /** Load Constructor */
    public X_C_Recurring_Document (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_Recurring_Document[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Invoice getC_Invoice() throws RuntimeException
    {
		return (I_C_Invoice)MTable.get(getCtx(), I_C_Invoice.Table_Name)
			.getPO(getC_Invoice_ID(), get_TrxName());	}

	/** Set Invoice.
		@param C_Invoice_ID 
		Invoice Identifier
	  */
	public void setC_Invoice_ID (int C_Invoice_ID)
	{
		if (C_Invoice_ID < 1) 
			set_Value (COLUMNNAME_C_Invoice_ID, null);
		else 
			set_Value (COLUMNNAME_C_Invoice_ID, Integer.valueOf(C_Invoice_ID));
	}

	/** Get Invoice.
		@return Invoice Identifier
	  */
	public int getC_Invoice_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Invoice_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Order getC_Order() throws RuntimeException
    {
		return (I_C_Order)MTable.get(getCtx(), I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_Value (COLUMNNAME_C_Order_ID, null);
		else 
			set_Value (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Payment getC_Payment() throws RuntimeException
    {
		return (I_C_Payment)MTable.get(getCtx(), I_C_Payment.Table_Name)
			.getPO(getC_Payment_ID(), get_TrxName());	}

	/** Set Payment.
		@param C_Payment_ID 
		Payment identifier
	  */
	public void setC_Payment_ID (int C_Payment_ID)
	{
		if (C_Payment_ID < 1) 
			set_Value (COLUMNNAME_C_Payment_ID, null);
		else 
			set_Value (COLUMNNAME_C_Payment_ID, Integer.valueOf(C_Payment_ID));
	}

	/** Get Payment.
		@return Payment identifier
	  */
	public int getC_Payment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Payment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Project getC_Project() throws RuntimeException
    {
		return (I_C_Project)MTable.get(getCtx(), I_C_Project.Table_Name)
			.getPO(getC_Project_ID(), get_TrxName());	}

	/** Set Project.
		@param C_Project_ID 
		Financial Project
	  */
	public void setC_Project_ID (int C_Project_ID)
	{
		if (C_Project_ID < 1) 
			set_Value (COLUMNNAME_C_Project_ID, null);
		else 
			set_Value (COLUMNNAME_C_Project_ID, Integer.valueOf(C_Project_ID));
	}

	/** Get Project.
		@return Financial Project
	  */
	public int getC_Project_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Project_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Recurring Document.
		@param C_Recurring_Document_ID Recurring Document	  */
	public void setC_Recurring_Document_ID (int C_Recurring_Document_ID)
	{
		if (C_Recurring_Document_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Recurring_Document_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Recurring_Document_ID, Integer.valueOf(C_Recurring_Document_ID));
	}

	/** Get Recurring Document.
		@return Recurring Document	  */
	public int getC_Recurring_Document_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Recurring_Document_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Recurring getC_Recurring() throws RuntimeException
    {
		return (I_C_Recurring)MTable.get(getCtx(), I_C_Recurring.Table_Name)
			.getPO(getC_Recurring_ID(), get_TrxName());	}

	/** Set Recurring.
		@param C_Recurring_ID 
		Recurring Document
	  */
	public void setC_Recurring_ID (int C_Recurring_ID)
	{
		if (C_Recurring_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Recurring_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Recurring_ID, Integer.valueOf(C_Recurring_ID));
	}

	/** Get Recurring.
		@return Recurring Document
	  */
	public int getC_Recurring_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Recurring_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** DocAction AD_Reference_ID=53411 */
	public static final int DOCACTION_AD_Reference_ID=53411;
	/** Complete = CO */
	public static final String DOCACTION_Complete = "CO";
	/** Post = PO */
	public static final String DOCACTION_Post = "PO";
	/** Prepare = PR */
	public static final String DOCACTION_Prepare = "PR";
	/** Set Document Action.
		@param DocAction 
		The targeted status of the document
	  */
	public void setDocAction (String DocAction)
	{

		set_Value (COLUMNNAME_DocAction, DocAction);
	}

	/** Get Document Action.
		@return The targeted status of the document
	  */
	public String getDocAction () 
	{
		return (String)get_Value(COLUMNNAME_DocAction);
	}

	public I_GL_JournalBatch getGL_JournalBatch() throws RuntimeException
    {
		return (I_GL_JournalBatch)MTable.get(getCtx(), I_GL_JournalBatch.Table_Name)
			.getPO(getGL_JournalBatch_ID(), get_TrxName());	}

	/** Set Journal Batch.
		@param GL_JournalBatch_ID 
		General Ledger Journal Batch
	  */
	public void setGL_JournalBatch_ID (int GL_JournalBatch_ID)
	{
		if (GL_JournalBatch_ID < 1) 
			set_Value (COLUMNNAME_GL_JournalBatch_ID, null);
		else 
			set_Value (COLUMNNAME_GL_JournalBatch_ID, Integer.valueOf(GL_JournalBatch_ID));
	}

	/** Get Journal Batch.
		@return General Ledger Journal Batch
	  */
	public int getGL_JournalBatch_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GL_JournalBatch_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_GL_Journal getGL_Journal() throws RuntimeException
    {
		return (I_GL_Journal)MTable.get(getCtx(), I_GL_Journal.Table_Name)
			.getPO(getGL_Journal_ID(), get_TrxName());	}

	/** Set Journal.
		@param GL_Journal_ID 
		General Ledger Journal
	  */
	public void setGL_Journal_ID (int GL_Journal_ID)
	{
		if (GL_Journal_ID < 1) 
			set_Value (COLUMNNAME_GL_Journal_ID, null);
		else 
			set_Value (COLUMNNAME_GL_Journal_ID, Integer.valueOf(GL_Journal_ID));
	}

	/** Get Journal.
		@return General Ledger Journal
	  */
	public int getGL_Journal_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GL_Journal_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Comment/Help.
		@param Help 
		Comment or Hint
	  */
	public void setHelp (String Help)
	{
		set_Value (COLUMNNAME_Help, Help);
	}

	/** Get Comment/Help.
		@return Comment or Hint
	  */
	public String getHelp () 
	{
		return (String)get_Value(COLUMNNAME_Help);
	}

	/** Set Override Run Limit.
		@param IsOverrideRunLimit 
		Allow additional runs of this recurring document
	  */
	public void setIsOverrideRunLimit (boolean IsOverrideRunLimit)
	{
		set_Value (COLUMNNAME_IsOverrideRunLimit, Boolean.valueOf(IsOverrideRunLimit));
	}

	/** Get Override Run Limit.
		@return Allow additional runs of this recurring document
	  */
	public boolean isOverrideRunLimit () 
	{
		Object oo = get_Value(COLUMNNAME_IsOverrideRunLimit);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** RecurringType AD_Reference_ID=282 */
	public static final int RECURRINGTYPE_AD_Reference_ID=282;
	/** Invoice = I */
	public static final String RECURRINGTYPE_Invoice = "I";
	/** Order = O */
	public static final String RECURRINGTYPE_Order = "O";
	/** GL Journal Batch = G */
	public static final String RECURRINGTYPE_GLJournalBatch = "G";
	/** Project = J */
	public static final String RECURRINGTYPE_Project = "J";
	/** GL Journal = N */
	public static final String RECURRINGTYPE_GLJournal = "N";
	/** Set Recurring Type.
		@param RecurringType 
		Type of Recurring Document
	  */
	public void setRecurringType (String RecurringType)
	{

		set_Value (COLUMNNAME_RecurringType, RecurringType);
	}

	/** Get Recurring Type.
		@return Type of Recurring Document
	  */
	public String getRecurringType () 
	{
		return (String)get_Value(COLUMNNAME_RecurringType);
	}

	/** Set Remaining Runs.
		@param RunsRemaining 
		Number of recurring runs remaining
	  */
	public void setRunsRemaining (int RunsRemaining)
	{
		set_ValueNoCheck (COLUMNNAME_RunsRemaining, Integer.valueOf(RunsRemaining));
	}

	/** Get Remaining Runs.
		@return Number of recurring runs remaining
	  */
	public int getRunsRemaining () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_RunsRemaining);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Status.
		@param Status 
		Status of the currently running check
	  */
	public void setStatus (String Status)
	{
		set_Value (COLUMNNAME_Status, Status);
	}

	/** Get Status.
		@return Status of the currently running check
	  */
	public String getStatus () 
	{
		return (String)get_Value(COLUMNNAME_Status);
	}
}