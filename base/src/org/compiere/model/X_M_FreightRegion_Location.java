/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for M_FreightRegion_Location
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_M_FreightRegion_Location extends PO implements I_M_FreightRegion_Location, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_M_FreightRegion_Location (Properties ctx, int M_FreightRegion_Location_ID, String trxName)
    {
      super (ctx, M_FreightRegion_Location_ID, trxName);
      /** if (M_FreightRegion_Location_ID == 0)
        {
			setM_FreightRegion_ID (0);
			setM_FreightRegion_Location_ID (0);
        } */
    }

    /** Load Constructor */
    public X_M_FreightRegion_Location (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_M_FreightRegion_Location[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_Country getC_Country() throws RuntimeException
    {
		return (I_C_Country)MTable.get(getCtx(), I_C_Country.Table_Name)
			.getPO(getC_Country_ID(), get_TrxName());	}

	/** Set Country.
		@param C_Country_ID 
		Country 
	  */
	public void setC_Country_ID (int C_Country_ID)
	{
		if (C_Country_ID < 1) 
			set_Value (COLUMNNAME_C_Country_ID, null);
		else 
			set_Value (COLUMNNAME_C_Country_ID, Integer.valueOf(C_Country_ID));
	}

	/** Get Country.
		@return Country 
	  */
	public int getC_Country_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Country_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Region getC_Region() throws RuntimeException
    {
		return (I_C_Region)MTable.get(getCtx(), I_C_Region.Table_Name)
			.getPO(getC_Region_ID(), get_TrxName());	}

	/** Set Region.
		@param C_Region_ID 
		Identifies a geographical Region
	  */
	public void setC_Region_ID (int C_Region_ID)
	{
		if (C_Region_ID < 1) 
			set_Value (COLUMNNAME_C_Region_ID, null);
		else 
			set_Value (COLUMNNAME_C_Region_ID, Integer.valueOf(C_Region_ID));
	}

	/** Get Region.
		@return Identifies a geographical Region
	  */
	public int getC_Region_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Region_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_FreightRegion getM_FreightRegion() throws RuntimeException
    {
		return (I_M_FreightRegion)MTable.get(getCtx(), I_M_FreightRegion.Table_Name)
			.getPO(getM_FreightRegion_ID(), get_TrxName());	}

	/** Set Freight Region.
		@param M_FreightRegion_ID Freight Region	  */
	public void setM_FreightRegion_ID (int M_FreightRegion_ID)
	{
		if (M_FreightRegion_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_FreightRegion_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_FreightRegion_ID, Integer.valueOf(M_FreightRegion_ID));
	}

	/** Get Freight Region.
		@return Freight Region	  */
	public int getM_FreightRegion_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_FreightRegion_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getM_FreightRegion_ID()));
    }

	/** Set Freight Region Location.
		@param M_FreightRegion_Location_ID Freight Region Location	  */
	public void setM_FreightRegion_Location_ID (int M_FreightRegion_Location_ID)
	{
		if (M_FreightRegion_Location_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_FreightRegion_Location_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_FreightRegion_Location_ID, Integer.valueOf(M_FreightRegion_Location_ID));
	}

	/** Get Freight Region Location.
		@return Freight Region Location	  */
	public int getM_FreightRegion_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_FreightRegion_Location_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Postcode.
		@param Postal 
		Postcode
	  */
	public void setPostal (String Postal)
	{
		set_Value (COLUMNNAME_Postal, Postal);
	}

	/** Get Postcode.
		@return Postcode
	  */
	public String getPostal () 
	{
		return (String)get_Value(COLUMNNAME_Postal);
	}
}