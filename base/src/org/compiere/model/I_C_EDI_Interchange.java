/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for C_EDI_Interchange
 *  @author Adempiere (generated) 
 *  @version 1.5.0
 */
public interface I_C_EDI_Interchange 
{

    /** TableName=C_EDI_Interchange */
    public static final String Table_Name = "C_EDI_Interchange";

    /** AD_Table_ID=53430 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name Action */
    public static final String COLUMNNAME_Action = "Action";

	/** Set Action.
	  * Indicates the Action to be performed
	  */
	public void setAction (String Action);

	/** Get Action.
	  * Indicates the Action to be performed
	  */
	public String getAction();

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_EDI_Ack_ID */
    public static final String COLUMNNAME_C_EDI_Ack_ID = "C_EDI_Ack_ID";

	/** Set EDI Acknowledgement	  */
	public void setC_EDI_Ack_ID (int C_EDI_Ack_ID);

	/** Get EDI Acknowledgement	  */
	public int getC_EDI_Ack_ID();

	public I_C_EDI_Ack getC_EDI_Ack() throws RuntimeException;

    /** Column name C_EDI_Interchange_ID */
    public static final String COLUMNNAME_C_EDI_Interchange_ID = "C_EDI_Interchange_ID";

	/** Set EDI Interchange	  */
	public void setC_EDI_Interchange_ID (int C_EDI_Interchange_ID);

	/** Get EDI Interchange	  */
	public int getC_EDI_Interchange_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name ErrorMsg */
    public static final String COLUMNNAME_ErrorMsg = "ErrorMsg";

	/** Set Error Msg	  */
	public void setErrorMsg (String ErrorMsg);

	/** Get Error Msg	  */
	public String getErrorMsg();

    /** Column name Inbound */
    public static final String COLUMNNAME_Inbound = "Inbound";

	/** Set Inbound	  */
	public void setInbound (boolean Inbound);

	/** Get Inbound	  */
	public boolean isInbound();

    /** Column name InterchangeControlNo */
    public static final String COLUMNNAME_InterchangeControlNo = "InterchangeControlNo";

	/** Set Interchange Control Number	  */
	public void setInterchangeControlNo (String InterchangeControlNo);

	/** Get Interchange Control Number	  */
	public String getInterchangeControlNo();

    /** Column name InterchangeSender */
    public static final String COLUMNNAME_InterchangeSender = "InterchangeSender";

	/** Set Interchange Sender ID	  */
	public void setInterchangeSender (String InterchangeSender);

	/** Get Interchange Sender ID	  */
	public String getInterchangeSender();

    /** Column name InterchangeSenderQualifier */
    public static final String COLUMNNAME_InterchangeSenderQualifier = "InterchangeSenderQualifier";

	/** Set InterchangeSenderQualifier	  */
	public void setInterchangeSenderQualifier (String InterchangeSenderQualifier);

	/** Get InterchangeSenderQualifier	  */
	public String getInterchangeSenderQualifier();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
