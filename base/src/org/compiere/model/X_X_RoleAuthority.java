/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for X_RoleAuthority
 *  @author Adempiere (generated) 
 *  @version 1.03 - $Id$ */
public class X_X_RoleAuthority extends PO implements I_X_RoleAuthority, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20150128L;

    /** Standard Constructor */
    public X_X_RoleAuthority (Properties ctx, int X_RoleAuthority_ID, String trxName)
    {
      super (ctx, X_RoleAuthority_ID, trxName);
      /** if (X_RoleAuthority_ID == 0)
        {
			setAD_Role_ID (0);
			setC_Currency_ID (0);
			setX_AuthorityType_ID (0);
        } */
    }

    /** Load Constructor */
    public X_X_RoleAuthority (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_X_RoleAuthority[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_AD_Role getAD_Role() throws RuntimeException
    {
		return (I_AD_Role)MTable.get(getCtx(), I_AD_Role.Table_Name)
			.getPO(getAD_Role_ID(), get_TrxName());	}

	/** Set Role.
		@param AD_Role_ID 
		Responsibility Role
	  */
	public void setAD_Role_ID (int AD_Role_ID)
	{
		if (AD_Role_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_AD_Role_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Role_ID, Integer.valueOf(AD_Role_ID));
	}

	/** Get Role.
		@return Responsibility Role
	  */
	public int getAD_Role_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Role_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Approval Amount Accumulated.
		@param AmtApprovalAccum 
		The approval amount limit for this role accumulated on a period
	  */
	public void setAmtApprovalAccum (BigDecimal AmtApprovalAccum)
	{
		set_Value (COLUMNNAME_AmtApprovalAccum, AmtApprovalAccum);
	}

	/** Get Approval Amount Accumulated.
		@return The approval amount limit for this role accumulated on a period
	  */
	public BigDecimal getAmtApprovalAccum () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AmtApprovalAccum);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Approval Amount.
		@param ApprovalAmt 
		Document Approval Amount
	  */
	public void setApprovalAmt (BigDecimal ApprovalAmt)
	{
		set_Value (COLUMNNAME_ApprovalAmt, ApprovalAmt);
	}

	/** Get Approval Amount.
		@return Document Approval Amount
	  */
	public BigDecimal getApprovalAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ApprovalAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_Currency getC_Currency() throws RuntimeException
    {
		return (I_C_Currency)MTable.get(getCtx(), I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_Value (COLUMNNAME_C_Currency_ID, null);
		else 
			set_Value (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_X_AuthorityType getX_AuthorityType() throws RuntimeException
    {
		return (I_X_AuthorityType)MTable.get(getCtx(), I_X_AuthorityType.Table_Name)
			.getPO(getX_AuthorityType_ID(), get_TrxName());	}

	/** Set AuthorityType.
		@param X_AuthorityType_ID AuthorityType	  */
	public void setX_AuthorityType_ID (int X_AuthorityType_ID)
	{
		if (X_AuthorityType_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_X_AuthorityType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_X_AuthorityType_ID, Integer.valueOf(X_AuthorityType_ID));
	}

	/** Get AuthorityType.
		@return AuthorityType	  */
	public int getX_AuthorityType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_X_AuthorityType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}