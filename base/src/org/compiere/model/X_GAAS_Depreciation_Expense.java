/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for GAAS_Depreciation_Expense
 *  @author Adempiere (generated) 
 *  @version 1.03 - $Id$ */
public class X_GAAS_Depreciation_Expense extends PO implements I_GAAS_Depreciation_Expense, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20131008L;

    /** Standard Constructor */
    public X_GAAS_Depreciation_Expense (Properties ctx, int GAAS_Depreciation_Expense_ID, String trxName)
    {
      super (ctx, GAAS_Depreciation_Expense_ID, trxName);
      /** if (GAAS_Depreciation_Expense_ID == 0)
        {
			setCostAmt (Env.ZERO);
			setGAAS_Asset_Acct_ID (0);
			setGAAS_Depreciation_Expense_ID (0);
			setProcessed (false);
// N
        } */
    }

    /** Load Constructor */
    public X_GAAS_Depreciation_Expense (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_GAAS_Depreciation_Expense[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_A_Asset getA_Asset() throws RuntimeException
    {
		return (I_A_Asset)MTable.get(getCtx(), I_A_Asset.Table_Name)
			.getPO(getA_Asset_ID(), get_TrxName());	}

	/** Set Asset.
		@param A_Asset_ID 
		Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID)
	{
		if (A_Asset_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_ID, Integer.valueOf(A_Asset_ID));
	}

	/** Get Asset.
		@return Asset used internally or by customers
	  */
	public int getA_Asset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Asset Depreciation Date.
		@param AssetDepreciationDate 
		Date of last depreciation
	  */
	public void setAssetDepreciationDate (Timestamp AssetDepreciationDate)
	{
		set_ValueNoCheck (COLUMNNAME_AssetDepreciationDate, AssetDepreciationDate);
	}

	/** Get Asset Depreciation Date.
		@return Date of last depreciation
	  */
	public Timestamp getAssetDepreciationDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_AssetDepreciationDate);
	}

	/** Set Book Amount.
		@param BookAmt Book Amount	  */
	public void setBookAmt (BigDecimal BookAmt)
	{
		throw new IllegalArgumentException ("BookAmt is virtual column");	}

	/** Get Book Amount.
		@return Book Amount	  */
	public BigDecimal getBookAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BookAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Book Amount at End.
		@param BookAmtEnd Book Amount at End	  */
	public void setBookAmtEnd (BigDecimal BookAmtEnd)
	{
		throw new IllegalArgumentException ("BookAmtEnd is virtual column");	}

	/** Get Book Amount at End.
		@return Book Amount at End	  */
	public BigDecimal getBookAmtEnd () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BookAmtEnd);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Closing Balance.
		@param ClosingBalance Closing Balance	  */
	public void setClosingBalance (BigDecimal ClosingBalance)
	{
		set_ValueNoCheck (COLUMNNAME_ClosingBalance, ClosingBalance);
	}

	/** Get Closing Balance.
		@return Closing Balance	  */
	public BigDecimal getClosingBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ClosingBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cost Value.
		@param CostAmt 
		Value with Cost
	  */
	public void setCostAmt (BigDecimal CostAmt)
	{
		set_Value (COLUMNNAME_CostAmt, CostAmt);
	}

	/** Get Cost Value.
		@return Value with Cost
	  */
	public BigDecimal getCostAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CostAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_Period getC_Period() throws RuntimeException
    {
		return (I_C_Period)MTable.get(getCtx(), I_C_Period.Table_Name)
			.getPO(getC_Period_ID(), get_TrxName());	}

	/** Set Period.
		@param C_Period_ID 
		Period of the Calendar
	  */
	public void setC_Period_ID (int C_Period_ID)
	{
		if (C_Period_ID < 1) 
			set_Value (COLUMNNAME_C_Period_ID, null);
		else 
			set_Value (COLUMNNAME_C_Period_ID, Integer.valueOf(C_Period_ID));
	}

	/** Get Period.
		@return Period of the Calendar
	  */
	public int getC_Period_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Period_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Depreciation Expense Amount.
		@param DepreciationExpenseAmt Depreciation Expense Amount	  */
	public void setDepreciationExpenseAmt (BigDecimal DepreciationExpenseAmt)
	{
		set_ValueNoCheck (COLUMNNAME_DepreciationExpenseAmt, DepreciationExpenseAmt);
	}

	/** Get Depreciation Expense Amount.
		@return Depreciation Expense Amount	  */
	public BigDecimal getDepreciationExpenseAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DepreciationExpenseAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public I_GAAS_Asset_Acct getGAAS_Asset_Acct() throws RuntimeException
    {
		return (I_GAAS_Asset_Acct)MTable.get(getCtx(), I_GAAS_Asset_Acct.Table_Name)
			.getPO(getGAAS_Asset_Acct_ID(), get_TrxName());	}

	/** Set Asset Acct.
		@param GAAS_Asset_Acct_ID Asset Acct	  */
	public void setGAAS_Asset_Acct_ID (int GAAS_Asset_Acct_ID)
	{
		if (GAAS_Asset_Acct_ID < 1) 
			set_Value (COLUMNNAME_GAAS_Asset_Acct_ID, null);
		else 
			set_Value (COLUMNNAME_GAAS_Asset_Acct_ID, Integer.valueOf(GAAS_Asset_Acct_ID));
	}

	/** Get Asset Acct.
		@return Asset Acct	  */
	public int getGAAS_Asset_Acct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Asset_Acct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Build Depreciation Expense.
		@param GAAS_BuildDepreciation Build Depreciation Expense	  */
	public void setGAAS_BuildDepreciation (String GAAS_BuildDepreciation)
	{
		set_Value (COLUMNNAME_GAAS_BuildDepreciation, GAAS_BuildDepreciation);
	}

	/** Get Build Depreciation Expense.
		@return Build Depreciation Expense	  */
	public String getGAAS_BuildDepreciation () 
	{
		return (String)get_Value(COLUMNNAME_GAAS_BuildDepreciation);
	}

	/** Set Depreciation Expense.
		@param GAAS_Depreciation_Expense_ID Depreciation Expense	  */
	public void setGAAS_Depreciation_Expense_ID (int GAAS_Depreciation_Expense_ID)
	{
		if (GAAS_Depreciation_Expense_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_GAAS_Depreciation_Expense_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_GAAS_Depreciation_Expense_ID, Integer.valueOf(GAAS_Depreciation_Expense_ID));
	}

	/** Get Depreciation Expense.
		@return Depreciation Expense	  */
	public int getGAAS_Depreciation_Expense_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Depreciation_Expense_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Opening Balance.
		@param OpeningBalance Opening Balance	  */
	public void setOpeningBalance (BigDecimal OpeningBalance)
	{
		set_ValueNoCheck (COLUMNNAME_OpeningBalance, OpeningBalance);
	}

	/** Get Opening Balance.
		@return Opening Balance	  */
	public BigDecimal getOpeningBalance () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_OpeningBalance);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Period No.
		@param PeriodNo 
		Unique Period Number
	  */
	public void setPeriodNo (int PeriodNo)
	{
		set_ValueNoCheck (COLUMNNAME_PeriodNo, Integer.valueOf(PeriodNo));
	}

	/** Get Period No.
		@return Unique Period Number
	  */
	public int getPeriodNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_PeriodNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Processed_Flag.
		@param Processed_Flag Processed_Flag	  */
	public void setProcessed_Flag (String Processed_Flag)
	{
		throw new IllegalArgumentException ("Processed_Flag is virtual column");	}

	/** Get Processed_Flag.
		@return Processed_Flag	  */
	public String getProcessed_Flag () 
	{
		return (String)get_Value(COLUMNNAME_Processed_Flag);
	}
}