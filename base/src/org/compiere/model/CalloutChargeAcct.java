package org.compiere.model;

import java.util.Properties;

public class CalloutChargeAcct extends CalloutEngine
{
	public String Acct (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		if(value == null)
			return "";
		
		mTab.setValue("Ch_Revenue_Acct", value);
		
		return "";
	}

}
