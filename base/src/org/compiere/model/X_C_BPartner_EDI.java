/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for C_BPartner_EDI
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_C_BPartner_EDI extends PO implements I_C_BPartner_EDI, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_C_BPartner_EDI (Properties ctx, int C_BPartner_EDI_ID, String trxName)
    {
      super (ctx, C_BPartner_EDI_ID, trxName);
      /** if (C_BPartner_EDI_ID == 0)
        {
			setC_BPartner_EDI_ID (0);
			setC_BPartner_ID (0);
			setC_EDIFormat_ID (0);
			setC_EDIProcessor_ID (0);
			setInbound (false);
			setName (null);
			setValue (null);
        } */
    }

    /** Load Constructor */
    public X_C_BPartner_EDI (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_BPartner_EDI[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_AD_Table getAD_Table() throws RuntimeException
    {
		return (I_AD_Table)MTable.get(getCtx(), I_AD_Table.Table_Name)
			.getPO(getAD_Table_ID(), get_TrxName());	}

	/** Set Table.
		@param AD_Table_ID 
		Database Table information
	  */
	public void setAD_Table_ID (int AD_Table_ID)
	{
		if (AD_Table_ID < 1) 
			set_Value (COLUMNNAME_AD_Table_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Table_ID, Integer.valueOf(AD_Table_ID));
	}

	/** Get Table.
		@return Database Table information
	  */
	public int getAD_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set BPartner EDI.
		@param C_BPartner_EDI_ID BPartner EDI	  */
	public void setC_BPartner_EDI_ID (int C_BPartner_EDI_ID)
	{
		if (C_BPartner_EDI_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_EDI_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_EDI_ID, Integer.valueOf(C_BPartner_EDI_ID));
	}

	/** Get BPartner EDI.
		@return BPartner EDI	  */
	public int getC_BPartner_EDI_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_EDI_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_DocType getC_DocType() throws RuntimeException
    {
		return (I_C_DocType)MTable.get(getCtx(), I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_Value (COLUMNNAME_C_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_EDI_DocType getC_EDI_DocType() throws RuntimeException
    {
		return (I_C_EDI_DocType)MTable.get(getCtx(), I_C_EDI_DocType.Table_Name)
			.getPO(getC_EDI_DocType_ID(), get_TrxName());	}

	/** Set EDI Doc Type.
		@param C_EDI_DocType_ID EDI Doc Type	  */
	public void setC_EDI_DocType_ID (int C_EDI_DocType_ID)
	{
		if (C_EDI_DocType_ID < 1) 
			set_Value (COLUMNNAME_C_EDI_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_EDI_DocType_ID, Integer.valueOf(C_EDI_DocType_ID));
	}

	/** Get EDI Doc Type.
		@return EDI Doc Type	  */
	public int getC_EDI_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDI_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_EDIFormat getC_EDIFormat() throws RuntimeException
    {
		return (I_C_EDIFormat)MTable.get(getCtx(), I_C_EDIFormat.Table_Name)
			.getPO(getC_EDIFormat_ID(), get_TrxName());	}

	/** Set EDI Format.
		@param C_EDIFormat_ID EDI Format	  */
	public void setC_EDIFormat_ID (int C_EDIFormat_ID)
	{
		if (C_EDIFormat_ID < 1) 
			set_Value (COLUMNNAME_C_EDIFormat_ID, null);
		else 
			set_Value (COLUMNNAME_C_EDIFormat_ID, Integer.valueOf(C_EDIFormat_ID));
	}

	/** Get EDI Format.
		@return EDI Format	  */
	public int getC_EDIFormat_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDIFormat_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_EDIProcessor getC_EDIProcessor() throws RuntimeException
    {
		return (I_C_EDIProcessor)MTable.get(getCtx(), I_C_EDIProcessor.Table_Name)
			.getPO(getC_EDIProcessor_ID(), get_TrxName());	}

	/** Set EDI Processor.
		@param C_EDIProcessor_ID EDI Processor	  */
	public void setC_EDIProcessor_ID (int C_EDIProcessor_ID)
	{
		if (C_EDIProcessor_ID < 1) 
			set_Value (COLUMNNAME_C_EDIProcessor_ID, null);
		else 
			set_Value (COLUMNNAME_C_EDIProcessor_ID, Integer.valueOf(C_EDIProcessor_ID));
	}

	/** Get EDI Processor.
		@return EDI Processor	  */
	public int getC_EDIProcessor_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDIProcessor_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set EDI Event Listener.
		@param EDIEventListener EDI Event Listener	  */
	public void setEDIEventListener (String EDIEventListener)
	{
		set_Value (COLUMNNAME_EDIEventListener, EDIEventListener);
	}

	/** Get EDI Event Listener.
		@return EDI Event Listener	  */
	public String getEDIEventListener () 
	{
		return (String)get_Value(COLUMNNAME_EDIEventListener);
	}

	/** Set EDI Sender Id.
		@param EDI_Sender 
		EDI Sender Identification
	  */
	public void setEDI_Sender (String EDI_Sender)
	{
		set_Value (COLUMNNAME_EDI_Sender, EDI_Sender);
	}

	/** Get EDI Sender Id.
		@return EDI Sender Identification
	  */
	public String getEDI_Sender () 
	{
		return (String)get_Value(COLUMNNAME_EDI_Sender);
	}

	public I_AD_Sequence getEDI_Sequence() throws RuntimeException
    {
		return (I_AD_Sequence)MTable.get(getCtx(), I_AD_Sequence.Table_Name)
			.getPO(getEDI_Sequence_ID(), get_TrxName());	}

	/** Set EDI Interchange Sequence.
		@param EDI_Sequence_ID 
		EDI Interchange Sequence
	  */
	public void setEDI_Sequence_ID (int EDI_Sequence_ID)
	{
		if (EDI_Sequence_ID < 1) 
			set_Value (COLUMNNAME_EDI_Sequence_ID, null);
		else 
			set_Value (COLUMNNAME_EDI_Sequence_ID, Integer.valueOf(EDI_Sequence_ID));
	}

	/** Get EDI Interchange Sequence.
		@return EDI Interchange Sequence
	  */
	public int getEDI_Sequence_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_EDI_Sequence_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Comment/Help.
		@param Help 
		Comment or Hint
	  */
	public void setHelp (String Help)
	{
		set_Value (COLUMNNAME_Help, Help);
	}

	/** Get Comment/Help.
		@return Comment or Hint
	  */
	public String getHelp () 
	{
		return (String)get_Value(COLUMNNAME_Help);
	}

	/** Set Inbound.
		@param Inbound Inbound	  */
	public void setInbound (boolean Inbound)
	{
		set_Value (COLUMNNAME_Inbound, Boolean.valueOf(Inbound));
	}

	/** Get Inbound.
		@return Inbound	  */
	public boolean isInbound () 
	{
		Object oo = get_Value(COLUMNNAME_Inbound);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getValue());
    }
}