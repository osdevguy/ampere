package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.util.Env;

/**
 * @author Nikunj Panelia
 *
 */
public class CallOutImportInvoice extends CalloutEngine
{
	
	public String taxAmt (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		if(value==null)
			return "";
		
		BigDecimal amt=BigDecimal.ZERO;
		BigDecimal rate=BigDecimal.ZERO;
		if( (mField.getColumnName().equals("AmtAcctCr") || mField.getColumnName().equals("AmtAcctDr")))
		{
			if(mTab.getValue("C_Tax_ID") ==null)
				return "";
			amt=(BigDecimal)value;
			int tax_ID=(Integer)mTab.getValue("C_Tax_ID");
			
			MTax tax=new MTax(Env.getCtx(),tax_ID, null);
			rate=tax.getRate();
		}
		else if(mField.getColumnName().equals("C_Tax_ID"))
		{
			if(mTab.getValue("AmtAcctCr") == null &&  mTab.getValue("AmtAcctDr") == null)
				return "";
			if(mTab.getValue("AmtAcctCr") !=null && ((BigDecimal)mTab.getValue("AmtAcctCr")).compareTo(BigDecimal.ZERO) > 0)
				amt= (BigDecimal)mTab.getValue("AmtAcctCr");
			else if(mTab.getValue("AmtAcctDr") !=null && ((BigDecimal)mTab.getValue("AmtAcctDr")).compareTo(BigDecimal.ZERO) > 0)
				amt= (BigDecimal)mTab.getValue("AmtAcctDr");
			
			MTax tax=new MTax(Env.getCtx(), (Integer)value, null);
			rate=tax.getRate();	
			
		}

		BigDecimal TaxAmt=calculateTaxAmt(amt,rate);
		int precision=2;
		if(mTab.getValue("C_Currency_ID") != null && (Integer)mTab.getValue("C_Currency_ID") > 0)
		{
			MCurrency cur=MCurrency.get(Env.getCtx(), (Integer)mTab.getValue("C_Currency_ID"));
			precision=cur.getStdPrecision();
		}
		TaxAmt=TaxAmt.setScale(precision, BigDecimal.ROUND_HALF_UP);
		mTab.setValue("TaxAmt", TaxAmt);
		BigDecimal unitAmt=amt.subtract(TaxAmt);
		
		mTab.setValue("PriceActual", unitAmt);
		return "";
	}	//	tax

	public static BigDecimal calculateTaxAmt(BigDecimal amt, BigDecimal rate)
	{
		BigDecimal tax=BigDecimal.ZERO;
		BigDecimal multiplier=rate.divide(new BigDecimal(100), 12, BigDecimal.ROUND_HALF_UP);
		multiplier = multiplier.add(Env.ONE);
		BigDecimal base = amt.divide(multiplier, 12, BigDecimal.ROUND_HALF_UP); 
		tax = amt.subtract(base);
		return tax;
	}

	public String tax (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		String column = mField.getColumnName();
		if (value == null)
			return "";

		//	Check Product
		int M_Product_ID = 0;
		if (column.equals("M_Product_ID"))
			M_Product_ID = ((Integer)value).intValue();
		int C_Charge_ID = 0;
		if (column.equals("C_Charge_ID") )
			C_Charge_ID = ((Integer)value).intValue();
		
		int shipC_BPartner_Location_ID =0;
		int billC_BPartner_Location_ID =0;
		//	Check Partner Location
		if (M_Product_ID != 0 || C_Charge_ID != 0)
		{
			shipC_BPartner_Location_ID = Env.getContextAsInt(ctx, WindowNo, "C_BPartner_Location_ID");
			billC_BPartner_Location_ID = shipC_BPartner_Location_ID;
		}
				
		if(column.equals("C_BPartner_Location_ID"))
		{
			shipC_BPartner_Location_ID=(Integer)value;
			billC_BPartner_Location_ID = shipC_BPartner_Location_ID;
			if(mTab.getValue("M_Product_ID") == null && mTab.getValue("C_Charge_ID") == null )
				return "";
			if(mTab.getValue("M_Product_ID") !=null && (Integer)mTab.getValue("M_Product_ID") > 0)
			{
				M_Product_ID=(Integer)mTab.getValue("M_Product_ID");
			}
			else if(mTab.getValue("C_Charge_ID") !=null && (Integer)mTab.getValue("C_Charge_ID") > 0)
			{
				C_Charge_ID=(Integer)mTab.getValue("C_Charge_ID");
			}
		}

		//	Dates
		Timestamp billDate = Env.getContextAsDate(ctx, WindowNo, "DateAcct");
		Timestamp shipDate = billDate;

		int AD_Org_ID = Env.getContextAsInt(ctx, WindowNo, "AD_Org_ID");
		if(AD_Org_ID==0)
			AD_Org_ID=Env.getAD_Org_ID(Env.getCtx());

		int M_Warehouse_ID = Env.getContextAsInt(ctx, "#M_Warehouse_ID");
		boolean isSOTrx=true;
		if(mTab.getValue("AmtAcctDr") !=null && ((BigDecimal)mTab.getValue("AmtAcctDr")).compareTo(BigDecimal.ZERO) > 0)
			isSOTrx=false;
		//
		if(AD_Org_ID==0 || M_Warehouse_ID==0 || billC_BPartner_Location_ID==0 || (M_Product_ID==0 &&  C_Charge_ID==0))
			return "";
		
		int C_Tax_ID = Tax.get(ctx, M_Product_ID, C_Charge_ID, billDate, shipDate,
			AD_Org_ID, M_Warehouse_ID, billC_BPartner_Location_ID, shipC_BPartner_Location_ID,
			isSOTrx);
		//
		if (C_Tax_ID != 0)
			mTab.setValue("C_Tax_ID", new Integer(C_Tax_ID));
		/*else
			mTab.fireDataStatusEEvent(CLogger.retrieveError());*/
		//
	
		
		return "";
	}
}
