/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for C_EDI_Ack
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_C_EDI_Ack extends PO implements I_C_EDI_Ack, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_C_EDI_Ack (Properties ctx, int C_EDI_Ack_ID, String trxName)
    {
      super (ctx, C_EDI_Ack_ID, trxName);
      /** if (C_EDI_Ack_ID == 0)
        {
			setC_EDI_Ack_ID (0);
			setInterchangeControlNo (null);
			setInterchangeSender (null);
			setIsSubmitted (false);
// N
        } */
    }

    /** Load Constructor */
    public X_C_EDI_Ack (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_EDI_Ack[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set EDI Acknowledgement.
		@param C_EDI_Ack_ID EDI Acknowledgement	  */
	public void setC_EDI_Ack_ID (int C_EDI_Ack_ID)
	{
		if (C_EDI_Ack_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDI_Ack_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDI_Ack_ID, Integer.valueOf(C_EDI_Ack_ID));
	}

	/** Get EDI Acknowledgement.
		@return EDI Acknowledgement	  */
	public int getC_EDI_Ack_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDI_Ack_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_EDI_Interchange getC_EDI_Interchange() throws RuntimeException
    {
		return (I_C_EDI_Interchange)MTable.get(getCtx(), I_C_EDI_Interchange.Table_Name)
			.getPO(getC_EDI_Interchange_ID(), get_TrxName());	}

	/** Set EDI Interchange.
		@param C_EDI_Interchange_ID EDI Interchange	  */
	public void setC_EDI_Interchange_ID (int C_EDI_Interchange_ID)
	{
		if (C_EDI_Interchange_ID < 1) 
			set_Value (COLUMNNAME_C_EDI_Interchange_ID, null);
		else 
			set_Value (COLUMNNAME_C_EDI_Interchange_ID, Integer.valueOf(C_EDI_Interchange_ID));
	}

	/** Get EDI Interchange.
		@return EDI Interchange	  */
	public int getC_EDI_Interchange_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDI_Interchange_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Functional Identifier.
		@param FunctionalIdentifier Functional Identifier	  */
	public void setFunctionalIdentifier (String FunctionalIdentifier)
	{
		set_Value (COLUMNNAME_FunctionalIdentifier, FunctionalIdentifier);
	}

	/** Get Functional Identifier.
		@return Functional Identifier	  */
	public String getFunctionalIdentifier () 
	{
		return (String)get_Value(COLUMNNAME_FunctionalIdentifier);
	}

	/** Set Interchange Control Number.
		@param InterchangeControlNo Interchange Control Number	  */
	public void setInterchangeControlNo (String InterchangeControlNo)
	{
		set_Value (COLUMNNAME_InterchangeControlNo, InterchangeControlNo);
	}

	/** Get Interchange Control Number.
		@return Interchange Control Number	  */
	public String getInterchangeControlNo () 
	{
		return (String)get_Value(COLUMNNAME_InterchangeControlNo);
	}

	/** Set Interchange Sender ID.
		@param InterchangeSender Interchange Sender ID	  */
	public void setInterchangeSender (String InterchangeSender)
	{
		set_Value (COLUMNNAME_InterchangeSender, InterchangeSender);
	}

	/** Get Interchange Sender ID.
		@return Interchange Sender ID	  */
	public String getInterchangeSender () 
	{
		return (String)get_Value(COLUMNNAME_InterchangeSender);
	}

	/** Set InterchangeSenderQualifier.
		@param InterchangeSenderQualifier InterchangeSenderQualifier	  */
	public void setInterchangeSenderQualifier (String InterchangeSenderQualifier)
	{
		set_Value (COLUMNNAME_InterchangeSenderQualifier, InterchangeSenderQualifier);
	}

	/** Get InterchangeSenderQualifier.
		@return InterchangeSenderQualifier	  */
	public String getInterchangeSenderQualifier () 
	{
		return (String)get_Value(COLUMNNAME_InterchangeSenderQualifier);
	}

	/** Set Submitted.
		@param IsSubmitted Submitted	  */
	public void setIsSubmitted (boolean IsSubmitted)
	{
		set_Value (COLUMNNAME_IsSubmitted, Boolean.valueOf(IsSubmitted));
	}

	/** Get Submitted.
		@return Submitted	  */
	public boolean isSubmitted () 
	{
		Object oo = get_Value(COLUMNNAME_IsSubmitted);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Lines Accepted.
		@param LinesAccepted Lines Accepted	  */
	public void setLinesAccepted (BigDecimal LinesAccepted)
	{
		set_Value (COLUMNNAME_LinesAccepted, LinesAccepted);
	}

	/** Get Lines Accepted.
		@return Lines Accepted	  */
	public BigDecimal getLinesAccepted () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LinesAccepted);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Lines Included.
		@param LinesIncluded Lines Included	  */
	public void setLinesIncluded (BigDecimal LinesIncluded)
	{
		set_Value (COLUMNNAME_LinesIncluded, LinesIncluded);
	}

	/** Get Lines Included.
		@return Lines Included	  */
	public BigDecimal getLinesIncluded () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LinesIncluded);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Lines Received.
		@param LinesReceived Lines Received	  */
	public void setLinesReceived (BigDecimal LinesReceived)
	{
		set_Value (COLUMNNAME_LinesReceived, LinesReceived);
	}

	/** Get Lines Received.
		@return Lines Received	  */
	public BigDecimal getLinesReceived () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LinesReceived);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Sender Group Control Number.
		@param SenderGroupControlNo Sender Group Control Number	  */
	public void setSenderGroupControlNo (String SenderGroupControlNo)
	{
		set_Value (COLUMNNAME_SenderGroupControlNo, SenderGroupControlNo);
	}

	/** Get Sender Group Control Number.
		@return Sender Group Control Number	  */
	public String getSenderGroupControlNo () 
	{
		return (String)get_Value(COLUMNNAME_SenderGroupControlNo);
	}
}