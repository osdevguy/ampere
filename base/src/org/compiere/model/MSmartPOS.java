/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.process.DocAction;
import org.compiere.util.CCache;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.ValueNamePair;

/**
 * SmartPOS configuration model
 * @author Paul Bowden
 */
public class MSmartPOS extends X_C_SmartPOS
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7906423679951693385L;


	/**
	 * 	Get MSmartPOS from Cache
	 *	@param ctx context
	 *	@param C_SmartPOS_ID id
	 *	@return MSmartPOS
	 */
	public static MSmartPOS get (Properties ctx, String queueName, int AD_Client_ID)
	{
		MSmartPOS retValue = (MSmartPOS) s_cache.get (AD_Client_ID + "_" + queueName);
		if (retValue != null)
			return retValue;
		retValue = MTable.get(ctx, MSmartPOS.Table_ID)
		.createQuery("JMSqueue = ? AND AD_Client_ID = ?", null)
		.setParameters(queueName, AD_Client_ID)
		.setOnlyActiveRecords(true)
		.firstOnly();

		if (retValue != null && retValue.get_ID () != 0)
			s_cache.put (AD_Client_ID + "_" + queueName, retValue);
		return retValue;
	} //	get

	/**	Cache						*/
	private static CCache<String,MSmartPOS> s_cache = new CCache<String,MSmartPOS>("C_SmartPOS", 5);
	
	public int getC_Currency_ID() {
		return getM_PriceList().getC_Currency_ID();
	}
	
	
	/**************************************************************************
	 * 	Standard Constructor
	 *	@param ctx context
	 *	@param C_SmartPOS_ID group
	 *	@param trxName trx
	 */
	public MSmartPOS (Properties ctx, int C_SmartPOS_ID, String trxName)
	{
		super (ctx, C_SmartPOS_ID, trxName);
	}	//	MSmartPOS

	/**
	 * 	Load Constructor
	 *	@param ctx context
	 *	@param rs result set
	 *	@param trxName trx
	 */
	public MSmartPOS (Properties ctx, ResultSet rs, String trxName)
	{
		super (ctx, rs, trxName);
	}	//	MSmartPOS

	public static List<MSmartPOS> getOrgPOS(Properties ctx, int AD_Client_ID, int AD_Org_ID) {
		Query query;
		if ( AD_Org_ID > 0 )
		{
			query = MTable.get(ctx, MSmartPOS.Table_ID)
			.createQuery("AD_Client_ID = ? AND AD_Org_ID = ?", null);
			query.setParameters(AD_Org_ID);
		}
		else
		{
			query = MTable.get(ctx, MSmartPOS.Table_ID)
			.createQuery("AD_Client_ID = ?", null);
		}

		query.setOnlyActiveRecords(true).setClient_ID();

		return query.list();
	}

	/**
	 * Cretaes payment receipts
	 * @param trx
	 * @param dateInvoice
	 * @param orderId
	 * @param payment
	 * @param invoiceId
	 * @param DocAction
	 * @param DocStatus
	 * @param dataLogin
	 * @param description
	 * @return
	 * @throws InterruptedException
	 */
	public Boolean createPayment(Trx trx, Date dateInvoice, int C_BPartner_ID, int orderId ,BigDecimal payAmt, 
			int invoiceId,	 String description, BigDecimal amount,HashMap<String, Object> to, boolean isReceipt)
	throws InterruptedException
	{
		Env.setContext(getCtx(),"#AD_Client_ID", getAD_Client_ID());
		Env.setContext(getCtx(),"#AD_Org_ID", getAD_Org_ID());

		Boolean lok = false;
		MPayment payment = new MPayment(getCtx(), 0, trx.getTrxName());

		payment.setAD_Client_ID(getAD_Client_ID());
		payment.setAD_Org_ID(getAD_Org_ID());
		payment.setIsActive(true);
		payment.setDateAcct(new Timestamp(dateInvoice.getTime()));
		payment.setC_BPartner_ID(C_BPartner_ID);
		payment.setTrxType("S");
		payment.setC_Order_ID(orderId);
		payment.setIsReceipt(isReceipt);
		payment.setBankAccountDetails(getC_BankAccount_ID());
		payment.setC_Invoice_ID(invoiceId);
		payment.setC_DocType_ID(isReceipt);

		if(to == null)
		{
			payment.setTenderType("K");
			payment.setCreditCardType("M");
		}
		else
		{
			String auxStr = ((String)to.get("name")).trim();
			for (ValueNamePair vnp : MRefList.getList(getCtx(), MPayment.TENDERTYPE_AD_Reference_ID, false))
			{
				if ( vnp.getName().equals(auxStr))
					payment.setTenderType(vnp.getValue());
			}

			auxStr = ((String)to.get("credit-card-type")); 
			for (ValueNamePair vnp : MRefList.getList(getCtx(), MPayment.CREDITCARDTYPE_AD_Reference_ID, false))
			{
				if ( vnp.getName().equals(auxStr))
					payment.setCreditCardType(vnp.getValue());
			}

			auxStr = ((String)to.get("expiration-date")); 
			if(auxStr != null){
				auxStr = auxStr.trim();
				if(auxStr.length() >= 2 )
					payment.setCreditCardExpMM(new Integer(auxStr.substring(0,2)));
				else
					payment.setCreditCardExpMM(0);

				if(auxStr.length() >= 4)
					payment.setCreditCardExpYY(new Integer(auxStr.substring(2,4)));
				else
					payment.setCreditCardExpMM(0);
			}else{
				payment.setCreditCardExpMM(1);
				payment.setCreditCardExpYY(3);
			}


			auxStr = (String)to.get("account-no"); 
			if(auxStr != null)
				payment.setAccountNo(auxStr);

			auxStr = (String)to.get("check-no"); 
			if(auxStr != null)
				payment.setCheckNo(auxStr);

			auxStr = (String)to.get("micr"); 
			if(auxStr != null)
				payment.setMicr(auxStr);

			auxStr = (String)to.get("number"); 
			if(auxStr != null)
				payment.setCreditCardNumber(auxStr);
		}

		payment.setDocAction(DocAction.ACTION_Complete);
		payment.setDocStatus(DocAction.STATUS_Drafted);
		payment.setPayAmt(payAmt);
		payment.setC_Currency_ID(MClient.get(getCtx()).getC_Currency_ID());
		
		payment.setIsAllocated(true);
		payment.setDescription(description);
		payment.setC_ConversionType_ID(114); 
		payment.setPosted(false);
		payment.setProcessed(true);
		payment.setIsApproved(true);
		payment.setOProcessing("N");

		payment.setRoutingNo("");
		payment.setAccountNo("");
		payment.setIsOverUnderPayment(false);
		payment.setR_CVV2Match(false);
		payment.setOverUnderAmt(amount);

		payment.saveEx();
		lok = payment.processIt(DocAction.ACTION_Complete);
		lok = payment.save();
		if (!lok ){
			trx.rollback();
			trx.close();
			log.log(Level.WARNING, "Could not Update Payment: "+payment.toString());
		} 

		return lok;
	}//createPayment


	public Integer getC_PaymentTerm_ID() {
		return getC_BPartnerCashTrx().getC_PaymentTerm_ID();
	}


}	//	MSmartPOS
