/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for M_WorkCentre
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_M_WorkCentre extends PO implements I_M_WorkCentre, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_M_WorkCentre (Properties ctx, int M_WorkCentre_ID, String trxName)
    {
      super (ctx, M_WorkCentre_ID, trxName);
      /** if (M_WorkCentre_ID == 0)
        {
			setM_WorkCentre_ID (0);
        } */
    }

    /** Load Constructor */
    public X_M_WorkCentre (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_M_WorkCentre[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Capacity In Time UOM.
		@param CapacityInTimeUOM 
		Define capacity in Capacity UOM (e.g. 40)
	  */
	public void setCapacityInTimeUOM (BigDecimal CapacityInTimeUOM)
	{
		set_Value (COLUMNNAME_CapacityInTimeUOM, CapacityInTimeUOM);
	}

	/** Get Capacity In Time UOM.
		@return Define capacity in Capacity UOM (e.g. 40)
	  */
	public BigDecimal getCapacityInTimeUOM () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CapacityInTimeUOM);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_M_Product_Category getM_Product_Category() throws RuntimeException
    {
		return (I_M_Product_Category)MTable.get(getCtx(), I_M_Product_Category.Table_Name)
			.getPO(getM_Product_Category_ID(), get_TrxName());	}

	/** Set Product Category.
		@param M_Product_Category_ID 
		Category of a Product
	  */
	public void setM_Product_Category_ID (int M_Product_Category_ID)
	{
		if (M_Product_Category_ID < 1) 
			set_Value (COLUMNNAME_M_Product_Category_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_Category_ID, Integer.valueOf(M_Product_Category_ID));
	}

	/** Get Product Category.
		@return Category of a Product
	  */
	public int getM_Product_Category_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_Category_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Work Centre.
		@param M_WorkCentre_ID Work Centre	  */
	public void setM_WorkCentre_ID (int M_WorkCentre_ID)
	{
		if (M_WorkCentre_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_WorkCentre_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_WorkCentre_ID, Integer.valueOf(M_WorkCentre_ID));
	}

	/** Get Work Centre.
		@return Work Centre	  */
	public int getM_WorkCentre_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_WorkCentre_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_WorkCentreType getM_WorkCentreType() throws RuntimeException
    {
		return (I_M_WorkCentreType)MTable.get(getCtx(), I_M_WorkCentreType.Table_Name)
			.getPO(getM_WorkCentreType_ID(), get_TrxName());	}

	/** Set Work Centre Type ID.
		@param M_WorkCentreType_ID Work Centre Type ID	  */
	public void setM_WorkCentreType_ID (int M_WorkCentreType_ID)
	{
		if (M_WorkCentreType_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_WorkCentreType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_WorkCentreType_ID, Integer.valueOf(M_WorkCentreType_ID));
	}

	/** Get Work Centre Type ID.
		@return Work Centre Type ID	  */
	public int getM_WorkCentreType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_WorkCentreType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }
}