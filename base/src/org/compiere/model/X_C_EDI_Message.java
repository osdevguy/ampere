/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for C_EDI_Message
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_C_EDI_Message extends PO implements I_C_EDI_Message, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_C_EDI_Message (Properties ctx, int C_EDI_Message_ID, String trxName)
    {
      super (ctx, C_EDI_Message_ID, trxName);
      /** if (C_EDI_Message_ID == 0)
        {
			setC_EDI_Interchange_ID (0);
			setC_EDI_Message_ID (0);
        } */
    }

    /** Load Constructor */
    public X_C_EDI_Message (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_EDI_Message[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Action AD_Reference_ID=53456 */
	public static final int ACTION_AD_Reference_ID=53456;
	/** Acknowledge all = 1 */
	public static final String ACTION_AcknowledgeAll = "1";
	/** Reject all = 4 */
	public static final String ACTION_RejectAll = "4";
	/** Acknowledged = 7 */
	public static final String ACTION_Acknowledged = "7";
	/** Set Action.
		@param Action 
		Indicates the Action to be performed
	  */
	public void setAction (String Action)
	{

		set_Value (COLUMNNAME_Action, Action);
	}

	/** Get Action.
		@return Indicates the Action to be performed
	  */
	public String getAction () 
	{
		return (String)get_Value(COLUMNNAME_Action);
	}

	public I_AD_Table getAD_Table() throws RuntimeException
    {
		return (I_AD_Table)MTable.get(getCtx(), I_AD_Table.Table_Name)
			.getPO(getAD_Table_ID(), get_TrxName());	}

	/** Set Table.
		@param AD_Table_ID 
		Database Table information
	  */
	public void setAD_Table_ID (int AD_Table_ID)
	{
		if (AD_Table_ID < 1) 
			set_Value (COLUMNNAME_AD_Table_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Table_ID, Integer.valueOf(AD_Table_ID));
	}

	/** Get Table.
		@return Database Table information
	  */
	public int getAD_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_EDI_Interchange getC_EDI_Interchange() throws RuntimeException
    {
		return (I_C_EDI_Interchange)MTable.get(getCtx(), I_C_EDI_Interchange.Table_Name)
			.getPO(getC_EDI_Interchange_ID(), get_TrxName());	}

	/** Set EDI Interchange.
		@param C_EDI_Interchange_ID EDI Interchange	  */
	public void setC_EDI_Interchange_ID (int C_EDI_Interchange_ID)
	{
		if (C_EDI_Interchange_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDI_Interchange_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDI_Interchange_ID, Integer.valueOf(C_EDI_Interchange_ID));
	}

	/** Get EDI Interchange.
		@return EDI Interchange	  */
	public int getC_EDI_Interchange_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDI_Interchange_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDI Message.
		@param C_EDI_Message_ID EDI Message	  */
	public void setC_EDI_Message_ID (int C_EDI_Message_ID)
	{
		if (C_EDI_Message_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDI_Message_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDI_Message_ID, Integer.valueOf(C_EDI_Message_ID));
	}

	/** Get EDI Message.
		@return EDI Message	  */
	public int getC_EDI_Message_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDI_Message_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** ErrorMsg AD_Reference_ID=53457 */
	public static final int ERRORMSG_AD_Reference_ID=53457;
	/** Invalid Value = 12 */
	public static final String ERRORMSG_InvalidValue = "12";
	/** Missing = 13 */
	public static final String ERRORMSG_Missing = "13";
	/** Unspecified error = 18 */
	public static final String ERRORMSG_UnspecifiedError = "18";
	/** Unknown interchange sender = 23 */
	public static final String ERRORMSG_UnknownInterchangeSender = "23";
	/** Duplicate detected = 26 */
	public static final String ERRORMSG_DuplicateDetected = "26";
	/** Control count does not match number of instances = 29 */
	public static final String ERRORMSG_ControlCountDoesNotMatchNumberOfInstances = "29";
	/** Unknown interchange recipient = 43 */
	public static final String ERRORMSG_UnknownInterchangeRecipient = "43";
	/** Data segment missing/invalid = 6 */
	public static final String ERRORMSG_DataSegmentMissingInvalid = "6";
	/** Interchange recipient not actual recipient = 7 */
	public static final String ERRORMSG_InterchangeRecipientNotActualRecipient = "7";
	/** Set Error Msg.
		@param ErrorMsg Error Msg	  */
	public void setErrorMsg (String ErrorMsg)
	{

		set_Value (COLUMNNAME_ErrorMsg, ErrorMsg);
	}

	/** Get Error Msg.
		@return Error Msg	  */
	public String getErrorMsg () 
	{
		return (String)get_Value(COLUMNNAME_ErrorMsg);
	}

	/** Set MessageReferenceNo.
		@param MessageReferenceNo MessageReferenceNo	  */
	public void setMessageReferenceNo (String MessageReferenceNo)
	{
		set_Value (COLUMNNAME_MessageReferenceNo, MessageReferenceNo);
	}

	/** Get MessageReferenceNo.
		@return MessageReferenceNo	  */
	public String getMessageReferenceNo () 
	{
		return (String)get_Value(COLUMNNAME_MessageReferenceNo);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getMessageReferenceNo());
    }

	/** Set Message Type.
		@param MessageType Message Type	  */
	public void setMessageType (String MessageType)
	{
		set_Value (COLUMNNAME_MessageType, MessageType);
	}

	/** Get Message Type.
		@return Message Type	  */
	public String getMessageType () 
	{
		return (String)get_Value(COLUMNNAME_MessageType);
	}

	/** Set Record ID.
		@param Record_ID 
		Direct internal record ID
	  */
	public void setRecord_ID (int Record_ID)
	{
		if (Record_ID < 0) 
			set_Value (COLUMNNAME_Record_ID, null);
		else 
			set_Value (COLUMNNAME_Record_ID, Integer.valueOf(Record_ID));
	}

	/** Get Record ID.
		@return Direct internal record ID
	  */
	public int getRecord_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Record_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}