/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for C_EDIFormat_Line
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_C_EDIFormat_Line extends PO implements I_C_EDIFormat_Line, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_C_EDIFormat_Line (Properties ctx, int C_EDIFormat_Line_ID, String trxName)
    {
      super (ctx, C_EDIFormat_Line_ID, trxName);
      /** if (C_EDIFormat_Line_ID == 0)
        {
			setC_EDIFormat_Line_ID (0);
			setEDIName (null);
			setIsOneToMany (false);
// 'N'
			setName (null);
			setType (null);
// E
			setValue (null);
        } */
    }

    /** Load Constructor */
    public X_C_EDIFormat_Line (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_EDIFormat_Line[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_AD_Column getAD_Column() throws RuntimeException
    {
		return (I_AD_Column)MTable.get(getCtx(), I_AD_Column.Table_Name)
			.getPO(getAD_Column_ID(), get_TrxName());	}

	/** Set Column.
		@param AD_Column_ID 
		Column in the table
	  */
	public void setAD_Column_ID (int AD_Column_ID)
	{
		if (AD_Column_ID < 1) 
			set_Value (COLUMNNAME_AD_Column_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Column_ID, Integer.valueOf(AD_Column_ID));
	}

	/** Get Column.
		@return Column in the table
	  */
	public int getAD_Column_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Column_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_EDIFormat getC_EDIFormat() throws RuntimeException
    {
		return (I_C_EDIFormat)MTable.get(getCtx(), I_C_EDIFormat.Table_Name)
			.getPO(getC_EDIFormat_ID(), get_TrxName());	}

	/** Set EDI Format.
		@param C_EDIFormat_ID EDI Format	  */
	public void setC_EDIFormat_ID (int C_EDIFormat_ID)
	{
		if (C_EDIFormat_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDIFormat_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDIFormat_ID, Integer.valueOf(C_EDIFormat_ID));
	}

	/** Get EDI Format.
		@return EDI Format	  */
	public int getC_EDIFormat_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDIFormat_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDI Format Line.
		@param C_EDIFormat_Line_ID EDI Format Line	  */
	public void setC_EDIFormat_Line_ID (int C_EDIFormat_Line_ID)
	{
		if (C_EDIFormat_Line_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDIFormat_Line_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDIFormat_Line_ID, Integer.valueOf(C_EDIFormat_Line_ID));
	}

	/** Get EDI Format Line.
		@return EDI Format Line	  */
	public int getC_EDIFormat_Line_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDIFormat_Line_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_EDIFormat getC_EmbeddedEDIFormat() throws RuntimeException
    {
		return (I_C_EDIFormat)MTable.get(getCtx(), I_C_EDIFormat.Table_Name)
			.getPO(getC_EmbeddedEDIFormat_ID(), get_TrxName());	}

	/** Set Embedded EDI Format.
		@param C_EmbeddedEDIFormat_ID Embedded EDI Format	  */
	public void setC_EmbeddedEDIFormat_ID (int C_EmbeddedEDIFormat_ID)
	{
		if (C_EmbeddedEDIFormat_ID < 1) 
			set_Value (COLUMNNAME_C_EmbeddedEDIFormat_ID, null);
		else 
			set_Value (COLUMNNAME_C_EmbeddedEDIFormat_ID, Integer.valueOf(C_EmbeddedEDIFormat_ID));
	}

	/** Get Embedded EDI Format.
		@return Embedded EDI Format	  */
	public int getC_EmbeddedEDIFormat_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EmbeddedEDIFormat_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set EDIName.
		@param EDIName EDIName	  */
	public void setEDIName (String EDIName)
	{
		set_Value (COLUMNNAME_EDIName, EDIName);
	}

	/** Get EDIName.
		@return EDIName	  */
	public String getEDIName () 
	{
		return (String)get_Value(COLUMNNAME_EDIName);
	}

	/** Set Comment/Help.
		@param Help 
		Comment or Hint
	  */
	public void setHelp (String Help)
	{
		set_Value (COLUMNNAME_Help, Help);
	}

	/** Get Comment/Help.
		@return Comment or Hint
	  */
	public String getHelp () 
	{
		return (String)get_Value(COLUMNNAME_Help);
	}

	/** Set Acknowledgement Line.
		@param IsAckLine Acknowledgement Line	  */
	public void setIsAckLine (boolean IsAckLine)
	{
		set_Value (COLUMNNAME_IsAckLine, Boolean.valueOf(IsAckLine));
	}

	/** Get Acknowledgement Line.
		@return Acknowledgement Line	  */
	public boolean isAckLine () 
	{
		Object oo = get_Value(COLUMNNAME_IsAckLine);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Mandatory.
		@param IsMandatory 
		Data entry is required in this column
	  */
	public void setIsMandatory (boolean IsMandatory)
	{
		set_Value (COLUMNNAME_IsMandatory, Boolean.valueOf(IsMandatory));
	}

	/** Get Mandatory.
		@return Data entry is required in this column
	  */
	public boolean isMandatory () 
	{
		Object oo = get_Value(COLUMNNAME_IsMandatory);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Repeating Line.
		@param IsOneToMany 
		Line might repeat one or more time in the source document
	  */
	public void setIsOneToMany (boolean IsOneToMany)
	{
		set_Value (COLUMNNAME_IsOneToMany, Boolean.valueOf(IsOneToMany));
	}

	/** Get Repeating Line.
		@return Line might repeat one or more time in the source document
	  */
	public boolean isOneToMany () 
	{
		Object oo = get_Value(COLUMNNAME_IsOneToMany);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set IsReset.
		@param IsReset IsReset	  */
	public void setIsReset (boolean IsReset)
	{
		set_Value (COLUMNNAME_IsReset, Boolean.valueOf(IsReset));
	}

	/** Get IsReset.
		@return IsReset	  */
	public boolean isReset () 
	{
		Object oo = get_Value(COLUMNNAME_IsReset);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Position.
		@param Position Position	  */
	public void setPosition (int Position)
	{
		set_Value (COLUMNNAME_Position, Integer.valueOf(Position));
	}

	/** Get Position.
		@return Position	  */
	public int getPosition () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Position);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Type AD_Reference_ID=53454 */
	public static final int TYPE_AD_Reference_ID=53454;
	/** Element = E */
	public static final String TYPE_Element = "E";
	/** Segment = S */
	public static final String TYPE_Segment = "S";
	/** Set Type.
		@param Type 
		Type of Validation (SQL, Java Script, Java Language)
	  */
	public void setType (String Type)
	{

		set_Value (COLUMNNAME_Type, Type);
	}

	/** Get Type.
		@return Type of Validation (SQL, Java Script, Java Language)
	  */
	public String getType () 
	{
		return (String)get_Value(COLUMNNAME_Type);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getValue());
    }

	/** Set VariableName.
		@param VariableName VariableName	  */
	public void setVariableName (String VariableName)
	{
		set_Value (COLUMNNAME_VariableName, VariableName);
	}

	/** Get VariableName.
		@return VariableName	  */
	public String getVariableName () 
	{
		return (String)get_Value(COLUMNNAME_VariableName);
	}
}