/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for M_Replenish
 *  @author Adempiere (generated) 
 *  @version 1.5.0
 */
public interface I_M_Replenish 
{

    /** TableName=M_Replenish */
    public static final String Table_Name = "M_Replenish";

    /** AD_Table_ID=249 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name AX_Seasonality_ID */
    public static final String COLUMNNAME_AX_Seasonality_ID = "AX_Seasonality_ID";

	/** Set Seasonality Type.
	  * Seasonality Type
	  */
	public void setAX_Seasonality_ID (int AX_Seasonality_ID);

	/** Get Seasonality Type.
	  * Seasonality Type
	  */
	public int getAX_Seasonality_ID();

	public I_AX_Seasonality getAX_Seasonality() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DailyUsageAve */
    public static final String COLUMNNAME_DailyUsageAve = "DailyUsageAve";

	/** Set DailyUsageAve.
	  * Ave Daily Usage during Average Sample Days defined for the product
	  */
	public void setDailyUsageAve (BigDecimal DailyUsageAve);

	/** Get DailyUsageAve.
	  * Ave Daily Usage during Average Sample Days defined for the product
	  */
	public BigDecimal getDailyUsageAve();

    /** Column name DailyUsageAvePlusSeasonality */
    public static final String COLUMNNAME_DailyUsageAvePlusSeasonality = "DailyUsageAvePlusSeasonality";

	/** Set DailyUsageAvePlusSeasonality.
	  * DailyUsageAvePlusSeasonality
	  */
	public void setDailyUsageAvePlusSeasonality (BigDecimal DailyUsageAvePlusSeasonality);

	/** Get DailyUsageAvePlusSeasonality.
	  * DailyUsageAvePlusSeasonality
	  */
	public BigDecimal getDailyUsageAvePlusSeasonality();

    /** Column name DailyUsageMax */
    public static final String COLUMNNAME_DailyUsageMax = "DailyUsageMax";

	/** Set DailyUsageMax.
	  * Max Daily Usage during Average Sample Days defined for the product
	  */
	public void setDailyUsageMax (BigDecimal DailyUsageMax);

	/** Get DailyUsageMax.
	  * Max Daily Usage during Average Sample Days defined for the product
	  */
	public BigDecimal getDailyUsageMax();

    /** Column name DailyUsageMin */
    public static final String COLUMNNAME_DailyUsageMin = "DailyUsageMin";

	/** Set DailyUsageMin	  */
	public void setDailyUsageMin (BigDecimal DailyUsageMin);

	/** Get DailyUsageMin	  */
	public BigDecimal getDailyUsageMin();

    /** Column name DaysSalesSamplePeriod */
    public static final String COLUMNNAME_DaysSalesSamplePeriod = "DaysSalesSamplePeriod";

	/** Set DaysSalesSamplePeriod.
	  * DaysSalesSample
	  */
	public void setDaysSalesSamplePeriod (BigDecimal DaysSalesSamplePeriod);

	/** Get DaysSalesSamplePeriod.
	  * DaysSalesSample
	  */
	public BigDecimal getDaysSalesSamplePeriod();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Level_Max */
    public static final String COLUMNNAME_Level_Max = "Level_Max";

	/** Set Maximum Level.
	  * Maximum Inventory level for this product
	  */
	public void setLevel_Max (BigDecimal Level_Max);

	/** Get Maximum Level.
	  * Maximum Inventory level for this product
	  */
	public BigDecimal getLevel_Max();

    /** Column name Level_Min */
    public static final String COLUMNNAME_Level_Min = "Level_Min";

	/** Set Minimum Level.
	  * Minimum Inventory level for this product
	  */
	public void setLevel_Min (BigDecimal Level_Min);

	/** Get Minimum Level.
	  * Minimum Inventory level for this product
	  */
	public BigDecimal getLevel_Min();

    /** Column name M_Locator_ID */
    public static final String COLUMNNAME_M_Locator_ID = "M_Locator_ID";

	/** Set Locator.
	  * Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID);

	/** Get Locator.
	  * Warehouse Locator
	  */
	public int getM_Locator_ID();

	public I_M_Locator getM_Locator() throws RuntimeException;

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public I_M_Product getM_Product() throws RuntimeException;

    /** Column name M_Warehouse_ID */
    public static final String COLUMNNAME_M_Warehouse_ID = "M_Warehouse_ID";

	/** Set Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID);

	/** Get Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID();

	public I_M_Warehouse getM_Warehouse() throws RuntimeException;

    /** Column name M_WarehouseSource_ID */
    public static final String COLUMNNAME_M_WarehouseSource_ID = "M_WarehouseSource_ID";

	/** Set Source Warehouse.
	  * Optional Warehouse to replenish from
	  */
	public void setM_WarehouseSource_ID (int M_WarehouseSource_ID);

	/** Get Source Warehouse.
	  * Optional Warehouse to replenish from
	  */
	public int getM_WarehouseSource_ID();

	public I_M_Warehouse getM_WarehouseSource() throws RuntimeException;

    /** Column name QtyBatchSize */
    public static final String COLUMNNAME_QtyBatchSize = "QtyBatchSize";

	/** Set Qty Batch Size	  */
	public void setQtyBatchSize (BigDecimal QtyBatchSize);

	/** Get Qty Batch Size	  */
	public BigDecimal getQtyBatchSize();

    /** Column name RecalcUsage */
    public static final String COLUMNNAME_RecalcUsage = "RecalcUsage";

	/** Set RecalcUsage.
	  * Recaculate daily usage and reset replenishment max and min
	  */
	public void setRecalcUsage (String RecalcUsage);

	/** Get RecalcUsage.
	  * Recaculate daily usage and reset replenishment max and min
	  */
	public String getRecalcUsage();

    /** Column name ReplenishType */
    public static final String COLUMNNAME_ReplenishType = "ReplenishType";

	/** Set Replenish Type.
	  * Method for re-ordering a product
	  */
	public void setReplenishType (String ReplenishType);

	/** Get Replenish Type.
	  * Method for re-ordering a product
	  */
	public String getReplenishType();

    /** Column name SafetyStockDaysUsage */
    public static final String COLUMNNAME_SafetyStockDaysUsage = "SafetyStockDaysUsage";

	/** Set SafetyStockDaysUsage.
	  * SafetyStockDaysUsage
	  */
	public void setSafetyStockDaysUsage (BigDecimal SafetyStockDaysUsage);

	/** Get SafetyStockDaysUsage.
	  * SafetyStockDaysUsage
	  */
	public BigDecimal getSafetyStockDaysUsage();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}
