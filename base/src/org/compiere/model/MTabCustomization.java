package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MTabCustomization extends X_AD_Tab_Customization
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -8856837077506056538L;

	public static final int		SUPERUSER			= 100;

	public MTabCustomization(Properties ctx, int AD_Tab_Customization_ID, String trxName)
	{
		super(ctx, AD_Tab_Customization_ID, trxName);
	}

	public MTabCustomization(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}

	public static MTabCustomization get(Properties ctx, int AD_User_ID, int AD_Tab_ID, boolean isQuickEntry, String trxName)
	{
		if (MTable.get(ctx, Table_Name) == null) {
			return  null;
		}
		Query query = new Query(ctx, Table_Name, " AD_User_ID=? AND AD_Tab_ID=? AND IsQuickEntry=?", trxName);
		query.setClient_ID();
		return query.setParameters(new Object[] { AD_User_ID, AD_Tab_ID, (isQuickEntry ? "Y" : "N") }).first();
	}

	/**
	 * Save Tab Customization Data
	 * 
	 * @author Sachin Bhimani
	 * @param ctx
	 * @param AD_Tab_ID
	 * @param AD_User_ID
	 * @param Custom
	 * @param GridView
	 * @param isQuickEntry
	 * @param trxName
	 * @param interval 
	 * @param isRefreshTimer 
	 * @return
	 */
	public static boolean saveData(Properties ctx, int AD_Tab_ID, int AD_User_ID, String Custom, String GridView,
			boolean isQuickEntry, String trxName, boolean isRefreshGridTimer, int interval)
	{
		MTabCustomization tabCust = get(ctx, AD_User_ID, AD_Tab_ID, isQuickEntry, trxName);

		if (tabCust != null && tabCust.getAD_Tab_Customization_ID() > 0)
		{
			tabCust.setcustom(Custom);
			tabCust.setIsDisplayedGrid(GridView);
			tabCust.setIsRefreshGridTimer(isRefreshGridTimer);
			tabCust.setRefreshInterval(interval);
		}
		else
		{
			tabCust = new MTabCustomization(ctx, 0, trxName);
			tabCust.setAD_Tab_ID(AD_Tab_ID);
			tabCust.setAD_User_ID(AD_User_ID);
			tabCust.setcustom(Custom);
			tabCust.setIsDisplayedGrid(GridView);
			tabCust.setisQuickEntry(isQuickEntry);
			tabCust.setIsRefreshGridTimer(isRefreshGridTimer);
			tabCust.setRefreshInterval(interval);
		}

		return tabCust.save();

	} // saveTabCustomization

}
