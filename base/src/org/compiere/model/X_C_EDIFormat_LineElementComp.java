/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for C_EDIFormat_LineElementComp
 *  @author Adempiere (generated) 
 *  @version 1.5.0 - $Id$ */
public class X_C_EDIFormat_LineElementComp extends PO implements I_C_EDIFormat_LineElementComp, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20160701L;

    /** Standard Constructor */
    public X_C_EDIFormat_LineElementComp (Properties ctx, int C_EDIFormat_LineElementComp_ID, String trxName)
    {
      super (ctx, C_EDIFormat_LineElementComp_ID, trxName);
      /** if (C_EDIFormat_LineElementComp_ID == 0)
        {
			setC_EDIFormat_LineElementComp_ID (0);
			setElementType (null);
// F
			setName (null);
			setValue (null);
        } */
    }

    /** Load Constructor */
    public X_C_EDIFormat_LineElementComp (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_EDIFormat_LineElementComp[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_AD_Column getAD_Column() throws RuntimeException
    {
		return (I_AD_Column)MTable.get(getCtx(), I_AD_Column.Table_Name)
			.getPO(getAD_Column_ID(), get_TrxName());	}

	/** Set Column.
		@param AD_Column_ID 
		Column in the table
	  */
	public void setAD_Column_ID (int AD_Column_ID)
	{
		if (AD_Column_ID < 1) 
			set_Value (COLUMNNAME_AD_Column_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Column_ID, Integer.valueOf(AD_Column_ID));
	}

	/** Get Column.
		@return Column in the table
	  */
	public int getAD_Column_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Column_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_AD_Reference getAD_Reference() throws RuntimeException
    {
		return (I_AD_Reference)MTable.get(getCtx(), I_AD_Reference.Table_Name)
			.getPO(getAD_Reference_ID(), get_TrxName());	}

	/** Set Reference.
		@param AD_Reference_ID 
		System Reference and Validation
	  */
	public void setAD_Reference_ID (int AD_Reference_ID)
	{
		throw new IllegalArgumentException ("AD_Reference_ID is virtual column");	}

	/** Get Reference.
		@return System Reference and Validation
	  */
	public int getAD_Reference_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Reference_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set EDI Format Line Element Component.
		@param C_EDIFormat_LineElementComp_ID EDI Format Line Element Component	  */
	public void setC_EDIFormat_LineElementComp_ID (int C_EDIFormat_LineElementComp_ID)
	{
		if (C_EDIFormat_LineElementComp_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDIFormat_LineElementComp_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDIFormat_LineElementComp_ID, Integer.valueOf(C_EDIFormat_LineElementComp_ID));
	}

	/** Get EDI Format Line Element Component.
		@return EDI Format Line Element Component	  */
	public int getC_EDIFormat_LineElementComp_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDIFormat_LineElementComp_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_EDIFormat_LineElement getC_EDIFormat_LineElement() throws RuntimeException
    {
		return (I_C_EDIFormat_LineElement)MTable.get(getCtx(), I_C_EDIFormat_LineElement.Table_Name)
			.getPO(getC_EDIFormat_LineElement_ID(), get_TrxName());	}

	/** Set EDI Format Line Element.
		@param C_EDIFormat_LineElement_ID EDI Format Line Element	  */
	public void setC_EDIFormat_LineElement_ID (int C_EDIFormat_LineElement_ID)
	{
		if (C_EDIFormat_LineElement_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_EDIFormat_LineElement_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_EDIFormat_LineElement_ID, Integer.valueOf(C_EDIFormat_LineElement_ID));
	}

	/** Get EDI Format Line Element.
		@return EDI Format Line Element	  */
	public int getC_EDIFormat_LineElement_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_EDIFormat_LineElement_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Const Value.
		@param ConstValue Const Value	  */
	public void setConstValue (String ConstValue)
	{
		set_Value (COLUMNNAME_ConstValue, ConstValue);
	}

	/** Get Const Value.
		@return Const Value	  */
	public String getConstValue () 
	{
		return (String)get_Value(COLUMNNAME_ConstValue);
	}

	/** Set Date Format.
		@param DateFormat 
		Date format used in the input format
	  */
	public void setDateFormat (String DateFormat)
	{
		set_Value (COLUMNNAME_DateFormat, DateFormat);
	}

	/** Get Date Format.
		@return Date format used in the input format
	  */
	public String getDateFormat () 
	{
		return (String)get_Value(COLUMNNAME_DateFormat);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** ElementType AD_Reference_ID=53455 */
	public static final int ELEMENTTYPE_AD_Reference_ID=53455;
	/** Constant = C */
	public static final String ELEMENTTYPE_Constant = "C";
	/** Field = F */
	public static final String ELEMENTTYPE_Field = "F";
	/** Header Field = H */
	public static final String ELEMENTTYPE_HeaderField = "H";
	/** Variable = V */
	public static final String ELEMENTTYPE_Variable = "V";
	/** Set Type.
		@param ElementType 
		Element Type (account or user defined)
	  */
	public void setElementType (String ElementType)
	{

		set_Value (COLUMNNAME_ElementType, ElementType);
	}

	/** Get Type.
		@return Element Type (account or user defined)
	  */
	public String getElementType () 
	{
		return (String)get_Value(COLUMNNAME_ElementType);
	}

	public I_AD_Column getHeader_Column() throws RuntimeException
    {
		return (I_AD_Column)MTable.get(getCtx(), I_AD_Column.Table_Name)
			.getPO(getHeader_Column_ID(), get_TrxName());	}

	/** Set Header Column.
		@param Header_Column_ID Header Column	  */
	public void setHeader_Column_ID (int Header_Column_ID)
	{
		if (Header_Column_ID < 1) 
			set_Value (COLUMNNAME_Header_Column_ID, null);
		else 
			set_Value (COLUMNNAME_Header_Column_ID, Integer.valueOf(Header_Column_ID));
	}

	/** Get Header Column.
		@return Header Column	  */
	public int getHeader_Column_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Header_Column_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Comment/Help.
		@param Help 
		Comment or Hint
	  */
	public void setHelp (String Help)
	{
		set_Value (COLUMNNAME_Help, Help);
	}

	/** Get Comment/Help.
		@return Comment or Hint
	  */
	public String getHelp () 
	{
		return (String)get_Value(COLUMNNAME_Help);
	}

	/** Set Mandatory.
		@param IsMandatory 
		Data entry is required in this column
	  */
	public void setIsMandatory (boolean IsMandatory)
	{
		set_Value (COLUMNNAME_IsMandatory, Boolean.valueOf(IsMandatory));
	}

	/** Get Mandatory.
		@return Data entry is required in this column
	  */
	public boolean isMandatory () 
	{
		Object oo = get_Value(COLUMNNAME_IsMandatory);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Lookup Column.
		@param LookupColumn Lookup Column	  */
	public void setLookupColumn (String LookupColumn)
	{
		set_Value (COLUMNNAME_LookupColumn, LookupColumn);
	}

	/** Get Lookup Column.
		@return Lookup Column	  */
	public String getLookupColumn () 
	{
		return (String)get_Value(COLUMNNAME_LookupColumn);
	}

	/** Set MinFieldLength.
		@param MinFieldLength MinFieldLength	  */
	public void setMinFieldLength (int MinFieldLength)
	{
		set_Value (COLUMNNAME_MinFieldLength, Integer.valueOf(MinFieldLength));
	}

	/** Get MinFieldLength.
		@return MinFieldLength	  */
	public int getMinFieldLength () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_MinFieldLength);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Position.
		@param Position Position	  */
	public void setPosition (int Position)
	{
		set_Value (COLUMNNAME_Position, Integer.valueOf(Position));
	}

	/** Get Position.
		@return Position	  */
	public int getPosition () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Position);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getValue());
    }

	/** Set VariableValue.
		@param VariableValue VariableValue	  */
	public void setVariableValue (String VariableValue)
	{
		set_Value (COLUMNNAME_VariableValue, VariableValue);
	}

	/** Get VariableValue.
		@return VariableValue	  */
	public String getVariableValue () 
	{
		return (String)get_Value(COLUMNNAME_VariableValue);
	}
}