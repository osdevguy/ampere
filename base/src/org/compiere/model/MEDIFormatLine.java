/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Trifon Trifonov.                                     * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Trifon Trifonov (trifonnt@users.sourceforge.net)                *
 *                                                                    *
 * Sponsors:                                                          *
 *  - Idalica (http://www.idalica.com/)                               *
 **********************************************************************/

package org.compiere.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.CLogger;
import org.compiere.util.DB;

public class MEDIFormatLine extends X_C_EDIFormat_Line {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**	Static Logger	*/
	private static CLogger	s_log	= CLogger.getCLogger (X_C_EDIFormat_Line.class);
	
	
	public MEDIFormatLine(Properties ctx, int C_EDIFormat_Line_ID, String trxName) {
		super(ctx, C_EDIFormat_Line_ID, trxName);
	}
	
	public MEDIFormatLine (Properties ctx, ResultSet rs, String trxName) {
		super (ctx, rs, trxName);
	}
	
	public MEDIFormatLineElement[] getEDIFormat_LineElements(String trxName) {
		List<MEDIFormatLineElement> resultList = new ArrayList<MEDIFormatLineElement>();
		                   
		StringBuffer sql = new StringBuffer("SELECT * ")
			.append(" FROM ").append(MEDIFormatLineElement.Table_Name)
			.append(" WHERE ").append(MEDIFormatLineElement.COLUMNNAME_C_EDIFormat_Line_ID).append("=?")
			.append(" AND IsActive=?")
			.append(" ORDER BY ").append(MEDIFormatLineElement.COLUMNNAME_Position);
		PreparedStatement pstmt = null;
		MEDIFormatLineElement ediLine = null;
		try {
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			pstmt.setInt(1, getC_EDIFormat_Line_ID());
			pstmt.setString(2, "Y");
			ResultSet rs = pstmt.executeQuery ();
			while ( rs.next() ) {
				ediLine = new MEDIFormatLineElement (getCtx(), rs, trxName);
				resultList.add(ediLine);
			}
			rs.close ();
			pstmt.close ();
			pstmt = null;
		} catch (SQLException e) {
			s_log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			try	{
				if (pstmt != null) pstmt.close ();
				pstmt = null;
			} catch (Exception e) {	pstmt = null; }
		}
		
		MEDIFormatLineElement[] result = (MEDIFormatLineElement[])resultList.toArray( new MEDIFormatLineElement[0]);
		return result;
	}
	
	/**
	 * 	Copy EDI Format Lines From other EDI Format
	 *	@param from source of EDI Format
	 *	@param to   target of EDI Format
	 *	@return number of lines copied
	 */
	public int copyLinesElementFrom (MEDIFormatLine from)
	{
		if (from == null)
			return 0;
		MEDIFormatLineElement[] linesElement = from.getEDIFormat_LineElements(get_TrxName());
	
		int count = 0;
		
		for (MEDIFormatLineElement fromLineElement : linesElement){
			MEDIFormatLineElement toLineElement = new MEDIFormatLineElement(getCtx(), 0, get_TrxName());
			PO.copyValues(fromLineElement, toLineElement, getAD_Client_ID(), getAD_Org_ID());
			toLineElement.setC_EDIFormat_Line_ID(get_ID());
			
			if (toLineElement.save(get_TrxName()))
				count++;
			else
				log.log(Level.SEVERE, "Record not saved!"+toLineElement);
			
			//copy EDI Lines Element
			toLineElement.copyLinesElementCompFrom(fromLineElement);
		}
		if (linesElement.length != count)
			log.log(Level.SEVERE, "Line difference - From=" + linesElement.length + " <> Saved=" + count);
		
		return count;
	}	//	copyLinesFrom

	@Override
	public String toString() {
		
		return "EDI Format Line: " + getValue() + " - " + getName() + "; Type: " + getType();
	}
	
}
