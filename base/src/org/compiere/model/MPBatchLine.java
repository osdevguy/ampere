package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MPBatchLine extends X_M_PBatch_Line
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8836637198723346713L;

	public MPBatchLine(Properties ctx, int M_PBatch_Line_ID, String trxName)
	{
		super(ctx, M_PBatch_Line_ID, trxName);
	}

	public MPBatchLine(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}

}
