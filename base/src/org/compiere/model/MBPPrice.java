/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.CLogger;
import org.compiere.util.Env;

/**
 *	Business partner Price override
 *	
 *  @author Paul Bowden
 */
public class MBPPrice extends X_M_BP_Price
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9187555438223385521L;

	
	/**	Logger	*/
	private static CLogger s_log = CLogger.getCLogger (MBPPrice.class);
	
	/**
	 * 	Persistency Constructor
	 *	@param ctx context
	 *	@param ignored ignored
	 *	@param trxName transaction
	 */
	public MBPPrice (Properties ctx, int M_BP_Price_ID, String trxName)
	{
		super(ctx, M_BP_Price_ID, trxName);

		if (M_BP_Price_ID == 0) {
			setPriceLimit (Env.ZERO);
			setPriceList (Env.ZERO);
			setPriceStd (Env.ZERO);
			setDiscount(Env.ZERO);
		}
		
	}	//	MBPPrice

	/**
	 * 	Load Constructor
	 *	@param ctx context
	 *	@param rs result set
	 *	@param trxName transaction
	 */
	public MBPPrice (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MBPPrice

	/**
	 * 	String Representation
	 *	@return info
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer ("MBPPrice[");
		sb.append("C_BPartner_ID="+getC_BPartner_ID())
			.append(",M_Product_ID=").append (getM_Product_ID())
			.append("]");
		return sb.toString ();
	} //	toString
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		if (PRICEOVERRIDETYPE_Discount.equals(getPriceOverrideType()))
		{
			setPriceStd(Env.ZERO);
			setPriceLimit(Env.ZERO);
			setPriceList(Env.ZERO);
			setIsNetPrice(false);
		}
		else
		{
			setDiscount(Env.ZERO);
		}
		
		return super.beforeSave(newRecord);
	}
	
}	//	MBPPrice
