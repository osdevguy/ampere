/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for GAAS_Asset_Group_Acct
 *  @author Adempiere (generated) 
 *  @version 1.03 - $Id$ */
public class X_GAAS_Asset_Group_Acct extends PO implements I_GAAS_Asset_Group_Acct, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20131008L;

    /** Standard Constructor */
    public X_GAAS_Asset_Group_Acct (Properties ctx, int GAAS_Asset_Group_Acct_ID, String trxName)
    {
      super (ctx, GAAS_Asset_Group_Acct_ID, trxName);
      /** if (GAAS_Asset_Group_Acct_ID == 0)
        {
			setA_Asset_Group_ID (0);
			setC_AcctSchema_ID (0);
			setDepreciationRate (Env.ZERO);
// 0.2
			setGAAS_Asset_Group_Acct_ID (0);
			setPostingType (null);
// A
        } */
    }

    /** Load Constructor */
    public X_GAAS_Asset_Group_Acct (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_GAAS_Asset_Group_Acct[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_A_Asset_Group getA_Asset_Group() throws RuntimeException
    {
		return (I_A_Asset_Group)MTable.get(getCtx(), I_A_Asset_Group.Table_Name)
			.getPO(getA_Asset_Group_ID(), get_TrxName());	}

	/** Set Asset Group.
		@param A_Asset_Group_ID 
		Group of Assets
	  */
	public void setA_Asset_Group_ID (int A_Asset_Group_ID)
	{
		if (A_Asset_Group_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_A_Asset_Group_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_A_Asset_Group_ID, Integer.valueOf(A_Asset_Group_ID));
	}

	/** Get Asset Group.
		@return Group of Assets
	  */
	public int getA_Asset_Group_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_Group_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_AcctSchema getC_AcctSchema() throws RuntimeException
    {
		return (I_C_AcctSchema)MTable.get(getCtx(), I_C_AcctSchema.Table_Name)
			.getPO(getC_AcctSchema_ID(), get_TrxName());	}

	/** Set Accounting Schema.
		@param C_AcctSchema_ID 
		Rules for accounting
	  */
	public void setC_AcctSchema_ID (int C_AcctSchema_ID)
	{
		if (C_AcctSchema_ID < 1) 
			set_Value (COLUMNNAME_C_AcctSchema_ID, null);
		else 
			set_Value (COLUMNNAME_C_AcctSchema_ID, Integer.valueOf(C_AcctSchema_ID));
	}

	/** Get Accounting Schema.
		@return Rules for accounting
	  */
	public int getC_AcctSchema_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_AcctSchema_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Default_Depn_Periods.
		@param Default_Depn_Periods 
		Default Depn Periods
	  */
	public void setDefault_Depn_Periods (BigDecimal Default_Depn_Periods)
	{
		set_Value (COLUMNNAME_Default_Depn_Periods, Default_Depn_Periods);
	}

	/** Get Default_Depn_Periods.
		@return Default Depn Periods
	  */
	public BigDecimal getDefault_Depn_Periods () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Default_Depn_Periods);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Depreciation Rate.
		@param DepreciationRate Depreciation Rate	  */
	public void setDepreciationRate (BigDecimal DepreciationRate)
	{
		set_Value (COLUMNNAME_DepreciationRate, DepreciationRate);
	}

	/** Get Depreciation Rate.
		@return Depreciation Rate	  */
	public BigDecimal getDepreciationRate () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DepreciationRate);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_ValidCombination getGAAS_Accum_Depr_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Accum_Depr_Acct(), get_TrxName());	}

	/** Set Accumulated Depreciation.
		@param GAAS_Accum_Depr_Acct Accumulated Depreciation	  */
	public void setGAAS_Accum_Depr_Acct (int GAAS_Accum_Depr_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Accum_Depr_Acct, Integer.valueOf(GAAS_Accum_Depr_Acct));
	}

	/** Get Accumulated Depreciation.
		@return Accumulated Depreciation	  */
	public int getGAAS_Accum_Depr_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Accum_Depr_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_Accum_Depr_OffSet_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Accum_Depr_OffSet_Acct(), get_TrxName());	}

	/** Set Accumulated Depr OffSet.
		@param GAAS_Accum_Depr_OffSet_Acct Accumulated Depr OffSet	  */
	public void setGAAS_Accum_Depr_OffSet_Acct (int GAAS_Accum_Depr_OffSet_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Accum_Depr_OffSet_Acct, Integer.valueOf(GAAS_Accum_Depr_OffSet_Acct));
	}

	/** Get Accumulated Depr OffSet.
		@return Accumulated Depr OffSet	  */
	public int getGAAS_Accum_Depr_OffSet_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Accum_Depr_OffSet_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_Asset_Cost_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Asset_Cost_Acct(), get_TrxName());	}

	/** Set Asset Cost.
		@param GAAS_Asset_Cost_Acct Asset Cost	  */
	public void setGAAS_Asset_Cost_Acct (int GAAS_Asset_Cost_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Asset_Cost_Acct, Integer.valueOf(GAAS_Asset_Cost_Acct));
	}

	/** Get Asset Cost.
		@return Asset Cost	  */
	public int getGAAS_Asset_Cost_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Asset_Cost_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Asset Group Accounting.
		@param GAAS_Asset_Group_Acct_ID Asset Group Accounting	  */
	public void setGAAS_Asset_Group_Acct_ID (int GAAS_Asset_Group_Acct_ID)
	{
		if (GAAS_Asset_Group_Acct_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_GAAS_Asset_Group_Acct_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_GAAS_Asset_Group_Acct_ID, Integer.valueOf(GAAS_Asset_Group_Acct_ID));
	}

	/** Get Asset Group Accounting.
		@return Asset Group Accounting	  */
	public int getGAAS_Asset_Group_Acct_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Asset_Group_Acct_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_Depreciation_Expense_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Depreciation_Expense_Acct(), get_TrxName());	}

	/** Set Depreciation Expense.
		@param GAAS_Depreciation_Expense_Acct Depreciation Expense	  */
	public void setGAAS_Depreciation_Expense_Acct (int GAAS_Depreciation_Expense_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Depreciation_Expense_Acct, Integer.valueOf(GAAS_Depreciation_Expense_Acct));
	}

	/** Get Depreciation Expense.
		@return Depreciation Expense	  */
	public int getGAAS_Depreciation_Expense_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Depreciation_Expense_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** GAAS_Depreciation_Type AD_Reference_ID=53551 */
	public static final int GAAS_DEPRECIATION_TYPE_AD_Reference_ID=53551;
	/** Straight Line = SL */
	public static final String GAAS_DEPRECIATION_TYPE_StraightLine = "SL";
	/** Reducing Balance = RB */
	public static final String GAAS_DEPRECIATION_TYPE_ReducingBalance = "RB";
	/** Set Depreciation Method.
		@param GAAS_Depreciation_Type Depreciation Method	  */
	public void setGAAS_Depreciation_Type (String GAAS_Depreciation_Type)
	{

		set_Value (COLUMNNAME_GAAS_Depreciation_Type, GAAS_Depreciation_Type);
	}

	/** Get Depreciation Method.
		@return Depreciation Method	  */
	public String getGAAS_Depreciation_Type () 
	{
		return (String)get_Value(COLUMNNAME_GAAS_Depreciation_Type);
	}

	public I_C_ValidCombination getGAAS_Disposal_Gain_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Disposal_Gain_Acct(), get_TrxName());	}

	/** Set Disposal Gain.
		@param GAAS_Disposal_Gain_Acct Disposal Gain	  */
	public void setGAAS_Disposal_Gain_Acct (int GAAS_Disposal_Gain_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Disposal_Gain_Acct, Integer.valueOf(GAAS_Disposal_Gain_Acct));
	}

	/** Get Disposal Gain.
		@return Disposal Gain	  */
	public int getGAAS_Disposal_Gain_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Disposal_Gain_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_Disposal_Loss_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Disposal_Loss_Acct(), get_TrxName());	}

	/** Set Disposal Loss.
		@param GAAS_Disposal_Loss_Acct Disposal Loss	  */
	public void setGAAS_Disposal_Loss_Acct (int GAAS_Disposal_Loss_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Disposal_Loss_Acct, Integer.valueOf(GAAS_Disposal_Loss_Acct));
	}

	/** Get Disposal Loss.
		@return Disposal Loss	  */
	public int getGAAS_Disposal_Loss_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Disposal_Loss_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_InTransfer_Incoming_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_InTransfer_Incoming_Acct(), get_TrxName());	}

	/** Set In Transfer Incoming.
		@param GAAS_InTransfer_Incoming_Acct In Transfer Incoming	  */
	public void setGAAS_InTransfer_Incoming_Acct (int GAAS_InTransfer_Incoming_Acct)
	{
		set_Value (COLUMNNAME_GAAS_InTransfer_Incoming_Acct, Integer.valueOf(GAAS_InTransfer_Incoming_Acct));
	}

	/** Get In Transfer Incoming.
		@return In Transfer Incoming	  */
	public int getGAAS_InTransfer_Incoming_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_InTransfer_Incoming_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_InTransfer_Outgoing_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_InTransfer_Outgoing_Acct(), get_TrxName());	}

	/** Set In Transfer Outgoing.
		@param GAAS_InTransfer_Outgoing_Acct In Transfer Outgoing	  */
	public void setGAAS_InTransfer_Outgoing_Acct (int GAAS_InTransfer_Outgoing_Acct)
	{
		set_Value (COLUMNNAME_GAAS_InTransfer_Outgoing_Acct, Integer.valueOf(GAAS_InTransfer_Outgoing_Acct));
	}

	/** Get In Transfer Outgoing.
		@return In Transfer Outgoing	  */
	public int getGAAS_InTransfer_Outgoing_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_InTransfer_Outgoing_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_Revaluation_Expense_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Revaluation_Expense_Acct(), get_TrxName());	}

	/** Set Revaluation Expense.
		@param GAAS_Revaluation_Expense_Acct Revaluation Expense	  */
	public void setGAAS_Revaluation_Expense_Acct (int GAAS_Revaluation_Expense_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Revaluation_Expense_Acct, Integer.valueOf(GAAS_Revaluation_Expense_Acct));
	}

	/** Get Revaluation Expense.
		@return Revaluation Expense	  */
	public int getGAAS_Revaluation_Expense_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Revaluation_Expense_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_Revaluation_Gain_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Revaluation_Gain_Acct(), get_TrxName());	}

	/** Set Revaluation Gain.
		@param GAAS_Revaluation_Gain_Acct Revaluation Gain	  */
	public void setGAAS_Revaluation_Gain_Acct (int GAAS_Revaluation_Gain_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Revaluation_Gain_Acct, Integer.valueOf(GAAS_Revaluation_Gain_Acct));
	}

	/** Get Revaluation Gain.
		@return Revaluation Gain	  */
	public int getGAAS_Revaluation_Gain_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Revaluation_Gain_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_Revaluation_Loss_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Revaluation_Loss_Acct(), get_TrxName());	}

	/** Set Revaluation Loss.
		@param GAAS_Revaluation_Loss_Acct Revaluation Loss	  */
	public void setGAAS_Revaluation_Loss_Acct (int GAAS_Revaluation_Loss_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Revaluation_Loss_Acct, Integer.valueOf(GAAS_Revaluation_Loss_Acct));
	}

	/** Get Revaluation Loss.
		@return Revaluation Loss	  */
	public int getGAAS_Revaluation_Loss_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Revaluation_Loss_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_Revaluation_OffSet_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Revaluation_OffSet_Acct(), get_TrxName());	}

	/** Set Revaluation OffSet.
		@param GAAS_Revaluation_OffSet_Acct Revaluation OffSet	  */
	public void setGAAS_Revaluation_OffSet_Acct (int GAAS_Revaluation_OffSet_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Revaluation_OffSet_Acct, Integer.valueOf(GAAS_Revaluation_OffSet_Acct));
	}

	/** Get Revaluation OffSet.
		@return Revaluation OffSet	  */
	public int getGAAS_Revaluation_OffSet_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Revaluation_OffSet_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_Revaluation_Reserve_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Revaluation_Reserve_Acct(), get_TrxName());	}

	/** Set Revaluation Reserve.
		@param GAAS_Revaluation_Reserve_Acct Revaluation Reserve	  */
	public void setGAAS_Revaluation_Reserve_Acct (int GAAS_Revaluation_Reserve_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Revaluation_Reserve_Acct, Integer.valueOf(GAAS_Revaluation_Reserve_Acct));
	}

	/** Get Revaluation Reserve.
		@return Revaluation Reserve	  */
	public int getGAAS_Revaluation_Reserve_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Revaluation_Reserve_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_ValidCombination getGAAS_Unidentified_Add_A() throws RuntimeException
    {
		return (I_C_ValidCombination)MTable.get(getCtx(), I_C_ValidCombination.Table_Name)
			.getPO(getGAAS_Unidentified_Add_Acct(), get_TrxName());	}

	/** Set Unidentified Additions.
		@param GAAS_Unidentified_Add_Acct Unidentified Additions	  */
	public void setGAAS_Unidentified_Add_Acct (int GAAS_Unidentified_Add_Acct)
	{
		set_Value (COLUMNNAME_GAAS_Unidentified_Add_Acct, Integer.valueOf(GAAS_Unidentified_Add_Acct));
	}

	/** Get Unidentified Additions.
		@return Unidentified Additions	  */
	public int getGAAS_Unidentified_Add_Acct () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_Unidentified_Add_Acct);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** PostingType AD_Reference_ID=125 */
	public static final int POSTINGTYPE_AD_Reference_ID=125;
	/** Actual = A */
	public static final String POSTINGTYPE_Actual = "A";
	/** Budget = B */
	public static final String POSTINGTYPE_Budget = "B";
	/** Commitment = E */
	public static final String POSTINGTYPE_Commitment = "E";
	/** Statistical = S */
	public static final String POSTINGTYPE_Statistical = "S";
	/** Reservation = R */
	public static final String POSTINGTYPE_Reservation = "R";
	/** Set PostingType.
		@param PostingType 
		The type of posted amount for the transaction
	  */
	public void setPostingType (String PostingType)
	{

		set_Value (COLUMNNAME_PostingType, PostingType);
	}

	/** Get PostingType.
		@return The type of posted amount for the transaction
	  */
	public String getPostingType () 
	{
		return (String)get_Value(COLUMNNAME_PostingType);
	}
}