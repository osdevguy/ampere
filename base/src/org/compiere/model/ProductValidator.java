package org.compiere.model;
/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/


import org.compiere.process.CreateDefaultProductReplenishment;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;


/**
 *	Validator for Product Replenishment
 *	
 *  @author Jobrian  Trinidad www.adaxa.com.au
 */
public class ProductValidator implements ModelValidator
{
	public ProductValidator ()
	{
		super ();
	}
	
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(ProductValidator.class);
	/** Client			*/
	private int		m_AD_Client_ID = -1;
	private int 		m_Client_ProductTemplate_ID = 0;
	
	/**
	 *	Initialize Validation
	 *	@param engine validation engine 
	 *	@param client client
	 */
	public void initialize (ModelValidationEngine engine, MClient client)
	{
		//client = null for global validator
		if (client != null) {	
			m_AD_Client_ID = client.getAD_Client_ID();
			log.info(client.toString());
		}
		else  {
			log.info("Initializing global validator: "+this.toString());
		}

		//	Tables to be monitored
		engine.addModelChange(MProduct.Table_Name, this);
		
		//	Documents to be monitored
	//	engine.addDocValidate(MInvoice.Table_Name, this);

	}	//	initialize

    /**
     *	Model Change of a monitored Table.
     *	Called after PO.beforeSave/PO.beforeDelete
     *	when you called addModelChange for the table
     *	@param po persistent object
     *	@param type TYPE_
     *	@return error message or null
     *	@exception Exception if the recipient wishes the change to be not accept.
     */
	public String modelChange (PO po, int type) throws Exception
	{
		if ( po == null ) {
			return null;
			
		}
		log.info(po.get_TableName() + " Type: "+type);


		if (po.get_TableName().equals(MProduct.Table_Name) &&
				(type == ModelValidator.TYPE_AFTER_NEW || type == ModelValidator.TYPE_AFTER_CHANGE ) )
		{
			
			if (m_Client_ProductTemplate_ID == 0 && po.getAD_Org_ID() != 0) {
				MOrgInfo oi = MOrgInfo.get(po.getCtx(), po.getAD_Org_ID(), po.get_TrxName());
				m_Client_ProductTemplate_ID = oi.get_ValueAsInt("m_producttemplate_id");

			}
			if (m_Client_ProductTemplate_ID == 0) {
				MClientInfo ci = MClientInfo.get(po.getCtx(), po.getAD_Client_ID(), po.get_TrxName());
				m_Client_ProductTemplate_ID = ci.get_ValueAsInt("m_producttemplate_id");
			}
			

			MProduct product = (MProduct) po;
			MProductCategory pc = (MProductCategory) product.getM_Product_Category();
			int category_Product_ID = pc.get_ValueAsInt("m_producttemplate_id");
			if (category_Product_ID > 0) {
				CreateDefaultProductReplenishment.setProductTemplate(category_Product_ID);
			}
			else {
				CreateDefaultProductReplenishment.setProductTemplate(m_Client_ProductTemplate_ID);
			}
			createDefaultReplenishment(product);
		}
		

		return null;
	}	//	modelChange

	private void createDefaultReplenishment(MProduct product) {
		try {
			CreateDefaultProductReplenishment.resetProduct(product.getM_Product_ID(), product.getValue(), product.get_TrxName());
		} catch (AdempiereUserError e) {
			log.severe(e.getMessage());
		}
	}
	
	/**
	 *	Validate Document.
	 *	Called as first step of DocAction.prepareIt 
     *	when you called addDocValidate for the table.
     *	Note that totals, etc. may not be correct.
	 *	@param po persistent object
	 *	@param timing see TIMING_ constants
     *	@return error message or null
	 */
	public String docValidate (PO po, int timing)
	{
		return null;
	}	//	docValidate

/**
 *	User Login.
	 *	Called when preferences are set
	 *	@param AD_Org_ID org
	 *	@param AD_Role_ID role
	 *	@param AD_User_ID user
	 *	@return error message or null
	 */
	public String login (int AD_Org_ID, int AD_Role_ID, int AD_User_ID)
	{
		log.info("AD_User_ID=" + AD_User_ID);
		return null;
	}	//	login

	
	/**
	 *	Get Client to be monitored
	 *	@return AD_Client_ID client
	 */
	public int getAD_Client_ID()
	{
		return m_AD_Client_ID;
	}	//	getAD_Client_ID

	
	/**
	 * 	String Representation
	 *	@return info
	 */
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ("ProductValidator");
		return sb.toString ();
	}	//	toString
	
}	//	Validator