/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for GAAS_AssetJournalLine_Det
 *  @author Adempiere (generated) 
 *  @version 1.03 - $Id$ */
public class X_GAAS_AssetJournalLine_Det extends PO implements I_GAAS_AssetJournalLine_Det, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20131008L;

    /** Standard Constructor */
    public X_GAAS_AssetJournalLine_Det (Properties ctx, int GAAS_AssetJournalLine_Det_ID, String trxName)
    {
      super (ctx, GAAS_AssetJournalLine_Det_ID, trxName);
      /** if (GAAS_AssetJournalLine_Det_ID == 0)
        {
			setCostAmt (Env.ZERO);
			setGAAS_AssetJournalLine_Det_ID (0);
			setGAAS_AssetJournalLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_GAAS_AssetJournalLine_Det (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_GAAS_AssetJournalLine_Det[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_A_Asset getA_Asset() throws RuntimeException
    {
		return (I_A_Asset)MTable.get(getCtx(), I_A_Asset.Table_Name)
			.getPO(getA_Asset_ID(), get_TrxName());	}

	/** Set Asset.
		@param A_Asset_ID 
		Asset used internally or by customers
	  */
	public void setA_Asset_ID (int A_Asset_ID)
	{
		if (A_Asset_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_ID, Integer.valueOf(A_Asset_ID));
	}

	/** Get Asset.
		@return Asset used internally or by customers
	  */
	public int getA_Asset_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Accumulated Depreciation Amount.
		@param AccumulatedDepreciationAmt Accumulated Depreciation Amount	  */
	public void setAccumulatedDepreciationAmt (BigDecimal AccumulatedDepreciationAmt)
	{
		set_Value (COLUMNNAME_AccumulatedDepreciationAmt, AccumulatedDepreciationAmt);
	}

	/** Get Accumulated Depreciation Amount.
		@return Accumulated Depreciation Amount	  */
	public BigDecimal getAccumulatedDepreciationAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_AccumulatedDepreciationAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException
    {
		return (I_C_InvoiceLine)MTable.get(getCtx(), I_C_InvoiceLine.Table_Name)
			.getPO(getC_InvoiceLine_ID(), get_TrxName());	}

	/** Set Invoice Line.
		@param C_InvoiceLine_ID 
		Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID)
	{
		if (C_InvoiceLine_ID < 1) 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, null);
		else 
			set_Value (COLUMNNAME_C_InvoiceLine_ID, Integer.valueOf(C_InvoiceLine_ID));
	}

	/** Get Invoice Line.
		@return Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_InvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cost Value.
		@param CostAmt 
		Value with Cost
	  */
	public void setCostAmt (BigDecimal CostAmt)
	{
		set_Value (COLUMNNAME_CostAmt, CostAmt);
	}

	/** Get Cost Value.
		@return Value with Cost
	  */
	public BigDecimal getCostAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CostAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Asset Journal Line Detail.
		@param GAAS_AssetJournalLine_Det_ID Asset Journal Line Detail	  */
	public void setGAAS_AssetJournalLine_Det_ID (int GAAS_AssetJournalLine_Det_ID)
	{
		if (GAAS_AssetJournalLine_Det_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_GAAS_AssetJournalLine_Det_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_GAAS_AssetJournalLine_Det_ID, Integer.valueOf(GAAS_AssetJournalLine_Det_ID));
	}

	/** Get Asset Journal Line Detail.
		@return Asset Journal Line Detail	  */
	public int getGAAS_AssetJournalLine_Det_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_AssetJournalLine_Det_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_GAAS_AssetJournalLine getGAAS_AssetJournalLine() throws RuntimeException
    {
		return (I_GAAS_AssetJournalLine)MTable.get(getCtx(), I_GAAS_AssetJournalLine.Table_Name)
			.getPO(getGAAS_AssetJournalLine_ID(), get_TrxName());	}

	/** Set Asset Journal Line.
		@param GAAS_AssetJournalLine_ID Asset Journal Line	  */
	public void setGAAS_AssetJournalLine_ID (int GAAS_AssetJournalLine_ID)
	{
		if (GAAS_AssetJournalLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_GAAS_AssetJournalLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_GAAS_AssetJournalLine_ID, Integer.valueOf(GAAS_AssetJournalLine_ID));
	}

	/** Get Asset Journal Line.
		@return Asset Journal Line	  */
	public int getGAAS_AssetJournalLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_GAAS_AssetJournalLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Posted Amount.
		@param PostedAmt Posted Amount	  */
	public void setPostedAmt (BigDecimal PostedAmt)
	{
		throw new IllegalArgumentException ("PostedAmt is virtual column");	}

	/** Get Posted Amount.
		@return Posted Amount	  */
	public BigDecimal getPostedAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PostedAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}