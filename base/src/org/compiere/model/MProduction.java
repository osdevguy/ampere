package org.compiere.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.PeriodClosedException;
import org.compiere.process.DocAction;
import org.compiere.process.DocumentEngine;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.Util;

public class MProduction extends X_M_Production implements DocAction {

	/**
	 * 
	 */
	/** Log								*/
	private static CLogger		m_log = CLogger.getCLogger (MProduction.class);
	private static final long serialVersionUID = 1L;
	private int lineno;
	private int count;

	public static final String	SQL_GET_DOCTYPE			= "SELECT C_DocType_ID FROM C_DocType WHERE Name LIKE ? AND AD_Client_ID = ?";
	public static final String	SQL_GET_BOM_INFO		= "SELECT M_ProductBom_ID, BOMQty FROM M_Product_BOM	WHERE M_Product_ID=? ORDER BY Line";
	public static final String	SQL_RETRIEVE_SUBCOMPONENT_INFO	= "SELECT b.M_ProductBom_ID, BOMQty, BOMQty * ? ::Numeric AS RequiredQty, p.ProductType='I' AS IsProductTypeItem, p.IsBom, p.IsPhantom "
			+ " FROM M_Product_BOM b	INNER JOIN M_Product p ON (p.M_Product_ID = b.M_ProductBom_ID) "
			+ " WHERE b.M_Product_ID = ? ORDER BY b.Line";
	
	private HashMap<Integer, BigDecimal>	mapReservedQtyInfo			= null;
	
	private int					docType_PlannedOrder	= 0;
	

	
	/**	Process Message 			*/
	private String		m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean		m_justPrepared = false;
	private boolean bStopRollingProduction;
	
	public MProduction(Properties ctx, int M_Production_ID, String trxName) {
		super(ctx, M_Production_ID, trxName);
		if (M_Production_ID == 0) {
			setDocStatus(DOCSTATUS_Drafted);
			setDocAction (DOCACTION_Prepare);
		}
	}

	public MProduction(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	public MProduction( MOrderLine line ) {
		super( line.getCtx(), 0, line.get_TrxName());
		setAD_Client_ID(line.getAD_Client_ID());
		setAD_Org_ID(line.getAD_Org_ID());
		setMovementDate( line.getDatePromised() );
	}

	public String automaticProduction(MInOutLine line) {
		MInOut move = new MInOut(line.getCtx(),line.getM_InOut_ID(), line.get_TrxName());
		
		setAD_Client_ID(line.getAD_Client_ID());
		setAD_Org_ID(line.getAD_Org_ID());
		setC_Activity_ID(move.getC_Activity_ID());
		setMovementDate(move.getMovementDate());
		setName(move.getDocumentNo());
		setM_Locator_ID(line.getM_Locator_ID());
		setM_Product_ID(line.getM_Product_ID());
		setProductionQty(line.getMovementQty());
		saveEx();
		createLines(true);
		try {
			completeIt(getMovementDate(), true);
		} catch (AdempiereSystemError e) {
			e.printStackTrace();
		}
		
		return ("@M_Production_ID@ = " + getName());	
	}
	
	@Override
	public String completeIt()
	{
		// Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		String batchStatus = checkProductionBatchDocStatus();
		if (!Util.isEmpty(batchStatus, true))
			throw new AdempiereException(batchStatus);
		
		StringBuilder errors = new StringBuilder();
		int processed = 0;

		MProductionLine[] lines = getLines();
		
		for (MProductionLine line : lines )
		{
				MProduct product = (MProduct) line.getM_Product();
				MAttributeSet as = product.getAttributeSet();
				if ( as != null )
				{
					if ( (as.isMandatoryAlways() ||
							(as.isSerNo() && as.isSerNoMandatory()) ||
							(as.isLot() && as.isLotMandatory())) 
							&& line.getM_AttributeSetInstance_ID() == 0 )
					{
						errors.append("@M_AttributeSet_ID@ @IsMandatory@ (@Line@ #" + line.getLine() +
						", @M_Product_ID@=" + product.getValue() + ")\n");
					}
				}
		}
		if (errors.length() > 0)
		{
			m_processMsg = errors.toString();
			return DocAction.STATUS_Invalid;
		}
		MProductionBatch batch = (MProductionBatch) getM_Production_Batch();
		MMovement[] moves = batch.getMMovements(true);
		for (MMovement move : moves) {
			if (!move.isProcessed()) {
				errors.append("Inventory Move=" + move.getDocumentNo() + " is not complete.");
			}
		}
		
		errors.append(processLines(lines));
		if (errors.length() > 0)
		{
			m_processMsg = errors.toString();
			return DocAction.STATUS_Invalid;
		}
		processed = processed + lines.length;

		checkStockReservationCorrection();
		
		batch.setQtyReserved(batch.getQtyReserved().subtract(getProductionQty()));
		batch.setQtyCompleted(batch.getQtyCompleted().add(getProductionQty()));
		batch.saveEx(get_TrxName());
		
		// User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		setProcessed(true);
		setDocAction(DOCACTION_Close);
		save();
		//create another production to complete the target qty
		BigDecimal qty = batch.getTargetQty().subtract(batch.getQtyCompleted());
		bStopRollingProduction = MSysConfig.getBooleanValue("M_PRODUCTION_STOP_ROLLING", false, Env.getAD_Client_ID(getCtx()));
		if (bStopRollingProduction && qty.compareTo(Env.ZERO) == 0) {
			return DocAction.STATUS_Completed;
		}
		MProduction newMPO = batch.createProductionHeader(qty.compareTo(Env.ZERO) > 0? qty: Env.ONE);
		if (newMPO != null) {
			if (newMPO.createLines(false) > 0) {
				int[] asi = ((MProductionBatch)newMPO.getM_Production_Batch()).getASIPools();
				newMPO.reAssignASI(asi);
				newMPO.setIsCreated(true);
				newMPO.set_CustomColumn("filling_line", batch.get_ValueAsString("filling_line"));
				newMPO.saveEx();
			}
		}
		return DocAction.STATUS_Completed;
	}
	
	private Object processLines(MProductionLine[] lines)
	{
		StringBuilder errors = new StringBuilder();
		for (int i = 0; i < lines.length; i++)
		{
			String error = lines[i].createTransactions(getMovementDate(), false);
			if (!Util.isEmpty(error))
			{
				errors.append(error);
			}
			else
			{
				lines[i].setProcessed(true);
				lines[i].saveEx(get_TrxName());
			}
		}

		return errors.toString();
	}
	
	public String completeIt(Timestamp movedate, boolean mustBeStocked) throws AdempiereSystemError {
		
		int processed = 0;
		setMovementDate(movedate);
		MProductionLine[] lines = getLines();
		StringBuffer errors = new StringBuffer();
		for ( int i = 0; i<lines.length; i++) {
			errors.append( lines[i].createTransactions(getMovementDate(), mustBeStocked) );
			lines[i].setProcessed( true );
			lines[i].saveEx(get_TrxName());
			processed++;
		}
		
		if ( errors.toString().compareTo("") != 0 ) {
			log.log(Level.WARNING, errors.toString() );
			throw new AdempiereSystemError(errors.toString());
		}
		
		setProcessed(true);
		
		saveEx(get_TrxName());
		
		checkStockReservationCorrection();
		
		return processed + " production lines were processed";

	}


	public MProductionLine[] getLines() {
		ArrayList<MProductionLine> list = new ArrayList<MProductionLine>();
		
		String sql = "SELECT pl.M_ProductionLine_ID "
			+ "FROM M_ProductionLine pl "
			+ "WHERE pl.M_Production_ID = ?";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, get_TrxName());
			pstmt.setInt(1, get_ID());
			rs = pstmt.executeQuery();
			while (rs.next())
				list.add( new MProductionLine( getCtx(), rs.getInt(1), get_TrxName() ) );	
		}
		catch (SQLException ex)
		{
			throw new AdempiereException("Unable to load production lines", ex);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
		MProductionLine[] retValue = new MProductionLine[list.size()];
		list.toArray(retValue);
		return retValue;
	}
	
	public void deleteLines(String trxName) {

		String sql = "DELETE FROM M_ProductionLine WHERE M_Production_ID = ?";
		DB.executeUpdate(sql, getM_Production_ID(), trxName);
				

	}// deleteLines

	public int createLines(boolean mustBeStocked) {
		
		lineno = 1;

		count = 0;

		// product to be produced	
		MProduct finishedProduct = new MProduct(getCtx(), getM_Product_ID(), get_TrxName());
		
		MAttributeSet as = finishedProduct.getAttributeSet();
		if ( as != null && as.isSerNo())
		{
				int absQty = Math.abs(getProductionQty().intValue());
				BigDecimal qtyOne = getProductionQty().signum() < 0 ? Env.ONE.negate() : Env.ONE;
				for ( int i = 0; i < absQty; i++)
				{
					if ( i > 0 )
						lineno = lineno + 1;
					MProductionLine line = new MProductionLine( this );
					line.setLine( lineno );
					line.setM_Product_ID( finishedProduct.get_ID() );
					line.setM_Locator_ID( getM_Locator_ID() );
					line.setMovementQty( qtyOne );
					line.setPlannedQty( qtyOne );
					line.saveEx();
					count++;
				}
		}
		else
		{

			MProductionLine line = new MProductionLine( this );
			line.setLine( lineno );
			line.setM_Product_ID( finishedProduct.get_ID() );
			line.setM_Locator_ID( getM_Locator_ID() );
			line.setMovementQty( getProductionQty());
			line.setPlannedQty(getProductionQty());
			line.saveEx();
			count++;
		}

		//
		
		if (mustBeStocked) {
			createLines(mustBeStocked, finishedProduct, getProductionQty());
		} else {
			count = createLinesSP();
		}
		
		count = count + splitSerialNo();
		return count;
	}
	
	/**
	 * Create lines using store procedure to speed up process
	 */
	private int createLinesSP()
	{
		String sql = "SELECT production_createlines(?)" ;
		int lines = 0;
		lines  = DB.getSQLValue (get_TrxName(), sql , getM_Production_ID());
	
	
		Trx.get(get_TrxName(), false).commit();
		return lines;
		
	}
	
	/**
	 * Split production lines when product is serial no
	 * @return no of lines addes because of the split
	 */
	private int splitSerialNo()
	{
		int lines = 0;
		
		String sql = "SELECT * \n" +
	             "FROM m_productionline \n" +
	             "WHERE m_production_id = ? \n" +
	             "AND   isendproduct = 'N' \n" +
	             "AND   EXISTS (SELECT * \n" +
	             "              FROM m_product p \n" +
	             "                INNER JOIN m_attributeset mas ON p.m_attributeset_id = mas.m_attributeset_id \n" +
	             "              WHERE mas.isserno = 'Y' \n" +
	             "              AND   p.m_product_id = m_productionline.m_product_id)";

		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			pstmt = DB.prepareStatement(sql, get_TrxName());
			pstmt.setInt(1, getM_Production_ID());

			rs = pstmt.executeQuery();
			while (rs.next()) {
				MProductionLine pline = new MProductionLine(getCtx(), rs, get_TrxName());
				int lineno = pline.getLine();
				int notosplit = Math.abs(pline.getPlannedQty().intValue());
				BigDecimal qtyOne = pline.getPlannedQty().signum() > 0 ? Env.ONE : Env.ONE.negate();
				
				pline.setPlannedQty(qtyOne);
				pline.setQtyUsed(qtyOne);
				pline.setQtyReserved(qtyOne);
				pline.setMovementQty(qtyOne.negate());
				pline.save();

				for (int i=1; i < notosplit; i++) {
					MProductionLine newLine = new MProductionLine(this);
					lineno++;
					
					newLine.setLine(lineno);
					newLine.setM_Product_ID(pline.getM_Product_ID());
					newLine.setM_Locator_ID(pline.getM_Locator_ID());
					newLine.setProcessed(false);
					newLine.setIsEndProduct(false);
					newLine.setPlannedQty(qtyOne);
					newLine.setQtyUsed(qtyOne);
					newLine.setQtyReserved(qtyOne);
					newLine.setMovementQty(qtyOne.negate());
					newLine.save();
					lines++;
				}
			}
			
		} catch (Exception e) {
			throw new AdempiereException("Failed to split serialised products.", e);
		} finally {
			DB.close(rs, pstmt);
		}

			
		return lines;
	}

	private int createLines(boolean mustBeStocked, MProduct finishedProduct, BigDecimal requiredQty) {
		
		int defaultLocator = 0;
		
		MLocator finishedLocator = MLocator.get(getCtx(), getM_Locator_ID());
		
		int M_Warehouse_ID = finishedLocator.getM_Warehouse_ID();
		
		int asi = 0;

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			pstmt = DB.prepareStatement(SQL_GET_BOM_INFO, get_TrxName());
			pstmt.setInt(1, finishedProduct.getM_Product_ID());

			rs = pstmt.executeQuery();
			while (rs.next()) {
				
				lineno = lineno + 10;
				int BOMProduct_ID = rs.getInt(1);
				BigDecimal BOMQty = rs.getBigDecimal(2);
				BigDecimal BOMMovementQty = BOMQty.multiply(requiredQty);
				
				MProduct bomproduct = new MProduct(Env.getCtx(), BOMProduct_ID, get_TrxName());
				

				if ( bomproduct.isBOM() && bomproduct.isPhantom() )
				{
					createLines(mustBeStocked, bomproduct, BOMMovementQty);
				}
				else
				{

					defaultLocator = bomproduct.getM_Locator_ID();
					if ( defaultLocator == 0 )
						defaultLocator = getM_Locator_ID();

					if (!bomproduct.isStocked())
					{					
						MProductionLine BOMLine = null;
						BOMLine = new MProductionLine( this );
						BOMLine.setLine( lineno );
						BOMLine.setM_Product_ID( BOMProduct_ID );
						BOMLine.setM_Locator_ID( defaultLocator );  
						BOMLine.setQtyUsed(BOMMovementQty );
						BOMLine.setPlannedQty( BOMMovementQty );
						BOMLine.saveEx(get_TrxName());

						lineno = lineno + 10;
						count++;					
					}
					else if (BOMMovementQty.signum() == 0) 
					{
						MProductionLine BOMLine = null;
						BOMLine = new MProductionLine( this );
						BOMLine.setLine( lineno );
						BOMLine.setM_Product_ID( BOMProduct_ID );
						BOMLine.setM_Locator_ID( defaultLocator );  
						BOMLine.setQtyUsed( BOMMovementQty );
						BOMLine.setPlannedQty( BOMMovementQty );
						BOMLine.saveEx(get_TrxName());

						lineno = lineno + 10;
						count++;
					}
					else
					{

						// BOM stock info
						MStorage[] storages = null;
						MProduct usedProduct = MProduct.get(getCtx(), BOMProduct_ID);
						defaultLocator = usedProduct.getM_Locator_ID();
						if ( defaultLocator == 0 )
							defaultLocator = getM_Locator_ID();
						if (usedProduct == null || usedProduct.get_ID() == 0)
							return 0;

						MClient client = MClient.get(getCtx());
						MProductCategory pc = MProductCategory.get(getCtx(),
								usedProduct.getM_Product_Category_ID());
						String MMPolicy = pc.getMMPolicy();
						if (MMPolicy == null || MMPolicy.length() == 0) 
						{ 
							MMPolicy = client.getMMPolicy();
						}

						storages = MStorage.getWarehouse(getCtx(), M_Warehouse_ID, BOMProduct_ID, 0, null,
								MProductCategory.MMPOLICY_FiFo.equals(MMPolicy), true, 0, get_TrxName());

						MProductionLine BOMLine = null;
						int prevLoc = -1;
						int previousAttribSet = -1;
						// Create lines from storage until qty is reached
						for (int sl = 0; sl < storages.length; sl++) {

							BigDecimal lineQty = storages[sl].getQtyOnHand();
							if (lineQty.signum() != 0) {
								if (lineQty.compareTo(BOMMovementQty) > 0)
									lineQty = BOMMovementQty;


								int loc = storages[sl].getM_Locator_ID();
								int slASI = storages[sl].getM_AttributeSetInstance_ID();
								int locAttribSet = new MAttributeSetInstance(getCtx(), asi,
										get_TrxName()).getM_AttributeSet_ID();

								// roll up costing attributes if in the same locator
								if (locAttribSet == 0 && previousAttribSet == 0
										&& prevLoc == loc) {
									BOMLine.setQtyUsed(BOMLine.getQtyUsed()
											.add(lineQty));
									BOMLine.setPlannedQty(BOMLine.getQtyUsed());
									BOMLine.setQtyReserved(BOMLine.getQtyUsed());
									BOMLine.saveEx(get_TrxName());

								}
								// otherwise create new line
								else {
									BOMLine = new MProductionLine( this );
									BOMLine.setLine( lineno );
									BOMLine.setM_Product_ID( BOMProduct_ID );
									BOMLine.setM_Locator_ID( loc );
									BOMLine.setQtyUsed( lineQty);
									BOMLine.setPlannedQty( lineQty);
									BOMLine.setQtyReserved( lineQty);
									if ( slASI != 0 && locAttribSet != 0 )  // ie non costing attribute
										BOMLine.setM_AttributeSetInstance_ID(slASI);
									BOMLine.saveEx(get_TrxName());

									lineno = lineno + 10;
									count++;
								}
								prevLoc = loc;
								previousAttribSet = locAttribSet;
								// enough ?
								BOMMovementQty = BOMMovementQty.subtract(lineQty);
								if (BOMMovementQty.signum() == 0)
									break;
							}
						} // for available storages

						// fallback
						if (BOMMovementQty.signum() != 0 ) {
							if (!mustBeStocked)
							{

								// roll up costing attributes if in the same locator
								if ( previousAttribSet == 0
										&& prevLoc == defaultLocator) {
									BOMLine.setQtyUsed(BOMLine.getQtyUsed()
											.add(BOMMovementQty));
									BOMLine.setPlannedQty(BOMLine.getQtyUsed());
									BOMLine.setQtyReserved(BOMLine.getQtyUsed());
									BOMLine.saveEx(get_TrxName());

								}
								// otherwise create new line
								else {

									BOMLine = new MProductionLine( this );
									BOMLine.setLine( lineno );
									BOMLine.setM_Product_ID( BOMProduct_ID );
									BOMLine.setM_Locator_ID( defaultLocator );  
									BOMLine.setQtyUsed( BOMMovementQty);
									BOMLine.setPlannedQty( BOMMovementQty);
									BOMLine.setQtyReserved( BOMMovementQty);
									BOMLine.saveEx(get_TrxName());

									lineno = lineno + 10;
									count++;
								}

							}
							else
							{
								throw new AdempiereUserError("Not enough stock of " + BOMProduct_ID);
							}
						}
					}
				}
			} // for all bom products
		} catch (Exception e) {
			throw new AdempiereException("Failed to create production lines", e);
		}
		finally {
			DB.close(rs, pstmt);
		}

		return count;
	}
	
	@Override
	protected boolean beforeDelete() {
		
		if (isProcessed())
			return false;
		
		deleteLines(get_TrxName());
		return true;
	}

	@Override
	public boolean processIt(String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(processAction, getDocAction());
	}

	@Override
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	}

	@Override
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt()
	{
		docType_PlannedOrder = DB.getSQLValue(get_TrxName(), SQL_GET_DOCTYPE, "Planned Order", getAD_Client_ID());
		if (getC_DocType_ID() == docType_PlannedOrder)
			throw new AdempiereException("Planned Order not Prepared/Completed.");
		
		String batchStatus = checkProductionBatchDocStatus();
		if (!Util.isEmpty(batchStatus, true))
			throw new AdempiereException(batchStatus);

		if (log.isLoggable(Level.INFO))
			log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		// Std Period open?
		MPeriod.testPeriodOpen(getCtx(), getMovementDate(), MDocType.DOCBASETYPE_ManufacturingOrder, getAD_Org_ID());

		if (!isCreated())
		{
			m_processMsg = "Not created";
			return DocAction.STATUS_Invalid;
		}

		{
			m_processMsg = validateEndProduct(getM_Product_ID());
			if (!Util.isEmpty(m_processMsg))
			{
				return DocAction.STATUS_Invalid;
			}
		}

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;

		m_justPrepared = true;
		if (!DOCACTION_Complete.equals(getDocAction()))
			setDocAction(DOCACTION_Complete);
		return DocAction.STATUS_InProgress;
	}

	/**
	 * Check document status of production batch
	 */
	public String checkProductionBatchDocStatus()
	{
		MProductionBatch pBatch = (MProductionBatch) getM_Production_Batch();
		if (pBatch.isProcessed())
		{
			if (pBatch.getDocStatus().equals(MProductionBatch.DOCACTION_Close))
				return "Can not process, Production Batch is closed";
			else if (pBatch.getDocStatus().equals(MProductionBatch.DOCACTION_Void))
				return "Can not process, Production Batch is voided";
		}
		else
			return "Please first confirm/complete the production batch";

		return null;
	}

	protected String validateEndProduct(int M_Product_ID)
	{
		String msg = isBom(M_Product_ID);
		if (!Util.isEmpty(msg))
			return msg;

		try
		{
			if (!costsOK(M_Product_ID))
			{
				msg = "Excessive difference in standard costs";
				if (MSysConfig.getBooleanValue("MFG_ValidateCostsDifferenceOnCreate", false, getAD_Client_ID()))
				{
					return msg;
				}
				else
				{
					log.warning(msg);
				}
			}
		}
		catch (AdempiereUserError e)
		{
			throw new AdempiereException(e);
		}

		return null;
	}

	protected String isBom(int M_Product_ID)
	{
		String bom = DB.getSQLValueString(get_TrxName(), "SELECT isbom FROM M_Product WHERE M_Product_ID = ?",
				M_Product_ID);
		if ("N".compareTo(bom) == 0)
		{
			return "Attempt to create product line for Non Bill Of Materials";
		}
		int materials = DB.getSQLValue(get_TrxName(),
				"SELECT count(M_Product_BOM_ID) FROM M_Product_BOM WHERE M_Product_ID = ?", M_Product_ID);
		if (materials == 0)
		{
			return "Attempt to create product line for Bill Of Materials with no BOM Products";
		}
		return null;
	}

	protected boolean costsOK(int M_Product_ID) throws AdempiereUserError
	{
		MProduct product = MProduct.get(getCtx(), M_Product_ID);
		String costingMethod = product.getCostingMethod(MClient.get(getCtx()).getAcctSchema());
		// will not work if non-standard costing is used
		if (MAcctSchema.COSTINGMETHOD_StandardCosting.equals(costingMethod))
		{
			String sql = "SELECT ABS(((cc.currentcostprice-(SELECT SUM(c.currentcostprice*bom.bomqty)"
					+ " FROM m_cost c"
					+ " INNER JOIN m_product_bom bom ON (c.m_product_id=bom.m_productbom_id)"
					+ " INNER JOIN m_costelement ce ON (c.m_costelement_id = ce.m_costelement_id AND ce.costingmethod = 'S')"
					+ " WHERE bom.m_product_id = pp.m_product_id)" + " )/cc.currentcostprice))" + " FROM m_product pp"
					+ " INNER JOIN m_cost cc on (cc.m_product_id=pp.m_product_id)"
					+ " INNER JOIN m_costelement ce ON (cc.m_costelement_id=ce.m_costelement_id)"
					+ " WHERE cc.currentcostprice > 0 AND pp.M_Product_ID = ?" + " AND ce.costingmethod='S'";

			BigDecimal costPercentageDiff = DB.getSQLValueBD(get_TrxName(), sql, M_Product_ID);

			if (costPercentageDiff == null)
			{
				costPercentageDiff = Env.ZERO;
				String msg = "Could not retrieve costs";
				if (MSysConfig.getBooleanValue("MFG_ValidateCostsOnCreate", false, getAD_Client_ID()))
				{
					throw new AdempiereUserError(msg);
				}
				else
				{
					log.warning(msg);
				}
			}

			if ((costPercentageDiff.compareTo(new BigDecimal("0.005"))) < 0)
				return true;

			return false;
		}
		return true;
	}

	@Override
	public boolean approveIt()
	{
		return true;
	}

	@Override
	public boolean rejectIt()
	{
		return true;
	}

	@Override
	public boolean voidIt()
	{

		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;

		if (DOCSTATUS_Closed.equals(getDocStatus()) || DOCSTATUS_Reversed.equals(getDocStatus())
				|| DOCSTATUS_Voided.equals(getDocStatus()))
		{
			m_processMsg = "Document Closed: " + getDocStatus();
			setDocAction(DOCACTION_None);
			return false;
		}

		// Not Processed
		if (DOCSTATUS_Drafted.equals(getDocStatus()) || DOCSTATUS_Invalid.equals(getDocStatus())
				|| DOCSTATUS_Approved.equals(getDocStatus()) || DOCSTATUS_NotApproved.equals(getDocStatus())
				|| DOCSTATUS_InProgress.equals(getDocStatus()))
		{
			setIsCreated(false);
			deleteLines(get_TrxName());
			setProductionQty(BigDecimal.ZERO);
		}
		else
		{
			boolean accrual = false;
			try
			{
				MPeriod.testPeriodOpen(getCtx(), getMovementDate(), MDocType.DOCBASETYPE_ManufacturingOrder, getAD_Org_ID());
			}
			catch (PeriodClosedException e)
			{
				accrual = true;
			}

			if (accrual)
				return reverseAccrualIt();
			else
				return reverseCorrectIt();
		}

		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;

		setProcessed(true);
		setDocAction(DOCACTION_None);

		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		return true;
	}

	@Override
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		MProduction reversal = reverse(false);
		if (reversal == null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		m_processMsg = reversal.getDocumentNo();

		return true;
	}

	private MProduction reverse(boolean accrual)
	{
		Timestamp reversalDate = accrual ? Env.getContextAsDate(getCtx(), "#Date") : getMovementDate();
		if (reversalDate == null)
		{
			reversalDate = new Timestamp(System.currentTimeMillis());
		}

		MPeriod.testPeriodOpen(getCtx(), reversalDate, MDocType.DOCBASETYPE_ManufacturingOrder, getAD_Org_ID());
		MProduction reversal = null;
		reversal = copyFrom(reversalDate);

		StringBuilder msgadd = new StringBuilder("{->").append(getDocumentNo()).append(")");
		reversal.addDescription(msgadd.toString());
		reversal.setReversal_ID(getM_Production_ID());
		reversal.saveEx(get_TrxName());

		if (!reversal.processIt(DocAction.ACTION_Complete))
		{
			m_processMsg = "Reversal ERROR: " + reversal.getProcessMsg();
			return null;
		}

		reversal.closeIt();
		reversal.setProcessing(false);
		reversal.setDocStatus(DOCSTATUS_Reversed);
		reversal.setDocAction(DOCACTION_None);
		reversal.saveEx(get_TrxName());

		msgadd = new StringBuilder("(").append(reversal.getDocumentNo()).append("<-)");
		addDescription(msgadd.toString());

		setProcessed(true);
		setReversal_ID(reversal.getM_Production_ID());
		setDocStatus(DOCSTATUS_Reversed); // may come from void
		setDocAction(DOCACTION_None);
		
		MProductionBatch pBatch = (MProductionBatch) getM_Production_Batch();
		pBatch.reserveStock(reversal.getM_Product(), getProductionQty());
		pBatch.orderedStock(reversal.getM_Product(), getProductionQty());
		pBatch.saveEx(get_TrxName());
		
		return reversal;
	}

	private MProduction copyFrom(Timestamp reversalDate)
	{
		MProduction to = new MProduction(getCtx(), 0, get_TrxName());
		PO.copyValues(this, to, getAD_Client_ID(), getAD_Org_ID());

		int count = DB.getSQLValue(get_TrxName(),
				"SELECT COUNT(M_Production_ID) FROM M_Production WHERE M_Production_Batch_ID=?",
				getM_Production_Batch_ID());
		to.setDocumentNo(getM_Production_Batch().getDocumentNo() + String.format("-%02d", ++count));
		//
		to.setDocStatus(DOCSTATUS_Drafted); // Draft
		to.setDocAction(DOCACTION_Complete);
		to.setMovementDate(reversalDate);
		to.setIsComplete("N");
		to.setIsCreated(true);
		to.setProcessing(false);
		to.setProcessed(false);
		to.setProductionQty(getProductionQty().negate());
		to.saveEx();
		MProductionLine[] flines = getLines();
		for (MProductionLine fline : flines)
		{
			MProductionLine tline = new MProductionLine(to);
			PO.copyValues(fline, tline, getAD_Client_ID(), getAD_Org_ID());
			tline.setM_Production_ID(to.getM_Production_ID());
			tline.setMovementQty(fline.getMovementQty().negate());
			tline.setPlannedQty(fline.getPlannedQty().negate());
			tline.setQtyUsed(fline.getQtyUsed().negate());
			tline.saveEx();
		}

		return to;
	}

	public MMovement createMovement() throws Exception
	{
		MProductionLine[] lines = getLines();
		if (lines.length == 0) {
			//nothing to create;
			return null;
		}
		I_M_Production_Batch batch = getM_Production_Batch();
		MMovement move = new MMovement(getCtx(), 0, get_TrxName());
		MWarehouse wh = (MWarehouse) getM_Locator().getM_Warehouse();
		boolean allowSameLocator = wh.get_ValueAsBoolean("IsAllowSameLocatorMove");
		move.setClientOrg(this);
		move.setDescription("Material move created from Production Batch#" + batch.getDocumentNo());
		try {
			move.set_Value("M_Warehouse_ID", wh.getM_Warehouse_ID());
			move.set_Value("M_Warehouse_To_ID", wh.getM_Warehouse_ID());
		} catch (Exception e) {
			//in case columns are not defined
		}
		//set fields
		move.set_Value("M_Production_Batch_ID", batch.getM_Production_Batch_ID());
		
		move.saveEx();
		log.fine("Movement Documentno=" + move.getDocumentNo() + " created for Production Batch=" + batch.getDocumentNo() );
		
		for (MProductionLine line : lines) {
			if (line.isEndProduct() || line.getM_Product().isBOM() || !line.getM_Product().isStocked()) {
				log.fine("End Product. No need to move." + line.getM_Product().getValue());
				continue;
			}
			else if (Env.ZERO.compareTo(line.getMovementQty()) == 0)  {
				log.fine("No quantity to to move." + line.getM_Product().getValue());
				continue;
			}
			if (getM_Locator_ID() == line.getM_Product().getM_Locator_ID() && !allowSameLocator) {
				throw new AdempiereUserError("Cannot use same locator. Please Allow Same Locator in Warehouse Settings");
			}
			
			MMovementLine moveLine = new MMovementLine(move);
			moveLine.setM_LocatorTo_ID(getM_Locator_ID());
			moveLine.setM_Locator_ID(line.getM_Product().getM_Locator_ID());
			moveLine.setM_Product_ID(line.getM_Product_ID());
			moveLine.setM_AttributeSetInstance_ID(line.getM_AttributeSetInstance_ID());
			moveLine.setM_AttributeSetInstanceTo_ID(line.getM_AttributeSetInstance_ID());
			//moveLine.setMovementQty(line.getMovementQty().negate());
			moveLine.set_Value("MovementQty", line.getMovementQty().negate()); //skip UOM check
			if (moveLine.getMovementQty().compareTo(line.getMovementQty().negate()) != 0) {
				throw new AdempiereException("Please adjust standard precision for " + line.getM_Product().getC_UOM().getName() );
			}
			moveLine.saveEx();
		}	
		return move;
	}
	
	/**
	 * Add to Description
	 * 
	 * @param description text
	 */
	public void addDescription(String description)
	{
		String desc = getDescription();
		if (desc == null)
			setDescription(description);
		else
		{
			StringBuilder msgd = new StringBuilder(desc).append(" | ").append(description);
			setDescription(msgd.toString());
		}
	} // addDescription

	@Override
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO))
			log.info(toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		MProduction reversal = reverse(true);
		if (reversal == null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		m_processMsg = reversal.getDocumentNo();

		return true;
	}

	@Override
	public boolean reActivateIt()
	{
		return false;
	}

	@Override
	public String getSummary()
	{
		return getDocumentNo();
	}

	@Override
	public String getDocumentInfo()
	{
		return getDocumentNo();
	}

	@Override
	public File createPDF()
	{
		return null;
	}

	@Override
	public String getProcessMsg()
	{
		return m_processMsg;
	}

	@Override
	public int getDoc_User_ID()
	{
		return getCreatedBy();
	}

	@Override
	public int getC_Currency_ID()
	{
		return MClient.get(getCtx()).getC_Currency_ID();
	}

	@Override
	public BigDecimal getApprovalAmt()
	{
		return BigDecimal.ZERO;
	}
	
	@Override
	protected boolean beforeSave(boolean newRecord) {
		
		if (newRecord && getM_Production_Batch_ID() == 0) {
			MProductionBatch batch = new MProductionBatch(getCtx(), 0, get_TrxName());
			batch.setClientOrg(this);
			batch.setMovementDate(getMovementDate());
			batch.setTargetQty(getProductionQty());
			batch.setQtyOrdered(getProductionQty());
			batch.setDescription(getDescription());
			batch.setC_DocType_ID(getC_DocType_ID());
			batch.setQtyCompleted(Env.ZERO);
			batch.setM_Product_ID(getM_Product_ID());
			batch.setM_Locator_ID(getM_Locator_ID());
			batch.setDocumentNo(getDocumentNo());
			batch.setProcessed(false);
			batch.setCountOrder(1);
			batch.saveEx();
			
			if (batch.get_ID() == 0) {
				return false;
			}
			setM_Production_Batch_ID(batch.get_ID());
			setDocumentNo(batch.getDocumentNo() + "-01");
			return true;
		}

		return true;
	}

	public boolean costsOK() throws AdempiereUserError {
		// Warning will not work if non-standard costing is used
		String sql = "SELECT ABS(((cc.currentcostprice-(SELECT SUM(c.currentcostprice*bom.bomqty)"
            + " FROM m_cost c"
            + " INNER JOIN m_product_bom bom ON (c.m_product_id=bom.m_productbom_id)"
            + " WHERE bom.m_product_id = pp.m_product_id  AND c.m_costelement_id = ce.m_costelement_id)"
            + " )/cc.currentcostprice))"
            + " FROM m_product pp"
            + " INNER JOIN m_cost cc on (cc.m_product_id=pp.m_product_id)"
            + " INNER JOIN m_costelement ce ON (cc.m_costelement_id=ce.m_costelement_id)"
            + " WHERE cc.currentcostprice > 0 AND pp.M_Product_ID = ?"
            + " AND ce.costingmethod='S'";
		
		BigDecimal costPercentageDiff = DB.getSQLValueBD(get_TrxName(), sql, getM_Product_ID());
		
		if (costPercentageDiff == null)
		{
			throw new AdempiereUserError("Could not retrieve costs");
		}
		
		MClientInfo ci = MClientInfo.get(getCtx(), getAD_Client_ID());
		BigDecimal percentDiffMax =  (BigDecimal) ci.get_Value("PercentDiffMax");
		if (percentDiffMax == null) {
			percentDiffMax = new BigDecimal(0.005);
		}
		
		if ( (costPercentageDiff.compareTo(percentDiffMax))< 0 )
			return true;
		
		return false;

	}
	
	public void reAssignASI(int[] pool)
	{
		String sql = "SELECT pl.* "
			+ "FROM M_ProductionLine pl "
			+ "WHERE pl.IsEndProduct='Y' AND pl.M_Production_ID = ? ";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, get_TrxName());
			pstmt.setInt(1, get_ID());
			rs = pstmt.executeQuery();
			
			for (int asi : pool) {
				if (rs.next()) {
					MProductionLine line = new MProductionLine(getCtx(), rs, get_TrxName());
					line.setM_AttributeSetInstance_ID(asi);
					line.save();
				} else {
					break;
				}
			}

		}
		catch (SQLException ex)
		{
			throw new AdempiereException("Unable to load production lines", ex);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

	}

	/**
	 * Clear the reserved stock against batch while user manually changes
	 * usedQty on production lines.
	 */
	private void checkStockReservationCorrection()
	{
		mapReservedQtyInfo = new HashMap<Integer, BigDecimal>();

		reservedStockDetails(getM_Product_ID(), getProductionQty());

		for (Integer productID : mapReservedQtyInfo.keySet())
		{
			BigDecimal currReleasedQty = DB.getSQLValueBD(get_TrxName(),
					"SELECT SUM(MovementQty) FROM M_ProductionLine WHERE M_Production_ID=? AND M_Product_ID=? AND IsEndProduct='N'",
					getM_Production_ID(), productID);

			if (currReleasedQty == null)
				currReleasedQty = Env.ZERO;
			BigDecimal prevReservedQty = mapReservedQtyInfo.get(productID);
			BigDecimal diffQty = prevReservedQty.add(currReleasedQty);

			if (diffQty.signum() != 0)
			{
				if (MStorage.add(getCtx(), getM_Locator().getM_Warehouse_ID(), getM_Locator_ID(), productID, 0, 0,
						Env.ZERO, diffQty.negate(), Env.ZERO, get_TrxName()))
				{
					m_log.log(Level.INFO, "Stock Reserved/Released, " + diffQty + " Qty of "
							+ MProduct.get(getCtx(), productID).getValue());
				}
				else
				{
					m_log.log(Level.SEVERE, "Storage reserved/released not updated for Production #" + getDocumentNo()
							+ ", Product :" + MProduct.get(getCtx(), productID).getValue() + ", Qty :" + diffQty);
				}
			}
		}

		mapReservedQtyInfo.clear();

	} // checkStockReservationCorrection

	public void reservedStockDetails(int M_Product_ID, BigDecimal bomQty)
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			pstmt = DB.prepareStatement(SQL_RETRIEVE_SUBCOMPONENT_INFO, get_TrxName());
			pstmt.setBigDecimal(1, bomQty);
			pstmt.setInt(2, M_Product_ID);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				int BOMProduct_ID = rs.getInt("M_ProductBom_ID");
				BigDecimal requiredQty = rs.getBigDecimal("RequiredQty");

				if (!rs.getBoolean("IsProductTypeItem"))
					continue;

				if (rs.getString("IsBOM").equals("Y") && rs.getString("IsPhantom").equals("Y"))
				{
					reservedStockDetails(BOMProduct_ID, requiredQty);
				}
				else
				{
					if (mapReservedQtyInfo.containsKey(BOMProduct_ID))
						mapReservedQtyInfo.put(BOMProduct_ID, mapReservedQtyInfo.get(BOMProduct_ID).add(requiredQty));
					else
						mapReservedQtyInfo.put(BOMProduct_ID, requiredQty);
				}
			}
		}
		catch (Exception e)
		{
			throw new AdempiereException("Failed to retrieve sub-component info. ", e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

	} // reservedStockDetails

}
