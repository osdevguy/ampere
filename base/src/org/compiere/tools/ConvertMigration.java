package org.compiere.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;

import org.apache.commons.lang.StringEscapeUtils;
import org.compiere.util.Util;

public class ConvertMigration {

	/**
	 * @param path to sql migration scripts (with oracle and postgresql subdirectories)
	 * @param starting sequence number of output xml migrations
	 */
	public static void main(String[] args) {
		
		String dir = args[0];
		if ( Util.isEmpty(dir))
			return;
		
		int seqNo = new Integer(args[1]);
		
		File base = new File(dir);
		if ( !(base.exists() && base.isDirectory()) )
			return;
		
		File oracle = new File(dir+File.separator+"oracle");
		if ( !(oracle.exists() && oracle.isDirectory()))
			return;
		
		File output = new File(dir+File.separator+"xml");
		if (!output.exists())
		{
			output.mkdir();
		}
		
		File[] oracleFiles = oracle.listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".sql");
			}
		});
		
		Arrays.sort(oracleFiles);
		
		for (File oracleFile : oracleFiles )
		{
			
			File postgresqlFile = new File(dir+File.separator+"postgresql"+File.separator+oracleFile.getName());
			String postgresStr = "";
			String oracleStr = "";
			
			if (postgresqlFile.exists())
				postgresStr = getContents(postgresqlFile);
			else
				System.out.println("Missing postgresql file:" + postgresqlFile.getName());
			
			if (oracleFile.exists())
				oracleStr = getContents(oracleFile);
			else
				System.out.println("Missing oracle file:" + oracleFile.getName());
			
			String scriptName = oracleFile.getName();
			scriptName = scriptName.substring(scriptName.indexOf("_")+1, scriptName.indexOf(".sql"));
			
			StringBuilder builder = new StringBuilder();
			builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" + 
					"<Migrations>\n" + 
					"  <Migration EntityType=\"D\" Name=\"")
					.append(scriptName)
					.append("\" ReleaseNo=\"\" SeqNo=\"")
					.append(seqNo)
					.append("\">\n");

			oracleStr += "\n;";		// make sure last command is terminated
			String[] parts = oracleStr.split("(\n)");
			String comment = "";
			String sql = "";
			int stepNo = 10;
			for (String part : parts)
			{	
				// end of sql statement
				if ( part.trim().equals(";"))
				{
					if (!Util.isEmpty(sql, true))
					{
						builder.append("    <Step DBType=\"Oracle\" SeqNo=\"")
						.append(stepNo).append("\" StepType=\"SQL\">\n");
						if ( !Util.isEmpty(comment,true) ) 
							builder.append("      <Comments>" + StringEscapeUtils.escapeXml(comment) + "</Comments>\n");
						builder.append("      <SQLStatement>")
						.append(StringEscapeUtils.escapeXml(sql))
						.append(";</SQLStatement>\n");

						builder.append("      <RollbackStatement></RollbackStatement>\n" + 
						"    </Step>\n");

						stepNo += 10;
					}

					comment = "";
					sql = "";
				}
				// comment line
				else if ( part.trim().startsWith("--"))
				{
					if (!Util.isEmpty(comment,true))
						comment += "\n";
					comment += part;
				}
				// SQL line
				else 
				{
					if (!Util.isEmpty(sql,true))
						sql += "\n";
					sql += part;
				}
			}

			postgresStr += "\n;"; 		// make sure last command is terminated
			parts = postgresStr.split("\n");
			for (String part : parts)
			{			
				// end of sql statement
				if ( part.trim().equals(";"))
				{
					if ( !Util.isEmpty(sql, true))
					{
						builder.append("    <Step DBType=\"Postgres\" SeqNo=\"")
						.append(stepNo).append("\" StepType=\"SQL\">\n");
						if ( !Util.isEmpty(comment,true) ) 
							builder.append("      <Comments>" + StringEscapeUtils.escapeXml(comment) + "</Comments>\n");

						builder.append("      <SQLStatement>")
						.append(StringEscapeUtils.escapeXml(sql))
						.append(";</SQLStatement>\n");

						builder.append("      <RollbackStatement></RollbackStatement>\n" + 
						"    </Step>\n");

						stepNo += 10;
					}
					comment = "";
					sql = "";
				}
				// comment line
				else if ( part.trim().startsWith("--"))
				{
					if (!Util.isEmpty(comment,true))
						comment += "\n";
					comment += part;
				}
				// SQL line
				else 
				{
					if (!Util.isEmpty(sql,true))
						sql += "\n";
					sql += part;
				}
			}

			builder.append("  </Migration>\n" + 
			"</Migrations>");
			
			try {
				String outputFileName =  seqNo++ + "_" + scriptName + ".xml";
				
				File outputFile = new File(output.getAbsolutePath() + File.separator + outputFileName);
				outputFile.createNewFile();
				Writer outputWriter = new BufferedWriter(new FileWriter(outputFile));
				try {
				      //FileWriter always assumes default encoding is OK!
				      outputWriter.write( builder.toString() );
				    }
				    finally {
				      outputWriter.close();
				    }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		    
		    
		}
	}
		
	/**
	 * @param aFile
	 * @return
	 */
	static public String getContents(File aFile) {
		//...checks on aFile are elided
		StringBuilder contents = new StringBuilder();

		try {
			//use buffering, reading one line at a time
			//FileReader always assumes default encoding is OK!
			BufferedReader input =  new BufferedReader(new FileReader(aFile));
			try {
				String line = null; //not declared within while loop
				/*
				 * readLine is a bit quirky :
				 * it returns the content of a line MINUS the newline.
				 * it returns null only for the END of the stream.
				 * it returns an empty String if two newlines appear in a row.
				 */
				while (( line = input.readLine()) != null){
					contents.append(line);
					contents.append("\n");
				}
			}
			finally {
				input.close();
			}
		}
		catch (IOException ex){
			ex.printStackTrace();
		}

		return contents.toString();
	}

}
