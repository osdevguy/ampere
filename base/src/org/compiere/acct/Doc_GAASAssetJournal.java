/**
 * 
 */
package org.compiere.acct;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;

import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MAsset;
import org.compiere.model.MAssetGroup;
import org.compiere.model.MAssetInfoFin;
import org.compiere.model.MClientInfo;
import org.compiere.model.MCurrency;
import org.compiere.model.MGAASAssetJournal;
import org.compiere.model.MGAASAssetJournalLine;
import org.compiere.model.MGAASAssetJournalLineDet;
import org.compiere.model.MGAASDepreciationExpense;
import org.compiere.model.MGAASDepreciationWorkfile;
import org.compiere.model.MPeriod;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Ashley G Ramdass
 * 
 */
public class Doc_GAASAssetJournal extends Doc
{
    /**
     * Constructor
     * 
     * @param ass
     *            accounting schemata
     * @param rs
     *            record
     * @param trxName
     *            trx
     */
    public Doc_GAASAssetJournal(MAcctSchema[] ass, ResultSet rs, String trxName)
    {
        super(ass, MGAASAssetJournal.class, rs, "GLJ", trxName);
    } // Doc_GAAS_AssetJournal

    @Override
    public String loadDocumentDetails()
    {
        MGAASAssetJournal assetJournal = (MGAASAssetJournal) getPO();
        setDateDoc(assetJournal.getStatementDate());

        // Amounts
        setAmount(Doc.AMTTYPE_Gross, assetJournal.getStatementDifference());

        // Set Currency
        MClientInfo clientInfo = MClientInfo.get(getCtx(),
                assetJournal.getAD_Client_ID());
        MAcctSchema acctSchema = MAcctSchema.get(getCtx(),
                clientInfo.getC_AcctSchema1_ID());
        setC_Currency_ID(acctSchema.getC_Currency_ID());

        // Init lines
        p_lines = loadLines(assetJournal);
        log.fine("Lines=" + p_lines.length);
        return null;
    }

    @Override
    public BigDecimal getBalance()
    {
        BigDecimal retValue = Env.ZERO;
        return retValue;
    }

    private void updateAssetInfoFin(MAsset asset,
            MGAASAssetJournalLine journalLine)
    {
        if (journalLine.getC_InvoiceLine_ID() == 0)
        {
            return;
        }

        String sql = "SELECT i.C_BPartner_ID, i.C_Invoice_ID, i.DocumentNo"
                + " FROM C_Invoice i INNER JOIN C_InvoiceLine il"
                + " ON i.C_Invoice_ID=il.C_Invoice_ID"
                + " WHERE il.C_InvoiceLine_ID=?";

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        MAssetInfoFin infoFin = MAssetInfoFin.get(asset);

        if (infoFin == null)
        {
            infoFin = new MAssetInfoFin(getCtx(), 0, getTrxName());
            infoFin.setA_Asset_ID(asset.get_ID());
            infoFin.setAD_Org_ID(asset.getAD_Org_ID());
            infoFin.setIsActive(true);
        }

        try
        {
            pstmt = DB.prepareStatement(sql, getTrxName());
            pstmt.setInt(1, journalLine.getC_InvoiceLine_ID());
            rs = pstmt.executeQuery();

            if (rs.next())
            {
                infoFin.setC_BPartner_ID(rs.getInt(1));
                infoFin.set_ValueOfColumn("C_Invoice_ID", rs.getInt(2));
                infoFin.set_ValueOfColumn("InvoiceNo", rs.getString(3));
            }
        }
        catch (Exception ex)
        {
            log.log(Level.SEVERE, "Could not get invoice information", ex);
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        infoFin.saveEx();
    }

    private MAsset createAsset(MGAASAssetJournalLine journalLine)
    {
        if (journalLine.getA_Asset_Group_ID() == 0)
        {
            throw new IllegalStateException("No Asset group for the line");
        }

        MAsset asset = new MAsset(getCtx(), 0, getTrxName());
        asset.setAD_Org_ID(journalLine.getAD_Org_ID());
        asset.setName(journalLine.getDescription());
        asset.setA_Asset_Group_ID(journalLine.getA_Asset_Group_ID());
        asset.setDescription(journalLine.getDescription());
        asset.set_ValueOfColumn("GAAS_Asset_Type", "FA"); // Fixed Asset
        asset.set_ValueOfColumn("A_Qty_Original", journalLine.getQty());
        asset.set_ValueOfColumn("A_Qty_Current", journalLine.getQty());
        asset.saveEx();
        log.info("Asset created: " + asset.get_ID());

        updateAssetInfoFin(asset, journalLine);
        return asset;
    }

    public MAsset createAsset(MGAASAssetJournalLine journalLine,
            MGAASAssetJournalLineDet detailLine)
    {
        if (journalLine.getA_Asset_Group_ID() == 0)
        {
            throw new IllegalStateException("No Asset group for the line");
        }

        MAsset asset = new MAsset(getCtx(), 0, getTrxName());
        asset.setAD_Org_ID(journalLine.getAD_Org_ID());
        asset.setName(detailLine.getDescription());
        asset.setA_Asset_Group_ID(journalLine.getA_Asset_Group_ID());
        asset.setDescription(detailLine.getDescription());
        asset.set_ValueOfColumn("GAAS_Asset_Type", "FA"); // Fixed Asset
        asset.set_ValueOfColumn("A_Qty_Original", detailLine.getQty());
        asset.set_ValueOfColumn("A_Qty_Current", detailLine.getQty());
        asset.saveEx();
        log.info("Asset created: " + asset.get_ID());

        updateAssetInfoFin(asset, journalLine);
        return asset;
    }

    private void updateDepreciationWorkfile(MAsset asset, BigDecimal cost,
            BigDecimal accumDepr)
    {
        MAssetGroup assetGroup = new MAssetGroup(asset.getCtx(), 
        		asset.getA_Asset_Group_ID(), asset.get_TrxName());

        MGAASDepreciationWorkfile workfile = MGAASDepreciationWorkfile
                .get(asset);
        
        if (workfile == null)
        {
            return ;
        }
        
        workfile.setAssetCost(cost);
        workfile.setInitialAccumulatedDepr(accumDepr);

        if (assetGroup.get_ValueAsBoolean("GAAS_DepreciateFromAddition"))
        {
            workfile.set_ValueOfColumn("AssetDepreciationDate",
                    getDateDoc());
        }
        else
        {
            workfile.set_ValueOfColumn("AssetDepreciationDate",
                    assetGroup.get_Value("GAAS_DepreciateFrom"));
        }
        
        workfile.saveEx();
    }
    
    private void createDefaulddep(MGAASAssetJournalLine assetJournalLine)
    {

		 
	    String sql = "SELECT aa.GAAS_Asset_Acct_ID"
                + " FROM GAAS_Asset_Acct aa  "
                + " WHERE aa.A_Asset_ID=? AND aa.IsActive='Y'";
	    
	    PreparedStatement pstmt = null;
        ResultSet rs = null;
        int assetAcctId =0;
        try
        {
            pstmt = DB.prepareStatement(sql, getTrxName());
            pstmt.setInt(1, assetJournalLine.getA_Asset_ID());
            rs = pstmt.executeQuery();

            while (rs.next())
            {
            	assetAcctId = rs.getInt(1);
            }
            
            MGAASAssetJournal assetjournal= new MGAASAssetJournal(getCtx(), assetJournalLine.getGAAS_AssetJournal_ID(), getTrxName());
            
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(assetjournal.getDateAcct().getTime());
            cal.add(Calendar.MONTH, -1);
            Timestamp depDate = new Timestamp(cal.getTimeInMillis());
            MPeriod acctPeriod = MPeriod.get(getCtx(),
            		depDate, assetJournalLine.getAD_Org_ID());
            
            int precision = getPrecision(assetAcctId);
            
            MGAASDepreciationExpense expense = new MGAASDepreciationExpense(
            		getCtx(), 0,getTrxName());
            expense.setAD_Org_ID(getAD_Org_ID());
            expense.setGAAS_Asset_Acct_ID(assetAcctId);
            expense.setCostAmt(assetJournalLine.getCostAmt());
            expense.setA_Asset_ID(assetJournalLine.getA_Asset_ID());
            expense.setDepreciationExpenseAmt(assetJournalLine.getAccumulatedDepreciationAmt()
            		.setScale(precision, BigDecimal.ROUND_HALF_UP));
            expense.setDescription("Initial accumulated depreciation entry.");
            if (acctPeriod != null)
            {
                expense.setC_Period_ID(acctPeriod.getC_Period_ID());
            }
            expense.setAssetDepreciationDate(depDate);
            expense.setPeriodNo(0);
            expense.setOpeningBalance(BigDecimal.ZERO);
            expense.setClosingBalance(assetJournalLine.getAccumulatedDepreciationAmt().setScale(precision,
            		BigDecimal.ROUND_HALF_UP));
            expense.setProcessed(true);
            
            expense.saveEx();
        }
        catch (SQLException ex)
        {
        	log.log(Level.SEVERE, "Could not asset account information", ex);
        }
        finally
        {
            DB.close(rs, pstmt);
        }

    
    }
    public int getPrecision(int assetAcctId)
    {
        String sql = "SELECT C_AcctSchema_ID FROM GAAS_Asset_Acct "
                + "WHERE GAAS_Asset_Acct_ID=?";

        int acctSchemaId = DB.getSQLValue(getTrxName(), sql, assetAcctId);

        MAcctSchema acctSchema = MAcctSchema.get(getCtx(), acctSchemaId);
        int currencyId = acctSchema.getC_Currency_ID();
        return MCurrency.getStdPrecision(getCtx(), currencyId);
    }
    private ArrayList<Fact> createFacts_New(MAcctSchema as)
    {
        Fact fact = new Fact(this, as, Fact.POST_Actual);

        for (DocLine docLine : p_lines)
        {
            DocLine_AssetJournal assetDocLine = (DocLine_AssetJournal) docLine;

            if (!assetDocLine.hasDetails())
            {
                // Simple scenario 1 - 1 link between journal line and asset
                MGAASAssetJournalLine assetJournalLine = assetDocLine
                        .getPOLine();

                MAsset asset;
                if (assetJournalLine.getA_Asset_ID() > 0)
                {
                    asset = new MAsset(getCtx(),
                            assetJournalLine.getA_Asset_ID(), getTrxName());
                }
                else
                {
                    asset = createAsset(assetJournalLine);
                    assetJournalLine.setA_Asset_ID(asset.getA_Asset_ID());
                    assetJournalLine.saveEx();
                    
                    
                }
                
                MGAASDepreciationExpense processedExpense =   MGAASDepreciationExpense.getProcessedExpense(getCtx(), assetJournalLine.getA_Asset_ID(), 0, getTrxName());
                if(processedExpense == null && assetJournalLine.getAccumulatedDepreciationAmt().compareTo(BigDecimal.ZERO) >0)
                {
                	createDefaulddep(assetJournalLine);
                }

                MAccount assetCostAcct = assetDocLine.getAccount(
                        DocLine.ASSET_COST_ACCT, as, asset.get_ID(),
                        assetJournalLine.getNew_Asset_Acct_ID(), getTrxName());
                MAccount revaluationGainAcct = assetDocLine.getAccount(
                        DocLine.GAAS_REVALUATION_GAIN_ACCT, as, asset.get_ID(),
                        assetJournalLine.getNew_Asset_Acct_ID(), getTrxName());
                MAccount revaluationLossAcct = assetDocLine.getAccount(
                        DocLine.GAAS_REVALUATION_LOSS_ACCT, as, asset.get_ID(),
                        assetJournalLine.getNew_Asset_Acct_ID(), getTrxName());
                MAccount unidentifiedAddition = assetDocLine.getAccount(
                        DocLine.ASSET_UNIDENTIFIED_ADDITIONS_ACCT, as,
                        asset.get_ID(),
                        assetJournalLine.getNew_Asset_Acct_ID(), getTrxName());
                MAccount accumulatedDeprAcct = assetDocLine.getAccount(
                        DocLine.GAAS_ACCUMULATE_DEPR_ACCT, as, asset.get_ID(),
                        assetJournalLine.getNew_Asset_Acct_ID(), getTrxName());

                BigDecimal costAmount = assetJournalLine.getCostAmt();
                BigDecimal accumulatedDeprAmount = Env.ZERO;

                assetDocLine.createFactLine(fact, assetCostAcct,
                        getC_Currency_ID(), costAmount, Env.ZERO);

                if (assetDocLine.isMatchedInvoice())
                {
                	MAccount unidentifiedAdditionInvoice = docLine.getExtAssetAccount(
                             DocLine.ASSET_UNIDENTIFIED_ADDITIONS_ACCT, as, 
                             getTrxName());
                    BigDecimal postedAmount = assetDocLine.getPostedAmount(as,
                            getTrxName());
                    BigDecimal difference = costAmount.subtract(postedAmount);

                    if( postedAmount.compareTo(Env.ZERO) != 0)
                    {
                    	assetDocLine.createFactLine(fact, unidentifiedAdditionInvoice,
                    			getC_Currency_ID(), Env.ZERO, postedAmount);
                    }

                    if (difference.compareTo(Env.ZERO) > 0)
                    {
                        assetDocLine.createFactLine(fact, revaluationGainAcct,
                                getC_Currency_ID(), Env.ZERO, difference);
                    }
                    else if (difference.compareTo(Env.ZERO) < 0)
                    {
                        assetDocLine.createFactLine(fact, revaluationLossAcct,
                                getC_Currency_ID(), difference.abs(), Env.ZERO);
                    }
                }
                else
                {
                    accumulatedDeprAmount = assetJournalLine
                            .getAccumulatedDepreciationAmt();
                    BigDecimal difference = costAmount
                            .subtract(accumulatedDeprAmount);

                    if (accumulatedDeprAmount.compareTo(Env.ZERO) > 0)
                    {
                        assetDocLine.createFactLine(fact, accumulatedDeprAcct,
                                getC_Currency_ID(), Env.ZERO,
                                accumulatedDeprAmount);
                    }

                    if (difference.compareTo(Env.ZERO) > 0)
                    {
                        assetDocLine.createFactLine(fact, unidentifiedAddition,
                                getC_Currency_ID(), Env.ZERO, difference);
                    }
                }

                updateDepreciationWorkfile(asset, costAmount,
                        accumulatedDeprAmount);
            }
            else
            {
                if (assetDocLine.isMatchedInvoice())
                {
                    // Invoice line is split into multiple assets
                    MGAASAssetJournalLine assetJournalLine = assetDocLine
                            .getPOLine();
                    MGAASAssetJournalLineDet details[] = assetJournalLine
                            .getDetails(false);

                    BigDecimal totalCostAmt = Env.ZERO;
                    for (MGAASAssetJournalLineDet detail : details)
                    {
                        MAsset asset;
                        if (detail.getA_Asset_ID() > 0)
                        {
                            asset = new MAsset(getCtx(),
                                    detail.getA_Asset_ID(), getTrxName());
                        }
                        else
                        {
                            asset = createAsset(assetJournalLine, detail);
                            detail.setA_Asset_ID(asset.getA_Asset_ID());
                            detail.saveEx();
                        }

                        MAccount assetCostAcct = assetDocLine.getAccount(
                                DocLine.ASSET_COST_ACCT, as, asset.get_ID(), 0,
                                getTrxName());
                        MAccount revaluationGainAcct = assetDocLine.getAccount(
                                DocLine.GAAS_REVALUATION_GAIN_ACCT, as,
                                asset.get_ID(), 0, getTrxName());
                        MAccount accumulatedDeprAcct = assetDocLine.getAccount(
                                DocLine.GAAS_ACCUMULATE_DEPR_ACCT, as,
                                asset.get_ID(), 0, getTrxName());

                        BigDecimal costAmount = detail.getCostAmt();
                        assetDocLine.createFactLine(fact, assetCostAcct,
                                getC_Currency_ID(), costAmount, Env.ZERO,
                                asset.get_ID());
                        updateDepreciationWorkfile(asset, costAmount, Env.ZERO);

                        BigDecimal accumulatedDeprAmount = detail
                                .getAccumulatedDepreciationAmt();
                        BigDecimal difference = costAmount
                                .subtract(accumulatedDeprAmount);

                        if (accumulatedDeprAmount.compareTo(Env.ZERO) > 0)
                        {
                            assetDocLine.createFactLine(fact,
                                    accumulatedDeprAcct, getC_Currency_ID(),
                                    Env.ZERO, accumulatedDeprAmount,
                                    asset.get_ID());
                            assetDocLine.createFactLine(fact,
                                    revaluationGainAcct, getC_Currency_ID(),
                                    Env.ZERO, difference, asset.get_ID());
                        }

                        totalCostAmt = totalCostAmt.add(costAmount);
                    }

                    MAccount revaluationGainAcct = assetDocLine
                            .getExtAssetAccount(
                                    DocLine.GAAS_REVALUATION_GAIN_ACCT, as,
                                    getTrxName());
                    MAccount revaluationLossAcct = assetDocLine
                            .getExtAssetAccount(
                                    DocLine.GAAS_REVALUATION_LOSS_ACCT, as,
                                    getTrxName());
                    MAccount unidentifiedAdditionInvoice = docLine.getExtAssetAccount(
                            DocLine.ASSET_UNIDENTIFIED_ADDITIONS_ACCT, as, 
                            getTrxName());
                    BigDecimal postedAmount = assetDocLine.getPostedAmount(as,
                            getTrxName());
                    if( postedAmount.compareTo(Env.ZERO) != 0)
                    {
                    	assetDocLine.createFactLine(fact, unidentifiedAdditionInvoice,
                    			getC_Currency_ID(), Env.ZERO, postedAmount);
                    }
                    BigDecimal difference = totalCostAmt.subtract(postedAmount);
                    if (difference.compareTo(Env.ZERO) > 0)
                    {
                        assetDocLine.createFactLine(fact, revaluationGainAcct,
                                getC_Currency_ID(), Env.ZERO, difference);
                    }
                    else if (difference.compareTo(Env.ZERO) < 0)
                    {
                        assetDocLine.createFactLine(fact, revaluationLossAcct,
                                getC_Currency_ID(), difference.abs(), Env.ZERO);
                    }
                }
            }
        }

        ArrayList<Fact> facts = new ArrayList<Fact>();
        facts.add(fact);
        return facts;
    }

    private ArrayList<Fact> createFacts_Dep(MAcctSchema as)
    {
        Fact fact = new Fact(this, as, Fact.POST_Actual);

        for (DocLine docLine : p_lines)
        {
            DocLine_AssetJournal assetDocLine = (DocLine_AssetJournal) docLine;
            MGAASAssetJournalLine journalLine = assetDocLine.getPOLine();

            if (journalLine.getGAAS_Depreciation_Expense_ID() <= 0)
            {
                continue;
            }

            MGAASDepreciationExpense deprExpense = new MGAASDepreciationExpense(
                    getCtx(), journalLine.getGAAS_Depreciation_Expense_ID(),
                    getTrxName());

            MAccount deprExpenseAcct = assetDocLine.getAccount(
                    DocLine.GAAS_DEPRECIAITION_EXPENSE_ACCT, as,
                    journalLine.getA_Asset_ID(),
                    deprExpense.getGAAS_Asset_Acct_ID(), getTrxName());
            MAccount accumulatedDeprAcct = assetDocLine.getAccount(
                    DocLine.GAAS_ACCUMULATE_DEPR_ACCT, as,
                    journalLine.getA_Asset_ID(),
                    deprExpense.getGAAS_Asset_Acct_ID(), getTrxName());

            BigDecimal deprExpenseAmt = deprExpense.getDepreciationExpenseAmt();

            assetDocLine.createFactLine(fact, deprExpenseAcct,
                    getC_Currency_ID(), deprExpenseAmt, Env.ZERO,
                    journalLine.getA_Asset_ID());
            assetDocLine.createFactLine(fact, accumulatedDeprAcct,
                    getC_Currency_ID(), Env.ZERO, deprExpenseAmt,
                    journalLine.getA_Asset_ID());
        }

        ArrayList<Fact> facts = new ArrayList<Fact>();
        facts.add(fact);
        return facts;
    }

    private BigDecimal getDepreciationExpenseAmt(int assetId, int assetAcctId)
    {
        BigDecimal retAmount = Env.ZERO;

        String sql = "SELECT NVL(SUM(DepreciationExpenseAmt),0)"
                + " FROM GAAS_Depreciation_Expense WHERE A_Asset_ID=?"
                + " AND GAAS_Asset_Acct_ID=? AND Processed='Y'";

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, getTrxName());
            pstmt.setInt(1, assetId);
            pstmt.setInt(2, assetAcctId);
            rs = pstmt.executeQuery();

            if (rs.next())
            {
                retAmount = rs.getBigDecimal(1);
            }
        }
        catch (Exception ex)
        {
            log.log(Level.SEVERE,
                    "Could not get Accumulated Depreciation Amount", ex);
            throw new IllegalStateException(
                    "Could not get Accumulated Depreciation Amount");
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        return retAmount;
    }

    private ArrayList<Fact> createFacts_Trn(MAcctSchema as)
    {
        Fact fact = new Fact(this, as, Fact.POST_Actual);

        for (DocLine docLine : p_lines)
        {
            DocLine_AssetJournal assetDocLine = (DocLine_AssetJournal) docLine;
            MGAASAssetJournalLine journalLine = assetDocLine.getPOLine();

            MGAASDepreciationWorkfile workfile = MGAASDepreciationWorkfile.get(
                    getCtx(), journalLine.getA_Asset_ID(), getTrxName());

            int oldAssetAcctId = journalLine.getOld_Asset_Acct_ID();
            int newAssetAcctId = journalLine.getNew_Asset_Acct_ID();
            BigDecimal assetCost = workfile.getAssetCost();
            BigDecimal initialAccumulatedDepr = workfile
                    .getInitialAccumulatedDepr();
            BigDecimal depreciationExpense = getDepreciationExpenseAmt(
                    journalLine.getA_Asset_ID(), oldAssetAcctId);
            BigDecimal accumulatedDepr = initialAccumulatedDepr
                    .add(depreciationExpense);

            MAccount oldAssetCostAcct = assetDocLine.getAccount(
                    DocLine.ASSET_COST_ACCT, as, journalLine.getA_Asset_ID(),
                    oldAssetAcctId, getTrxName());

            MAccount oldDeprExpenseAcct = assetDocLine.getAccount(
                    DocLine.GAAS_DEPRECIAITION_EXPENSE_ACCT, as,
                    journalLine.getA_Asset_ID(), oldAssetAcctId, getTrxName());

            MAccount oldAccumulatedDeprAcct = assetDocLine.getAccount(
                    DocLine.GAAS_ACCUMULATE_DEPR_ACCT, as,
                    journalLine.getA_Asset_ID(), oldAssetAcctId, getTrxName());

            MAccount newAssetCostAcct = assetDocLine.getAccount(
                    DocLine.ASSET_COST_ACCT, as, journalLine.getA_Asset_ID(),
                    newAssetAcctId, getTrxName());

            MAccount newDeprExpenseAcct = assetDocLine.getAccount(
                    DocLine.GAAS_DEPRECIAITION_EXPENSE_ACCT, as,
                    journalLine.getA_Asset_ID(), newAssetAcctId, getTrxName());

            MAccount newAccumulatedDeprAcct = assetDocLine.getAccount(
                    DocLine.GAAS_ACCUMULATE_DEPR_ACCT, as,
                    journalLine.getA_Asset_ID(), newAssetAcctId, getTrxName());

            assetDocLine.createFactLine(fact, oldAssetCostAcct,
                    getC_Currency_ID(), Env.ZERO, assetCost,
                    journalLine.getA_Asset_ID());

            assetDocLine.createFactLine(fact, newAssetCostAcct,
                    getC_Currency_ID(), assetCost, Env.ZERO,
                    journalLine.getA_Asset_ID());

            /* adaxa-pb: don't move depreciation expense 
            
            assetDocLine.createFactLine(fact, oldDeprExpenseAcct,
                    getC_Currency_ID(), Env.ZERO, depreciationExpense,
                    journalLine.getA_Asset_ID());

            assetDocLine.createFactLine(fact, newDeprExpenseAcct,
                    getC_Currency_ID(), depreciationExpense, Env.ZERO,
                    journalLine.getA_Asset_ID());
                    */

            assetDocLine.createFactLine(fact, oldAccumulatedDeprAcct,
                    getC_Currency_ID(), accumulatedDepr, Env.ZERO,
                    journalLine.getA_Asset_ID());

            assetDocLine.createFactLine(fact, newAccumulatedDeprAcct,
                    getC_Currency_ID(), Env.ZERO, accumulatedDepr,
                    journalLine.getA_Asset_ID());
        }

        ArrayList<Fact> facts = new ArrayList<Fact>();
        facts.add(fact);
        return facts;
    }

    private ArrayList<Fact> createFacts_Dis(MAcctSchema as)
    {
    	ArrayList<Fact> facts = new ArrayList<Fact>();
    	Fact fact = new Fact(this, as, Fact.POST_Actual);
    	MGAASAssetJournal assetJournal = (MGAASAssetJournal) getPO();
    	if(assetJournal.getDocStatus().equals(DocumentEngine.STATUS_Voided))
    	{
    		return facts;
    	}

        for (DocLine docLine : p_lines)
        {
            DocLine_AssetJournal assetDocLine = (DocLine_AssetJournal) docLine;
            MGAASAssetJournalLine journalLine = assetDocLine.getPOLine();

            BigDecimal bookAmt = journalLine.getInitialCost().subtract(
                    journalLine.getAccumulatedDepreciationAmt());
            BigDecimal disposalAmt = journalLine.getDisposalAmt();

            MAccount accumulatedDeprAcct = assetDocLine.getAccount(
                    DocLine.GAAS_ACCUMULATE_DEPR_ACCT, as,
                    journalLine.getA_Asset_ID(),
                    journalLine.getNew_Asset_Acct_ID(), getTrxName());

            MAccount assetCostAcct = assetDocLine.getAccount(
                    DocLine.ASSET_COST_ACCT, as, journalLine.getA_Asset_ID(),
                    journalLine.getNew_Asset_Acct_ID(), getTrxName());

            MAccount inTransferAcct = assetDocLine.getAccount(
                    DocLine.GAAS_INTRANSFER_OUTGOING_ACCT, as,
                    journalLine.getA_Asset_ID(),
                    journalLine.getNew_Asset_Acct_ID(), getTrxName());

            assetDocLine.createFactLine(fact, assetCostAcct,
                    getC_Currency_ID(), Env.ZERO, journalLine.getInitialCost(),
                    journalLine.getA_Asset_ID());

            assetDocLine.createFactLine(fact, accumulatedDeprAcct,
                    getC_Currency_ID(),
                    journalLine.getAccumulatedDepreciationAmt(), Env.ZERO,
                    journalLine.getA_Asset_ID());

            if (disposalAmt.compareTo(Env.ZERO) != 0)
            {
                assetDocLine.createFactLine(fact, inTransferAcct,
                        getC_Currency_ID(), disposalAmt, Env.ZERO,
                        journalLine.getA_Asset_ID());
            }

            BigDecimal difference = disposalAmt.subtract(bookAmt);

            if (difference.compareTo(Env.ZERO) > 0)
            {
                MAccount disposalAcct = assetDocLine.getAccount(
                        DocLine.GAAS_DISPOSAL_GAIN_ACCT, as,
                        journalLine.getA_Asset_ID(),
                        journalLine.getNew_Asset_Acct_ID(), getTrxName());

                assetDocLine.createFactLine(fact, disposalAcct,
                        getC_Currency_ID(), Env.ZERO, difference,
                        journalLine.getA_Asset_ID());
            }
            else if (difference.compareTo(Env.ZERO) < 0)
            {
                MAccount disposalAcct = assetDocLine.getAccount(
                        DocLine.GAAS_DISPOSAL_LOSS_ACCT, as,
                        journalLine.getA_Asset_ID(),
                        journalLine.getNew_Asset_Acct_ID(), getTrxName());

                assetDocLine.createFactLine(fact, disposalAcct,
                        getC_Currency_ID(), difference.abs(), Env.ZERO,
                        journalLine.getA_Asset_ID());
            }
        }

        facts.add(fact);
        return facts;
    }

    private ArrayList<Fact> createFacts_Rev(MAcctSchema as)
    {
        Fact fact = new Fact(this, as, Fact.POST_Actual);

        for (DocLine docLine : p_lines)
        {
            DocLine_AssetJournal assetDocLine = (DocLine_AssetJournal) docLine;
            MGAASAssetJournalLine journalLine = assetDocLine.getPOLine();

            BigDecimal difference = journalLine.getCostAmt().subtract(
                    journalLine.getInitialCost());
            
            MAccount revaluationReserveAcct = assetDocLine.getAccount(
                    DocLine.GAAS_REVALUATION_RESERVE_ACCT, as,
                    journalLine.getA_Asset_ID(),
                    journalLine.getNew_Asset_Acct_ID(), getTrxName());

            if (difference.compareTo(Env.ZERO) > 0)
            {
                MAccount revaluationGainAcct = assetDocLine.getAccount(
                        DocLine.GAAS_REVALUATION_GAIN_ACCT, as,
                        journalLine.getA_Asset_ID(),
                        journalLine.getNew_Asset_Acct_ID(), getTrxName());
                
                assetDocLine.createFactLine(fact, revaluationReserveAcct,
                        getC_Currency_ID(), difference, Env.ZERO,
                        journalLine.getA_Asset_ID());

                assetDocLine.createFactLine(fact, revaluationGainAcct,
                        getC_Currency_ID(), Env.ZERO, difference,
                        journalLine.getA_Asset_ID());
            }
            else if (difference.compareTo(Env.ZERO) < 0)
            {
                MAccount revaluationLossAcct = assetDocLine.getAccount(
                        DocLine.GAAS_REVALUATION_LOSS_ACCT, as,
                        journalLine.getA_Asset_ID(),
                        journalLine.getNew_Asset_Acct_ID(), getTrxName());

                assetDocLine.createFactLine(fact, revaluationLossAcct,
                        getC_Currency_ID(), difference.abs(), Env.ZERO,
                        journalLine.getA_Asset_ID());
                
                assetDocLine.createFactLine(fact, revaluationReserveAcct,
                        getC_Currency_ID(), Env.ZERO, difference.abs(),
                        journalLine.getA_Asset_ID());
            }
        }

        ArrayList<Fact> facts = new ArrayList<Fact>();
        facts.add(fact);
        return facts;
    }

    /**
     * Create Facts (Accounting Logic)
     */
    public ArrayList<Fact> createFacts(MAcctSchema as)
    {
        MGAASAssetJournal assetJournal = (MGAASAssetJournal) getPO();

        if (MGAASAssetJournal.ENTRYTYPE_New.equals(assetJournal.getEntryType()))
        {
            return createFacts_New(as);
        }
        else if (MGAASAssetJournal.ENTRYTYPE_Depreciation.equals(assetJournal
                .getEntryType()))
        {
            return createFacts_Dep(as);
        }
        else if (MGAASAssetJournal.ENTRYTYPE_Transfers.equals(assetJournal
                .getEntryType()))
        {
            return createFacts_Trn(as);
        }
        else if (MGAASAssetJournal.ENTRYTYPE_Disposal.equals(assetJournal
                .getEntryType()))
        {
            return createFacts_Dis(as);
        }
        else if (MGAASAssetJournal.ENTRYTYPE_Revaluation.equals(assetJournal
                .getEntryType()))
        {
            return createFacts_Rev(as);
        }
        else
        {
            throw new IllegalStateException(
                    "Entry type for this Asset Journal cannot be processed: "
                            + assetJournal.getEntryType());
        }
    }

    public DocLine[] loadLines(MGAASAssetJournal assetJournal)
    {
        ArrayList<DocLine> list = new ArrayList<DocLine>();
        MGAASAssetJournalLine lines[] = assetJournal.getLines(false);

        for (MGAASAssetJournalLine line : lines)
        {
            if (line.isActive())
            {
                DocLine_AssetJournal docLine = new DocLine_AssetJournal(line,
                        this);
                list.add(docLine);
            }
        }

        DocLine[] docLines = new DocLine[list.size()];
        list.toArray(docLines);
        return docLines;
    }

}
