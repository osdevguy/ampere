package org.compiere.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MReplenish;
import org.compiere.model.MSysConfig;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class RecalcUsage extends SvrProcess {

	private int	p_M_Product_ID = 0;
	private int p_M_Warehouse_ID = 0;
	private Timestamp p_As_of_Date = null;
	
	private static CLogger s_log = CLogger.getCLogger (RecalcUsage.class);
	
	protected String doIt() throws Exception {
		
		MReplenish retValue = null;
		
		BigDecimal defSampling = new BigDecimal( MSysConfig.getValue("AX_REPLENISH_DEFAULT_DAYSAMPLING"));
		BigDecimal defSafetyStock = new BigDecimal( MSysConfig.getValue("AX_REPLENISH_DEFAULT_SAFETYSTOCK"));
		String sql = "SELECT * FROM M_Replenish WHERE M_Product_ID=? AND M_Warehouse_ID = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement (sql, get_TrxName());
			pstmt.setInt (1, p_M_Product_ID);
			pstmt.setInt (2, p_M_Warehouse_ID);
			rs = pstmt.executeQuery ();
		
			if (rs.next ())
				retValue = new MReplenish (getCtx(), rs, get_TrxName());
	
			BigDecimal sampling = (BigDecimal) retValue.get_Value("dayssalessampleperiod");
			
			if ((sampling == null) || (sampling.equals(Env.ZERO))) {
				log.log(Level.INFO, "Sampling Period is not set. Using Default Values. ");
				sampling = Env.ONE;
				retValue.set_CustomColumn("dayssalessampleperiod", sampling);
			}			
			retValue.set_CustomColumn("dailyusagemin", getMinUsage(p_As_of_Date, p_M_Product_ID, p_M_Warehouse_ID,  sampling, getCtx()));
			retValue.set_CustomColumn("dailyusageave", getAveUsage(p_As_of_Date, p_M_Product_ID, p_M_Warehouse_ID, sampling, getCtx()));
			retValue.set_CustomColumn("dailyusagemax", getMaxUsage(p_As_of_Date, p_M_Product_ID, p_M_Warehouse_ID,  sampling, getCtx()));

			BigDecimal safetyStock = (BigDecimal)  retValue.get_Value("safetystockdaysusage");
			if ((safetyStock == null) || (safetyStock.equals(Env.ZERO))) {
				log.log(Level.INFO, "Safety stock is not set. Using Default Values. ");
				safetyStock = Env.ONE;
				retValue.set_CustomColumn("safetystockdaysusage", safetyStock);
			}			
			
			retValue.set_CustomColumn("dailyusageaveplusseasonality", getUsagePlusSeasonality(p_As_of_Date, p_M_Product_ID, p_M_Warehouse_ID, 
					sampling, safetyStock, getCtx()));
		
			retValue.save();
		}
		catch (Exception e)
		{
			log.log (Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		
		return null;
	}

	protected void prepare() {
		int nWin = 50;  //Assume the user will not have more than this open windows
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("ASOF"))
			{
				p_As_of_Date = ((Timestamp)para[i].getParameter());
			}
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
	
		
		for (int j=0; j< nWin; j++ ) {
			if (Env.getContext(getCtx(), j, "WindowName").equals("Replenish Intouch")) {
				p_M_Product_ID = Env.getContextAsInt(getCtx(), j, "M_Product_ID");
				p_M_Warehouse_ID = Env.getContextAsInt(getCtx(), j, "M_Warehouse_ID");
				break;
			}
		}

	} //prepare
	
	public static BigDecimal getMinUsage(Timestamp date, int product_id, int warehouse_id, Object daysampling, Properties ctx) {
		
		BigDecimal val = Env.ZERO;
		
		val=  DB.getSQLValueBD(
				null,
				"SELECT usagemin(?, ?, ?, ?, ?,?)",
				date, daysampling, Env.getAD_Client_ID(ctx), Env.getAD_Org_ID(ctx), product_id, warehouse_id);
		
		s_log.log(Level.FINE, "Usage Min =" + val.toString() + " M_Product_ID = " + product_id);
		return val.setScale(4, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal getMaxUsage(Timestamp date, int product_id, int warehouse_id, Object daysampling, Properties ctx) {

		BigDecimal val = Env.ZERO;
		
		val =  DB.getSQLValueBD(
				null,
				"SELECT usagemax(?, ?, ?, ?, ?,?)",
				date, daysampling, Env.getAD_Client_ID(ctx), Env.getAD_Org_ID(ctx), product_id, warehouse_id);
		
		s_log.log(Level.FINE, "Usage Max =" + val.toString() + " M_Product_ID = " + product_id);
		return val.setScale(4, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal getAveUsage(Timestamp date, int product_id, int warehouse_id, Object daysampling, Properties ctx) {

		BigDecimal val = Env.ZERO;
		
		val =  DB.getSQLValueBD(
				null,
				"SELECT usageave(?, ?, ?, ?, ?,?)",
				date, daysampling, Env.getAD_Client_ID(ctx), Env.getAD_Org_ID(ctx), product_id, warehouse_id);
		
		s_log.log(Level.FINE, "Usage Ave =" + val.toString() + " M_Product_ID = " + product_id);
		return val.setScale(4, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal getUsagePlusSeasonality(Timestamp date, int product_id, int warehouse_id, Object daysampling, Object daysafetystock, Properties ctx) {

		BigDecimal val = Env.ZERO;

		val =  DB.getSQLValueBD(
				null,
				"SELECT forecastusage(?, ?, ?, ?, ?, ?, ?)",
				date, daysampling,daysafetystock, Env.getAD_Client_ID(ctx), Env.getAD_Org_ID(ctx), product_id, warehouse_id);

		s_log.log(Level.FINE, "Usage Ave Plus Seasonality =" + val.toString() + " M_Product_ID = " + product_id);
		return val.setScale(4, BigDecimal.ROUND_HALF_UP);
	}
	
/*  Wrong Implementation
	public static BigDecimal getMinPlusSeasonality(Timestamp date, int product_id, int warehouse_id, Object daysampling, Object daysafetystock, Properties ctx) {
		
		BigDecimal val = Env.ZERO;
		
		val=  DB.getSQLValueBD(
				null,
				"SELECT minplusseasonality(?, ?, ?, ?, ?, ?, ?)",
				date, daysampling,daysafetystock, Env.getAD_Client_ID(ctx), Env.getAD_Org_ID(ctx), product_id, warehouse_id);
		
		return val.setScale(4, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal getMaxPlusSeasonality(Timestamp date, int product_id, int warehouse_id, Object daysampling, Object daysafetystock, Properties ctx) {

		BigDecimal val = Env.ZERO;
		
		val =  DB.getSQLValueBD(
				null,
				"SELECT maxplusseasonality(?, ?, ?, ?, ?,?, ?)",
				date, daysampling,daysafetystock, Env.getAD_Client_ID(ctx), Env.getAD_Org_ID(ctx), product_id, warehouse_id);
		
		return val.setScale(4, BigDecimal.ROUND_HALF_UP);
	}
*/
	
/*	
 * Disregard this function. Already available in getUsagePlusSeasonality
 * 
	public static BigDecimal getAvePlusSeasonality(Timestamp date, int product_id, int warehouse_id, Object daysampling, Object daysafetystock, Properties ctx) {

		BigDecimal val = Env.ZERO;
		
		val =  DB.getSQLValueBD(
				null,
				"SELECT aveplusseasonality(?, ?, ?, ?, ?,?, ?)",
				date, daysampling,daysafetystock, Env.getAD_Client_ID(ctx), Env.getAD_Org_ID(ctx), product_id, warehouse_id);
		
		return val.setScale(4, BigDecimal.ROUND_HALF_UP);
	}
*/	
}
