package org.compiere.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.model.MPayment;
import org.compiere.model.X_I_Invoice;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Nikunj Panelia
 *
 */
public class ImportPaymentFromBankStatement extends SvrProcess 
{

	/**	Tender Type of payment	*/
	private String tenderType=null;
	
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] params = getParameter();
		for (ProcessInfoParameter p : params)
		{
			if ( p.getParameterName().equals("TenderType") )
				tenderType = p.getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + p.getParameterName());
		}
	}

	/**
	 *  Perform process.
	 *  @return clear Message
	 *  @throws Exception
	 */
	protected String doIt() throws Exception
	{
		String unImportCheck = " C_Payment_ID IS null AND C_BankAccount_ID IS NOT null AND C_BPartner_ID IS NOT null AND AD_Client_ID=" + getAD_Client_ID() ;
		StringBuffer sql=new StringBuffer();
		
		int noInsert = 0;
		sql = new StringBuffer ("SELECT * FROM I_Invoice "
				  + "WHERE I_IsImported<>'E' AND ").append(unImportCheck);
		try
		{
			PreparedStatement pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
			ResultSet rs = pstmt.executeQuery ();
			while (rs.next ())
			{
				X_I_Invoice imp = new X_I_Invoice (getCtx (), rs,  get_TrxName());
			
				MPayment payment=new MPayment(getCtx(), 0, get_TrxName());
				
				payment.setAD_Client_ID(getAD_Client_ID());
				payment.setAD_Org_ID(Env.getAD_Org_ID(getCtx()));
				payment.setAccountNo(imp.get_ValueAsString("BankAccountNo"));
				payment.setDateAcct(imp.getDateAcct());
				payment.setDateTrx(imp.getDateAcct());
				payment.setC_DocType_ID(imp.get_ValueAsInt("C_DocType_Payment_ID"));
				payment.setC_BPartner_ID(imp.getC_BPartner_ID());
				if(imp.get_Value("AmtAcctCr") !=null && ((BigDecimal)imp.get_Value("AmtAcctCr")).compareTo(BigDecimal.ZERO) > 0)
					payment.setPayAmt((BigDecimal)imp.get_Value("AmtAcctCr"));
				if(imp.get_Value("AmtAcctDr") !=null && ((BigDecimal)imp.get_Value("AmtAcctDr")).compareTo(BigDecimal.ZERO) > 0)
					payment.setPayAmt((BigDecimal)imp.get_Value("AmtAcctDr"));
				payment.setTenderType(tenderType);
				if(imp.get_ValueAsBoolean("IsChargeOnPayment"))
				{
					payment.setC_Charge_ID(imp.getC_Charge_ID());						
				}
				payment.setC_BankAccount_ID(imp.get_ValueAsInt("C_BankAccount_ID"));
				payment.setC_Currency_ID(imp.getC_Currency_ID());
				payment.saveEx();
				imp.set_ValueOfColumn("C_Payment_ID", payment.get_ID());
				if(payment.getC_Charge_ID()> 0)
				{
					imp.setI_IsImported(true);
					imp.setProcessed(true);
				}
				payment.processIt(DocAction.ACTION_Complete);
				payment.saveEx();
				imp.saveEx();
				noInsert++;
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, "Problem when creating payment", e);
		}
				
		return "Payments imported :" +noInsert;
	}	
}