/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                        *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MAccount;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MAcctSchemaDefault;
import org.compiere.model.MDocType;
import org.compiere.model.MFactAcct;
import org.compiere.model.MGLCategory;
import org.compiere.model.MInvoice;
import org.compiere.model.MJournal;
import org.compiere.model.MJournalBatch;
import org.compiere.model.MJournalLine;
import org.compiere.model.MOrg;
import org.compiere.model.MPayment;
import org.compiere.model.MRole;
import org.compiere.model.Query;
import org.compiere.model.X_T_CombinedArApRevalue;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 *	Invoice Aging Report.
 *	Based on RV_Aging.
 *  @author Jorg Janke
 *  @version $Id: Aging.java,v 1.5 2006/10/07 00:58:44 jjanke Exp $
 */
public class CombinedArApRevalue extends SvrProcess
{
	private int			p_C_AcctSchema_ID = 0;
	private int			p_C_ConversionType_ID = 0;
	/** The date to calculate the days due from			*/
	private Timestamp	p_StatementDate = null;
	private String 		p_IsSOTrx = "B";  // Y for ap N for ar B for both
	private int			p_C_Currency_ID = 0;
	private int			as_C_Currency_ID = 0;
	private int			p_AD_Org_ID = 0;
	private int			p_C_BP_Group_ID = 0;
	private int			p_C_BPartner_ID = 0;
	/** Number of days between today and statement date	*/
	private boolean p_IncludePayments = true;
	/** GL Document Type				*/
	private int				p_C_DocTypeReval_ID = 0;
	
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_AcctSchema_ID"))
				p_C_AcctSchema_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("C_ConversionType_ID"))
				p_C_ConversionType_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("StatementDate"))
				p_StatementDate = (Timestamp)para[i].getParameter();
			else if (name.equals("IsSOTrx") && para[i].getParameter() != null )
				p_IsSOTrx = (String) para[i].getParameter();
			else if (name.equals("C_Currency_ID"))
				p_C_Currency_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("AD_Org_ID"))
				p_AD_Org_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("C_BP_Group_ID"))
				p_C_BP_Group_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("IsIncludePayments"))
				p_IncludePayments  = "Y".equals(para[i].getParameter());
			else if (name.equals("C_DocTypeReval_ID"))
				p_C_DocTypeReval_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		// get currency_id for account schema
		final MAcctSchema as = MAcctSchema.get(getCtx(), p_C_AcctSchema_ID);
		as_C_Currency_ID = as.getC_Currency_ID();
		
		if (p_StatementDate == null)
			p_StatementDate = new Timestamp (System.currentTimeMillis());
		
	}	//	prepare

	/**
	 * 	DoIt
	 *	@return Message
	 *	@throws Exception
	 */
	protected String doIt() throws Exception
	{
		log.info("StatementDate=" + p_StatementDate + ", IsSOTrx=" + p_IsSOTrx
			+ ", C_Currency_ID=" + as_C_Currency_ID + ", AD_Org_ID=" + p_AD_Org_ID
			+ ", C_BP_Group_ID=" + p_C_BP_Group_ID + ", C_BPartner_ID=" + p_C_BPartner_ID);
		
		StringBuffer sql = new StringBuffer("INSERT INTO T_CombinedArApRevalue ")
		.append(" (AD_PInstance_ID, AD_Client_ID, AD_Org_ID, C_Activity_ID, C_BP_Group_ID, C_BPartner_ID, C_Campaign_ID, C_Currency_ID, C_Invoice_ID, C_Payment_ID,  ")
		.append(" C_Project_ID, IsIncludePayments, IsSOTrx, ListSources, StatementDate, Created, Updated, IsActive, TrxAmt, OpenAmt, Account_ID, C_DocTypeReval_ID) ");
		//
		sql.append(" SELECT ?, oi.AD_Client_ID, oi.AD_Org_ID, oi.C_Activity_ID, bp.C_BP_Group_ID, oi.C_BPartner_ID, oi.C_Campaign_ID," +
				" oi.C_Currency_ID, oi.C_Invoice_ID, oi.C_Payment_ID, oi.C_Project_ID, ?, oi.IsSOTrx, 'Y', ?, ?, ?, 'Y', ");								//	5..6
			String s = ",oi.C_Currency_ID," + as_C_Currency_ID + ",?," 
			+ (p_C_ConversionType_ID == 0 ? "oi.C_ConversionType_ID" : p_C_ConversionType_ID) + ",oi.AD_Client_ID,oi.AD_Org_ID)";
			sql.append("COALESCE(currencyConvert(oi.GrandTotal").append(s).append(",0)")		//	
				.append(", COALESCE(currencyConvert((CASE WHEN oi.C_Invoice_ID IS NOT NULL" +
				" THEN invoiceOpenToDate(oi.C_Invoice_ID, 0, ?)" +
				" ELSE paymentAvailableToDate(oi.C_Payment_ID, ?)*CASE WHEN oi.ISSoTrx='Y' THEN -1 ELSE 1 END" +
				" END )").append(s).append(",0)");
		sql.append(", (select max(account_id) from fact_acct fa where fa.c_tax_id is null and " + 
				"((fa.ad_table_id=318 and fa.record_id=oi.c_invoice_id) or (fa.ad_table_id=335 and " + 
				" fa.record_id=oi.c_payment_id and case when oi.issotrx='Y' then fa.amtacctcr<>0 else fa.amtacctdr<>0 end))), ");
		sql.append( p_C_DocTypeReval_ID > 0 ? p_C_DocTypeReval_ID : "NULL" );
		sql.append(" FROM RV_CombinedOpenItem oi"
			+ " INNER JOIN C_BPartner bp ON (oi.C_BPartner_ID=bp.C_BPartner_ID) " +
			" LEFT OUTER JOIN C_Payment p ON (oi.C_Payment_ID=p.C_Payment_ID) ");
		s = ( p_IsSOTrx.equals("B") ? "Y','N" : p_IsSOTrx );
			sql.append("WHERE oi.ISSoTrx IN ('" + s + "') ");
		if ( !p_IncludePayments )
			sql.append(" AND oi.C_Invoice_ID IS NOT NULL ");
		if ( p_C_Currency_ID != 0 )
			sql.append(" AND oi.C_Currency_ID = " + p_C_Currency_ID );
		if (p_C_BPartner_ID > 0)
			sql.append(" AND oi.C_BPartner_ID=").append(p_C_BPartner_ID);
		else if (p_C_BP_Group_ID > 0)
			sql.append(" AND bp.C_BP_Group_ID=").append(p_C_BP_Group_ID);
		if (p_AD_Org_ID > 0) // BF 2655587
		{
			sql.append(" AND oi.AD_Org_ID=").append(p_AD_Org_ID);
		}
		
		sql.append(" AND oi.DateInvoiced <= ? AND p.C_Charge_ID IS NULL ");
		/*sql.append(" AND (CASE WHEN oi.C_Invoice_ID IS NOT NULL" +
				" THEN invoiceOpenToDate(oi.C_Invoice_ID, 0, ?)" +
				" ELSE paymentAvailableToDate(oi.C_Payment_ID, ?)" +
				" END ) <> 0");*/
		sql.append(" ORDER BY oi.C_BPartner_ID, oi.C_Currency_ID, oi.C_Invoice_ID, oi.C_Payment_ID ");
		
		log.fine("SQL: " + sql.toString());
		
		String finalSql = MRole.getDefault(getCtx(), false).addAccessSQL(
			sql.toString(), "oi", MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO);	
		log.finer(finalSql);

		PreparedStatement pstmt = null;
		//
		int counter = 0;
		int rows = 0;
		
		//
		try
		{
			Date date = new Date(p_StatementDate.getTime());
			pstmt = DB.prepareStatement(finalSql, get_TrxName());
			pstmt.setInt(1, getAD_PInstance_ID());
			pstmt.setString(2, p_IncludePayments ? "Y":"N");
			pstmt.setDate(3, date);
			pstmt.setDate(4, date);
			pstmt.setDate(5, date);
			pstmt.setDate(6, date);
			pstmt.setDate(7, date);
			pstmt.setDate(8, date);
			pstmt.setDate(9, date);
			pstmt.setDate(10, date);
			//pstmt.setDate(11, date);
			//pstmt.setDate(12, date);
			
			log.fine("Inserting items");
			rows = pstmt.executeUpdate();

			log.fine("Inserted " + rows + " items");
		
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, finalSql, e);
		}
		finally 
		{
			DB.close(pstmt);
		}
		
		sql = new StringBuffer("UPDATE T_CombinedArApRevalue ")
				.append(" SET ")
				.append(" AmtAcctOpenPosted = COALESCE((select sum(fa.amtacctdr-fa.amtacctcr) from fact_acct fa " + 
						"left join c_allocationline al on (fa.record_id=al.c_allocationhdr_id and fa.line_id=al.c_allocationline_id) " + 
						" where ((fa.AD_Table_ID=318 AND fa.Record_ID=T_CombinedArApRevalue.C_Invoice_ID AND fa.Line_ID IS NULL))" +
						" AND fa.account_id=t_combinedaraprevalue.account_id AND fa.DateAcct <= t_combinedaraprevalue.statementdate)*(CASE WHEN t_combinedaraprevalue.IsSoTrx='N' THEN -1 ELSE 1 END),0),")
				.append(" AmtAcctOpenSource =  COALESCE((select sum(fa.amtsourcedr-amtsourcecr)" +
						" from fact_acct fa " + 
						" where ((fa.AD_Table_ID=318 AND fa.Record_ID=T_CombinedArApRevalue.C_Invoice_ID AND fa.Line_ID IS NULL))" +
						" AND fa.account_id=t_combinedaraprevalue.account_id AND fa.DateAcct <= t_combinedaraprevalue.statementdate)*(CASE WHEN t_combinedaraprevalue.IsSoTrx='N' THEN -1 ELSE 1 END),0),")
				.append(" AmtRevalDiff = (select max(openamt-((amtacctdr - amtacctcr) * CASE WHEN"
						+ " trxamt=0 THEN 0 ELSE (openamt/trxamt) END) * "
						+ "CASE WHEN issotrx='N' THEN -1 ELSE 1 END) from fact_acct fa where fa.c_tax_id is"
						+ " null and ((fa.ad_table_id=318 and fa.record_id=c_invoice_id))) ")

				.append(" WHERE ad_pinstance_id = ?   ");



		rows = DB.executeUpdateEx(sql.toString(), new Object[] {getAD_PInstance_ID()}, get_TrxName());
		log.fine("Updated balance on " + rows + " items");
		
		sql = new StringBuffer("UPDATE T_CombinedArApRevalue ")
		.append(" SET ")
		.append(" AmtAcctOpenPosted = COALESCE(T_CombinedArApRevalue.AmtAcctOpenPosted,0) + COALESCE((select sum(fa.amtacctdr-fa.amtacctcr) from fact_acct fa " + 
				"left join c_allocationline al on (fa.record_id=al.c_allocationhdr_id and fa.line_id=al.c_allocationline_id) " + 
				" where ((fa.AD_Table_ID=335 AND fa.Record_ID=T_CombinedArApRevalue.C_Payment_ID))" +
				" AND fa.account_id=t_combinedaraprevalue.account_id AND fa.DateAcct <= t_combinedaraprevalue.statementdate)*(CASE WHEN t_combinedaraprevalue.IsSoTrx='N' THEN -1 ELSE 1 END),0),")
		.append(" AmtAcctOpenSource = COALESCE(T_CombinedArApRevalue.AmtAcctOpenSource,0) + COALESCE((select sum(fa.amtsourcedr-amtsourcecr)" +
				" from fact_acct fa " + 
				" where ((fa.AD_Table_ID=335 AND fa.Record_ID=T_CombinedArApRevalue.C_Payment_ID))" +
				" AND fa.account_id=t_combinedaraprevalue.account_id AND fa.DateAcct <= t_combinedaraprevalue.statementdate)*(CASE WHEN t_combinedaraprevalue.IsSoTrx='N' THEN -1 ELSE 1 END),0),")
		.append(" AmtRevalDiff = COALESCE(AmtRevalDiff,0) + (select max(openamt-((amtacctdr - amtacctcr) * CASE WHEN"
				+ " trxamt=0 THEN 0 ELSE (openamt/trxamt) END) * "
				+ "CASE WHEN issotrx='N' THEN -1 ELSE 1 END) from fact_acct fa where fa.c_tax_id is"
				+ " null and ((fa.ad_table_id=335 "
				+ "and fa.record_id=c_payment_id and case when issotrx='Y' then fa.amtacctcr<>0 else fa.amtacctdr<>0 end))) ")

				.append(" WHERE ad_pinstance_id = ?   ");



		rows = DB.executeUpdateEx(sql.toString(), new Object[] {getAD_PInstance_ID()}, get_TrxName());
		log.fine("Updated balance on " + rows + " items");
		sql = new StringBuffer("UPDATE T_CombinedArApRevalue ")
		.append(" SET ")
		.append(" AmtAcctOpenPosted = COALESCE(T_CombinedArApRevalue.AmtAcctOpenPosted,0) + COALESCE((select sum(fa.amtacctdr-fa.amtacctcr) from fact_acct fa " + 
				"left join c_allocationline al on (fa.record_id=al.c_allocationhdr_id and fa.line_id=al.c_allocationline_id) " + 
				" where ((fa.ad_table_id=735 and al.c_invoice_id=t_combinedaraprevalue.c_invoice_id) OR " + 
				" (fa.ad_table_id=735 and al.c_payment_id=t_combinedaraprevalue.c_payment_id))" +
				" AND fa.account_id=t_combinedaraprevalue.account_id AND fa.DateAcct <= t_combinedaraprevalue.statementdate)*(CASE WHEN t_combinedaraprevalue.IsSoTrx='N' THEN -1 ELSE 1 END),0),")
		.append(" AmtAcctOpenSource = COALESCE(T_CombinedArApRevalue.AmtAcctOpenSource,0) + COALESCE((SELECT SUM( "
				+ "CASE WHEN T_CombinedArApRevalue.AmtAcctOpenPosted = 0 THEN (fa.amtacctdr-amtacctcr) "
				+ "ELSE (fa.amtacctdr-amtacctcr)*(T_CombinedArApRevalue.AmtAcctOpenSource/T_CombinedArApRevalue.AmtAcctOpenPosted) END) " +
				" from fact_acct fa " + 
				"left join c_allocationline al on (fa.record_id=al.c_allocationhdr_id and fa.line_id=al.c_allocationline_id)" + 
				" where ((fa.ad_table_id=735 and al.c_invoice_id=t_combinedaraprevalue.c_invoice_id) OR " + 
				" (fa.ad_table_id=735 and al.c_payment_id=t_combinedaraprevalue.c_payment_id))" +
				" AND fa.account_id=t_combinedaraprevalue.account_id AND "
				+ "fa.DateAcct <= t_combinedaraprevalue.statementdate)*(CASE WHEN t_combinedaraprevalue.IsSoTrx='N' THEN -1 ELSE 1 END),0) ")
				.append(" WHERE ad_pinstance_id = ?   ");



		rows = DB.executeUpdateEx(sql.toString(), new Object[] {getAD_PInstance_ID()}, get_TrxName());
		log.fine("Updated balance on " + rows + " items");
		
		rows = DB.executeUpdateEx("DELETE FROM T_CombinedARAPRevalue WHERE AD_PInstance_ID=? AND ROUND(AmtAcctOpenPosted,2)=0", new Object[] {getAD_PInstance_ID()}, get_TrxName());
		log.fine("Deleted " + rows + " closed items");

		sql = new StringBuffer("UPDATE T_CombinedArApRevalue r SET openamt = COALESCE(currencyConvert(r.AmtAcctOpenSource, r.C_Currency_ID," + as_C_Currency_ID + ",r.StatementDate," 
				+ (p_C_ConversionType_ID == 0 ? "COALESCE((SELECT i.C_ConversionType_ID FROM C_Invoice i WHERE i.C_Invoice_ID=r.C_invoice_ID)," +
						"(SELECT p.C_ConversionType_ID FROM C_Payment p WHERE p.C_Payment_ID=r.C_Payment_ID))" : p_C_ConversionType_ID) + ",r.AD_Client_ID,r.AD_Org_ID),0),");
		sql.append("amtrevaldiff = currencyConvert(r.AmtAcctOpenSource, r.C_Currency_ID," + as_C_Currency_ID + ",r.StatementDate," 
				+ (p_C_ConversionType_ID == 0 ? "COALESCE((SELECT i.C_ConversionType_ID FROM C_Invoice i WHERE i.C_Invoice_ID=r.C_invoice_ID)," +
						"(SELECT p.C_ConversionType_ID FROM C_Payment p WHERE p.C_Payment_ID=r.C_Payment_ID))" : p_C_ConversionType_ID) + ",r.AD_Client_ID,r.AD_Org_ID) - AmtAcctOpenPosted");
		sql.append(" WHERE AD_PInstance_ID = ? ");

		rows = DB.executeUpdateEx(sql.toString(), new Object[] {getAD_PInstance_ID()}, get_TrxName());
		log.fine("Updated revaluation on " + rows + " items");

		//		Create Document
		String info = "";
		if (p_C_DocTypeReval_ID != 0)
		{
			if (p_C_Currency_ID != 0)
				log.warning("Can create Journal only for all currencies");
			else
				info = createGLJournal();
		}

		//	
		log.info("#" + counter + " - rows=" + rows);
		return info;
	}	//	doIt


	/**
	 * 	Create GL Journal
	 * 	@return document info
	 */
	private String createGLJournal()
	{
		final String whereClause = "AD_PInstance_ID=?";
		List <X_T_CombinedArApRevalue> list = new Query(getCtx(), X_T_CombinedArApRevalue.Table_Name, whereClause, get_TrxName())
		.setParameters(getAD_PInstance_ID())
		.setOrderBy("AD_Org_ID")
		.list();

		if (list.size() == 0)
			return " - No Records found";

		//
		MAcctSchema as = MAcctSchema.get(getCtx(), p_C_AcctSchema_ID);
		MAcctSchemaDefault asDefaultAccts = MAcctSchemaDefault.get(getCtx(), p_C_AcctSchema_ID);
		MGLCategory cat = MGLCategory.getDefaultSystem(getCtx());
		if (cat == null)
		{
			MDocType docType = MDocType.get(getCtx(), p_C_DocTypeReval_ID);
			cat = MGLCategory.get(getCtx(), docType.getGL_Category_ID());
		}
		//
		MJournalBatch batch = new MJournalBatch(getCtx(), 0, get_TrxName());
		batch.setDescription (getName());
		batch.setC_DocType_ID(p_C_DocTypeReval_ID);
		batch.setDateDoc(new Timestamp(System.currentTimeMillis()));
		batch.setDateAcct(p_StatementDate);
		batch.setC_Currency_ID(as.getC_Currency_ID());
		if (!batch.save())
			return " - Could not create Batch";
		//
		MJournal journal = null;
		BigDecimal drTotal = Env.ZERO;
		BigDecimal crTotal = Env.ZERO;
		int AD_Org_ID = 0;
		for (int i = 0; i < list.size(); i++)
		{
			X_T_CombinedArApRevalue gl = list.get(i);
			if (gl.getAmtRevalDiff().signum() == 0)
				continue;

			if ( gl.getC_Invoice_ID() > 0 )
			{
				MInvoice invoice = new MInvoice(getCtx(), gl.getC_Invoice_ID(), null);
				if (invoice.getC_Currency_ID() == as.getC_Currency_ID())
					continue;
				//
				if (journal == null)
				{
					journal = new MJournal (batch);
					journal.setC_AcctSchema_ID (as.getC_AcctSchema_ID());
					journal.setC_Currency_ID(as.getC_Currency_ID());
					journal.setC_ConversionType_ID(p_C_ConversionType_ID);
					MOrg org = MOrg.get(getCtx(), gl.getAD_Org_ID());
					journal.setDescription (getName() + " - " + org.getName());
					journal.setGL_Category_ID (cat.getGL_Category_ID());
					if (!journal.save())
						return " - Could not create Journal";
				}
				//
				MJournalLine line = new MJournalLine(journal);
				line.setLine((i+1) * 10);
				line.setDescription(invoice.getSummary());

				//
				String sqlFA = "SELECT fa.Fact_Acct_ID FROM Fact_Acct fa " +
						" JOIN C_Invoice i ON (fa.AD_Table_ID=318 AND fa.Record_ID=i.C_Invoice_ID) "
						+ " WHERE fa.Line_ID IS NULL  AND fa.C_Tax_ID IS NULL "
						+ " AND i.C_Invoice_ID = ?";

				int Fact_Acct_ID = DB.getSQLValue(get_TrxName(), sqlFA, gl.getC_Invoice_ID());
				//
				MFactAcct fa = new MFactAcct (getCtx(), Fact_Acct_ID, null);
				line.setC_ValidCombination_ID(MAccount.get(fa));
				
				BigDecimal amtReval = gl.getAmtRevalDiff();
				
				if ( !invoice.isSOTrx() )
				{
					amtReval = amtReval.negate();
				}
				
				BigDecimal dr = amtReval.compareTo(Env.ZERO) > 0 ? amtReval : Env.ZERO;
				BigDecimal cr = amtReval.compareTo(Env.ZERO) < 0 ? amtReval.negate() :  Env.ZERO;
				
				drTotal = drTotal.add(dr);
				crTotal = crTotal.add(cr);
				line.setAmtSourceDr (dr);
				line.setAmtAcctDr (dr);
				line.setAmtSourceCr (cr);
				line.setAmtAcctCr (cr);
				line.saveEx();
				//
				if (AD_Org_ID == 0)		//	invoice org id
					AD_Org_ID = gl.getAD_Org_ID();
			}
			else //if ( gl.getC_Payment_ID() > 0 )
			{
				MPayment payment = new MPayment(getCtx(), gl.getC_Payment_ID(), null);
				if (payment.getC_Currency_ID() == as.getC_Currency_ID())
					continue;
				//
				if (journal == null)
				{
					journal = new MJournal (batch);
					journal.setC_AcctSchema_ID (as.getC_AcctSchema_ID());
					journal.setC_Currency_ID(as.getC_Currency_ID());
					journal.setC_ConversionType_ID(p_C_ConversionType_ID);
					MOrg org = MOrg.get(getCtx(), gl.getAD_Org_ID());
					journal.setDescription (getName() + " - " + org.getName());
					journal.setGL_Category_ID (cat.getGL_Category_ID());
					if (!journal.save())
						return " - Could not create Journal";
				}
				//
				MJournalLine line = new MJournalLine(journal);
				line.setLine((i+1) * 10);
				line.setDescription(payment.getSummary());

				//
				String sqlFA = "SELECT fa.Fact_Acct_ID FROM Fact_Acct fa " +
						" JOIN C_Payment p ON (fa.AD_Table_ID=335 AND fa.Record_ID=p.C_Payment_ID) "
						+ " WHERE ((AmtAcctCR <> 0 AND p.IsReceipt='Y') OR (AmtAcctDr <> 0 AND p.IsReceipt = 'N')) "
						+ " AND p.C_Payment_ID = ?";

				int Fact_Acct_ID = DB.getSQLValue(get_TrxName(), sqlFA, payment.getC_Payment_ID());
				//
				MFactAcct fa = new MFactAcct (getCtx(), Fact_Acct_ID, null);
				line.setC_ValidCombination_ID(MAccount.get(fa));
				
				BigDecimal amtReval = gl.getAmtRevalDiff();
				
				if ( !payment.isReceipt() )
				{
					amtReval = amtReval.negate();
				}
				
				BigDecimal dr = amtReval.compareTo(Env.ZERO) > 0 ? amtReval : Env.ZERO;
				BigDecimal cr = amtReval.compareTo(Env.ZERO) < 0 ? amtReval.negate() :  Env.ZERO;
				
				drTotal = drTotal.add(dr);
				crTotal = crTotal.add(cr);
				line.setAmtSourceDr (dr);
				line.setAmtAcctDr (dr);
				line.setAmtSourceCr (cr);
				line.setAmtAcctCr (cr);
				line.saveEx();
				//
				if (AD_Org_ID == 0)		//	invoice org id
					AD_Org_ID = gl.getAD_Org_ID();
			}
			//	Change in Org
			if (AD_Org_ID != gl.getAD_Org_ID())
			{
				createBalancing (asDefaultAccts, journal, drTotal, crTotal, AD_Org_ID, (i+1) * 10);
				//
				AD_Org_ID = gl.getAD_Org_ID();
				drTotal = Env.ZERO;
				crTotal = Env.ZERO;
				journal = null;
			}
		}
		if ( drTotal.signum() != 0 || crTotal.signum() != 0 )
			createBalancing (asDefaultAccts, journal, drTotal, crTotal, AD_Org_ID, (list.size()+1) * 10);
		
		return " - " + batch.getDocumentNo() + " #" + list.size();
	}	//	createGLJournal

	/**
	 * 	Create Balancing Entry
	 *	@param asDefaultAccts acct schema default accounts
	 *	@param journal journal
	 *	@param drTotal dr
	 *	@param crTotal cr
	 *	@param AD_Org_ID org
	 *	@param lineNo base line no
	 */
	private void createBalancing (MAcctSchemaDefault asDefaultAccts, MJournal journal, 
		BigDecimal drTotal, BigDecimal crTotal, int AD_Org_ID, int lineNo)
	{
		if (journal == null)
			throw new IllegalArgumentException("Journal is null");
		//		CR Entry = Gain
		if (drTotal.signum() != 0)
		{
			MJournalLine line = new MJournalLine(journal);
			line.setLine(lineNo+1);
			MAccount base = MAccount.get(getCtx(), asDefaultAccts.getUnrealizedGain_Acct());
			MAccount acct = MAccount.get(getCtx(), asDefaultAccts.getAD_Client_ID(), AD_Org_ID, 
				asDefaultAccts.getC_AcctSchema_ID(), base.getAccount_ID(), base.getC_SubAcct_ID(),
				base.getM_Product_ID(), base.getC_BPartner_ID(), base.getAD_OrgTrx_ID(), 
				base.getC_LocFrom_ID(), base.getC_LocTo_ID(), base.getC_SalesRegion_ID(), 
				base.getC_Project_ID(), base.getC_Campaign_ID(), base.getC_Activity_ID(),
				base.getUser1_ID(), base.getUser2_ID(), base.getUserElement1_ID(), base.getUserElement2_ID(), null);
			line.setDescription(Msg.getElement(getCtx(), "UnrealizedGain_Acct"));
			line.setC_ValidCombination_ID(acct.getC_ValidCombination_ID());
			line.setAmtSourceCr (drTotal);
			line.setAmtAcctCr (drTotal);
			line.saveEx();
		}
		//	DR Entry = Loss
		if (crTotal.signum() != 0)
		{
			MJournalLine line = new MJournalLine(journal);
			line.setLine(lineNo+2);
			MAccount base = MAccount.get(getCtx(), asDefaultAccts.getUnrealizedLoss_Acct());
			MAccount acct = MAccount.get(getCtx(), asDefaultAccts.getAD_Client_ID(), AD_Org_ID, 
				asDefaultAccts.getC_AcctSchema_ID(), base.getAccount_ID(), base.getC_SubAcct_ID(),
				base.getM_Product_ID(), base.getC_BPartner_ID(), base.getAD_OrgTrx_ID(), 
				base.getC_LocFrom_ID(), base.getC_LocTo_ID(), base.getC_SalesRegion_ID(), 
				base.getC_Project_ID(), base.getC_Campaign_ID(), base.getC_Activity_ID(),
				base.getUser1_ID(), base.getUser2_ID(), base.getUserElement1_ID(), base.getUserElement2_ID(), null);
			line.setDescription(Msg.getElement(getCtx(), "UnrealizedLoss_Acct"));
			line.setC_ValidCombination_ID(acct.getC_ValidCombination_ID());
			line.setAmtSourceDr (crTotal);
			line.setAmtAcctDr (crTotal);
			line.saveEx();
		}
	}	//	createBalancing

}	//	Aging

