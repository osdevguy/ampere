package org.compiere.process;


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;


public class VendorReceiptGenerate extends SvrProcess {

	
    private static final CLogger logger = CLogger
            .getCLogger(VendorReceiptGenerate.class);
    private int p_C_Order_ID;
    
	@Override
	protected void prepare() {

		p_C_Order_ID = getRecord_ID();
	}

	@Override
	protected String doIt() throws Exception {

		String result = null;
		int nlines = 0;

        String sql = "SELECT ol.c_orderline_id, coalesce (p.m_locator_id, 0) as m_locator_id " +
                ", ol.qtyentered - coalesce(rl.qtyreceived, 0) as qty " +
                "from c_orderline ol " +
                "left join m_product  p on ol.m_product_id = p.m_product_id " +
                "left join ( " +
                "  select iol.c_orderline_id, sum(iol.movementqty) as qtyreceived " +
                "  from m_inout io " +
                "  inner join m_inoutline iol on io.m_inout_id = iol.m_inout_id " +
                "  where io.docstatus IN ('DR', 'IP', 'CO', 'CL') " +
                "  group by iol.c_orderline_id " +
                "  ) rl on ol.c_orderline_id = rl.c_orderline_id " +
                "where ol.c_order_id= ? " +
                "and ol.qtyentered - coalesce(rl.qtyreceived, 0) > 0";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try {
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(1, p_C_Order_ID);
            rs = pstmt.executeQuery();

            MOrder order = null;
            MInOut receipt = null;
            MInOutLine receiptLine = null;
            MOrderLine oLine = null;
            while (rs.next())
            {
        		if (receipt == null)
        		{
        			order = new MOrder(Env.getCtx(), p_C_Order_ID, get_TrxName());
        			
        			if (order.getC_DocType().getC_DocTypeShipment().getC_DocType_ID() == 0) {
        				throw new IllegalStateException("No Shipment Document defined in DocType=" + order.getC_DocType().getName());
        			}
        			receipt = new MInOut (order, 0, Env.getContextAsDate(getCtx(), "#Date"));
        			receipt.setM_Warehouse_ID(order.getM_Warehouse_ID());	//	sets Org too
        			if (!receipt.save()) {
        				String error = CLogger.retrieveErrorString("");
        				throw new IllegalStateException("Could not create Receipt -" + error);
        			}
        			
        			result = "Material Receipt#" + receipt.getDocumentNo();
        		}
        		
        		int C_OrderLine_ID = rs.getInt(1);
        		int M_Locator_ID = rs.getInt(2);
        		BigDecimal Qty = rs.getBigDecimal(3);
				oLine = new MOrderLine(getCtx(), C_OrderLine_ID, get_TrxName());
				receiptLine = new MInOutLine (receipt);
				receiptLine.setOrderLine(oLine, M_Locator_ID, Qty);
				receiptLine.setQty(Qty);
				receiptLine.setMovementQty(Qty
						.multiply(oLine.getQtyOrdered())
						.divide(oLine.getQtyEntered(), 12, BigDecimal.ROUND_HALF_UP));
				receiptLine.setC_UOM_ID(oLine.getC_UOM_ID());				
    			if (!receiptLine.save()) {
    				String error = CLogger.retrieveErrorString("");
    				throw new IllegalStateException("Could not create Receipt Line -" + error);
    			}
    			nlines++;
            }
            

        } catch (SQLException ex)
	    {
	        logger.log(Level.SEVERE, "Could not asset account information", ex);
	    }
	    finally
	    {
	        DB.close(rs, pstmt);
	    }        
        if (nlines == 0) {
        	result = "No Receipt Lines Created";
        }
        return result;
     }

}
