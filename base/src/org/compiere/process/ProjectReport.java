package org.compiere.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MClientInfo;
import org.compiere.model.MInventory;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MProductPricing;
import org.compiere.model.MProject;
import org.compiere.model.MProjectLine;
import org.compiere.model.MTimesheetEntry;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Nikunj Panelia
 *
 */
public class ProjectReport extends SvrProcess
{
	/** Project			*/
	private int			p_C_Project_ID = 0;
	private int 		ad_PInstance_ID=0;
	private int 		p_C_Acctschema_ID=0;
	private int  		ad_Client_ID  =0;
	private int   		ad_Org_ID=0;
	private MProject    project=null;
	private Timestamp		datefrom=null;
	private Timestamp		dateto=null;
	
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_Project_ID"))
				p_C_Project_ID = para[i].getParameterAsInt();
			else if (name.equals("C_AcctSchema_ID"))
				p_C_Acctschema_ID = para[i].getParameterAsInt();
			else if (name.equals("DateAcctFrom"))
			{
				datefrom =(Timestamp)para[i].getParameterAsTimestamp();
			}
			else if (name.equals("DateAcctTo"))
			{
				dateto = (Timestamp)para[i].getParameterAsTimestamp();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	protected String doIt() throws Exception 
	{
		ad_PInstance_ID=getAD_PInstance_ID();
		project =new MProject(getCtx(), p_C_Project_ID, get_TrxName());
		ad_Client_ID=getAD_Client_ID();
		ad_Org_ID=Env.getAD_Org_ID(Env.getCtx());
		fillTable();
	
		return null;
	}

	private void fillTable()
	{		
		
		MClientInfo info=MClientInfo.get(getCtx(), getAD_Client_ID());
		int cogsFrom=info.get_ValueAsInt("COGS_From");
		int cogsTo=info.get_ValueAsInt("COGS_To");
		int expenseFrom=info.get_ValueAsInt("Product_Expense_From");
		int expenseTo=info.get_ValueAsInt("Product_Expense_To");
		int revenueFrom=info.get_ValueAsInt("Revenue_From");
		int revenueTo=info.get_ValueAsInt("Revenue_To");
		
		
		if( cogsFrom < 0 || cogsTo < 0 || expenseFrom < 0 || expenseTo < 0 || revenueFrom < 0 || revenueTo < 0 )
		{
			throw new AdempiereException("Please configure accounts ranges in Client > Client Info Tab.");
		}
		
		StringBuffer sql=new StringBuffer("INSERT INTO T_ProjectReport")
				.append(" (Description,AD_PInstance_ID,C_Project_ID,C_ProjectPhase_ID,C_ProjectTask_ID,AD_Client_ID,AD_Org_ID,SeqNo ) ")
				.append(" SELECT 'Tech Time' ,?,?,p.C_ProjectPhase_ID,t.C_ProjectTask_ID,?,?,1  from C_ProjectTask  t inner join C_ProjectPhase p on p.C_ProjectPhase_ID=t.C_ProjectPhase_ID "
						+ " where C_Project_ID=? UNION ALL")
				.append(" SELECT 'Materials'  ,?,?,p.C_ProjectPhase_ID,t.C_ProjectTask_ID,?,?,2  from C_ProjectTask  t inner join C_ProjectPhase p on p.C_ProjectPhase_ID=t.C_ProjectPhase_ID  "
						+ " where C_Project_ID=? UNION ALL")
				.append(" SELECT 'Recharge Services',?,?,p.C_ProjectPhase_ID,t.C_ProjectTask_ID,?,?,3  from C_ProjectTask  t inner join C_ProjectPhase p on p.C_ProjectPhase_ID=t.C_ProjectPhase_ID "
						+ " where C_Project_ID=? UNION ALL")
				.append(" SELECT 'Unprocessed Hours',?,?,p.C_ProjectPhase_ID,t.C_ProjectTask_ID,?,?,4  from C_ProjectTask  t inner join C_ProjectPhase p on p.C_ProjectPhase_ID=t.C_ProjectPhase_ID  "
						+ " where C_Project_ID=? ");
		
		PreparedStatement pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try
		{			
			pstmt.setInt(1,ad_PInstance_ID);
			pstmt.setInt(2, p_C_Project_ID);
			pstmt.setInt(3, ad_Client_ID);
			pstmt.setInt(4, ad_Org_ID);
			pstmt.setInt(5, p_C_Project_ID);
			pstmt.setInt(6, ad_PInstance_ID);
			pstmt.setInt(7, p_C_Project_ID);
			pstmt.setInt(8, ad_Client_ID);
			pstmt.setInt(9, ad_Org_ID);
			pstmt.setInt(10, p_C_Project_ID);
			pstmt.setInt(11, ad_PInstance_ID);
			pstmt.setInt(12, p_C_Project_ID);
			pstmt.setInt(13, ad_Client_ID);
			pstmt.setInt(14, ad_Org_ID);
			pstmt.setInt(15, p_C_Project_ID);
			pstmt.setInt(16, ad_PInstance_ID);
			pstmt.setInt(17, p_C_Project_ID);
			pstmt.setInt(18, ad_Client_ID);
			pstmt.setInt(19, ad_Org_ID);
			pstmt.setInt(20, p_C_Project_ID);					

			pstmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "Can not create project report record.");
			throw new AdempiereException(
					"Can not create project report record.", e);
		} 
		finally 
		{
			DB.close(pstmt);
		}
		
		sql=new StringBuffer("INSERT INTO T_ProjectReport")
		 	.append(" (Description,AD_PInstance_ID,C_Project_ID,C_ProjectPhase_ID,AD_Client_ID,AD_Org_ID,SeqNo ) ")
		 	.append(" SELECT 'Sales Invoices' ,?,?,C_ProjectPhase_ID,?,?,5  from C_ProjectPhase  "
				+ " where C_Project_ID=?");
		 
		 
		pstmt = DB.prepareStatement(sql.toString(),
					get_TrxName());
		try
		{			
			pstmt.setInt(1,ad_PInstance_ID);
			pstmt.setInt(2,p_C_Project_ID);
			pstmt.setInt(3,ad_Client_ID);
			pstmt.setInt(4,ad_Org_ID);
			pstmt.setInt(5, p_C_Project_ID);
				
			pstmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
				log.log(Level.SEVERE, "Can not update sales Invoices line.");
			throw new AdempiereException(
						"Can not update sales Invoices line.", e);
		} 
		finally 
		{
				DB.close(pstmt);
		}
			
		sql=new StringBuffer("INSERT INTO T_ProjectReport")
	 	.append(" (Description,AD_PInstance_ID,C_Project_ID,AD_Client_ID,AD_Org_ID,SeqNo ) values ")
	 	.append(" ('Total',?,?,?,?,6  )");
	 
	 
		pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try
		{			
			pstmt.setInt(1,ad_PInstance_ID);
			pstmt.setInt(2,p_C_Project_ID);
			pstmt.setInt(3,ad_Client_ID);
			pstmt.setInt(4,ad_Org_ID);
			
			pstmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
				log.log(Level.SEVERE, "Can not update sales Invoices line.");
			throw new AdempiereException(
					"Can not update sales Invoices line.", e);
		} 
		finally 
		{
			DB.close(pstmt);
		}
		//update tech time		

		sql = new StringBuffer("UPDATE T_ProjectReport t")
			.append(" SET ")
			
			.append(" Hours = (Select SUM(QtyEntered) from S_TimesheetEntry s where Processed='Y' and s.C_ProjectTask_ID=t.C_ProjectTask_ID "
					+ " and s.C_ProjectPhase_ID=t.C_ProjectPhase_ID and s.C_Project_ID=t.C_Project_ID ")
					.append(getDateRange("s.DateAcct"))
					.append(" ) , " )
					
			.append(" Cost =(SELECT sum(amtacctdr) from Fact_Acct fa where ad_table_id=? and fa.C_ProjectPhase_ID=t.C_ProjectPhase_ID and fa.C_ProjectTask_Id=t.C_ProjectTask_ID"
					+ " and fa.postingtype='A' AND fa.account_id in( select C_ElementValue_ID from C_ElementValue where value::int between ? and ? and ad_client_ID=? ) "
					+ " and record_id in (select S_TimesheetEntry_ID from S_TimesheetEntry where C_Project_ID=t.C_Project_ID  )")
					.append(getDateRange("fa.DateAcct"))
					.append(" )  " )
					
			.append(" where t.description='Tech Time' and t.AD_PInstance_ID=?");
		pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try
		{			
			pstmt.setInt(1,MTimesheetEntry.Table_ID);
			pstmt.setInt(2,cogsFrom);
			pstmt.setInt(3,cogsTo);
			pstmt.setInt(4,getAD_Client_ID());
			pstmt.setInt(5, ad_PInstance_ID);
			
			pstmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "Can not update tech time.");
			throw new AdempiereException(
					"Can not update tech time", e);
		} 
		finally 
		{
			DB.close(pstmt);
		}
		
		//update tech time  and unprocessed line price
		ResultSet rs=null;
		StringBuffer sqlPrice=new StringBuffer("UPDATE T_ProjectReport  set Price=(?) where C_Project_ID=? and C_ProjectPhase_ID=?  and C_ProjectTask_ID=?")
		.append(" and description='Tech Time' and AD_PInstance_ID=?");
		StringBuffer sqlPriceUnprocessed=new StringBuffer("UPDATE T_ProjectReport  set Price=(?) where C_Project_ID=? and C_ProjectPhase_ID=?  and C_ProjectTask_ID=?")
		.append(" and description='Unprocessed Hours' and AD_PInstance_ID=?");
		
		sql=new StringBuffer("SELECT C_Project_ID,C_ProjectPhase_ID,C_ProjectTask_ID from T_ProjectReport ")
				.append(" where description='Tech Time' and AD_PInstance_ID=?");
		
		pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try
		{			

			pstmt.setInt(1, ad_PInstance_ID);
			
			rs=pstmt.executeQuery();
			while(rs.next())
			{
				getMaterialPrice(rs.getInt("C_Project_ID"), rs.getInt("C_ProjectPhase_ID"), rs.getInt("C_ProjectTask_ID"));
				if(priceProcessed.compareTo(BigDecimal.ZERO)!=0)
				{
					DB.executeUpdateEx(sqlPrice.toString(),new Object[]{priceProcessed, rs.getInt("C_Project_ID"),rs.getInt("C_ProjectPhase_ID"),rs.getInt("C_ProjectTask_ID"),ad_PInstance_ID}
						, get_TrxName());
				}
				if(priceUnProcessed.compareTo(BigDecimal.ZERO)!=0)
				{
					DB.executeUpdateEx(sqlPriceUnprocessed.toString(),new Object[]{priceUnProcessed, rs.getInt("C_Project_ID"),rs.getInt("C_ProjectPhase_ID"),rs.getInt("C_ProjectTask_ID"),ad_PInstance_ID}
					, get_TrxName());
				}
			}
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "Can not update Tech Time price.");
			throw new AdempiereException(
					"Can not update Tech Time price.", e);
		} 
		finally 
		{
			DB.close(pstmt);
		}
		
		// update Unprocessed hours
		sql=new StringBuffer(" UPDATE T_ProjectReport t ")
			.append("SET ")
			
			.append( "Hours = (Select SUM(QtyEntered) from S_TimesheetEntry s where  Processed='N' and s.C_ProjectTask_ID=t.C_ProjectTask_ID and "
					+ "s.C_ProjectPhase_ID=t.C_ProjectPhase_ID and s.C_Project_ID=t.C_Project_ID ")
					.append(getDateRange("s.DateAcct"))
					.append(" ) , " )
					
			.append(" Cost =(select sum(s.qtyentered*mc.CurrentCostPrice) from S_TimesheetEntry s inner join m_Product m on "
					+ " m.S_Resource_ID = s.S_Resource_ID inner join M_Cost mc on mc.m_product_id=m.m_product_id inner join c_acctschema  "
					+ " asm on asm.c_acctschema_id=mc.c_acctschema_id inner join m_costelement ce on ce.m_costelement_id=mc.m_costelement_id AND "
					+ " ce.costingmethod=asm.costingmethod Where mc.c_acctschema_id=? and  s.c_projecttask_id=t.c_projecttask_id "
					+ " and s.C_ProjectPhase_ID=t.C_ProjectPhase_ID and s.C_Project_ID=t.C_Project_ID "
					+ " and s.Processed='N' ")
					.append(getDateRange("s.DateAcct"))
					.append(" )  " )
		
			.append("where t.description='Unprocessed Hours' and t.AD_PInstance_ID=? ");
		
		pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try
		{			
			pstmt.setInt(1,p_C_Acctschema_ID);
			pstmt.setInt(2, ad_PInstance_ID);
			
			pstmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "Can not update unprocessed time.");
			throw new AdempiereException(
					"Can not update unprocessed time.", e);
		} 
		finally 
		{
			DB.close(pstmt);
		}

		
		//update Material hours
		sql=new StringBuffer(" UPDATE T_ProjectReport t SET  ")
		
		.append( "Hours =( select sum(QtyInternalUse) from M_InventoryLine il inner join M_Inventory i on i.M_Inventory_ID=il.M_Inventory_ID where "
				+ " i.C_project_ID= t.c_project_ID and il.C_ProjectPhase_ID=t.C_ProjectPhase_ID and il.c_projectTask_ID=t.C_ProjectTask_ID and i.Processed='Y'"
				+ " and i.C_DocType_ID=(select C_DocType_ID from C_DocType where name='Issue Material to Project') ")
				.append(getDateRange("i.MovementDate"))
				.append(" ) , " )
				
		.append(" Cost =( SELECT sum(amtacctdr-amtacctcr) from Fact_Acct fa where fa.ad_table_id=? and  fa.C_ProjectPhase_ID=t.C_ProjectPhase_ID and fa.C_ProjectTask_Id=t.C_ProjectTask_ID"
				+ " and fa.postingtype='A' AND fa.account_id in( select C_ElementValue_ID from C_ElementValue where value::int between ? and ? and ad_client_ID=? )"
				+ " and fa.record_id in (select M_Inventory_ID from M_Inventory where C_Project_ID=t.C_Project_ID and "
				+ " C_DocType_ID=(select C_DocType_ID from C_DocType where name='Issue Material to Project')) ")
				.append(getDateRange("fa.DateAcct"))
				.append(" )  " )
				
		
		.append("where t.description='Materials' and t.AD_PInstance_ID=? ");
		
		pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try
		{			
			pstmt.setInt(1,MInventory.Table_ID);
			pstmt.setInt(2,cogsFrom);
			pstmt.setInt(3,cogsTo);
			pstmt.setInt(4,getAD_Client_ID());
			pstmt.setInt(5,ad_PInstance_ID);
			
			pstmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "Can not update Material hours.");
			throw new AdempiereException(
					"Can not update Material hours.", e);
		} 
		finally 
		{
			DB.close(pstmt);
		}
		//update material time price
		sqlPrice=new StringBuffer("UPDATE T_ProjectReport  set Price=(?) where C_Project_ID=? and C_ProjectPhase_ID=?  and C_ProjectTask_ID=?")
			.append(" and description='Materials' and AD_PInstance_ID=?");
		sql=new StringBuffer("SELECT C_Project_ID,C_ProjectPhase_ID,C_ProjectTask_ID from T_ProjectReport ")
			.append(" where description='Materials' and AD_PInstance_ID=?");
				
		pstmt = DB.prepareStatement(sql.toString(),
					get_TrxName());
		try
		{			

			pstmt.setInt(1, ad_PInstance_ID);
					
			rs=pstmt.executeQuery();
			while(rs.next())
			{
				BigDecimal priceLine=BigDecimal.ZERO;
				priceLine=getLinePrice(rs.getInt("C_Project_ID"), rs.getInt("C_ProjectPhase_ID"), rs.getInt("C_ProjectTask_ID"));
				if(priceLine.compareTo(BigDecimal.ZERO)!=0)
				{
						DB.executeUpdateEx(sqlPrice.toString(),new Object[]{priceLine, rs.getInt("C_Project_ID"),rs.getInt("C_ProjectPhase_ID"),rs.getInt("C_ProjectTask_ID"),ad_PInstance_ID}
								, get_TrxName());
				}
			}
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "Can not update material price.");
				throw new AdempiereException(
						"Can not update material price.", e);
		} 
		finally 
		{
			DB.close(pstmt);
		}
				
		//Recharge Services 

		sql=new StringBuffer("UPDATE T_ProjectReport t " )
		.append(" SET Cost =( SELECT sum(amtacctdr) from Fact_Acct fa where fa.ad_table_id=? and  fa.C_ProjectPhase_ID=t.C_ProjectPhase_ID and"
				+ " fa.C_ProjectTask_Id=t.C_ProjectTask_ID " ) 
		.append("AND fa.account_id in( select C_ElementValue_ID from C_ElementValue where value::int between ? and ? and ad_client_ID=? ) and fa.record_id in ") 
		.append("(select il.C_Invoice_ID from C_InvoiceLine il join C_invoice i on il.C_Invoice_ID=i.C_Invoice_ID where il.C_Project_ID=t.C_Project_ID and i.issotrx='N' ) ")
				.append(getDateRange("fa.DateAcct"))
				.append(" )  " )	
		.append("where t.description='Recharge Services'  and t.AD_PInstance_ID=? ");
		pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try
		{			
			pstmt.setInt(1,MInvoice.Table_ID);
			pstmt.setInt(2,expenseFrom);
			pstmt.setInt(3,expenseTo);
			pstmt.setInt(4,getAD_Client_ID());
			pstmt.setInt(5, ad_PInstance_ID);
			
			pstmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "Can not update Recharge Services .");
			throw new AdempiereException(
					"Can not update Recharge Services.", e);
		} 
		finally 
		{
			DB.close(pstmt);
		}
		//Sales invoice
		
		sql=new StringBuffer("UPDATE T_ProjectReport t " )
		.append(" SET InvoicedAmt =( SELECT sum(amtsourcecr) from Fact_Acct fa where fa.ad_table_id=? and  fa.C_ProjectPhase_ID=t.C_ProjectPhase_ID and ")
		.append(" fa.account_id in( select C_ElementValue_ID from C_ElementValue where value::int between ? and ? and ad_client_ID=? ) and fa.record_id in ") 
		.append("(select il.C_Invoice_ID from C_InvoiceLine il join C_invoice i on il.C_Invoice_ID=i.C_Invoice_ID where il.C_Project_ID=t.C_Project_ID and i.issotrx='Y' ) ")
				.append(getDateRange("fa.DateAcct"))
				.append(" )  " )	
		.append(" where t.description='Sales Invoices'  and t.AD_PInstance_ID=? ");
		pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try
		{			
			pstmt.setInt(1,MInvoice.Table_ID);
			pstmt.setInt(2,revenueFrom);
			pstmt.setInt(3,revenueTo);
			pstmt.setInt(4,getAD_Client_ID());
			pstmt.setInt(5, ad_PInstance_ID);
			
			pstmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "Can not update Sales invoice.");
			throw new AdempiereException(
					"Can not update Sales invoice.", e);
		} 
		finally 
		{
			DB.close(pstmt);
		}
		
		BigDecimal revenueTotal=DB.getSQLValueBD(get_TrxName(),"select sum(InvoicedAmt) from T_ProjectReport where ad_pinstance_id= ? and description='Sales Invoices' ", ad_PInstance_ID  );
		BigDecimal expenceTotal = DB.getSQLValueBD(get_TrxName(),"select sum(cost) from T_ProjectReport where ad_pinstance_id= ? and description in ('Materials','Tech Time')", ad_PInstance_ID );
		sqlPrice=new StringBuffer("UPDATE T_ProjectReport  set Price=(?) where ")
			.append(" description='Total' and AD_PInstance_ID=? ");	
		if(revenueTotal==null)
			revenueTotal=BigDecimal.ZERO;
		if(expenceTotal==null)
			expenceTotal=BigDecimal.ZERO;
		
		DB.executeUpdateEx(sqlPrice.toString(),new Object[]{revenueTotal.subtract(expenceTotal),ad_PInstance_ID}, get_TrxName());
		 
		
	}

	
	private String getDateRange(String colName) 
	{
		StringBuffer sb=new StringBuffer();
		if(datefrom !=null && dateto !=null)
		{
			sb.append(" and ").append(colName).append("  between '")
					.append(datefrom).append("' and '" ).append( dateto ).append("'");
					
		}
		else if(datefrom!=null)
		{
			sb.append(" and ").append(colName).append(" >= '").append(datefrom).append("'");
		}
		else if(dateto!=null)
		{
			sb.append(" and ").append(colName).append(" <= '").append(dateto).append("'");
		}
		return sb.toString();
	}

	BigDecimal priceProcessed;
	BigDecimal priceUnProcessed;
	private void getMaterialPrice (int project_ID,int phase_ID,int task_ID)
	{
		priceProcessed=BigDecimal.ZERO;
		priceUnProcessed=BigDecimal.ZERO;
		MProductPricing m_productPrice=null;
		MTimesheetEntry[] entries= getEntries(Env.getCtx(),project_ID,phase_ID,task_ID);
		
		for(MTimesheetEntry entry : entries)
		{
			m_productPrice = new MProductPricing (entry.getM_Product_ID(), 
					entry.getC_BPartner_ID(), entry.getQtyEntered()	, false);
			m_productPrice.setM_PriceList_ID(project.getM_PriceList_ID());
			m_productPrice.setM_PriceList_Version_ID(project.getM_PriceList_Version_ID());
			m_productPrice.setPriceDate(entry.getDateAcct());
			m_productPrice.calculatePrice();
			if(entry.isProcessed())				
				priceProcessed=priceProcessed.add(entry.getQtyEntered().multiply(m_productPrice.getPriceStd()));
			else
				priceUnProcessed=priceUnProcessed.add(entry.getQtyEntered().multiply(m_productPrice.getPriceStd()));
		}	
	}	
	
	public  MTimesheetEntry[] getEntries (Properties ctx,int project_ID,int phase_ID,int task_ID)
	{
		StringBuffer whereClause=new StringBuffer();
		whereClause.append(" C_Project_ID=? and C_ProjectPhase_ID=? and C_ProjectTask_ID=? and AD_Client_ID=? ");
		whereClause.append(getDateRange("dateacct"));
		
		Query q = new Query(ctx, MTimesheetEntry.Table_Name, whereClause.toString(), null);
		
		q.setParameters(project_ID, phase_ID, task_ID , getAD_Client_ID());
		
		List <MProjectLine> list =q.list(); 
		MTimesheetEntry[] retValue = new MTimesheetEntry[list.size()];
		list.toArray(retValue);
		return retValue;
	}
	private BigDecimal getLinePrice (int project_ID,int phase_ID,int task_ID)
	{
		BigDecimal totalPrice=BigDecimal.ZERO;
		MProductPricing m_productPrice=null;
		MInventoryLine[] entries= getLines(Env.getCtx(),project_ID,phase_ID,task_ID);
		
		for(MInventoryLine line : entries)
		{
			if(!line.getParent().isProcessed())
				continue;
			MProject proj=new MProject(getCtx(), line.get_ValueAsInt("C_Project_ID"), get_TrxName());
			m_productPrice = new MProductPricing (line.getM_Product_ID(), 
					proj.getC_BPartner_ID(), line.getQtyInternalUse()	, false);
			m_productPrice.setM_PriceList_ID(project.getM_PriceList_ID());
			m_productPrice.setM_PriceList_Version_ID(project.getM_PriceList_Version_ID());
			m_productPrice.setPriceDate(line.getParent().getMovementDate());
			m_productPrice.calculatePrice();
			totalPrice=totalPrice.add(line.getQtyInternalUse().multiply(m_productPrice.getPriceStd()));
		}
		
		return totalPrice;
	}	
	
	public  MInventoryLine[] getLines (Properties ctx,int project_ID,int phase_ID,int task_ID)
	{
		StringBuffer whereClause=new StringBuffer();
		whereClause.append(" C_Project_ID=? and C_ProjectPhase_ID=? and C_ProjectTask_ID=? and AD_Client_ID=? ");
		whereClause.append(" and M_Inventory_id in (select M_Inventory_id from M_Inventory where processed='Y' ").append(getDateRange("MovementDate")).append(")");
		Query q = new Query(ctx, MInventoryLine.Table_Name , whereClause.toString(), null);
		
		q.setParameters(project_ID, phase_ID, task_ID , getAD_Client_ID());
		
		List <MProjectLine> list =q.list(); 
		MInventoryLine[] retValue = new MInventoryLine[list.size()];
		list.toArray(retValue);
		return retValue;
	}
}