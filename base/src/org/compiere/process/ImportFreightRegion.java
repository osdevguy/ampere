/**********************************************************************
* This file is part of Adempiere ERP Bazaar                           *
* http://www.adempiere.org                                            *
*                                                                     *
* Copyright (C) Contributors                                          *
*                                                                     *
* This program is free software; you can redistribute it and/or       *
* modify it under the terms of the GNU General Public License         *
* as published by the Free Software Foundation; either version 2      *
* of the License, or (at your option) any later version.              *
*                                                                     *
* This program is distributed in the hope that it will be useful,     *
* but WITHOUT ANY WARRANTY; without even the implied warranty of      *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the        *
* GNU General Public License for more details.                        *
*                                                                     *
* You should have received a copy of the GNU General Public License   *
* along with this program; if not, write to the Free Software         *
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,          *
* MA 02110-1301, USA.                                                 *
*                                                                     *
* Contributors:                                                       *
* - Carlos Ruiz - globalqss                                           *
***********************************************************************/
package org.compiere.process;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MFreightRegion;
import org.compiere.model.MFreightRegionLocation;
import org.compiere.model.MShipper;
import org.compiere.model.Query;
import org.compiere.model.X_I_FreightRegion;
import org.compiere.util.DB;
//import org.compiere.model.X_M_ProductPriceVendorBreak;  adaxa-pb not used

/**
 *	Import Freight Region
 *
 * 	@author Paul Bowden
 */
public class ImportFreightRegion extends SvrProcess
{
	/**	Client to be imported to		*/
	private int				m_AD_Client_ID = 0;
	/**	Delete old Imported				*/
	private boolean			m_deleteOldImported = false;
	private int m_AD_Org_ID = 0;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("AD_Client_ID"))
				m_AD_Client_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("AD_Org_ID"))
				m_AD_Org_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("DeleteOldImported"))
				m_deleteOldImported = "Y".equals(para[i].getParameter());
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}	//	prepare


	/**
	 *  Perform process.
	 *  @return Message
	 *  @throws Exception
	 */
	protected String doIt() throws Exception
	{
		StringBuffer sql = null;
		int no = 0;
		String clientCheck = " AND AD_Client_ID=" + m_AD_Client_ID;
		
		//	****	Prepare	****

		//	Delete Old Imported
		if (m_deleteOldImported)
		{
			sql = new StringBuffer ("DELETE I_FreightRegion "
				+ "WHERE I_IsImported='Y'").append(clientCheck);
			no = DB.executeUpdate(sql.toString(), get_TrxName());
			log.info("Delete Old Impored =" + no);
		}
		
//		Set Client, Org, IsActive, Created/Updated
		sql = new StringBuffer ("UPDATE I_FreightRegion "
			  + "SET AD_Client_ID = COALESCE (AD_Client_ID,").append (m_AD_Client_ID).append ("),"
			  + " AD_Org_ID = COALESCE (AD_Org_ID,").append (m_AD_Org_ID).append ("),"
			  + " IsActive = COALESCE (IsActive, 'Y'),"
			  + " Created = COALESCE (Created, SysDate),"
			  + " CreatedBy = COALESCE (CreatedBy, 0),"
			  + " Updated = COALESCE (Updated, SysDate),"
			  + " UpdatedBy = COALESCE (UpdatedBy, 0),"
			  + " I_ErrorMsg = ' ',"
			  + " I_IsImported = 'N' "
			  + "WHERE I_IsImported<>'Y' OR I_IsImported IS NULL");
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.info ("Reset=" + no);
		
		sql = new StringBuffer ( "UPDATE I_FreightRegion " +
				"SET M_Shipper_ID = (SELECT s.M_Shipper_ID FROM M_Shipper s " +
				"WHERE s.Name = ShipperName AND s.AD_Client_ID=I_FreightRegion.AD_Client_ID) " +
				"WHERE M_Shipper_ID IS NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdateEx (sql.toString (), get_TrxName());
		log.fine("Set M_Shipper_ID =" + no);
		
		sql = new StringBuffer ("UPDATE I_FreightRegion "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Shipper, ' "
				+ "WHERE M_Shipper_ID IS NULL "
				+ " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdateEx (sql.toString (), get_TrxName());
		if (no != 0)
			log.warning ("Lines with no Shipper: " + no);
		
		sql = new StringBuffer ( "UPDATE I_FreightRegion " +
				"SET M_FreightRegion_ID = (SELECT r.M_FreightRegion_ID FROM M_FreightRegion r " +
				"WHERE r.Name = I_FreightRegion.Name AND r.AD_Client_ID=I_FreightRegion.AD_Client_ID " +
				"AND r.M_Shipper_ID=I_FreightRegion.M_Shipper_ID) " +
				"WHERE M_Shipper_ID IS NOT NULL AND M_FreightRegion_ID IS NULL AND Name IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdateEx (sql.toString (), get_TrxName());
		log.fine("Set M_Shipper_ID =" + no);
		
		sql = new StringBuffer ("UPDATE I_FreightRegion "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Freight Region/Name, ' "
				+ "WHERE M_FreightRegion_ID IS NULL AND Name IS NULL "
				+ " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdateEx (sql.toString (), get_TrxName());
		if (no != 0)
			log.warning ("Lines with no Freight Region: " + no);
		
		sql = new StringBuffer ( "UPDATE I_FreightRegion " +
				"SET C_Country_ID = (SELECT c.C_Country_ID FROM C_Country c " +
				"WHERE c.Name = I_FreightRegion.CountryName AND (c.AD_Client_ID=I_FreightRegion.AD_Client_ID OR c.AD_Client_ID=0)) " +
				"WHERE C_Country_ID IS NULL AND CountryName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdateEx (sql.toString (), get_TrxName());
		log.fine("Set C_Country_ID =" + no);
		
		sql = new StringBuffer ("UPDATE I_FreightRegion "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Country, ' "
				+ "WHERE C_Country_ID IS NULL "
				+ " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdateEx (sql.toString (), get_TrxName());
		if (no != 0)
			log.warning ("Lines with no Country: " + no);

		sql = new StringBuffer ( "UPDATE I_FreightRegion " +
				"SET C_Region_ID = (SELECT r.C_Region_ID FROM C_Region r " +
				"WHERE r.Name = I_FreightRegion.RegionName AND (r.AD_Client_ID=I_FreightRegion.AD_Client_ID OR r.AD_Client_ID=0) " +
				"AND r.C_Country_ID=I_FreightRegion.C_Country_ID) " +
				"WHERE C_Country_ID IS NOT NULL AND C_Region_ID IS NULL AND RegionName IS NOT NULL AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdateEx (sql.toString (), get_TrxName());
		log.fine("Set C_Region_ID =" + no);
		
		sql = new StringBuffer ("UPDATE I_FreightRegion "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No matching Region, ' "
				+ "WHERE RegionName IS NOT NULL AND C_Region_ID IS NULL "
				+ " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdateEx (sql.toString (), get_TrxName());
		if (no != 0)
			log.warning ("Lines with no Region match: " + no);
		
		sql = new StringBuffer ("UPDATE I_FreightRegion "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Region OR Postcode only (not both), ' "
				+ "WHERE C_Region_ID IS NOT NULL AND Postal IS NOT NULL "
				+ " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdateEx (sql.toString (), get_TrxName());
		if (no != 0)
			log.warning ("Lines with both Region and Postal: " + no);
		
		sql = new StringBuffer ("UPDATE I_FreightRegion "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=Require one of Region OR Postcode, ' "
				+ "WHERE C_Region_ID IS NULL AND Postal IS NULL "
				+ " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdateEx (sql.toString (), get_TrxName());
		if (no != 0)
			log.warning ("Lines without Region and Postal: " + no);
		
		
		//	-------------------------------------------------------------------
		int noRegionInsert = 0;
		int noLocationInsert = 0;

		//	Go through Records
		log.fine("start inserting/updating ...");
		String where = "I_IsImported = 'N' " + clientCheck;
		List<X_I_FreightRegion> list = new Query(getCtx(), X_I_FreightRegion.Table_Name, where, get_TrxName())
		.setOnlyActiveRecords(true)
		.setOrderBy("M_Shipper_ID, M_FreightRegion_ID, Name, C_Country_ID, C_Region_ID, Postal")
		.list();
		
		MShipper shipper = null;
		MFreightRegion region = null;
		
		for ( X_I_FreightRegion imp : list )
		{
			if ( shipper == null || imp.getM_Shipper_ID() != shipper.getM_Shipper_ID() )
				shipper = (MShipper) imp.getM_Shipper();
			
			if ( region == null || (imp.getM_FreightRegion_ID() > 0 && region.getM_FreightRegion_ID() != imp.getM_FreightRegion_ID() )
					|| (imp.getName() != null && !imp.getName().equals(region.getName())) )
				region = (MFreightRegion) imp.getM_FreightRegion();
			
			if ( region.is_new() )
			{
				region.setAD_Org_ID(imp.getAD_Org_ID());
				region.setName(imp.getName());
				region.setDescription(imp.getDescription());
				region.setM_Shipper_ID(imp.getM_Shipper_ID());
				region.saveEx();
				noRegionInsert++;
			}
			
			MFreightRegionLocation location = new MFreightRegionLocation(imp.getCtx(), 0, get_TrxName());
			location.setAD_Org_ID(imp.getAD_Org_ID());
			location.setC_Country_ID(imp.getC_Country_ID());
			if ( imp.getPostal() != null )
				location.setPostal(imp.getPostal());
			else
				location.setC_Region_ID(imp.getC_Region_ID());
			location.setM_FreightRegion_ID(region.getM_FreightRegion_ID());
			location.saveEx();
			noLocationInsert++;
			
			imp.setI_IsImported(true);
			imp.setProcessed(true);
			imp.saveEx();
			
		}
		return "Inserted " + noRegionInsert + " freight regions and " + noLocationInsert + " locations.";
	}	//	doIt

}	//	ImportProduct
