/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Copyright (C) 2003-2007 e-Evolution,SC. All Rights Reserved.               *
 * Contributor(s): Victor Perez www.e-evolution.com                           *
 *****************************************************************************/

package org.compiere.process;

import java.util.logging.Level;

import org.compiere.model.MPaySelection;
import org.compiere.model.MUser;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * Delete Payselection
 * Don't actually delete, just mark the amount as 0
 * 
 * @author jobriant
 */
public class DeletePaySelection extends SvrProcess {

	private static final int COSTELEMENT_MATERIAL = 100;

	/** The Record */
	private int p_Record_ID = 0;

	private int m_level = 0;

	/**
	 * Prepare - e.g., get Parameters.
	 */
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		p_Record_ID = getRecord_ID();
	} //prepare

	/**
	 * Process
	 * 
	 * @return message
	 * @throws Exception
	 */

	protected String doIt() throws Exception 
	{
		MPaySelection payselection = new MPaySelection(getCtx(), p_Record_ID, get_TrxName());
		
		if (payselection.get_ID() == 0) {
			//not possible
			return "No record found.";
		}
		if (hasPayments()) {
			return "Document is Complete - Not Voidable";
		}
		
		if (deletePaySelectionLines())
		{
			payselection.setTotalAmt(Env.ZERO);
			if (payselection.getDescription() != null) {
				String desc = payselection.getDescription() + " Voided By " + MUser.get(getCtx()).getName();
				payselection.setDescription(desc);
			}
			else {
				
				payselection.setDescription("Voided By " + MUser.get(getCtx()).getName());
			}
			payselection.save();
			
		}

		commitEx();
		
		return "@OK@";

	} 

	
	private boolean deletePaySelectionLines()
	{
		String sql = "UPDATE C_PaySelectionLine " +
	             "  SET payamt = 0 " +
	             "WHERE c_payselection_id = ?";
		try {
			DB.executeUpdate(sql, p_Record_ID, get_TrxName());
			
			sql = "UPDATE C_PaySelectionCheck " +
		          "   SET payamt = 0 " +
		          "WHERE c_payselection_id = ?";
			DB.executeUpdate(sql, p_Record_ID, get_TrxName());
		} catch (Exception e) {
			log.severe(e.getMessage());
			return false;
		}
		return true;
	}

	private boolean hasPayments()
	{
		String sql = "SELECT COUNT(*) " +
	             "FROM c_payselectioncheck " +
	             "WHERE c_payselection_id = ? " +
	             "AND c_payment_id IS NOT NULL AND processed = 'Y'"; 
		
		int rows = DB.getSQLValue(get_TrxName(), sql, p_Record_ID);
		
		if (rows > 0) {
			return true;
		}
		
		return false;
	}
} 
