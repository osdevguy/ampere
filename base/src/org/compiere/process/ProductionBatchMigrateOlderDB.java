/**
 * 
 */
package org.compiere.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MProduction;
import org.compiere.model.MProductionBatch;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

/**
 * Production batch migration with older DB
 * 
 * @author Sachin
 */
public class ProductionBatchMigrateOlderDB extends SvrProcess
{
	int	newBatchCnt		= 0;
	int	modifyBatchCnt	= 0;

	/*
	 * (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	protected void prepare()
	{
	}

	/*
	 * (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception
	{
		// Correction data for production batch not present
		StringBuilder updateSql = new StringBuilder();
		updateSql.append("UPDATE M_Production pd").append(" SET M_Production_Batch_ID = NULL ")
				.append(" FROM M_Production p ")
				.append(" LEFT JOIN M_Production_Batch pb ON (pb.M_Production_Batch_ID = p.M_Production_Batch_ID) ")
				.append(" WHERE pb.M_Production_Batch_ID IS NULL AND p.M_Production_Batch_ID IS NOT NULL AND pd.M_Production_ID = p.M_Production_ID");

		int no = DB.executeUpdate(updateSql.toString(), get_TrxName());
		log.info("Correction data = " + no);

		Trx trx = Trx.get(get_TrxName(), false);
		trx.commit(true);

		// Create or upgrade Production Batch
		String sql = "SELECT * FROM M_Production	Order BY M_Production_Batch_ID";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, get_TrxName());
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				createOrModifyProductionBatch(rs);
			}
		}
		catch (Exception e)
		{
			throw new AdempiereException("" + e);
		}

		addLog("No of New Production Batch created:" + newBatchCnt + " and No of Upgraded Production Batch:"
				+ modifyBatchCnt);

		return "";
	}

	private void createOrModifyProductionBatch(ResultSet rs)
	{
		MProduction production = new MProduction(getCtx(), rs, get_TrxName());
		boolean isNewBatch = true;
		MProductionBatch batch = null;

		if (production.getM_Production_Batch_ID() > 0)
		{
			batch = new MProductionBatch(getCtx(), production.getM_Production_Batch_ID(), get_TrxName());
			isNewBatch = false;
			if (batch == null || batch.getM_Production_Batch_ID() <= 0)
				isNewBatch = true;
			else
				modifyBatchCnt++;
		}

		if (isNewBatch)
		{
			batch = new MProductionBatch(getCtx(), 0, get_TrxName());
			batch.setM_Product_ID(production.getM_Product_ID());
			batch.setM_Locator_ID(production.getM_Locator_ID());
			batch.setAD_Client_ID(production.getAD_Client_ID());
			batch.setAD_Org_ID(production.getAD_Org_ID());
			batch.setDocumentNo(production.getDocumentNo());
			newBatchCnt++;
		}
		batch.setTargetQty(production.getProductionQty());
		batch.setMovementDate(production.getMovementDate());
		batch.setCountOrder(1);
		batch.setQtyCompleted(Env.ZERO);
		batch.setC_DocType_ID(production.getC_DocType_ID());
		batch.setDocStatus(production.getDocStatus());
		batch.setDocAction(production.getDocAction());
		batch.setQtyReserved(Env.ZERO);
		batch.setQtyOrdered(production.getProductionQty());

		// if (production.getDocStatus().equals(MProduction.DOCSTATUS_Drafted))
		// {
		// ;
		// }
		// else if
		// (production.getDocStatus().equals(MProduction.DOCSTATUS_InProgress))
		// {
		// ;
		// }
		// else
		if (production.getDocStatus().equals(MProduction.DOCSTATUS_Completed)
				|| production.getDocStatus().equals(MProduction.DOCSTATUS_Closed))
		{
			batch.setQtyCompleted(production.getProductionQty());
			batch.setQtyOrdered(production.getProductionQty());
			batch.setProcessed(true);
		}

		batch.saveEx();

		DB.executeUpdateEx("UPDATE M_Production SET M_Production_Batch_ID = " + batch.getM_Production_Batch_ID()
				+ " WHERE M_Production_ID = " + production.getM_Production_ID(), get_TrxName());

	}
}
