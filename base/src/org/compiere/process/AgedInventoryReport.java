package org.compiere.process;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPeriod;
import org.compiere.model.MTable;
import org.compiere.print.MPrintFormat;
import org.compiere.print.MPrintFormatItem;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Ini;

/**
 * 
 * Aged Inventory Report
 * 
 * @author Nikunj Panelia
 *
 */
public class AgedInventoryReport extends SvrProcess 
{
	
	/** Product */
	private int		      m_Product_ID = 0;	
	/** As At Date */
	private Timestamp     asAtDate = null; 
	/** Product Category */
	private int		      m_Product_Category_ID = 0;	
	/** Accounting Schema */
	private int 		  c_AcctSchema_ID=0;
	
	ArrayList<MPeriod>    m_periods = new ArrayList<MPeriod>();
	ArrayList<String>     periodNameList=new ArrayList<String> ();
	private int 		  m_AD_Client_ID = 0;
	
	private static Logger log = CLogger.getCLogger(AgedInventoryReport.class);

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare() {

		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_ID"))
				m_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("MovementDate"))
				asAtDate = para[i].getParameterAsTimestamp();
			else if (name.equals("M_Product_Category_ID"))
				m_Product_Category_ID = para[i].getParameterAsInt();
			else if (name.equals("C_AcctSchema_ID"))
				c_AcctSchema_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
			m_AD_Client_ID = Env.getAD_Client_ID(getCtx());
		}
	}

	/**
	 *  Perform process.
	 *  @return Message 
	 *  @throws Exception if not successful
	 */
	protected String doIt() throws Exception 
	{
		
		getPeriods();
		getPeriodName();
		fillTable();
		
		if (Ini.isClient())
			getProcessInfo().setTransientObject(getPrintFormat());
		else
			getProcessInfo().setSerializableObject(getPrintFormat());
		
		return "";
	}
	
	
	private void fillTable()
	{
		boolean haveProduct=false;
		boolean haveCatagory=false;
		
		StringBuffer sql=new StringBuffer("INSERT INTO T_Aged_Inventory " +
				"(m_product_id,ad_pinstance_id,Ad_Client_id,ad_org_id,Total ) " +
				"SELECT mt.m_product_id,?,mt.ad_client_id, mt.ad_org_id,sum(mt.movementqty)  " +
				"FROM M_Transaction mt INNER JOIN m_product mp ON mp.m_product_id= mt.m_product_id " )
				.append("WHERE mp.ad_client_id = ? AND (mt.movementdate :: date) <= ?  ");
		
		if(m_Product_ID!=0)
		{
			sql.append(" AND mp.M_Product_ID=? ");
			haveProduct=true;
		}
		else if(m_Product_Category_ID!=0)
		{
			sql.append(" AND mp.M_Product_Category_ID=?");
			haveCatagory=true;
		}
			
		sql.append(" GROUP BY mt.ad_client_id, mt.ad_org_id, mt.m_product_id");
		
		
		PreparedStatement pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try
		{
			
			pstmt.setInt(1, getAD_PInstance_ID());
			pstmt.setInt(2, m_AD_Client_ID);
			pstmt.setDate(3, new Date(asAtDate.getTime()));
		
			if (haveProduct == true) 
			{
				pstmt.setInt(4, m_Product_ID);
			} 
			else if (haveCatagory == true) 
			{
				pstmt.setInt(4, m_Product_Category_ID);			
			}

			pstmt.executeUpdate();
		} 
		catch (SQLException e) {
			log.log(Level.SEVERE, "Can not create aged inventory record.");
			throw new AdempiereException(
					"Can not create aged inventory record", e);
		} finally {
			DB.close(pstmt);
		}
		
		
		sql = new StringBuffer("UPDATE T_Aged_Inventory t")
		.append(" SET ")
		.append( " Cost = ( SELECT mc.currentcostprice FROM m_cost  mc inner join m_product mp  " +
				" on  mp.m_product_id=mc.m_product_id inner join c_acctschema  asm on asm.c_acctschema_id=mc.c_acctschema_id " +
				" inner join m_costelement ce on ce.m_costelement_id=mc.m_costelement_id AND ce.costingmethod=asm.costingmethod " +
				" Where mp.m_product_id=t.m_product_id and mc.c_acctschema_id=? ) ")
		.append(" WHERE ad_pinstance_id = ?   ");;
		
		pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try 
		{
			pstmt.setInt(1, c_AcctSchema_ID);
			pstmt.setInt(2, getAD_PInstance_ID());
			pstmt.executeUpdate();
		}
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "cost update failed");
			throw new AdempiereException(
					"Cost update failed", e);
		}
		finally 
		{
			DB.close(pstmt);
		}
		


		
		sql = new StringBuffer("UPDATE T_Aged_Inventory t")
				.append(" SET ")
				.append(" TotalValue= ( Total * Cost ) , ")
				.append(" Month_0 =  COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (date_trunc('month', ? :: date)) and (? :: date)) , 0 ) , ")
				.append(" Month_1 =  COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 ),  ")
				.append(" Month_2 =  COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 ),  ")
				.append(" Month_3 =  COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 ),  ")
				.append(" Month_4 =  COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 ),  ")
				.append(" Month_5 =  COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 ),  ")
				.append(" Month_6 =  COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 ),  ")
				.append(" Month_7 =  COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 ),  ")
				.append(" Month_8 =  COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 ),  ")
				.append(" Month_9 =  COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 ),  ")
				.append(" Month_10 = COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 ),  ")
				.append(" Month_11 = COALESCE( (SELECT sum(mt.MovementQty) FROM M_Transaction mt WHERE    t.M_Product_ID= mt.M_Product_ID "
						+ " and mt.movementtype  IN ('V+','V-') and movementdate between  (? :: date)  and (? :: date)) ,0 )   ")

				.append(" WHERE ad_pinstance_id = ?   ");
			pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try 
		{
			pstmt.setDate(1, new Date(asAtDate.getTime()) );
			pstmt.setDate(2, new Date(asAtDate.getTime()) );
			pstmt.setDate(3, new Date(m_periods.get(1).getStartDate().getTime()) );
			pstmt.setDate(4, new Date(m_periods.get(1).getEndDate().getTime()) );
			pstmt.setDate(5, new Date(m_periods.get(2).getStartDate().getTime()) );
			pstmt.setDate(6, new Date(m_periods.get(2).getEndDate().getTime()) );
			pstmt.setDate(7, new Date(m_periods.get(3).getStartDate().getTime()) );
			pstmt.setDate(8, new Date(m_periods.get(3).getEndDate().getTime()) );
			pstmt.setDate(9, new Date(m_periods.get(4).getStartDate().getTime()) );
			pstmt.setDate(10, new Date(m_periods.get(4).getEndDate().getTime()) );
			pstmt.setDate(11, new Date(m_periods.get(5).getStartDate().getTime()) );
			pstmt.setDate(12, new Date(m_periods.get(5).getEndDate().getTime()) );
			pstmt.setDate(13, new Date(m_periods.get(6).getStartDate().getTime()) );
			pstmt.setDate(14, new Date(m_periods.get(6).getEndDate().getTime()) );
			pstmt.setDate(15, new Date(m_periods.get(7).getStartDate().getTime()) );
			pstmt.setDate(16, new Date(m_periods.get(7).getEndDate().getTime()) );
			pstmt.setDate(17, new Date(m_periods.get(8).getStartDate().getTime()) );
			pstmt.setDate(18, new Date(m_periods.get(8).getEndDate().getTime()) );
			pstmt.setDate(19, new Date(m_periods.get(9).getStartDate().getTime()) );
			pstmt.setDate(20, new Date(m_periods.get(9).getEndDate().getTime()) );
			pstmt.setDate(21, new Date(m_periods.get(10).getStartDate().getTime()) );
			pstmt.setDate(22, new Date(m_periods.get(10).getEndDate().getTime()) );
			pstmt.setDate(23, new Date(m_periods.get(11).getStartDate().getTime()) );
			pstmt.setDate(24, new Date(m_periods.get(11).getEndDate().getTime()) );
			pstmt.setInt(25, getAD_PInstance_ID());
		
			pstmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "First update failed");
			throw new AdempiereException(
					"Failed when updating purchase details.", e);
		}
		finally 
		{
			DB.close(pstmt);
		}
		
		
		sql = new StringBuffer("UPDATE T_Aged_Inventory t")
				.append(" SET ")
				.append(" Usage_3_months = COALESCE ((   SELECT sum(mt.movementqty) FROM m_transaction mt  WHERE     t.M_Product_ID= mt.M_Product_ID   "
						+ " AND MovementType IN ('C-','C+') AND  mt.movementdate between  ( ? :: date) and ( ?  :: date)) , 0 ) ,")
				.append(" Usage_6_months = COALESCE ((   SELECT sum(mt.movementqty) FROM m_transaction mt  WHERE     t.M_Product_ID= mt.M_Product_ID   "
						+ " AND MovementType IN ('C-','C+') AND  mt.movementdate between  ( ? :: date) and ( ?  :: date)) , 0 ) ,")
				.append(" Usage_12_months = COALESCE((   SELECT sum(mt.movementqty) FROM m_transaction mt  WHERE     t.M_Product_ID= mt.M_Product_ID   "
						+ " AND MovementType IN ('C-','C+') AND  mt.movementdate between  ( ? :: date) and ( ?  :: date)) , 0 ) ,")
				.append(" Usage_18_months = COALESCE((   SELECT sum(mt.movementqty) FROM m_transaction mt  WHERE     t.M_Product_ID= mt.M_Product_ID   "
						+ " AND MovementType IN ('C-','C+') AND  mt.movementdate between  ( ? :: date) and ( ?  :: date)) , 0 ) ,")
				.append(" Usage_24_months = COALESCE((   SELECT sum(mt.movementqty) FROM m_transaction mt  WHERE     t.M_Product_ID= mt.M_Product_ID   "
						+ " AND MovementType IN ('C-','C+') AND  mt.movementdate between  ( ? :: date) and ( ?  :: date)) ,0 ) ")
			
				.append(" WHERE ad_pinstance_id = ?   ");
		
		

			pstmt = DB.prepareStatement(sql.toString(),
				get_TrxName());
		try 
		{
			pstmt.setDate(1, new Date(m_periods.get(2).getStartDate().getTime()) );
			pstmt.setDate(2, new Date(asAtDate.getTime()) );
			pstmt.setDate(3, new Date(m_periods.get(5).getStartDate().getTime()) );
			pstmt.setDate(4, new Date(asAtDate.getTime()) );
			pstmt.setDate(5, new Date(m_periods.get(11).getStartDate().getTime()) );
			pstmt.setDate(6, new Date(asAtDate.getTime()) );
			pstmt.setDate(7, new Date(m_periods.get(17).getStartDate().getTime()) );
			pstmt.setDate(8, new Date(asAtDate.getTime()) );
			pstmt.setDate(9, new Date(m_periods.get(23).getStartDate().getTime()) );
			pstmt.setDate(10, new Date(asAtDate.getTime()) );
			pstmt.setInt(11, getAD_PInstance_ID());
		
			pstmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "Second update failed");
			throw new AdempiereException(
					"Failed when updating usage details.", e);
		}
		finally 
		{
			DB.close(pstmt);
		}
		
		
		String prosql="select updateagedinventory(" + getAD_PInstance_ID() + ")";
	
		PreparedStatement pstmt2 = DB.prepareStatement(prosql.toString(),get_TrxName());
		try 
		{
			pstmt2.execute();
		} 
		catch (SQLException e) 
		{
			log.log(Level.SEVERE, "Third update failed");
		}
		
		
		
	}
	
	
	/**
	 * @return list of 24 periods before  As At Date
	 */
	private ArrayList<MPeriod> getPeriods()
	{
		MPeriod m_period=MPeriod.get(getCtx(), asAtDate,Env.getAD_Org_ID(getCtx()));
		m_periods.add(m_period);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(asAtDate.getTime());
		
		for(int i=0;i<23;i++)
		{
			cal.add(Calendar.MONTH, -1);
			Timestamp depDate = new Timestamp(cal.getTimeInMillis());
			MPeriod acctPeriod = MPeriod.get(getCtx(), depDate,
					Env.getAD_Org_ID(getCtx()));
			cal.setTimeInMillis(depDate.getTime());
			m_periods.add(acctPeriod);
			
		}
		
		return m_periods;
	}
	
	
	/**
	 * @return list of names of last 12 periods.
	 */
	private ArrayList<String> getPeriodName()
	{		
		for(int i=0;i<12;i++)
		{
			periodNameList.add(m_periods.get(i).getName());
		}
		return periodNameList;
	}
	
	
	/**************************************************************************
	 *	Get PrintFormat
	 * 	@return print format
	 */
	private MPrintFormat getPrintFormat()
	{
		MPrintFormat pf=MPrintFormat.get(getCtx(), 0, MTable.getTable_ID("T_Aged_Inventory"));

		int count = pf.getItemCount();
		for (int i = 0; i < count; i++)
		{
			MPrintFormatItem pfi = pf.getItem(i);
			String ColumnName = pfi.getColumnName();
			//
			if (ColumnName == null)
			{
				log.log(Level.SEVERE, "No ColumnName for #" + i + " - " + pfi);
				if (pfi.isPrinted())
					pfi.setIsPrinted(false);
				if (pfi.isOrderBy())
					pfi.setIsOrderBy(false);
				if (pfi.getSortNo() != 0)
					pfi.setSortNo(0);
			}
			else if (ColumnName.startsWith("Month"))
			{
				
				int index= Integer.parseInt(ColumnName.substring(6));
				
				if (pfi.isPrinted())
					pfi.setIsPrinted(true);
				if (pfi.isOrderBy())
					pfi.setIsOrderBy(false);
				/*if (pfi.getSortNo() != 130-(index*(10)+1));
					pfi.setSortNo(130-(index*(10)+1));*/
				pfi.setName(periodNameList.get(index));
				pfi.setPrintName(periodNameList.get(index));
				//pfi.setSeqNo(130-(index*(10)+1));
				
			}

			else if (ColumnName.equals("M_Product_ID"))
			{
				if (pfi.isPrinted())
					pfi.setIsPrinted(true);
				if (!pfi.isOrderBy())
					pfi.setIsOrderBy(true);
				if (pfi.getSortNo() != 10)
					pfi.setSortNo(10);
				pfi.setSeqNo(10);
				
			}
			else if (ColumnName.equals("Total"))
			{
				if (pfi.isPrinted())
					pfi.setIsPrinted(true);
				if (!pfi.isOrderBy())
					pfi.setIsOrderBy(false);
				if (pfi.getSortNo() != 140)
					pfi.setSortNo(140);
				pfi.setSeqNo(140);
			}
			else if (ColumnName.equals("Cost"))
			{
				if (pfi.isPrinted())
					pfi.setIsPrinted(true);
				if (!pfi.isOrderBy())
					pfi.setIsOrderBy(false);
				if (pfi.getSortNo() != 150)
					pfi.setSortNo(150);
				pfi.setSeqNo(150);
			}
			else if (ColumnName.equals("TotalValue"))
			{
				if (pfi.isPrinted())
					pfi.setIsPrinted(true);
				if (!pfi.isOrderBy())
					pfi.setIsOrderBy(false);
				if (pfi.getSortNo() != 160)
					pfi.setSortNo(160);
				pfi.setSeqNo(160);
			}
			/*else if(ColumnName.startsWith("Usage"))
			{
				int index;
				if(ColumnName.length()==14)
					index= Integer.parseInt(Character.toString(ColumnName.charAt(6)));
				else
					index= Integer.parseInt(ColumnName.substring(6,8));
				
				pfi.setSeqNo(170+index);
				
			}*/
			
			
			pfi.saveEx();
			log.fine(pfi.toString());
		}
		//	set translated to original
		pf.setTranslation();
		pf = MPrintFormat.get (getCtx(), pf.getAD_PrintFormat_ID(), true);	//	no cache
		return pf;
	}	//	getPrintFormat


}//  AgedInventoryReport
