/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import org.compiere.model.MMigration;

public class MigrationApply extends SvrProcess {

	private MMigration migration;
	private boolean failOnError = false;
	private boolean apply = true;

	@Override
	protected String doIt() throws Exception {

		if ( migration == null || migration.is_new() )
		{
			addLog("No migration");
			return "@Error@";
		}
		
		migration.setFailOnError(failOnError);
		
		if ( apply )
		{
			migration.apply();
			
			if (migration.getStatusCode() == null)
				migration.setStatusCode(MMigration.STATUSCODE_Failed);
			
			if ( MMigration.STATUSCODE_Applied.equals(migration.getStatusCode()) )
			{
				addLog("Migration successful");
				int n = migration.addMenuItems();
				if (n > 0) {
					addLog("#" + n + " Menu Items have been created at default location. \n Pleace check Menu Tree.");
				}
				n = migration.addDisplayedGrid();
				if (n > 0) {
					addLog("#" + n + " Tabs with Fields set to Display on Grid ");
				}
				
			}
			else if ( MMigration.STATUSCODE_PartiallyApplied.equals(migration.getStatusCode()) )
			{
				addLog("Migration partially applied. Please review migration steps for errors.");
			}
			else if (  MMigration.STATUSCODE_Failed.equals(migration.getStatusCode()) )
			{
				addLog("Migration failed. Please review migration steps for errors.");
			}
			
		}
		else 
		{
			migration.rollback();

			if ( MMigration.STATUSCODE_Unapplied.equals(migration.getStatusCode()) )
			{
				addLog("Rollback successful.");
			}
			else
			{
				addLog("Rollback failed. Please review migration steps for errors.");
			}
			
		}

		return "@OK@";
	}

	@Override
	protected void prepare() {
		
		migration = new MMigration(getCtx(), getRecord_ID(), get_TrxName());
		
		ProcessInfoParameter[] params = getParameter();
		for ( ProcessInfoParameter p : params)
		{
			String para = p.getParameterName();
			if ( para.equals("FailOnError") )
				failOnError  = "Y".equals((String)p.getParameter());
			else if (para.equals("Apply"))
				apply = MMigration.APPLY_Apply.equals(p.getParameterAsString());
		}

	}

}
