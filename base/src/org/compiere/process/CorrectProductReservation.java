package org.compiere.process;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProductionBatch;
import org.compiere.model.MTable;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.eevolution.model.MDDOrder;
import org.eevolution.model.MDDOrderLine;

public class CorrectProductReservation extends SvrProcess {

	private int productID = 0;
	private int productCategoryID = 0;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_ID"))
				productID = para[i].getParameterAsInt();
			else if (name.equals("M_Product_Category_ID"))
				productCategoryID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		
		String productWhere = " COALESCE(M_Product_ID,0) <> 0 ";
		
		if ( productID > 0 )
			productWhere = " M_Product_ID = " + productID;
		else if ( productCategoryID > 0)
			productWhere = " M_Product_ID IN (SELECT p.M_Product_ID "
					+ "FROM M_Product p WHERE p.M_Product_Category_ID = " + productCategoryID + ") ";
		
		String productionWhere = " EXISTS (SELECT 1 FROM M_PBatch_Line WHERE M_Production_Batch.M_Production_Batch_ID=M_PBatch_Line.M_Production_Batch_ID"
				+ "                   AND M_PBatch_Line.QtyReserved <> 0 "
				+ "                   AND " + productWhere + ")";
		
		Query productionQuery = MTable.get(getCtx(), MProductionBatch.Table_ID).createQuery(productionWhere, get_TrxName());
		productionQuery.setOrderBy("Created");
		productionQuery.setClient_ID();
		List<MProductionBatch> productionList = productionQuery.list();
		for (MProductionBatch pb : productionList)
		{
			pb.setQtyReserved(Env.ZERO);
			pb.saveEx();
		}
		
		String storageSql = "UPDATE M_Storage l "
				+ " SET QtyReserved=0, QtyOrdered=0 "
				+ " WHERE l.AD_Client_ID = " + getAD_Client_ID()
				+ " AND " + productWhere;
		
		int storagesUpdated = DB.executeUpdate(storageSql, get_TrxName());
		log.log(Level.FINE, "Storages reserved/ordered set to zero: " + storagesUpdated);
		for (MProductionBatch pb : productionList)
		{
			if (pb.DOCACTION_Complete.equals(pb.getDocStatus()) && pb.getQtyCompleted().compareTo(pb.getTargetQty()) < 0)
			{
				BigDecimal qty = pb.getTargetQty().subtract(pb.getQtyCompleted());
				pb.reserveStock(pb.getM_Product(), qty);
				pb.orderedStock(pb.getM_Product(), qty);
				pb.setQtyReserved(qty);
				pb.saveEx();
			}
		}
		
		String orderWhere = " EXISTS (SELECT 1 FROM C_OrderLine WHERE C_Order.C_Order_ID=C_OrderLine.C_Order_ID"
				+ "                   AND QtyReserved <> 0 "
				+ "                   AND " + productWhere + ")";
		
		Query query = MTable.get(getCtx(), MOrder.Table_ID).createQuery(orderWhere, get_TrxName());
		query.setOrderBy("Created");
		query.setClient_ID();
		List<MOrder> orderList = query.list();
		
		for (MOrder order : orderList)
		{
			MOrderLine[] lines = order.getLines(" AND QtyReserved <> 0 AND " + productWhere, "M_Product_ID, Line");
			for (MOrderLine line : lines)
			{
				line.setQtyReserved(Env.ZERO);
			}
			order.reserveStock(null, lines);
		}
		
		orderWhere = " EXISTS (SELECT 1 FROM DD_OrderLine WHERE DD_Order.DD_Order_ID=DD_OrderLine.DD_Order_ID"
				+ "                   AND (QtyReserved <> 0 OR QtyOrdered <> 0) "
				+ "                   AND " + productWhere + ")";
		
		query = MTable.get(getCtx(), MDDOrder.Table_ID).createQuery(orderWhere, get_TrxName());
		query.setOrderBy("Created");
		query.setClient_ID();
		List<MDDOrder> ddorderList = query.list();
		
		for (MDDOrder order : ddorderList)
		{
			MDDOrderLine[] lines = order.getLines(" AND (QtyReserved <> 0 OR QtyOrdered <> 0) "
					+ "                   AND " + productWhere, "M_Product_ID, Line");
			for (MDDOrderLine line : lines)
			{
				line.setQtyReserved(Env.ZERO);
			}
			order.reserveStock(lines);
		}

		
		return "OK";
	}
}
