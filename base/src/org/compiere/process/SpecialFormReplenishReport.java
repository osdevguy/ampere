/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 * Contributor(s): Chris Farley - northernbrewer                              *
 *****************************************************************************/
package org.compiere.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.compiere.model.MBPartner;
import org.compiere.model.MDocType;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MReplenishPO;
import org.compiere.model.MWarehouse;
import org.compiere.model.X_T_Replenish_PO;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 *	Replenishment Report
 *	
 *  @author Jorg Janke
 *  @version $Id: ReplenishReport.java,v 1.2 2006/07/30 00:51:01 jjanke Exp $
 *  
 *  Carlos Ruiz globalqss - integrate bug fixing from Chris Farley
 *    [ 1619517 ] Replenish report fails when no records in m_storage
 */
public class SpecialFormReplenishReport extends SvrProcess
{
	/** Warehouse				*/
	private int		p_M_Warehouse_ID = 0;
	/**	Optional BPartner		*/
	private int		p_C_BPartner_ID = 0;
	/** Return Info				*/
	private static String	m_info = "";
	
	private static Logger s_log = CLogger.getCLogger(SpecialFormReplenishReport.class);
	private static int C_DocType_ID;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Warehouse_ID"))
				p_M_Warehouse_ID = para[i].getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}	//	prepare

	/**
	 *  Perform process.
	 *  @return Message 
	 *  @throws Exception if not successful
	 */
	protected String doIt() throws Exception
	{
		log.info("M_Warehouse_ID=" + p_M_Warehouse_ID 
			+ ", C_BPartner_ID=" + p_C_BPartner_ID 
				);
		
		MWarehouse wh = MWarehouse.get(getCtx(), p_M_Warehouse_ID);
		if (wh.get_ID() == 0)  
			throw new AdempiereSystemError("@FillMandatory@ @M_Warehouse_ID@");
		//
		prepareTable(getAD_PInstance_ID());
		fillTable(getAD_PInstance_ID(), p_M_Warehouse_ID, false, 0);
		//
		//
/*		if (p_ReplenishmentCreate.equals("POO"))
			createPO();
		else if (p_ReplenishmentCreate.equals("POR"))
			createRequisition();
		else if (p_ReplenishmentCreate.equals("MMM"))
			createMovements();
		else if (p_ReplenishmentCreate.equals("DOO"))
			createDO();*/
		return m_info;
	}	//	doIt

	public void createReplenishment(int M_Warehouse_ID, int AD_PInstance_ID, boolean isUrgent, int priority)
	{
		prepareTable(AD_PInstance_ID);
		try {
			fillTable(AD_PInstance_ID, M_Warehouse_ID, isUrgent, priority);
		} catch (Exception e) {
			log.log(Level.SEVERE, e.getMessage());
		}
	}
	
	/**
	 * 	Prepare/Check Replenishment Table
	 */
	private void prepareTable(int AD_PInstance_ID)
	{
		//	Level_Max must be >= Level_Max
		String sql = "UPDATE M_Replenish"
			+ " SET Level_Max = Level_Min "
			+ "WHERE Level_Max < Level_Min";
		int no = DB.executeUpdate(sql, get_TrxName());
		if (no != 0)
			log.fine("Corrected Max_Level=" + no);
		
		//	Minimum Order should be 1
		sql = "UPDATE M_Product_PO"
			+ " SET Order_Min = 1 "
			+ "WHERE Order_Min IS NULL OR Order_Min < 1";
		no = DB.executeUpdate(sql, get_TrxName());
		if (no != 0)
			log.fine("Corrected Order Min=" + no);
		
		//	Pack should be 1
		sql = "UPDATE M_Product_PO"
			+ " SET Order_Pack = 1 "
			+ "WHERE Order_Pack IS NULL OR Order_Pack < 1";
		no = DB.executeUpdate(sql, get_TrxName());
		if (no != 0)
			log.fine("Corrected Order Pack=" + no);

		//	Set Current Vendor where only one vendor
		sql = "UPDATE M_Product_PO p"
			+ " SET IsCurrentVendor='Y' "
			+ "WHERE IsCurrentVendor<>'Y'"
			+ " AND EXISTS (SELECT pp.M_Product_ID FROM M_Product_PO pp "
				+ "WHERE p.M_Product_ID=pp.M_Product_ID "
				+ "GROUP BY pp.M_Product_ID "
				+ "HAVING COUNT(*) = 1)";
		no = DB.executeUpdate(sql, get_TrxName());
		if (no != 0)
			log.fine("Corrected CurrentVendor(Y)=" + no);

		//	More then one current vendor
		sql = "UPDATE M_Product_PO p"
			+ " SET IsCurrentVendor='N' "
			+ "WHERE IsCurrentVendor = 'Y'"
			+ " AND EXISTS (SELECT pp.M_Product_ID FROM M_Product_PO pp "
				+ "WHERE p.M_Product_ID=pp.M_Product_ID AND pp.IsCurrentVendor='Y' "
				+ "GROUP BY pp.M_Product_ID "
				+ "HAVING COUNT(*) > 1)";
		no = DB.executeUpdate(sql, get_TrxName());
		if (no != 0)
			log.fine("Corrected CurrentVendor(N)=" + no);
		
		//	Just to be sure
		sql = "DELETE T_Replenish_PO WHERE AD_PInstance_ID=" + AD_PInstance_ID;
		no = DB.executeUpdate(sql, get_TrxName());
		if (no != 0)
			log.fine("Delete Existing Temp=" + no);
	}	//	prepareTable

	/**
	 * 	Fill Table
	 * 	@param wh warehouse
	 */
	private void fillTable (int AD_PInstance_ID, int M_Warehouse_ID, boolean urgentOnly, int priority) throws Exception
	{
		String sql;
		
		sql = "SELECT COUNT(AD_PInstance_ID) FROM T_Replenish_PO WHERE AD_PInstance_ID=? AND M_Warehouse_ID=?";
		int no = DB.getSQLValue(get_TrxName(), sql, AD_PInstance_ID, M_Warehouse_ID);
		if (no > 0 ) {
			return;
		}

		sql = "SELECT ad_sequence_id FROM ad_sequence WHERE name = 'T_Replenish_PO'";
		int seqID = DB.getSQLValue(get_TrxName(), sql);
		
		if (urgentOnly) {
			sql = "INSERT INTO T_Replenish_PO  " +
		             "(T_Replenish_PO_ID, AD_PInstance_ID, IsComplete, M_Warehouse_ID, M_Product_ID, AD_Client_ID, AD_Org_ID, " +
		             " ReplenishType, Level_Min, Level_Max, " +
		             " Current_C_BPartner_ID, C_BPartner_ID, Order_Min, Order_Pack, QtyToOrder, POQty " +
					 " , created, createdby, updated, updatedby, isactive) ";
			sql = sql + " SELECT  " +
		             "	nextidfunc(" + seqID + ", 'N') as ID,  pi.ad_pinstance_id,  " +
		             " 'N' as IsComplete,  r.M_Warehouse_ID, r.M_Product_ID, r.AD_Client_ID, r.AD_Org_ID, " +
		             " r.ReplenishType, r.Level_Min, r.Level_Max, " +
		             " po.C_BPartner_ID, po.C_BPartner_ID, po.Order_Min, po.Order_Pack " +
		             "  , oo.backorder " +
		             "as QtyToOrder, 0 as POQty " +
		             ", pi.created, pi.createdby, pi.updated, pi.updatedby, pi.isactive " +
		             "FROM  M_Replenish r " +
		             "INNER JOIN M_Product_PO po ON (r.M_Product_ID=po.M_Product_ID)  " +
		             "INNER JOIN AD_PInstance pi ON r.ad_client_id = pi.ad_client_id " +
		             "LEFT JOIN " +
		             "(SELECT s.M_Product_ID " +
		             " , sum(s.qtyonhand-s.qtyreserved+qtyordered) as QtyAvailable " +
		             " FROM M_Storage s " +
		             "INNER JOIN M_Locator l on (s.m_locator_id = l.m_locator_id) " +
		             "WHERE l.M_Warehouse_ID=? " +
		             " group by s.m_product_id) Q ON r.m_product_id = q.m_product_id " +
		             "INNER JOIN " +
		             " (SELECT ol.m_product_id,  sum(ol.qtyreserved) as backorder " +
		             " FROM C_OrderLine ol INNER JOIN c_order oc on ol.c_order_id = oc.c_order_id " +
		             " WHERE oc.issotrx = 'Y' AND oc.docstatus = 'CO' AND ol.qtyentered > ol.qtydelivered  AND COALESCE(to_number(oc.priorityrule, '9'), 9999) <=  " 
		             + priority +
		             " GROUP BY ol.m_product_id ) oo ON po.m_product_id = oo.m_product_id " +
		             "WHERE  " +
		             " r.M_Warehouse_ID=? AND pi.ad_pinstance_id = ?" ;

			no = DB.executeUpdate(sql, new Object[]{M_Warehouse_ID, M_Warehouse_ID, AD_PInstance_ID}, false, get_TrxName());
			if (no != 0) {
				log.finest(sql);
				log.fine("Create Replenishment for Demand Only = " + no);
			}
		}
		else {
			//Reorder Below Min Level #1
			sql = "INSERT INTO T_Replenish_PO  " +
		             "(T_Replenish_PO_ID, AD_PInstance_ID, IsComplete, M_Warehouse_ID, M_Product_ID, AD_Client_ID, AD_Org_ID, " +
		             " ReplenishType, Level_Min, Level_Max, " +
		             " Current_C_BPartner_ID, C_BPartner_ID, Order_Min, Order_Pack, QtyToOrder, POQty " +
					 " , created, createdby, updated, updatedby, isactive) ";
			
			sql = sql + " SELECT  " +
		             "	nextidfunc(" + seqID + ", 'N') as ID,  pi.ad_pinstance_id,  " +
		             " 'N' as IsComplete,  r.M_Warehouse_ID, r.M_Product_ID, r.AD_Client_ID, r.AD_Org_ID, " +
		             " r.ReplenishType, r.Level_Min, r.Level_Max, " +
		             " po.C_BPartner_ID, po.C_BPartner_ID, po.Order_Min, po.Order_Pack " +
		             "  , r.Level_Max - COALESCE(q.QtyAvailable,0) " +
		             "as QtyToOrder, 0 as POQty " +
		             ", pi.created, pi.createdby, pi.updated, pi.updatedby, pi.isactive " +
		             "FROM  M_Replenish r " +
		             "INNER JOIN M_Product_PO po ON (r.M_Product_ID=po.M_Product_ID) AND po.iscurrentvendor ='Y' " +
		             "INNER JOIN AD_PInstance pi ON r.ad_client_id = pi.ad_client_id " +
		             "LEFT JOIN " +
		             "(SELECT s.M_Product_ID " +
		             " , sum(s.qtyonhand-s.qtyreserved+qtyordered) as QtyAvailable " +
		             " FROM M_Storage s " +
		             "INNER JOIN M_Locator l on (s.m_locator_id = l.m_locator_id) " +
		             "WHERE l.M_Warehouse_ID=?" +
		             " group by s.m_product_id) Q ON r.m_product_id = q.m_product_id " +
		             "WHERE  " +
		             " r.M_Warehouse_ID=?" +
		             " AND r.ReplenishType = '1' " +
		             " AND q.QtyAvailable <= r.Level_Min " +
		             " AND r.Level_Max - COALESCE(q.QtyAvailable,0) > 0" +
		             " AND pi.ad_pinstance_id = ?";		
			
			no = DB.executeUpdate(sql, new Object[]{M_Warehouse_ID, M_Warehouse_ID, AD_PInstance_ID}, false, get_TrxName());
			log.finest(sql);
			log.fine("Create Replenishment for X_M_Replenish.REPLENISHTYPE_ReorderBelowMinimumLevel(val=1)" + no);
	
			//Maintain Maximum Level  #2
			sql = "INSERT INTO T_Replenish_PO  " +
		             "(T_Replenish_PO_ID, AD_PInstance_ID, IsComplete, M_Warehouse_ID, M_Product_ID, AD_Client_ID, AD_Org_ID, " +
		             " ReplenishType, Level_Min, Level_Max, " +
		             " Current_C_BPartner_ID, C_BPartner_ID, Order_Min, Order_Pack, QtyToOrder, POQty " +
					 " , created, createdby, updated, updatedby, isactive) ";
			
			sql = sql + " SELECT  " +
		             "	nextidfunc(" + seqID + ", 'N') as ID,  pi.ad_pinstance_id,  " +
		             " 'N' as IsComplete,  r.M_Warehouse_ID, r.M_Product_ID, r.AD_Client_ID, r.AD_Org_ID, " +
		             " r.ReplenishType, r.Level_Min, r.Level_Max, " +
		             " po.C_BPartner_ID, po.C_BPartner_ID, po.Order_Min, po.Order_Pack " +
		             "  , r.Level_Max - COALESCE(q.QtyAvailable,0) " +
		             "as QtyToOrder, 0 as POQty " +
		             ", pi.created, pi.createdby, pi.updated, pi.updatedby, pi.isactive " +
		             "FROM  M_Replenish r " +
		             "INNER JOIN M_Product_PO po ON (r.M_Product_ID=po.M_Product_ID)  AND po.iscurrentvendor ='Y'  " +
		             "INNER JOIN AD_PInstance pi ON r.ad_client_id = pi.ad_client_id " +		             
		             "LEFT JOIN " +
		             "(SELECT s.M_Product_ID " +
		             " , sum(s.qtyonhand-s.qtyreserved+qtyordered) as QtyAvailable " +
		             " FROM M_Storage s " +
		             "INNER JOIN M_Locator l on (s.m_locator_id = l.m_locator_id) " +
		             "WHERE l.M_Warehouse_ID=? " + 
		             " group by s.m_product_id) Q ON r.m_product_id = q.m_product_id " +
		             "WHERE  " +
		             " r.M_Warehouse_ID = ?" +
		             " AND r.ReplenishType = '2' " +
		             " AND r.Level_Max - COALESCE(q.QtyAvailable,0) > 0" +
		             " AND pi.ad_pinstance_id = ?";				
			
			no = DB.executeUpdate(sql, new Object[]{M_Warehouse_ID, M_Warehouse_ID, AD_PInstance_ID}, false, get_TrxName());
			log.finest(sql);
			log.fine("Create Replenishment for X_M_Replenish.REPLENISHTYPE_MaintainMaximumLevel(val=2)" + no);
			
			//Maintain Minimum Level  #3
			sql = "INSERT INTO T_Replenish_PO  " +
		             "(T_Replenish_PO_ID, AD_PInstance_ID, IsComplete, M_Warehouse_ID, M_Product_ID, AD_Client_ID, AD_Org_ID, " +
		             " ReplenishType, Level_Min, Level_Max, " +
		             " Current_C_BPartner_ID, C_BPartner_ID, Order_Min, Order_Pack, QtyToOrder, POQty " +
					 " , created, createdby, updated, updatedby, isactive) ";
			
			sql = sql + " SELECT  " +
		             "	nextidfunc(" + seqID + ", 'N') as ID,  pi.ad_pinstance_id,  " +
		             " 'N' as IsComplete,  r.M_Warehouse_ID, r.M_Product_ID, r.AD_Client_ID, r.AD_Org_ID, " +
		             " r.ReplenishType, r.Level_Min, r.Level_Max, " +
		             " po.C_BPartner_ID, po.C_BPartner_ID, po.Order_Min, po.Order_Pack " +
		             "  , r.Level_Min - COALESCE(q.QtyAvailable,0) " +
		             "as QtyToOrder, 0 as POQty " +
		             ", pi.created, pi.createdby, pi.updated, pi.updatedby, pi.isactive " +
		             "FROM  M_Replenish r " +
		             "INNER JOIN M_Product_PO po ON (r.M_Product_ID=po.M_Product_ID)  AND po.iscurrentvendor ='Y'  " +
		             "INNER JOIN AD_PInstance pi ON r.ad_client_id = pi.ad_client_id " +	
		             "LEFT JOIN " +
		             "(SELECT s.M_Product_ID " +
		             " , sum(s.qtyonhand-s.qtyreserved+qtyordered) as QtyAvailable " +
		             " FROM M_Storage s " +
		             "INNER JOIN M_Locator l on (s.m_locator_id = l.m_locator_id) " +
		             "WHERE l.M_Warehouse_ID=?" +
		             " group by s.m_product_id) Q ON r.m_product_id = q.m_product_id " +
		             "WHERE  " +
		             " r.M_Warehouse_ID=?"  +
		             " AND r.ReplenishType = '3' " +
		             " AND COALESCE(q.QtyAvailable,0) <= r.Level_Min " +
		             " AND r.Level_Min - COALESCE(q.QtyAvailable,0) > 0" +	
					 " AND pi.ad_pinstance_id = ?";				
			no = DB.executeUpdate(sql, new Object[]{M_Warehouse_ID, M_Warehouse_ID, AD_PInstance_ID}, false, get_TrxName());
			log.finest(sql);
			log.fine("Create Replenishment for Maintain Minimum Level (val=3)" + no);
		}
		
		//set storage qtyonhand and qtyreserved
		sql = "UPDATE t_replenish_po " +
             "   SET qtyonhand = st.qtyonhand, qtyreserved = st.qtyreserved " +
             "FROM (SELECT l.m_warehouse_id, s.m_product_id, SUM(s.qtyonhand) AS qtyonhand, SUM(s.qtyreserved) AS qtyreserved " +
             "      FROM m_storage s  " +
             "        INNER JOIN m_locator l ON s.m_locator_id = l.m_locator_id " +
             "      GROUP BY l.m_warehouse_id, s.m_product_id) st " +
             "WHERE t_replenish_po.m_product_id = st.m_product_id " +
             "AND   t_replenish_po.m_warehouse_id = st.m_warehouse_id " +
             "AND   t_replenish_po.ad_pinstance_id = " + AD_PInstance_ID;
		no = DB.executeUpdate(sql, get_TrxName());
		
		if (no != 0)
			log.fine("Set Qty on Hand=" + no);
		
		if (urgentOnly) {
			//	Subtract QOH
			sql = "UPDATE T_Replenish_PO"
				+ " SET QtyToOrder = QtyToOrder - QtyOnHand "
				+ "WHERE QtyToOrder > 0"
				+ " AND AD_PInstance_ID=" + AD_PInstance_ID;
			no = DB.executeUpdate(sql, get_TrxName());
			if (no != 0)
				log.fine("Subtract QOH=" + no);			
		}

		//	Even dividable by Pack
		sql = "UPDATE T_Replenish_PO"
			+ " SET QtyToOrder = QtyToOrder - MOD(QtyToOrder, Order_Pack) + Order_Pack "
			+ "WHERE MOD(QtyToOrder, Order_Pack) <> 0"
			+ " AND QtyToOrder > 0"
			+ " AND AD_PInstance_ID=" + AD_PInstance_ID;
		no = DB.executeUpdate(sql, get_TrxName());
		if (no != 0)
			log.fine("Set OrderPackQty=" + no);
		

		//	Delete rows where nothing to order
		sql = "DELETE T_Replenish_PO "
			+ "WHERE QtyToOrder < 1"
		    + " AND AD_PInstance_ID=" + AD_PInstance_ID;
		no = DB.executeUpdate(sql, get_TrxName());
		if (no != 0)
			log.fine("Delete No QtyToOrder=" + no);
		
		// By default, Set PO Qty = qty to order
		sql = "UPDATE T_Replenish_PO"
				+ " SET POQty = QtyToOrder"
				+ " WHERE AD_PInstance_ID=" + AD_PInstance_ID;
			no = DB.executeUpdate(sql, get_TrxName());
			if (no != 0)
				log.fine("Set PO Qty=" + no);
		

		
		setDefaultPrice(AD_PInstance_ID);
	}	//	fillTable

	
	public void setDefaultPrice(int AD_PInstance_ID) {
		int old_C_BPartner_ID = 0;
		int M_Pricelist_ID = 0;
		MReplenishPO[] replenishs = getReplenish(null, AD_PInstance_ID);
		for (int i = 0; i < replenishs.length; i++) {
			MReplenishPO replenish = replenishs[i];
			if (old_C_BPartner_ID !=  replenish.getC_BPartner_ID()) {
				old_C_BPartner_ID = replenish.getC_BPartner_ID();
				MBPartner bp = new MBPartner(Env.getCtx(), replenish.getC_BPartner_ID(), null);
				M_Pricelist_ID = bp.getPO_PriceList_ID();
			}
			replenish.setDefaultPOPrice(M_Pricelist_ID);
			replenish.saveEx();
		}
	}
	/**
	 * 	Create PO's
	 */
	public static String createPO(int AD_PInstance_ID)
	{
		int noOrders = 0;
		String info = "";
		//
		MOrder order = null;
		MWarehouse wh = null;
		
		MDocType[] docTypes = MDocType.getOfDocBaseType(Env.getCtx(), "POO");
		
		C_DocType_ID = 0;
		for (MDocType docType : docTypes) {
			if (!docType.isSOTrx()) {
				C_DocType_ID = docType.getC_DocType_ID();
				break;
			}
		}
		
		if (C_DocType_ID == 0) {
			return "No Purchase Order Document Type found";
		}
			
		X_T_Replenish_PO[] replenishs = getReplenish("IsComplete='Y' AND Processed='N' ", AD_PInstance_ID);
		for (int i = 0; i < replenishs.length; i++)
		{
			X_T_Replenish_PO replenish = replenishs[i];
			if (wh == null || wh.getM_Warehouse_ID() != replenish.getM_Warehouse_ID())
				wh = MWarehouse.get(Env.getCtx(), replenish.getM_Warehouse_ID());
			//
			if (order == null 
				|| order.getC_BPartner_ID() != replenish.getC_BPartner_ID()
				|| order.getM_Warehouse_ID() != replenish.getM_Warehouse_ID())
			{
				
				//complete it and create receipts automatically
				if (order != null) {
					if (DocAction.STATUS_Completed.equals(order.completeIt())) {
						order.save();
					}
				}
				order = new MOrder(Env.getCtx(), 0, null);
				order.setIsSOTrx(false);
				order.setDatePromised(replenish.getDatePromised());
				order.setDocAction(DocAction.ACTION_Complete);
				order.setC_DocTypeTarget_ID(C_DocType_ID);
				MBPartner bp = new MBPartner(Env.getCtx(), replenish.getC_BPartner_ID(), null);
				order.setBPartner(bp);
				order.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));
				order.setDescription(Msg.getMsg(Env.getCtx(), "Replenishment"));
				//	Set Org/WH
				order.setAD_Org_ID(wh.getAD_Org_ID());
				order.setM_Warehouse_ID(wh.getM_Warehouse_ID());
				if (!order.save())
					return "Error saving";
				s_log.fine(order.toString());
				noOrders++;
				info += " - " + order.getDocumentNo();
			}
			MOrderLine line = new MOrderLine (order);
			line.setM_Product_ID(replenish.getM_Product_ID());
			line.setQty(replenish.getPOQty());
			line.setPrice();
			line.setPrice(replenish.getPOPrice());
			line.setPriceCost(replenish.getPOPrice());
			line.saveEx();
			replenish.setProcessed(true);
			replenish.saveEx();
		}
		//complete it and create receipts automatically
		if (order != null) {
			if (DocAction.STATUS_Completed.equals(order.completeIt())) {
				order.setDocStatus(DocAction.STATUS_Completed);
				order.save();
			}
		}

		m_info = "#" + noOrders + info;
		s_log.info(m_info);
		return m_info;
	}	//	createPO
	
	/**
	 * 	Get Replenish Records
	 *	@return replenish
	 */
	private static MReplenishPO[] getReplenish (String where, int AD_PInstance_ID)
	{
		String sql = "SELECT * FROM T_Replenish_PO "
			+ "WHERE AD_PInstance_ID=? AND C_BPartner_ID > 0 ";
		if (where != null && where.length() > 0)
			sql += " AND " + where;
		sql	+= " ORDER BY M_Warehouse_ID, C_BPartner_ID";
		ArrayList<MReplenishPO> list = new ArrayList<MReplenishPO>();
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql, null);
			pstmt.setInt (1, AD_PInstance_ID);
			ResultSet rs = pstmt.executeQuery ();
			while (rs.next ())
				list.add (new MReplenishPO (Env.getCtx(), rs, null));
			rs.close ();
			pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			s_log.log(Level.SEVERE, sql, e);
		}
		try
		{
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		}
		catch (Exception e)
		{
			pstmt = null;
		}
		MReplenishPO[] retValue = new MReplenishPO[list.size ()];
		list.toArray (retValue);
		return retValue;
	}	//	getReplenish

	
}	//	Replenish
