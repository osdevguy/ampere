/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import org.compiere.impexp.MImpFormat;
import org.compiere.impexp.MImpFormatRow;
import org.compiere.model.MColumn;
import org.compiere.model.MField;
import org.compiere.model.MImport;
import org.compiere.model.MTab;
import org.compiere.print.MPrintFormat;
import org.compiere.print.MPrintFormatItem;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;

public class ImportFormatFromPrintFormat extends SvrProcess {


	private int p_AD_ImpFormat_ID;
	private int p_AD_PrintFormat_ID;
	
	/**
	 * Generic importer process
	 * Runs validation against staging table then copies to target table based on column mapping.
	 */
	@Override
	protected void prepare() {

		for ( ProcessInfoParameter para : getParameter())
		{
			if ( para.getParameterName().equals("AD_ImpFormat_ID") )
				p_AD_ImpFormat_ID = para.getParameterAsInt();
			if ( para.getParameterName().equals("AD_PrintFormat_ID") )
				p_AD_PrintFormat_ID = para.getParameterAsInt();
			else 
				log.info("Parameter not found " + para.getParameterName());
		}

	}
	
	/**
	 */
	@Override
	protected String doIt() throws Exception {
		
		if ( p_AD_ImpFormat_ID == 0 )
			throw new AdempiereUserError("Import format required");
		if ( p_AD_PrintFormat_ID == 0)
			throw new AdempiereUserError("Print format required");
		
		MImpFormat imp = new MImpFormat(getCtx(), p_AD_ImpFormat_ID, get_TrxName());
		
		MPrintFormat pf = new MPrintFormat(getCtx(), p_AD_PrintFormat_ID, get_TrxName());
		MPrintFormatItem[] items = pf.getItems();
		int startno = 1;
		int created = 0;
		for (MPrintFormatItem field : items)
		{
			if ( MPrintFormatItem.PRINTFORMATTYPE_Field.equals(field.getPrintFormatType()) && field.getAD_Column_ID() > 0 && field.isPrinted())
			{
				MImpFormatRow row = new MImpFormatRow(imp);
				row.setSeqNo(field.getSeqNo());
				row.setAD_Column_ID(field.getAD_Column_ID());
				row.setName(field.getName());
				row.setStartNo(startno++);
				MColumn col = (MColumn) field.getAD_Column();
				if ( DisplayType.isDate(col.getAD_Reference_ID()))
					row.setDataType(MImpFormatRow.DATATYPE_Date);
				else if (DisplayType.isNumeric(col.getAD_Reference_ID()))
					row.setDataType(MImpFormatRow.DATATYPE_Number);
				else
					row.setDataType(MImpFormatRow.DATATYPE_String);
				row.saveEx();
				created++;
			}
		}
		
		return "Created: " + created;
		
	}

	/**
	 */
	@Override
	protected void postProcess(boolean success) {
		if (success) {
			
		} else {
              
		}
	}

}