/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.util.logging.Level;

import org.compiere.model.MProduct;
import org.compiere.model.MProductBOM;
import org.compiere.util.AdempiereUserError;


/**
 *	BOM Copy
 *	
 *  @author Tony Snook
 *  @version $Id: ProductBOMCopy.java,v 1.0 2013/02/07 tsnook Exp $
 */
public class BOMCopy extends SvrProcess
{
	/** Product From			*/
	private int			p_M_Product_ID = 0;
	/** Product To				*/
	private int			p_M_ProductTo_ID = 0;

	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("M_ProductTo_ID"))
				p_M_ProductTo_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}	//	prepare

	/**
	 * 	Process
	 *	@return message
	 *	@throws Exception
	 */
	protected String doIt () throws Exception
	{
		if (p_M_Product_ID == 0 || p_M_ProductTo_ID == 0)
			throw new AdempiereUserError("Invalid Parameter");
		//
		MProduct product = MProduct.get(getCtx(), p_M_Product_ID);
		MProduct productTo = MProduct.get(getCtx(), p_M_ProductTo_ID);
		log.info("Product=" + product + ", ProductTo=" + productTo);

		if(!product.isBOM())
			throw new AdempiereUserError("Product is not a BOM: "+product.getValue());
		if(!productTo.isBOM())
			throw new AdempiereUserError("To Product is not a BOM: "+productTo.getValue());
		
		int counter = 0;
		MProductBOM[] productsBOMs = MProductBOM.getBOMLines(product);
		if(productsBOMs.length > 0)
		{
			for (MProductBOM productsBOM : productsBOMs)
			{
				MProductBOM productsBOMTo = new MProductBOM(getCtx(), 0, get_TrxName());
				productsBOMTo.setM_Product_ID(productTo.getM_Product_ID());
				productsBOMTo.setM_ProductBOM_ID(productsBOM.getM_ProductBOM_ID());
				productsBOMTo.setLine(productsBOM.getLine());
				productsBOMTo.setBOMQty(productsBOM.getBOMQty());
				productsBOMTo.setBOMType(productsBOM.getBOMType());
				productsBOMTo.setDescription(productsBOM.getDescription());
				productsBOMTo.set_ValueOfColumn("reference_designator", productsBOM.get_Value("reference_designator"));
				productsBOMTo.saveEx();
				counter++;
			}
		}
		else
		{
			throw new AdempiereUserError("No BOM Lines for Product: "+product.getValue());
		}

		return "#" + counter;
	}	//	doIt
	
}	//	BOM Copy
