package org.compiere.process;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.compiere.model.MProduction;
import org.compiere.model.MProductionBatch;


/**
 * 
 * Process to complete Production Batch
 * @author jobriant
 *
 */
public class ProductionBatchComplete extends SvrProcess {

	private int p_M_Production_Batch_ID=0;
	private MProductionBatch m_productionbatch = null;
	
	protected void prepare() {
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			log.log(Level.SEVERE, "Unknown Parameter: " + name);		
		}
		

	}	//prepare

	@Override
	protected String doIt() throws Exception {

		String msg = "";
		p_M_Production_Batch_ID = getRecord_ID();
		m_productionbatch = new MProductionBatch(getCtx(), p_M_Production_Batch_ID, get_TrxName());

		MProduction production = m_productionbatch.getOpenOrder();
		
		if (production == null) {
			m_productionbatch.setProcessed(true);
			m_productionbatch.save();
			msg = "Production Batch completed.";
		}
		else { 
			msg = "Production Batch completed. Production Order#" 
					+ production.getDocumentNo()
					+ " with Production Qty="
					+ production.getProductionQty()
					+ " deleted.";
			production.delete(false);
			production.save();
			m_productionbatch.setQtyOrdered(BigDecimal.ZERO);
			m_productionbatch
					.setCountOrder(m_productionbatch.getCountOrder() - 1);
			m_productionbatch.setProcessed(true);
			m_productionbatch.save();
		}
		return msg;
	}
	


}
