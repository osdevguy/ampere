package org.compiere.process;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.compiere.model.MDistribution;



public class CopyFromDistribution extends SvrProcess {
	private int		glDistributionID = 0;
	@Override
	protected void prepare() {
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("GL_Distribution_ID"))
				glDistributionID = ((BigDecimal)para[i].getParameter()).intValue();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
							
		}
		
		
	}

	@Override
	protected String doIt() throws Exception {
	
		int toGlDistributionID = getRecord_ID();
		log.info("From gl_Distribution_ID=" + glDistributionID + " to " + toGlDistributionID);
		if (toGlDistributionID == 0)
			throw new IllegalArgumentException("Target GL_Distribution_ID == 0");
		if (glDistributionID == 0)
			throw new IllegalArgumentException("Source GL_Distribution_ID == 0");
		MDistribution from = new MDistribution (getCtx(), glDistributionID, get_TrxName());
		MDistribution to = new MDistribution (getCtx(), toGlDistributionID, get_TrxName());
		//
		int no = to.copyLinesFrom (from);		//	no Attributes
		//
		return "@Copied@=" + no;
	}

}
