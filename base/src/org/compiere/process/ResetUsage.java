package org.compiere.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MProduct;
import org.compiere.model.MReplenish;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * 	Reset Usage
 * Reset Average Usage, Max, and Min Reorder Points
 * @author jobriant
 */

public class ResetUsage extends SvrProcess {

	/**	Product Category		*/
	private int		p_M_Product_Category_ID = 0;
	private int 	p_M_Product_ID = 0;
	private String	p_ABCClass = null ;
	private boolean p_ResetAve = false;
	private boolean p_ResetMin = false;
	private boolean p_Substitute = false;
	private boolean p_Adjust = false;
	private int p_M_Warehouse_ID = 0;
	private Timestamp p_AsOfDate;

	//private static BigDecimal defSampling = new BigDecimal( MSysConfig.getValue("AX_REPLENISH_DEFAULT_DAYSAMPLING"));
	//private static BigDecimal defSafetyStock = new BigDecimal( MSysConfig.getValue("AX_REPLENISH_DEFAULT_SAFETYSTOCK"));
	//private static BigDecimal defLeadTime = new BigDecimal( MSysConfig.getValue("AX_REPLENISH_DEFAULT_LEADTIME"));
	private static BigDecimal defSampling = new BigDecimal (4);
	private static BigDecimal defSafetyStock = new BigDecimal (4);
	private static BigDecimal defLeadTime = new BigDecimal (4);

	/**
	 * 	Process
	 *	@return info
	 *	@throws Exception
	 */
	protected String doIt() throws Exception {
				
		return "Updated " + updateLevel() + " records.";
	}

	/**
	 * 	Prepare
	 */
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Warehouse_ID"))
				p_M_Warehouse_ID = para[i].getParameterAsInt();
			else if (name.equals("Date"))
				p_AsOfDate = (Timestamp) para[i].getParameter();
			else if (name.equals("M_Product_Category_ID"))
				p_M_Product_Category_ID = para[i].getParameterAsInt();
			else if (name.equals("ABCClass"))
				p_ABCClass = para[i].getParameter().toString().toUpperCase();
			else if (name.equals("ResetAve"))
				p_ResetAve = para[i].getParameter().toString().equals("Y");
			else if (name.equals("ResetMin"))
				p_ResetMin = para[i].getParameter().toString().equals("Y");
			else if (name.equals("Substitute"))
				p_Substitute = para[i].getParameter().toString().equals("Y");
			else if (name.equals("Adjust"))
				p_Adjust = para[i].getParameter().toString().equals("Y");
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
	
	}
	
	private int updateLevel() {
		
		int counter = 0;
		
		if ( p_AsOfDate == null)
			p_AsOfDate = new Timestamp(System.currentTimeMillis());
		
		for (MProduct product : getProducts()) {
//			BigDecimal leadTime = getLeadTime(product.get_ID()) ;
			
			
			
			for (MReplenish replenish : getReplenishments(product.get_ID())) {
				
				if ( p_M_Warehouse_ID > 0 && replenish.getM_Warehouse_ID() != p_M_Warehouse_ID )
					continue;
					
				BigDecimal minLevel = Env.ZERO;
				BigDecimal maxLevel = Env.ZERO;

				log.log(Level.INFO, "***[MREPLENISH]: M_Product_ID = " + product.get_ID() + "; M_Warehouse_ID = " + replenish.getM_Warehouse_ID() + " ***");

				BigDecimal sampling = (BigDecimal) replenish.get_Value("dayssalessampleperiod");
	
				if ((sampling == null) || (sampling.equals(Env.ZERO))) {
					log.log(Level.INFO, "Sampling Period is not set. Using Default Values. ");
					sampling = defSampling;
				}

				BigDecimal safetyStock = (BigDecimal)replenish.get_Value("safetystockdaysusage");
				if ((safetyStock == null) || (safetyStock.equals(Env.ZERO))) {
					log.log(Level.INFO, "No Safety Stock defined. Using Default Values. ");
					//log.log(Level.SEVERE, "SUM/0 - Will cause division by zero. Set Lead Time = 1");
					safetyStock = defSafetyStock;
				}
				
				String sql = "SELECT max(COALESCE(DeliveryTime_Target, 0)) FROM M_Product_PO " +
						" WHERE M_Product_ID = ? AND IsCurrentVendor = 'Y' " +
						" GROUP BY M_Product_ID ";
				Object[] params = new Object[] {replenish.getM_Product_ID()};			
				
				BigDecimal leadTime = DB.getSQLValueBDEx(get_TrxName(), sql, params);
				if ( leadTime == null || leadTime.compareTo(Env.ZERO) <= 0 )
					leadTime = safetyStock;
				
				BigDecimal usageAve = RecalcUsage.getAveUsage(p_AsOfDate, replenish.getM_Product_ID(), replenish.getM_Warehouse_ID(),sampling , getCtx()); 
				BigDecimal forecastUsage = usageAve;
				if ( p_Adjust )
					forecastUsage = RecalcUsage.getUsagePlusSeasonality(p_AsOfDate, replenish.getM_Product_ID(), replenish.getM_Warehouse_ID(), sampling, leadTime, getCtx());

				//Reset Average, Max and Min
				//Recalculate usage based on historical data and update fields
				if (p_ResetAve) {
					log.log(Level.INFO, "Resetting Average, Max and Min usage for sample period");

					replenish.set_CustomColumn("dailyusageave", usageAve);  // pre-computed ahead because it will be used again (BigDecimal) replenish.get_Value("dayssalessampleperiod");
					replenish.set_CustomColumn("dailyusagemin", RecalcUsage.getMinUsage(p_AsOfDate, replenish.getM_Product_ID(), replenish.getM_Warehouse_ID(), sampling, getCtx()));
					replenish.set_CustomColumn("dailyusagemax", RecalcUsage.getMaxUsage(p_AsOfDate, replenish.getM_Product_ID(), replenish.getM_Warehouse_ID(), sampling, getCtx()));
					replenish.set_CustomColumn("dailyusageaveplusseasonality", RecalcUsage.getUsagePlusSeasonality(p_AsOfDate, replenish.getM_Product_ID(), replenish.getM_Warehouse_ID(), sampling, safetyStock,  getCtx()));

				}

				minLevel = forecastUsage.multiply(safetyStock);
				
				//Reset Minimum based on Average Usage x Days Safety Stock
				//Update MinLevel based on usage x safetyStock
				if(p_ResetMin){
					log.log(Level.INFO, "p_ResetMin - Update level_min  = " + minLevel);
					replenish.set_CustomColumn("level_min", minLevel.setScale(4, BigDecimal.ROUND_HALF_UP));
				}

				//Substitute Daily Usage x Lead Time for Maximum
				//Update MaxLevel based on leadTime x Usage
				if (p_Substitute) {
					maxLevel = minLevel.add(leadTime.multiply(forecastUsage).multiply(new BigDecimal("2")));
					//maxLevel = leadTime.multiply(forecastUsage);
					log.log(Level.FINE, "p_Substitute - Update level_max = " + maxLevel);
					replenish.set_CustomColumn("level_max", maxLevel.setScale(4, BigDecimal.ROUND_HALF_UP));
				}

				replenish.save();
				counter++;
			}
		}
	
		return counter;
	}

	
	private List<MProduct> getProducts()
	{
		List<Object> params = new ArrayList<Object>();
		StringBuffer whereClause = new StringBuffer("AD_Client_ID=?");
		params.add(getAD_Client_ID());

		if (p_M_Product_Category_ID != 0) {
			whereClause.append(" AND M_Product_Category_ID=?");
			params.add(p_M_Product_Category_ID);
		}

		if (p_M_Product_ID != 0) {
			whereClause.append(" AND M_Product_ID=?");
			params.add(p_M_Product_ID);
		}
		
		if (p_ABCClass != null) {
			whereClause.append(" AND UPPER(classification)=UPPER(?)");
			params.add(p_ABCClass);
		}
		
		return new Query(getCtx(),MProduct.Table_Name, whereClause.toString(), get_TrxName())
					.setParameters(params)
					.list();
		
	}

	private List<MReplenish> getReplenishments(int product_id)
	{
		return MReplenish.getForProduct(getCtx(), product_id, get_TrxName());
	}
	
	private BigDecimal getLeadTime(int product_id) {

		BigDecimal leadTime =  DB.getSQLValueBD(
				get_TrxName(),
				"SELECT deliverytime_promised FROM M_Product_PO WHERE m_product_id = ?",
				product_id);
		if ((leadTime == null) || (leadTime.equals(Env.ZERO))) {
			log.log(Level.INFO, "No Lead Time captured. Using Default Values. ");
			//log.log(Level.SEVERE, "SUM/0 - Will cause division by zero. Set Lead Time = 1");
			return defLeadTime;
		}
		else
			return leadTime;
		
	}
	
}
