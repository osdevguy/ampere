/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.util.logging.Level;

import org.compiere.util.DB;


/**
 *	Production Delete
 *	
 *  @author Tony Snook
 */
public class ProductionDelete extends SvrProcess
{
	/** Delete Parameter		*/
	private boolean	p_DeleteOld = true;
	
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("DeleteOld"))
				p_DeleteOld = "Y".equals(para[i].getParameter());
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}

	}	//	prepare

	
	/**
	 * 	Process
	 *	@return message
	 *	@throws Exception
	 */
	protected String doIt () throws Exception
	{
		int no3 = 0;
		int clientid = getAD_Client_ID();
		//
		if (p_DeleteOld)
		{
			String sql1 = "DELETE FROM M_ProductionLineMA ma WHERE ma.AD_Client_ID = " + clientid + " AND EXISTS "
				+ "(SELECT * FROM M_ProductionLine l WHERE l.M_ProductionLine_ID=ma.M_ProductionLine_ID "
				+ " AND l.M_Production_ID IN (SELECT M_Production_ID FROM M_Production WHERE IsWIP='N' AND Processed='N'))";
			int no1 = DB.executeUpdate(sql1, get_TrxName());
			log.info("doIt - Deleted MA #" + no1);
			log.info("doIt - Starting");
			
			String sql2 = "DELETE FROM M_ProductionLine l WHERE l.AD_Client_ID = " + clientid + " AND "
				+ " l.M_Production_ID IN (SELECT M_Production_ID FROM M_Production WHERE IsWIP='N' AND Processed='N')";
			int no2 = DB.executeUpdate(sql2, get_TrxName());
			log.info("doIt - Deleted Line #" + no2);

			String sql3 = "DELETE FROM M_Production WHERE AD_Client_ID = " + clientid + " AND IsWIP='N' AND Processed='N' ";
			no3 = DB.executeUpdate(sql3, get_TrxName());
			log.info("doIt - Deleted #" + no3);
			
			commitEx();
		}
		
		return "Deleted @M_Production_ID@ - #" + no3;
	}	//	doIt
		
}	//	ProductionDelete
