package org.compiere.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MBankAccount;
import org.compiere.model.X_I_Payment;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;

public class ImportPaidOrder extends SvrProcess {

	private int				m_AD_Client_ID = 0;	
	/**	Organization to be imported to	*/
	private int				p_AD_Org_ID = 0;
	/** Default Bank Account			*/
	private int				p_C_BankAccount_ID = 0;
	/**	Delete old Imported				*/
	private boolean			p_deleteOldImported = false;
	/**	Document Action					*/
	private String			m_docAction = null;

	/** Properties						*/
	private Properties 		m_ctx;
	
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("AD_Org_ID"))
				p_AD_Org_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("AD_Client_ID"))
				m_AD_Client_ID = ((BigDecimal)para[i].getParameter()).intValue();			
			else if (name.equals("C_BankAccount_ID"))
				p_C_BankAccount_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else if (name.equals("DeleteOldImported"))
				p_deleteOldImported = "Y".equals(para[i].getParameter());
			else if (name.equals("DocAction"))
				m_docAction = (String)para[i].getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		m_ctx = Env.getCtx();
		
		if (m_AD_Client_ID == 0) {
			m_AD_Client_ID = Env.getAD_Client_ID(m_ctx);
		}

	}

	@Override
	protected String doIt() throws Exception {
		Trx trx = Trx.get(get_TrxName(), true);
		boolean commitOK = true;
		try {
			runCleanup();
			prepareImportOrder();
			prepareImportPayment();
			checkPaymentOrderMatching();
			//
			String orderError = createOrder();

			if  (orderError.length() == 0) {
				String paymentError = createPayment();
				if  (paymentError.length() > 0) {
					log.warning("Create Payment failed:"  + paymentError);
					commitOK = false;
				}
			} else {
				log.warning("Create Order Failed:"  + orderError);
			}
		} catch (Exception e) {
			commitOK = false;
		} finally {
			
			if (commitOK) {
				trx.commit();
			} else {
				getProcessInfo().setLogList(null);
				String error = CLogger.retrieveErrorString("");
				if (!"".equals(error)) {
					addLog(error);
				} 
				addLog("Transaction rolled back.");
				trx.rollback();
			}
			trx.close();
		}
		
		return "Import Payment and Orders Result";
	}





	/**
	 *	Delete Old Imported
	 */
	private void runCleanup()
	{
		if (!p_deleteOldImported) {
			return;
		}
		
		StringBuffer sql = null;
		int no = 0;
		String clientCheck = " AND AD_Client_ID=" + m_AD_Client_ID;

		sql = new StringBuffer ("DELETE I_Order "
			  + "WHERE I_IsImported='Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.fine("Delete Old Imported Order =" + no);
		
		sql = new StringBuffer ("DELETE I_Payment "
				  + "WHERE I_IsImported='Y'").append (clientCheck);
			no = DB.executeUpdate(sql.toString(), get_TrxName());
			log.fine("Delete Old Imported Payment =" + no);
	}
	
	
	private void prepareImportOrder() throws SQLException
	{
		ImportOrder.prepareImportOrder(getCtx(), m_AD_Client_ID, p_AD_Org_ID, get_TrxName());
		commitEx();
		
		//	-- New BPartner ---------------------------------------------------
		//	Go through Order Records w/o C_BPartner_ID
		ImportOrder.assignBusinessPartner(getCtx(), m_AD_Client_ID, get_TrxName());
		commitEx();
	}
	
	private void prepareImportPayment() throws SQLException
	{
		MBankAccount ba = MBankAccount.get(getCtx(), p_C_BankAccount_ID);
		ImportPayment.prepareImportPayment(ba, m_AD_Client_ID, p_AD_Org_ID, get_TrxName());
		commitEx();

	} 
	
	
	/**
	 * Match Order and Payment before creating Records
	 * @throws SQLException
	 */
	private void checkPaymentOrderMatching() throws SQLException 
	{
		StringBuffer sql = null;
		int no = 0;
		String clientCheck = " AND AD_Client_ID=" + m_AD_Client_ID;
		
		//OrderDocumentNo is mandatory in this Process
		sql = new StringBuffer ("UPDATE I_Payment p "
				+ "SET I_IsImported='E', I_ErrorMsg=I_ErrorMsg||'ERR=No Order DocumentNo, '"
				+ "WHERE OrderDocumentNo IS NULL "
				+ " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No Order DocumentNo=" + no);
		
		/*	
		sql = new StringBuffer ("UPDATE I_Payment p "
	            + "SET I_IsImported = 'E', I_ErrorMsg=I_ErrorMsg||'No Clean Order Matched, '  " 
	            + "WHERE p.OrderDocumentNo IS NULL "
	            + "OR EXISTS ( " 
	            + "		SELECT 1 FROM I_Order ox " 
	            + "		WHERE ox.I_ErrorMsg IS NOT NULL " 
	            + "	AND  p.OrderDocumentNo=ox.DocumentNo " 
	            + "	AND p.bpartnervalue=ox.bpartnervalue " 
	            + ") "
	            + "AND p.I_IsImported<>'Y'").append (clientCheck);;		
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No Matched Order=" + no);
		
		
		sql = new StringBuffer ("UPDATE I_Order o "
	            + "SET I_IsImported = 'E', I_ErrorMsg=I_ErrorMsg||'No Clean Payment Matched, '  " 
	            + "WHERE o.DocumentNo IS NULL "
	            + "OR EXISTS ( " 
	            + "		SELECT 1 FROM I_Payment px " 
	            + "		WHERE px.I_ErrorMsg IS NOT NULL " 
	            + "	AND  px.OrderDocumentNo=o.DocumentNo " 
	            + "	AND px.bpartnervalue=o.bpartnervalue " 
	            + ") "
	            + "AND o.I_IsImported<>'Y'").append (clientCheck);;		
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.warning ("No Matched Payment=" + no);
		
		*/
		commitEx();
	}
	
	private String createOrder() throws SQLException  
	{
		return ImportOrder.processImportOrder(getCtx(), getProcessInfo(), m_AD_Client_ID, m_docAction, get_TrxName());
		
	}

	private String createPayment() throws SQLException {
		StringBuffer sql = null;
		StringBuffer error = new StringBuffer(); 
		int no = 0;
		String clientCheck = " AND AD_Client_ID=" + m_AD_Client_ID;
		//  Order - run again after all checks
		sql = new StringBuffer ("UPDATE I_Payment i "
			  + "SET C_Order_ID=(SELECT MAX(C_Order_ID) FROM C_Order o"
			  + " WHERE i.OrderDocumentNo=o.DocumentNo AND i.AD_Client_ID=o.AD_Client_ID) "
			  + "WHERE C_Order_ID IS NULL AND OrderDocumentNo IS NOT NULL"
			  + " AND I_IsImported<>'Y'").append (clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no != 0)
			log.fine("Set Order from DocumentNo=" + no);
		
		
		sql = new StringBuffer("SELECT * FROM I_Payment"
				+ " WHERE I_IsImported='N' AND AD_Client_ID = ?"
				+ " AND EXISTS (SELECT 1 FROM I_Order "
				+ "    WHERE I_Order.DocumentNo = I_Payment.OrderDocumentNo "
				+ " 	AND (I_Order.I_ErrorMsg IS NULL OR length(I_Order.I_ErrorMsg) < 2) ) "
				+ " ORDER BY C_BankAccount_ID, CheckNo, DateTrx, R_AuthCode");
		
		MBankAccount account = null;
		PreparedStatement pstmt = null;
		int noInsert = 0;
		try
		{
			pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
			pstmt.setInt(1, Env.getAD_Client_ID(getCtx()));
			ResultSet rs = pstmt.executeQuery();
				
			while (rs.next())
			{ 
				X_I_Payment imp = new X_I_Payment(m_ctx, rs, get_TrxName());
				//	Get the bank account
				if (account == null || account.getC_BankAccount_ID() != imp.getC_BankAccount_ID())
				{
					account = MBankAccount.get (m_ctx, imp.getC_BankAccount_ID());
					log.info("New Account=" + account.getAccountNo());
				}
				
				if (ImportPayment.createPayment(m_ctx,imp, m_docAction, get_TrxName())) {
					noInsert++;
				} else {
					error.append(imp.get_ValueAsString("OrderDocumentNo") + ", ");
				}
				
			}
			
			//	Close database connection
			rs.close();
			pstmt.close();
			rs = null;
			pstmt = null;
			
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		
		sql = new StringBuffer ("UPDATE I_Payment "
				+ "SET I_IsImported='N', Updated=SysDate "
				+ "WHERE I_IsImported<>'Y'").append(clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		addLog (0, null, new BigDecimal (no), "Import Payment @Errors@");
		
		addLog (0, null, new BigDecimal (noInsert), "@C_Payment_ID@: @Inserted@");
		return error.toString();
	}
}
