package org.compiere.process;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.DBException;
import org.compiere.model.MInOut;
import org.compiere.model.MManifest;
import org.compiere.model.MTable;
import org.compiere.util.CLogger;
import org.compiere.util.Trx;

/**
 * Complete all shipments in the Manifest
 * 
 * @author jobriant
 * 
 */
public class CompleteShipmentManifest extends SvrProcess {

	private int p_M_InOut_Manifest_ID = 0;
	/** Use locking to prevent duplicated invoices */
	private boolean p_useLock = true;
	private int completed;
	private boolean hasError;
	private ArrayList<MInOut> retries = new ArrayList<MInOut>();
	private boolean retry = false;
	/**
	 * Prepare - e.g., get Parameters.
	 */
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_InOut_Manifest_ID"))
				p_M_InOut_Manifest_ID = para[i].getParameterAsInt();
			else if (name.equals("IsUseLock"))
				p_useLock = para[i].getParameterAsBoolean();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);

		}

		if (p_M_InOut_Manifest_ID == 0 && getTable_ID() == MManifest.Table_ID)
			p_M_InOut_Manifest_ID = getRecord_ID();

	} // prepare

	/**
	 * complete Shipments
	 * 
	 * @return info
	 * @throws Exception
	 */
	protected String doIt() throws Exception {
		log.info("M_InOut_Manifest_ID=" + p_M_InOut_Manifest_ID);

		completed = 0;

		MManifest manifest = new MManifest(getCtx(), p_M_InOut_Manifest_ID, get_TrxName());
		hasError = false;

		List<MInOut> shipments = MTable.get(getCtx(), MInOut.Table_ID)
				.createQuery("M_InOut_ID IN (SELECT M_InOut_ID FROM M_InOut_ManifestLine WHERE M_InOut_Manifest_ID=?) ", null)
				.setParameters(p_M_InOut_Manifest_ID)
				.setOrderBy("M_InOut_ID").list();

		ExecutorService pool = Executors.newFixedThreadPool(1);

		for (MInOut shipment : shipments) {
			if (MInOut.DOCSTATUS_Completed.equals(shipment.getDocStatus())
					|| MInOut.DOCSTATUS_Closed.equals(shipment.getDocStatus())) {
				addLog(shipment.getM_InOut_ID(), shipment.getMovementDate(),
						null, shipment.getDocumentNo() + ": Already complete");
				continue;
			}

			pool.submit(completeShipmentTask(shipment));
		}

		pool.shutdown();
		pool.awaitTermination(2, TimeUnit.HOURS);

		// retry deadlocked sequentially
		if (retries.size() > 0) {
			retry = true;
			pool = Executors.newSingleThreadExecutor();
			for (MInOut shipment : retries) {
				log.log(Level.INFO, "Retrying: " + shipment.getDocumentNo());
				pool.submit(completeShipmentTask(shipment));
			}
		}

		pool.shutdown();
		pool.awaitTermination(2, TimeUnit.HOURS);

		if (!hasError) {
			manifest.setProcessed(true);
			manifest.setIsComplete("Y");
			manifest.saveEx();
		}

		return "@Completed@ = " + completed;
	} // doIt

	private Runnable completeShipmentTask(final MInOut shipment) {

		Runnable task = 
				new Runnable() {
			public void run() {
				
				log.log(Level.INFO, "Completing shipment: " + shipment.getDocumentNo());
				Trx trx = Trx.get(Trx.createTrxName(), true);
				try {

					shipment.set_TrxName(trx.getTrxName());
					//			Fails if there is a confirmation
					shipment.setDocAction(DocAction.ACTION_Prepare);
					shipment.processIt(DocAction.ACTION_Complete);
					shipment.saveEx();

					//
					if (MInOut.DOCSTATUS_Completed.equals(shipment
							.getDocStatus())
							|| MInOut.DOCSTATUS_Closed.equals(shipment
									.getDocStatus()))
						addLog(shipment.getM_InOut_ID(),
								shipment.getMovementDate(), null,
								shipment.getDocumentNo() + ": Completed");
					else {
						String error = CLogger.retrieveErrorString("");
								
						throw new AdempiereException("Failed to complete " + shipment.getDocumentNo() + ":" + error);			
					}

					trx.commit();
					completed++;

				} catch (Exception e) {

					if ( DBException.isSQLState(e, "40P01") && !retry )   // deadlock detected  
					{
						log.log(Level.INFO, "Deadlock: " + shipment.getDocumentNo());
						retries.add(shipment);
					}
					else
					{
						
						addLog(shipment.getM_InOut_ID(),
								shipment.getMovementDate(), null,
								shipment.getDocumentNo() + ": Failed to complete-" + e.getMessage());
						log.log(Level.SEVERE, "Error processing shipment:" + e.getMessage());
						hasError = true;
					}
					trx.rollback();
				} finally {
					trx.close();
				}

				log.log(Level.INFO, "Completed shipment: " + shipment.getDocumentNo());
			}
		};
		return task;
	}
}
