package org.compiere.process;

import java.math.BigDecimal;
import java.util.logging.Level;

import org.compiere.model.MEDIFormat;

public class CopyFromEdiFormat extends SvrProcess{

	private int	m_C_EdiFormat_ID = 0;
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_EDIFormat_ID"))
				m_C_EdiFormat_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
	}
	
	@Override
	protected String doIt() throws Exception {
		int To_C_EdiFormat_ID = getRecord_ID();
		log.info("From C_EdiFormat_ID=" + m_C_EdiFormat_ID + " to " + To_C_EdiFormat_ID);
		if (To_C_EdiFormat_ID == 0)
			throw new IllegalArgumentException("Target C_EdiFormat_ID == 0");
		if (m_C_EdiFormat_ID == 0)
			throw new IllegalArgumentException("Source C_EdiFormat_ID == 0");
		MEDIFormat from = new MEDIFormat (getCtx(), m_C_EdiFormat_ID, null);
		MEDIFormat to = new MEDIFormat (getCtx(), To_C_EdiFormat_ID, null);
		
		int no = to.copyLinesFrom (from);
		
		return "@Copied@=" +no;
	}

}
