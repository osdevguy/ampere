package org.compiere.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MPayment;
import org.compiere.model.X_I_Invoice;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Nikunj Panelia
 *
 */
public class ImportInvoiceFromBankStatement extends SvrProcess
{
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare(){

	}

	/**
	 *  Perform process.
	 *  @return clear Message
	 *  @throws Exception
	 */
	protected String doIt() throws Exception
	{
		String unImportCheck = " C_Payment_ID IS NOT null AND C_Invoice_ID IS null AND C_BankAccount_ID IS NOT null AND C_BPartner_ID IS NOT null AND IsChargeOnPayment <> 'Y' "
				+ " AND I_IsImported <>'E' AND AD_Client_ID=" + getAD_Client_ID() ;
		StringBuffer sql=new StringBuffer();
		
		int noInsert = 0;
		sql = new StringBuffer ("SELECT * FROM I_Invoice WHERE ").append(unImportCheck);
		try 
		{
			PreparedStatement pstmt = DB.prepareStatement(sql.toString(),
					get_TrxName());
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				X_I_Invoice imp = new X_I_Invoice(getCtx(), rs, get_TrxName());
				MPayment payment=new MPayment(getCtx(), imp.get_ValueAsInt("C_Payment_ID"),	get_TrxName());
				if(payment.getC_Charge_ID() > 0)
					continue;
			
				MInvoice invoice = new MInvoice(getCtx(), 0, get_TrxName());
				invoice.setAD_Client_ID(getAD_Client_ID());
				invoice.setAD_Org_ID(Env.getAD_Org_ID(getCtx()));
				invoice.setDateAcct(imp.getDateAcct());
				invoice.setDateInvoiced(imp.getDateInvoiced());
				invoice.setC_Currency_ID(imp.getC_Currency_ID());
				invoice.setC_DocType_ID(imp.getC_DocType_ID());
				invoice.setC_BPartner_ID(imp.getC_BPartner_ID());
				if(imp.get_Value("AmtAcctCr") !=null && ((BigDecimal)imp.get_Value("AmtAcctCr")).compareTo(BigDecimal.ZERO) > 0)
					invoice.setIsSOTrx(true);
				if(imp.get_Value("AmtAcctDr") !=null && ((BigDecimal)imp.get_Value("AmtAcctDr")).compareTo(BigDecimal.ZERO) > 0)
					invoice.setIsSOTrx(false);
				if(imp.getC_BPartner_Location_ID()>0)
					invoice.setC_BPartner_Location_ID(imp.getC_BPartner_Location_ID());
				invoice.saveEx();				
				imp.setC_Invoice_ID (invoice.getC_Invoice_ID());

				MInvoiceLine line = new MInvoiceLine(getCtx(), 0, get_TrxName());
				line.setAD_Client_ID(invoice.getAD_Client_ID());
				line.setAD_Org_ID(invoice.getAD_Org_ID());
				line.setC_Tax_ID(imp.getC_Tax_ID());
				line.setTaxAmt(imp.getTaxAmt());
				line.setPrice(imp.getPriceActual());
				
				if(imp.getC_Charge_ID()>0)
					line.setC_Charge_ID(imp.getC_Charge_ID());
				else if(imp.getM_Product_ID()>0)
					line.setM_Product_ID(imp.getM_Product_ID());
									
				line.setQty(BigDecimal.ONE);
				line.setC_Invoice_ID(invoice.get_ID());
				line.saveEx();
	
				imp.setC_InvoiceLine_ID(line.getC_InvoiceLine_ID());
				imp.setI_IsImported(true);
				imp.setProcessed(true);
				imp.saveEx();
				noInsert++;
			}
				
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, "Problem when creating invoice", e);
		}
		return "Invoice Imported: " + noInsert;
	}
}