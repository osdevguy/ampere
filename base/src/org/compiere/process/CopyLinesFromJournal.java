
package org.compiere.process;


import java.math.BigDecimal;
import java.util.logging.Level;

import org.compiere.model.MJournal;


/**
 *  Copy GL Lines
 *  
 * @author Nikunj
 *
 */
public class CopyLinesFromJournal extends SvrProcess
{
	private int		m_GL_Journal_ID = 0;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("GL_Journal_ID"))
				m_GL_Journal_ID = ((BigDecimal)para[i].getParameter()).intValue();
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
	}	//	prepare

	/**
	 *  Perform process.
	 *  @return Message (clear text)
	 *  @throws Exception if not successful
	 */
	protected String doIt() throws Exception
	{
		int To_GL_Journal_ID = getRecord_ID();
		log.info("doIt - From GL_Journal_ID=" + m_GL_Journal_ID + " to " + To_GL_Journal_ID);
		if (To_GL_Journal_ID == 0)
			throw new IllegalArgumentException("Target GL_Journal_ID == 0");
		if (m_GL_Journal_ID == 0)
			throw new IllegalArgumentException("Source GL_Journal_ID == 0");
		MJournal from = new MJournal (getCtx(), m_GL_Journal_ID, get_TrxName());
		MJournal to = new MJournal (getCtx(), To_GL_Journal_ID, get_TrxName());
		//
		int no = to.copyDetailsFrom (from);
		//
		return "@Copied@=" + no;
	}	//	doIt

}
