/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.io.File;
import java.util.ArrayList;

import org.compiere.impexp.ImpFormat;
import org.compiere.model.MPInstance;
import org.compiere.model.MProcess;
import org.compiere.model.MProcessPara;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.Util;

public class ImportFileLoader extends SvrProcess {
	
	private ImpFormat 			m_format;
	private File				m_file = null;

	
	/**	Logger							*/
	protected CLogger			log = CLogger.getCLogger (getClass());

	// Parameters
	private String		p_FileName = null;
	private String 		p_CharsetName = null;
	private int 		p_AD_ImpFormat_ID;
	private int 		p_AD_Client_ID = 0;
	private int			p_AD_Org_ID = 0;
	private int			p_AD_User_ID = 0;
	private boolean 	p_excludeHeaderRow = false;
	private int 	p_AD_Process_ID = 0;
	/**
	 */
	@Override
	protected void prepare() {

		// Each Report & Process parameter name is set by the field DB Column Name
		for ( ProcessInfoParameter para : getParameter())
		{
			if ( para.getParameterName().equals("FileName") )
				p_FileName = para.getParameterAsString();			
			else if ( para.getParameterName().equals("CharSetName") )
				p_CharsetName = para.getParameterAsString();
			else if ( para.getParameterName().equals("AD_ImpFormat_ID") )
				p_AD_ImpFormat_ID = para.getParameterAsInt();
			else if ( para.getParameterName().equals("IsExcludeHeader") )
				p_excludeHeaderRow = para.getParameterAsBoolean();
			else if ( para.getParameterName().equals("AD_Client_ID") )
				p_AD_Client_ID = para.getParameterAsInt();
			else if ( para.getParameterName().equals("AD_Org_ID") )
				p_AD_Org_ID = para.getParameterAsInt();
			else if ( para.getParameterName().equals("AD_User_ID") )
				p_AD_User_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("AD_Process_ID"))
				p_AD_Process_ID = para.getParameterAsInt();
		}
	}
	
	/**
	 */
	@Override
	protected String doIt() throws Exception {
		
		if (Util.isEmpty(p_FileName))
			throw new AdempiereUserError("FileName required");
		
		m_file = new File(p_FileName);
		if (m_file == null || !m_file.exists())
			return "No file at " + p_FileName + ".";
		
		if ( p_AD_ImpFormat_ID <= 0 )
			throw new AdempiereUserError("Import Format required");
		
		m_format = ImpFormat.load(p_AD_ImpFormat_ID);
		if ( m_format == null )
			throw new AdempiereSystemError("Import Format not loaded");

	
		int loaded = m_format.loadFile(getCtx(), p_CharsetName, m_file, get_TrxName(), p_AD_Client_ID, p_AD_Org_ID, p_AD_User_ID, p_excludeHeaderRow );
		addLog("File:" + m_file.getName() + ", Loaded Rows: " + loaded);
		
		if (p_AD_Process_ID > 0)
		{

	        MProcess process = MProcess.get(getCtx(), p_AD_Process_ID);
	        process.set_TrxName(get_TrxName());
			MPInstance instance = new MPInstance(process, 0);
	        instance.saveEx();
	        ProcessInfo poInfo = new ProcessInfo(process.getName(), p_AD_Process_ID);
	        
	        ArrayList<ProcessInfoParameter> parameters = new ArrayList<ProcessInfoParameter>();
	        
	        MProcessPara[] paras = process.getParameters();
	        for (MProcessPara para : paras)
	        {
	        	String name = para.getColumnName();
	        	for (ProcessInfoParameter param : getParameter() )
	        	{
	        		if (name.equals(param.getParameterName()))
	        		{
	        			parameters.add(new ProcessInfoParameter(name, param.getParameter(),
	        					param.getParameter_To(), param.getInfo(), param.getInfo_To()));
	        			break;
	        		}
	        	}
	        }
	        
	        ProcessInfoParameter params[] = new ProcessInfoParameter[parameters.size()];
	        params = parameters.toArray(params);
	        
	        poInfo.setParameter(params); 
	        poInfo.setAD_PInstance_ID(instance.get_ID());
	        
			poInfo.setAD_User_ID(p_AD_User_ID > 0 ? p_AD_User_ID : Env.getAD_User_ID(getCtx()));
			poInfo.setAD_Client_ID(p_AD_Client_ID > 0 ? p_AD_Client_ID :Env.getAD_Client_ID(getCtx()));
			poInfo.setAD_PInstance_ID(instance.getAD_PInstance_ID());
			if ( !process.processIt(poInfo, Trx.get(get_TrxName(), false)) ) {
				throw new Exception("Failed to process - " + poInfo.getSummary());
			}
		}
		
		return "@OK@";
	}

}
