package org.compiere.process;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MProduction;
import org.compiere.model.MProductionBatch;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;


/**
 * 
 * Process to create production lines based on the plans
 * defined for a particular production header
 * @author Paul Bowden
 *
 */
public class ProductionCreate extends SvrProcess {

	private int p_M_Production_ID=0;
	private MProduction m_production = null;
	private boolean mustBeStocked = false;  //not used
	private boolean recreate = false;
	private BigDecimal newQty = null;
	//private int p_M_Locator_ID=0;
	
	private int[] asi = null;
	
	protected void prepare() {
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if ("Recreate".equals(name))
				recreate = "Y".equals(para[i].getParameter());
			else if ("ProductionQty".equals(name))
				newQty  = (BigDecimal) para[i].getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);		
		}
		
		p_M_Production_ID = getRecord_ID();
		m_production = new MProduction(getCtx(), p_M_Production_ID, get_TrxName());
		
		String sql = "SELECT M_AttributesetInstance_ID FROM M_ProductionLine WHERE IsUseThisTime = 'Y' " + 
						" AND M_AttributesetInstance_ID <> 0 AND M_Production_ID = ?";
		
		asi = DB.getIDsEx(null, sql, p_M_Production_ID);

	}	//prepare

	@Override
	protected String doIt() throws Exception {

		if ( m_production.get_ID() == 0 )
			throw new AdempiereUserError("Could not load production header");

		if ( m_production.isProcessed() )
			return "Already processed";

		return createLines();

	}
	

	protected String createLines() throws Exception {
		
		int created = 0;
		isBom(m_production.getM_Product_ID());
		
		if (!m_production.costsOK())
			throw new AdempiereUserError("Excessive difference in standard costs");
		
		if (!recreate && "true".equalsIgnoreCase(m_production.get_ValueAsString("IsCreated")))
			throw new AdempiereUserError("Production already created.");

		// Check batch having production planned Qty.
		BigDecimal cntQty = Env.ZERO;
		MProductionBatch pBatch = (MProductionBatch) m_production.getM_Production_Batch();
		for (MProduction p : pBatch.getHeaders(true))
		{
			if (p.getM_Production_ID() != m_production.getM_Production_ID())
				cntQty = cntQty.add(p.getProductionQty());
		}

		StringBuffer sb = new StringBuffer();
		BigDecimal maxPlanQty = pBatch.getTargetQty().subtract(cntQty);
		if ((newQty.signum() >= 0 && maxPlanQty.signum() >= 0 && newQty.compareTo(maxPlanQty) > 0)
				|| (newQty.signum() <= 0 && maxPlanQty.signum() <= 0 && newQty.abs().compareTo(maxPlanQty.abs()) > 0))
		{
			sb.append("Production batch target qty is: " + pBatch.getTargetQty()
					+ " <BR/>Total production planned qty on current batch is: " + cntQty
					+ " <BR/>Maximum plan qty should be: " + maxPlanQty + "<BR/>");
		}
		
		if (newQty != null )
			m_production.setProductionQty(newQty);
		
		m_production.deleteLines(get_TrxName());
		m_production.save(get_TrxName());
		created = m_production.createLines(mustBeStocked);
		if ( created == 0 ) {
			return "Failed to create production lines"; 
		}
		
		m_production.reAssignASI(asi);
		m_production.setIsCreated(true);
		m_production.save(get_TrxName());
		
		//jobrian - update Production Batch
		int M_Production_Batch_ID = m_production.get_ValueAsInt("M_Production_Batch_ID");
		MProductionBatch batch = new MProductionBatch(getCtx(), M_Production_Batch_ID, get_TrxName());
		batch.setQtyOrdered(m_production.getProductionQty());
		batch.save();
		
		sb.append(created + " production lines were created");
		return sb.toString();
	}
	
	protected void isBom(int M_Product_ID) throws Exception
	{
		String bom = DB.getSQLValueString(get_TrxName(), "SELECT isbom FROM M_Product WHERE M_Product_ID = ?", M_Product_ID);
		if ("N".compareTo(bom) == 0)
		{
			throw new AdempiereUserError ("Attempt to create product line for Non Bill Of Materials");
		}
		int materials = DB.getSQLValue(get_TrxName(), "SELECT count(M_Product_BOM_ID) FROM M_Product_BOM WHERE M_Product_ID = ?", M_Product_ID);
		if (materials == 0)
		{
			throw new AdempiereUserError ("Attempt to create product line for Bill Of Materials with no BOM Products");
		}
	}
}
