/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2011 Adaxa Pty Ltd. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html 	     *
 *****************************************************************************/
package org.compiere.process;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Random;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MDocType;
import org.compiere.model.MOpportunity;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MSalesStage;
import org.compiere.util.AdempiereUserError;


/**
 *	Generate randomized data for tests/demos
 *	
 *  @author Paul Bowden
 * 
 */
public class GenerateTestData extends SvrProcess
{
	private int			p_C_DocTypeTarget_ID = 0;
	private int			p_M_PriceList_Version_ID = 0;
	private Timestamp	p_Date_From = null;
	private Timestamp	p_Date_To = null;
	private int[] bps;
	private int[] stages;
	private Random random = new Random();
	private Calendar cal = Calendar.getInstance();
	private long duration = 0;
	private int[] products;
	private int p_Count = 10;
	private int p_SalesRep_ID = 0;
	private int p_AD_Org_ID = 0;
	private int p_M_Warehouse_ID = 0;
	private String p_DocAction = MOrder.DOCACTION_Complete;

	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_DocTypeTarget_ID"))
				p_C_DocTypeTarget_ID = para[i].getParameterAsInt();
			else if (name.equals("DateTrx"))
			{
				p_Date_From = (Timestamp)para[i].getParameter();
				p_Date_To = (Timestamp)para[i].getParameter_To();
			} else if (name.equals("M_PriceList_Version_ID")) {
				p_M_PriceList_Version_ID = para[i].getParameterAsInt();
			} 
			else if (name.equals("SalesRep_ID")) {
				p_SalesRep_ID  = para[i].getParameterAsInt();
			}
			else if (name.equals("AD_Org_ID")) {
				p_AD_Org_ID  = para[i].getParameterAsInt();
			}
			else if (name.equals("M_Warehouse_ID")) {
				p_M_Warehouse_ID  = para[i].getParameterAsInt();
			}
			else if (name.equals("Count"))
			{
				p_Count  = para[i].getParameterAsInt();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		if ( p_SalesRep_ID == 0 )
			p_SalesRep_ID = getAD_User_ID();
		
		
		
	}	//	prepare

	/**
	 * 	Process
	 *	@return msg
	 *	@throws Exception
	 */
	protected String doIt () throws Exception
	{
		log.info("C_DocTypeTarget_ID=" + p_C_DocTypeTarget_ID 
			+ ", Date=" + p_Date_From + "->" + p_Date_To
			+ ", M_PriceList_Version_ID=" + p_M_PriceList_Version_ID);
		
		if (p_C_DocTypeTarget_ID == 0)
			throw new AdempiereUserError("@NotFound@: @C_DocTypeTarget_ID@");
		

		MDocType dt = MDocType.get(getCtx(), p_C_DocTypeTarget_ID);
		if (!dt.getDocBaseType().equals(MDocType.DOCBASETYPE_SalesOrder))
			throw new AdempiereException("Only Sales Order document types supported");
		
		 bps = MBPartner.getAllIDs(MBPartner.Table_Name, "IsCustomer='Y' AND IsActive='Y' AND AD_Client_ID=" + getAD_Client_ID(), get_TrxName());
		 products = MProduct.getAllIDs(MProduct.Table_Name, "IsActive='Y' AND EXISTS (SELECT * FROM" +
		 		" M_ProductPrice pp " +
		 		" WHERE pp.M_Product_ID=M_Product.M_Product_ID AND pp.M_PriceList_Version_ID=" + p_M_PriceList_Version_ID + ") ", get_TrxName());
		 stages = MSalesStage.getAllIDs(MSalesStage.Table_Name, "IsActive='Y' AND IsClosed='N' AND AD_Client_ID=" + getAD_Client_ID(), get_TrxName());
		
		
		if (p_Date_To == null || p_Date_To.getTime() < p_Date_From.getTime())
			p_Date_To = new Timestamp(cal.getTimeInMillis());
		
		if (p_Date_From == null || p_Date_To.getTime() < p_Date_From.getTime())
		{
			cal.add(Calendar.YEAR, -1);
			p_Date_From = new Timestamp(cal.getTimeInMillis());
		}
		else
		{
			cal.setTime(p_Date_From);
		}
		
		duration  = p_Date_To.getTime() - p_Date_From.getTime();
		
		
		for ( int i = 0; i < p_Count; i++ )
		{
			createRandomOrder();
		}
		
		return "@OK@";
	}	//	doIt
	
	/*
	 * Create a random document
	 */
	private void createRandomOrder() {
		
		MOrder order = new MOrder(getCtx(), 0, get_TrxName());
		MBPartner bp = getRandomCustomer();
		
		order.setBPartner(bp);
		
		order.setAD_Org_ID(p_AD_Org_ID);
		order.setM_PriceList_ID(p_M_PriceList_Version_ID);
		long addTime = Math.abs(random.nextLong()) % duration;
		
		Timestamp date = new Timestamp(p_Date_From.getTime() + addTime);
		order.setDateOrdered(date);
		order.setDateAcct(date);
		order.setDatePromised(date);
		order.setM_Warehouse_ID(p_M_Warehouse_ID);
		int salesRep_ID = p_SalesRep_ID;
		if (bp.getSalesRep_ID() != 0)
			salesRep_ID = bp.getSalesRep_ID();
		order.setSalesRep_ID(salesRep_ID);   // TODO randomize sales rep?
		order.setC_DocTypeTarget_ID(p_C_DocTypeTarget_ID);
		order.saveEx();
		
		for ( int i = 0; i < random.nextInt(9) + 1; i++)
		{
			MOrderLine ol = new MOrderLine(order);
			ol.setProduct(getRandomProduct());
			ol.setQty(new BigDecimal(random.nextInt(100)));
			ol.saveEx();
		}

		MDocType dt = MDocType.get(getCtx(), p_C_DocTypeTarget_ID);
		
		if ( MDocType.DOCSUBTYPESO_Proposal.equals(dt.getDocSubTypeSO()) || MDocType.DOCSUBTYPESO_Quotation.equals(dt.getDocSubTypeSO()) )
				p_DocAction = MOrder.DOCACTION_Prepare;
		
		order.setDocAction(p_DocAction);
		order.processIt(p_DocAction);
		order.saveEx();
		
		if (MDocType.DOCSUBTYPESO_Proposal.equals(dt.getDocSubTypeSO()) || MDocType.DOCSUBTYPESO_Quotation.equals(dt.getDocSubTypeSO()) )
		{
			MOpportunity op = new MOpportunity(getCtx(), 0, get_TrxName());
			op.setAD_Org_ID(order.getAD_Org_ID());
			op.setAD_User_ID(order.getAD_User_ID());
			op.setC_BPartner_ID(order.getC_BPartner_ID());
			op.setC_Campaign_ID(order.getC_Campaign_ID());
			op.setC_Currency_ID(order.getC_Currency_ID());
			op.setC_Order_ID(order.getC_Order_ID());
			op.setExpectedCloseDate(order.getDatePromised());
			op.setOpportunityAmt(order.getGrandTotal());
			MSalesStage stage = getRandomStage();
			op.setC_SalesStage_ID(stage.getC_SalesStage_ID());
			op.setSalesRep_ID(order.getSalesRep_ID());
			op.setProbability(stage.getProbability());
			op.saveEx();
			order.set_ValueOfColumn("C_Opportunity_ID", op.getC_Opportunity_ID());
			order.saveEx();
		}
		
		addLog("@Created@: " + order.getDocumentNo());
		
	}

	private MProduct getRandomProduct() {
		
		int productId = products[random.nextInt(products.length)];
		return MProduct.get(getCtx(), productId);
	}

	private MBPartner getRandomCustomer() {
		
		int bpId = bps[random.nextInt(bps.length)];
		return new MBPartner(getCtx(), bpId, get_TrxName());
		
	}	
	
	private MSalesStage getRandomStage() {
		
		int stageId = stages[random.nextInt(stages.length)];
		return new MSalesStage(getCtx(), stageId, get_TrxName());
	}
}
