/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.compiere.model.MArchive;
import org.compiere.model.MAttachment;
import org.compiere.model.MTable;
import org.compiere.model.Query;

public class CopyBinaryObjects extends SvrProcess {


	private int p_destination = 0;
	private String p_path = "";
	private String p_type = "";
	private static final String BLOB_TYPE_ATTACHMENT = "ATT";
	private static final String BLOB_TYPE_ARCHIVE = "ARC";
	private static final String BLOB_TYPE_IMAGE = "IMG";
	
	@Override
	protected void prepare() {

		// Each Report & Process parameter name is set by the field DB Column Name
		for ( ProcessInfoParameter para : getParameter())
		{
			if ( para.getParameterName().equals("Destination") )
				p_destination = para.getParameterAsInt();
			else if ( para.getParameterName().equals("Path") )
				p_path = para.getParameterAsString();
			else if ( para.getParameterName().equals("Type"))
				p_type = para.getParameterAsString();
			else 
				log.info("Parameter not found " + para.getParameterName());
		}

	}

	@Override
	protected String doIt() throws Exception {

		int success = 0;
		if (BLOB_TYPE_ATTACHMENT.equals(p_type))
		{
			Query query = MTable.get(getCtx(), MAttachment.Table_ID).createQuery(null, get_TrxName());
			query.setOrderBy("AD_Table_ID, Record_ID");
			query.setClient_ID();

			List<MAttachment> attachments =	query.list();

			for (MAttachment attachment : attachments)
			{
				String tablename = MTable.getTableName(getCtx(), attachment.getAD_Table_ID());
				if ( attachment.copyAttachment(p_destination, p_path) )
					success++;
				else
					addLog(attachment.getAD_Attachment_ID(), new Timestamp(new Date().getTime()), null,
							"Failed to copy attachment for Table: " + tablename + ", Record ID: " + attachment.getRecord_ID());
			}
		}
		else if (BLOB_TYPE_ARCHIVE.equals(p_type))
		{

			Query query = MTable.get(getCtx(), MArchive.Table_ID).createQuery(null, get_TrxName());
			query.setOrderBy("AD_Table_ID, Record_ID");
			query.setClient_ID();

			List<MArchive> archives =	query.list();

			for (MArchive archive : archives)
			{
				String tablename = MTable.getTableName(getCtx(), archive.getAD_Table_ID());
				if ( archive.copyArchive(p_destination, p_path) )
					success++;
				else
					addLog(archive.getAD_Archive_ID(), new Timestamp(new Date().getTime()), null,
							"Failed to copy archive for Table: " + tablename + ", Record ID: " + archive.getRecord_ID());
			}

		}
		return "Copied: " + success;
	}

	
}