/*******************************************************************************
 * (C) Astidian Systems 2012                                                   *
 *                                                                             *
 ******************************************************************************/
package com.astidian.compiere.fa;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MGAASDepreciationExpense;
import org.compiere.model.MPeriod;
import org.compiere.util.Env;

/**
 * @author Ashley G Ramdass
 * 
 */
public class SLDepreciation extends Depreciation
{

    public SLDepreciation(Properties ctx, int assetId, String trxName)
    {
        super(ctx, assetId, trxName);
    }

    @Override
    public void updateDepreciationExpenses(int assetAcctId)
    {
        int totalPeriods = getTotalUselifeMonths();
        BigDecimal depreciationRate = getDepreciationRate(assetAcctId);

        if (totalPeriods == 0)
        {
            throw new IllegalStateException("Total periods = 0");
        }

        if (getAssetCost() == null || getAssetCost().compareTo(Env.ZERO) <= 0)
        {
            throw new IllegalStateException("Asset cost invalid: "
                    + getAssetCost());
        }

        int precision = getPrecision(assetAcctId);
        BigDecimal initialAccumDepr = getInitialAccumulatedDepr();
        BigDecimal assetCost = getAssetCost();

        int startPeriod = getStartPeriod();

        if (startPeriod >= totalPeriods)
        {
            return;
        }

        int depreciationPeriods = (totalPeriods - startPeriod) + 1;
      
        BigDecimal depreciableAmt = assetCost.subtract(getSalvageValue());

        // Count for periods that have already been processed
        int depreciatedPeriodsCount = getProcessedDepreciationsCount(getCtx(),
                getAssetId(), assetAcctId, getTrxName());

        // Compute the new depreciation periods based on number of periods and
        // already processed periods
        depreciationPeriods = depreciationPeriods - depreciatedPeriodsCount;

        BigDecimal depreciationExpenseAmt = assetCost
                .subtract(initialAccumDepr);
        depreciationExpenseAmt = depreciationExpenseAmt
                .subtract(getSalvageValue());

        // Take into consideration the already processed accumulated
        // depreciations
        depreciationExpenseAmt = depreciationExpenseAmt
                .subtract(getAccumulatedDeprAmt(getCtx(), getAssetId(),
                        assetAcctId, getTrxName()));

        if (depreciationExpenseAmt.compareTo(Env.ZERO) <= 0)
        {
            return;
        }

        String description = "SL Depreciation";
        int totalDeprecabelPeriod=0;
        if (depreciationRate.compareTo(Env.ZERO) > 0)
        {
        	totalDeprecabelPeriod=(BigDecimal.valueOf(1200).divide(depreciationRate.multiply(BigDecimal.valueOf(100)),BigDecimal.ROUND_HALF_UP)).intValue();
        	depreciationExpenseAmt=depreciationExpenseAmt.divide(BigDecimal.valueOf(totalDeprecabelPeriod-depreciatedPeriodsCount),precision);
        	
         /*   depreciationExpenseAmt = depreciationExpenseAmt
                    .multiply(depreciationRate);
            depreciationExpenseAmt = depreciationExpenseAmt.divide(
                    new BigDecimal(12), 6, BigDecimal.ROUND_HALF_UP);*/
            description += " - Percentage = "
                    + depreciationRate.multiply(new BigDecimal(100))
                            .toPlainString() + "%";
        }
        else
        {
            depreciationExpenseAmt = depreciationExpenseAmt.divide(
                    new BigDecimal(depreciationPeriods), 8,
                    BigDecimal.ROUND_HALF_UP);
        }

        BigDecimal openingAccumDepr = initialAccumDepr;
        BigDecimal closingAccumDepr = Env.ZERO;
        Timestamp depreciationDate = getAssetDepreciationDate();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(depreciationDate.getTime());

        super.deleteNotProcessedDepreciations(assetAcctId);

        List<MGAASDepreciationExpense> processedExpenses = MGAASDepreciationExpense
                .getProcessedExpenses(getCtx(), getAssetId(),
                        new Timestamp(0), depreciationDate, getTrxName());

        for (MGAASDepreciationExpense expense : processedExpenses)
        {
            closingAccumDepr = openingAccumDepr.add(expense
                    .getDepreciationExpenseAmt());
            expense.setOpeningBalance(openingAccumDepr.setScale(precision,
                    BigDecimal.ROUND_HALF_UP));
            expense.setClosingBalance(closingAccumDepr.setScale(precision,
                    BigDecimal.ROUND_HALF_UP));
            expense.setPeriodNo(startPeriod);
            expense.saveEx();
            openingAccumDepr = closingAccumDepr;
            startPeriod++;
        }
        
        for (int i = startPeriod; i <= totalPeriods; i++)
        {
        	if(totalDeprecabelPeriod > 0 && i > totalDeprecabelPeriod)
           		break;
        	Timestamp currDate = new Timestamp(cal.getTimeInMillis());
            cal.add(Calendar.MONTH, 1);
            Timestamp nextDate = new Timestamp(cal.getTimeInMillis());

            List<MGAASDepreciationExpense> expenses = MGAASDepreciationExpense
                    .getProcessedExpenses(getCtx(), getAssetId(), currDate,
                            nextDate, getTrxName());

            if (expenses.size() == 0)
            {
                MGAASDepreciationExpense expense = new MGAASDepreciationExpense(
                        getCtx(), 0, getTrxName());
                expense.setAD_Org_ID(getAsset().getAD_Org_ID());
                expense.setGAAS_Asset_Acct_ID(assetAcctId);
                expense.setCostAmt(assetCost);
                expense.setA_Asset_ID(getAssetId());
                expense.setDepreciationExpenseAmt(depreciationExpenseAmt
                        .setScale(precision, BigDecimal.ROUND_HALF_UP));
                expense.setAssetDepreciationDate(currDate);
                expense.setDescription(description);
                MPeriod acctPeriod = MPeriod.get(getCtx(),
                        currDate, expense.getAD_Org_ID());
                if (acctPeriod != null)
                {
                    expense.setC_Period_ID(acctPeriod.getC_Period_ID());
                }
                closingAccumDepr = openingAccumDepr.add(depreciationExpenseAmt);
                expense.setPeriodNo(i);
                expense.setOpeningBalance(openingAccumDepr.setScale(precision,
                        BigDecimal.ROUND_HALF_UP));
                expense.setClosingBalance(closingAccumDepr.setScale(precision,
                        BigDecimal.ROUND_HALF_UP));

                expense.saveEx();
                
                openingAccumDepr = closingAccumDepr;
            }
            else
            {
                for (MGAASDepreciationExpense expense : expenses)
                {
                    closingAccumDepr = openingAccumDepr.add(expense
                            .getDepreciationExpenseAmt());
                    expense.setPeriodNo(i);
                    expense.setOpeningBalance(openingAccumDepr.setScale(precision,
                            BigDecimal.ROUND_HALF_UP));
                    expense.setClosingBalance(closingAccumDepr.setScale(precision,
                            BigDecimal.ROUND_HALF_UP));

                    expense.saveEx();
                    openingAccumDepr = closingAccumDepr;
                    i++;
                }
                
                i--;
            }

            if (openingAccumDepr.add(depreciationExpenseAmt).compareTo(
                    depreciableAmt) > 0)
            {
                depreciationExpenseAmt = depreciableAmt
                        .subtract(openingAccumDepr);
            }

            if (depreciationExpenseAmt.compareTo(Env.ZERO) <= 0)
            {
                depreciationExpenseAmt = Env.ZERO;
            }
        }
        if(totalDeprecabelPeriod>0 && totalPeriods > totalDeprecabelPeriod)
        {
        	for(int j=1 ; j<=totalPeriods-totalDeprecabelPeriod ; j++)
        	{
        		Timestamp resumeDate = new Timestamp(cal.getTimeInMillis());
        		cal.add(Calendar.MONTH, 1);
        		MGAASDepreciationExpense expense = new MGAASDepreciationExpense(
                        getCtx(), 0, getTrxName());
                expense.setAD_Org_ID(getAsset().getAD_Org_ID());
                expense.setGAAS_Asset_Acct_ID(assetAcctId);
                expense.setCostAmt(assetCost);
                expense.setA_Asset_ID(getAssetId());
                expense.setDepreciationExpenseAmt(BigDecimal.ZERO);
                expense.setAssetDepreciationDate(resumeDate);
                expense.setDescription(description);
                MPeriod acctPeriod = MPeriod.get(getCtx(),
                		resumeDate, expense.getAD_Org_ID());
                if (acctPeriod != null)
                {
                    expense.setC_Period_ID(acctPeriod.getC_Period_ID());
                }
                closingAccumDepr = openingAccumDepr.add(depreciationExpenseAmt);
                expense.setPeriodNo(totalDeprecabelPeriod+j);
                expense.setOpeningBalance(openingAccumDepr.setScale(precision,
                        BigDecimal.ROUND_HALF_UP));
                expense.setClosingBalance(closingAccumDepr.setScale(precision,
                        BigDecimal.ROUND_HALF_UP));

                expense.saveEx();
            
        	}
        }
        
        
        super.deleteDeprecatedDepreciations();
    }
}
