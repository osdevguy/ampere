/**
 * 
 */
package com.astidian.compiere.fa;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;

import org.compiere.model.MGAASAssetJournal;
import org.compiere.model.MGAASAssetJournalLine;
import org.compiere.model.MPeriod;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

/**
 * @author Ashley G Ramdass
 * 
 */
public class GenerateDepreciationEntries extends SvrProcess
{
    private static final CLogger logger = CLogger.getCLogger(GenerateDepreciationEntries.class);
    private int assetJournalId;
    private int p_assetId;
    private int p_assetGroupId;

    @Override
    protected void prepare()
    {
        this.assetJournalId = getRecord_ID();

        ProcessInfoParameter[] para = getParameter();
        for (ProcessInfoParameter element : para)
        {
            String name = element.getParameterName();
            if (element.getParameter() == null)
            {
                continue;
            }
            else if (name.equals("A_Asset_ID"))
            {
                p_assetId = element.getParameterAsInt();
            }
            else if (name.equals("A_Asset_Group_ID"))
            {
                p_assetGroupId = element.getParameterAsInt();
            }
            else
            {
                logger.log(Level.SEVERE, "Unknown Parameter: " + name);
            }
        }

    }

    @Override
    protected String doIt() throws Exception
    {
        if (this.assetJournalId <= 0)
        {
            throw new IllegalStateException("Asset Journal invalid");
        }

        MGAASAssetJournal assetJournal = new MGAASAssetJournal(getCtx(),
                assetJournalId, get_TrxName());

        if (!MGAASAssetJournal.ENTRYTYPE_Depreciation.equals(assetJournal
                .getEntryType()))
        {
            throw new IllegalStateException(
                    "Asset Journal type is not depreciation");
        }

        int periodId = assetJournal.getC_Period_ID();

        if (periodId <= 0)
        {
            throw new IllegalStateException("Invalid period");
        }
        MPeriod period= MPeriod.get(getCtx(), periodId);
        String sql = "SELECT de.A_Asset_ID, de.GAAS_Depreciation_Expense_ID, de.Description"
                + " FROM GAAS_Depreciation_Expense de"
                + " INNER JOIN A_Asset a"
                + " ON a.A_Asset_ID=de.A_Asset_ID"
                + " WHERE (de.C_Period_ID=? or de.assetdepreciationdate Between (? :: date) and (? :: date)) "
                + " AND NOT EXISTS (SELECT * FROM GAAS_AssetJournalLine ajl"
                + " INNER JOIN GAAS_AssetJournal aj"
                + " ON aj.GAAS_AssetJournal_ID=ajl.GAAS_AssetJournal_ID"
                + " AND aj.DocStatus <> 'VO'"
                + " AND ajl.GAAS_Depreciation_Expense_ID=de.GAAS_Depreciation_Expense_ID)";
        
        if (p_assetId > 0)
        {
            sql += " AND de.A_Asset_ID=?";
        }
        
        if (p_assetGroupId > 0)
        {
            sql += " AND a.A_Asset_Group_ID=?";
        }
        
        sql += " AND a.IsActive='Y' AND a.IsDisposed<>'Y' AND de.DepreciationExpenseAmt>0";
        
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        int count = 0;
        
        try
        {
            int paramIndex = 0;
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(++paramIndex, periodId);
            pstmt.setDate(++paramIndex, new Date(period.getStartDate().getTime()));
            pstmt.setDate(++paramIndex, new Date(period.getEndDate().getTime()));
            
            
            if (p_assetId > 0)
            {
                pstmt.setInt(++paramIndex, p_assetId);
            }
            
            if (p_assetGroupId > 0)
            {
                pstmt.setInt(++paramIndex, p_assetGroupId);
            }
            
            rs = pstmt.executeQuery();
            
            while (rs.next())
            {
                int assetId = rs.getInt(1);
                int deprExpenseId = rs.getInt(2);
                String description = rs.getString(3);
                
                MGAASAssetJournalLine journalLine = new MGAASAssetJournalLine(getCtx(), 0, get_TrxName());
                journalLine.setGAAS_AssetJournal_ID(assetJournalId);
                journalLine.setA_Asset_ID(assetId);
                journalLine.setGAAS_Depreciation_Expense_ID(deprExpenseId);
                journalLine.setDescription(description);
                journalLine.saveEx();
                count++;
            }
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, "Could not query data", ex);
            return "Error";
        }
        finally
        {
            DB.close(rs, pstmt);
        }
        

        return "Count #" + count;
    }
}
