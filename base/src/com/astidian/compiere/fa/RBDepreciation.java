/*******************************************************************************
 * (C) Astidian Systems 2012                                                   *
 *                                                                             *
 ******************************************************************************/
package com.astidian.compiere.fa;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MGAASDepreciationExpense;
import org.compiere.model.MPeriod;
import org.compiere.util.Env;

/**
 * @author Ashley G Ramdass
 * 
 */
public class RBDepreciation extends Depreciation
{

    public RBDepreciation(Properties ctx, int assetId, String trxName)
    {
        super(ctx, assetId, trxName);
    }

    @Override
    public void updateDepreciationExpenses(int assetAcctId)
    {
        int totalPeriods = getTotalUselifeMonths();

        if (totalPeriods == 0)
        {
            throw new IllegalStateException("Total periods = 0");
        }

        if (getAssetCost() == null || getAssetCost().compareTo(Env.ZERO) <= 0)
        {
            throw new IllegalStateException("Asset cost invalid: "
                    + getAssetCost());
        }

        int precision = getPrecision(assetAcctId);
        BigDecimal initialAccumDepr = getInitialAccumulatedDepr();
        BigDecimal assetCost = getAssetCost();

        int startPeriod = getStartPeriod();

        if (startPeriod >= totalPeriods)
        {
            return;
        }

        BigDecimal depreciationRate = getDepreciationRate(assetAcctId);

        if (depreciationRate.compareTo(Env.ZERO) <= 0)
        {
            throw new IllegalStateException("Invalid depreciation rate: "
                    + depreciationRate);
        }

        String description = "RB Depreciation - Rate = "
                + depreciationRate.toPlainString();

        BigDecimal depreciableAmt = assetCost.subtract(getSalvageValue());

        BigDecimal openingAccumDepr = initialAccumDepr;
        BigDecimal closingAccumDepr = Env.ZERO;
        BigDecimal depreciationExpenseAmt = Env.ZERO;
        Timestamp depreciationDate = getAssetDepreciationDate();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(depreciationDate.getTime());

        super.deleteNotProcessedDepreciations(assetAcctId);

        List<MGAASDepreciationExpense> processedExpenses = MGAASDepreciationExpense
                .getProcessedExpenses(getCtx(), getAssetId(), new Timestamp(0),
                        depreciationDate, getTrxName());

        for (MGAASDepreciationExpense expense : processedExpenses)
        {
            closingAccumDepr = openingAccumDepr.add(expense
                    .getDepreciationExpenseAmt());
            expense.setOpeningBalance(openingAccumDepr.setScale(precision,
                    BigDecimal.ROUND_HALF_UP));
            expense.setClosingBalance(closingAccumDepr.setScale(precision,
                    BigDecimal.ROUND_HALF_UP));
            expense.setPeriodNo(startPeriod);
            expense.saveEx();
            openingAccumDepr = closingAccumDepr;
            startPeriod++;
        }

        for (int i = startPeriod; i <= totalPeriods; i++)
        {
            Timestamp currDate = new Timestamp(cal.getTimeInMillis());
            cal.add(Calendar.MONTH, 1);
            Timestamp nextDate = new Timestamp(cal.getTimeInMillis());

            List<MGAASDepreciationExpense> expenses = MGAASDepreciationExpense
                    .getProcessedExpenses(getCtx(), getAssetId(), currDate,
                            nextDate, getTrxName());

            if (expenses.size() == 0)
            {
                MGAASDepreciationExpense expense = new MGAASDepreciationExpense(
                        getCtx(), 0, getTrxName());
                expense.setAD_Org_ID(getAsset().getAD_Org_ID());
                expense.setPeriodNo(i);
                expense.setGAAS_Asset_Acct_ID(assetAcctId);
                expense.setCostAmt(assetCost);
                expense.setA_Asset_ID(getAssetId());
                expense.setAssetDepreciationDate(new Timestamp(cal
                        .getTimeInMillis()));
                expense.setDescription(description);
                MPeriod acctPeriod = MPeriod.get(getCtx(),
                		new Timestamp(cal.getTimeInMillis()), expense.getAD_Org_ID());
                if (acctPeriod != null)
                {
                    expense.setC_Period_ID(acctPeriod.getC_Period_ID());
                }

                depreciationExpenseAmt = assetCost.subtract(openingAccumDepr)
                        .multiply(depreciationRate);
                depreciationExpenseAmt = depreciationExpenseAmt.divide(
                        new BigDecimal(12), 8, BigDecimal.ROUND_HALF_UP);

                if (openingAccumDepr.add(depreciationExpenseAmt).compareTo(
                        depreciableAmt) > 0)
                {
                    depreciationExpenseAmt = depreciableAmt
                            .subtract(openingAccumDepr);
                }

                if (depreciationExpenseAmt.compareTo(Env.ZERO) < 0)
                {
                    depreciationExpenseAmt = Env.ZERO;
                }

                expense.setDepreciationExpenseAmt(depreciationExpenseAmt
                        .setScale(precision, BigDecimal.ROUND_HALF_UP));

                closingAccumDepr = openingAccumDepr.add(depreciationExpenseAmt);
                expense.setOpeningBalance(openingAccumDepr.setScale(precision,
                        BigDecimal.ROUND_HALF_UP));
                expense.setClosingBalance(closingAccumDepr.setScale(precision,
                        BigDecimal.ROUND_HALF_UP));

                expense.saveEx();
                
                openingAccumDepr = closingAccumDepr;
            }
            else
            {
                for (MGAASDepreciationExpense expense : expenses)
                {
                    closingAccumDepr = openingAccumDepr.add(expense
                            .getDepreciationExpenseAmt());
                    expense.setPeriodNo(i);
                    expense.setOpeningBalance(openingAccumDepr.setScale(precision,
                            BigDecimal.ROUND_HALF_UP));
                    expense.setClosingBalance(closingAccumDepr.setScale(precision,
                            BigDecimal.ROUND_HALF_UP));

                    expense.saveEx();
                    openingAccumDepr = closingAccumDepr;
                    i++;
                }
                i--;
            }
        }
        super.deleteDeprecatedDepreciations();
    }

}
