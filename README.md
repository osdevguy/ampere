# Ampere

Adempiere with a few things left out.

## Getting Started

Introduction to building (using gradle) and deploying (with Docker).

### Prerequisites

You need git and docker-compose installed on your system.


### Installing

Clone this repository.

```
git clone https://gitlab.com/adaxa/ampere.git
```
Run the build.

```
./gradlew build
```
Install build artifacts

```
./gradlew installDist
```
Change to the output directory


```
cd build/install/ampere/
```
Edit the config file (set passwords and hostname and domain)

```
vi .env
```
Create self-signed certificates for testing on system without access for letsencrypt (NB script uses sudo to update host certificate store which you may not want to do!)

```
cd certs
./generate_ssl_cert_and_truststore
cd ..
```
Add rule to hosts for your selected hostname

```
sudo -- sh -c "echo 127.0.0.1 www.ampere.local >> /etc/hosts

```

Fire up the docker containers

```
docker-compose up
```

Open https://www.ampere.local in your browser to view.


## License

This project is licensed under GPL v2 - see the [license.html](license.html) file for details

## Acknowledgments

* [Compiere](http://www.compiere.com/) for the original code
* [Adempiere](http://www.adempiere.net/) all the community contributions